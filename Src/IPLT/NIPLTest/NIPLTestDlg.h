
// NIPLTestDlg.h : header file
//

#pragma once


// CNIPLTestDlg dialog
class CNIPLTestDlg : public CDialogEx
{
// Construction
public:
	CNIPLTestDlg(CWnd* pParent = NULL);	// standard constructor
	~CNIPLTestDlg();	

// Dialog Data
	enum { IDD = IDD_NIPLTEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	void LoadNIPJob();
	void ReleaseNIPLib();

private:
	NIPJob m_dJob;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonTest();
};
