#pragma once

#include "CommonOpenCV.h"

struct AFX_EXT_CLASS NIPLParam
{
	bool m_bEnable;
	bool m_bUseGpu;

	NIPLParam() {
		m_bEnable = false;
		m_bUseGpu = false;
	}
	virtual ~NIPLParam() {
	}
};

struct AFX_EXT_CLASS NIPLParam_CreateImage: public NIPLParam
{
	enum {
		TYPE_GRAY_UINT8 = 0x00,
		TYPE_GRAY_FLOAT = 0x01,
		TYPE_COLOR_UINT8 = 0x02
	};

	enum {
		CHANNEL_1 = 0x00,
		TYPE_FLOAT = 0x00,
	};

	int m_nType;
	int m_nWidth;
	int m_nHeight;
	float m_nValue;

	NIPLParam_CreateImage() {
		Init();
	}
	virtual ~NIPLParam_CreateImage() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nType = TYPE_GRAY_UINT8;
		m_nWidth = 0;
		m_nHeight = 0;
		m_nValue = 0.f;
	}

	bool SetType(wstring strType) {
		m_nType = TYPE_GRAY_UINT8;
		if (strType == L"TYPE_GRAY_UINT8") m_nType = TYPE_GRAY_UINT8;
		else if (strType == L"TYPE_GRAY_FLOAT") m_nType = TYPE_GRAY_FLOAT;
		else if (strType == L"TYPE_COLOR_UINT8") m_nType = TYPE_COLOR_UINT8;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_LoadTemplateImage : public NIPLParam
{
	wstring m_strTemplateImagePath;

	NIPLParam_LoadTemplateImage() {
		Init();
	}
	virtual ~NIPLParam_LoadTemplateImage() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_strTemplateImagePath = L"";
	}
};



struct AFX_EXT_CLASS NIPLParam_Color2Gray : public NIPLParam
{
	enum {
		GRAYLEVEL_256 = 0x00,
		GRAYLEVEL_FLOAT = 0x01,
	};

	enum {
		CHANNEL_ALL = 0x00,
		CHANNEL_R = 0x01,
		CHANNEL_G = 0x02,
		CHANNEL_B = 0x03
	};

	int m_nGrayLevel;
	int m_nChannel;

	NIPLParam_Color2Gray() {
		Init();
	}
	virtual ~NIPLParam_Color2Gray() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nGrayLevel = GRAYLEVEL_256;
		m_nChannel = CHANNEL_ALL;
	}

	bool SetGrayLevel(wstring strGrayLevel) {
		if (strGrayLevel == L"GRAYLEVEL_256") m_nGrayLevel = GRAYLEVEL_256;
		else if (strGrayLevel == L"GRAYLEVEL_FLOAT") m_nGrayLevel = GRAYLEVEL_FLOAT;
		else {
			return false;
		}

		return true;
	}

	bool SetChannel(wstring strChannel) {
		if (strChannel == L"CHANNEL_ALL") m_nChannel = CHANNEL_ALL;
		else if (strChannel == L"CHANNEL_R") m_nChannel = CHANNEL_R;
		else if (strChannel == L"CHANNEL_G") m_nChannel = CHANNEL_G;
		else if (strChannel == L"CHANNEL_B") m_nChannel = CHANNEL_B;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_Copy : public NIPLParam
{
	NIPLParam_Copy() {
		Init();
	}
	virtual ~NIPLParam_Copy() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
	}
};

struct AFX_EXT_CLASS NIPLParam_Invert : public NIPLParam
{
	NIPLParam_Invert() {
		Init();
	}
	virtual ~NIPLParam_Invert() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
	}
};

struct AFX_EXT_CLASS NIPLParam_Reduce : public NIPLParam
{
	enum {
		METHOD_AVERAGE = 0x01,
		METHOD_MIN = 0x03,
		METHOD_MAX = 0x04
	};

	int m_nMethod;
	bool m_bVert;
	bool m_bGraph;

	NIPLParam_Reduce() {
		Init();
	}
	virtual ~NIPLParam_Reduce() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_AVERAGE;
		m_bVert = false;
		m_bGraph = false;
	}

	bool SetMethod(wstring strMethod) {
		if (strMethod == L"METHOD_AVERAGE") m_nMethod = METHOD_AVERAGE;
		else if (strMethod == L"METHOD_MIN") m_nMethod = METHOD_MIN;
		else if (strMethod == L"METHOD_MAX") m_nMethod = METHOD_MAX;
		else {
			return false;
		}

		return true;
	}
};


struct AFX_EXT_CLASS NIPLParam_Operate : public NIPLParam
{
	enum {
		METHOD_ADD = 0x01,
		METHOD_SUBTRACT = 0x02,
		METHOD_AND = 0x03,
		METHOD_OR = 0x04,
		METHOD_XOR = 0x05,
	};

	int m_nMethod;
	Mat m_dTargetImg;

	NIPLParam_Operate() {
		Init();
	}
	virtual ~NIPLParam_Operate() {
		Clear();
	}
	virtual void Clear() {
		m_dTargetImg.release();

		Init();
	}

	void Init() {
		m_nMethod = METHOD_ADD;
	}

	bool SetMethod(wstring strMethod) {
		if (strMethod == L"METHOD_ADD") m_nMethod = METHOD_ADD;
		else if (strMethod == L"METHOD_SUBTRACT") m_nMethod = METHOD_SUBTRACT;
		else if (strMethod == L"METHOD_AND") m_nMethod = METHOD_AND;
		else if (strMethod == L"METHOD_OR") m_nMethod = METHOD_OR;
		else if (strMethod == L"METHOD_XOR") m_nMethod = METHOD_XOR;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_MorphologyOperate : public NIPLParam
{
	enum {
		METHOD_ERODE = 0x01,
		METHOD_DILATE = 0x02,
		METHOD_OPEN = 0x03,
		METHOD_CLOSE = 0x04
	};

	int m_nMethod;
	int m_nFilterSizeX;
	int m_nFilterSizeY;
	bool m_bCircleFilter;

	NIPLParam_MorphologyOperate() {
		Init();
	}
	virtual ~NIPLParam_MorphologyOperate() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_ERODE;
		m_nFilterSizeX = 0;
		m_nFilterSizeY = 0;
		m_bCircleFilter = false;
	}

	bool SetMethod(wstring strMethod) {
		if (strMethod == L"METHOD_ERODE") m_nMethod = METHOD_ERODE;
		else if (strMethod == L"METHOD_DILATE") m_nMethod = METHOD_DILATE;
		else if (strMethod == L"METHOD_OPEN") m_nMethod = METHOD_OPEN;
		else if (strMethod == L"METHOD_CLOSE") m_nMethod = METHOD_CLOSE;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_Move : public NIPLParam
{
	enum {
		FILL_BLACK = 0x01,
		FILL_WHITE = 0x02,
		ROTATE = 0x03
	};

	float m_nOffsetX;
	float m_nOffsetY;
	int m_nBorder;

	NIPLParam_Move() {
		Init();
	}
	virtual ~NIPLParam_Move() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nOffsetX = 0.f;
		m_nOffsetY = 0.f;
	}

	bool SetBorder(wstring strBorder) {
		if (strBorder == L"FILL_BLACK") m_nBorder = FILL_BLACK;
		else if (strBorder == L"FILL_WHITE") m_nBorder = FILL_WHITE;
		else if (strBorder == L"ROTATE") m_nBorder = ROTATE;
		else {
			return false;
		}

		return true;
	}
};


struct AFX_EXT_CLASS NIPLParam_Rotate : public NIPLParam
{
	float m_nCenterPosX;
	float m_nCenterPosY;
	float m_nAngle;
	float m_nScale;

	NIPLParam_Rotate() {
		Init();
	}
	virtual ~NIPLParam_Rotate() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nCenterPosX = 0.f;
		m_nCenterPosY = 0.f;
		m_nAngle = 0.f;
		m_nScale = 0.f;
	}
};

struct AFX_EXT_CLASS NIPLParam_CopySubImageFrom : public NIPLParam
{
	int m_nStartPosX;
	int m_nStartPosY;
	int m_nEndPosX;
	int m_nEndPosY;

	NIPLParam_CopySubImageFrom() {
		Init();
	}
	virtual ~NIPLParam_CopySubImageFrom() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nStartPosX = 0;
		m_nStartPosY = 0;
		m_nEndPosX = 0;
		m_nEndPosY = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_CopySubImageTo : public NIPLParam
{
	Mat m_dTargetImg;
	int m_nStartPosX;
	int m_nStartPosY;

	NIPLParam_CopySubImageTo() {
		Init();
	}
	virtual ~NIPLParam_CopySubImageTo() {
		Clear();
	}
	virtual void Clear() {
		m_dTargetImg.release();
		Init();
	}

	void Init() {
		m_nStartPosX = 0;
		m_nStartPosY = 0;
	}
};

struct AFX_EXT_CLASS NIPLParam_Thresholding : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_UPPER = 0x01,
		METHOD_LOWER = 0x02,
		METHOD_OTSU = 0x04,	// only used for flag, if this value is set, it'll work same to METHOD_UPPER_OTSU
		METHOD_UPPER_OTSU = METHOD_OTSU | METHOD_UPPER,
		METHOD_LOWER_OTSU = METHOD_OTSU | METHOD_LOWER
	};

	int m_nMethod;
	float m_nThreshold;
	bool m_bApplyMaskOnlyToResult;

	NIPLParam_Thresholding() {
		Init();
	}
	virtual ~NIPLParam_Thresholding() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nThreshold = 0.f;
		m_bApplyMaskOnlyToResult = false;
	}

	bool SetMethod(wstring strMethod) {
		if (strMethod == L"METHOD_UPPER") m_nMethod = METHOD_UPPER;
		else if (strMethod == L"METHOD_LOWER") m_nMethod = METHOD_LOWER;
		else if (strMethod == L"METHOD_OTSU") m_nMethod = METHOD_OTSU;	
		else if (strMethod == L"METHOD_UPPER_OTSU") m_nMethod = METHOD_UPPER_OTSU;
		else if (strMethod == L"METHOD_LOWER_OTSU") m_nMethod = METHOD_LOWER_OTSU;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_ColorThresholding : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_UPPER = 0x01,
		METHOD_LOWER = 0x02,
		METHOD_IN_RANGE = 0x03
	};

	int m_nMethod_R;
	int m_nMethod_G;
	int m_nMethod_B;
	float m_nLowerValue_R;
	float m_nLowerValue_G;
	float m_nLowerValue_B;
	float m_nUpperValue_R;
	float m_nUpperValue_G;
	float m_nUpperValue_B;
	float m_nLowerRatio_R_G;
	float m_nLowerRatio_R_B;
	float m_nLowerRatio_G_B;
	float m_nUpperRatio_R_G;
	float m_nUpperRatio_R_B;
	float m_nUpperRatio_G_B;

	NIPLParam_ColorThresholding() {
		Init();
	}
	virtual ~NIPLParam_ColorThresholding() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod_R = METHOD_NONE;
		m_nMethod_G = METHOD_NONE;
		m_nMethod_B = METHOD_NONE;
		m_nLowerValue_R = 0.f;
		m_nLowerValue_G = 0.f;
		m_nLowerValue_B = 0.f;
		m_nUpperValue_R = 0.f;
		m_nUpperValue_G = 0.f;
		m_nUpperValue_B = 0.f;
		m_nLowerRatio_R_G = 0.f;
		m_nLowerRatio_R_B = 0.f;
		m_nLowerRatio_G_B = 0.f;
		m_nUpperRatio_R_G = 0.f;
		m_nUpperRatio_R_B = 0.f;
		m_nUpperRatio_G_B = 0.f;
	}

	bool SetMethod(wstring strMethod, int &nMethod) {
		if (strMethod == L"METHOD_UPPER") nMethod = METHOD_UPPER;
		else if (strMethod == L"METHOD_LOWER") nMethod = METHOD_LOWER;
		else if (strMethod == L"METHOD_IN_RANGE") nMethod = METHOD_IN_RANGE;
		else {
			return false;
		}

		return true;
	}

	bool SetMethod_R(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_R);
	}

	bool SetMethod_G(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_G);
	}

	bool SetMethod_B(wstring strMethod) {
		return SetMethod(strMethod, m_nMethod_B);
	}
};

struct AFX_EXT_CLASS NIPLParam_Smoothing : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_FILTER = 0x01
	};

	int m_nMethod;
	int m_nFilterSize;

	NIPLParam_Smoothing() {
		Init();
	}
	virtual ~NIPLParam_Smoothing() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nFilterSize = 0;
	}

	bool SetMethod(wstring strMethod) {
		m_nMethod = METHOD_NONE;
		if (strMethod == L"METHOD_FILTER") m_nMethod = METHOD_FILTER;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_EdgeDetecting : public NIPLParam
{
	enum {
		METHOD_NONE = 0x00,
		METHOD_CANNY = 0x01,
		METHOD_CONTOUR = 0x02
	};

	int m_nMethod;
	float m_nCannyLowerThreshold;
	float m_nCannyUpperThreshold;
	int m_nFilterSize;
	int m_nLineThickness;

	NIPLParam_EdgeDetecting() {
		Init();
	}
	virtual ~NIPLParam_EdgeDetecting() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nMethod = METHOD_NONE;
		m_nCannyLowerThreshold = 0.f;
		m_nCannyUpperThreshold = 0.f;
		m_nFilterSize = 0;
		m_nLineThickness = 0;
	}

	bool SetMethod(wstring strMethod) {
		if (strMethod == L"METHOD_CANNY") m_nMethod = METHOD_CANNY;
		else if (strMethod == L"METHOD_CONTOUR") m_nMethod = METHOD_CONTOUR;
		else {
			return false;
		}

		return true;
	}
};

struct AFX_EXT_CLASS NIPLParam_Thinning : public NIPLParam
{
	NIPLParam_Thinning() {
		Init();
	}
	virtual ~NIPLParam_Thinning() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
	}
};

struct AFX_EXT_CLASS NIPLParam_ConvexHull : public NIPLParam
{
	bool m_bFill;
	int m_nLineThickness;

	NIPLParam_ConvexHull() {
		Init();
	}
	virtual ~NIPLParam_ConvexHull() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_bFill = false;
		m_nLineThickness = 0;
	}
};

