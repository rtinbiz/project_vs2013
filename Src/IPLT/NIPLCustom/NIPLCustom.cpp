// NIPLCustom.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NIPLCustom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

NIPLCustom *NIPLCustom::pThis = 0x00;

NIPLCustom *NIPLCustom::GetInstance(BOOL bNew)
{
	if(bNew) {
		return new NIPLCustom();
	}

	if(pThis == 0x00) {
		pThis = new NIPLCustom();
	}

	return pThis;
}

void NIPLCustom::ReleaseInstance(NIPLCustom *pThat)
{
	if(pThat != 0x00) {
		delete pThat;
	}
	else if(pThis != 0x00) {
		delete pThis;
		pThis = 0x00;
	}
}

NIPLCustom::NIPLCustom() : NIPLCV()
{

}

NIPLCustom::~NIPLCustom()
{
}
