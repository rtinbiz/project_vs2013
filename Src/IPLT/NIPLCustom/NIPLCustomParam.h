#pragma once

#include "NIPLParam.h"

#define STR_SHOW_IMAGE_OUTPUT L"Output"
#define STR_SHOW_IMAGE_RECT L"Rect"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_INSIDE L"RectMinRadiusEdgeInside"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE L"RectMinRadiusEdge"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN L"RectMinRadiusEdgeBin"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_BIN_2 L"RectMinRadiusEdgeBin2"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_SPOT L"RectMinRadiusEdgeSpot"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE L"RectMinRadiusEdgeHole"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_HOLE_2 L"RectMinRadiusEdgeHole2"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE L"RectMinRadiusEdgeProfile"
#define STR_SHOW_IMAGE_RECT_MIN_RADIUS_EDGE_PROFILE_2 L"RectMinRadiusEdgeProfile2"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE L"RectMaxRadiusEdge"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN L"RectMaxRadiusEdgeBin"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_BIN_2 L"RectMaxRadiusEdgeBin2"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_SPOT L"RectMaxRadiusEdgeSpot"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE L"RectMaxRadiusEdgeHole"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_HOLE_2 L"RectMaxRadiusEdgeHole2"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE L"RectMaxRadiusEdgeProfile"
#define STR_SHOW_IMAGE_RECT_MAX_RADIUS_EDGE_PROFILE_2 L"RectMaxRadiusEdgeProfile2"
#define STR_SHOW_IMAGE_RECT_FIT_BACKGROUND L"RectFitBackground"
#define STR_SHOW_IMAGE_RECT_EXT_CHAR L"RectExtChar"
#define STR_SHOW_IMAGE_RECT_BIN_CHAR L"RectBinChar"
#define STR_SHOW_IMAGE_RECT_BIN_CHAR_REGION L"RectBinCharRegion"
#define STR_SHOW_IMAGE_RECT_MATCHED_CHAR_BIN L"RectMatchedCharBin"
#define STR_SHOW_IMAGE_RECT_MATCHED_CHAR L"RectMatchedChar"
#define STR_SHOW_IMAGE_RECT_MATCHED_CHAR_TEMPLATE L"RectMatchedCharTemplate"
#define STR_SHOW_IMAGE_RECT_CHAR_LOT_NUMBER L"RectCharLotNumber"
#define STR_SHOW_IMAGE_RECT_CHAR L"RectChar"
#define STR_SHOW_IMAGE_RECT_BROKEN_CHAR L"RectBrokenChar"
#define STR_SHOW_IMAGE_RECT_SURFACE L"RectSurface"
#define STR_SHOW_IMAGE_RECT_SURFACE_BLOCK L"RectSurfaceBlock"
#define STR_SHOW_IMAGE_RECT_SURFACE_BLOCK_BIN L"RectSurfaceBlockBin"
#define STR_SHOW_IMAGE_RECT_HOLE L"RectHole"
#define STR_SHOW_IMAGE_RECT_HOLE_BLOCK L"RectHoleBlock"
#define STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN L"RectHoleBlockBin"
#define STR_SHOW_IMAGE_RECT_HOLE_BLOCK_BIN_CIRCLE L"RectHoleBlockBinCircle"
//#define STR_SHOW_IMAGE_RECT_BIN L"RectBin"
//#define STR_SHOW_IMAGE_RECT_BIN_ELIMIATE_NOISE L"RectBinEliminateNoise"
//#define STR_SHOW_IMAGE_CIRCLE_BIN L"CircleBin"
//#define STR_SHOW_IMAGE_CIRCLE_BIN_ELIMIATE_NOISE L"CircleBinEliminateNoise"

#define STR_SHOW_IMAGE_BIN L"Bin"
#define STR_SHOW_IMAGE_ELMINIATE_NOISE L"ElmilinateNoise"
#define STR_SHOW_IMAGE_MAJOR_REGION L"MajorRegion"
#define STR_SHOW_IMAGE_FILL_HOLE L"FillHole"
#define STR_SHOW_IMAGE_EDGE L"Edge"
#define STR_SHOW_IMAGE_CIRCLE L"Circle"
#define STR_SHOW_IMAGE_LINE_BY_CIRCLE_TEST L"LineByCircleTest"
#define STR_SHOW_IMAGE_LINE L"Line"

#define STR_SHOW_IMAGE_COLOR_CONVERT L"ColorConvert"
#define STR_SHOW_IMAGE_MIN_CIRCLE L"MinCircle"
#define STR_SHOW_IMAGE_MIN_CIRCLE_BIN L"MinCircleBin"
#define STR_SHOW_IMAGE_MIN_CIRCLE_EDGE L"MinCircleEdge"
#define STR_SHOW_IMAGE_MIN_CIRCLE_OUTPUT L"MinCircleOutput"
#define STR_SHOW_IMAGE_MAX_CIRCLE L"MaxCircle"
#define STR_SHOW_IMAGE_MAX_CIRCLE_BIN L"MaxCircleBin"
#define STR_SHOW_IMAGE_MAX_CIRCLE_EDGE L"MaxCircleEdge"
#define STR_SHOW_IMAGE_MAX_CIRCLE_OUTPUT L"MaxCircleOutput"
#define STR_SHOW_IMAGE_CIRCLE_OUTPUT L"CircleOutput"
#define STR_SHOW_IMAGE_CIRCLE_MARGIN_OUTPUT L"CircleMarginOutput"

#define STR_LDC_COMP_TERMINAL L"Terminal"
#define STR_LDC_COMP_GUIDE L"Guide"
#define STR_LDC_COMP_TUBE L"Tube"
#define STR_LDC_COMP_BOLT L"Bolt"
#define STR_LDC_COMP_CLIP L"Clip"
#define STR_LDC_COMP_PAPER L"Paper"

struct NIPL_LDC_ROI {
	enum COLOR_TYPE {
		COLOR_BLACK,
		COLOR_RED,
		COLOR_WHITE
	};
	int m_nType;
	int m_nSetId;
	Rect m_rcBoundingBox;
	Point m_ptRef;
};

struct AFX_EXT_CLASS NIPLParam_LDC : public NIPLParam
{
	wstring m_strDataPath;
	vector<NIPL_LDC_ROI> m_listROI;
	wstring m_strShowImage;

	float m_nThreshold;
	float m_nRefThreshold;

	bool m_bForceDetectAll;

	NIPLParam_LDC() {
		Init();
	}
	virtual ~NIPLParam_LDC() {
		Clear();
	}
	virtual void Clear() {
		m_listROI.clear();

		Init();
	}

	void Init() {
		m_strDataPath = L"";
		m_strShowImage = L"";

		m_nThreshold = 0.f;
		m_nRefThreshold = 0.f;

		m_bForceDetectAll = false;
	}
};

struct AFX_EXT_CLASS NIPLParam_LDC_Calibration : public NIPLParam
{
	wstring m_strDataPath;
	bool m_bAutoDetect;

	Point m_ptStartLeftGuidLine;
	Point m_ptEndLeftGuidLine;
	Point m_ptStartRightGuidLine;
	Point m_ptEndRightGuidLine;

	int m_nAutoDetectLeftRange;
	int m_nAutoDetectRightRange;

	int m_nNormalizeWidth;

	NIPLParam_LDC_Calibration() {
		Init();
	}
	virtual ~NIPLParam_LDC_Calibration() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_strDataPath = L"";
		m_bAutoDetect = false;

		m_ptStartLeftGuidLine = Point(0, 0);
		m_ptEndLeftGuidLine = Point(0, 0);
		m_ptStartRightGuidLine = Point(0, 0);
		m_ptEndRightGuidLine = Point(0, 0);

		m_nAutoDetectLeftRange = 0;
		m_nAutoDetectRightRange = 0;

		m_nNormalizeWidth = 0;
	}
};

//
// MagCore
// 

#define STR_MAGCORE_COMP_UPPER L"Upper"
#define STR_MAGCORE_COMP_LOWER L"Lower"
#define STR_MAGCORE_COMP_INNER L"Inner"
#define STR_MAGCORE_COMP_OUTER L"Outer"

struct AFX_EXT_CLASS NIPLParam_MagCore: public NIPLParam
{
	wstring m_strDataPath;
	wstring m_strComp;

/*
	int m_nRadiusEdgeThickness;
	float m_nRadiusEdgeBinarizeThreshold;
	float m_nRadiusEdgeBinarizeThreshold2;
	int m_nRadiusEdgeDefectMinSize;
	int m_nRadiusEdgeDefectMinSize2;
	int m_nRadiusEdgeDefectMaxSize;
*/
	bool m_bRadiusEdgeDoubleLine;
	bool m_bMinRadiusEdgeInsideCheck;
	bool m_bMinRadiusEdgeCheck;
	bool m_bMaxRadiusEdgeCheck;
	bool m_bMinRadiusEdgeBlack;
	bool m_bMaxRadiusEdgeBlack;
	int m_nMinRadiusEdgeInsideAreaHeight;
	int m_nMinRadiusEdgeAreaHeight;
	int m_nMaxRadiusEdgeAreaHeight;
/*
	float m_nRadiusEdgeLineBinarizeThreshold;
	float m_nRadiusEdgeLineProfileMinMean;
	float m_nRadiusEdgeLineProfileMinStd;
	float m_nRadiusEdgeLineProfileMaxStd;
*/
	float m_nMinRadiusEdgeInsideThreshold;
	float m_nMinRadiusEdgeSpotBinarizeThreshold;
	float m_nMaxRadiusEdgeSpotBinarizeThreshold;
	float m_nMinRadiusEdgeSpotProfileThreshold;
	float m_nMaxRadiusEdgeSpotProfileThreshold;
	float m_nMinRadiusEdgeHoleBinarizeThreshold;
	float m_nMinRadiusEdgeHoleBinarizeThreshold2;
	float m_nMaxRadiusEdgeHoleBinarizeThreshold;
	float m_nMaxRadiusEdgeHoleBinarizeThreshold2;
	float m_nMinRadiusEdgeBinarizeThreshold;
	float m_nMinRadiusEdgeBinarizeThreshold2;
	float m_nMaxRadiusEdgeBinarizeThreshold;
	float m_nMaxRadiusEdgeBinarizeThreshold2;
	float m_nMinRadiusEdgeProfileThreshold;
	float m_nMinRadiusEdgeProfileThreshold2;
	float m_nMaxRadiusEdgeProfileThreshold;
	float m_nMaxRadiusEdgeProfileThreshold2;
	int m_nRadiusEdgeSpotMinSize;
	int m_nRadiusEdgeSpotMaxSize;
	int m_nRadiusEdgeHoleMinSize;
	int m_nRadiusEdgeHoleMaxSize;
	int m_nRadiusEdgePerfectHoleMinSize;
	int m_nRadiusEdgeMinSize;
	int m_nRadiusEdgeMaxSize;
	float m_nRadiusEdgeProfileCurveBarSize;
	float m_nRadiusEdgeProfileCurveAngle;
	float m_nRadiusEdgeProfileDensity;

	int m_nSurfaceBlockCount;
	int m_nSurfaceMinRadiusEdgeMargin;
	int m_nSurfaceMaxRadiusEdgeMargin;
	float m_nSurfaceBlockBinarizeThreshold;
	float m_nSurfaceBlockVarianceRatio;
	float m_nSurfaceBlockVarianceMax;
	float m_nSurfaceBlockVarianceThreshold;

	int m_nHoleBlockCountX;
	int m_nHoleBlockCountY;
	int m_nHoleMinRadiusEdgeMargin;
	int m_nHoleMaxRadiusEdgeMargin;
	float m_nHoleBinarizeThreshold;
	int m_nHoleMinSize;
	int m_nHoleMaxSize;
	float m_nHoleSizeRatio;
	float m_nHoleFitRatio;
	float m_nHoleSizeFitRatio;
	float m_nHoleFillRatio;
	int m_nHoleSmallSize;
	float m_nHoleSmallSizeRatio;
	float m_nHoleSmallFitRatio;
	float m_nHoleSmallSizeFitRatio;
	int m_nHoleBigSize;
	float m_nHoleBigSizeRatio;

	bool m_bCharCheck;
	Mat m_dCharTemplateImage;
	int m_nCharMinRadiusEdgeMargin;
	int m_nCharMaxRadiusEdgeMargin;
	float m_nCharBinarizeThreshold;
	int m_nCharConnectDistance;
	float m_nCharRecogRatio;
	float m_nCharMatchRatio;
	int m_nCharAreaSize;
	float m_nCharLotNumberAreaRatio;
	float m_nCharFillRatioX;
	float m_nCharFillRatioY;
	bool m_bCharBlack;

	wstring m_strShowImage;

	NIPLParam_Circle2Rect m_dParam_Circle2Rect;
	//NIPLParam_FitBackground m_dParam_FitBackground;
	//NIPLParam_Binarize m_dParam_Binarize;
	//NIPLParam_EliminateNoise m_dParam_EliminateNoise;
	//NIPLParam_Rect2Circle m_dParam_Rect2Circle;

	NIPLParam_MagCore() {
		Init();
	}
	virtual ~NIPLParam_MagCore() {
		Clear();
	}
	virtual void Clear() {
		m_dParam_Circle2Rect.Clear();
//		m_dParam_FitBackground.Clear();
//		m_dParam_Binarize.Clear();
//		m_dParam_EliminateNoise.Clear();
//		m_dParam_Rect2Circle.Clear();

		m_dCharTemplateImage.release();

		Init();
	}

	void Init() {
		m_strDataPath = L"";
		m_strComp = L"";
		m_strShowImage = L"";

		m_bRadiusEdgeDoubleLine = false;
		m_bMinRadiusEdgeInsideCheck = false;
		m_bMinRadiusEdgeCheck = false;
		m_bMaxRadiusEdgeCheck = false;
		m_bMinRadiusEdgeBlack = false;
		m_bMaxRadiusEdgeBlack = false;
		m_nMinRadiusEdgeInsideAreaHeight = 0;
		m_nMinRadiusEdgeAreaHeight = 0;
		m_nMaxRadiusEdgeAreaHeight = 0;
		m_nMinRadiusEdgeInsideThreshold = 0.f;
		m_nMinRadiusEdgeSpotBinarizeThreshold = 0.f;
		m_nMaxRadiusEdgeSpotBinarizeThreshold = 0.f;
		m_nMinRadiusEdgeSpotProfileThreshold = 0.f;
		m_nMaxRadiusEdgeSpotProfileThreshold = 0.f;
		m_nMinRadiusEdgeHoleBinarizeThreshold = 0.f;
		m_nMinRadiusEdgeHoleBinarizeThreshold2 = 0.f;
		m_nMaxRadiusEdgeHoleBinarizeThreshold = 0.f;
		m_nMaxRadiusEdgeHoleBinarizeThreshold2 = 0.f;
		m_nMinRadiusEdgeBinarizeThreshold = 0.f;
		m_nMinRadiusEdgeBinarizeThreshold2 = 0.f;
		m_nMaxRadiusEdgeBinarizeThreshold = 0.f;
		m_nMaxRadiusEdgeBinarizeThreshold2 = 0.f;
		m_nMinRadiusEdgeProfileThreshold = 0.f;
		m_nMinRadiusEdgeProfileThreshold2 = 0.f;
		m_nMaxRadiusEdgeProfileThreshold = 0.f;
		m_nMaxRadiusEdgeProfileThreshold2 = 0.f;
		m_nRadiusEdgeSpotMinSize = 0;
		m_nRadiusEdgeSpotMaxSize = 0;
		m_nRadiusEdgeHoleMinSize = 0;
		m_nRadiusEdgeHoleMaxSize = 0;
		m_nRadiusEdgeMinSize = 0;
		m_nRadiusEdgeMaxSize = 0;
		m_nRadiusEdgeProfileCurveBarSize = 0.f;
		m_nRadiusEdgeProfileCurveAngle = 0.f;
		m_nRadiusEdgeProfileDensity = 0.f;

		m_nSurfaceBlockCount = 0;
		m_nSurfaceMinRadiusEdgeMargin = 0;
		m_nSurfaceMaxRadiusEdgeMargin = 0;
		m_nSurfaceBlockBinarizeThreshold = 0.f;
		m_nSurfaceBlockVarianceRatio = 0.f;
		m_nSurfaceBlockVarianceMax = 0.f;
		m_nSurfaceBlockVarianceThreshold = 0.f;

		m_nHoleBlockCountX = 0;
		m_nHoleBlockCountY = 0;
		m_nHoleMinRadiusEdgeMargin = 0;
		m_nHoleMaxRadiusEdgeMargin = 0;
		m_nHoleBinarizeThreshold = 0.f;
		m_nHoleMinSize = 0;
		m_nHoleMaxSize = 0;
		m_nHoleSizeRatio = 0.f;
		m_nHoleFitRatio = 0.f;
		m_nHoleSizeFitRatio = 0.f;
		m_nHoleFillRatio = 0.f;
		m_nHoleSmallSize = 0;
		m_nHoleSmallSizeRatio = 0.f;
		m_nHoleSmallFitRatio = 0.f;
		m_nHoleSmallSizeFitRatio = 0.f;
		m_nHoleBigSize = 0;
		m_nHoleBigSizeRatio = 0.f;

		m_bCharCheck = false;
		m_nCharMinRadiusEdgeMargin = 0;
		m_nCharMaxRadiusEdgeMargin = 0;
		m_nCharBinarizeThreshold = 0.f;
		m_nCharConnectDistance = 0;
		m_nCharRecogRatio = 0.f;
		m_nCharMatchRatio = 0.f;
		m_nCharAreaSize = 0;
		m_nCharLotNumberAreaRatio = 0.f;
		m_nCharFillRatioX = 0.f;
		m_nCharFillRatioY = 0.f;
		m_bCharBlack = false;
	}
};

struct AFX_EXT_CLASS NIPLParam_MagCore_Calibration : public NIPLParam_MagCore
{
	int m_nColor2GrayChannel;
	int m_nBrightnessEnhancementLevel;

	float m_nCheckCoreThreshold;

	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nMinRadius;
	int m_nMaxRadius;
	int m_nMinRadiusRange;
	int m_nMaxRadiusRange;
	int m_nMinRadiusMargin;
	int m_nMaxRadiusMargin;

	int m_nMinRadiusPosX;
	int m_nMaxRadiusPosX;
	float m_nSamplingAngleStep;
	float m_nCircle2RectAngleStep;
	float m_nRect2CircleAngleStep;

	bool m_bMinRadiusReverseEdgeDetect;
	bool m_bMaxRadiusReverseEdgeDetect;
	bool m_bMinRadiusBinarize;
	bool m_bMaxRadiusBinarize;
	bool m_bMinRadiusBlack;
	bool m_bMaxRadiusBlack;

	NIPLParam_MagCore_Calibration() {
		Init();
	}
	virtual ~NIPLParam_MagCore_Calibration() {
		Clear();
	}
	virtual void Clear() {
		Init();
	}

	void Init() {
		m_nColor2GrayChannel = 0;
		m_nBrightnessEnhancementLevel = 0;

		m_nCheckCoreThreshold = 0.f;

		m_nCenterPosX = 0;
		m_nCenterPosY = 0;
		m_nMinRadius = 0;
		m_nMaxRadius = 0;
		m_nMinRadiusRange = 0;
		m_nMaxRadiusRange = 0;
		m_nMinRadiusMargin = 0;
		m_nMaxRadiusMargin = 0;

		m_nMinRadiusPosX = 0;
		m_nMaxRadiusPosX = 0;

		m_nSamplingAngleStep = 0.f;
		m_nCircle2RectAngleStep = 0.f;
		m_nRect2CircleAngleStep = 0.f;

		m_bMinRadiusReverseEdgeDetect = false;
		m_bMaxRadiusReverseEdgeDetect = false;
		m_bMinRadiusBinarize = false;
		m_bMaxRadiusBinarize = false;
		m_bMinRadiusBlack = false;
		m_bMaxRadiusBlack = false;
	}

	bool SetColor2GrayChannel(wstring strChannel) {
		m_nColor2GrayChannel = NIPLParam_Color2Gray::CHANNEL_ALL;
		if (strChannel == L"CHANNEL_ALL") m_nColor2GrayChannel = NIPLParam_Color2Gray::CHANNEL_ALL;
		else if (strChannel == L"CHANNEL_R") m_nColor2GrayChannel = NIPLParam_Color2Gray::CHANNEL_R;
		else if (strChannel == L"CHANNEL_G") m_nColor2GrayChannel = NIPLParam_Color2Gray::CHANNEL_G;
		else if (strChannel == L"CHANNEL_B") m_nColor2GrayChannel = NIPLParam_Color2Gray::CHANNEL_B;
		else {
			return false;
		}

		return true;
	}
};

