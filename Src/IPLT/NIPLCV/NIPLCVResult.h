#pragma once

#include "NIPLResult.h"

#define NIPLBLOB_BOUNDING_BOX_START_X L"Start X"
#define NIPLBLOB_BOUNDING_BOX_START_Y L"Start Y"
#define NIPLBLOB_BOUNDING_BOX_END_X L"End X"
#define NIPLBLOB_BOUNDING_BOX_END_Y L"End Y"
#define NIPLBLOB_SIZE L"Size"

struct AFX_EXT_CLASS NIPLBlob {
	Rect m_rcBoundingBox;
	int m_nSize;
	Mat m_dImg;

	NIPLBlob(Rect rcBoundingBox, int nSize) {
		m_rcBoundingBox = rcBoundingBox;
		m_nSize = nSize;
	}

	NIPLBlob(Mat dImg, Rect rcBoundingBox, int nSize) {
		m_dImg = dImg;
		m_rcBoundingBox = rcBoundingBox;
		m_nSize = nSize;
	}

	Rect GetBoundingBox() {
		return m_rcBoundingBox;
	}

	Point GetCenterPos() {
		Rect rc = GetBoundingBox();
		Point pt(rc.x + rc.width / 2, rc.y + rc.height / 2);

		return pt;
	}

	bool GetValue(wstring strName, int &nValue)
	{
		float nValueFloat;
		if (!GetValue(strName, nValueFloat)) {
			return false;
		}

		nValue = (int)nValueFloat;
		return true;
	}

	bool GetValue(wstring strName, float &nValue)
	{
		if (CHECK_STRING(strName, NIPLBLOB_BOUNDING_BOX_START_X)) {
			nValue = (float)m_rcBoundingBox.x;
		}
		else if (CHECK_STRING(strName, NIPLBLOB_BOUNDING_BOX_START_Y)) {
			nValue = (float)m_rcBoundingBox.y;
		}
		else if (CHECK_STRING(strName, NIPLBLOB_BOUNDING_BOX_END_X)) {
			nValue = (float)(m_rcBoundingBox.x + m_rcBoundingBox.width);
		}
		else if (CHECK_STRING(strName, NIPLBLOB_BOUNDING_BOX_END_Y)) {
			nValue = (float)(m_rcBoundingBox.y + m_rcBoundingBox.height);
		}
		else if (CHECK_STRING(strName, NIPLBLOB_SIZE)) {
			nValue = (float)m_nSize;
		}
		else {
			return false;
		}

		return true;
	}

	wstring GetDesc() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"(%d, %d, %d, %d) Size : %d",
			m_rcBoundingBox.x, m_rcBoundingBox.y, m_rcBoundingBox.x + m_rcBoundingBox.width, m_rcBoundingBox.y + m_rcBoundingBox.height,
			m_nSize);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"(%d, %d, %d, %d) Size : %d",
			m_rcBoundingBox.x, m_rcBoundingBox.y, m_rcBoundingBox.x + m_rcBoundingBox.width, m_rcBoundingBox.y + m_rcBoundingBox.height,
			m_nSize);
#endif

		return szDesc;
	}

	wstring GetTitleForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%s\t%s\t%s\t%s\t%s",
			NIPLBLOB_BOUNDING_BOX_START_X, NIPLBLOB_BOUNDING_BOX_START_Y,
			NIPLBLOB_BOUNDING_BOX_END_X, NIPLBLOB_BOUNDING_BOX_END_Y,
			NIPLBLOB_SIZE);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%s\t%s\t%s\t%s\t%s",
			NIPLBLOB_BOUNDING_BOX_START_X, NIPLBLOB_BOUNDING_BOX_START_Y,
			NIPLBLOB_BOUNDING_BOX_END_X, NIPLBLOB_BOUNDING_BOX_END_Y,
			NIPLBLOB_SIZE);
#endif

		return szDesc;
	}

	wstring GetDescForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%d\t%d\t%d\t%d\t%d",
			m_rcBoundingBox.x, m_rcBoundingBox.y, m_rcBoundingBox.x + m_rcBoundingBox.width, m_rcBoundingBox.y + m_rcBoundingBox.height,
			m_nSize);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%d\t%d\t%d\t%d\t%d",
			m_rcBoundingBox.x, m_rcBoundingBox.y, m_rcBoundingBox.x + m_rcBoundingBox.width, m_rcBoundingBox.y + m_rcBoundingBox.height,
			m_nSize);
		swprintf(szDesc, 256, L"%s\t%s\t%s\t%s\t%s",
			NIPLBLOB_BOUNDING_BOX_START_X, NIPLBLOB_BOUNDING_BOX_START_Y,
			NIPLBLOB_BOUNDING_BOX_END_X, NIPLBLOB_BOUNDING_BOX_END_Y,
			NIPLBLOB_SIZE);
#endif        

		return szDesc;
	}
};

#define NIPLDEFECT_TYPE L"Type"
struct AFX_EXT_CLASS NIPLDefect : public NIPLBlob {
	int m_nType;
	int m_nSetId;

	NIPLDefect(int nType, Rect rcBoundingBox, int nSetId, int nSize) : NIPLBlob(rcBoundingBox, nSize) {
		m_nType = nType;
		m_nSetId = nSetId;
	}

	bool GetValue(wstring strName, int &nValue)
	{
		float nValueFloat;
		if (!GetValue(strName, nValueFloat)) {
			return false;
		}

		nValue = (int)nValueFloat;
		return true;
	}

	bool GetValue(wstring strName, float &nValue)
	{
		if (NIPLBlob::GetValue(strName, nValue)) {
			return true;
		}

		if (CHECK_STRING(strName, NIPLDEFECT_TYPE)) {
			nValue = (float)m_nType;
		}
		else {
			return false;
		}

		return true;
	}

	wstring GetDesc() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"Type(%d) ", m_nType);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"Type(%d) ", m_nType);
#endif           

		wstring strDesc = szDesc;
		strDesc += NIPLBlob::GetDesc();

		return strDesc;
	}

	wstring GetTitleForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%s\t", NIPLDEFECT_TYPE);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%s\t", NIPLDEFECT_TYPE);
#endif           
		
		wstring strDesc = szDesc;
		strDesc += NIPLBlob::GetTitleForSaving();

		return strDesc;
	}

	wstring GetDescForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%d\t", m_nType);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%d\t", m_nType);
#endif               
		
		wstring strDesc = szDesc;
		strDesc += NIPLBlob::GetDescForSaving();

		return strDesc;
	}

	virtual wstring GetTypeDesc() {
		return L"";
	}
};

#define NIPLLINE_START_X L"Start X"
#define NIPLLINE_START_Y L"Start Y"
#define NIPLLINE_END_X L"End X"
#define NIPLLINE_END_Y L"End Y"
#define NIPLLINE_LENGTH L"Length"
#define NIPLLINE_ANGLE L"Angle"

struct AFX_EXT_CLASS NIPLLine {
	Point m_ptStart;
	Point m_ptEnd;
	float m_nLength;
	float m_nAngle;

	NIPLLine() {
		m_ptStart = Point(0, 0);
		m_ptEnd = Point(0, 0);
		m_nLength = 0.f;
		m_nAngle = 0.f;
	}

	NIPLLine(Point ptStart, Point ptEnd, float nLength, float nAngle) {
		m_ptStart = ptStart;
		m_ptEnd = ptEnd;
		m_nLength = nLength;
		m_nAngle = nAngle;
	}

	Rect GetBoundingBox() {
		Rect rc;
		rc.x = min(m_ptStart.x, m_ptEnd.x);
		rc.y = min(m_ptStart.y, m_ptEnd.y); 
		rc.width = abs(m_ptEnd.x - m_ptStart.x) + 1;
		rc.height = abs(m_ptEnd.y - m_ptStart.y) + 1;

		return rc;
	}

	Point GetCenterPos() {
		Rect rc = GetBoundingBox();
		Point pt(rc.x + rc.width / 2, rc.y + rc.height / 2);

		return pt;
	}

	bool GetValue(wstring strName, int &nValue)
	{
		float nValueFloat;
		if (!GetValue(strName, nValueFloat)) {
			return false;
		}

		nValue = (int)nValueFloat;
		return true;
	}

	bool GetValue(wstring strName, float &nValue)
	{
		if (CHECK_STRING(strName, NIPLLINE_START_X)) {
			nValue = (float)m_ptStart.x;
		}
		else if (CHECK_STRING(strName, NIPLLINE_START_Y)) {
			nValue = (float)m_ptStart.y;
		}
		else if (CHECK_STRING(strName, NIPLLINE_END_X)) {
			nValue = (float)m_ptEnd.x;
		}
		else if (CHECK_STRING(strName, NIPLLINE_END_Y)) {
			nValue = (float)m_ptEnd.y;
		}
		else if (CHECK_STRING(strName, NIPLLINE_LENGTH)) {
			nValue = (float)m_nLength;
		}
		else if (CHECK_STRING(strName, NIPLLINE_ANGLE)) {
			nValue = (float)m_nAngle;
		}
		else {
			return false;
		}

		return true;
	}

	wstring GetDesc() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"(%d, %d) (%d, %d) Length : %.1f %.1f",
			m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y, m_nLength, m_nAngle);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"(%d, %d) (%d, %d) Length : %.1f %.1f",
			m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y, m_nLength, m_nAngle);
#endif        

		return szDesc;
	}

	wstring GetTitleForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%s\t%s\t%s\t%s\t%s\t%s",
			NIPLLINE_START_X, NIPLLINE_START_Y, NIPLLINE_END_X, NIPLLINE_END_Y, NIPLLINE_LENGTH, NIPLLINE_ANGLE);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%s\t%s\t%s\t%s\t%s\t%s",
			NIPLLINE_START_X, NIPLLINE_START_Y, NIPLLINE_END_X, NIPLLINE_END_Y, NIPLLINE_LENGTH, NIPLLINE_ANGLE);
#endif        

		return szDesc;
	}

	wstring GetDescForSaving() {
		wchar_t szDesc[256];

#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%d\t%d\t%d\t%d\t%.1f\t%.1f",
			m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y, m_nLength, m_nAngle);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%d\t%d\t%d\t%d\t%.1f\t%.1f",
			m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y, m_nLength, m_nAngle);
#endif           

		return szDesc;
	}
};


#define NIPLCIRCLE_CENTER_X L"Center X"
#define NIPLCIRCLE_CENTER_Y L"Center Y"
#define NIPLCIRCLE_RADIUS L"Radius"

struct AFX_EXT_CLASS NIPLCircle {
	Point m_ptCenter;
	int m_nRadius;

	NIPLCircle() {
		m_ptCenter = Point(0, 0);
		m_nRadius = 0;
	}

	NIPLCircle(Point ptCenter, int nRadius) {
		m_ptCenter = ptCenter;
		m_nRadius = nRadius;
	}

	Rect GetBoundingBox() {
		Rect rc;
		rc.x = m_ptCenter.x - m_nRadius;
		rc.y = m_ptCenter.y - m_nRadius;
		rc.width = 2 * m_nRadius;
		rc.height = 2 * m_nRadius;

		return rc;
	}

	Point GetCenterPos() {
		return m_ptCenter;
	}

	bool GetValue(wstring strName, int &nValue)
	{
		float nValueFloat;
		if (!GetValue(strName, nValueFloat)) {
			return false;
		}

		nValue = (int)nValueFloat;
		return true;
	}

	bool GetValue(wstring strName, float &nValue)
	{
		if (CHECK_STRING(strName, NIPLCIRCLE_CENTER_X)) {
			nValue = (float)m_ptCenter.x;
		}
		else if (CHECK_STRING(strName, NIPLCIRCLE_CENTER_Y)) {
			nValue = (float)m_ptCenter.y;
		}
		else if (CHECK_STRING(strName, NIPLCIRCLE_RADIUS)) {
			nValue = (float)m_nRadius;
		}
		else {
			return false;
		}

		return true;
	}

	wstring GetDesc() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"(%d, %d) Radius : %d", m_ptCenter.x, m_ptCenter.y, m_nRadius);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"(%d, %d) Radius : %d", m_ptCenter.x, m_ptCenter.y, m_nRadius);
#endif           

		return szDesc;
	}

	wstring GetTitleForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%s\t%s\t%s", NIPLCIRCLE_CENTER_X, NIPLCIRCLE_CENTER_Y, NIPLCIRCLE_RADIUS);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%s\t%s\t%s", NIPLCIRCLE_CENTER_X, NIPLCIRCLE_CENTER_Y, NIPLCIRCLE_RADIUS);
#endif           

		return szDesc;
	}

	wstring GetDescForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%d\t%d\t%d", m_ptCenter.x, m_ptCenter.y, m_nRadius);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%d\t%d\t%d", m_ptCenter.x, m_ptCenter.y, m_nRadius);
#endif           

		return szDesc;
	}
};

#define NIPLELLIPSE_CENTER_X L"Center X"
#define NIPLELLIPSE_CENTER_Y L"Center Y"
#define NIPLELLIPSE_WIDTH L"Width"
#define NIPLELLIPSE_HEIGHT L"Height"
#define NIPLELLIPSE_ANGLE L"Angle"

struct AFX_EXT_CLASS NIPLEllipse {
	RotatedRect m_rcEllipse;

	NIPLEllipse() {
		m_rcEllipse = RotatedRect();
	}

	NIPLEllipse(RotatedRect rcEllipse) {
		m_rcEllipse = rcEllipse;
	}

	Rect GetBoundingBox() {
		Point2f center = m_rcEllipse.center;
		Size2f size = m_rcEllipse.size;
		float angle = m_rcEllipse.angle;

		float degree = angle * 3.1415f / 180.f;
		float majorAxe = size.width / 2.f;
		float minorAxe = size.height / 2.f;
		float x = center.x;
		float y = center.y;
		float c_degree = cos(degree);
		float s_degree = sin(degree);
		float t1 = atan(-(majorAxe*s_degree) / (minorAxe*c_degree));
		float c_t1 = cos(t1);
		float s_t1 = sin(t1);
		float w1 = majorAxe*c_t1*c_degree;
		float w2 = minorAxe*s_t1*s_degree;
		float maxX = x + w1 - w2;
		float minX = x - w1 + w2;

		t1 = atan((minorAxe*c_degree) / (majorAxe*s_degree));
		c_t1 = cos(t1);
		s_t1 = sin(t1);
		w1 = minorAxe*s_t1*c_degree;
		w2 = majorAxe*c_t1*s_degree;
		float maxY = y + w1 + w2;
		float minY = y - w1 - w2;
		if (minY > maxY) {
			float temp = minY;
			minY = maxY;
			maxY = temp;
		}
		if (minX > maxX) {
			float temp = minX;
			minX = maxX;
			maxX = temp;
		}

		Rect r((int)minX, (int)minY, (int)(maxX - minX + 1), (int)(maxY - minY + 1));
		return r;
	}

	Point GetCenterPos() {
		Point pt = Point((int)m_rcEllipse.center.x, (int)m_rcEllipse.center.y);
		return pt;
	}

	bool GetValue(wstring strName, int &nValue)
	{
		float nValueFloat;
		if (!GetValue(strName, nValueFloat)) {
			return false;
		}

		nValue = (int)nValueFloat;
		return true;
	}

	bool GetValue(wstring strName, float &nValue)
	{
		if (CHECK_STRING(strName, NIPLELLIPSE_CENTER_X)) {
			nValue = m_rcEllipse.center.x;
		}
		else if (CHECK_STRING(strName, NIPLELLIPSE_CENTER_Y)) {
			nValue = m_rcEllipse.center.y;
		}
		else if (CHECK_STRING(strName, NIPLELLIPSE_WIDTH)) {
			nValue = m_rcEllipse.size.width;
		}
		else if (CHECK_STRING(strName, NIPLELLIPSE_HEIGHT)) {
			nValue = m_rcEllipse.size.height;
		}
		else if (CHECK_STRING(strName, NIPLELLIPSE_ANGLE)) {
			nValue = m_rcEllipse.angle;
		}
		else {
			return false;
		}

		return true;
	}

	wstring GetDesc() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"(%.1f, %.1f) Size(%.1f, %.1f) Angle : %.1f",
			m_rcEllipse.center.x, m_rcEllipse.center.y, m_rcEllipse.size.width, m_rcEllipse.size.height, m_rcEllipse.angle);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"(%.1f, %.1f) Size(%.1f, %.1f) Angle : %.1f",
			m_rcEllipse.center.x, m_rcEllipse.center.y, m_rcEllipse.size.width, m_rcEllipse.size.height, m_rcEllipse.angle);
#endif           

		return szDesc;
	}

	wstring GetTitleForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%s\t%s\t%s\t%s\t%s", NIPLELLIPSE_CENTER_X, NIPLELLIPSE_CENTER_Y, NIPLELLIPSE_WIDTH, NIPLELLIPSE_HEIGHT, NIPLELLIPSE_ANGLE);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%s\t%s\t%s\t%s\t%s", NIPLELLIPSE_CENTER_X, NIPLELLIPSE_CENTER_Y, NIPLELLIPSE_WIDTH, NIPLELLIPSE_HEIGHT, NIPLELLIPSE_ANGLE);
#endif           

		return szDesc;
	}

	wstring GetDescForSaving() {
		wchar_t szDesc[256];
		
#ifdef _WINDOWS    
		swprintf_s(szDesc, L"%.1f\t%.1f\t%.1f\t%.1f\t%.1f", 
			m_rcEllipse.center.x, m_rcEllipse.center.y, m_rcEllipse.size.width, m_rcEllipse.size.height, m_rcEllipse.angle);
#endif
#ifdef _LINUX    
		swprintf(szDesc, 256, L"%.1f\t%.1f\t%.1f\t%.1f\t%.1f", 
			m_rcEllipse.center.x, m_rcEllipse.center.y, m_rcEllipse.size.width, m_rcEllipse.size.height, m_rcEllipse.angle);
#endif  

		return szDesc;
	}
};


#define NIPL_RESULT_FIND_BLOB L"FindBlob"
class AFX_EXT_CLASS NIPLResult_FindBlob : public NIPLResult
{
public :
	vector<NIPLBlob> m_listBlob;

	NIPLResult_FindBlob() {
		Init();
	}
	virtual ~NIPLResult_FindBlob() {
		Clear();
	}
	virtual void Clear() {
		m_listBlob.clear();
		Init();
	}

	virtual void Init() {
		m_strType = NIPL_RESULT_FIND_BLOB;
	}

	NIPLResult_FindBlob &operator=(const NIPLResult_FindBlob &dResult) {
		copy(dResult.m_listBlob.begin(), dResult.m_listBlob.end(), m_listBlob.begin());

		return *this;
	}
};

#define NIPL_RESULT_DEFECT L"Defect"
class AFX_EXT_CLASS NIPLResult_Defect : public NIPLResult
{
public:
	vector<NIPLDefect> m_listDefect;
	vector<int> m_listNonDefectSetIds;

	NIPLResult_Defect() {
		Init();
	}
	virtual ~NIPLResult_Defect() {
		Clear();
	}
	virtual void Clear() {
		m_listDefect.clear();
		m_listNonDefectSetIds.clear();

		Init();
	}

	virtual void Init() {
		m_strType = NIPL_RESULT_DEFECT;
	}

	NIPLResult_Defect &operator=(const NIPLResult_Defect &dResult) {
		copy(dResult.m_listDefect.begin(), dResult.m_listDefect.end(), m_listDefect.begin());

		return *this;
	}

	bool FindNonDefectBySetId(int nSetId) {
		auto it = find(m_listNonDefectSetIds.begin(), m_listNonDefectSetIds.end(), nSetId);
		if (it != m_listNonDefectSetIds.end()) {
			return true;
		}

		return false;
	}

	void SetNonDefectBySetId(int nSetId) {
		if (!FindNonDefectBySetId(nSetId)) {
			m_listNonDefectSetIds.push_back(nSetId);
		}

		// remove all defects with the set id
		while (1) {
			auto it = find_if(m_listDefect.begin(), m_listDefect.end(), [nSetId](const NIPLDefect &dDefect) -> bool
			{
				if (dDefect.m_nSetId > 0 && dDefect.m_nSetId == nSetId) {
					return true;
				}

				return false;
			});

			if (it != m_listDefect.end()) {
				m_listDefect.erase(it);
			}
			else {
				break;
			}
		}
	}
};

#define NIPL_RESULT_FIND_LINE L"FindLine"
class AFX_EXT_CLASS NIPLResult_FindLine : public NIPLResult
{
public:
	vector<NIPLLine> m_listLine;

	NIPLResult_FindLine() {
		Init();
	}
	virtual ~NIPLResult_FindLine() {
		Clear();
	}
	virtual void Clear() {
		m_listLine.clear();
		Init();
	}

	virtual void Init() {
		m_strType = NIPL_RESULT_FIND_LINE;
	}

	NIPLResult_FindLine &operator=(const NIPLResult_FindLine &dResult) {
		copy(dResult.m_listLine.begin(), dResult.m_listLine.end(), m_listLine.begin());

		return *this;
	}
};

#define NIPL_RESULT_FIND_CIRCLE L"FindCircle"
class AFX_EXT_CLASS NIPLResult_FindCircle : public NIPLResult
{
public:
	vector<NIPLCircle> m_listCircle;

	NIPLResult_FindCircle() {
		Init();
	}
	virtual ~NIPLResult_FindCircle() {
		Clear();
	}
	virtual void Clear() {
		m_listCircle.clear();
		Init();
	}

	virtual void Init() {
		m_strType = NIPL_RESULT_FIND_CIRCLE;
	}

	NIPLResult_FindCircle &operator=(const NIPLResult_FindCircle &dResult) {
		copy(dResult.m_listCircle.begin(), dResult.m_listCircle.end(), m_listCircle.begin());

		return *this;
	}
};

#define NIPL_RESULT_FIND_ELLIPSE L"FindEllipse"
class AFX_EXT_CLASS NIPLResult_FindEllipse : public NIPLResult
{
public:
	vector<NIPLEllipse> m_listEllipse;

	NIPLResult_FindEllipse() {
		Init();
	}
	virtual ~NIPLResult_FindEllipse() {
		Clear();
	}
	virtual void Clear() {
		m_listEllipse.clear();
		Init();
	}

	virtual void Init() {
		m_strType = NIPL_RESULT_FIND_ELLIPSE;
	}

	NIPLResult_FindEllipse &operator=(const NIPLResult_FindEllipse &dResult) {
		copy(dResult.m_listEllipse.begin(), dResult.m_listEllipse.end(), m_listEllipse.begin());

		return *this;
	}
};
