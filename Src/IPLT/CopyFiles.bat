if %1 == PRE (
..\CopyOrigFiles.bat %3

echo copy header files to header folder...
copy %3.h ..\..\..\Include
copy %3Param.h ..\..\..\Include
copy %3Result.h ..\..\..\Include
) else (
echo copy output files to library and bin folder...
copy %2%3.lib ..\..\..\Lib
copy %2%3%4 ..\..\..\Bin
)
