#pragma once

#include "NIPLCV.h"
#include "NIPLGPUParam.h"
#include "NIPLGPUResult.h"

#ifndef _BUILD_NIPL_GPU
#ifdef _DEBUG
#pragma comment (lib, "NIPLGPU_d.lib")
#else
#pragma comment (lib, "NIPLGPU.lib")
#endif
#endif

#define NIPL_GPU_METHOD_IMPL(FuncName) NIPL_ERR NIPLGPU::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLGPU : public NIPLCV {
public :
	static NIPLGPU *pThis;
	static NIPLGPU *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPLGPU *pThat = 0x00);

	NIPL_METHOD_DECL(Smoothing);
	NIPL_METHOD_DECL(Diff);

	NIPLGPU();
	virtual ~NIPLGPU();
};