#pragma once

#ifdef _DEBUG
#pragma comment (lib, "../Lib/CUDA64/cudart.lib")
#else
#pragma comment (lib, "../Lib/CUDA64/cudart_static.lib")
#endif

#include "opencv2/core/cuda.hpp"