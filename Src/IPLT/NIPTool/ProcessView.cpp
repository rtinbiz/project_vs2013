
#include "stdafx.h"
#include "MainFrm.h"
#include "ProcessView.h"
#include "Resource.h"
#include "NIPTool.h"

/*
class CProcessViewMenuButton : public CMFCToolBarMenuButton
{
	friend class CProcessView;

	DECLARE_SERIAL(CProcessViewMenuButton)

public:
	CProcessViewMenuButton(HMENU hMenu = NULL) : CMFCToolBarMenuButton((UINT)-1, hMenu, -1)
	{
	}

	virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE,
		BOOL bCustomizeMode = FALSE, BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE)
	{
		pImages = CMFCToolBar::GetImages();

		CAfxDrawState ds;
		pImages->PrepareDrawImage(ds);

		CMFCToolBarMenuButton::OnDraw(pDC, rect, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

		pImages->EndDrawImage(ds);
	}
};

IMPLEMENT_SERIAL(CProcessViewMenuButton, CMFCToolBarMenuButton, 1)
*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProcessView::CProcessView()
{
	m_nCurrSort = ID_SORTING_GROUPBYTYPE;
}

CProcessView::~CProcessView()
{
}

BEGIN_MESSAGE_MAP(CProcessView, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	//	ON_COMMAND(ID_CLASS_ADD_MEMBER_FUNCTION, OnClassAddMemberFunction)
	//	ON_COMMAND(ID_CLASS_ADD_MEMBER_VARIABLE, OnClassAddMemberVariable)
	//	ON_COMMAND(ID_CLASS_DEFINITION, OnClassDefinition)
	//	ON_COMMAND(ID_CLASS_PARAMETER, OnClassParameter)
	//	ON_COMMAND(ID_NEW_FOLDER, OnNewFolder)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	//	ON_COMMAND_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnSort)
	//	ON_UPDATE_COMMAND_UI_RANGE(ID_SORTING_GROUPBYTYPE, ID_SORTING_SORTBYACCESS, OnUpdateSort)
	//	ON_NOTIFY(TVN_SELCHANGED, PROCESS_VIEW_TREE_ID, OnSelChanged)
	ON_NOTIFY(NM_CLICK, PROCESS_VIEW_TREE_ID, OnNMClick)
	ON_NOTIFY(NM_DBLCLK, PROCESS_VIEW_TREE_ID, OnNMDblClick)

//	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProcessView message handlers

int CProcessView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Create views:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndProcessViewTree.Create(dwViewStyle, rectDummy, this, PROCESS_VIEW_TREE_ID))
	{
		TRACE0("Failed to create Class View\n");
		return -1;      // fail to create
	}

	// Load images:
/*	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_SORT);
	m_wndToolBar.LoadToolBar(IDR_SORT, 0, 0, TRUE);
*/
	OnChangeVisualStyle();
/*
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));

	m_wndToolBar.SetOwner(this);

	// All commands will be routed via this control , not via the parent frame:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	CMenu menuSort;
	menuSort.LoadMenu(IDR_POPUP_SORT);

	m_wndToolBar.ReplaceButton(ID_SORT_MENU, CProcessViewMenuButton(menuSort.GetSubMenu(0)->GetSafeHmenu()));

	CProcessViewMenuButton* pButton =  DYNAMIC_DOWNCAST(CProcessViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->m_bText = FALSE;
		pButton->m_bImage = TRUE;
		pButton->SetImage(GetCmdMgr()->GetCmdImg(m_nCurrSort));
		pButton->SetMessageWnd(this);
	}
*/

	return 0;
}

void CProcessView::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CProcessView::FillProcessView(NIPJob &dMenu)
{
	for (auto pCategory : dMenu.m_listCategory) {
		HTREEITEM hCategory = InsertCategory(pCategory->m_strName);
		if (hCategory == nullptr) {
			continue;
		}

		for (auto pProcess : pCategory->m_listProcess) {
			HTREEITEM hMethod = InsertProcess(pProcess->m_strName, hCategory, (DWORD_PTR)pProcess);
			if (hMethod == nullptr) {
				continue;
			}
		}

		m_wndProcessViewTree.Expand(hCategory, TVE_EXPAND);
	}
}

HTREEITEM CProcessView::InsertCategory(wstring strName)
{
	HTREEITEM hItem = m_wndProcessViewTree.InsertItem(strName.c_str(), 2, 2, TVI_ROOT);
	if (hItem == nullptr) {
		return nullptr;
	}

	m_wndProcessViewTree.SetItemData(hItem, 0);
	return hItem;
}

HTREEITEM CProcessView::InsertProcess(wstring strName, HTREEITEM hParent, DWORD_PTR pProcess)
{
	HTREEITEM hItem = m_wndProcessViewTree.InsertItem(strName.c_str(), 1, 1, hParent);
	if (hItem == nullptr) {
		return nullptr;
	}

	m_wndProcessViewTree.SetItemData(hItem, pProcess);
	return hItem;

//	string v1 = root["product"].v(0); // Cardigan Sweater
//	string v2 = root["product"]["catalog_item"]["price"].GetContent(); // 39.95
//	string v3 = root.AddVariable("n", "val").Serialize(); // created a new variable in root, n="val"

/*
	HTREEITEM hRoot = m_wndProcessViewTree.InsertItem(_T("FakeApp classes"), 0, 0);
	m_wndProcessViewTree.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);

	HTREEITEM hClass = m_wndProcessViewTree.InsertItem(_T("CFakeAboutDlg"), 1, 1, hRoot);
	m_wndProcessViewTree.InsertItem(_T("CFakeAboutDlg()"), 3, 3, hClass);

	m_wndProcessViewTree.Expand(hRoot, TVE_EXPAND);

	hClass = m_wndProcessViewTree.InsertItem(_T("CFakeApp"), 1, 1, hRoot);
	m_wndProcessViewTree.InsertItem(_T("CFakeApp()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("InitInstance()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("OnAppAbout()"), 3, 3, hClass);

	hClass = m_wndProcessViewTree.InsertItem(_T("CFakeAppDoc"), 1, 1, hRoot);
	m_wndProcessViewTree.InsertItem(_T("CFakeAppDoc()"), 4, 4, hClass);
	m_wndProcessViewTree.InsertItem(_T("~CFakeAppDoc()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("OnNewDocument()"), 3, 3, hClass);

	hClass = m_wndProcessViewTree.InsertItem(_T("CFakeAppView"), 1, 1, hRoot);
	m_wndProcessViewTree.InsertItem(_T("CFakeAppView()"), 4, 4, hClass);
	m_wndProcessViewTree.InsertItem(_T("~CFakeAppView()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("GetDocument()"), 3, 3, hClass);
	m_wndProcessViewTree.Expand(hClass, TVE_EXPAND);

	hClass = m_wndProcessViewTree.InsertItem(_T("CFakeAppFrame"), 1, 1, hRoot);
	m_wndProcessViewTree.InsertItem(_T("CFakeAppFrame()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("~CFakeAppFrame()"), 3, 3, hClass);
	m_wndProcessViewTree.InsertItem(_T("m_wndMenuBar"), 6, 6, hClass);
	m_wndProcessViewTree.InsertItem(_T("m_wndToolBar"), 6, 6, hClass);
	m_wndProcessViewTree.InsertItem(_T("m_wndStatusBar"), 6, 6, hClass);

	hClass = m_wndProcessViewTree.InsertItem(_T("Globals"), 2, 2, hRoot);
	m_wndProcessViewTree.InsertItem(_T("theFakeApp"), 5, 5, hClass);
	m_wndProcessViewTree.Expand(hClass, TVE_EXPAND);
*/
}

void CProcessView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndProcessViewTree;
	ASSERT_VALID(pWndTree);

	if (pWnd != pWndTree)
	{
		CDockablePane::OnContextMenu(pWnd, point);
		return;
	}

	if (point != CPoint(-1, -1))
	{
		// Select clicked item:
		CPoint ptTree = point;
		pWndTree->ScreenToClient(&ptTree);

		UINT flags = 0;
		HTREEITEM hTreeItem = pWndTree->HitTest(ptTree, &flags);
		if (hTreeItem != NULL)
		{
			pWndTree->SelectItem(hTreeItem);
		}
	}

	pWndTree->SetFocus();
/*	CMenu menu;
	menu.LoadMenu(IDR_POPUP_SORT);

	CMenu* pSumMenu = menu.GetSubMenu(0);

	if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
	{
		CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

		if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSumMenu->m_hMenu, FALSE, TRUE))
			return;

		((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
		UpdateDialogControls(this, FALSE);
	}
*/
}

void CProcessView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

//	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;
//	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndProcessViewTree.SetWindowPos(NULL, rectClient.left + 1, rectClient.top + 1, rectClient.Width() - 2, rectClient.Height() - 2, SWP_NOACTIVATE | SWP_NOZORDER);
}

BOOL CProcessView::PreTranslateMessage(MSG* pMsg)
{
	return CDockablePane::PreTranslateMessage(pMsg);
}


/*
void CProcessView::OnSort(UINT id)
{
	if (m_nCurrSort == id)
	{
		return;
	}

	m_nCurrSort = id;

	CProcessViewMenuButton* pButton =  DYNAMIC_DOWNCAST(CProcessViewMenuButton, m_wndToolBar.GetButton(0));

	if (pButton != NULL)
	{
		pButton->SetImage(GetCmdMgr()->GetCmdImg(id));
		m_wndToolBar.Invalidate();
		m_wndToolBar.UpdateWindow();
	}
}

void CProcessView::OnUpdateSort(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(pCmdUI->m_nID == m_nCurrSort);
}

void CProcessView::OnClassAddMemberFunction()
{
	AfxMessageBox(_T("Add member function..."));
}

void CProcessView::OnClassAddMemberVariable()
{
	// TODO: Add your command handler code here
}

void CProcessView::OnClassDefinition()
{
	// TODO: Add your command handler code here
}

void CProcessView::OnClassParameter()
{
	// TODO: Add your command handler code here
}

void CProcessView::OnNewFolder()
{
	AfxMessageBox(_T("New Folder..."));
}
*/

void CProcessView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rectTree;
	m_wndProcessViewTree.GetWindowRect(rectTree);
	ScreenToClient(rectTree);

	rectTree.InflateRect(1, 1);
	dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

void CProcessView::OnSetFocus(CWnd* pOldWnd)
{
//	UpdateParameter();

	m_wndProcessViewTree.SetFocus();

	CDockablePane::OnSetFocus(pOldWnd);
}

void CProcessView::OnChangeVisualStyle()
{
	m_ProcessViewImages.DeleteImageList();

	UINT uiBmpId = theApp.m_bHiColorIcons ? IDB_CLASS_VIEW_24 : IDB_CLASS_VIEW;

	CBitmap bmp;
	if (!bmp.LoadBitmap(uiBmpId))
	{
		TRACE(_T("Can't load bitmap: %x\n"), uiBmpId);
		ASSERT(FALSE);
		return;
	}

	BITMAP bmpObj;
	bmp.GetBitmap(&bmpObj);

	UINT nFlags = ILC_MASK;

	nFlags |= (theApp.m_bHiColorIcons) ? ILC_COLOR24 : ILC_COLOR4;

	m_ProcessViewImages.Create(16, bmpObj.bmHeight, nFlags, 0, 0);
	m_ProcessViewImages.Add(&bmp, RGB(255, 0, 0));

	m_wndProcessViewTree.SetImageList(&m_ProcessViewImages, TVSIL_NORMAL);

/*
	m_wndToolBar.CleanUpLockedImgs();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_SORT_24 : IDR_SORT, 0, 0, TRUE / * Locked * /);
*/
}

/*
void CProcessView::OnSelChanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	UpdateParameter();
}
*/

void CProcessView::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	CPoint pt;
	GetCursorPos(&pt);
	m_wndProcessViewTree.ScreenToClient(&pt);
	HTREEITEM hItem = m_wndProcessViewTree.HitTest(pt);
	if (hItem == NULL) {
		return;
	}

	NIPJobProcess *pProcess = (NIPJobProcess *)m_wndProcessViewTree.GetItemData(hItem);
	UpdateParameter(pProcess);
}

void CProcessView::OnNMDblClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	// Do Nothing... Just preventing dispatching notification to other views

	*pResult = 0;
}

//BOOL CProcessView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
//{
//	// TODO: Add your message handler code here and/or call default
//	return CDockablePane::OnSetCursor(pWnd, nHitTest, message);
//}

void CProcessView::UpdateParameter(NIPJobProcess *pProcess)
{
	CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();
	pMainFrame->UpdateParameterView(pProcess, true);
}
