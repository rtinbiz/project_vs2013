
// NIPToolDoc.h : interface of the CNIPToolDoc class
//


#pragma once


class CNIPToolDoc : public CDocument
{
protected: // create from serialization only
	CNIPToolDoc();
	DECLARE_DYNCREATE(CNIPToolDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName) override;
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName) override;
	virtual void OnCloseDocument() override;

#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CNIPToolDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	NIPJob m_dJob;
	NIPJobProcess *m_pModifyingProcess;
	
public :
	static NIPJob m_dClipboardJob;
	static bool CanPaste() { return (m_dClipboardJob.m_listProcess.size() > 0); }

	NIPJob *GetJob() { return &m_dJob; }
	void SetModifyingProcess(NIPJobProcess *pProcess);
	NIPJobProcess *GetModifyingProcess();

	NIPJobProcess *AddProcess(NIPJobProcess *pProcess, NIPJobProcess *pCurProcess = nullptr);
	NIPJobProcess *ModifyProcess(NIPJobProcess *pProcess);
	bool SwitchProcess(NIPJobProcess *pProcess, NIPJobProcess *pTargetProcess);
	bool DeleteProcess(NIPJobProcess *pProcess);

	bool RunProcess(NIPJobProcess *pProcess);
	bool RunJob();

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
public:
};
