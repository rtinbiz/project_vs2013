
#pragma once

#define PROCESS_VIEW_TREE_ID 2

/////////////////////////////////////////////////////////////////////////////
// CProcessViewTree window

class CProcessViewTree : public CTreeCtrl
{
// Construction
public:
	CProcessViewTree();

// Overrides
protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

// Implementation
public:
	virtual ~CProcessViewTree();

protected:
	DECLARE_MESSAGE_MAP()
public:
};
