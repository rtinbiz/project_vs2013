#pragma once
#include "afxwin.h"


// CCopyModelDlg 대화 상자입니다.

class CCopyModelDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCopyModelDlg)

public:
	CCopyModelDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCopyModelDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_COPY_MODEL_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CString m_strModelId;
	virtual BOOL OnInitDialog();
};
