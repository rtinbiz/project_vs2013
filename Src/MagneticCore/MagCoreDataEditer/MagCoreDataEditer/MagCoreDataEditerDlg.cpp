
// MagCoreDataEditerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MagCoreDataEditer.h"
#include "MagCoreDataEditerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMagCoreDataEditerDlg dialog



CMagCoreDataEditerDlg::CMagCoreDataEditerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMagCoreDataEditerDlg::IDD, pParent)
	, m_strImagePos(_T(""))
	, m_strImageInfo(_T(""))
	, m_strModelId(_T(""))
	, m_nCompIndex(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_strImageFilePath = "";

	m_nDataColor2GrayChannel = 0;
	m_nDataBrightnessEnhancementLevel = 0;
	m_nDataCheckCoreThreshold = 0.f;
	m_nDataCenterPosX = 0;
	m_nDataCenterPosY = 0;
	m_nDataMinRadiusPosX = 0;
	m_nDataMaxRadiusPosX = 0;
	m_nDataMinRadiusRange = 0;
	m_nDataMaxRadiusRange = 0;
	m_nDataMinRadiusMargin = 0;
	m_nDataMaxRadiusMargin = 0;
	m_bDataMinRadiusReverseEdgeDetect = 0;
	m_bDataMaxRadiusReverseEdgeDetect = 0;
	m_bDataMinRadiusBinarize = 0;
	m_bDataMaxRadiusBinarize = 0;
	m_bDataMinRadiusBlack = 0;
	m_bDataMaxRadiusBlack = 0;
}

void CMagCoreDataEditerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Text(pDX, IDC_IMAGE_POS, m_strImagePos);
	DDX_Text(pDX, IDC_IMAGE_INFO, m_strImageInfo);
	DDX_Control(pDX, IDC_MODEL_LIST, m_ctlModelList);
	DDX_Text(pDX, IDC_MODEL_ID, m_strModelId);
	DDX_Radio(pDX, IDC_COMP_INNER, m_nCompIndex);
	DDX_Text(pDX, IDC_DATA_CENTER_POS_X, m_nDataCenterPosX);
	DDX_Text(pDX, IDC_DATA_CENTER_POS_Y, m_nDataCenterPosY);
	DDX_Text(pDX, IDC_DATA_MIN_RADIUS_POS_X, m_nDataMinRadiusPosX);
	DDX_Text(pDX, IDC_DATA_MAX_RADIUS_POS_X, m_nDataMaxRadiusPosX);
	DDX_Text(pDX, IDC_DATA_MIN_RADIUS_RANGE, m_nDataMinRadiusRange);
	DDX_Text(pDX, IDC_DATA_MAX_RADIUS_RANGE, m_nDataMaxRadiusRange);
	DDX_Text(pDX, IDC_DATA_MIN_RADIUS_MARGIN, m_nDataMinRadiusMargin);
	DDX_Text(pDX, IDC_DATA_MAX_RADIUS_MARGIN, m_nDataMaxRadiusMargin);
	DDX_Check(pDX, IDC_DATA_MIN_RADIUS_REVERSE_EDGE_DETECT, m_bDataMinRadiusReverseEdgeDetect);
	DDX_Check(pDX, IDC_DATA_MAX_RADIUS_REVERSE_EDGE_DETECT, m_bDataMaxRadiusReverseEdgeDetect);
	DDX_Check(pDX, IDC_DATA_MIN_RADIUS_BINARIZE, m_bDataMinRadiusBinarize);
	DDX_Check(pDX, IDC_DATA_MAX_RADIUS_BINARIZE, m_bDataMaxRadiusBinarize);
	DDX_Check(pDX, IDC_DATA_MIN_RADIUS_BLACK, m_bDataMinRadiusBlack);
	DDX_Check(pDX, IDC_DATA_MAX_RADIUS_BLACK, m_bDataMaxRadiusBlack);
	DDX_Control(pDX, IDC_COMBO_COLOR_TO_GRAY_CHANNEL, m_ctlColor2GrayChannel);
	DDX_CBIndex(pDX, IDC_COMBO_COLOR_TO_GRAY_CHANNEL, m_nDataColor2GrayChannel);
	DDX_Text(pDX, IDC_DATA_BRIGHTNESS_ENHANCEMENT_LEVEL, m_nDataBrightnessEnhancementLevel);
	DDX_Text(pDX, IDC_DATA_CHECK_CORE_THRESHOLD, m_nDataCheckCoreThreshold);
}

BEGIN_MESSAGE_MAP(CMagCoreDataEditerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(IDC_OPEN_IMAGE, &CMagCoreDataEditerDlg::OnBnClickedOpenImage)
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CMagCoreDataEditerDlg::OnBnClickedLoadImage)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CMagCoreDataEditerDlg::OnImagePosValue)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_DBLCLICK, &CMagCoreDataEditerDlg::OnImagePosDblClick)

	ON_LBN_SELCHANGE(IDC_MODEL_LIST, &CMagCoreDataEditerDlg::OnSelchangeModelList)
	ON_COMMAND_RANGE(IDC_COMP_INNER, IDC_COMP_OUTER, OnClickedCompRadio)
	ON_BN_CLICKED(IDC_SAVE_DATA, &CMagCoreDataEditerDlg::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_SHOW_ORIGINAL_IMAGE, &CMagCoreDataEditerDlg::OnBnClickedShowOriginalImage)
	ON_BN_CLICKED(IDC_SHOW_CALIBRATION_IMAGE, &CMagCoreDataEditerDlg::OnBnClickedShowCalibrationImage)
	ON_BN_CLICKED(IDC_SHOW_CALIBRATION_IMAGE_WITH_MARGIN, &CMagCoreDataEditerDlg::OnBnClickedShowCalibrationImageWithMargin)
	ON_BN_CLICKED(IDC_DATA_APPLY, &CMagCoreDataEditerDlg::OnBnClickedDataApply)
	ON_BN_CLICKED(IDC_RELOAD_MODEL_DATA, &CMagCoreDataEditerDlg::OnBnClickedReloadModelData)
	ON_BN_CLICKED(IDC_SAVE_IMAGE_AS_SAMPLE, &CMagCoreDataEditerDlg::OnBnClickedSaveImageAsSample)
	ON_WM_DROPFILES()
	ON_BN_CLICKED(IDC_EDIT_CHAR_TEMPLATE, &CMagCoreDataEditerDlg::OnBnClickedEditCharTemplate)
	ON_BN_CLICKED(IDC_COPY_MODEL_DATA, &CMagCoreDataEditerDlg::OnBnClickedCopyModelData)
	ON_BN_CLICKED(IDC_DELETE_MODEL_DATA, &CMagCoreDataEditerDlg::OnBnClickedDeleteModelData)
	ON_BN_CLICKED(IDC_EDIT_INSPECTION_DATA, &CMagCoreDataEditerDlg::OnBnClickedEditInspectionData)
	ON_BN_CLICKED(IDC_COVERT_COLOR, &CMagCoreDataEditerDlg::OnBnClickedCovertColor)
	ON_BN_CLICKED(IDC_SHOW_CALIBRATION_RESULT_IMAGE, &CMagCoreDataEditerDlg::OnBnClickedShowCalibrationResultImage)
END_MESSAGE_MAP()


// CMagCoreDataEditerDlg message handlers

BOOL CMagCoreDataEditerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	DragAcceptFiles(TRUE);

	// Initialize Color to Gray Channel List
	m_ctlColor2GrayChannel.AddString(L"CHANNEL_ALL");
	m_ctlColor2GrayChannel.AddString(L"CHANNEL_R");
	m_ctlColor2GrayChannel.AddString(L"CHANNEL_G");
	m_ctlColor2GrayChannel.AddString(L"CHANNEL_B");

	// TODO: Add extra initialization here
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);

	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}

	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	LoadModelList();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMagCoreDataEditerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMagCoreDataEditerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMagCoreDataEditerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMagCoreDataEditerDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialogEx::OnSetFocus(pOldWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	m_wndImg.SetFocus();
}

void CMagCoreDataEditerDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
}

void CMagCoreDataEditerDlg::OnBnClickedOpenImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"jpg", LPCTSTR(m_strImageFilePath), OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	m_strImageFilePath = iFileDlg.GetPathName();
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImageFilePath);

	OnBnClickedLoadImage();
}

void CMagCoreDataEditerDlg::OnBnClickedLoadImage()
{
	ClearPosBox();

	GetDlgItem(IDC_IMAGE_PATH)->GetWindowText(m_strImageFilePath);

	int nLength = m_strImageFilePath.GetLength();
	int nIndex = m_strImageFilePath.ReverseFind('\\');
	if (nIndex > 0) {
		CString strFileName = m_strImageFilePath.Right(nLength - nIndex - 1);
		if (strFileName.MakeLower().Find(L".jpg") > 0 || strFileName.MakeLower().Find(L".bmp") > 0) {
			m_strImageFolderPath = m_strImageFilePath.Left(nIndex + 1);

			LoadImage();
			ShowPosBox();
		}
		else {
			m_strImageFolderPath = m_strImageFilePath;
			if (m_strImageFolderPath[nLength - 1] != '\\') {
				m_strImageFolderPath += "\\";
			}

			LoadCompImage();
		}
	}
}

void CMagCoreDataEditerDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImageFilePath), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImageFilePath));
		return;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImageFilePath));
	}

	m_dImg = dImg;

	ShowImage();
}

void CMagCoreDataEditerDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

LRESULT CMagCoreDataEditerDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));
	SetControlDataValue(CPoint(nPosX, nPosY));

	return 0;
}

void CMagCoreDataEditerDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	m_strImagePos = szText;
	UpdateData(FALSE);
}

void CMagCoreDataEditerDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	m_strImageInfo = szText;
	UpdateData(FALSE);
}

void CMagCoreDataEditerDlg::LoadModelList()
{
	m_ctlModelList.ResetContent();

	WIN32_FIND_DATA dFindData;
	wstring strFormat(VISION_FOLDER);
	strFormat += L"\\*.vid";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	int nFileCount = 0;
	if (hFile != INVALID_HANDLE_VALUE) {
		bool bFirstFile = true;
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			wchar_t *pCut = wcsrchr(dFindData.cFileName, '.');
			if (pCut) {
				*pCut = 0X00;
			}

			int nIndex = m_ctlModelList.GetCount();
			nIndex = m_ctlModelList.InsertString(nIndex, dFindData.cFileName);
			m_ctlModelList.SetCurSel(nIndex);
		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);
	}

	int nCount = m_ctlModelList.GetCount();
	if (nCount > 0) {
		AddLogText(L"Success to check Model Data Files, Count : %d", nCount);
		m_ctlModelList.SetCurSel(0);
		ChangeModel();
	}
	else {
		AddLogText(L"Fail to check Model Data Files, Count : %d", nCount);
	}
}


void CMagCoreDataEditerDlg::OnSelchangeModelList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeModel();
}

void CMagCoreDataEditerDlg::ChangeModel()
{
	int nIndex = m_ctlModelList.GetCurSel();
	if (nIndex < 0) {
		AddLogText(L"Fail to change model. No model selected.");
		return;
	}

	m_ctlModelList.GetText(nIndex, m_strModel);

	UpdateData(FALSE);
	LoadVIData();
	LoadNIPJob();

	m_strImageFolderPath = VISION_MODEL_SAMPLE_FOLDER;
	m_strImageFolderPath += m_strModel;
	m_strImageFolderPath += "\\";
	ChangeCompData();
}

bool CMagCoreDataEditerDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_NIP_JOB_FILE;
	
	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		AddLogText(L"Fail to load Common Data File : %s", strJobFile.c_str());
		return false;
	}
	else {
		AddLogText(L"Success to load Common Data File : %s", strJobFile.c_str());
	}

	return true;
}

void CMagCoreDataEditerDlg::DoImageCalibration(wstring strShowImage)
{
	AddLogText(L"Do Image Calibration");

	NIPO *pNIPO = NIPO::GetInstance();

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	// Calibration
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = GetCompName();
		pParam->m_strShowImage = strShowImage;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = m_dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			m_dImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				if (!CHECK_STRING(strShowImage, STR_SHOW_IMAGE_OUTPUT)) {
					NIPLResult_FindEllipse *pResult = (NIPLResult_FindEllipse *)dOutput.m_pResult;
					NIPLEllipse dMinCircle = pResult->m_listEllipse[0];
					NIPLEllipse dMaxCircle = pResult->m_listEllipse[1];

					m_wndImg.ClearImageMark();

					ImageMark dMark;
					dMark.m_bCircle = true;
					dMark.m_rcBoundingBox = dMinCircle.GetBoundingBox();
					m_wndImg.AddImageMark(dMark);

					dMark.Clear();
					dMark.m_bCircle = true;
					dMark.m_rcBoundingBox = dMaxCircle.GetBoundingBox();
					m_wndImg.AddImageMark(dMark);

					m_wndImg.ShowImageMark();
				}

				delete dOutput.m_pResult;
			}

			ShowImage();
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Calibration. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
		return;
	}

	AddLogText(L"Success to do Image Calibration");
}

void CMagCoreDataEditerDlg::OnClickedCompRadio(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCompData();
}

void CMagCoreDataEditerDlg::LoadVIData()
{
	m_strVIDataPath = VISION_FOLDER;
	m_strVIDataPath += m_strModel;
	m_strVIDataPath += L".vid";

	if (!m_dVI.LoadData(wstring(m_strVIDataPath))) {
		AddLogText(L"Fail to load Model Data : %s", LPCTSTR(m_strVIDataPath));
		return;
	}
	else {
		AddLogText(L"Success to load Model Data : %s", LPCTSTR(m_strVIDataPath));
	}

	m_strModelId = m_dVI.m_dModel.m_strId.c_str();

	UpdateData(FALSE);
}

void CMagCoreDataEditerDlg::ChangeCompData()
{
	UpdateData();
	UpdateCompData();
	LoadCompImage();
}

void CMagCoreDataEditerDlg::LoadCompImage()
{
	CString strComp = GetCompName();

	m_strImageFilePath = m_strImageFolderPath;
	m_strImageFilePath += strComp;
	m_strImageFilePath += ".jpg";
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImageFilePath);

	LoadImage();

	ShowPosBox();
}

void CMagCoreDataEditerDlg::ShowPosBox()
{
	UpdateData();

	m_wndImg.ClearImageMark();

	int nMarkSize = 5;		// half size
	int nLineLenth = 20;	// half length
	int nLineWidth = 2;

	ImageMark dMark;
	// Center Pos
	dMark.m_bCircle = true;
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataCenterPosX - nMarkSize, m_nDataCenterPosY - nMarkSize, nMarkSize * 2 + 1, nMarkSize * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	// Min Radius Pos X
	dMark.Clear();
	dMark.m_bCircle = true;
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMinRadiusPosX - nMarkSize, m_nDataCenterPosY - nMarkSize, nMarkSize * 2 + 1, nMarkSize * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	// Max Radius Pos X
	dMark.Clear();
	dMark.m_bCircle = true;
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMaxRadiusPosX - nMarkSize, m_nDataCenterPosY - nMarkSize, nMarkSize * 2 + 1, nMarkSize * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	// Min Radius Range
	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMinRadiusPosX - m_nDataMinRadiusRange, m_nDataCenterPosY - nLineLenth, nLineWidth, nLineLenth * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMinRadiusPosX + m_nDataMinRadiusRange, m_nDataCenterPosY - nLineLenth, nLineWidth, nLineLenth * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMinRadiusPosX - m_nDataMinRadiusRange, m_nDataCenterPosY, m_nDataMinRadiusRange * 2 + 1, nLineWidth);
	m_wndImg.AddImageMark(dMark);

	// Max Radius Range
	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMaxRadiusPosX - m_nDataMaxRadiusRange, m_nDataCenterPosY - nLineLenth, nLineWidth, nLineLenth * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMaxRadiusPosX + m_nDataMaxRadiusRange, m_nDataCenterPosY - nLineLenth, nLineWidth, nLineLenth * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMaxRadiusPosX - m_nDataMaxRadiusRange, m_nDataCenterPosY, m_nDataMaxRadiusRange * 2 + 1, nLineWidth);
	m_wndImg.AddImageMark(dMark);

	// Min Radius Margin
	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMinRadiusPosX + m_nDataMinRadiusMargin - nMarkSize, m_nDataCenterPosY - nMarkSize, nMarkSize * 2 + 1, nMarkSize * 2 + 1);
	m_wndImg.AddImageMark(dMark);

	// Max Radius Margin
	dMark.Clear();
	dMark.m_bFill = true;
	dMark.m_rcBoundingBox = Rect(m_nDataMaxRadiusPosX - m_nDataMaxRadiusMargin - nMarkSize, m_nDataCenterPosY - nMarkSize, nMarkSize * 2 + 1, nMarkSize * 2 + 1);
	m_wndImg.AddImageMark(dMark);


	m_wndImg.ShowImageMark();
}

void CMagCoreDataEditerDlg::ClearPosBox()
{
	m_wndImg.ClearImageMark();
}

void CMagCoreDataEditerDlg::UpdateCompData()
{
	VIData_Comp *pCompData = GetVIData(m_nCompIndex);
	if (pCompData == nullptr) {
		return;
	}

	VIData_Calibration &dCal = pCompData->m_dCal;
	m_nDataColor2GrayChannel = dCal.m_nColor2GrayChannel;
	m_nDataBrightnessEnhancementLevel = dCal.m_nBrightnessEnhancementLevel;
	m_nDataCheckCoreThreshold = dCal.m_nCheckCoreThreshold;;
	m_nDataCenterPosX = dCal.m_nCenterPosX;
	m_nDataCenterPosY = dCal.m_nCenterPosY;
	m_nDataMinRadiusPosX = dCal.m_nMinRadiusPosX;
	m_nDataMaxRadiusPosX = dCal.m_nMaxRadiusPosX;
	m_nDataMinRadiusRange= dCal.m_nMinRadiusRange;
	m_nDataMaxRadiusRange = dCal.m_nMaxRadiusRange;
	m_nDataMinRadiusMargin = dCal.m_nMinRadiusMargin;
	m_nDataMaxRadiusMargin = dCal.m_nMaxRadiusMargin;
	m_bDataMinRadiusReverseEdgeDetect = dCal.m_bMinRadiusReverseEdgeDetect;
	m_bDataMaxRadiusReverseEdgeDetect = dCal.m_bMaxRadiusReverseEdgeDetect;
	m_bDataMinRadiusBinarize = dCal.m_bMinRadiusBinarize;
	m_bDataMaxRadiusBinarize = dCal.m_bMaxRadiusBinarize;
	m_bDataMinRadiusBlack = dCal.m_bMinRadiusBlack;
	m_bDataMaxRadiusBlack = dCal.m_bMaxRadiusBlack;

	UpdateData(FALSE);
}

void CMagCoreDataEditerDlg::ApplyCompData()
{
	UpdateData();

	VIData_Comp *pCompData = GetVIData(m_nCompIndex);
	if (pCompData == nullptr) {
		return;
	}

	VIData_Calibration &dCal = pCompData->m_dCal;
	dCal.m_nColor2GrayChannel = m_nDataColor2GrayChannel;
	dCal.m_nBrightnessEnhancementLevel = m_nDataBrightnessEnhancementLevel;
	dCal.m_nCheckCoreThreshold = m_nDataCheckCoreThreshold;
	dCal.m_nCenterPosX = m_nDataCenterPosX;
	dCal.m_nCenterPosY = m_nDataCenterPosY;
	dCal.m_nMinRadiusPosX = m_nDataMinRadiusPosX;
	dCal.m_nMaxRadiusPosX = m_nDataMaxRadiusPosX;
	dCal.m_nMinRadiusRange= m_nDataMinRadiusRange;
	dCal.m_nMaxRadiusRange = m_nDataMaxRadiusRange;
	dCal.m_nMinRadiusMargin = m_nDataMinRadiusMargin;
	dCal.m_nMaxRadiusMargin = m_nDataMaxRadiusMargin;
	dCal.m_bMinRadiusReverseEdgeDetect = m_bDataMinRadiusReverseEdgeDetect;
	dCal.m_bMaxRadiusReverseEdgeDetect = m_bDataMaxRadiusReverseEdgeDetect;
	dCal.m_bMinRadiusBinarize = m_bDataMinRadiusBinarize;
	dCal.m_bMaxRadiusBinarize = m_bDataMaxRadiusBinarize;
	dCal.m_bMinRadiusBlack = m_bDataMinRadiusBlack;
	dCal.m_bMaxRadiusBlack = m_bDataMaxRadiusBlack;

	ShowPosBox();
}

void CMagCoreDataEditerDlg::OnBnClickedSaveData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
	SaveVIData();
}

void CMagCoreDataEditerDlg::SaveVIData()
{
	UpdateData();

	m_dVI.m_dModel.m_strId = m_strModelId;

	if (!m_dVI.SaveData(wstring(m_strVIDataPath))) {
		AddLogText(L"Fail to save Model Data : %s", LPCTSTR(m_strVIDataPath));
		return;
	}
	else {
		AddLogText(L"Success to save Model Data : %s", LPCTSTR(m_strVIDataPath));
	}
}

LRESULT CMagCoreDataEditerDlg::OnImagePosDblClick(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
//	SetControlDataValue(CPoint(nPosX, nPosY), true);

	return 0;
}

VIData_Comp *CMagCoreDataEditerDlg::GetVIData(int nCompIndex)
{
	VIData_Comp *pCompData = nullptr;
	switch (nCompIndex) {
	case COMP_INNER: pCompData = &m_dVI.m_dInner; break;
	case COMP_LOWER: pCompData = &m_dVI.m_dLower; break;
	case COMP_UPPER: pCompData = &m_dVI.m_dUpper; break;
	case COMP_OUTER: pCompData = &m_dVI.m_dOuter; break;
	}

	return pCompData;
}

void CMagCoreDataEditerDlg::OnBnClickedDataApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
}

void CMagCoreDataEditerDlg::SetControlDataValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	CWnd *pWnd = GetFocusControl();
	if (pWnd == nullptr) {
		return;
	}

	wchar_t szText[256];
	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_DATA_CENTER_POS_X:
	case IDC_DATA_CENTER_POS_Y:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataCenterPosX)) {
			return;
		}

		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_CENTER_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_DATA_CENTER_POS_Y)->SetWindowText(szText);
		swprintf_s(szText, L"%d", m_nDataMinRadiusPosX + ptImagePos.x - m_nDataCenterPosX);
		GetDlgItem(IDC_DATA_MIN_RADIUS_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", m_nDataMaxRadiusPosX + ptImagePos.x - m_nDataCenterPosX);
		GetDlgItem(IDC_DATA_MAX_RADIUS_POS_X)->SetWindowText(szText);
		break;

	case IDC_DATA_MIN_RADIUS_POS_X:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMinRadiusPosX)) {
			return;
		}

		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_MIN_RADIUS_POS_X)->SetWindowText(szText);
		break;
	case IDC_DATA_MAX_RADIUS_POS_X:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMaxRadiusPosX)) {
			return;
		}

		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_MAX_RADIUS_POS_X)->SetWindowText(szText);
		break;
	case IDC_DATA_MIN_RADIUS_RANGE:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMinRadiusPosX - m_nDataMinRadiusRange)
			&& !CanChangeControlDataValue(ptImagePos, m_nDataMinRadiusPosX + m_nDataMinRadiusRange)) {
			return;
		}

		swprintf_s(szText, L"%d", abs(ptImagePos.x - m_nDataMinRadiusPosX));
		GetDlgItem(IDC_DATA_MIN_RADIUS_RANGE)->SetWindowText(szText);
		break;
	case IDC_DATA_MAX_RADIUS_RANGE:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMaxRadiusPosX - m_nDataMaxRadiusRange)
			&& !CanChangeControlDataValue(ptImagePos, m_nDataMaxRadiusPosX + m_nDataMaxRadiusRange)) {
			return;
		}

		swprintf_s(szText, L"%d", abs(ptImagePos.x - m_nDataMaxRadiusPosX));
		GetDlgItem(IDC_DATA_MAX_RADIUS_RANGE)->SetWindowText(szText);
		break;
	case IDC_DATA_MIN_RADIUS_MARGIN:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMinRadiusPosX + m_nDataMinRadiusMargin)) {
			return;
		}

		swprintf_s(szText, L"%d", ptImagePos.x - m_nDataMinRadiusPosX);
		GetDlgItem(IDC_DATA_MIN_RADIUS_MARGIN)->SetWindowText(szText);
		break;
	case IDC_DATA_MAX_RADIUS_MARGIN:
		if (!CanChangeControlDataValue(ptImagePos, m_nDataMaxRadiusPosX - m_nDataMaxRadiusMargin)) {
			return;
		}

		swprintf_s(szText, L"%d", -(ptImagePos.x - m_nDataMaxRadiusPosX));
		GetDlgItem(IDC_DATA_MAX_RADIUS_MARGIN)->SetWindowText(szText);
		break;
	}

	ApplyCompData();
}

CWnd *CMagCoreDataEditerDlg::GetFocusControl()
{
	CWnd *pWnd = GetFocus();
	if (pWnd == nullptr) {
		return nullptr;
	}

	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_DATA_CENTER_POS_X:
	case IDC_DATA_CENTER_POS_Y:
	case IDC_DATA_MIN_RADIUS_POS_X:
	case IDC_DATA_MAX_RADIUS_POS_X:
	case IDC_DATA_MIN_RADIUS_RANGE:
	case IDC_DATA_MAX_RADIUS_RANGE:
	case IDC_DATA_MIN_RADIUS_MARGIN:
	case IDC_DATA_MAX_RADIUS_MARGIN:
		return pWnd;
	}

	return nullptr;
}

bool CMagCoreDataEditerDlg::CanChangeControlDataValue(CPoint ptImagePos, int nTargetPosX)
{
	int nGapX = ptImagePos.x - nTargetPosX;
	int nGapY = ptImagePos.y - m_nDataCenterPosY;
	float nDist = (float)sqrt(pow(nGapX, 2) + pow(nGapY, 2));
	if (nDist > 100.f) {
		return false;
	}

	return true;
}

CString CMagCoreDataEditerDlg::GetCompName()
{
	CString strComp = L"";
	switch (m_nCompIndex) {
	case COMP_INNER: strComp = STR_MAGCORE_COMP_INNER; break;
	case COMP_LOWER: strComp = STR_MAGCORE_COMP_LOWER; break;
	case COMP_UPPER: strComp = STR_MAGCORE_COMP_UPPER; break;
	case COMP_OUTER: strComp = STR_MAGCORE_COMP_OUTER; break;
	}

	return strComp;
}

void CMagCoreDataEditerDlg::OnBnClickedShowOriginalImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadImage();
	ShowPosBox();
}

void CMagCoreDataEditerDlg::OnBnClickedShowCalibrationImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
	SaveVIData();

	LoadImage();
	ClearPosBox();

	DoImageCalibration(STR_SHOW_IMAGE_CIRCLE_OUTPUT);
}



void CMagCoreDataEditerDlg::OnBnClickedShowCalibrationResultImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
	SaveVIData();

	LoadImage();
	ClearPosBox();

	DoImageCalibration(STR_SHOW_IMAGE_OUTPUT);
}


void CMagCoreDataEditerDlg::OnBnClickedShowCalibrationImageWithMargin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
	SaveVIData();

	LoadImage();
	ClearPosBox();

	DoImageCalibration(STR_SHOW_IMAGE_CIRCLE_MARGIN_OUTPUT);
}

void CMagCoreDataEditerDlg::OnBnClickedReloadModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadModelList();
}

void CMagCoreDataEditerDlg::OnBnClickedCovertColor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
	SaveVIData();

	LoadImage();
	ShowPosBox();

	DoImageCalibration(STR_SHOW_IMAGE_COLOR_CONVERT);
}

void CMagCoreDataEditerDlg::OnBnClickedSaveImageAsSample()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadImage();

	CString strComp = GetCompName();

	wstring strImagePath = VISION_MODEL_SAMPLE_FOLDER;
	strImagePath += m_strModel + L"\\";
	strImagePath += strComp;
	strImagePath += L".jpg";

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->SaveImage(strImagePath, m_dImg)) {
		AddLogText(L"Success to save Image as Sample : %s", strImagePath.c_str());
	}
	else {
		AddLogText(L"Fail to save Image as Sample : %s", strImagePath.c_str());
	}
}


void CMagCoreDataEditerDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int nCount = DragQueryFile(hDropInfo, 0xffffffff, nullptr, 0);
	if (nCount > 1) {
		AddLogText(L"Cannot load multiple files.");
		return;
	}

	wchar_t szPath[1024];
	DragQueryFile(hDropInfo, 0, szPath, 1023);

	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(szPath);
	OnBnClickedLoadImage();
	
	CDialogEx::OnDropFiles(hDropInfo);
}

void CMagCoreDataEditerDlg::OnBnClickedEditInspectionData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// save VI data first
	ApplyCompData();
	SaveVIData();

	ShowWindow(SW_HIDE);

	CInspectionDlg iDlg;

	iDlg.m_strModelId = m_strModel;
	iDlg.m_nCompIndex = m_nCompIndex;

	iDlg.m_strImageFolderPath = m_strImageFolderPath;
	iDlg.m_strImageFilePath = m_strImageFilePath;
	
	iDlg.m_strVIDataPath = m_strVIDataPath;

	// to keep show image option for debugging
	static wstring strShowImage = L"Output";
	iDlg.m_strShowImage = strShowImage;

	iDlg.DoModal();

	strShowImage = iDlg.m_strShowImage;

	// load VI data again
	LoadVIData();

	m_nCompIndex = iDlg.m_nCompIndex;
	UpdateData(FALSE);
	ChangeCompData();

	// load image file being edited in inspection dialog
	m_strImageFilePath = iDlg.m_strImageFilePath;
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImageFilePath);
	OnBnClickedLoadImage();

/*
	CRect rc;
	iDlg.GetWindowPos(rc);
	SetWindowPos(NULL, rc.left, rc.top, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
*/

	ShowWindow(SW_SHOW);
}


void CMagCoreDataEditerDlg::OnBnClickedEditCharTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	VIData_Comp *pCompData = GetVIData(COMP_OUTER);
	if (pCompData == nullptr) {
		return;
	}

	VIData_Inspection &dIns = pCompData->m_dIns;
	if (CHECK_STRING(dIns.m_strCharCheck, L"false")) {
		AddLogText(L"Cannot edit Character Template. Turn on 'Char Check' first in Outer Inspection options");
		return;
	}

	ShowWindow(SW_HIDE);

	CCharTemplateDlg iDlg;

	iDlg.m_strModelId = m_strModel;

	iDlg.m_strImagePath = m_strImageFolderPath;
	iDlg.m_strImagePath += L"Outer.jpg";

	iDlg.m_strTemplatePath = VISION_MODEL_SAMPLE_FOLDER;
	iDlg.m_strTemplatePath += m_strModel + L"\\";
	iDlg.m_strTemplatePath += VISION_CHAR_TEMPLATE_FILE;

	iDlg.m_strVIDataPath = m_strVIDataPath;

	iDlg.DoModal();

	ShowWindow(SW_SHOW);
}

bool CMagCoreDataEditerDlg::FindModel(CString strModel)
{
	CString strText;
	int nCount = m_ctlModelList.GetCount();
	for (int i = 0; i < nCount; i++) {
		m_ctlModelList.GetText(i, strText);
		if (strText == strModel) {
			return true;
		}
	}

	return false;
}

void CMagCoreDataEditerDlg::OnBnClickedCopyModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CCopyModelDlg iDlg;
	iDlg.m_strModelId = m_strModel;

	INT_PTR nRet = iDlg.DoModal();
	if (nRet != IDOK) {
		return;
	}

	CString strNewModelId = iDlg.m_strModelId;
	if (FindModel(strNewModelId)) {
		AddLogText(L"Cannot copy model. The same model Id already exists.");
		return;
	}

	// Copy VI File
	CString strNewVIDataPath = VISION_FOLDER;
	strNewVIDataPath += LPCTSTR(strNewModelId);
	strNewVIDataPath += L".vid";
	if (CopyFile(LPCTSTR(m_strVIDataPath), LPCTSTR(strNewVIDataPath), FALSE)) {
		AddLogText(L"Success to copy VI data file : %s", LPCTSTR(strNewVIDataPath));
	}
	else {
		AddLogText(L"Fail to copy VI data file : %s", LPCTSTR(strNewVIDataPath));
	}

	// Copy Sample Images
	CString strImageFolderPath = VISION_MODEL_SAMPLE_FOLDER;
	strImageFolderPath += m_strModel;
	strImageFolderPath += "\\";

	CString strNewImageFolderPath = VISION_MODEL_SAMPLE_FOLDER;
	strNewImageFolderPath += strNewModelId;
	strNewImageFolderPath += "\\";

	if (CreateDirectory(strNewImageFolderPath, NULL)) {
		AddLogText(L"Success to create sample data folder : %s", LPCTSTR(strNewImageFolderPath));
	}
	else {
		AddLogText(L"Fail to create sample data folder : %s", LPCTSTR(strNewImageFolderPath));
	}


	CString strFileName = STR_MAGCORE_COMP_INNER;
	strFileName += L".jpg";
	if (CopyFile(LPCTSTR(strImageFolderPath + strFileName), LPCTSTR(strNewImageFolderPath + strFileName), FALSE)) {
		AddLogText(L"Success to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_LOWER;
	strFileName += L".jpg";
	if (CopyFile(LPCTSTR(strImageFolderPath + strFileName), LPCTSTR(strNewImageFolderPath + strFileName), FALSE)) {
	AddLogText(L"Success to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_UPPER;
	strFileName += L".jpg";
	if (CopyFile(LPCTSTR(strImageFolderPath + strFileName), LPCTSTR(strNewImageFolderPath + strFileName), FALSE)) {
		AddLogText(L"Success to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_OUTER;
	strFileName += L".jpg";
	if (CopyFile(LPCTSTR(strImageFolderPath + strFileName), LPCTSTR(strNewImageFolderPath + strFileName), FALSE)) {
		AddLogText(L"Success to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to copy sample image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	strFileName = VISION_CHAR_TEMPLATE_FILE;
	if (CopyFile(LPCTSTR(strImageFolderPath + strFileName), LPCTSTR(strNewImageFolderPath + strFileName), FALSE)) {
		AddLogText(L"Success to copy template image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to copy template image : %s", LPCTSTR(strNewImageFolderPath + strFileName));
	}

	int nIndex = m_ctlModelList.GetCount();
	nIndex = m_ctlModelList.InsertString(nIndex, strNewModelId);

	AddLogText(L"Success to copy model : %s", LPCTSTR(strNewModelId));

	m_ctlModelList.SetCurSel(nIndex);
	ChangeModel();

	// Set 
	m_strModelId = strNewModelId;
	UpdateData(FALSE);
	SaveVIData();
}

void CMagCoreDataEditerDlg::OnBnClickedDeleteModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strMessage = L"Are you sure to delete the model \'" + m_strModel + L"\' ?\nWarning : All data will be deleted and cannot be recovered.";
	int nRet = AfxMessageBox(strMessage, MB_OKCANCEL);
	if (nRet != IDOK) {
		return;
	}

	// Delete VI File
	if(DeleteFile(LPCTSTR(m_strVIDataPath))) {
		AddLogText(L"Success to delete VI data file : %s", LPCTSTR(m_strVIDataPath));
	}
	else {
		AddLogText(L"Fail to delete VI data file : %s", LPCTSTR(m_strVIDataPath));
	}

	// Delete Sample Images
	CString strImageFolderPath = VISION_MODEL_SAMPLE_FOLDER;
	strImageFolderPath += m_strModel;
	strImageFolderPath += "\\";

	CString strFileName = STR_MAGCORE_COMP_INNER;
	strFileName += L".jpg";
	if (DeleteFile(LPCTSTR(strImageFolderPath + strFileName))) {
		AddLogText(L"Success to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_LOWER;
	strFileName += L".jpg";
	if (DeleteFile(LPCTSTR(strImageFolderPath + strFileName))) {
		AddLogText(L"Success to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_UPPER;
	strFileName += L".jpg";
	if (DeleteFile(LPCTSTR(strImageFolderPath + strFileName))) {
		AddLogText(L"Success to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	strFileName = STR_MAGCORE_COMP_OUTER;
	strFileName += L".jpg";
	if (DeleteFile(LPCTSTR(strImageFolderPath + strFileName))) {
		AddLogText(L"Success to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to delete sample image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	strFileName = VISION_CHAR_TEMPLATE_FILE;
	if (DeleteFile(LPCTSTR(strImageFolderPath + strFileName))) {
		AddLogText(L"Success to delete template image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}
	else {
		AddLogText(L"Fail to delete template image : %s", LPCTSTR(strImageFolderPath + strFileName));
	}

	if (RemoveDirectory(strImageFolderPath)) {
		AddLogText(L"Success to delete sample data folder : %s", LPCTSTR(strImageFolderPath));
	}
	else {
		AddLogText(L"Fail to delete sample data folder : %s", LPCTSTR(strImageFolderPath));
	}

	int nIndex = m_ctlModelList.GetCurSel();
	m_ctlModelList.DeleteString(nIndex);

	AddLogText(L"Success to delete model : %s", LPCTSTR(m_strModel));

	if (nIndex == m_ctlModelList.GetCount()) {
		nIndex--;
	}
	if (nIndex > 0) {
		m_ctlModelList.SetCurSel(nIndex);
		ChangeModel();
	}
}
