// CharTemplateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MagCoreDataEditer.h"
#include "CharTemplateDlg.h"
#include "afxdialogex.h"


// CCharTemplateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCharTemplateDlg, CDialogEx)

CCharTemplateDlg::CCharTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCharTemplateDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_nTemplateAngle = 0.f;

	m_nSubImageStartX = 0;
	m_nSubImageStartY = 0;
	m_nSubImageEndX = 0;
	m_nSubImageEndY = 0;
}

CCharTemplateDlg::~CCharTemplateDlg()
{
}

void CCharTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Text(pDX, IDC_IMAGE_INFO, m_strImageInfo);
	DDX_Text(pDX, IDC_IMAGE_POS, m_strImagePos);
	DDX_Control(pDX, IDC_TEMPLATE_AREA, m_ctlTemplateArea);
	DDX_Text(pDX, IDC_TEMPLATE_INFO, m_strTemplateInfo);
	DDX_Text(pDX, IDC_SUBIMAGE_START_POS_X, m_nSubImageStartX);
	DDX_Text(pDX, IDC_SUBIMAGE_START_POS_Y, m_nSubImageStartY);
	DDX_Text(pDX, IDC_SUBIMAGE_END_POS_X, m_nSubImageEndX);
	DDX_Text(pDX, IDC_SUBIMAGE_END_POS_Y, m_nSubImageEndY);
	DDX_Text(pDX, IDC_MODEL_ID, m_strModelId);
}

BEGIN_MESSAGE_MAP(CCharTemplateDlg, CDialogEx)
	ON_BN_CLICKED(IDC_OPEN_IMAGE, &CCharTemplateDlg::OnBnClickedOpenImage)
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CCharTemplateDlg::OnBnClickedLoadImage)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_LEFT_UP, &CCharTemplateDlg::OnBnClickedTemplateSizeLeftUp)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_LEFT_DOWN, &CCharTemplateDlg::OnBnClickedTemplateSizeLeftDown)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_RIGHT_DOWN, &CCharTemplateDlg::OnBnClickedTemplateSizeRightDown)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_RIGHT_UP, &CCharTemplateDlg::OnBnClickedTemplateSizeRightUp)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_TOP_UP, &CCharTemplateDlg::OnBnClickedTemplateSizeTopUp)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_TOP_DOWN, &CCharTemplateDlg::OnBnClickedTemplateSizeTopDown)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_BOTTOM_DOWN, &CCharTemplateDlg::OnBnClickedTemplateSizeBottomDown)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_BOTTOM_UP, &CCharTemplateDlg::OnBnClickedTemplateSizeBottomUp)
	ON_BN_CLICKED(IDC_TEMPLATE_SIZE_RESET, &CCharTemplateDlg::OnBnClickedTemplateSizeReset)
	ON_BN_CLICKED(IDC_TEMPLATE_ROTATE_UP, &CCharTemplateDlg::OnBnClickedTemplateRotateUp)
	ON_BN_CLICKED(IDC_TEMPLATE_ROTATE_DOWN, &CCharTemplateDlg::OnBnClickedTemplateRotateDown)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CCharTemplateDlg::OnImagePosValue)
	ON_BN_CLICKED(IDC_TEMPLATE_ROTATE_180, &CCharTemplateDlg::OnBnClickedTemplateRotate180)
	ON_BN_CLICKED(IDC_AUTO_DETECT, &CCharTemplateDlg::OnBnClickedAutoDetect)
	ON_BN_CLICKED(IDC_SET_TEMPLATE, &CCharTemplateDlg::OnBnClickedSetTemplate)
	ON_BN_CLICKED(IDC_SAVE_TEMPLATE, &CCharTemplateDlg::OnBnClickedSaveTemplate)
	ON_BN_CLICKED(IDC_RELOAD_TEMPLATE, &CCharTemplateDlg::OnBnClickedReloadTemplate)
	ON_BN_CLICKED(IDC_AUTO_FIT, &CCharTemplateDlg::OnBnClickedAutoFit)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()


// CCharTemplateDlg 메시지 처리기입니다.
BOOL CCharTemplateDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	DragAcceptFiles(TRUE);

	// Set Image Control
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);
	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}
	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	// Set Template Control
	m_ctlTemplateArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlTemplateArea.ShowWindow(SW_HIDE);
	if (!m_wndTemplate.CreateCtl(this, IDC_TEMPLATE_CTRL))
	{
		AddLogText(L"Failed to create template view");
		return FALSE;
	}
	m_wndTemplate.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);
	m_wndTemplate.SetSendEventMessage(false);

	LoadNIPJob();

	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);
	LoadImage();
	LoadTemplate();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

bool CCharTemplateDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_NIP_JOB_FILE;

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		AddLogText(L"Fail to load Common Data File : %s", strJobFile.c_str());
		return false;
	}
	else {
		AddLogText(L"Success to load Common Data File : %s", strJobFile.c_str());
	}

	return true;
}


void CCharTemplateDlg::OnBnClickedOpenImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"jpg", LPCTSTR(m_strImagePath), OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	m_strImagePath = iFileDlg.GetPathName();
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);

	LoadImage();
}

void CCharTemplateDlg::OnBnClickedLoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadImage();
}

void CCharTemplateDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

void CCharTemplateDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NIPO *pNIPO = NIPO::GetInstance();

	GetDlgItem(IDC_IMAGE_PATH)->GetWindowText(m_strImagePath);

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImagePath), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImagePath));
		return;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImagePath));
	}

	m_nSubImageStartX = 0;
	m_nSubImageStartY = 0;
	m_nSubImageEndX = 0;
	m_nSubImageEndY = 0;
	GetOuterRectImage(dImg);

	UpdateData(FALSE);

	ShowImage();
}

void CCharTemplateDlg::GetOuterRectImage(Mat dImg)
{
	wstring strComp = L"Outer";

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dCharTemplateImg;

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	// Calibration
	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = strComp;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			m_dCalibrationImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;

				m_dMinCircle = pResult->m_listCircle[0];
				m_dMaxCircle = pResult->m_listCircle[1];

				delete pResult;
			}
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Calibration. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
		return;
	}

	// convert image to color image
	cvtColor(m_dCalibrationImg, m_dImg, CV_GRAY2BGR);

	bool bDefect = false;
	strProcessName = L"MagCore Inspection";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = strComp;
		pParam->m_strShowImage = STR_SHOW_IMAGE_RECT_EXT_CHAR;

		// Set Circle Position
		NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;
		dParam_Circle2Rect.m_nCenterPosX = m_dMinCircle.m_ptCenter.x;
		dParam_Circle2Rect.m_nCenterPosY = m_dMinCircle.m_ptCenter.y;
		dParam_Circle2Rect.m_nMinRadius = m_dMinCircle.m_nRadius;
		dParam_Circle2Rect.m_nMaxRadius = m_dMaxCircle.m_nRadius;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = m_dCalibrationImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			m_dImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)dOutput.m_pResult;
				NIPLDefect &dDefect = pResult->m_listDefect[0];
				m_nSubImageStartX = dDefect.m_rcBoundingBox.x;
				m_nSubImageStartY = dDefect.m_rcBoundingBox.y;
				m_nSubImageEndX = m_nSubImageStartX + dDefect.m_rcBoundingBox.width;
				m_nSubImageEndY = m_nSubImageStartY + dDefect.m_rcBoundingBox.height;

				delete dOutput.m_pResult;
			}
		}
		else {
			AddLogText(L"  ! [%s] Cannot Get Outer Rect Image. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Get Outer Rect Image.", strProcessName.c_str());
		return;
	}
}

void CCharTemplateDlg::LoadTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strTemplatePath), dImg)) {
		AddLogText(L"Fail to load Template : %s", LPCTSTR(m_strTemplatePath));
		return;
	}
	else {
		AddLogText(L"Success to load Template : %s", LPCTSTR(m_strTemplatePath));
	}

	m_dTemplate = dImg;
	ResetTemplate();
}

void CCharTemplateDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
	ShowSubImageBox();
}

void CCharTemplateDlg::ShowTemplate()
{
	Mat dRotateTemplate = RotateTemplate();
	m_dTemplateROI = Mat(dRotateTemplate, m_rcTemplateROI);

	m_wndTemplate.Show(m_dTemplateROI);
	ShowImageInfo(true);
}

void CCharTemplateDlg::ShowImageInfo(bool bTemplate)
{
	Mat dImg = bTemplate ? m_dTemplateROI : m_dImg;

	int nSizeX = dImg.cols;
	int nSizeY = dImg.rows;

	wstring strDepth = L"?";
	int nDepth = dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	if (bTemplate) {
		m_strTemplateInfo = szText;
	}
	else {
		m_strImageInfo = szText;
	}
	UpdateData(FALSE);
}


void CCharTemplateDlg::OnBnClickedTemplateSizeLeftUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(true, 1, 0);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeLeftDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(true, -1, 0);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeRightUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(false, -1, 0);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeRightDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(false, 1, 0);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeTopUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(true, 0, 1);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeTopDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(true, 0, -1);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeBottomUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(false, 0, -1);
}

void CCharTemplateDlg::OnBnClickedTemplateSizeBottomDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeTemplateSize(false, 0, 1);
}

void CCharTemplateDlg::ChangeTemplateSize(bool bLeftOrTop, int nDeltaX, int nDeltaY)
{
	// If up-side down, reverse direction and delta value
	if (m_nTemplateAngle > 90.f && m_nTemplateAngle <= 270.f) {
		bLeftOrTop = !bLeftOrTop;
		nDeltaX *= -1;
		nDeltaY *= -1;
	}

	Rect rcTemplateROI = m_rcTemplateROI;
	bool bValid = true;
	if (bLeftOrTop) {
		rcTemplateROI.x += nDeltaX;
		rcTemplateROI.width -= nDeltaX;

		rcTemplateROI.y += nDeltaY;
		rcTemplateROI.height -= nDeltaY;

		if (rcTemplateROI.x < 0 || rcTemplateROI.width <= 0
			|| rcTemplateROI.y < 0 || rcTemplateROI.height <= 0) {
			bValid = false;
		}
	}
	else {
		rcTemplateROI.width += nDeltaX;
		rcTemplateROI.height += nDeltaY;

		if (rcTemplateROI.width <= 0 || (rcTemplateROI.x + rcTemplateROI.width - 1) >= m_dTemplate.cols
			|| rcTemplateROI.height <= 0 || (rcTemplateROI.y + rcTemplateROI.height - 1) >= m_dTemplate.rows) {
			bValid = false;
		}
	}

	if (bValid) {
		m_rcTemplateROI = rcTemplateROI;
		ShowTemplate();
	}
}

Mat CCharTemplateDlg::RotateTemplate()
{
	NIPL *pNIPL = NIPL::GetInstance();

	NIPLInput dInput;
	NIPLOutput dOutput;

	NIPLParam_Rotate dParam_Rotate;
	dParam_Rotate.m_nCenterPosX = m_rcTemplateROI.x + (m_rcTemplateROI.width - 1) * 0.5f;
	dParam_Rotate.m_nCenterPosY = m_rcTemplateROI.y + (m_rcTemplateROI.height - 1) * 0.5f;
	dParam_Rotate.m_nAngle = m_nTemplateAngle;
	dParam_Rotate.m_nScale = 1.f;

	dInput.m_dImg = m_dTemplate;
	dInput.m_pParam = &dParam_Rotate;
	pNIPL->Rotate(&dInput, &dOutput);

	return dOutput.m_dImg;
}

void CCharTemplateDlg::OnBnClickedTemplateSizeReset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ResetTemplate();
}


void CCharTemplateDlg::OnBnClickedTemplateRotateUp()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nTemplateAngle += 0.1f;
	if (m_nTemplateAngle >= 360.f) m_nTemplateAngle -= 360.f;
	ShowTemplate();
}


void CCharTemplateDlg::OnBnClickedTemplateRotateDown()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nTemplateAngle -= 0.1f;
	if (m_nTemplateAngle < 0.f) m_nTemplateAngle += 360.f;
	ShowTemplate();
}

void CCharTemplateDlg::OnBnClickedTemplateRotate180()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nCount = 18;
	for (int i = 0; i < nCount; i++) {
		m_nTemplateAngle += (180.f / nCount);
		if (m_nTemplateAngle >= 360.f) m_nTemplateAngle -= 360.f;
		ShowTemplate();

		// Message Loop to show image changing
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		Sleep(10);
	}
}

LRESULT CCharTemplateDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));
	SetControlDataValue(CPoint(nPosX, nPosY));

	return 0;
}

void CCharTemplateDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	m_strImagePos = szText;
	UpdateData(FALSE);
}

void CCharTemplateDlg::SetControlDataValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	CWnd *pWnd = GetFocusControl();
	if (pWnd == nullptr) {
		return;
	}

	wchar_t szText[256];
	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_SUBIMAGE_START_POS_X:
	case IDC_SUBIMAGE_START_POS_Y:
		{
			int nGapX = ptImagePos.x - m_nSubImageStartX;
			int nGapY = ptImagePos.y - m_nSubImageStartY;
			swprintf_s(szText, L"%d", ptImagePos.x);
			GetDlgItem(IDC_SUBIMAGE_START_POS_X)->SetWindowText(szText);
			swprintf_s(szText, L"%d", ptImagePos.y);
			GetDlgItem(IDC_SUBIMAGE_START_POS_Y)->SetWindowText(szText);

			swprintf_s(szText, L"%d", m_nSubImageEndX + nGapX);
			GetDlgItem(IDC_SUBIMAGE_END_POS_X)->SetWindowText(szText);
			swprintf_s(szText, L"%d", m_nSubImageEndY + nGapY);
			GetDlgItem(IDC_SUBIMAGE_END_POS_Y)->SetWindowText(szText);
		}
		break;
	case IDC_SUBIMAGE_END_POS_X:
	case IDC_SUBIMAGE_END_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_SUBIMAGE_END_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_SUBIMAGE_END_POS_Y)->SetWindowText(szText);
		break;
	}

	ShowSubImageBox();
}

CWnd *CCharTemplateDlg::GetFocusControl()
{
	CWnd *pWnd = GetFocus();
	if (pWnd == nullptr) {
		return nullptr;
	}

	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_SUBIMAGE_START_POS_X:
	case IDC_SUBIMAGE_START_POS_Y:
	case IDC_SUBIMAGE_END_POS_X:
	case IDC_SUBIMAGE_END_POS_Y:
		return pWnd;
	}

	return nullptr;
}

void CCharTemplateDlg::ShowSubImageBox()
{
	UpdateData();

	m_wndImg.ClearImageMark();

	ImageMark dMark;
	dMark.m_rcBoundingBox.x = m_nSubImageStartX;
	dMark.m_rcBoundingBox.y = m_nSubImageStartY;
	dMark.m_rcBoundingBox.width = m_nSubImageEndX - m_nSubImageStartX;
	dMark.m_rcBoundingBox.height = m_nSubImageEndY - m_nSubImageStartY;

	m_wndImg.AddImageMark(dMark);

	m_wndImg.ShowImageMark();
}

void CCharTemplateDlg::OnBnClickedAutoDetect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CCharTemplateDlg::OnBnClickedSetTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Rect rcSubImage;
	rcSubImage.x = m_nSubImageStartX;
	rcSubImage.y = m_nSubImageStartY;
	rcSubImage.width = m_nSubImageEndX - m_nSubImageStartX;
	rcSubImage.height = m_nSubImageEndY - m_nSubImageStartY;

	if (rcSubImage.x < 0 || rcSubImage.y < 0
		|| rcSubImage.width <= 0 || rcSubImage.height <= 0) {
		AddLogText(L"Cannot crop Sub Image. Invalid Pos or Size.");
		return;
	}

	m_dTemplate = Mat(m_dImg, rcSubImage);
	ResetTemplate();
}

void CCharTemplateDlg::ResetTemplate()
{
	m_rcTemplateROI = Rect(0, 0, m_dTemplate.cols, m_dTemplate.rows);
	m_nTemplateAngle = 0.f;
	ShowTemplate();
}


void CCharTemplateDlg::OnBnClickedSaveTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (CHECK_EMPTY_IMAGE(m_dTemplateROI)) {
		AddLogText(L"Cannot save Templage Image. No Template Set.");
		return;
	}

	wstring strImagePath = m_strTemplatePath;

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->SaveImage(strImagePath, m_dTemplateROI)) {
		AddLogText(L"Success to save Template Image : %s", strImagePath.c_str());
	}
	else {
		AddLogText(L"Fail to save Template Image : %s", strImagePath.c_str());
	}
}


void CCharTemplateDlg::OnBnClickedReloadTemplate()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadTemplate();
}

void CCharTemplateDlg::OnBnClickedAutoFit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (AutoFitTemplate()) {
		AddLogText(L"Success to find Auto Fit Template.");
	}
	else {
		AddLogText(L"Fail to find Auto Fit Template Image.");
	}
}

bool CCharTemplateDlg::AutoFitTemplate()
{
	if (CHECK_EMPTY_IMAGE(m_dTemplateROI)) {
		AddLogText(L"  ! Cannot Find Auto Fit Template. No Template Image.");
		return false;
	}

	wstring strComp = L"Outer";

	NIPO *pNIPO = NIPO::GetInstance();

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	bool bDefect = false;
	strProcessName = L"MagCore Inspection";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str());
			return false;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = strComp;
		pParam->m_strShowImage = STR_SHOW_IMAGE_RECT_MATCHED_CHAR;

		// Set Character Template Image
		pParam->m_dCharTemplateImage = m_dTemplateROI;

		// Set Circle Position
		NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;
		dParam_Circle2Rect.m_nCenterPosX = m_dMinCircle.m_ptCenter.x;
		dParam_Circle2Rect.m_nCenterPosY = m_dMinCircle.m_ptCenter.y;
		dParam_Circle2Rect.m_nMinRadius = m_dMinCircle.m_nRadius;
		dParam_Circle2Rect.m_nMaxRadius = m_dMaxCircle.m_nRadius;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = m_dCalibrationImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			if (dOutput.m_pResult != nullptr) {
				delete dOutput.m_pResult;
			}

			if (countNonZero(dOutput.m_dImg) == 0) {
				AddLogText(L"  ! [%s] Set Auto Fit Template. Invalid Character Position.", strProcessName.c_str());
				return false;
			}

			m_dTemplate = dOutput.m_dImg;
			ResetTemplate();
		}
		else {
			AddLogText(L"  ! [%s] Set Auto Fit Template. Error Code : %d", strProcessName.c_str(), nErr);
			return false;
		}
	}
	else {
		AddLogText(L"  ! [%s] Set Auto Fit Template.", strProcessName.c_str());
		return false;
	}

	return true;
}


void CCharTemplateDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	int nCount = DragQueryFile(hDropInfo, 0xffffffff, nullptr, 0);
	if (nCount > 1) {
		AddLogText(L"Cannot load multiple files.");
		return;
	}

	wchar_t szPath[1024];
	DragQueryFile(hDropInfo, 0, szPath, 1023);

	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(szPath);
	OnBnClickedLoadImage();

	CDialogEx::OnDropFiles(hDropInfo);
}
