#include "stdafx.h"
#include "VIData.h"

bool VIData::LoadData(wstring strDataPath)
{
	Clear();

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return 0;
	}

	auto dRoot = dXML.GetRootElement();
	auto listCategory = dRoot.GetChildren();
	for (auto pCategory : listCategory) {
		wstring strElementName = str2wstr(pCategory->GetElementName());
		if (CHECK_STRING(strElementName, STR_MODEL)) {
			wstring strId = str2wstr(pCategory->v("id"));

			m_dModel.m_strId = strId;
		}
		else if (CHECK_STRING(strElementName, STR_CALIBRATION)) {
			auto listData = pCategory->GetChildren();
			for (auto pData : listData) {
				strElementName = str2wstr(pData->GetElementName());

				VIData_Comp *pCompData = nullptr;
				if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_INNER)) pCompData = &m_dInner;
				else if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_LOWER)) pCompData = &m_dLower;
				else if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_UPPER)) pCompData = &m_dUpper;
				else pCompData = &m_dOuter;
				VIData_Calibration &dCal = pCompData->m_dCal;

				try { dCal.m_nColor2GrayChannel = stoi(pData->v("color_to_gray_channel")); }
				catch (...) { dCal.m_nColor2GrayChannel = 0; }
				try { dCal.m_nBrightnessEnhancementLevel = stoi(pData->v("brightness_enhancement_level")); }
				catch (...) { dCal.m_nBrightnessEnhancementLevel = 0; }

				try { dCal.m_nCheckCoreThreshold = stof(pData->v("check_core_threshold")); }
				catch (...) { dCal.m_nCheckCoreThreshold = 0.f; }

				try { dCal.m_nCenterPosX = stoi(pData->v("center_pos_x")); }
				catch (...) { dCal.m_nCenterPosX = 0; }
				try { dCal.m_nCenterPosY = stoi(pData->v("center_pos_y")); }
				catch (...) { dCal.m_nCenterPosY = 0; }
				try { dCal.m_nMinRadiusPosX = stoi(pData->v("min_radius_pos_x")); }
				catch (...) { dCal.m_nMinRadiusPosX = 0; }
				try { dCal.m_nMaxRadiusPosX = stoi(pData->v("max_radius_pos_x")); }
				catch (...) { dCal.m_nMaxRadiusPosX = 0; }
				try { dCal.m_nMinRadiusRange= stoi(pData->v("min_radius_range")); }
				catch (...) { dCal.m_nMinRadiusRange= 0; }
				try { dCal.m_nMaxRadiusRange = stoi(pData->v("max_radius_range")); }
				catch (...) { dCal.m_nMaxRadiusRange = 0; }
				try { dCal.m_nMinRadiusMargin = stoi(pData->v("min_radius_margin")); }
				catch (...) { dCal.m_nMinRadiusMargin = 0; }
				try { dCal.m_nMaxRadiusMargin = stoi(pData->v("max_radius_margin")); }
				catch (...) { dCal.m_nMaxRadiusMargin = 0; }

				try { dCal.m_bMinRadiusReverseEdgeDetect = stoi(pData->v("min_radius_reverse_edge_detect")) != 0; }
				catch (...) { dCal.m_bMinRadiusReverseEdgeDetect = false; }
				try { dCal.m_bMaxRadiusReverseEdgeDetect = stoi(pData->v("max_radius_reverse_edge_detect")) != 0; }
				catch (...) { dCal.m_bMaxRadiusReverseEdgeDetect = false; }

				try { dCal.m_bMinRadiusBinarize = stoi(pData->v("min_radius_binarize")) != 0; }
				catch (...) { dCal.m_bMinRadiusBinarize = false; }
				try { dCal.m_bMaxRadiusBinarize = stoi(pData->v("max_radius_binarize")) != 0; }
				catch (...) { dCal.m_bMaxRadiusBinarize = false; }
				try { dCal.m_bMinRadiusBlack = stoi(pData->v("min_radius_black")) != 0; }
				catch (...) { dCal.m_bMinRadiusBlack = false; }
				try { dCal.m_bMaxRadiusBlack = stoi(pData->v("max_radius_black")) != 0; }
				catch (...) { dCal.m_bMaxRadiusBlack = false; }
			}
		}
		else if (CHECK_STRING(strElementName, STR_INSPECTION)) {
			auto listData = pCategory->GetChildren();
			for (auto pData : listData) {
				strElementName = str2wstr(pData->GetElementName());

				VIData_Comp *pCompData = nullptr;
				if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_INNER)) pCompData = &m_dInner;
				else if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_LOWER)) pCompData = &m_dLower;
				else if (CHECK_STRING(strElementName, STR_MAGCORE_COMP_UPPER)) pCompData = &m_dUpper;
				else pCompData = &m_dOuter;
				VIData_Inspection &dIns = pCompData->m_dIns;

				dIns.m_strRadiusEdgeDoubleLine = str2wstr(pData->v("radius_edge_double_line"));
				if (dIns.m_strRadiusEdgeDoubleLine.empty()) dIns.m_strRadiusEdgeDoubleLine = L"0";
				dIns.m_strMinRadiusEdgeInsideCheck = str2wstr(pData->v("min_radius_edge_inside_check"));
				if (dIns.m_strMinRadiusEdgeInsideCheck.empty()) dIns.m_strMinRadiusEdgeInsideCheck = L"0";
				dIns.m_strMinRadiusEdgeCheck = str2wstr(pData->v("min_radius_edge_check"));
				if (dIns.m_strMinRadiusEdgeCheck.empty()) dIns.m_strMinRadiusEdgeCheck = L"0";
				dIns.m_strMaxRadiusEdgeCheck = str2wstr(pData->v("max_radius_edge_check"));
				if (dIns.m_strMaxRadiusEdgeCheck.empty()) dIns.m_strMaxRadiusEdgeCheck = L"0";
				dIns.m_strMinRadiusEdgeBlack = str2wstr(pData->v("min_radius_edge_black"));
				if (dIns.m_strMinRadiusEdgeBlack.empty()) dIns.m_strMinRadiusEdgeBlack = L"0";
				dIns.m_strMaxRadiusEdgeBlack = str2wstr(pData->v("max_radius_edge_black"));
				if (dIns.m_strMaxRadiusEdgeBlack.empty()) dIns.m_strMaxRadiusEdgeBlack = L"0";
				dIns.m_strMinRadiusEdgeInsideAreaHeight = str2wstr(pData->v("min_radius_edge_inside_area_height"));
				if (dIns.m_strMinRadiusEdgeInsideAreaHeight.empty()) dIns.m_strMinRadiusEdgeInsideAreaHeight = L"0";
				dIns.m_strMinRadiusEdgeAreaHeight = str2wstr(pData->v("min_radius_edge_area_height"));
				if (dIns.m_strMinRadiusEdgeAreaHeight.empty()) dIns.m_strMinRadiusEdgeAreaHeight = L"0";
				dIns.m_strMaxRadiusEdgeAreaHeight = str2wstr(pData->v("max_radius_edge_area_height"));
				if (dIns.m_strMaxRadiusEdgeAreaHeight.empty()) dIns.m_strMaxRadiusEdgeAreaHeight = L"0";
				dIns.m_strMinRadiusEdgeInsideThreshold = str2wstr(pData->v("min_radius_edge_inside_threshold"));
				if (dIns.m_strMinRadiusEdgeInsideThreshold.empty()) dIns.m_strMinRadiusEdgeInsideThreshold = L"0";
				dIns.m_strMinRadiusEdgeSpotBinarizeThreshold = str2wstr(pData->v("min_radius_edge_spot_binarize_threshold"));
				if (dIns.m_strMinRadiusEdgeSpotBinarizeThreshold.empty()) dIns.m_strMinRadiusEdgeSpotBinarizeThreshold = L"0";
				dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold = str2wstr(pData->v("max_radius_edge_spot_binarize_threshold"));
				if (dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold.empty()) dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold = L"0";
				dIns.m_strMinRadiusEdgeSpotProfileThreshold = str2wstr(pData->v("min_radius_edge_spot_profile_threshold"));
				if (dIns.m_strMinRadiusEdgeSpotProfileThreshold.empty()) dIns.m_strMinRadiusEdgeSpotProfileThreshold = L"0";
				dIns.m_strMaxRadiusEdgeSpotProfileThreshold = str2wstr(pData->v("max_radius_edge_spot_profile_threshold"));
				if (dIns.m_strMaxRadiusEdgeSpotProfileThreshold.empty()) dIns.m_strMaxRadiusEdgeSpotProfileThreshold = L"0";
				dIns.m_strMinRadiusEdgeHoleBinarizeThreshold = str2wstr(pData->v("min_radius_edge_hole_binarize_threshold"));
				if (dIns.m_strMinRadiusEdgeHoleBinarizeThreshold.empty()) dIns.m_strMinRadiusEdgeHoleBinarizeThreshold = L"0";
				dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2 = str2wstr(pData->v("min_radius_edge_hole_binarize_threshold_2"));
				if (dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2.empty()) dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2 = L"0";
				dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold = str2wstr(pData->v("max_radius_edge_hole_binarize_threshold"));
				if (dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold.empty()) dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold = L"0";
				dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2 = str2wstr(pData->v("max_radius_edge_hole_binarize_threshold_2"));
				if (dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2.empty()) dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2 = L"0";
				dIns.m_strMinRadiusEdgeBinarizeThreshold = str2wstr(pData->v("min_radius_edge_binarize_threshold"));
				if (dIns.m_strMinRadiusEdgeBinarizeThreshold.empty()) dIns.m_strMinRadiusEdgeBinarizeThreshold = L"0";
				dIns.m_strMinRadiusEdgeBinarizeThreshold2 = str2wstr(pData->v("min_radius_edge_binarize_threshold_2"));
				if (dIns.m_strMinRadiusEdgeBinarizeThreshold2.empty()) dIns.m_strMinRadiusEdgeBinarizeThreshold2 = L"0";
				dIns.m_strMaxRadiusEdgeBinarizeThreshold = str2wstr(pData->v("max_radius_edge_binarize_threshold"));
				if (dIns.m_strMaxRadiusEdgeBinarizeThreshold.empty()) dIns.m_strMaxRadiusEdgeBinarizeThreshold = L"0";
				dIns.m_strMaxRadiusEdgeBinarizeThreshold2 = str2wstr(pData->v("max_radius_edge_binarize_threshold_2"));
				if (dIns.m_strMaxRadiusEdgeBinarizeThreshold2.empty()) dIns.m_strMaxRadiusEdgeBinarizeThreshold2 = L"0";
				dIns.m_strMinRadiusEdgeProfileThreshold = str2wstr(pData->v("min_radius_edge_profile_threshold"));
				if (dIns.m_strMinRadiusEdgeProfileThreshold.empty()) dIns.m_strMinRadiusEdgeProfileThreshold = L"0";
				dIns.m_strMinRadiusEdgeProfileThreshold2 = str2wstr(pData->v("min_radius_edge_profile_threshold_2"));
				if (dIns.m_strMinRadiusEdgeProfileThreshold2.empty()) dIns.m_strMinRadiusEdgeProfileThreshold2 = L"0";
				dIns.m_strMaxRadiusEdgeProfileThreshold = str2wstr(pData->v("max_radius_edge_profile_threshold"));
				if (dIns.m_strMaxRadiusEdgeProfileThreshold.empty()) dIns.m_strMaxRadiusEdgeProfileThreshold = L"0";
				dIns.m_strMaxRadiusEdgeProfileThreshold2 = str2wstr(pData->v("max_radius_edge_profile_threshold_2"));
				if (dIns.m_strMaxRadiusEdgeProfileThreshold2.empty()) dIns.m_strMaxRadiusEdgeProfileThreshold2 = L"0";

				dIns.m_strCharCheck = str2wstr(pData->v("char_check"));
				if (dIns.m_strCharCheck.empty()) dIns.m_strCharCheck = L"0";
				dIns.m_strCharBlack = str2wstr(pData->v("char_black"));
				if (dIns.m_strCharBlack.empty()) dIns.m_strCharBlack = L"0";
				dIns.m_strCharMinRadiusEdgeMargin = str2wstr(pData->v("char_min_radius_edge_margin"));
				if (dIns.m_strCharMinRadiusEdgeMargin.empty()) dIns.m_strCharMinRadiusEdgeMargin = L"0";
				dIns.m_strCharMaxRadiusEdgeMargin = str2wstr(pData->v("char_max_radius_edge_margin"));
				if (dIns.m_strCharMaxRadiusEdgeMargin.empty()) dIns.m_strCharMaxRadiusEdgeMargin = L"0";
				dIns.m_strCharConnectDistance = str2wstr(pData->v("char_connect_distance"));
				if (dIns.m_strCharConnectDistance.empty()) dIns.m_strCharConnectDistance = L"0";

				dIns.m_strSurfaceMinRadiusEdgeMargin = str2wstr(pData->v("surface_min_radius_edge_margin"));
				if (dIns.m_strSurfaceMinRadiusEdgeMargin.empty()) dIns.m_strSurfaceMinRadiusEdgeMargin = L"0";
				dIns.m_strSurfaceMaxRadiusEdgeMargin = str2wstr(pData->v("surface_max_radius_edge_margin"));
				if (dIns.m_strSurfaceMaxRadiusEdgeMargin.empty()) dIns.m_strSurfaceMaxRadiusEdgeMargin = L"0";
				dIns.m_strSurfaceBlockCount = str2wstr(pData->v("surface_block_count"));
				if (dIns.m_strSurfaceBlockCount.empty()) dIns.m_strSurfaceBlockCount = L"0";
				dIns.m_strSurfaceBlockBinarizeThreshold= str2wstr(pData->v("surface_block_binarize_threshold"));
				if (dIns.m_strSurfaceBlockBinarizeThreshold.empty()) dIns.m_strSurfaceBlockBinarizeThreshold = L"0";
				dIns.m_strSurfaceBlockVarianceRatio = str2wstr(pData->v("surface_block_variance_ratio"));
				if (dIns.m_strSurfaceBlockVarianceRatio.empty()) dIns.m_strSurfaceBlockVarianceRatio= L"0";
				dIns.m_strSurfaceBlockVarianceMax = str2wstr(pData->v("surface_block_variance_max"));
				if (dIns.m_strSurfaceBlockVarianceMax.empty()) dIns.m_strSurfaceBlockVarianceMax= L"0";
				dIns.m_strSurfaceBlockVarianceThreshold = str2wstr(pData->v("surface_block_variance_threshold"));
				if (dIns.m_strSurfaceBlockVarianceThreshold.empty()) dIns.m_strSurfaceBlockVarianceThreshold= L"0";

				dIns.m_strHoleMinRadiusEdgeMargin = str2wstr(pData->v("hole_min_radius_edge_margin"));
				if (dIns.m_strHoleMinRadiusEdgeMargin.empty()) dIns.m_strHoleMinRadiusEdgeMargin = L"0";
				dIns.m_strHoleMaxRadiusEdgeMargin = str2wstr(pData->v("hole_max_radius_edge_margin"));
				if (dIns.m_strHoleMaxRadiusEdgeMargin.empty()) dIns.m_strHoleMaxRadiusEdgeMargin = L"0";
				dIns.m_strHoleBlockCountX = str2wstr(pData->v("hole_block_count_x"));
				if (dIns.m_strHoleBlockCountX.empty()) dIns.m_strHoleBlockCountX = L"0";
				dIns.m_strHoleBlockCountY = str2wstr(pData->v("hole_block_count_y"));
				if (dIns.m_strHoleBlockCountY.empty()) dIns.m_strHoleBlockCountY = L"0";
				dIns.m_strHoleBinarizeThreshold = str2wstr(pData->v("hole_binarize_threshold"));
				if (dIns.m_strHoleBinarizeThreshold.empty()) dIns.m_strHoleBinarizeThreshold = L"0";
				dIns.m_strHoleMinSize = str2wstr(pData->v("hole_min_size"));
				if (dIns.m_strHoleMinSize.empty()) dIns.m_strHoleMinSize = L"0";
				dIns.m_strHoleSizeRatio = str2wstr(pData->v("hole_size_ratio"));
				if (dIns.m_strHoleSizeRatio.empty()) dIns.m_strHoleSizeRatio = L"0";
				dIns.m_strHoleSmallSize = str2wstr(pData->v("hole_small_size"));
				if (dIns.m_strHoleSmallSize.empty()) dIns.m_strHoleSmallSize = L"0";
				dIns.m_strHoleSmallSizeRatio = str2wstr(pData->v("hole_small_size_ratio"));
				if (dIns.m_strHoleSmallSizeRatio.empty()) dIns.m_strHoleSmallSizeRatio = L"0";
				dIns.m_strHoleBigSize = str2wstr(pData->v("hole_big_size"));
				if (dIns.m_strHoleBigSize.empty()) dIns.m_strHoleBigSize = L"0";
				dIns.m_strHoleBigSizeRatio = str2wstr(pData->v("hole_big_size_ratio"));
				if (dIns.m_strHoleBigSizeRatio.empty()) dIns.m_strHoleBigSizeRatio = L"0";
			}
		}
	}

	return true;
}

bool VIData::SaveData(wstring strDataPath)
{
	XML dXML;

	// Set Header
	XMLHeader dHeader;
	XMLVariable &version = dHeader.GetVersion();
	XMLVariable &encoding = dHeader.GetEncoding();
	XMLVariable &standalone = dHeader.GetStandalone();
	version.SetValue("1.0");
	encoding.SetValue("utf-8");
	standalone.SetValue("");
	dXML.SetHeader(dHeader);

	// Set Body
	XMLElement dRoot("VIData");
	dRoot.AddVariable("version", STR_VIDATA_VERSION);

	XMLElement dXMLModel("Model");
	dXMLModel.AddVariable("id", wstr2str(m_dModel.m_strId).c_str());
	dRoot.AddElement(dXMLModel);

	_SetXMLData(STR_CALIBRATION, dRoot);
	_SetXMLData(STR_INSPECTION, dRoot);

	dXML.SetRootElement(dRoot);

	auto nResult = dXML.Save(strDataPath.c_str());
	if (nResult != XML3::XML_ERROR::OK) {
		return 0;
	}

	return true;
}

void VIData::_SetXMLData(wstring strElementName, XMLElement &dXMLRoot)
{
	XMLElement dXMLCategory(wstr2str(strElementName).c_str());

	for (int i = 0; i < COMP_COUNT; i++) {
		VIData_Comp *pCompData = nullptr;
		wstring strComp = L"";
		switch (i) {
		case COMP_INNER: pCompData = &m_dInner; strComp = STR_MAGCORE_COMP_INNER; break;
		case COMP_LOWER: pCompData = &m_dLower; strComp = STR_MAGCORE_COMP_LOWER; break;
		case COMP_UPPER: pCompData = &m_dUpper; strComp = STR_MAGCORE_COMP_UPPER; break;
		case COMP_OUTER: pCompData = &m_dOuter; strComp = STR_MAGCORE_COMP_OUTER; break;
		}

		XMLElement dXMLData(wstr2str(strComp).c_str());

		if (CHECK_STRING(strElementName, STR_CALIBRATION)) {
			VIData_Calibration &dCal = pCompData->m_dCal;

			string strColor2GrayChannel = to_string(dCal.m_nColor2GrayChannel);
			string strBrightnessEnhancementLevel = to_string(dCal.m_nBrightnessEnhancementLevel);
			string strCheckCoreThreshold = to_string(dCal.m_nCheckCoreThreshold);
			string strCenterPosX = to_string(dCal.m_nCenterPosX);
			string strCenterPosY = to_string(dCal.m_nCenterPosY);
			string strMinRadiusPosX = to_string(dCal.m_nMinRadiusPosX);
			string strMaxRadiusPosX = to_string(dCal.m_nMaxRadiusPosX);
			string strMinRadiusRange = to_string(dCal.m_nMinRadiusRange);
			string strMaxRadiusRange = to_string(dCal.m_nMaxRadiusRange);
			string strMinRadiusMargin = to_string(dCal.m_nMinRadiusMargin);
			string strMaxRadiusMargin = to_string(dCal.m_nMaxRadiusMargin);
			string strMinRadiusReverseEdgeDetect = to_string(dCal.m_bMinRadiusReverseEdgeDetect ? 1 : 0);
			string strMaxRadiusReverseEdgeDetect = to_string(dCal.m_bMaxRadiusReverseEdgeDetect ? 1 : 0);
			string strMinRadiusBinarize = to_string(dCal.m_bMinRadiusBinarize ? 1 : 0);
			string strMaxRadiusBinarize = to_string(dCal.m_bMaxRadiusBinarize ? 1 : 0);
			string strMinRadiusBlack = to_string(dCal.m_bMinRadiusBlack ? 1 : 0);
			string strMaxRadiusBlack = to_string(dCal.m_bMaxRadiusBlack ? 1 : 0);

			dXMLData.AddVariable("color_to_gray_channel", strColor2GrayChannel.c_str());
			dXMLData.AddVariable("brightness_enhancement_level", strBrightnessEnhancementLevel.c_str());
			dXMLData.AddVariable("check_core_threshold", strCheckCoreThreshold.c_str());
			dXMLData.AddVariable("center_pos_x", strCenterPosX.c_str());
			dXMLData.AddVariable("center_pos_y", strCenterPosY.c_str());
			dXMLData.AddVariable("min_radius_pos_x", strMinRadiusPosX.c_str());
			dXMLData.AddVariable("max_radius_pos_x", strMaxRadiusPosX.c_str());
			dXMLData.AddVariable("min_radius_margin", strMinRadiusMargin.c_str());
			dXMLData.AddVariable("min_radius_range", strMinRadiusRange.c_str());
			dXMLData.AddVariable("max_radius_range", strMaxRadiusRange.c_str());
			dXMLData.AddVariable("max_radius_margin", strMaxRadiusMargin.c_str());
			dXMLData.AddVariable("min_radius_reverse_edge_detect", strMinRadiusReverseEdgeDetect.c_str());
			dXMLData.AddVariable("max_radius_reverse_edge_detect", strMaxRadiusReverseEdgeDetect.c_str());
			dXMLData.AddVariable("min_radius_binarize", strMinRadiusBinarize.c_str());
			dXMLData.AddVariable("max_radius_binarize", strMaxRadiusBinarize.c_str());
			dXMLData.AddVariable("min_radius_black", strMinRadiusBlack.c_str());
			dXMLData.AddVariable("max_radius_black", strMaxRadiusBlack.c_str());
		}
		else if (CHECK_STRING(strElementName, STR_INSPECTION)) {
			VIData_Inspection &dIns = pCompData->m_dIns;

			string strRadiusEdgeDoubleLine = wstr2str(dIns.m_strRadiusEdgeDoubleLine);
			string strMinRadiusEdgeInsideCheck = wstr2str(dIns.m_strMinRadiusEdgeInsideCheck);
			string strMinRadiusEdgeCheck = wstr2str(dIns.m_strMinRadiusEdgeCheck);
			string strMaxRadiusEdgeCheck = wstr2str(dIns.m_strMaxRadiusEdgeCheck);
			string strMinRadiusEdgeBlack = wstr2str(dIns.m_strMinRadiusEdgeBlack);
			string strMaxRadiusEdgeBlack = wstr2str(dIns.m_strMaxRadiusEdgeBlack);
			string strMinRadiusEdgeInsideAreaHeight = wstr2str(dIns.m_strMinRadiusEdgeInsideAreaHeight);
			string strMinRadiusEdgeAreaHeight = wstr2str(dIns.m_strMinRadiusEdgeAreaHeight);
			string strMaxRadiusEdgeAreaHeight = wstr2str(dIns.m_strMaxRadiusEdgeAreaHeight);
			string strMinRadiusEdgeInsideThreshold = wstr2str(dIns.m_strMinRadiusEdgeInsideThreshold);
			string strMinRadiusEdgeSpotBinarizeThreshold = wstr2str(dIns.m_strMinRadiusEdgeSpotBinarizeThreshold);
			string strMaxRadiusEdgeSpotBinarizeThreshold = wstr2str(dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold);
			string strMinRadiusEdgeSpotProfileThreshold = wstr2str(dIns.m_strMinRadiusEdgeSpotProfileThreshold);
			string strMaxRadiusEdgeSpotProfileThreshold = wstr2str(dIns.m_strMaxRadiusEdgeSpotProfileThreshold);
			string strMinRadiusEdgeHoleBinarizeThreshold = wstr2str(dIns.m_strMinRadiusEdgeHoleBinarizeThreshold);
			string strMinRadiusEdgeHoleBinarizeThreshold2 = wstr2str(dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2);
			string strMaxRadiusEdgeHoleBinarizeThreshold = wstr2str(dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold);
			string strMaxRadiusEdgeHoleBinarizeThreshold2 = wstr2str(dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2);
			string strMinRadiusEdgeBinarizeThreshold = wstr2str(dIns.m_strMinRadiusEdgeBinarizeThreshold);
			string strMinRadiusEdgeBinarizeThreshold2 = wstr2str(dIns.m_strMinRadiusEdgeBinarizeThreshold2);
			string strMaxRadiusEdgeBinarizeThreshold = wstr2str(dIns.m_strMaxRadiusEdgeBinarizeThreshold);
			string strMaxRadiusEdgeBinarizeThreshold2 = wstr2str(dIns.m_strMaxRadiusEdgeBinarizeThreshold2);
			string strMinRadiusEdgeProfileThreshold = wstr2str(dIns.m_strMinRadiusEdgeProfileThreshold);
			string strMinRadiusEdgeProfileThreshold2 = wstr2str(dIns.m_strMinRadiusEdgeProfileThreshold2);
			string strMaxRadiusEdgeProfileThreshold = wstr2str(dIns.m_strMaxRadiusEdgeProfileThreshold);
			string strMaxRadiusEdgeProfileThreshold2 = wstr2str(dIns.m_strMaxRadiusEdgeProfileThreshold2);
			string strCharCheck = wstr2str(dIns.m_strCharCheck);
			string strCharBlack = wstr2str(dIns.m_strCharBlack);
			string strCharMinRadiusEdgeMargin = wstr2str(dIns.m_strCharMinRadiusEdgeMargin);
			string strCharMaxRadiusEdgeMargin = wstr2str(dIns.m_strCharMaxRadiusEdgeMargin);
			string strCharConnectDistance = wstr2str(dIns.m_strCharConnectDistance);
			string strSurfaceMinRadiusEdgeMargin = wstr2str(dIns.m_strSurfaceMinRadiusEdgeMargin);
			string strSurfaceMaxRadiusEdgeMargin = wstr2str(dIns.m_strSurfaceMaxRadiusEdgeMargin);
			string strSurfaceBlockCount = wstr2str(dIns.m_strSurfaceBlockCount);
			string strSurfaceBlockBinarizeThreshold = wstr2str(dIns.m_strSurfaceBlockBinarizeThreshold);
			string strSurfaceBlockVarianceRatio = wstr2str(dIns.m_strSurfaceBlockVarianceRatio);
			string strSurfaceBlockVarianceMax = wstr2str(dIns.m_strSurfaceBlockVarianceMax);
			string strSurfaceBlockVarianceThreshold = wstr2str(dIns.m_strSurfaceBlockVarianceThreshold);
			string strHoleMinRadiusEdgeMargin = wstr2str(dIns.m_strHoleMinRadiusEdgeMargin);
			string strHoleMaxRadiusEdgeMargin = wstr2str(dIns.m_strHoleMaxRadiusEdgeMargin);
			string strHoleBlockCountX = wstr2str(dIns.m_strHoleBlockCountX);
			string strHoleBlockCountY = wstr2str(dIns.m_strHoleBlockCountY);
			string strHoleBinarizeThreshold = wstr2str(dIns.m_strHoleBinarizeThreshold);
			string strHoleMinSize = wstr2str(dIns.m_strHoleMinSize);
			string strHoleSizeRatio = wstr2str(dIns.m_strHoleSizeRatio);
			string strHoleSmallSize = wstr2str(dIns.m_strHoleSmallSize);
			string strHoleSmallSizeRatio = wstr2str(dIns.m_strHoleSmallSizeRatio);
			string strHoleBigSize = wstr2str(dIns.m_strHoleBigSize);
			string strHoleBigSizeRatio = wstr2str(dIns.m_strHoleBigSizeRatio);

			dXMLData.AddVariable("radius_edge_double_line", strRadiusEdgeDoubleLine.c_str());
			dXMLData.AddVariable("min_radius_edge_inside_check", strMinRadiusEdgeInsideCheck.c_str());
			dXMLData.AddVariable("min_radius_edge_check", strMinRadiusEdgeCheck.c_str());
			dXMLData.AddVariable("max_radius_edge_check", strMaxRadiusEdgeCheck.c_str());
			dXMLData.AddVariable("min_radius_edge_black", strMinRadiusEdgeBlack.c_str());
			dXMLData.AddVariable("max_radius_edge_black", strMaxRadiusEdgeBlack.c_str());
			dXMLData.AddVariable("min_radius_edge_inside_area_height", strMinRadiusEdgeInsideAreaHeight.c_str());
			dXMLData.AddVariable("min_radius_edge_area_height", strMinRadiusEdgeAreaHeight.c_str());
			dXMLData.AddVariable("max_radius_edge_area_height", strMaxRadiusEdgeAreaHeight.c_str());
			dXMLData.AddVariable("min_radius_edge_inside_threshold", strMinRadiusEdgeInsideThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_spot_binarize_threshold", strMinRadiusEdgeSpotBinarizeThreshold.c_str());
			dXMLData.AddVariable("max_radius_edge_spot_binarize_threshold", strMaxRadiusEdgeSpotBinarizeThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_spot_profile_threshold", strMinRadiusEdgeSpotProfileThreshold.c_str());
			dXMLData.AddVariable("max_radius_edge_spot_profile_threshold", strMaxRadiusEdgeSpotProfileThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_hole_binarize_threshold", strMinRadiusEdgeHoleBinarizeThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_hole_binarize_threshold_2", strMinRadiusEdgeHoleBinarizeThreshold2.c_str());
			dXMLData.AddVariable("max_radius_edge_hole_binarize_threshold", strMaxRadiusEdgeHoleBinarizeThreshold.c_str());
			dXMLData.AddVariable("max_radius_edge_hole_binarize_threshold_2", strMaxRadiusEdgeHoleBinarizeThreshold2.c_str());
			dXMLData.AddVariable("min_radius_edge_binarize_threshold", strMinRadiusEdgeBinarizeThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_binarize_threshold_2", strMinRadiusEdgeBinarizeThreshold2.c_str());
			dXMLData.AddVariable("max_radius_edge_binarize_threshold", strMaxRadiusEdgeBinarizeThreshold.c_str());
			dXMLData.AddVariable("max_radius_edge_binarize_threshold_2", strMaxRadiusEdgeBinarizeThreshold2.c_str());
			dXMLData.AddVariable("min_radius_edge_profile_threshold", strMinRadiusEdgeProfileThreshold.c_str());
			dXMLData.AddVariable("min_radius_edge_profile_threshold_2", strMinRadiusEdgeProfileThreshold2.c_str());
			dXMLData.AddVariable("max_radius_edge_profile_threshold", strMaxRadiusEdgeProfileThreshold.c_str());
			dXMLData.AddVariable("max_radius_edge_profile_threshold_2", strMaxRadiusEdgeProfileThreshold2.c_str());

			dXMLData.AddVariable("char_check", strCharCheck.c_str());
			dXMLData.AddVariable("char_black", strCharBlack.c_str());
			dXMLData.AddVariable("char_min_radius_edge_margin", strCharMinRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("char_max_radius_edge_margin", strCharMaxRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("char_connect_distance", strCharConnectDistance.c_str());

			dXMLData.AddVariable("surface_min_radius_edge_margin", strSurfaceMinRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("surface_max_radius_edge_margin", strSurfaceMaxRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("surface_block_count", strSurfaceBlockCount.c_str());
			dXMLData.AddVariable("surface_block_binarize_threshold", strSurfaceBlockBinarizeThreshold.c_str());
			dXMLData.AddVariable("surface_block_variance_ratio", strSurfaceBlockVarianceRatio.c_str());
			dXMLData.AddVariable("surface_block_variance_max", strSurfaceBlockVarianceMax.c_str());
			dXMLData.AddVariable("surface_block_variance_threshold", strSurfaceBlockVarianceThreshold.c_str());

			dXMLData.AddVariable("hole_min_radius_edge_margin", strHoleMinRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("hole_max_radius_edge_margin", strHoleMaxRadiusEdgeMargin.c_str());
			dXMLData.AddVariable("hole_block_count_x", strHoleBlockCountX.c_str());
			dXMLData.AddVariable("hole_block_count_y", strHoleBlockCountY.c_str());
			dXMLData.AddVariable("hole_binarize_threshold", strHoleBinarizeThreshold.c_str());
			dXMLData.AddVariable("hole_min_size", strHoleMinSize.c_str());
			dXMLData.AddVariable("hole_size_ratio", strHoleSizeRatio.c_str());
			dXMLData.AddVariable("hole_small_size", strHoleSmallSize.c_str());
			dXMLData.AddVariable("hole_small_size_ratio", strHoleSmallSizeRatio.c_str());
			dXMLData.AddVariable("hole_big_size", strHoleBigSize.c_str());
			dXMLData.AddVariable("hole_big_size_ratio", strHoleBigSizeRatio.c_str());
		}

		dXMLCategory.AddElement(dXMLData);
	}

	dXMLRoot.AddElement(dXMLCategory);
}

