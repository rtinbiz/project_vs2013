#pragma once

// CCharTemplateDlg 대화 상자입니다.

class CCharTemplateDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CCharTemplateDlg)

private :
	HICON m_hIcon;

	NIPJob m_dJob;

	Mat m_dImg;
	Mat m_dCalibrationImg;
	NIPLImageCtl m_wndImg;

	Mat m_dTemplate;
	Mat m_dTemplateROI;
	NIPLImageCtl m_wndTemplate;
	Rect m_rcTemplateROI;
	float m_nTemplateAngle;

	NIPLCircle m_dMinCircle;
	NIPLCircle m_dMaxCircle;

public :
	CString m_strImagePath;
	CString m_strTemplatePath;
	CString m_strVIDataPath;

	bool LoadNIPJob();
	void ShowImage();
	void ShowTemplate();
	void LoadImage();
	void LoadTemplate();
	void ShowImageInfo(bool bTemplate = false);
	void AddLogText(const wchar_t* szText, ...);
	void ChangeTemplateSize(bool bLeftOrTop, int nDeltaX, int nDeltaY);
	Mat RotateTemplate();
	void GetOuterRectImage(Mat dImg);
	void ShowImagePosValue(CPoint ptImagePos);
	void SetControlDataValue(CPoint ptImagePos);
	CWnd *GetFocusControl();
	void ShowSubImageBox();
	void ResetTemplate();
	bool AutoFitTemplate();

public:
	CCharTemplateDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCharTemplateDlg();

	CListBox m_ctlLogList;
	CStatic m_ctlImageArea;
	CString m_strImageInfo;
	CString m_strImagePos;
	CStatic m_ctlTemplateArea;
	CString m_strTemplateInfo;
	int m_nSubImageStartX;
	int m_nSubImageStartY;
	int m_nSubImageEndX;
	int m_nSubImageEndY;
	CString m_strModelId;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CHAR_TEMPLATE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOpenImage();
	afx_msg void OnBnClickedLoadImage();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedTemplateSizeLeftUp();
	afx_msg void OnBnClickedTemplateSizeLeftDown();
	afx_msg void OnBnClickedTemplateSizeRightDown();
	afx_msg void OnBnClickedTemplateSizeRightUp();
	afx_msg void OnBnClickedTemplateSizeTopUp();
	afx_msg void OnBnClickedTemplateSizeTopDown();
	afx_msg void OnBnClickedTemplateSizeBottomDown();
	afx_msg void OnBnClickedTemplateSizeBottomUp();
	afx_msg void OnBnClickedTemplateSizeReset();
	afx_msg void OnBnClickedTemplateRotateUp();
	afx_msg void OnBnClickedTemplateRotateDown();
	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedTemplateRotate180();
	afx_msg void OnBnClickedAutoDetect();
	afx_msg void OnBnClickedSetTemplate();
	afx_msg void OnBnClickedSaveTemplate();
	afx_msg void OnBnClickedReloadTemplate();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnBnClickedAutoFit();
};
