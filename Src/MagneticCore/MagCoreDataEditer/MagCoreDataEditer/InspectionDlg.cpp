// InspectionDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MagCoreDataEditer.h"
#include "InspectionDlg.h"
#include "afxdialogex.h"

// CInspectionDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CInspectionDlg, CDialogEx)

CInspectionDlg::CInspectionDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInspectionDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CInspectionDlg::~CInspectionDlg()
{
}

void CInspectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMMON_DATA, m_ctlCommonData);
	DDX_Control(pDX, IDC_COMP_DATA, m_ctlCompData);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Text(pDX, IDC_IMAGE_INFO, m_strImageInfo);
	DDX_Text(pDX, IDC_IMAGE_POS, m_strImagePos);
	DDX_Text(pDX, IDC_MODEL_ID, m_strModelId);
	DDX_Radio(pDX, IDC_COMP_INNER, m_nCompIndex);
}

BEGIN_MESSAGE_MAP(CInspectionDlg, CDialogEx)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_OPEN_IMAGE, &CInspectionDlg::OnBnClickedOpenImage)
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CInspectionDlg::OnBnClickedLoadImage)
	ON_COMMAND_RANGE(IDC_COMP_INNER, IDC_COMP_OUTER, OnClickedCompRadio)
	ON_BN_CLICKED(IDC_SHOW_ORIGINAL_IMAGE, &CInspectionDlg::OnBnClickedShowOriginalImage)
	ON_BN_CLICKED(IDC_INSPECTION_TEST, &CInspectionDlg::OnBnClickedInspectionTest)
	ON_BN_CLICKED(IDC_DATA_APPLY, &CInspectionDlg::OnBnClickedDataApply)
	ON_BN_CLICKED(IDC_SAVE_DATA, &CInspectionDlg::OnBnClickedSaveData)
	ON_WM_SETFOCUS()
	ON_WM_DROPFILES()
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CInspectionDlg::OnImagePosValue)
	ON_BN_CLICKED(IDC_RELOAD_MODEL_DATA, &CInspectionDlg::OnBnClickedReloadModelData)
	ON_BN_CLICKED(IDC_DATA_APPLY_ALL, &CInspectionDlg::OnBnClickedDataApplyAll)
	ON_BN_CLICKED(IDC_COVERT_COLOR, &CInspectionDlg::OnBnClickedCovertColor)
	ON_BN_CLICKED(IDC_BACKUP_MODEL_DATA, &CInspectionDlg::OnBnClickedBackupModelData)
	ON_BN_CLICKED(IDC_RESTORE_MODEL_DATA, &CInspectionDlg::OnBnClickedRestoreModelData)
END_MESSAGE_MAP()

// CInspectionDlg 메시지 처리기입니다.

void CInspectionDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (m_nCompIndex == COMP_ALL) {
		m_nCompIndex = COMP_OUTER;
	}
	UpdateData(FALSE);

	GetWindowRect(&m_rcPos);

	CDialogEx::OnOK();
}

void CInspectionDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_nCompIndex == COMP_ALL) {
		m_nCompIndex = COMP_OUTER;
	}
	UpdateData(FALSE);

	GetWindowRect(&m_rcPos);

	CDialogEx::OnClose();
}

void CInspectionDlg::GetWindowPos(CRect &rc)
{
	rc = m_rcPos;
}


BOOL CInspectionDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	DragAcceptFiles(TRUE);

	// Set Image Control
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);
	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}
	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	SetDataCtrlFont();

	m_ctlCommonData.SetVSDotNetLook();
	m_ctlCommonData.MarkModifiedProperties(TRUE);

	m_ctlCompData.SetVSDotNetLook();
	m_ctlCompData.MarkModifiedProperties(TRUE);

	SetFixedCommonData();

	LoadData();

	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImageFilePath);
	LoadImage();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CInspectionDlg::SetFixedCommonData()
{
	m_listFixedCommonData.push_back(L"Input / Output");
	m_listFixedCommonData.push_back(L"Data Path");
	m_listFixedCommonData.push_back(L"Component");

	m_listFixedCommonData.push_back(L"Radius Edge Double Line");
	m_listFixedCommonData.push_back(L"Min Radius Edge Inside Check");
	m_listFixedCommonData.push_back(L"Min Radius Edge Check");
	m_listFixedCommonData.push_back(L"Max Radius Edge Check");
	m_listFixedCommonData.push_back(L"Min Radius Edge Black");
	m_listFixedCommonData.push_back(L"Max Radius Edge Black");
	m_listFixedCommonData.push_back(L"Min Radius Edge Inside Area Height");
	m_listFixedCommonData.push_back(L"Min Radius Edge Area Height");
	m_listFixedCommonData.push_back(L"Max Radius Edge Area Height");
	m_listFixedCommonData.push_back(L"Min Radius Edge Inside Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Spot Binarize Threshold");
	m_listFixedCommonData.push_back(L"Max Radius Edge Spot Binarize Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Spot Profile Threshold");
	m_listFixedCommonData.push_back(L"Max Radius Edge Spot Profile Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Hole Binarize Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Hole Binarize Threshold 2");
	m_listFixedCommonData.push_back(L"Max Radius Edge Hole Binarize Threshold");
	m_listFixedCommonData.push_back(L"Max Radius Edge Hole Binarize Threshold 2");
	m_listFixedCommonData.push_back(L"Min Radius Edge Binarize Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Binarize Threshold 2");
	m_listFixedCommonData.push_back(L"Max Radius Edge Binarize Threshold");
	m_listFixedCommonData.push_back(L"Max Radius Edge Binarize Threshold 2");
	m_listFixedCommonData.push_back(L"Min Radius Edge Profile Threshold");
	m_listFixedCommonData.push_back(L"Min Radius Edge Profile Threshold 2");
	m_listFixedCommonData.push_back(L"Max Radius Edge Profile Threshold");
	m_listFixedCommonData.push_back(L"Max Radius Edge Profile Threshold 2");

	m_listFixedCommonData.push_back(L"Surface Inspection");
	m_listFixedCommonData.push_back(L"Surface Min Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Surface Max Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Surface Block Count");
	m_listFixedCommonData.push_back(L"Surface Block Binarize Threshold");
	m_listFixedCommonData.push_back(L"Surface Block Variance Ratio");
	m_listFixedCommonData.push_back(L"Surface Block Variance Max");
	m_listFixedCommonData.push_back(L"Surface Block Variance Threshold");

	m_listFixedCommonData.push_back(L"Hole Min Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Hole Max Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Hole Block Count X");
	m_listFixedCommonData.push_back(L"Hole Block Count Y");
	m_listFixedCommonData.push_back(L"Hole Binarize Threshold");
	m_listFixedCommonData.push_back(L"Hole Min Size");
	m_listFixedCommonData.push_back(L"Hole Size Ratio");
	m_listFixedCommonData.push_back(L"Hole Small Size");
	m_listFixedCommonData.push_back(L"Hole Small Size Ratio");
	m_listFixedCommonData.push_back(L"Hole Big Size");
	m_listFixedCommonData.push_back(L"Hole Big Size Ratio");

	m_listFixedCommonData.push_back(L"Character Template Image Name");
	m_listFixedCommonData.push_back(L"Character Check");
	m_listFixedCommonData.push_back(L"Character Black");
	m_listFixedCommonData.push_back(L"Character Min Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Character Max Radius Edge Margin");
	m_listFixedCommonData.push_back(L"Character Connect Distance");
	m_listFixedCommonData.push_back(L"Center Position X");
	m_listFixedCommonData.push_back(L"Center Position Y");
	m_listFixedCommonData.push_back(L"Min Radius");
	m_listFixedCommonData.push_back(L"Max Radius");
}

bool CInspectionDlg::CheckFixedCommonData(wstring strName)
{
	auto it = find(m_listFixedCommonData.begin(), m_listFixedCommonData.end(), strName);
	if (it == m_listFixedCommonData.end()) {
		return false;
	}

	return true;
}


void CInspectionDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

bool CInspectionDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_NIP_JOB_FILE;

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		AddLogText(L"Fail to load Common Data File : %s", strJobFile.c_str());
		return false;
	}
	else {
		AddLogText(L"Success to load Common Data File : %s", strJobFile.c_str());
	}

	return true;
}

bool CInspectionDlg::SaveNIPJob()
{
	wstring strJobFile = VISION_NIP_JOB_FILE;

	NIPO *pNIPO = NIPO::GetInstance();
	if (pNIPO->SaveJob(strJobFile, &m_dJob) == FALSE) {
		AddLogText(L"Fail to save Common Data File Data : %s", strJobFile.c_str());
		return false;
	}
	else {
		AddLogText(L"Success to save Common Data File Data : %s", strJobFile.c_str());
	}

	return true;
}


void CInspectionDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImageFilePath), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImageFilePath));
		return;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImageFilePath));
	}

	m_dImg = dImg;

	ShowImage();
}

void CInspectionDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
}

void CInspectionDlg::LoadCompImage()
{
	CString strComp = GetCompName();

	m_strImageFilePath = m_strImageFolderPath;
	m_strImageFilePath += strComp;
	m_strImageFilePath += ".jpg";
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImageFilePath);

	LoadImage();
}

CString CInspectionDlg::GetCompName()
{
	CString strComp = L"";
	switch (m_nCompIndex) {
	case COMP_INNER: strComp = STR_MAGCORE_COMP_INNER; break;
	case COMP_LOWER: strComp = STR_MAGCORE_COMP_LOWER; break;
	case COMP_UPPER: strComp = STR_MAGCORE_COMP_UPPER; break;
	case COMP_OUTER: strComp = STR_MAGCORE_COMP_OUTER; break;
	}

	return strComp;
}

VIData_Comp *CInspectionDlg::GetVIData(int nCompIndex)
{
	VIData_Comp *pCompData = nullptr;
	switch (nCompIndex) {
	case COMP_INNER: pCompData = &m_dVI.m_dInner; break;
	case COMP_LOWER: pCompData = &m_dVI.m_dLower; break;
	case COMP_UPPER: pCompData = &m_dVI.m_dUpper; break;
	case COMP_OUTER: pCompData = &m_dVI.m_dOuter; break;
	}

	return pCompData;
}

void CInspectionDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	m_strImageInfo = szText;
	UpdateData(FALSE);
}


void CInspectionDlg::OnBnClickedOpenImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"jpg", LPCTSTR(m_strImageFilePath), OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	m_strImageFilePath = iFileDlg.GetPathName();

	OnBnClickedLoadImage();
}

void CInspectionDlg::OnBnClickedLoadImage()
{
	GetDlgItem(IDC_IMAGE_PATH)->GetWindowText(m_strImageFilePath);

	int nLength = m_strImageFilePath.GetLength();
	int nIndex = m_strImageFilePath.ReverseFind('\\');
	if (nIndex > 0) {
		CString strFileName = m_strImageFilePath.Right(nLength - nIndex - 1);
		if (strFileName.MakeLower().Find(L".jpg") > 0 || strFileName.MakeLower().Find(L".bmp") > 0) {
			m_strImageFolderPath = m_strImageFilePath.Left(nIndex + 1);

			LoadImage();
		}
		else {
			m_strImageFolderPath = m_strImageFilePath;
			if (m_strImageFolderPath[nLength - 1] != '\\') {
				m_strImageFolderPath += "\\";
			}

			LoadCompImage();
		}
	}
}

void CInspectionDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int nCount = DragQueryFile(hDropInfo, 0xffffffff, nullptr, 0);
	if (nCount > 1) {
		AddLogText(L"Cannot load multiple files.");
		return;
	}

	wchar_t szPath[1024];
	DragQueryFile(hDropInfo, 0, szPath, 1023);

	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(szPath);
	OnBnClickedLoadImage();

	CDialogEx::OnDropFiles(hDropInfo);
}

void CInspectionDlg::OnClickedCompRadio(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCompData();
	LoadCompImage();
}

void CInspectionDlg::ChangeCompData()
{
	UpdateData();
	UpdateCompData();
}

void CInspectionDlg::UpdateCompData()
{
	VIData_Comp *pCompData = GetVIData(m_nCompIndex);
	if (pCompData == nullptr) {
		return;
	}

	VIData_Inspection &dIns = pCompData->m_dIns;

	m_ctlCompData.RemoveAll();

	bool bValue;
	CMFCPropertyGridProperty* pParamGrid = nullptr;
	CMFCPropertyGridProperty* pPropGroup = new CMFCPropertyGridProperty(L"[Edge Inspection]");
	if (pPropGroup) {
		bValue = (dIns.m_strRadiusEdgeDoubleLine != L"0" && !dIns.m_strRadiusEdgeDoubleLine.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Radius Edge Double Line", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

	pPropGroup = new CMFCPropertyGridProperty(L"[Min Radius Edge Inspection]");
	if (pPropGroup) {
		bValue = (dIns.m_strMinRadiusEdgeCheck != L"0" && !dIns.m_strMinRadiusEdgeCheck.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Check", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		bValue = (dIns.m_strMinRadiusEdgeBlack != L"0" && !dIns.m_strMinRadiusEdgeBlack.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Black", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		bValue = (dIns.m_strMinRadiusEdgeInsideCheck != L"0" && !dIns.m_strMinRadiusEdgeInsideCheck.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Inside Check", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Inside Area Height", (_variant_t)dIns.m_strMinRadiusEdgeInsideAreaHeight.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Area Height", (_variant_t)dIns.m_strMinRadiusEdgeAreaHeight.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Inside Threshold", (_variant_t)dIns.m_strMinRadiusEdgeInsideThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Binarize Threshold", (_variant_t)dIns.m_strMinRadiusEdgeBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Spot Binarize Threshold", (_variant_t)dIns.m_strMinRadiusEdgeSpotBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Hole Binarize Threshold", (_variant_t)dIns.m_strMinRadiusEdgeHoleBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Profile Threshold", (_variant_t)dIns.m_strMinRadiusEdgeProfileThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Spot Profile Threshold", (_variant_t)dIns.m_strMinRadiusEdgeSpotProfileThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Binarize Threshold 2", (_variant_t)dIns.m_strMinRadiusEdgeBinarizeThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Hole Binarize Threshold 2", (_variant_t)dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Min Radius Edge Profile Threshold 2", (_variant_t)dIns.m_strMinRadiusEdgeProfileThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

	pPropGroup = new CMFCPropertyGridProperty(L"[Max Radius Edge Inspection]");
	if (pPropGroup) {
		bValue = (dIns.m_strMaxRadiusEdgeCheck != L"0" && !dIns.m_strMaxRadiusEdgeCheck.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Check", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		bValue = (dIns.m_strMaxRadiusEdgeBlack != L"0" && !dIns.m_strMaxRadiusEdgeBlack.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Black", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Area Height", (_variant_t)dIns.m_strMaxRadiusEdgeAreaHeight.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Binarize Threshold", (_variant_t)dIns.m_strMaxRadiusEdgeBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Spot Binarize Threshold", (_variant_t)dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Hole Binarize Threshold", (_variant_t)dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Profile Threshold", (_variant_t)dIns.m_strMaxRadiusEdgeProfileThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Spot Profile Threshold", (_variant_t)dIns.m_strMaxRadiusEdgeSpotProfileThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Binarize Threshold 2", (_variant_t)dIns.m_strMaxRadiusEdgeBinarizeThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Hole Binarize Threshold 2", (_variant_t)dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Max Radius Edge Profile Threshold 2", (_variant_t)dIns.m_strMaxRadiusEdgeProfileThreshold2.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

	pPropGroup = new CMFCPropertyGridProperty(L"[Surface Inspection]");
	if (pPropGroup) {
		pParamGrid = new CMFCPropertyGridProperty(L"Surface Min Radius Edge Margin", (_variant_t)dIns.m_strSurfaceMinRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Max Radius Edge Margin", (_variant_t)dIns.m_strSurfaceMaxRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Block Count", (_variant_t)dIns.m_strSurfaceBlockCount.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Block Binarize Threshold", (_variant_t)dIns.m_strSurfaceBlockBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Block Variance Ratio", (_variant_t)dIns.m_strSurfaceBlockVarianceRatio.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Block Variance Max", (_variant_t)dIns.m_strSurfaceBlockVarianceMax.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Surface Block Variance Threshold", (_variant_t)dIns.m_strSurfaceBlockVarianceThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

	pPropGroup = new CMFCPropertyGridProperty(L"[Hole Inspection]");
	if (pPropGroup) {
		pParamGrid = new CMFCPropertyGridProperty(L"Hole Min Radius Edge Margin", (_variant_t)dIns.m_strHoleMinRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Max Radius Edge Margin", (_variant_t)dIns.m_strHoleMaxRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Block Count X", (_variant_t)dIns.m_strHoleBlockCountX.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Block Count Y", (_variant_t)dIns.m_strHoleBlockCountY.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Binarize Threshold", (_variant_t)dIns.m_strHoleBinarizeThreshold.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Min Size", (_variant_t)dIns.m_strHoleMinSize.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Size Ratio", (_variant_t)dIns.m_strHoleSizeRatio.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Small Size", (_variant_t)dIns.m_strHoleSmallSize.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Small Size Ratio", (_variant_t)dIns.m_strHoleSmallSizeRatio.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Big Size", (_variant_t)dIns.m_strHoleBigSize.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Hole Big Size Ratio", (_variant_t)dIns.m_strHoleBigSizeRatio.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

	pPropGroup = new CMFCPropertyGridProperty(L"[Character Recognition]");
	if (pPropGroup) {
		bValue = (dIns.m_strCharCheck != L"0" && !dIns.m_strCharCheck.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Character Check", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		bValue = (dIns.m_strCharBlack != L"0" && !dIns.m_strCharBlack.empty());
		pParamGrid = new CMFCPropertyGridProperty(L"Character Black", (_variant_t)bValue, L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Character Min Radius Edge Margin", (_variant_t)dIns.m_strCharMinRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Character Max Radius Edge Margin", (_variant_t)dIns.m_strCharMaxRadiusEdgeMargin.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		pParamGrid = new CMFCPropertyGridProperty(L"Character Connect Distance", (_variant_t)dIns.m_strCharConnectDistance.c_str(), L"");
		if (pParamGrid) pPropGroup->AddSubItem(pParamGrid);

		m_ctlCompData.AddProperty(pPropGroup);
	}

}

void CInspectionDlg::ApplyCompData(bool bApplyAllCompData)
{
	UpdateData();

	if (bApplyAllCompData) {
		for (int i = 0; i < COMP_COUNT; i++) {
			ApplyCompData(i, i == (COMP_COUNT - 1));
		}
	}
	else {
		ApplyCompData(m_nCompIndex);
	}
}

void CInspectionDlg::ApplyCompData(int nCompIndex, bool bResetModifiedFlag)
{
	VIData_Comp *pCompData = GetVIData(nCompIndex);
	if (pCompData == nullptr) {
		return;
	}

	VIData_Inspection &dIns = pCompData->m_dIns;

	CMFCPropertyGridProperty* pPropGroup = nullptr;
	CMFCPropertyGridProperty* pParamGrid = nullptr;

	int nCount = m_ctlCompData.GetPropertyCount();
	for (int i = 0; i < nCount; i++) {
		pPropGroup = m_ctlCompData.GetProperty(i);
		if (pPropGroup == nullptr) {
			continue;
		}

		int nSubItemCount = pPropGroup->GetSubItemsCount();
		for (int j = 0; j < nSubItemCount; j++) {
			pParamGrid = pPropGroup->GetSubItem(j);
			if (pParamGrid == nullptr) {
				continue;
			}

			if (pParamGrid->IsModified() == false) {
				continue;
			}

			wstring strName = pParamGrid->GetName();
			if (CHECK_STRING(strName, L"Radius Edge Double Line")) {
				SetPropToParam(pParamGrid, dIns.m_strRadiusEdgeDoubleLine);
			}
			if (CHECK_STRING(strName, L"Min Radius Edge Inside Check")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeInsideCheck);
			}
			if (CHECK_STRING(strName, L"Min Radius Edge Check")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeCheck);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Check")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeCheck);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Black")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeBlack);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Black")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeBlack);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Inside Area Height")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeInsideAreaHeight);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Area Height")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeAreaHeight);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Area Height")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeAreaHeight);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Inside Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeInsideThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Spot Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeSpotBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Spot Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeSpotBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Spot Profile Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeSpotProfileThreshold);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Spot Profile Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeSpotProfileThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Hole Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeHoleBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Hole Binarize Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeHoleBinarizeThreshold2);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Hole Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Hole Binarize Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeHoleBinarizeThreshold2);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Binarize Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeBinarizeThreshold2);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Binarize Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeBinarizeThreshold2);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Profile Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeProfileThreshold);
			}
			else if (CHECK_STRING(strName, L"Min Radius Edge Profile Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMinRadiusEdgeProfileThreshold2);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Profile Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeProfileThreshold);
			}
			else if (CHECK_STRING(strName, L"Max Radius Edge Profile Threshold 2")) {
				SetPropToParam(pParamGrid, dIns.m_strMaxRadiusEdgeProfileThreshold2);
			}

			else if (CHECK_STRING(strName, L"Surface Min Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceMinRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Surface Max Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceMaxRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Surface Block Count")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceBlockCount);
			}
			else if (CHECK_STRING(strName, L"Surface Block Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceBlockBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Surface Block Variance Ratio")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceBlockVarianceRatio);
			}
			else if (CHECK_STRING(strName, L"Surface Block Variance Max")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceBlockVarianceMax);
			}
			else if (CHECK_STRING(strName, L"Surface Block Variance Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strSurfaceBlockVarianceThreshold);
			}

			else if (CHECK_STRING(strName, L"Hole Min Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleMinRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Hole Max Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleMaxRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Hole Block Count X")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleBlockCountX);
			}
			else if (CHECK_STRING(strName, L"Hole Block Count Y")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleBlockCountY);
			}
			else if (CHECK_STRING(strName, L"Hole Binarize Threshold")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleBinarizeThreshold);
			}
			else if (CHECK_STRING(strName, L"Hole Min Size")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleMinSize);
			}
			else if (CHECK_STRING(strName, L"Hole Size Ratio")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleSizeRatio);
			}
			else if (CHECK_STRING(strName, L"Hole Small Size")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleSmallSize);
			}
			else if (CHECK_STRING(strName, L"Hole Small Size Ratio")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleSmallSizeRatio);
			}
			else if (CHECK_STRING(strName, L"Hole Big Size")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleBigSize);
			}
			else if (CHECK_STRING(strName, L"Hole Big Size Ratio")) {
				SetPropToParam(pParamGrid, dIns.m_strHoleBigSizeRatio);
			}

			else if (CHECK_STRING(strName, L"Character Check")) {
				SetPropToParam(pParamGrid, dIns.m_strCharCheck);
			}
			else if (CHECK_STRING(strName, L"Character Black")) {
				SetPropToParam(pParamGrid, dIns.m_strCharBlack);
			}
			else if (CHECK_STRING(strName, L"Character Min Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strCharMinRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Character Max Radius Edge Margin")) {
				SetPropToParam(pParamGrid, dIns.m_strCharMaxRadiusEdgeMargin);
			}
			else if (CHECK_STRING(strName, L"Character Connect Distance")) {
				SetPropToParam(pParamGrid, dIns.m_strCharConnectDistance);
			}

			if (bResetModifiedFlag) {
				pParamGrid->SetOriginalValue(pParamGrid->GetValue());
				pParamGrid->ResetOriginalValue();
			}
		}
	}
}

void CInspectionDlg::SetPropToParam(CMFCPropertyGridProperty *pParamGrid, wstring &strValue)
{
	const COleVariant &dValue = pParamGrid->GetValue();

	switch (dValue.vt) {
	case VT_BOOL:
		if (dValue.boolVal == 0) strValue = L"0";
		else strValue = L"1";
		break;
	default:
		strValue = wstring((wchar_t *)dValue.pbstrVal);
	}
}

void CInspectionDlg::OnBnClickedShowOriginalImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadImage();
}

void CInspectionDlg::OnBnClickedInspectionTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyData();
	SaveData();

	LoadImage();
	DoVisionInspection();
}

void CInspectionDlg::DoVisionInspection()
{
	wstring strComp = GetCompName();

	AddLogText(L"Start Inspection, Comp : %s", strComp.c_str());

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dCalibrationImg;
	Mat dCharTemplateImg;

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
		strProcessName = L"Load Template Image";
		pProcess = m_dJob.FindProcess(strProcessName);
		if (pProcess) {
			NIPLParam_LoadTemplateImage *pParam = (NIPLParam_LoadTemplateImage *)pNIPO->SetNIPLParam(pProcess);

			wstring strTemplateImagePath = VISION_MODEL_SAMPLE_FOLDER;
			strTemplateImagePath += m_strModelId + L"\\";
			strTemplateImagePath += VISION_CHAR_TEMPLATE_FILE;

			pParam->m_strTemplateImagePath = strTemplateImagePath;

			NIPLInput dInput;
			NIPLOutput dOutput;

			dInput.m_pParam = pParam;
			NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				dCharTemplateImg = dOutput.m_dImg;
			}
		}
		else {
			AddLogText(L"  ! [%s] Cannot Load Template Image File.", strProcessName.c_str());
			return;
		}
	}

	// Calibration
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = strComp;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = m_dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			dCalibrationImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;

				dMinCircle = pResult->m_listCircle[0];
				dMaxCircle = pResult->m_listCircle[1];

				delete pResult;
			}
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Calibration. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
		return;
	}

	bool bDefect = false;
	strProcessName = L"MagCore Inspection";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = strComp;
		pParam->m_strShowImage = m_strShowImage;		// reset value with the string set in editor 

		// Set Character Template Image
		pParam->m_dCharTemplateImage = dCharTemplateImg;

		// Set Circle Position
		NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;
		dParam_Circle2Rect.m_nCenterPosX = dMinCircle.m_ptCenter.x;
		dParam_Circle2Rect.m_nCenterPosY = dMinCircle.m_ptCenter.y;
		dParam_Circle2Rect.m_nMinRadius = dMinCircle.m_nRadius;
		dParam_Circle2Rect.m_nMaxRadius = dMaxCircle.m_nRadius;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dCalibrationImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			// convert image to color image
			cvtColor(dOutput.m_dImg, m_dImg, CV_GRAY2BGR);

			if (dOutput.m_pResult != nullptr) {
				bDefect = true;

				Scalar dColor;
				NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)dOutput.m_pResult;
				for (auto &dDefect : pResult->m_listDefect) {
					switch (dDefect.m_nType) {
//					case NIPLDefect_MagCore::DEFECT_TYPE_SCRETCH: dColor = CV_RGB(255, 0, 0); break;
					case NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER: dColor = CV_RGB(0, 0, 255); break;
//					case NIPLDefect_MagCore::DEFECT_TYPE_EDGE: dColor = CV_RGB(255, 0, 0); break;
					case NIPLDefect_MagCore::DEFECT_TYPE_SURFACE: dColor = CV_RGB(0, 255, 0); break;
					case NIPLDefect_MagCore::DEFECT_TYPE_BLOCK: dColor = CV_RGB(255, 255, 0); break;
					default: dColor = CV_RGB(255, 0, 0); break;
					}

					rectangle(m_dImg, dDefect.m_rcBoundingBox, dColor, 1);
				}

				delete pResult;
			}
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Inspection. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Perform Inspection.", strProcessName.c_str());
		return;
	}

	if (bDefect) {
		AddLogText(L"End Vision Inspection, Found Defect.");
	}
	else {
		AddLogText(L"End Vision Inspection, No Defect");
	}

	ShowImage();
}

void CInspectionDlg::SetDataCtrlFont()
{
	::DeleteObject(m_fntData.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntData.CreateFontIndirect(&lf);

	m_ctlCommonData.SetFont(&m_fntData);
	m_ctlCompData.SetFont(&m_fntData);
}

void CInspectionDlg::SaveCommonData()
{
	SaveNIPJob();
}

void CInspectionDlg::LoadCommonData()
{
	LoadNIPJob();

	m_ctlCommonData.RemoveAll();

	wstring strProcessName = L"MagCore Inspection";
	NIPJobProcess *pProcess = m_dJob.FindProcess(strProcessName);

	if (pProcess == nullptr) {
		m_ctlCommonData.Invalidate(TRUE);
		AddLogText(L"Cannot load Common Data. No Inspection Process in NIPJob Data.");
		return;
	}

	// Force to restore Show Image for debugging
	NIPJobParam *pParam = pProcess->FindParam(L"Show Image");
	if (pParam) {
		pParam->m_strValue = m_strShowImage;
	}

	AddCommonDataGroup(pProcess);
}

void CInspectionDlg::AddCommonDataGroup(NIPJobProcess *pProcess, CMFCPropertyGridProperty *pParentPropGroup)
{
	for (auto pParam : pProcess->m_listParam) {
		if (pParam->m_bGroup) {
			// if it's the "Input / Output" param group of subprocess, skip it..
			if (pProcess->m_bSubProcess && pParam->m_strName == STR_PARAM_GROUP_INPUT_OUTPUT) {
				continue;
			}

			AddCommonDataGroup(pParam, pParentPropGroup);
		}
		else {
			AddCommonData(pParam, pParentPropGroup);
		}
	}

	for (auto pSubProcess : pProcess->m_listSubProcess) {
		wstring strPropGroupName = L"[";
		strPropGroupName += pSubProcess->m_strName + L"]";
		CMFCPropertyGridProperty* pPropGroup = new CMFCPropertyGridProperty(strPropGroupName.c_str());

		AddCommonDataGroup(pSubProcess, pPropGroup);

		if (pParentPropGroup == nullptr) {
			m_ctlCommonData.AddProperty(pPropGroup);
		}
		else {
			pParentPropGroup->AddSubItem(pPropGroup);
		}

		if (CheckFixedCommonData(strPropGroupName)) {
			pPropGroup->Show(FALSE);
		}
	}
}

void CInspectionDlg::AddCommonDataGroup(NIPJobParam *pGroup, CMFCPropertyGridProperty *pParentPropGroup)
{
	if (pGroup == nullptr) {
		return;
	}

	wstring strPropGroupName = pGroup->m_strName;
	CMFCPropertyGridProperty* pPropGroup = new CMFCPropertyGridProperty(strPropGroupName.c_str());

	for (auto pParam : pGroup->m_listParam) {
		if (pParam->m_bGroup) {
			AddCommonDataGroup(pParam, pPropGroup);
		}
		else {
			AddCommonData(pParam, pPropGroup);
		}
	}

	if (pParentPropGroup == nullptr) {
		m_ctlCommonData.AddProperty(pPropGroup);
	}
	else {
		pParentPropGroup->AddSubItem(pPropGroup);
	}

	if (CheckFixedCommonData(strPropGroupName)) {
		pPropGroup->Show(FALSE);
	}
}

void CInspectionDlg::AddCommonData(NIPJobParam *pParam, CMFCPropertyGridProperty *pPropGroup)
{
	if (pParam == nullptr) {
		return;
	}

	wstring strName = pParam->m_strName;
	wstring strValue = pParam->m_strValue;
	wstring strMin = pParam->m_strMin;
	wstring strMax = pParam->m_strMax;
	wstring strDesc = pParam->m_strDesc;

	CMFCPropertyGridProperty* pParamGrid = nullptr;

	if (pParam->m_nType == NIPJobParam::MPT_INT || pParam->m_nType == NIPJobParam::MPT_FLOAT || pParam->m_nType == NIPJobParam::MPT_STRING) {
		const wchar_t *pValue = strValue.c_str();
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)pValue, strDesc.c_str());
	}
	else if (pParam->m_nType == NIPJobParam::MPT_BOOL) {
		bool bValue = (strValue != L"false");
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)bValue, strDesc.c_str());
	}
	else if (pParam->m_nType == NIPJobParam::MPT_COMBO) {
		const wchar_t *pValue = strValue.c_str();
		pParamGrid = new CMFCPropertyGridProperty(strName.c_str(), (_variant_t)pValue, strDesc.c_str());
		for (auto strComboValue : pParam->m_listComboValue) {
			pParamGrid->AddOption(strComboValue.c_str());
		}
		pParamGrid->AllowEdit(FALSE);
	}
	else {
		return;
	}

	if (pPropGroup == nullptr) {
		m_ctlCommonData.AddProperty(pParamGrid);
	}
	else {
		pPropGroup->AddSubItem(pParamGrid);
	}

	if (CheckFixedCommonData(strName)) {
		pParamGrid->Show(FALSE);
	}
}

void CInspectionDlg::ApplyCommonData()
{
	wstring strProcessName = L"MagCore Inspection";
	NIPJobProcess *pProcess = m_dJob.FindProcess(strProcessName);

	if (pProcess == nullptr) {
		m_ctlCommonData.Invalidate(TRUE);
		AddLogText(L"Cannot apply Common Data. No Inspection Process in NIPJob Data.");
		return;
	}

	pProcess->Clear(true);

	// set enable true as default
	pProcess->m_bEnable = true;

	NIPJobParam *pParam = nullptr;

	int nCount = m_ctlCommonData.GetPropertyCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pProp = m_ctlCommonData.GetProperty(i);
		if (pProp == nullptr) {
			continue;
		}

		if (pProp->IsGroup()) {
			wstring strName = pProp->GetName();
			if (strName[0] == '[') {		// Sub Processs
				SetPropGroupToSubProcess(pProp, pProcess);
			}
			else {
				SetPropGroupToParamGroup(pProp, pProcess, true);
			}
		}
		else {
			SetPropToParam(pProp, (void *)pProcess, true);
		}
	}

	// Force to set Show Image to 'Output'
	pParam = pProcess->FindParam(L"Show Image");
	if (pParam) {
		m_strShowImage = pParam->m_strValue;
		pParam->m_strValue = L"Output";
	}
}

void CInspectionDlg::SetPropGroupToSubProcess(CMFCPropertyGridProperty *pPropGroup, NIPJobProcess *pProcess)
{
	if (pPropGroup == nullptr || pProcess == nullptr) {
		return;
	}

	wstring strName = pPropGroup->GetName();
	size_t nLength = strName.size();
	strName = strName.substr(1, nLength - 2);	// remove '[' and ']'

	NIPJobProcess *pSubProcess = pProcess->AddSubProcess(strName);
	if (pSubProcess == nullptr) {
		return;
	}

	int nCount = pPropGroup->GetSubItemsCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pProp = pPropGroup->GetSubItem(i);
		if (pProp == nullptr) {
			continue;
		}

		if (pProp->IsGroup()) {
			wstring strName = pProp->GetName();
			if (strName[0] == '[') {			// Nested Sub Process
				SetPropGroupToSubProcess(pProp, pSubProcess);
			}
			else {
				SetPropGroupToParamGroup(pProp, pSubProcess, true);
			}
		}
		else {
			SetPropToParam(pProp, (void *)pSubProcess, true);
		}
	}
}

void CInspectionDlg::SetPropGroupToParamGroup(CMFCPropertyGridProperty *pPropGroup, void *pParent, bool bProcess)
{
	if (pPropGroup == nullptr || pParent == nullptr) {
		return;
	}

	NIPJobParam *pGroup;
	if (bProcess) {
		pGroup = ((NIPJobProcess *)pParent)->AddParamGroup(pPropGroup->GetName());
	}
	else {
		pGroup = ((NIPJobParam *)pParent)->AddParamGroup(pPropGroup->GetName());
	}

	int nCount = pPropGroup->GetSubItemsCount();
	for (int i = 0; i < nCount; i++) {
		CMFCPropertyGridProperty *pParamGrid = pPropGroup->GetSubItem(i);
		if (pParamGrid == nullptr) {
			continue;
		}

		if (pParamGrid->IsGroup()) {
			SetPropGroupToParamGroup(pParamGrid, (void *)pGroup, false);
		}
		else {
			SetPropToParam(pParamGrid, (void *)pGroup, false);
		}
	}
}

void CInspectionDlg::SetPropToParam(CMFCPropertyGridProperty *pParamGrid, void *pParent, bool bProcess)
{
	if (pParamGrid == nullptr) {
		return;
	}

	const COleVariant &dValue = pParamGrid->GetValue();
	wstring strValue = L"";
	wstring strType = L"";
	int nOptionCount = pParamGrid->GetOptionCount();
	switch (dValue.vt) {
	case VT_I4:
	case VT_INT:
		strValue = to_wstring(dValue.intVal);
		strType = L"int";
		break;
	case VT_R4:
		strValue = to_wstring(dValue.fltVal);
		strType = L"float";
		break;
	case VT_BOOL:
		if (dValue.boolVal == 0) strValue = L"false";
		else strValue = L"true";
		strType = L"bool";
		break;
	default:
		strValue = wstring((wchar_t *)dValue.pbstrVal);
		if (nOptionCount > 0) {
			strType = L"combo";
		}
		else {
			strType = L"string";
		}
		break;
	}

	NIPJobParam *pNewParam = nullptr;
	if (bProcess) {
		pNewParam = ((NIPJobProcess *)pParent)->AddParam(pParamGrid->GetName(), strValue, strType);
	}
	else {
		pNewParam = ((NIPJobParam *)pParent)->AddParam(pParamGrid->GetName(), strValue, strType);
	}

	if (pNewParam) {
		CMFCPropertyGridPropertyEx *pParamGridEx = static_cast<CMFCPropertyGridPropertyEx *>(pParamGrid);
		if (pParamGridEx) {
			if (pParamGridEx->GetMaxValue() != 0) {
				wstring strMin = to_wstring(pParamGridEx->GetMinValue());
				pNewParam->m_strMin = strMin;

				wstring strMax = to_wstring(pParamGridEx->GetMaxValue());
				pNewParam->m_strMax = strMax;
			}
		}

		wstring strDesc(LPCTSTR(pParamGrid->GetDescription()));
		pNewParam->m_strDesc = strDesc;

		if (nOptionCount > 0) {
			for (int i = 0; i < nOptionCount; i++) {
				pNewParam->AddComboValue(pParamGrid->GetOption(i));
			}
		}
	}

	pParamGrid->SetOriginalValue(dValue);
	pParamGrid->ResetOriginalValue();
}

void CInspectionDlg::OnBnClickedDataApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyData();
}

void CInspectionDlg::OnBnClickedDataApplyAll()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyData(true);
}

void CInspectionDlg::OnBnClickedSaveData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyData();
	SaveData();
}

void CInspectionDlg::ApplyData(bool bApplyAllCompData)
{
	ApplyCommonData();
	ApplyCompData(bApplyAllCompData);
}

void CInspectionDlg::LoadData()
{
	LoadCommonData();
	LoadCompData();
}

void CInspectionDlg::SaveData()
{
	SaveCommonData();
	SaveCompData();
}

void CInspectionDlg::LoadCompData()
{
	m_strVIDataPath = VISION_FOLDER;
	m_strVIDataPath += m_strModelId;
	m_strVIDataPath += L".vid";

	if (!m_dVI.LoadData(wstring(m_strVIDataPath))) {
		AddLogText(L"Fail to load Component Data File : %s", LPCTSTR(m_strVIDataPath));
		return;
	}
	else {
		AddLogText(L"Success to load Component Data File : %s", LPCTSTR(m_strVIDataPath));
	}

	ChangeCompData();
}

void CInspectionDlg::SaveCompData()
{
	UpdateData();

	if (!m_dVI.SaveData(wstring(m_strVIDataPath))) {
		AddLogText(L"Fail to save VI Data : %s", LPCTSTR(m_strVIDataPath));
		return;
	}
	else {
		AddLogText(L"Success to save VI Data : %s", LPCTSTR(m_strVIDataPath));
	}
}

void CInspectionDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialogEx::OnSetFocus(pOldWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	m_wndImg.SetFocus();
}

LRESULT CInspectionDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));

	return 0;
}

void CInspectionDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
		else if (nDepth == CV_32F) {
			Vec3f dPixel = m_dImg.at<Vec3f>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%.1f, %.1f, %.1f", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	m_strImagePos = szText;
	UpdateData(FALSE);
}


void CInspectionDlg::OnBnClickedReloadModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadData();
}

void CInspectionDlg::OnBnClickedCovertColor()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	LoadImage();

	DoImageCalibration_ConvertColor();
}

void CInspectionDlg::DoImageCalibration_ConvertColor()
{
	AddLogText(L"Do Image Calibration");

	NIPO *pNIPO = NIPO::GetInstance();

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	// Calibration
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
			return;
		}

		pParam->m_strDataPath = m_strVIDataPath;
		pParam->m_strComp = GetCompName();
		pParam->m_strShowImage = STR_SHOW_IMAGE_COLOR_CONVERT;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = m_dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			m_dImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindEllipse *pResult = (NIPLResult_FindEllipse *)dOutput.m_pResult;
				NIPLEllipse dMinCircle = pResult->m_listEllipse[0];
				NIPLEllipse dMaxCircle = pResult->m_listEllipse[1];

				m_wndImg.ClearImageMark();

				ImageMark dMark;
				dMark.m_bCircle = true;
				dMark.m_rcBoundingBox = dMinCircle.GetBoundingBox();
				m_wndImg.AddImageMark(dMark);

				dMark.Clear();
				dMark.m_bCircle = true;
				dMark.m_rcBoundingBox = dMaxCircle.GetBoundingBox();
				m_wndImg.AddImageMark(dMark);

				m_wndImg.ShowImageMark();

				delete pResult;
			}

			ShowImage();
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Calibration. Error Code : %d", strProcessName.c_str(), nErr);
			return;
		}
	}
	else {
		AddLogText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
		return;
	}

	AddLogText(L"Success to do Image Calibration");
}


void CInspectionDlg::OnBnClickedBackupModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SaveData();

	wstring strDataFile = VISION_NIP_JOB_FILE;
	wstring strBackupFile = strDataFile + L"_backup";

	if (CopyFile(strDataFile.c_str(), strBackupFile.c_str(), FALSE)) {
		AddLogText(L"Success to backup Common Data File : %s", strBackupFile.c_str());
	}
	else {
		AddLogText(L"Fail to backup Common Data File : %s", strBackupFile.c_str());
	}

	strDataFile = m_strVIDataPath;
	strBackupFile = strDataFile + L"_backup";

	if (CopyFile(strDataFile.c_str(), strBackupFile.c_str(), FALSE)) {
		AddLogText(L"Success to backup Component Data File : %s", strBackupFile.c_str());
	}
	else {
		AddLogText(L"Fail to backup Component Data File : %s", strBackupFile.c_str());
	}
}


void CInspectionDlg::OnBnClickedRestoreModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strMessage = L"Are you sure to restore data ?\nWarning : All current data can be changed and cannot be recovered.";
	int nRet = AfxMessageBox(strMessage, MB_OKCANCEL);
	if (nRet != IDOK) {
		return;
	}

	wstring strDataFile = VISION_NIP_JOB_FILE;
	wstring strBackupFile = strDataFile + L"_backup";

	if (CopyFile(strBackupFile.c_str(), strDataFile.c_str(), FALSE)) {
		AddLogText(L"Success to restore Common Data File : %s", strDataFile.c_str());
	}
	else {
		AddLogText(L"Fail to restore Common Data File : %s", strDataFile.c_str());
	}

	strDataFile = m_strVIDataPath;
	strBackupFile = strDataFile + L"_backup";

	if (CopyFile(strBackupFile.c_str(), strDataFile.c_str(), FALSE)) {
		AddLogText(L"Success to restore Component Data File : %s", strDataFile.c_str());
	}
	else {
		AddLogText(L"Fail to restore Component Data File : %s", strDataFile.c_str());
	}

	LoadData();
}
