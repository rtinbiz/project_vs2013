#pragma once
#include "afxpropertygridctrl.h"

#include "VIData.h"

#define STR_PARAM_GROUP_INPUT_OUTPUT L"Input / Output"

class CMFCPropertyGridCtrlEx : public CMFCPropertyGridCtrl
{
public :
	CMFCPropertyGridCtrlEx() {
		m_nLeftColumnWidth = 250;
	}
};


class CMFCPropertyGridPropertyEx : public CMFCPropertyGridProperty
{
public:
	int GetMinValue() { return m_nMinValue; }
	int GetMaxValue() { return m_nMaxValue; }
};

// CInspectionDlg 대화 상자입니다.

class CInspectionDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CInspectionDlg)

private :
	HICON m_hIcon;
	Mat m_dImg;
	NIPLImageCtl m_wndImg;

	NIPJob m_dJob;
	VIData m_dVI;

	CFont m_fntData;

	vector<wstring> m_listFixedCommonData;

public :
	CRect m_rcPos;
	CString m_strImageFolderPath;
	CString m_strImageFilePath;
	CString m_strVIDataPath;
	wstring m_strShowImage;

public:
	CInspectionDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CInspectionDlg();

	void GetWindowPos(CRect &rc);
	bool LoadNIPJob();
	bool SaveNIPJob();
	void ShowImage();
	void LoadCompImage();
	void LoadImage();
	void ShowImageInfo();
	void AddLogText(const wchar_t* szText, ...);
	CString GetCompName();
	void ChangeCompData();
	void UpdateCompData();
	VIData_Comp *GetVIData(int nCompIndex);
	void DoVisionInspection();
	void ShowImagePosValue(CPoint ptImagePos);

	void SetDataCtrlFont();

	void LoadCommonData();
	void SaveCommonData();
	void AddCommonDataGroup(NIPJobProcess *pProcess, CMFCPropertyGridProperty *pParentPropGroup = nullptr);
	void AddCommonDataGroup(NIPJobParam *pGroup, CMFCPropertyGridProperty *pParentPropGroup = nullptr);
	void AddCommonData(NIPJobParam *pParam, CMFCPropertyGridProperty *pGroup = nullptr);

	void ApplyCommonData();
	void SetPropGroupToSubProcess(CMFCPropertyGridProperty *pPropGroup, NIPJobProcess *pProcess);
	void SetPropGroupToParamGroup(CMFCPropertyGridProperty *pPropGroup, void *pParent, bool bProcess);
	void SetPropToParam(CMFCPropertyGridProperty *pParamGrid, void *pParent, bool bProcess);
	void SetPropToParam(CMFCPropertyGridProperty *pParamGrid, wstring &strValue);

	void ApplyData(bool bApplyAllCompData = false);
	void LoadData();
	void SaveData();

	void LoadCompData();
	void SaveCompData();
	void ApplyCompData(bool bApplyAllCompData);
	void ApplyCompData(int nCompIndex, bool bResetModifiedFlag = true);

	void SetFixedCommonData();
	bool CheckFixedCommonData(wstring strName);

	void AddCompData(wstring strName, wstring strValue, int nType, CMFCPropertyGridProperty *pPropGroup);
	void DoImageCalibration_ConvertColor();

public :
	CListBox m_ctlLogList;
	CStatic m_ctlImageArea;
	CString m_strImageInfo;
	CString m_strImagePos;
	CString m_strModelId;
	int m_nCompIndex;


// 대화 상자 데이터입니다.
	enum { IDD = IDD_INSPECTION_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CMFCPropertyGridCtrlEx m_ctlCommonData;
	CMFCPropertyGridCtrlEx m_ctlCompData;

	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	virtual void OnOK();
	afx_msg void OnClose();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOpenImage();
	afx_msg void OnBnClickedLoadImage();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnClickedCompRadio(UINT nID);
	afx_msg void OnBnClickedShowOriginalImage();
	afx_msg void OnBnClickedInspectionTest();
	afx_msg void OnBnClickedDataApply();
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedReloadModelData();
	afx_msg void OnBnClickedDataApplyAll();
	afx_msg void OnBnClickedCovertColor();
	afx_msg void OnBnClickedBackupModelData();
	afx_msg void OnBnClickedRestoreModelData();
};
