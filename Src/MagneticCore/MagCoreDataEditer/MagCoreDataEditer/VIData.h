#pragma once

#include "Xml/xml3.h"
using namespace XML3;

#define STR_MODEL L"Model"
#define STR_CALIBRATION L"Calibration"
#define STR_INSPECTION L"Inspection"

#define STR_MAGCORE_COMP_UPPER L"Upper"
#define STR_MAGCORE_COMP_LOWER L"Lower"
#define STR_MAGCORE_COMP_INNER L"Inner"
#define STR_MAGCORE_COMP_OUTER L"Outer"

#define STR_VIDATA_VERSION "1.0"

enum COMP_INDEX {
	COMP_INNER = 0,
	COMP_UPPER,
	COMP_OUTER,
	COMP_LOWER,
	COMP_COUNT,
	COMP_ALL = COMP_COUNT
};

struct VIData_Model {
	wstring m_strId;

	VIData_Model() {
		clear();
	}

	void clear() {
		m_strId.clear();
	}
};

struct VIData_Calibration {
	int m_nColor2GrayChannel;
	int m_nBrightnessEnhancementLevel;

	float m_nCheckCoreThreshold;

	int m_nCenterPosX;
	int m_nCenterPosY;
	int m_nMinRadiusPosX;
	int m_nMaxRadiusPosX;
	int m_nMinRadiusRange;
	int m_nMaxRadiusRange;
	int m_nMinRadiusMargin;
	int m_nMaxRadiusMargin;

	bool m_bMinRadiusReverseEdgeDetect;
	bool m_bMaxRadiusReverseEdgeDetect;
	bool m_bMinRadiusBinarize;
	bool m_bMaxRadiusBinarize;
	bool m_bMinRadiusBlack;
	bool m_bMaxRadiusBlack;

	VIData_Calibration() {
		clear();
	}

	void clear() {
		m_nColor2GrayChannel = 0;
		m_nBrightnessEnhancementLevel = 0;

		m_nCheckCoreThreshold = 0.f;

		m_nCenterPosX = 0;
		m_nCenterPosY = 0;
		m_nMinRadiusPosX = 0;
		m_nMaxRadiusPosX = 0;
		m_nMinRadiusRange = 0;
		m_nMaxRadiusRange = 0;
		m_nMinRadiusMargin = 0;
		m_nMaxRadiusMargin = 0;

		m_bMinRadiusReverseEdgeDetect = false;
		m_bMaxRadiusReverseEdgeDetect = false;
		m_bMinRadiusBinarize = false;
		m_bMaxRadiusBinarize = false;
		m_bMinRadiusBlack = false;
		m_bMaxRadiusBlack = false;
	}

/*
	bool operator<(const VIData_ROI& dROI)
	{
		if (m_nCompIndex != dROI.m_nCompIndex) {
			return (m_nCompIndex < dROI.m_nCompIndex);
		}
		if (m_nSetId != dROI.m_nSetId) {
			return (m_nSetId < dROI.m_nSetId);
		}

		switch (m_nCompIndex) {
		case COMP_TERMINAL:
			if (m_nCenterY != dROI.m_nCenterY) {
				return (m_nCenterY < dROI.m_nCenterY);
			}
			if (m_nCenterX != dROI.m_nCenterX) {
				return (m_nCenterX < dROI.m_nCenterX);
			}
			if (m_nRadius != dROI.m_nRadius) {
				return (m_nRadius < dROI.m_nRadius);
			}
			break;
		default:
			if (m_nStartY != dROI.m_nStartY) {
				return (m_nStartY < dROI.m_nStartY);
			}
			if (m_nEndY != dROI.m_nEndY) {
				return (m_nEndY < dROI.m_nEndY);
			}
			if (m_nStartX != dROI.m_nStartX) {
				return (m_nStartX < dROI.m_nStartX);
			}
			if (m_nEndX != dROI.m_nEndX) {
				return (m_nEndX < dROI.m_nEndX);
			}
		}

		return false;
	}
*/
};

struct VIData_Inspection {
	wstring m_strRadiusEdgeDoubleLine;
	wstring m_strMinRadiusEdgeInsideCheck;
	wstring m_strMinRadiusEdgeCheck;
	wstring m_strMaxRadiusEdgeCheck;
	wstring m_strMinRadiusEdgeBlack;
	wstring m_strMaxRadiusEdgeBlack;
	wstring m_strMinRadiusEdgeInsideAreaHeight;
	wstring m_strMinRadiusEdgeAreaHeight;
	wstring m_strMaxRadiusEdgeAreaHeight;
	wstring m_strMinRadiusEdgeInsideThreshold;
	wstring m_strMinRadiusEdgeSpotBinarizeThreshold;
	wstring m_strMaxRadiusEdgeSpotBinarizeThreshold;
	wstring m_strMinRadiusEdgeSpotProfileThreshold;
	wstring m_strMaxRadiusEdgeSpotProfileThreshold;
	wstring m_strMinRadiusEdgeHoleBinarizeThreshold;
	wstring m_strMinRadiusEdgeHoleBinarizeThreshold2;
	wstring m_strMaxRadiusEdgeHoleBinarizeThreshold;
	wstring m_strMaxRadiusEdgeHoleBinarizeThreshold2;
	wstring m_strMinRadiusEdgeBinarizeThreshold;
	wstring m_strMinRadiusEdgeBinarizeThreshold2;
	wstring m_strMaxRadiusEdgeBinarizeThreshold;
	wstring m_strMaxRadiusEdgeBinarizeThreshold2;
	wstring m_strMinRadiusEdgeProfileThreshold;
	wstring m_strMinRadiusEdgeProfileThreshold2;
	wstring m_strMaxRadiusEdgeProfileThreshold;
	wstring m_strMaxRadiusEdgeProfileThreshold2;

	wstring m_strCharCheck;
	wstring m_strCharBlack;
	wstring m_strCharMinRadiusEdgeMargin;
	wstring m_strCharMaxRadiusEdgeMargin;
	wstring m_strCharConnectDistance;

	wstring m_strSurfaceMinRadiusEdgeMargin;
	wstring m_strSurfaceMaxRadiusEdgeMargin;
	wstring m_strSurfaceBlockCount;
	wstring m_strSurfaceBlockBinarizeThreshold;
	wstring m_strSurfaceBlockVarianceRatio;
	wstring m_strSurfaceBlockVarianceMax;
	wstring m_strSurfaceBlockVarianceThreshold;

	wstring m_strHoleMinRadiusEdgeMargin;
	wstring m_strHoleMaxRadiusEdgeMargin;
	wstring m_strHoleBlockCountX;
	wstring m_strHoleBlockCountY;
	wstring m_strHoleBinarizeThreshold;
	wstring m_strHoleMinSize;
	wstring m_strHoleSizeRatio;
	wstring m_strHoleSmallSize;
	wstring m_strHoleSmallSizeRatio;
	wstring m_strHoleBigSize;
	wstring m_strHoleBigSizeRatio;

	VIData_Inspection() {
		clear();
	}

	void clear() {
		m_strRadiusEdgeDoubleLine.clear();
		m_strMinRadiusEdgeInsideCheck.clear();
		m_strMinRadiusEdgeCheck.clear();
		m_strMaxRadiusEdgeCheck.clear();
		m_strMinRadiusEdgeBlack.clear();
		m_strMaxRadiusEdgeBlack.clear();
		m_strMinRadiusEdgeInsideAreaHeight.clear();
		m_strMinRadiusEdgeAreaHeight.clear();
		m_strMaxRadiusEdgeAreaHeight.clear();
		m_strMinRadiusEdgeInsideThreshold.clear();
		m_strMinRadiusEdgeSpotBinarizeThreshold.clear();
		m_strMaxRadiusEdgeSpotBinarizeThreshold.clear();
		m_strMinRadiusEdgeSpotProfileThreshold.clear();
		m_strMaxRadiusEdgeSpotProfileThreshold.clear();
		m_strMinRadiusEdgeHoleBinarizeThreshold.clear();
		m_strMinRadiusEdgeHoleBinarizeThreshold2.clear();
		m_strMaxRadiusEdgeHoleBinarizeThreshold.clear();
		m_strMaxRadiusEdgeHoleBinarizeThreshold2.clear();
		m_strMinRadiusEdgeBinarizeThreshold.clear();
		m_strMinRadiusEdgeBinarizeThreshold2.clear();
		m_strMaxRadiusEdgeBinarizeThreshold.clear();
		m_strMaxRadiusEdgeBinarizeThreshold2.clear();
		m_strMinRadiusEdgeProfileThreshold.clear();
		m_strMinRadiusEdgeProfileThreshold2.clear();
		m_strMaxRadiusEdgeProfileThreshold.clear();
		m_strMaxRadiusEdgeProfileThreshold2.clear();

		m_strCharCheck.clear();
		m_strCharBlack.clear();
		m_strCharMinRadiusEdgeMargin.clear();
		m_strCharMaxRadiusEdgeMargin.clear();
		m_strCharConnectDistance.clear();

		m_strSurfaceMinRadiusEdgeMargin.clear();
		m_strSurfaceMaxRadiusEdgeMargin.clear();
		m_strSurfaceBlockCount.clear();
		m_strSurfaceBlockBinarizeThreshold.clear();
		m_strSurfaceBlockVarianceMax.clear();
		m_strSurfaceBlockVarianceThreshold.clear();
		m_strSurfaceBlockVarianceRatio.clear();

		m_strHoleMinRadiusEdgeMargin.clear();
		m_strHoleMaxRadiusEdgeMargin.clear();
		m_strHoleBlockCountX.clear();
		m_strHoleBlockCountY.clear();
		m_strHoleBinarizeThreshold.clear();
		m_strHoleMinSize.clear();
		m_strHoleSizeRatio.clear();
		m_strHoleSmallSize.clear();
		m_strHoleSmallSizeRatio.clear();
		m_strHoleBigSize.clear();
		m_strHoleBigSizeRatio.clear();
	}

	/*
	bool operator<(const VIData_ROI& dROI)
	{
	if (m_nCompIndex != dROI.m_nCompIndex) {
	return (m_nCompIndex < dROI.m_nCompIndex);
	}
	if (m_nSetId != dROI.m_nSetId) {
	return (m_nSetId < dROI.m_nSetId);
	}

	switch (m_nCompIndex) {
	case COMP_TERMINAL:
	if (m_nCenterY != dROI.m_nCenterY) {
	return (m_nCenterY < dROI.m_nCenterY);
	}
	if (m_nCenterX != dROI.m_nCenterX) {
	return (m_nCenterX < dROI.m_nCenterX);
	}
	if (m_nRadius != dROI.m_nRadius) {
	return (m_nRadius < dROI.m_nRadius);
	}
	break;
	default:
	if (m_nStartY != dROI.m_nStartY) {
	return (m_nStartY < dROI.m_nStartY);
	}
	if (m_nEndY != dROI.m_nEndY) {
	return (m_nEndY < dROI.m_nEndY);
	}
	if (m_nStartX != dROI.m_nStartX) {
	return (m_nStartX < dROI.m_nStartX);
	}
	if (m_nEndX != dROI.m_nEndX) {
	return (m_nEndX < dROI.m_nEndX);
	}
	}

	return false;
	}
	*/
};

struct VIData_Comp {
	VIData_Calibration m_dCal;
	VIData_Inspection m_dIns;

	VIData_Comp() {
		clear();
	}

	void clear() {
		m_dCal.clear();
		m_dIns.clear();
	}
};

class VIData {
public :
	VIData_Model m_dModel;
	VIData_Comp m_dInner;
	VIData_Comp m_dLower;
	VIData_Comp m_dUpper;
	VIData_Comp m_dOuter;

	VIData() {
	}

	virtual ~VIData() {
		Clear();
	}

	void Clear() {
		m_dModel.clear();
		m_dInner.clear();
		m_dLower.clear();
		m_dUpper.clear();
		m_dOuter.clear();
	}

	bool LoadData(wstring strDataPath);
	bool SaveData(wstring strDataPath);

private :
	void _SetXMLData(wstring strElementName, XMLElement &dXMLRoot);
};