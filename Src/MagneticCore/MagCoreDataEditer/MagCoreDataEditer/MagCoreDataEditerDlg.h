
// MagCoreDataEditerDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "VIData.h"
#include "CharTemplateDlg.h"
#include "CopyModelDlg.h"
#include "InspectionDlg.h"

// CMagCoreDataEditerDlg dialog
class CMagCoreDataEditerDlg : public CDialogEx
{
// Construction
public:
	CMagCoreDataEditerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MAGCOREDATAEDITER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	NIPLImageCtl m_wndImg;
	Mat m_dImg;
	CString m_strModel;

	NIPJob m_dJob;
	VIData m_dVI;
	CString m_strVIDataPath;

	CString m_strImageFolderPath;
	CString m_strImageFilePath;

public:
	void ShowImage();
	void LoadImage();
	void AddLogText(const wchar_t* szText, ...);
	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	LRESULT OnImagePosDblClick(WPARAM wParam, LPARAM lParam);
	void ShowImagePosValue(CPoint ptImagePos);
	void ShowImageInfo();
	void LoadModelList();
	void ChangeModel();
	bool LoadNIPJob();

	void DoImageCalibration(wstring strShowImage);

	void LoadVIData();
	void SaveVIData();
	void ChangeCompData();
	int GetSingleSelectCompDataIndex();
	void UpdateCompData();
	void ApplyCompData();
	VIData_Comp *GetVIData(int nCompIndex);
	void SetControlDataValue(CPoint ptImagePos);
	CWnd *GetFocusControl();
	void LoadCompImage();
	void ShowPosBox();
	void ClearPosBox();
	CString GetCompName();
	bool FindModel(CString strModel);
	bool CanChangeControlDataValue(CPoint ptImagePos, int nTargetPosX);

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	int m_nDataCenterPosX;
	int m_nDataCenterPosY;
	int m_nDataMinRadiusPosX;
	int m_nDataMaxRadiusPosX;
	int m_nDataMinRadiusRange;
	int m_nDataMaxRadiusRange;
	int m_nDataMinRadiusMargin;
	int m_nDataMaxRadiusMargin;
	BOOL m_bDataMinRadiusReverseEdgeDetect;
	BOOL m_bDataMaxRadiusReverseEdgeDetect;
	BOOL m_bDataMinRadiusBinarize;
	BOOL m_bDataMaxRadiusBinarize;
	BOOL m_bDataMinRadiusBlack;
	BOOL m_bDataMaxRadiusBlack;

	CStatic m_ctlImageArea;
	CListBox m_ctlLogList;
	CString m_strImagePos;
	CString m_strImageInfo;
	CListBox m_ctlModelList;
	CString m_strModelId;
	int m_nCompIndex;
	CButton m_ctlDataApply;

	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedOpenImage();
	afx_msg void OnBnClickedLoadImage();
	afx_msg void OnSelchangeModelList();
	afx_msg void OnClickedCompRadio(UINT nID);
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnBnClickedShowOriginalImage();
	afx_msg void OnBnClickedShowCalibrationImage();
	afx_msg void OnBnClickedShowCalibrationImageWithMargin();
	afx_msg void OnBnClickedDataApply();
	afx_msg void OnBnClickedReloadModelData();
	afx_msg void OnBnClickedSaveImageAsSample();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnBnClickedEditCharTemplate();
	afx_msg void OnBnClickedCopyModelData();
	afx_msg void OnBnClickedDeleteModelData();
	afx_msg void OnBnClickedEditInspectionData();
	CComboBox m_ctlColor2GrayChannel;
	int m_nDataColor2GrayChannel;
	int m_nDataBrightnessEnhancementLevel;
	float m_nDataCheckCoreThreshold;
	afx_msg void OnBnClickedCovertColor();
	afx_msg void OnBnClickedShowCalibrationResultImage();
};
