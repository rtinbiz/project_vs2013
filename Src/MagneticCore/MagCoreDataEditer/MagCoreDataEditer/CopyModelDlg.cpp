// CopyModelDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MagCoreDataEditer.h"
#include "CopyModelDlg.h"
#include "afxdialogex.h"


// CCopyModelDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCopyModelDlg, CDialogEx)

CCopyModelDlg::CCopyModelDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCopyModelDlg::IDD, pParent)
{

}

CCopyModelDlg::~CCopyModelDlg()
{
}

void CCopyModelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MODEL_ID, m_strModelId);
}


BEGIN_MESSAGE_MAP(CCopyModelDlg, CDialogEx)
END_MESSAGE_MAP()


// CCopyModelDlg 메시지 처리기입니다.


BOOL CCopyModelDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
