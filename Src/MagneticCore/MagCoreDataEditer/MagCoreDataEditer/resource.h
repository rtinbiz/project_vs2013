//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MagCoreDataEditer.rc에서 사용되고 있습니다.
//
#define IDD_COPY_MODEL_DIALOG           5
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAGCOREDATAEDITER_DIALOG    102
#define IDD_CHAR_TEMPLATE_DIALOG        103
#define IDD_INSPECTION_DIALOG           104
#define IDR_MAINFRAME                   128
#define IDC_IMAGE_AREA                  1000
#define IDC_TEMPLATE_AREA               1001
#define IDC_OPEN_IMAGE                  1002
#define IDC_IMAG_PATH                   1003
#define IDC_IMAGE_PATH                  1003
#define IDC_LOAD_IMAGE                  1004
#define IDC_LOG_LIST                    1005
#define IDC_IMAGE_POS                   1006
#define IDC_IMAGE_INFO                  1007
#define IDC_MODEL_LIST                  1008
#define IDC_TEMPLATE_INFO               1008
#define IDC_COMP_INNER                  1010
#define IDC_COMP_LOWER                  1011
#define IDC_COMP_UPPER                  1012
#define IDC_COMP_OUTER                  1013
#define IDC_MODEL_ID                    1018
#define IDC_DATA_CENTER_POS_RANGE       1019
#define IDC_DATA_MIN_RADIUS_RANGE       1019
#define IDC_DATA_MIN_RADIUS_CHECK_EDGE_LINE 1020
#define IDC_DATA_MAX_RADIUS_RANGE       1020
#define IDC_DATA_CENTER_POS_X           1021
#define IDC_DATA_CENTER_POS_Y           1022
#define IDC_DATA_MIN_RADIUS_POS_X       1023
#define IDC_SUBIMAGE_START_POS_X        1024
#define IDC_DATA_BRIGHTNESS_ENHANCEMENT_LEVEL 1024
#define IDC_SUBIMAGE_START_POS_Y        1025
#define IDC_DATA_CHECK_CORE_THRESHOLD   1025
#define IDC_DATA_MAX_RADIUS_POS_X       1026
#define IDC_DATA_MIN_RADIUS_MARGIN      1027
#define IDC_DATA_MAX_RADIUS_MARGIN      1028
#define IDC_DATA_MIN_RADIUS_REVERSE_EDGE_DETECT 1029
#define IDC_DATA_MAX_RADIUS_CHECK_EDGE_LINE 1030
#define IDC_DATA_CHAR_CHECK             1031
#define IDC_DATA_CHECK_BLACK            1032
#define IDC_DATA_SURFACE_BLOCK_THRESHOLD 1033
#define IDC_DATA_SURFACE_BLOCK_LINE_THRESHOLD 1034
#define IDC_SHOW_CALIBRATION_IMAGE      1035
#define IDC_DATA_MAX_RADIUS_REVERSE_EDGE_DETECT 1036
#define IDC_DATA_MIN_RADIUS_BINARIZE    1037
#define IDC_SAVE_DATA                   1038
#define IDC_TEST_IMAGE                  1039
#define IDC_SUBIMAGE_END_POS_X          1039
#define IDC_DATA_MAX_RADIUS_BINARIZE    1040
#define IDC_DATA_MIN_RADIUS_BLACK       1041
#define IDC_DATA_MAX_RADIUS_BLACK       1042
#define IDC_SHOW_ORIGINAL_IMAGE         1043
#define IDC_SHOW_CALIBRATION_IMAGE_WITH_MARGIN 1044
#define IDC_INSPECTION_TEST             1045
#define IDC_SHOW_CALIBRATION_RESULT_IMAGE 1045
#define IDC_RELOAD_MODEL_DATA           1046
#define IDC_SUBIMAGE_START_POS_Y2       1046
#define IDC_SUBIMAGE_END_POS_Y          1046
#define IDC_SHOW_CALIBRATION_IMAGE2     1047
#define IDC_DATA_APPLY                  1047
#define IDC_SHOW_HISEQ_IMAGE            1048
#define IDC_BACKUP_MODEL_DATA           1048
#define IDC_SAVE_IMAGE_AS_SAMPLE        1049
#define IDC_DATA_APPLY_ALL              1049
#define IDC_EDIT_CHAR_TEMPLATE          1050
#define IDC_AUTO_DETECT                 1050
#define IDC_RESTORE_MODEL_DATA          1050
#define IDC_TEMPLATE_SIZE_LEFT_UP       1051
#define IDC_TEMPLATE_SIZE_LEFT_DOWN     1052
#define IDC_TEMPLATE_SIZE_RIGHT_DOWN    1053
#define IDC_TEMPLATE_SIZE_RIGHT_UP      1054
#define IDC_TEMPLATE_SIZE_TOP_DOWN      1055
#define IDC_TEMPLATE_SIZE_TOP_UP        1056
#define IDC_TEMPLATE_SIZE_BOTTOM_UP     1057
#define IDC_TEMPLATE_SIZE_BOTTOM_DOWN   1058
#define IDC_TEMPLATE_ROTATE_DOWN        1059
#define IDC_TEMPLATE_ROTATE_UP          1060
#define IDC_BUTTON3                     1061
#define IDC_TEMPLATE_ROTATE_180         1061
#define IDC_SAVE_TEMPLATE               1062
#define IDC_SET_TEMPLATE                1063
#define IDC_TEMPLATE_SIZE_RESET         1064
#define IDC_DELETE_MODEL_DATA           1065
#define IDC_RELOAD_TEMPLATE             1065
#define IDC_COPY_MODEL_DATA             1066
#define IDC_AUTO_FIT                    1066
#define IDC_COMMON_DATA                 1069
#define IDC_COMP_DATA                   1070
#define IDC_EDIT_INSPECTION_DATA        1070
#define IDC_COMBO_COLOR_TO_GRAY_CHANNEL 1071
#define IDC_COVERT_COLOR                1072
#define IDC_IMAGE_CTRL                  2001
#define IDC_TEMPLATE_CTRL               2002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1073
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
