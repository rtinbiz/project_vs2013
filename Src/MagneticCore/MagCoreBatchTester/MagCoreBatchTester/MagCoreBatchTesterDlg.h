
// MagCoreBatchTesterDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#define VISION_FOLDER L"D:\\nBizSystem\\Vision\\"
#define VISION_MODEL_SAMPLE_FOLDER L"D:\\nBizSystem\\Vision\\ModelSample\\"

struct ImageData {
	int m_nIndex;
	wstring m_strModel;
	wstring m_strType;
	wstring m_strFolderPath;
	wstring m_strResultManual;
	wstring m_strFileName;

	ImageData() {
		m_strModel.clear();
		m_strType.clear();
		m_strFolderPath.clear();
		m_strResultManual.clear();
		m_strFileName.clear();
	}
};

struct ResultData {
	wstring m_strModel;
	wstring m_strType;
	wstring m_strFileIndex;
	wstring m_strResultPath;

	wstring m_strResult_Inner;
	wstring m_strResult_Lower;
	wstring m_strResult_Upper;
	wstring m_strResult_Outer;

	wstring m_strResult_Center_Inner;
	wstring m_strResult_Center_Lower;
	wstring m_strResult_Center_Upper;
	wstring m_strResult_Center_Outer;

	wstring m_strResult_Final;
	wstring m_strResult_Manual;
	wstring m_strResult_Compare;

	ResultData() {
		m_strModel.clear();
		m_strType.clear();
		m_strFileIndex.clear();
		m_strResultPath.clear();

		m_strResult_Inner.clear();
		m_strResult_Lower.clear();
		m_strResult_Upper.clear();
		m_strResult_Outer.clear();

		m_strResult_Center_Inner.clear();
		m_strResult_Center_Lower.clear();
		m_strResult_Center_Upper.clear();
		m_strResult_Center_Outer.clear();

		m_strResult_Final.clear();
		m_strResult_Manual.clear();
		m_strResult_Compare.clear();
	}
};

struct ResultCenterData {
	Point2f m_ptCenterMean;
	Point2f m_ptCenterStd;
	Point2f m_ptCenterMax;

	wstring m_strCenterMean;
	wstring m_strCenterStd;
	wstring m_strCenterMax;
};


// CMagCoreBatchTesterDlg 대화 상자
class CMagCoreBatchTesterDlg : public CDialogEx
{
// 생성입니다.
public:
	CMagCoreBatchTesterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_MAGCOREBATCHTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	wstring m_strResultPath;

	CString m_strDataFile;
	CListBox m_ctlResultList;

	int m_nIndex_Inner;
	int m_nIndex_Lower;
	int m_nIndex_Upper;
	int m_nIndex_Outer;

	bool m_bCheckCalibrationOnly;
	bool m_bSaveCenterData;

	bool m_bStopProcess;
	int m_nErrorCount;
	int m_nDefectCount;
	int m_nResultNGCount;

	vector<ImageData> m_listData;
	CCriticalSection m_csData;
	map<wstring, ResultData> m_listResult;
	CCriticalSection m_csResult;

	vector<Point> m_listCenter_Inner;
	vector<Point> m_listCenter_Lower;
	vector<Point> m_listCenter_Upper;
	vector<Point> m_listCenter_Outer;

	ResultCenterData m_dResultCenter_Inner;
	ResultCenterData m_dResultCenter_Lower;
	ResultCenterData m_dResultCenter_Upper;
	ResultCenterData m_dResultCenter_Outer;

	NIPJob m_dJob;

	DWORD m_nInspectionStartTime;
	DWORD m_nInspectionProcessTime;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void ProcessDataFile();
	void ForceToAddResultText(const wchar_t* szText, ...);
	void AddResultText(const wchar_t* szText, ...);
	void AddResultText(wstring strText);
	bool LoadBatchDataFile();
	bool CheckImageDataFile();
	bool LoadNIPJob();
	void DoVisionInspection(wstring strFilePath, wstring strModel, wstring strType, wstring strResultManual, wstring strFileIndex, wstring strComp);
	bool DoVisionInspection(wstring strFilePath, wstring strModel, wstring strType, wstring strFileIndex, wstring strComp, Point &ptCenter, bool &bCalibrationError, bool &bDefect);
	void SetResultData(wstring strModel, wstring strType, wstring strResultManual, wstring strFileIndex, wstring strComp, Point ptCenter, bool bSuccess, bool bCalibrationError, bool bDefect);
	bool SaveResultFile();
	bool CheckFileExist(wstring strFilePath);
	void SetProcessText(const wchar_t* szText, ...);
	void SetResultCenterData(wstring strComp);

	afx_msg void OnBnClickedStartBatchTest();
	afx_msg void OnBnClickedClearResult();
	afx_msg void OnBnClickedStopBatchTest();
	CButton m_ctlStopBatchTest;
	CButton m_ctlStartBatchTest;
	afx_msg void OnClose();
	afx_msg void OnBnClickedOpenResultFoler();
	afx_msg void OnBnClickedSaveResultFile();
	CString m_strProcess;

	static void ProcessThread(CMagCoreBatchTesterDlg *pThis, bool *pbFinished);
	void ProcessData();
	bool PopImageData(ImageData &dData);
	BOOL m_bNoResultDisplay;
	BOOL m_bNoSaveImageFile;
	int m_nProcessJobCount;
	afx_msg void OnBnClickedCheckNoDisplayResult();
	afx_msg void OnBnClickedCheckNoSaveImageFile();
};
