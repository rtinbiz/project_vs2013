
// MagCoreBatchTesterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "MagCoreBatchTester.h"
#include "MagCoreBatchTesterDlg.h"
#include "afxdialogex.h"
#include <thread>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMagCoreBatchTesterDlg 대화 상자

CMagCoreBatchTesterDlg::CMagCoreBatchTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMagCoreBatchTesterDlg::IDD, pParent)
	, m_strProcess(_T(""))
	, m_bNoResultDisplay(TRUE)
	, m_bNoSaveImageFile(FALSE)
	, m_nProcessJobCount(16)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_strDataFile = "D:\\nBizSystem\\Vision\\BatchTest_0414.nib";
//	m_strDataFile = "D:\\nBizSystem\\Vision\\BatchTest_0314.nib";
//	m_strDataFile = "D:\\nBizSystem\\Vision\\BatchTest_0303.nib";
//	m_strDataFile = "D:\\nBizSystem\\Vision\\BatchTest_0118.nib";
}

void CMagCoreBatchTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DATA_FILE, m_strDataFile);
	DDX_Control(pDX, IDC_RESULT_LIST, m_ctlResultList);
	DDX_Control(pDX, IDC_STOP_BATCH_TEST, m_ctlStopBatchTest);
	DDX_Control(pDX, IDC_START_BATCH_TEST, m_ctlStartBatchTest);
	DDX_Text(pDX, IDC_PROCESS, m_strProcess);
	DDX_Check(pDX, IDC_CHECK_NO_DISPLAY_RESULT, m_bNoResultDisplay);
	DDX_Check(pDX, IDC_CHECK_NO_SAVE_IMAGE_FILE, m_bNoSaveImageFile);
	DDX_Text(pDX, IDC_EDIT_PROCESS_JOB_COUNT, m_nProcessJobCount);
	DDV_MinMaxInt(pDX, m_nProcessJobCount, 1, 32);
}

BEGIN_MESSAGE_MAP(CMagCoreBatchTesterDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_START_BATCH_TEST, &CMagCoreBatchTesterDlg::OnBnClickedStartBatchTest)
	ON_BN_CLICKED(IDC_CLEAR_RESULT, &CMagCoreBatchTesterDlg::OnBnClickedClearResult)
	ON_BN_CLICKED(IDC_STOP_BATCH_TEST, &CMagCoreBatchTesterDlg::OnBnClickedStopBatchTest)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_OPEN_RESULT_FOLER, &CMagCoreBatchTesterDlg::OnBnClickedOpenResultFoler)
	ON_BN_CLICKED(IDC_SAVE_RESULT_FILE, &CMagCoreBatchTesterDlg::OnBnClickedSaveResultFile)
	ON_BN_CLICKED(IDC_CHECK_NO_DISPLAY_RESULT, &CMagCoreBatchTesterDlg::OnBnClickedCheckNoDisplayResult)
	ON_BN_CLICKED(IDC_CHECK_NO_SAVE_IMAGE_FILE, &CMagCoreBatchTesterDlg::OnBnClickedCheckNoSaveImageFile)
END_MESSAGE_MAP()


// CMagCoreBatchTesterDlg 메시지 처리기

BOOL CMagCoreBatchTesterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_ctlStartBatchTest.ShowWindow(SW_SHOW);
	m_ctlStopBatchTest.ShowWindow(SW_HIDE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMagCoreBatchTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMagCoreBatchTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMagCoreBatchTesterDlg::OnBnClickedStartBatchTest()
{
	UpdateData();

	m_listData.clear();
	m_listResult.clear();

	m_listCenter_Inner.clear();
	m_listCenter_Lower.clear();
	m_listCenter_Upper.clear();
	m_listCenter_Outer.clear();

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!LoadBatchDataFile()) {
		ForceToAddResultText(L"Cannot Load Batch Data File");
		return;
	}

	if (!CheckImageDataFile()) {
		ForceToAddResultText(L"Cannot Check Image Data Files");
		return;
	}

	if (!LoadNIPJob()) {
		ForceToAddResultText(L"Cannot Load NIPJob Data File");
		return;
	}

	ProcessDataFile();

	SetResultCenterData(STR_MAGCORE_COMP_INNER);
	SetResultCenterData(STR_MAGCORE_COMP_LOWER);
	SetResultCenterData(STR_MAGCORE_COMP_UPPER);
	SetResultCenterData(STR_MAGCORE_COMP_OUTER);

	if (!m_bCheckCalibrationOnly) {
		if (!SaveResultFile()) {
			AddResultText(L"Cannot Save Result File");
			return;
		}
	}

	AddResultText(L"Finished.");
}

void CMagCoreBatchTesterDlg::OnBnClickedStopBatchTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStopProcess = true;
}

bool CMagCoreBatchTesterDlg::LoadBatchDataFile()
{
	XML dXML;
	auto nResult = dXML.Load(LPCTSTR(m_strDataFile));
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	wstring strModel;
	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, L"Model")) {
			strModel = str2wstr(pData->v("id"));
			ForceToAddResultText(L"Mode : %s", strModel.c_str());
		}
		else if (CHECK_STRING(strElementName, L"Setting")) {
			m_nIndex_Inner = stoi(pData->v("inner"));
			m_nIndex_Upper = stoi(pData->v("upper"));
			m_nIndex_Outer = stoi(pData->v("outer"));
			m_nIndex_Lower = stoi(pData->v("lower"));
			m_strResultPath = str2wstr(pData->v("result"));

			try { m_bCheckCalibrationOnly = (stoi(pData->v("check_calibration_only")) == 1) ? true : false; }
			catch (...) { m_bCheckCalibrationOnly = false; }

			try { m_bSaveCenterData = (stoi(pData->v("save_center_data")) == 1) ? true : false; }
			catch (...) { m_bSaveCenterData = false; }

			AddResultText(L"Index, Inner : %d, Lower : %d, Upper : %d, Outer : %d", m_nIndex_Inner, m_nIndex_Lower, m_nIndex_Upper, m_nIndex_Outer);
			AddResultText(L"Check Calibration Only : %d", (int)m_bCheckCalibrationOnly);
			ForceToAddResultText(L"Result Path : %s", m_strResultPath.c_str());
		}
		else if (CHECK_STRING(strElementName, L"Data")) {
			ImageData dImageData;
			try { dImageData.m_strModel = str2wstr(pData->v("model")); }
			catch (...) { dImageData.m_strModel = strModel; }

			dImageData.m_strType = str2wstr(pData->v("type"));
			dImageData.m_strResultManual = str2wstr(pData->v("result"));
			dImageData.m_strFolderPath = str2wstr(pData->v("path"));

			m_listData.push_back(dImageData);

			ForceToAddResultText(L"Data, Model : %s, Type : %s, Result : %s, Path : %s", dImageData.m_strModel.c_str(), dImageData.m_strType.c_str(), dImageData.m_strResultManual.c_str(), dImageData.m_strFolderPath.c_str());
		}
	}

	return true;
}

bool CMagCoreBatchTesterDlg::CheckImageDataFile()
{
	if (m_listData.size() == 0) {
		return false;
	}

	vector<ImageData> listDataFoler(m_listData);

	m_listData.clear();
	for (auto &dData : listDataFoler) {
		wstring strModel = dData.m_strModel;
		wstring strType = dData.m_strType;
		wstring strResultManual = dData.m_strResultManual;
		wstring strFolderPath = dData.m_strFolderPath;

		WIN32_FIND_DATA dFindData;
		wstring strFormat = strFolderPath + L"\\*.jpg";
		HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		int nFileCount = 0;
		if (hFile != INVALID_HANDLE_VALUE) {
			bool bFirstFile = true;
			int nIndex = 0;
			hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
			do {
				ImageData dImageData;
				dImageData.m_nIndex = nFileCount;
				dImageData.m_strModel = strModel;
				dImageData.m_strType = strType;
				dImageData.m_strResultManual = strResultManual;
				dImageData.m_strFolderPath = strFolderPath;
				dImageData.m_strFileName = dFindData.cFileName;

				m_listData.push_back(dImageData);
				nFileCount++;
			} while (::FindNextFile(hFile, &dFindData));

			::FindClose(hFile);
		}

		AddResultText(L"Files in %s : %d", strFolderPath.c_str(), nFileCount);
	}

	AddResultText(L"Total Files : %d", m_listData.size());
	return true;
}

void CMagCoreBatchTesterDlg::ProcessDataFile()
{
	SetProcessText(L"");

	int nCount = (int)m_listData.size();
	if (nCount == 0) {
		ForceToAddResultText(L"No Data File to be processed.");
		return;
	}

	ForceToAddResultText(L"Start Processing!! Totla Data File Count : %d", nCount);

	m_ctlStartBatchTest.ShowWindow(SW_HIDE);
	m_ctlStopBatchTest.ShowWindow(SW_SHOW);

	m_bStopProcess = false;
	m_nErrorCount = 0;
	m_nDefectCount = 0;
	m_nResultNGCount = 0;

	thread *listProcessThread = new thread[m_nProcessJobCount];
	bool *listProcessThreadFinished = new bool[m_nProcessJobCount];
	for (int i = 0; i < m_nProcessJobCount; i++) {
		listProcessThreadFinished[i] = false;
		listProcessThread[i] = thread(ProcessThread, this, &(listProcessThreadFinished[i]));
		listProcessThread[i].detach();
	}

	DWORD nStartTime = ::GetTickCount();
	DWORD nElapsedTime = 0;
	float nAverageCount = 0.f;
	float nAverageTime = 0.f;
	DWORD nLeftTime = 0;
	int nDoneCount;
	int nLeftCount;

	bool bFinished;
	do {
		bFinished = true;
		for (int i = 0; i < m_nProcessJobCount; i++) {
			if (!listProcessThreadFinished[i]) {
				bFinished = false;
				break;
			}
		}

		nLeftCount = m_listData.size();
		nDoneCount = nCount - nLeftCount;
		nElapsedTime = ::GetTickCount() - nStartTime;

		if (m_listResult.size() == 0) {
			SetProcessText(L"[%d/%d (%.0f%%), %02d:%02d] Start Processing ...",
				nDoneCount, nCount, (nDoneCount * 100.f) / nCount, (nElapsedTime / 1000) / 60, (nElapsedTime / 1000) % 60);
		}
		else {
			nAverageTime = ((float)nElapsedTime) / nDoneCount;
			nLeftTime = nLeftCount * nAverageTime;
			nAverageCount = nDoneCount / (nElapsedTime / 1000.f);

			SetProcessText(L"[%d/%d (%.0f%%), %02d:%02d (%02d:%02d Left)] [Avg : %.0f ms/data, %.1f data/s] Error : %d, Defect : %d, NG : %d / %d (%.1f%%), Total NG : %d / %d (%.1f%%)",
				nDoneCount, nCount, (nDoneCount * 100.f) / nCount, (nElapsedTime / 1000) / 60, (nElapsedTime / 1000) % 60, (nLeftTime / 1000) / 60, (nLeftTime / 1000) % 60,
				nAverageTime, nAverageCount, m_nErrorCount, m_nDefectCount, m_nResultNGCount, m_listResult.size(), (m_nResultNGCount * 100.f) / m_listResult.size(), m_nResultNGCount, nCount / 4, (m_nResultNGCount * 100.f) / (nCount / 4));
		}

		// Message Loop to check push stop process
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		Sleep(500);
	} while (!bFinished);

	nLeftCount = m_listData.size();
	nDoneCount = nCount - nLeftCount;
	nAverageTime = ((float)nElapsedTime) / nDoneCount;
	nAverageCount = nDoneCount / (nElapsedTime / 1000.f);

	SetProcessText(L"[%d/%d (%.0f%%), %02d:%02d (%s)] [Avg : %.0f ms/data, %.1f data/s] Error : %d, Defect : %d, NG : %d / %d (%.1f%%), Total NG : %d / %d (%.1f%%)",
		nDoneCount, nCount, (nDoneCount * 100.f) / nCount, (nElapsedTime / 1000) / 60, (nElapsedTime / 1000) % 60, m_bStopProcess ? L"Stopped" : L"Finished",
		nAverageTime, nAverageCount, m_nErrorCount, m_nDefectCount, m_nResultNGCount, m_listResult.size(), (m_nResultNGCount * 100.f) / m_listResult.size(), m_nResultNGCount, nCount / 4, (m_nResultNGCount * 100.f) / (nCount / 4));

	delete[] listProcessThread;
	delete[] listProcessThreadFinished;

	ForceToAddResultText(L"Finish Processing!! Error Count : %d, Defect Count : %d", m_nErrorCount, m_nDefectCount);
	ForceToAddResultText(L"Total Result Data Count : %d", m_listResult.size());

	m_ctlStartBatchTest.ShowWindow(SW_SHOW);
	m_ctlStopBatchTest.ShowWindow(SW_HIDE);
}

void CMagCoreBatchTesterDlg::ProcessThread(CMagCoreBatchTesterDlg *pThis, bool *pbFinished)
{
	pThis->ProcessData();

	*pbFinished = true;
}

void CMagCoreBatchTesterDlg::ProcessData()
{
	ImageData dData;
	while (!m_bStopProcess && PopImageData(dData)) {
		int nIndex = dData.m_nIndex;
		wstring strModel = dData.m_strModel;
		wstring strType = dData.m_strType;
		wstring strResultManual = dData.m_strResultManual;
		wstring strFolderPath = dData.m_strFolderPath;
		wstring strFileName = dData.m_strFileName;
		wstring strFilePath = strFolderPath + L"\\" + strFileName;

		AddResultText(L"Process Data (%d) : Type : %s, File : %s", nIndex, strType.c_str(), strFilePath.c_str());

		size_t nStartOffset = 0;
		size_t nEndOffset = nStartOffset;

		// File Index
		nEndOffset = strFileName.find('_', nStartOffset);
		if (nEndOffset == wstring::npos) {
			nIndex++;
			m_nErrorCount++;
			continue;
		}

		wstring strFileIndex = strFileName.substr(nStartOffset, nEndOffset - nStartOffset);
		nStartOffset = nEndOffset + 1;

		// TimeStamp : Skip
		nEndOffset = strFileName.find('_', nStartOffset);
		if (nEndOffset == wstring::npos) {
			nIndex++;
			m_nErrorCount++;
			continue;
		}
		nStartOffset = nEndOffset + 1;

		// Image Index
		nEndOffset = strFileName.find('.', nStartOffset);
		if (nEndOffset == wstring::npos) {
			nIndex++;
			m_nErrorCount++;

			continue;
		}
		wstring strImageIndex = strFileName.substr(nStartOffset, nEndOffset - nStartOffset);
		int nImageIndex = stoi(strImageIndex);

//		AddResultText(L"  > File Index : %s, Image Index : %d", strFileIndex.c_str(), nImageIndex);

		wstring strComp;
		if (nImageIndex == m_nIndex_Inner) strComp = STR_MAGCORE_COMP_INNER;
		else if (nImageIndex == m_nIndex_Upper) strComp = STR_MAGCORE_COMP_UPPER;
		else if (nImageIndex == m_nIndex_Outer) strComp = STR_MAGCORE_COMP_OUTER;
		else if (nImageIndex == m_nIndex_Lower) strComp = STR_MAGCORE_COMP_LOWER;

		DoVisionInspection(strFilePath, strModel, strType, strResultManual, strFileIndex, strComp);
	}
}

bool CMagCoreBatchTesterDlg::PopImageData(ImageData &dData)
{
	bool bSuccess = false;
	m_csData.Lock();
	if (!m_listData.empty()) {
		dData = m_listData.back();
		m_listData.pop_back();

		bSuccess = true;
	}
	m_csData.Unlock();

	return bSuccess;
}

bool CMagCoreBatchTesterDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_FOLDER;
	strJobFile += L"VI_MagCore.nip";

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		return false;
	}

	AddResultText(L"NIPJob Data File : %s", strJobFile.c_str());

	return true;
}

void CMagCoreBatchTesterDlg::DoVisionInspection(wstring strFilePath, wstring strModel, wstring strType, wstring strResultManual, wstring strFileIndex, wstring strComp)
{
	bool bCalibrationError = false;
	bool bDefect = false;
	Point ptCenter;

	bool bSuccess = false;
	try {
		bSuccess = DoVisionInspection(strFilePath, strModel, strType, strFileIndex, strComp, ptCenter, bCalibrationError, bDefect);
	}
	catch (...){
		ForceToAddResultText(L"Exception (%s) : %s", strComp.c_str(), strFilePath.c_str());
	}

	if (!bSuccess) {
		m_nErrorCount++;
	}
	if (bDefect) {
		m_nDefectCount++;
	}

	SetResultData(strModel, strType, strResultManual, strFileIndex, strComp, ptCenter, bSuccess, bCalibrationError, bDefect);
}

bool CMagCoreBatchTesterDlg::DoVisionInspection(wstring strFilePath, wstring strModel, wstring strType, wstring strFileIndex, wstring strComp, Point &ptCenter, bool &bCalibrationError, bool &bDefect)
{
//	AddResultText(L"  > Start Vision Inspection, Comp : %s", strComp.c_str());

	bCalibrationError = false;

	NIPO *pNIPO = NIPO::GetInstance();
	Mat dImg;
	if (!pNIPO->LoadImage(strFilePath, dImg)) {
		AddResultText(L"  ! Cannot Load Image File");
		return false;
	}

	wstring strVIDFile = VISION_FOLDER + strModel + L".vid";
	if (!CheckFileExist(strVIDFile)) {
		AddResultText(L"  ! Cannot Load VI Data File");
		return false;
	}

	wstring strResultFolderPath = m_strResultPath + L"\\" + strModel + L"\\" + strType + L"\\" + strFileIndex;
	wstring strDefectFilePath = strResultFolderPath + L"\\" + strComp + L"_Defect.jpg";
	if (!m_bNoSaveImageFile) {
		if (!m_bCheckCalibrationOnly) {
			SHCreateDirectoryEx(NULL, strResultFolderPath.c_str(), NULL);
		}

		wstring strOrigFilePath = strResultFolderPath + L"\\" + strComp + L".jpg";
		if (!m_bCheckCalibrationOnly) {
			CopyFile(strFilePath.c_str(), strOrigFilePath.c_str(), FALSE);
		}

		DeleteFile(strDefectFilePath.c_str());	// delete defect image file that was saved previously.
	}

	Mat dCalibrationImg;
	Mat dCharTemplateImg;
	Mat dDefectImg;

	// Load Character Template Image
	wstring strProcessName;
	NIPJobProcess *pProcess;

	m_nInspectionStartTime = ::GetTickCount();

	if (!m_bCheckCalibrationOnly && CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
		strProcessName = L"Load Template Image";
		pProcess = m_dJob.FindProcess(strProcessName);
		if (pProcess) {
			NIPLParam_LoadTemplateImage *pParam = (NIPLParam_LoadTemplateImage *)pNIPO->SetNIPLParam(pProcess);

			wstring strTemplateImagePath = VISION_MODEL_SAMPLE_FOLDER;
			strTemplateImagePath += strModel + L"\\";
			strTemplateImagePath += L"CharTemplate.bmp";

			pParam->m_strTemplateImagePath = strTemplateImagePath;

			NIPLInput dInput;
			NIPLOutput dOutput;

			dInput.m_pParam = pParam;
			NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				dCharTemplateImg = dOutput.m_dImg;
			}
		}
		else {
			AddResultText(L"  ! [%s] Cannot Load Template Image File.", strProcessName.c_str());
			m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
			return false;
		}
	}

	// Calibration
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddResultText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
			bCalibrationError = true;
			m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
			return false;
		}

		pParam->m_strDataPath = strVIDFile;
		pParam->m_strComp = strComp;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			dCalibrationImg = dOutput.m_dImg;

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;

				dMinCircle = pResult->m_listCircle[0];
				dMaxCircle = pResult->m_listCircle[1];

				ptCenter = dMinCircle.m_ptCenter;

				delete pResult;
			}
		}
		else {
			AddResultText(L"  ! [%s] Cannot Perform Calibration. Error Code : %d", strProcessName.c_str(), nErr);
			bCalibrationError = true;
			m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
			return false;
		}
	}
	else {
		AddResultText(L"  ! [%s] Cannot Perform Calibration.", strProcessName.c_str());
		bCalibrationError = true;
		m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
		return false;
	}

	if (m_bCheckCalibrationOnly) {
		AddResultText(L"  > End Vision Inspection, Check Calibration Only. Center (%d. %d)", ptCenter.x, ptCenter.y);
		m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
		return true;
	}

	strProcessName = L"MagCore Inspection";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddResultText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str()	);
			m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
			return false;
		}

		pParam->m_strDataPath = strVIDFile;
		pParam->m_strComp = strComp;

		// Set Character Template Image
		pParam->m_dCharTemplateImage = dCharTemplateImg;

		// Set Circle Position
		NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;
		dParam_Circle2Rect.m_nCenterPosX = dMinCircle.m_ptCenter.x;
		dParam_Circle2Rect.m_nCenterPosY = dMinCircle.m_ptCenter.y;
		dParam_Circle2Rect.m_nMinRadius = dMinCircle.m_nRadius;
		dParam_Circle2Rect.m_nMaxRadius = dMaxCircle.m_nRadius;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dCalibrationImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			if (dOutput.m_pResult != nullptr) {
				bDefect = true;

				// convert image to color image
				cvtColor(dCalibrationImg, dDefectImg, CV_GRAY2BGR);

				Scalar dColor;
				NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)dOutput.m_pResult;
				for (auto &dDefect : pResult->m_listDefect) {
					switch (dDefect.m_nType) {
						//					case NIPLDefect_MagCore::DEFECT_TYPE_SCRETCH: dColor = CV_RGB(255, 0, 0); break;
					case NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER: dColor = CV_RGB(0, 0, 255); break;
						//					case NIPLDefect_MagCore::DEFECT_TYPE_EDGE: dColor = CV_RGB(255, 0, 0); break;
					case NIPLDefect_MagCore::DEFECT_TYPE_SURFACE: dColor = CV_RGB(0, 255, 0); break;
					default: dColor = CV_RGB(255, 0, 0); break;
					}

					rectangle(dDefectImg, dDefect.m_rcBoundingBox, dColor, 1);
				}

				delete pResult;
			}
		}
		else {
			AddResultText(L"  ! [%s] Cannot Perform Inspection. Error Code : %d", strProcessName.c_str(), nErr);
			m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
			return false;
		}
	}
	else {
		AddResultText(L"  ! [%s] Cannot Perform Inspection.", strProcessName.c_str());
		m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;
		return false;
	}

	m_nInspectionProcessTime = ::GetTickCount() - m_nInspectionStartTime;

	if (bDefect) {
		if (!m_bNoSaveImageFile) {
			pNIPO->SaveImage(strDefectFilePath, dDefectImg);
		}
//		AddResultText(L"  !!!! End Vision Inspection, Found Defect : %s", strDefectFilePath.c_str());
	}
	else {
//		AddResultText(L"  > End Vision Inspection, No Defect");
	}

	return true;
}

void CMagCoreBatchTesterDlg::SetResultData(wstring strModel, wstring strType, wstring strResultManual, wstring strFileIndex, wstring strComp, Point ptCenter, bool bSuccess, bool bCalibrationError, bool bDefect)
{
	wstring strKey = strModel + L"-" + strType + L"-" + strFileIndex;

	wstring strResult;
	if (!bSuccess) {
		if (bCalibrationError) strResult = L"Cal_Error";
		else strResult = L"Error";
	}
	else if (bDefect) strResult = L"NG";
	else strResult = L"OK";

	wstring strCenter = L"X:" + to_wstring(ptCenter.x) + L" Y:" + to_wstring(ptCenter.y);

	m_csResult.Lock();

	auto pItem = m_listResult.find(strKey);
	if (pItem == m_listResult.end()) {		// first item
		if (!bSuccess || bDefect) {
			m_nResultNGCount++;
		}

		ResultData dData;

		dData.m_strModel = strModel;
		dData.m_strType = strType;
		dData.m_strFileIndex = strFileIndex;
		dData.m_strResultPath = m_strResultPath + L"\\" + strModel + L"\\" + strType + L"\\" + strFileIndex;

		if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) {
			dData.m_strResult_Inner = strResult;
			dData.m_strResult_Center_Inner = strCenter;
			if (!bCalibrationError) m_listCenter_Inner.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) {
			dData.m_strResult_Upper = strResult;
			dData.m_strResult_Center_Upper = strCenter;
			if (!bCalibrationError) m_listCenter_Upper.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
			dData.m_strResult_Outer = strResult;
			dData.m_strResult_Center_Outer = strCenter; 
			if (!bCalibrationError) m_listCenter_Outer.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) {
			dData.m_strResult_Lower = strResult;
			dData.m_strResult_Center_Lower = strCenter;
			if (!bCalibrationError) m_listCenter_Lower.push_back(ptCenter);
		}

		if (!CHECK_STRING(strResult, L"OK")) {
			dData.m_strResult_Final = L"NG";
		}
		else {
			dData.m_strResult_Final = L"OK";
		}

		dData.m_strResult_Manual = strResultManual;
		if (CHECK_STRING(dData.m_strResult_Final, dData.m_strResult_Manual)) {
			dData.m_strResult_Compare = L"SAME";
		}
		else {
			dData.m_strResult_Compare = L"DIFF";
		}

		m_listResult[strKey] = dData;
	}
	else {
		ResultData &dData = pItem->second;

		if ((!bSuccess || bDefect) && CHECK_STRING(dData.m_strResult_Final, L"OK")) {
			m_nResultNGCount++;
		}

		if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) {
			dData.m_strResult_Inner = strResult;
			dData.m_strResult_Center_Inner = strCenter;
			if (!bCalibrationError) m_listCenter_Inner.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) {
			dData.m_strResult_Upper = strResult;
			dData.m_strResult_Center_Upper = strCenter;
			if (!bCalibrationError) m_listCenter_Upper.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
			dData.m_strResult_Outer = strResult;
			dData.m_strResult_Center_Outer = strCenter;
			if (!bCalibrationError) m_listCenter_Outer.push_back(ptCenter);
		}
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) {
			dData.m_strResult_Lower = strResult;
			dData.m_strResult_Center_Lower = strCenter;
			if (!bCalibrationError) m_listCenter_Lower.push_back(ptCenter);
		}

		if (!CHECK_STRING(strResult, L"OK")) {
			dData.m_strResult_Final = L"NG";
		}
		if (CHECK_STRING(dData.m_strResult_Final, dData.m_strResult_Manual)) {
			dData.m_strResult_Compare = L"SAME";
		}
		else {
			dData.m_strResult_Compare = L"DIFF";
		}
	}

	m_csResult.Unlock();
}

bool CMagCoreBatchTesterDlg::SaveResultFile()
{
	wstring strResultFilePath = m_strResultPath + L"\\MagCore_Result.csv";
	CStdioFile iFile;
	if (iFile.Open(strResultFilePath.c_str(), CFile::modeCreate | CFile::modeWrite) == FALSE) {
		return false;
	}

	wstring strText;
	if (m_bCheckCalibrationOnly) {
		strText = L"Model,Type,Index,Center_Inner,Center_Upper,Center_Outer,Center_Lower\n";
	}
	else if (!m_bSaveCenterData) {
		strText = L"Model,Type,Index,Inner,Upper,Outer,Lower,Final,Manual,Compare,ResultPath\n";
	}
	else {
		strText = L"Model,Type,Index,Inner,Upper,Outer,Lower,Final,Manual,Compare,Center_Inner,Center_Lower,Center_Upper,Center_Outer,ResultPath\n";
	}

	iFile.WriteString(strText.c_str());

	ForceToAddResultText(L"Result Count : %d", m_listResult.size());

	for (auto dItem : m_listResult) {
		ResultData &dData = dItem.second;

		if (m_bCheckCalibrationOnly) {
			strText = dData.m_strModel + L"," + dData.m_strType + L"," + dData.m_strFileIndex + L","
				+ dData.m_strResult_Center_Inner + L"," + dData.m_strResult_Center_Upper + L"," + dData.m_strResult_Center_Outer + L"\n" + L"," + dData.m_strResult_Center_Lower;;
		}
		else if (!m_bSaveCenterData) {
			strText = dData.m_strModel + L"," + dData.m_strType + L"," + dData.m_strFileIndex + L"," + dData.m_strResult_Inner + L","
				+ dData.m_strResult_Upper + L"," + dData.m_strResult_Outer + L"," + dData.m_strResult_Lower + L","
				+ dData.m_strResult_Final + L"," + dData.m_strResult_Manual + L"," + dData.m_strResult_Compare + L","
				+ dData.m_strResultPath + L"\n";
		}
		else {
			strText = dData.m_strModel + L"," + dData.m_strType + L"," + dData.m_strFileIndex + L"," + dData.m_strResult_Inner + L","
				+ dData.m_strResult_Upper + L"," + dData.m_strResult_Outer + L"," + dData.m_strResult_Lower + L","
				+ dData.m_strResult_Final + L"," + dData.m_strResult_Manual + L"," + dData.m_strResult_Compare + L","
				+ dData.m_strResult_Center_Inner + L"," + dData.m_strResult_Center_Lower + L"," + dData.m_strResult_Center_Upper + L"," + dData.m_strResult_Center_Outer + L","
				+ dData.m_strResultPath + L"\n";
		}

		iFile.WriteString(strText.c_str());
	}

	if (m_bCheckCalibrationOnly) {
		strText = L",,Average :," + m_dResultCenter_Inner.m_strCenterMean + L"," + m_dResultCenter_Upper.m_strCenterMean + L"," + m_dResultCenter_Outer.m_strCenterMean + L"\n" + L"," + m_dResultCenter_Lower.m_strCenterMean;
		iFile.WriteString(strText.c_str());

		strText = L",,STD :," + m_dResultCenter_Inner.m_strCenterStd + L"," + m_dResultCenter_Upper.m_strCenterStd + L"," + m_dResultCenter_Outer.m_strCenterStd + L"\n" + L"," + m_dResultCenter_Inner.m_strCenterStd;
		iFile.WriteString(strText.c_str());

		strText = L",,MAX :," + m_dResultCenter_Inner.m_strCenterMax + L"," + m_dResultCenter_Upper.m_strCenterMax + L"," + m_dResultCenter_Outer.m_strCenterMax + L"\n" + L"," + m_dResultCenter_Lower.m_strCenterMax;
		iFile.WriteString(strText.c_str());
	}
	else if (m_bSaveCenterData) {
		strText = L",,,,,,,,,Average :," + m_dResultCenter_Inner.m_strCenterMean + L"," + m_dResultCenter_Outer.m_strCenterMean + L"\n" + L"," + m_dResultCenter_Lower.m_strCenterMean + L"," + m_dResultCenter_Upper.m_strCenterMean;
		iFile.WriteString(strText.c_str());

		strText = L",,,,,,,,,STD :," + m_dResultCenter_Inner.m_strCenterStd + L"," + m_dResultCenter_Upper.m_strCenterStd + L"," + m_dResultCenter_Outer.m_strCenterStd + L"\n" + L"," + m_dResultCenter_Inner.m_strCenterStd;
		iFile.WriteString(strText.c_str());

		strText = L",,,,,,,,,MAX :," + m_dResultCenter_Inner.m_strCenterMax + L"," + m_dResultCenter_Upper.m_strCenterMax + L"," + m_dResultCenter_Outer.m_strCenterMax + L"\n" + L"," + m_dResultCenter_Lower.m_strCenterMax;
		iFile.WriteString(strText.c_str());
	}

	iFile.Close();

	ShellExecute(NULL, L"open", strResultFilePath.c_str(), NULL, NULL, SW_SHOW);

	AddResultText(L"Save Result File : %s", strResultFilePath.c_str());

	return true;
}

void CMagCoreBatchTesterDlg::ForceToAddResultText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlResultList.GetCount();
	nIndex = m_ctlResultList.InsertString(nIndex, strFullText.c_str());
	m_ctlResultList.SetCurSel(nIndex);
}

void CMagCoreBatchTesterDlg::AddResultText(const wchar_t* szText, ...)
{
	if (m_bNoResultDisplay) {
		return;
	}

	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	AddResultText(wstring(szBufferText));
}

void CMagCoreBatchTesterDlg::AddResultText(wstring strText)
{
	if (m_bNoResultDisplay) {
		return;
	}

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += strText;

	int nIndex = m_ctlResultList.GetCount();
	nIndex = m_ctlResultList.InsertString(nIndex, strFullText.c_str());
	m_ctlResultList.SetCurSel(nIndex);
}

void CMagCoreBatchTesterDlg::OnBnClickedClearResult()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_ctlResultList.ResetContent();
}

void CMagCoreBatchTesterDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_bStopProcess = true;

	CDialogEx::OnClose();
}


void CMagCoreBatchTesterDlg::OnBnClickedOpenResultFoler()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShellExecute(NULL, L"open", m_strResultPath.c_str(), NULL, NULL, SW_SHOW);
}


void CMagCoreBatchTesterDlg::OnBnClickedSaveResultFile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!SaveResultFile()) {
		AddResultText(L"Cannot Save Result File");
		return;
	}
}

bool CMagCoreBatchTesterDlg::CheckFileExist(wstring strFilePath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE h = FindFirstFile(strFilePath.c_str(), &FindFileData);
	if (h != INVALID_HANDLE_VALUE) {
		FindClose(h);
		return true;
	}

	return false;
}

void CMagCoreBatchTesterDlg::SetProcessText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	GetDlgItem(IDC_PROCESS)->SetWindowText(szBufferText);
}

void CMagCoreBatchTesterDlg::SetResultCenterData(wstring strComp)
{
	vector<Point> *plistCenter;
	ResultCenterData *pCenterData;
	if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) {
		plistCenter = &m_listCenter_Inner;
		pCenterData = &m_dResultCenter_Inner;
	}
	else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) {
		plistCenter = &m_listCenter_Upper;
		pCenterData = &m_dResultCenter_Upper;
	}
	else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
		plistCenter = &m_listCenter_Outer;
		pCenterData = &m_dResultCenter_Outer;
	}
	else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) {
		plistCenter = &m_listCenter_Lower;
		pCenterData = &m_dResultCenter_Lower;
	}

	int nCount = plistCenter->size();
	Mat dCenterX(1, nCount, CV_16UC1);
	Mat dCenterY(1, nCount, CV_16UC1);
	Point ptCenter;
	for (int i = 0; i < nCount; i++) {
		ptCenter = (*plistCenter)[i];
		dCenterX.at<UINT16>(0, i) = ptCenter.x;
		dCenterY.at<UINT16>(0, i) = ptCenter.y;
	}

	Scalar dMean;
	Scalar dStd;
	meanStdDev(dCenterX, dMean, dStd);
	float nMean = (float)dMean[0];
	float nStd = (float)dStd[0];

	double nMax;
	double nMin;
	minMaxLoc(dCenterX, &nMin, &nMax);
	nMax = max(nMean - nMin, nMax - nMean);

	pCenterData->m_ptCenterMean.x = nMean;
	pCenterData->m_ptCenterStd.x = nStd;
	pCenterData->m_ptCenterMax.x = (float)nMax;

	meanStdDev(dCenterY, dMean, dStd);
	nMean = (float)dMean[0];
	nStd = (float)dStd[0];

	minMaxLoc(dCenterY, &nMin, &nMax);
	nMax = max(nMean - nMin, nMax - nMean);

	pCenterData->m_ptCenterMean.y = nMean;
	pCenterData->m_ptCenterStd.y = nStd;
	pCenterData->m_ptCenterMax.y = (float)nMax;

	wchar_t szBuffer[256];
	swprintf(szBuffer, L"X:%.1f Y:%.1f", pCenterData->m_ptCenterMean.x, pCenterData->m_ptCenterMean.y);
	pCenterData->m_strCenterMean = szBuffer;

	swprintf(szBuffer, L"X:%.1f Y:%.1f", pCenterData->m_ptCenterStd.x, pCenterData->m_ptCenterStd.y);
	pCenterData->m_strCenterStd = szBuffer;

	swprintf(szBuffer, L"X:%.1f Y:%.1f", pCenterData->m_ptCenterMax.x, pCenterData->m_ptCenterMax.y);
	pCenterData->m_strCenterMax = szBuffer;

	AddResultText(L"%s Center, Average(X:%.1f Y:%.1f) STD(X:%.1f Y:%.1f) Max(X:%.1f Y:%.1f)", strComp.c_str(),
		pCenterData->m_ptCenterMean.x, pCenterData->m_ptCenterMean.y,
		pCenterData->m_ptCenterStd.x, pCenterData->m_ptCenterStd.y,
		pCenterData->m_ptCenterMax.x, pCenterData->m_ptCenterMax.y);
}



void CMagCoreBatchTesterDlg::OnBnClickedCheckNoDisplayResult()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
}


void CMagCoreBatchTesterDlg::OnBnClickedCheckNoSaveImageFile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
}
