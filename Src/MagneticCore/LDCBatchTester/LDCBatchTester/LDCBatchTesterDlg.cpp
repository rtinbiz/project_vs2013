
// LDCBatchTesterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "LDCBatchTester.h"
#include "LDCBatchTesterDlg.h"
#include "afxdialogex.h"
#include <thread>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CLDCBatchTesterDlg 대화 상자

CLDCBatchTesterDlg::CLDCBatchTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLDCBatchTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_strDataFile = "D:\\nBizSystem\\Vision\\BatchTest.nib";
	m_strProcess = "";
}

void CLDCBatchTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DATA_FILE, m_strDataFile);
	DDX_Control(pDX, IDC_RESULT_LIST, m_ctlResultList);
	DDX_Control(pDX, IDC_STOP_BATCH_TEST, m_ctlStopBatchTest);
	DDX_Control(pDX, IDC_START_BATCH_TEST, m_ctlStartBatchTest);
	DDX_Text(pDX, IDC_PROCESS, m_strProcess);
}

BEGIN_MESSAGE_MAP(CLDCBatchTesterDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_START_BATCH_TEST, &CLDCBatchTesterDlg::OnBnClickedStartBatchTest)
	ON_BN_CLICKED(IDC_CLEAR_RESULT, &CLDCBatchTesterDlg::OnBnClickedClearResult)
	ON_BN_CLICKED(IDC_STOP_BATCH_TEST, &CLDCBatchTesterDlg::OnBnClickedStopBatchTest)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_OPEN_RESULT_FOLER, &CLDCBatchTesterDlg::OnBnClickedOpenResultFoler)
	ON_BN_CLICKED(IDC_SAVE_RESULT_FILE, &CLDCBatchTesterDlg::OnBnClickedSaveResultFile)
END_MESSAGE_MAP()


// CLDCBatchTesterDlg 메시지 처리기

BOOL CLDCBatchTesterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	m_ctlStartBatchTest.ShowWindow(SW_SHOW);
	m_ctlStopBatchTest.ShowWindow(SW_HIDE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CLDCBatchTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CLDCBatchTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLDCBatchTesterDlg::OnBnClickedStartBatchTest()
{
	UpdateData();

	m_listData.clear();
	m_listResult.clear();

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!LoadBatchDataFile()) {
		AddResultText(L"Cannot Load Batch Data File");
		return;
	}

	if (!CheckImageDataFile()) {
		AddResultText(L"Cannot Check Image Data Files");
		return;
	}

	if (!LoadNIPJob()) {
		AddResultText(L"Cannot Load NIPJob Data File");
		return;
	}

	m_ctlStartBatchTest.ShowWindow(SW_HIDE);
	m_ctlStopBatchTest.ShowWindow(SW_SHOW);

	m_bStopProcess = false;
	ProcessDataFile();

	if (!SaveResultFile()) {
		AddResultText(L"Cannot Save Result File");
		return;
	}
}

void CLDCBatchTesterDlg::OnBnClickedStopBatchTest()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_bStopProcess = true;
}

bool CLDCBatchTesterDlg::LoadBatchDataFile()
{
	XML dXML;
	auto nResult = dXML.Load(LPCTSTR(m_strDataFile));
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, L"Setting")) {
			m_strResultPath = str2wstr(pData->v("result"));

			AddResultText(L"Result Path : %s", m_strResultPath.c_str());
		}
		else if (CHECK_STRING(strElementName, L"Data")) {
			ImageData dImageData;
			dImageData.m_strType = str2wstr(pData->v("type"));
			dImageData.m_strModel = str2wstr(pData->v("model"));
			dImageData.m_strResultManual = str2wstr(pData->v("result"));
			dImageData.m_strFolderPath = str2wstr(pData->v("path"));

			m_listData.push_back(dImageData);

			AddResultText(L"Data, Type : %s, Model : %s, Path : %s", dImageData.m_strType.c_str(), dImageData.m_strModel.c_str(), dImageData.m_strFolderPath.c_str());
		}
	}

	return true;
}

bool CLDCBatchTesterDlg::CheckImageDataFile()
{
	if (m_listData.size() == 0) {
		return false;
	}

	vector<ImageData> listDataFoler(m_listData);

	m_listData.clear();
	for (auto &dData : listDataFoler) {
		wstring strType = dData.m_strType;
		wstring strModel = dData.m_strModel;
		wstring strResultManual = dData.m_strResultManual;
		wstring strFolderPath = dData.m_strFolderPath;

		WIN32_FIND_DATA dFindData;
		wstring strFormat = strFolderPath + L"\\*.jpg";
		HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		int nFileCount = 0;
		if (hFile != INVALID_HANDLE_VALUE) {
			bool bFirstFile = true;
			int nIndex = 0;
			hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
			do {
				ImageData dImageData;
				dImageData.m_strType = strType;
				dImageData.m_strModel = strModel;
				dImageData.m_strResultManual = strResultManual;
				dImageData.m_strFolderPath = strFolderPath;
				dImageData.m_strFileName = dFindData.cFileName;

				m_listData.push_back(dImageData);
				nFileCount++;
			} while (::FindNextFile(hFile, &dFindData));

			::FindClose(hFile);
		}

		AddResultText(L"Files in %s : %d", strFolderPath.c_str(), nFileCount);
	}

	AddResultText(L"Total Files : %d", m_listData.size());
	return true;
}

void CLDCBatchTesterDlg::ProcessDataFile()
{
	SetProcessText(L"");

	m_nErrorCount = 0;
	m_nDefectCount = 0;

	int nCount = (int)m_listData.size();
	int nIndex = 1;
	for (auto &dData : m_listData) {
		wstring strType = dData.m_strType;
		wstring strModel = dData.m_strModel;
		wstring strResultManual = dData.m_strResultManual;
		wstring strFolderPath = dData.m_strFolderPath;
		wstring strFileName = dData.m_strFileName;
		wstring strFilePath = strFolderPath + L"\\" + strFileName;

		AddResultText(L"[%d/%d (%.0f%%)] Type : %s, File : %s", nIndex, nCount, (nIndex * 100.f) / nCount, strType.c_str(), strFilePath.c_str());

		size_t nStartOffset = 0;
		size_t nEndOffset = nStartOffset;

		// File Index
		nEndOffset = strFileName.find('.', nStartOffset);
		if (nEndOffset == wstring::npos) {
			nIndex++;
			m_nErrorCount++;
			AddResultText(L"Done. Cannot Parse Image Index, Current Error Count : %d, Defect Count : %d", m_nErrorCount, m_nDefectCount);

			continue;
		}
		wstring strFileIndex = strFileName.substr(nStartOffset, nEndOffset - nStartOffset);
		AddResultText(L"  > File Index : %s", strFileIndex.c_str());

		DoVisionInspection(strFilePath, strType, strModel, strFileIndex, strResultManual);
		
		AddResultText(L"Done, Current Error Count : %d, Defect Count : %d", m_nErrorCount, m_nDefectCount);
		SetProcessText(L"[%d/%d (%.0f%%)] Error : %d, Defect : %d", nIndex, nCount, (nIndex * 100.f) / nCount, m_nErrorCount, m_nDefectCount);

		// Message Loop to check push stop process
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (m_bStopProcess) {
			AddResultText(L"Stop Processing");
			break;
		}

		nIndex++;
	}

	AddResultText(L"Finish!! Error Count : %d, Defect Count : %d", m_nErrorCount, m_nDefectCount);
	AddResultText(L"Total Result Data Count : %d", m_listResult.size());

	m_ctlStartBatchTest.ShowWindow(SW_SHOW);
	m_ctlStopBatchTest.ShowWindow(SW_HIDE);
}

bool CLDCBatchTesterDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_FOLDER;
	strJobFile += L"VI_LDC.nip";

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		return false;
	}

	AddResultText(L"NIPJob Data File : %s", strJobFile.c_str());

	return true;
}

bool CLDCBatchTesterDlg::DoVisionInspection(wstring strFilePath, wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual)
{
	AddResultText(L"  > Start Vision Inspection, Type : %s, Model : %s, Index : %s", strType.c_str(), strModel.c_str(), strFileIndex.c_str());

	NIPO *pNIPO = NIPO::GetInstance();
	Mat dImg;
	if (!pNIPO->LoadImage(strFilePath, dImg)) {
		AddResultText(L"  ! Cannot Load Image File");
		return false;
	}

	wstring strResultFolderPath = m_strResultPath + L"\\" + strType + L"\\" + strModel;
	SHCreateDirectoryEx(NULL, strResultFolderPath.c_str(), NULL);

	wstring strOrigFilePath = strResultFolderPath + L"\\" + strFileIndex + L".jpg";
	pNIPO->SaveImage(strOrigFilePath, dImg);

	wstring strVIData = VISION_FOLDER + strModel + L".vid";
	bool bFinalNG = false;

	Mat dCalibrationImg;
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_CALIBRATION, VISION_CALIBRATION_DATA_PATH, dImg, dCalibrationImg, bFinalNG);

	Mat dOutImg;
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_TERMINAL, strVIData, dCalibrationImg, dOutImg, bFinalNG);
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_TUBE, strVIData, dCalibrationImg, dOutImg, bFinalNG);
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_GUIDE, strVIData, dCalibrationImg, dOutImg, bFinalNG);
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_BOLT, strVIData, dCalibrationImg, dOutImg, bFinalNG);
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_PAPER, strVIData, dCalibrationImg, dOutImg, bFinalNG);
	DoVisionInspection(strType, strModel, strFileIndex, strResultManual, STR_PROCESS_LDC_CLIP, strVIData, dCalibrationImg, dOutImg, bFinalNG);

	if (bFinalNG) {
		wstring strDefectFilePath = strResultFolderPath + L"\\" + strFileIndex + L"_Defect.jpg";
		pNIPO->SaveImage(strDefectFilePath, dOutImg);

		AddResultText(L"  !!!! End Vision Inspection, Found Defect : %s", strDefectFilePath.c_str());
	}
	else {
		AddResultText(L"  > End Vision Inspection, No Defect");
	}

	return true;
}

void CLDCBatchTesterDlg::DoVisionInspection(wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual, wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFinalNG) {
	bool bDefect = false;
	bool bSkip = false;
	bool bSuccess = DoVisionInspectionProcess(strProcessName, strVIData, dImg, dOutImg, bSkip, bDefect);

	if (bDefect) {
		bFinalNG = true;
	}

	if (!bSuccess) {
		m_nErrorCount++;
	}
	if (bDefect) {
		m_nDefectCount++;
	}

	if (!bSkip) {
		SetResultData(strType, strModel, strFileIndex, strResultManual, strProcessName, bSuccess, bDefect);
	}
}

bool CLDCBatchTesterDlg::DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bSkip, bool &bDefect)
{
	AddResultText(L"  > Process : %s", strProcessName.c_str());

	bSkip = false;

	bDefect = false;
	NIPO *pNIPO = NIPO::GetInstance();
	NIPJobProcess *pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam *pParam = pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddResultText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str());
			return false;
		}

		if (CHECK_STRING(strProcessName, L"LDC Calibration")) ((NIPLParam_LDC_Calibration *)pParam)->m_strDataPath = strVIData;
		else ((NIPLParam_LDC *)pParam)->m_strDataPath = strVIData;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);

		delete pParam;

		if (NIPL_SUCCESS(nErr)) {
			if (CHECK_STRING(strProcessName, L"LDC Calibration")) {
				dOutImg = dOutput.m_dImg;
			}
			else {
				if (dOutput.m_pResult != nullptr) {
					bDefect = true;
					if (CHECK_EMPTY_IMAGE(dOutImg)) {
						dImg.copyTo(dOutImg);
					}

					NIPLResult_Defect_LDC *pResult = (NIPLResult_Defect_LDC *)dOutput.m_pResult;
					for (auto &dDefect : pResult->m_listDefect) {
						rectangle(dOutImg, dDefect.m_rcBoundingBox, CV_RGB(255, 0, 0), 3);
					}
				}
			}
		}
		else if (NIPL_PASS(nErr)) {
			bSkip = true;
			AddResultText(L"    => Skip.", strProcessName.c_str());
		}
		else {
			AddResultText(L"  ! [%s] Cannot Perform Inspection. Error Code : %d.", strProcessName.c_str(), nErr);
			return false;
		}
	}
	else {
		bSkip = true;
		AddResultText(L"    => Skip.", strProcessName.c_str());
	}

	return true;
}

void CLDCBatchTesterDlg::SetResultData(wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual, wstring strProcessName, bool bSuccess, bool bDefect)
{
	wstring strKey = strType + L"-" + strModel + L"-" + strFileIndex;

	wstring strResult;
	if (!bSuccess) strResult = L"Error";
	else if (bDefect) strResult = L"NG";
	else strResult = L"OK";

	auto pItem = m_listResult.find(strKey);
	if (pItem == m_listResult.end()) {		// first item
		ResultData dData;

		dData.m_strType = strType;
		dData.m_strModel = strModel;
		dData.m_strFileIndex = strFileIndex;
		dData.m_strResultPath = m_strResultPath + L"\\" + strType + L"\\" + strModel;

		if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CALIBRATION)) dData.m_strResult_Calibration = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_TERMINAL)) dData.m_strResult_Terminal = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_TUBE)) dData.m_strResult_Tube = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_GUIDE)) dData.m_strResult_Guide = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_BOLT)) dData.m_strResult_Bolt = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_PAPER)) dData.m_strResult_Paper = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CLIP)) dData.m_strResult_Clip = strResult;

		if (!CHECK_STRING(strResult, L"OK")) {
			dData.m_strResult_Final = L"NG";
		}
		else {
			dData.m_strResult_Final = L"OK";
		}

		dData.m_strResult_Manual = strResultManual;
		if (CHECK_STRING(dData.m_strResult_Final, dData.m_strResult_Manual)) {
			dData.m_strResult_Compare = L"SAME";
		}
		else {
			dData.m_strResult_Compare = L"DIFF";
		}

		m_listResult[strKey] = dData;
	}
	else {
		ResultData &dData = pItem->second;

		if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CALIBRATION)) dData.m_strResult_Calibration = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_TERMINAL)) dData.m_strResult_Terminal = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_TUBE)) dData.m_strResult_Tube = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_GUIDE)) dData.m_strResult_Guide = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_BOLT)) dData.m_strResult_Bolt = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_PAPER)) dData.m_strResult_Paper = strResult;
		else if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CLIP)) dData.m_strResult_Clip = strResult;

		if (!CHECK_STRING(strResult, L"OK")) {
			dData.m_strResult_Final = L"NG";
		}

		if (CHECK_STRING(dData.m_strResult_Final, dData.m_strResult_Manual)) {
			dData.m_strResult_Compare = L"SAME";
		}
		else {
			dData.m_strResult_Compare = L"DIFF";
		}
	}
}

bool CLDCBatchTesterDlg::SaveResultFile()
{
	wstring strResultFilePath = m_strResultPath + L"\\LDC_Result.csv";
	CStdioFile iFile;
	if (iFile.Open(strResultFilePath.c_str(), CFile::modeCreate | CFile::modeWrite) == FALSE) {
		return false;
	}

	wstring strText = L"Type,Model,Index,Cal,Terminal,Tube,Guide,Paper,Clip,Bolt,Final,Manual,Compare,ResultPath\n";
	iFile.WriteString(strText.c_str());

	for (auto dItem : m_listResult) {
		ResultData &dData = dItem.second;
		strText = dData.m_strType + L"," + dData.m_strModel + L"," + dData.m_strFileIndex + L"," + dData.m_strResult_Calibration
			+ L"," + dData.m_strResult_Terminal + L"," + dData.m_strResult_Tube + L"," + dData.m_strResult_Guide + L"," + dData.m_strResult_Paper
			+ L"," + dData.m_strResult_Clip + L"," + dData.m_strResult_Bolt + L"," + dData.m_strResult_Final 
			+ L"," + dData.m_strResult_Manual + L"," + dData.m_strResult_Compare + L"," + dData.m_strResultPath + L"\n";

		iFile.WriteString(strText.c_str());
	}

	iFile.Close();

	ShellExecute(NULL, L"open", strResultFilePath.c_str(), NULL, NULL, SW_SHOW);

	AddResultText(L"Save Result File : %s", strResultFilePath.c_str());

	return true;
}

void CLDCBatchTesterDlg::AddResultText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	AddResultText(wstring(szBufferText));
}

void CLDCBatchTesterDlg::AddResultText(wstring strText)
{
	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += strText;

	int nIndex = m_ctlResultList.GetCount();
	nIndex = m_ctlResultList.InsertString(nIndex, strFullText.c_str());
	m_ctlResultList.SetCurSel(nIndex);
}

void CLDCBatchTesterDlg::SetProcessText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	m_strProcess = szBufferText;
	UpdateData(FALSE);
}


void CLDCBatchTesterDlg::OnBnClickedClearResult()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_ctlResultList.ResetContent();
}


void CLDCBatchTesterDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_bStopProcess = true;

	CDialogEx::OnClose();
}


void CLDCBatchTesterDlg::OnBnClickedOpenResultFoler()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShellExecute(NULL, L"open", m_strResultPath.c_str(), NULL, NULL, SW_SHOW);
}


void CLDCBatchTesterDlg::OnBnClickedSaveResultFile()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!SaveResultFile()) {
		AddResultText(L"Cannot Save Result File");
		return;
	}
}

bool CLDCBatchTesterDlg::CheckFileExist(wstring strFilePath)
{
	WIN32_FIND_DATA FindFileData;
	HANDLE h = FindFirstFile(strFilePath.c_str(), &FindFileData);
	if (h != INVALID_HANDLE_VALUE) {
		FindClose(h);
		return true;
	}

	return false;
}
