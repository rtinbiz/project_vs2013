//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// LDCBatchTester.rc에서 사용되고 있습니다.
//
#define IDD_LDCBATCHTESTER_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_DATA_FILE              1000
#define IDC_START_BATCH_TEST            1001
#define IDC_RESULT_LIST                 1002
#define IDC_CLEAR_RESULT                1003
#define IDC_STOP_BATCH_TEST             1004
#define IDC_OPEN_RESULT_FOLER           1005
#define IDC_SAVE_RESULT_FILE            1006
#define IDC_PROCESS                     1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
