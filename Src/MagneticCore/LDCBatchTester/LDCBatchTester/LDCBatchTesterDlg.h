
// LDCBatchTesterDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"

#define STR_PROCESS_LDC_CALIBRATION L"LDC Calibration"
#define STR_PROCESS_LDC_TERMINAL L"LDC Terminal"
#define STR_PROCESS_LDC_TUBE L"LDC Tube"
#define STR_PROCESS_LDC_GUIDE L"LDC Guide"
#define STR_PROCESS_LDC_BOLT L"LDC Bolt"
#define STR_PROCESS_LDC_PAPER L"LDC Paper"
#define STR_PROCESS_LDC_CLIP L"LDC Clip"

#define VISION_FOLDER L"D:\\nBizSystem\\Vision\\"
#define	VISION_CALIBRATION_DATA_PATH _T("D:\\nBizSystem\\Vision\\Calibration.vid")


struct ImageData {
	wstring m_strType;
	wstring m_strModel;
	wstring m_strResultManual;
	wstring m_strFolderPath;
	wstring m_strFileName;

	ImageData() {
		m_strType.clear();
		m_strModel.clear();
		m_strResultManual.clear();
		m_strFolderPath.clear();
		m_strFileName.clear();
	}
};

struct ResultData {
	wstring m_strType;
	wstring m_strModel;
	wstring m_strFileIndex;
	wstring m_strResultPath;

	wstring m_strResult_Calibration;
	wstring m_strResult_Terminal;
	wstring m_strResult_Tube;
	wstring m_strResult_Guide;
	wstring m_strResult_Paper;
	wstring m_strResult_Clip;
	wstring m_strResult_Bolt;

	wstring m_strResult_Final;
	wstring m_strResult_Manual;
	wstring m_strResult_Compare;

	ResultData() {
		m_strType.clear();
		m_strModel.clear();
		m_strFileIndex.clear();
		m_strResultPath.clear();

		m_strResult_Calibration.clear();
		m_strResult_Terminal.clear();
		m_strResult_Tube.clear();
		m_strResult_Guide.clear();
		m_strResult_Paper.clear();
		m_strResult_Clip.clear();
		m_strResult_Bolt.clear();

		m_strResult_Final.clear();
		m_strResult_Manual.clear();
		m_strResult_Compare.clear();
	}
};


// CLDCBatchTesterDlg 대화 상자
class CLDCBatchTesterDlg : public CDialogEx
{
// 생성입니다.
public:
	CLDCBatchTesterDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LDCBATCHTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	wstring m_strResultPath;

	CString m_strDataFile;
	CListBox m_ctlResultList;

	bool m_bStopProcess;
	int m_nErrorCount;
	int m_nDefectCount;
	vector<ImageData> m_listData;
	map<wstring, ResultData> m_listResult;

	NIPJob m_dJob;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void ProcessDataFile();
	void AddResultText(const wchar_t* szText, ...);
	void AddResultText(wstring strText);
	void SetProcessText(const wchar_t* szText, ...);
	bool LoadBatchDataFile();
	bool CheckImageDataFile();
	bool LoadNIPJob();
	bool DoVisionInspection(wstring strFilePath, wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual);
	void DoVisionInspection(wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual, wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFinalNG);
	bool DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bSkip, bool &bDefect);
	void SetResultData(wstring strType, wstring strModel, wstring strFileIndex, wstring strResultManual, wstring strProcessName, bool bSuccess, bool bDefect);
	bool SaveResultFile();
	bool CheckFileExist(wstring strFilePath);


	afx_msg void OnBnClickedStartBatchTest();
	afx_msg void OnBnClickedClearResult();
	afx_msg void OnBnClickedStopBatchTest();
	CButton m_ctlStopBatchTest;
	CButton m_ctlStartBatchTest;
	afx_msg void OnClose();
	afx_msg void OnBnClickedOpenResultFoler();
	afx_msg void OnBnClickedSaveResultFile();
	CString m_strProcess;
};
