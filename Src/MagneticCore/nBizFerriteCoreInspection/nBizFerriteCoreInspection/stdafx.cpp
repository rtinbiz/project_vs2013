
// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// nBizFerriteCoreInspection.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"
G_SystemModeStruct G_SystemModeData;

char* ConvertUnicodeToMultybyte(CString strUnicode)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
	char* pMultibyte = new char[nLen];
	memset(pMultibyte, 0x00, (nLen)*sizeof(char));
	if (nLen > 1020)nLen = 1020;
	WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, pMultibyte, nLen, NULL, NULL);
	//strcpy(chResult, pMultibyte);
	strncpy(chResult, pMultibyte, nLen);
	delete[] pMultibyte;
	return chResult; 
}


char* ConvertUnicodeToMultybyte2(CString strUnicode)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
	char* pMultibyte = new char[nLen];
	memset(pMultibyte, 0x00, (nLen)*sizeof(char));
	WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, pMultibyte, nLen, NULL, NULL);
	return pMultibyte;
}

wchar_t* ConvertMultybyteToUnicode(const char *chText)
{
	//wchar_t* pszTmp = NULL; 
	//int iLen = ::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, 0);
	//if (iLen > 1020)iLen = 1020;
	//pszTmp = new wchar_t[iLen + 1];
	//::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, iLen);
	//wcscpy(wcResult, pszTmp);
	//delete[] pszTmp;
	//return wcResult;

	wchar_t* pszTmp = NULL;
	int iLen = ::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, 0);
	pszTmp = new wchar_t[iLen + 1];
	::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, iLen);
	return pszTmp;

}


void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect)
{
	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biXPelsPerMeter = 100;
	bitmapInfo.bmiHeader.biYPelsPerMeter = 100;
	bitmapInfo.bmiHeader.biClrUsed = 0;
	bitmapInfo.bmiHeader.biClrImportant = 0;
	bitmapInfo.bmiHeader.biSizeImage = 0;
	bitmapInfo.bmiHeader.biWidth = pImgIpl->width;
	bitmapInfo.bmiHeader.biHeight = -pImgIpl->height;
	IplImage* tempImage;
	if (pImgIpl->nChannels == 3)
	{
		tempImage = (IplImage*)cvClone(pImgIpl);
		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
	}
	else if (pImgIpl->nChannels == 1)
	{
		tempImage = cvCreateImage(cvGetSize(pImgIpl), IPL_DEPTH_8U, 3);
		cvCvtColor(pImgIpl, tempImage, CV_GRAY2BGR);
		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
	}
	//20160319_KYS
	IplImage* LinedImage = cvCloneImage(tempImage);
	cvLine(LinedImage, cvPoint(LinedImage->width / 2, 0), cvPoint(LinedImage->width / 2, LinedImage->height), CV_RGB(255, 0, 0), 3, 8, 0);
	cvLine(LinedImage, cvPoint(0, LinedImage->height / 2), cvPoint(LinedImage->width, LinedImage->height / 2), CV_RGB(255, 0, 0), 3, 8, 0);
	//
	pDC->SetStretchBltMode(COLORONCOLOR);
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(),
		0, 0, LinedImage->width, LinedImage->height, LinedImage->imageData, &bitmapInfo,
		DIB_RGB_COLORS, SRCCOPY);
	cvReleaseImage(&LinedImage);

	cvReleaseImage(&tempImage);
}

