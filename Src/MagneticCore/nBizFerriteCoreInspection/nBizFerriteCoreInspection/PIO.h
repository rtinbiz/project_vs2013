#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "stdafx.h"
#include "AxisMotion.h"
//#include "tmcDApiAed.h"
//#include "tmcDApiDef.h"
#include "pmiMApi.h"
#include "pmiMApiDefs.h"
#include "1.Common/Led/LedButton.h"
#include "1.Common\roundbutton\roundbutton2.h"



#define WM_TIMER_IO WM_USER + 100
#define WM_TIMER_100mS WM_TIMER_IO + 1
// CIOViewDlg 대화 상자입니다.
#define BG_COLOR WHITE
#define FG_COLOR GREEN

#define IO_NUM 64

/////////////////////////////////////////////////////////////////////////////////////////////
//DI DEFINE

#define IN_EM_STOP					0
#define IN_AIR_SENSOR				1
#define IN_LOAD_DETECT				2 //NC-->NO

#define IN_LOAD_OPEN					18
#define IN_LOAD_STOP					19
#define IN_STG1_CTR_RET				20
#define IN_STG1_CTR_FWD			21
#define IN_STG4_STOP_OPEN			22
#define IN_STG4_STOP_CLOSE		23
#define IN_STG5_CTR_RET				24
#define IN_STG5_CTR_FWD			25
#define IN_STG6_CAM_UP				26
#define IN_STG6_CAM_DOWN			27

#define IN_UNLOAD_NG_OUT_FWD	28
#define IN_UNLOAD_NG_OUT_RET	29


const CString ST_FI_DI[] = {

	L"IN_EM_STOP",
	L"IN_AIR_SENSOR",
	L"IN_LOAD_DETECT",
	L"", L"", L"", L"", L"",

	L"", L"", L"", L"", L"", L"", L"", L"",

	L"", L"",
	L"IN_LOAD_OPEN",
	L"IN_LOAD_STOP",
	L"IN_STG1_CTR_RET",
	L"IN_STG1_CTR_FWD",
	L"IN_STG4_STOP_OPEN",
	L"IN_STG4_STOP_CLOSE",

	L"IN_STG5_CTR_RET",
	L"IN_STG5_CTR_FWD",
	L"IN_STG6_CAM_UP",
	L"IN_STG6_CAM_DOWN",
	L"IN_UNLOAD_NG_OUT_FWD",
	L"IN_UNLOAD_NG_OUT_RET",
	L"", L"",

};
/////////////////////////////////////////////////////////////////////////////////////////////
//DO DEFINE
#define OUT_TOWER_RED					0
#define OUT_TOWER_YELLOW			1
#define OUT_TOWER_GREEN				2
#define OUT_BUZZER						3
#define OUT_BEFORE_CONV_RUN		4				//20160301_KYS_ADD_NC_USE

#define OUT_LOAD_OPEN					18
#define OUT_LOAD_STOP					19
#define OUT_STG1_CTR_RET				20
#define OUT_STG1_CTR_FWD				21
#define OUT_STG4_STOP_OPEN			22
#define OUT_STG4_STOP_CLOSE		23
#define OUT_STG5_CTR_RET				24
#define OUT_STG5_CTR_FWD				25
#define OUT_STG6_CAM_UP				26
#define OUT_STG6_CAM_DOWN			27
#define OUT_UNLOAD_NG_OUT_FWD	28
#define OUT_UNLOAD_NG_OUT_RET		29


const CString ST_FI_DO[] = {

	L"OUT_TOWER_RED",
	L"OUT_TOWER_YELLOW	",
	L"OUT_TOWER_GREEN",
	L"OUT_BUZZER	",
	L"OUT_BEFORE_CONV_RUN",
	L"",
	L"",
	L"",

	L"", L"", L"", L"", L"", L"", L"", L"",

	//L"", L"", L"", L"", L"", L"", L"", L"",
	//L"", L"", L"", L"", L"", L"", L"", L"",

	L"", L"",
	L"OUT_LOAD_OPEN",
	L"OUT_LOAD_STOP",
	L"OUT_STG1_CTR_RET",
	L"OUT_STG1_CTR_FWD",
	L"OUT_STG4_STOP_OPEN",
	L"OUT_STG4_STOP_CLOSE	",

	L"OUT_STG5_CTR_RET",
	L"OUT_STG5_CTR_FWD",
	L"OUT_STG6_CAM_UP",
	L"OUT_STG6_CAM_DOWN",
	L"OUT_UNLOAD_NG_OUT_FWD",
	L"OUT_UNLOAD_NG_OUT_RET"
	L"", L""

};


/////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _TIMER
{
	int	Accu;		//timer count
	int	On;		//timer input
	int	Set;		//timer settime
	int	OTE;		//timer output

	_TIMER(){memset(this,0x00, sizeof(TIMER));}
	void clear(){memset(this,0x00, sizeof(TIMER));}
} TIMER;

typedef struct _COUNTER
{
	int	Accu;		//counter 
	int	On;		//counter input
	int	Set;		//counter settime
	int	OTE;		//counter output

	_COUNTER(){memset(this,0x00, sizeof(COUNTER));}
	void clear(){memset(this,0x00, sizeof(COUNTER));}
} COUNTER;


class PIO :public CDialogEx
{
	DECLARE_DYNAMIC(PIO)

public:
	PIO(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~PIO();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_IO };

	CRITICAL_SECTION m_criDIO; //DIO read/write mutex

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	void m_fnDeInit(void);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();

	unsigned int		m_uiOutStatus;
	//unsigned long		m_uiGetDin32Ch, m_uiGetDin32Ch2;
	//unsigned long		m_uiGetDout32Ch, m_uiGetDout32Ch2;
	unsigned int		m_uiGetDin32Ch, m_uiGetDin32Ch2;
	unsigned int		m_uiGetDout32Ch, m_uiGetDout32Ch2;

	BOOL LCRLimitDisable;
	BOOL partPassing;

	//2016.04.06 jec add
	CWinThread* m_pThreads;
	//U16		m_wOutStatus;
	//U32		m_uiGetDin32Ch, m_uiGetDin32Ch2;
	//U32		m_uiGetDout32Ch, m_uiGetDout32Ch2;
	//3 cards
	unsigned int		m_Din32[3][2];
	unsigned int		m_Dout32[3][2];

	CLedButton m_DILED[IO_NUM];
	CLedButton m_DOLED[IO_NUM];

	int DI[IO_NUM];
	int DO[IO_NUM];

	int DI_prev[IO_NUM];
	int DO_prev[IO_NUM];

	TIMER	timer[IO_NUM]; //50ms
	COUNTER	counter[IO_NUM]; //
	int twlamp, buzzstop;

	afx_msg void OnBnClickedChkDO(UINT nID);
	int timerAutoRunDIO(void);

	int readDI(int address, int board);
	int readDI_prev(int address, int board);
	int readDO(int address, int board);
	int writeDO(int val, int address, int board);
	int timerTowerLamp(void);
	int timerCheckDIO(void);
	int virtualDIO(void);
	int home(void);

	//2016.04.06 jec add
	volatile bool isThreadRunning;
	int initIOLEDLabel(void);

	//CRoundButton2 m_btnStart[10];
	//CRoundButton2 m_btnStop[10];
	//CRoundButton2 m_btnStep[10];
	//CRoundButton2 m_btnErrorReset[10];
	//CRoundButton2 m_btnStepClear[10];

	//20160315_KYS
	CRoundButton2 m_btnStart[12];
	CRoundButton2 m_btnStop[12];
	CRoundButton2 m_btnStep[12];
	CRoundButton2 m_btnErrorReset[12];
	CRoundButton2 m_btnStepClear[12];

	afx_msg void OnBnClickedBtnBatchStart(UINT nID);
	afx_msg void OnBnClickedBtnBatchStop(UINT nID);
	afx_msg void OnBnClickedBtnStep(UINT nID);
	afx_msg void OnBnClickedBtnErrorReset(UINT nID);
	afx_msg void OnBnClickedBtnBatchClear(UINT nID);
	afx_msg void OnBnClickedBtnBatchStart6(UINT nID);
	afx_msg void OnBnClickedBtnBatchStop6(UINT nID);
	afx_msg void OnBnClickedBtnStep6(UINT nID);
	afx_msg void OnBnClickedBtnErrorReset6(UINT nID);
	afx_msg void OnBnClickedBtnBatchClear6(UINT nID);

	int m_fnInitDIO();
	
	CRoundButton2 m_btnOutCnvyrStart;
	CRoundButton2 m_btnOutCnvyrStop;
	CRoundButton2 m_btnOutCnvyrStep;
	CRoundButton2 m_btnOutCnvyrErrorReset;
	CRoundButton2 m_btnOutCnvyrClear;

	afx_msg void OnBnClickedBtnOutCnvrStart();
	afx_msg void OnBnClickedBtnOutCnvrStop();
	afx_msg void OnBnClickedBtnOutCnvrStep();
	afx_msg void OnBnClickedBtnOutCnvrReset();
	afx_msg void OnBnClickedBtnOutCnvrClear();
	afx_msg void OnBnClickedBtnAllClear();
	CRoundButton2 m_btnAllClear;
};

