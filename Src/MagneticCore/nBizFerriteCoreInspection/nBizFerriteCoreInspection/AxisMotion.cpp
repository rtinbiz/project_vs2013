#include "stdafx.h"
#include "AxisMotion.h"
#include "nBizFerriteCoreInspection.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(AxisMotion, CDialogEx)

extern CMainWnd	*G_MainWnd;

#define	WM_CLOSE_DIALOG						(WM_USER + 3)

#define CMP_DIALOG							0
#define DIO_DIALOG								1
#define INTERPOLATION_DIALOG				2
#define INTERRUPT_DIALOG					3
#define MASTER_SLAVE_DIALOG				4

union {
	unsigned int mem;

	struct {
		USHORT EMG:1;
		USHORT ALM:1;
		USHORT P_LMT:1;
		USHORT N_LMT:1;
		USHORT ORG:1;
		USHORT DIR:1;
		USHORT H_OK:1;
		USHORT PCS:1;
		USHORT CRC:1;
		USHORT EZ:1;
		USHORT CLR:1;
		USHORT LAT:1;
		USHORT SD:1;
		USHORT INP:1;
		USHORT SON:1;
		USHORT RST:1;
		USHORT STA:1;
	}mem_bit;
}udtCardStatus, udtOldCardStatus, udtShuttleStatus, udtLCRAxisStatus[NUM_AXIS];

#pragma comment(lib,"C:\\Program Files\\Alpha Motion\\MotionComposer\\MApi(Library)\\Library\\pmiMApi.lib")

int exiAxisNo;


AxisMotion::AxisMotion(CWnd* pParent /*=NULL*/)
	: CDialogEx(AxisMotion::IDD, pParent)
{

}

AxisMotion::~AxisMotion(void)
{

}


void AxisMotion::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	for (int id = 0; id < 8; id++)
	{
		DDX_Control(pDX, IDC_EDT_CURR_VEL0 + id, m_EdtVelocity[id]);
		DDX_Control(pDX, IDC_EDT_CURR_ACT_POS0 + id, m_EdtActPos[id]);
		DDX_Control(pDX, IDC_EDT_CURR_CMD_POS0 + id, m_EdtCmdPos[id]);
	}

	DDX_Control(pDX, IDC_BTM_RST_STATUS, m_BtmRstStatus);
	DDX_Control(pDX, IDC_BTM_SON_STATUS, m_BtmSonStatus);
	DDX_Control(pDX, IDC_BTM_CCLR_STATUS, m_BtmCclrStatus);
	DDX_Control(pDX, IDC_BTM_LLMT_STATUS, m_BtmNLmtStatus);
	DDX_Control(pDX, IDC_BTM_PLMT_STATUS, m_BtmPLmtStatus);
	DDX_Control(pDX, IDC_BTM_ALM_STATUS, m_BtmAlmStatus);
	DDX_Control(pDX, IDC_BTM_INP_STATUS, m_BtmInpStatus);
	DDX_Control(pDX, IDC_BTM_EMG_STATUS, m_BtmEmgStatus);
	DDX_Control(pDX, IDC_BTM_EZ_STATUS, m_BtmEzStatus);
	DDX_Control(pDX, IDC_BTM_ORG_STATUS, m_BtmOrgStatus);

	DDX_Control(pDX, IDC_CB_HOME_MODE, m_CbHomeMode);
	DDX_Control(pDX, IDC_CB_ENCODER_MODE, m_CbSelectEncoderMode);
	DDX_Control(pDX, IDC_CB_PULSE_MODE, m_CbSelectPulseMode);
	//DDX_Control(pDX, IDC_CB_RANGE, m_CbSelectRange);
	DDX_Control(pDX, IDC_CB_AXIS_SELECT, m_CbAxisSelect);
	DDX_Control(pDX, IDC_CB_SPEED_MODE, m_CbSpeedMode);
	DDX_Control(pDX, IDC_EDT_CURR_CMD_POS8, m_EdtCmdPos8);
	DDX_Control(pDX, IDC_EDT_CURR_ACT_POS8, m_EdtActPos8);
	DDX_Control(pDX, IDC_EDT_CURR_VEL8, m_EdtVelocity8);

	//for (int id = 0; id < NUM_AXIS; id++)
	//{
	//	DDX_Control(pDX, IDC_BTN_LCR_AXIS1 + id, m_btnLCRAxisGoToReady[id]);
	//	DDX_Control(pDX, IDC_BTN_LCR_AXIS11 + id, m_btnLCRAxisHome[id]);
	//	DDX_Control(pDX, IDC_BTN_LCR_AXIS21 + id, m_btnLCRAxisAlarmReset[id]);
	//	DDX_Control(pDX, IDC_BTN_LCR_AXIS31 + id, m_btnLCRAxisJOGP[id]);
	//	DDX_Control(pDX, IDC_BTN_LCR_AXIS41 + id, m_btnLCRAxisJOGN[id]);
	//}

	DDX_Control(pDX, IDC_EDT_CURR_CMD_POS13, m_EdtCmdPosCvyor);
	DDX_Control(pDX, IDC_EDT_CURR_ACT_POS13, m_EdtActPosCvyor);
	DDX_Control(pDX, IDC_EDT_CURR_VEL13, m_EdtVelocityCvyor);
	DDX_Control(pDX, IDC_BTN_CNVYOR_SET, m_btnOutCnvyorIncSet);
	DDX_Control(pDX, IDC_TXT_CNVYOR_INCREMENT, m_staticOutCnvyorInc);
	DDX_Control(pDX, IDC_BTN_CNVYOR_MOVE, m_btnOutCnvyrIncMove);
	DDX_Control(pDX, IDC_BUTTON1, m_btnAxisSetEnable);
	DDX_Control(pDX, IDC_BUTTON9, m_btnAxisSetDisable);
	DDX_Control(pDX, IDC_BTN_CNVYOR_SET2, m_btnInCnvyorIncSet);
	DDX_Control(pDX, IDC_BTN_CNVYOR_MOVE2, m_btnInCnvyrIncMove);
	DDX_Control(pDX, IDC_AXIS_ALARM_RESET3, m_btnOutCnvyrReset);
	DDX_Control(pDX, IDC_AXIS_ALARM_RESET2, m_btnInCnvyrReset);
	DDX_Control(pDX, IDC_AXIS_CLEAR, m_btnOutCnvyrClearPos);
	DDX_Control(pDX, IDC_AXIS_CLEAR2, m_btnInCnvyrClearPos);
	DDX_Control(pDX, IDC_BUTTON10, m_btnAxisSetEnable2);
	DDX_Control(pDX, IDC_BUTTON11, m_btnAxisSetDisable2);
	DDX_Control(pDX, IDC_TXT_CNVYOR_INCREMENT2, m_staticInCnvyorInc);
	DDX_Control(pDX, IDC_BUTTON6, m_btnAxisEMStop);
}

BEGIN_MESSAGE_MAP(AxisMotion, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_CHK_ORG_LOGIC, OnChkOrgLogic)
	ON_BN_CLICKED(IDC_CHK_EZ_LOGIC, OnChkEzLogic)
	ON_BN_CLICKED(IDC_CHK_EMG_LOGIC, OnChkEmgLogic)
	ON_BN_CLICKED(IDC_CHK_INP_LOGIC, OnChkInpLogic)
	ON_BN_CLICKED(IDC_CHK_ALM_LOGIC, OnChkAlmLogic)
	ON_BN_CLICKED(IDC_CHK_LMT_LOGIC, OnChkLmtLogic)
	ON_BN_CLICKED(IDC_CHK_INP_ENABLE, OnChkInpEnable)
	ON_BN_CLICKED(IDC_CHK_ENCODER_DIR, OnChkEncoderDir)
	ON_BN_CLICKED(IDC_CHK_HOME_DIR, OnChkHomeDir)
	ON_BN_CLICKED(IDC_BTN_ESTOP, OnBtnEstop)
	ON_BN_CLICKED(IDC_BTN_SSTOP, OnBtnSstop)
	//ON_BN_CLICKED(IDC_BTN_PJOG, OnBtnPjog)
	//ON_BN_CLICKED(IDC_BTN_NJOG, OnBtnNjog)
	ON_BN_CLICKED(IDC_BTN_INCMOVE, OnBtnIncmove)
	ON_BN_CLICKED(IDC_BTN_ABSMOVE, OnBtnAbsmove)
	ON_BN_CLICKED(IDC_BTN_HOMEMOVE, OnBtnHomemove)	
	ON_BN_CLICKED(IDC_BTN_MULTI_INCMOVE, OnBtnMultiIncmove)
	ON_BN_CLICKED(IDC_BTN_MULTI_ABSMOVE, OnBtnMultiAbsmove)
	ON_BN_CLICKED(IDC_BTN_CLEAR_POS, OnBtnClearPos)
	ON_BN_CLICKED(IDC_BTN_MULTI_ESTOP, OnBtnMultiEstop)
	ON_BN_CLICKED(IDC_BTN_MULTI_SSTOP, OnBtnMultiSstop)
	ON_BN_CLICKED(IDC_BTN_OVERRIDE_SPEED, OnBtnOverrideSpeed)
	ON_BN_CLICKED(IDC_BTN_OVERRIDE_ABS, OnBtnOverrideAbs)
	ON_BN_CLICKED(IDC_BTN_CCLR, OnBtnCclr)
	ON_BN_CLICKED(IDC_BTN_SVON_ONOFF, OnBtnSvonOnoff)
	ON_BN_CLICKED(IDC_BTN_RST_ONOFF, OnBtnRstOnoff)
	ON_CBN_SELCHANGE(IDC_CB_PULSE_MODE, OnSelchangeCbPulseMode)
	ON_CBN_SELCHANGE(IDC_CB_ENCODER_MODE, OnSelchangeCbEncoderMode)
	ON_CBN_SELCHANGE(IDC_CB_HOME_MODE, OnSelchangeCbHomeMode)
	ON_CBN_SELCHANGE(IDC_CB_AXIS_SELECT, OnSelchangeCbAxisSelect)	
	//ON_MESSAGE(WM_CLOSE_DIALOG, OnReleaseDlg)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_CB_SPEED_MODE, &AxisMotion::OnCbnSelchangeCbSpeedMode)

	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()

	ON_BN_CLICKED(IDC_BTN_CNVYOR_SET, &AxisMotion::OnBnClickedBtnOutCnvyorSet)
	ON_BN_CLICKED(IDC_BTN_CNVYOR_MOVE, &AxisMotion::OnBnClickedBtnCnvyorMove)
	ON_BN_CLICKED(IDC_BUTTON1, &AxisMotion::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON9, &AxisMotion::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_AXIS_ALARM_RESET2, &AxisMotion::OnBnClickedAxisAlarmReset2)
	ON_BN_CLICKED(IDC_AXIS_ALARM_RESET, &AxisMotion::OnBnClickedAxisAlarmReset)
	ON_BN_CLICKED(IDC_BUTTON10, &AxisMotion::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &AxisMotion::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BTN_CNVYOR_MOVE2, &AxisMotion::OnBnClickedBtnCnvyorMove2)
	ON_BN_CLICKED(IDC_BTN_CNVYOR_SET2, &AxisMotion::OnBnClickedBtnCnvyorSet2)
	ON_BN_CLICKED(IDC_AXIS_CLEAR2, &AxisMotion::OnBnClickedAxisClear2)
	ON_BN_CLICKED(IDC_AXIS_CLEAR, &AxisMotion::OnBnClickedAxisClear)
	ON_BN_CLICKED(IDC_AXIS_ALARM_RESET3, &AxisMotion::OnBnClickedAxisAlarmReset3)
END_MESSAGE_MAP()

void AxisMotion::SetUMMsgResponse(int msg, int rsp)
{
	//m_csAxisPos.Lock();
	axisMsgRsp.RSP[msg] = rsp;
	//m_csAxisPos.Unlock();
}


int AxisMotion::GetUMMsgResponse(int msg)
{
	int rsp;
	//m_csAxisPos.Lock();
	rsp = axisMsgRsp.RSP[msg];
	//m_csAxisPos.Unlock();
	return rsp;
}

BOOL AxisMotion::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//////////////////////////////////////////////////////////////////////////
	// Motion Control Board를 Load 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iConNum = 0;
	CString strMsg = _T("");	

	exiAxisNo = 0;

#ifndef VIRTUAL_RUN

	iRet = pmiSysLoad( TMC_FALSE, &iConNum );
	//iRet = pmiSysLoad( TMC_TRUE, &iConNum );

	if(iRet >= 0)
	{
		// 파라미터 파일은 MotionComposer.ini 파일에 Parameter_File = 경로를 쫓아 간다.
		//iRet = pmiConParamLoad(NULL);
		char szCmeFilePath[MAX_PATH];
		sprintf_s(szCmeFilePath,"%s", TMC_FILE_NAME);
		if(pmiConParamLoad(szCmeFilePath) != TMC_RV_OK){
			AfxMessageBox(L"Axis Motion Parameter File Not Exists.");
			G_AddLog(3,L"pmiConParamLoad failed");
		}
	}else
	{		
		// Motion Conrol Board가 정상적으로 Load 되지 않았을때
		// 메세지 창에 에러 Load 에러 값을 표시 합니다.
		strMsg.Format(_T("Motion pmiSysLoad Fail\n Error No. = %d"), iRet);
		AfxMessageBox(strMsg);
		//MessageBox(strMsg);

		return FALSE;
	}

	SetTimer(100, 100, NULL);
	SetTimer(200, 100, NULL);

#endif

	InitDlgItem();	// 다이알로그에 있는 각 아이템의 초기값을 표시합니다.

	//DISABLE AXIS POSITION SET BUTTON
	m_btnOutCnvyorIncSet.EnableWindow(FALSE);
	m_btnAxisSetEnable.EnableWindow(TRUE);
	m_btnAxisSetDisable.EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void AxisMotion::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


void AxisMotion::m_fnInit(void)
{
	int iRet = 0;
	CString strMsg;
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	ShowWindow(SW_HIDE);

	GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	
#ifndef VIRTUAL_RUN
	//emg
	int iIsCheck;
	iRet = pmiGnSetEmgLevel( CARD_NO, emLOGIC_B );
	iRet = pmiGnGetEmgLevel( CARD_NO, &iIsCheck );
#endif

	strMsg.Format(L"%.f", G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_IN_INC]);
	GetDlgItem(IDC_TXT_CNVYOR_INCREMENT2)->SetWindowText(strMsg);
	m_staticInCnvyorInc.SetText(strMsg, BLACK, WHITE);

	strMsg.Format(L"%.f", G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_OUT_INC]);
	GetDlgItem(IDC_TXT_CNVYOR_INCREMENT)->SetWindowText(strMsg);
	m_staticOutCnvyorInc.SetText(strMsg, BLACK, WHITE);

	//Set Disable
	OnBnClickedButton11();
	OnBnClickedButton9();

#ifndef VIRTUAL_RUN
	//LCR Axis ready position
	for(int i = 0; i < NUM_AXIS; i++)
	{
		iRet = initAxis(i);
		if( iRet == TMC_RV_OK) {
			G_AddLog(3,L"Axis %d Motor on", i);
		}else{
			G_AddLog(3,L"Axis %d Motor on failed", i);
			//Error
		}
	}
#endif
}

void AxisMotion::m_fnDeInit(void)
{
		int iRet = 0;

#ifndef VIRTUAL_RUN
	// TODO: Add your message handler code here and/or call default
	//////////////////////////////////////////////////////////////////////////
	// Load 된 Motion Control Board를 Unload 합니다.	                    //
	//////////////////////////////////////////////////////////////////////////
	iRet = pmiSysUnload();
#endif
}

void AxisMotion::OnClose() 
{
	CDialog::OnClose();
}

BOOL AxisMotion::ReturnMessage(int iRet)
{
	CString strBuff = _T("");

	if( iRet != TMC_RV_OK )
	{
		//strBuff.Format(_T("Error Return Code : %d"), iRet);
		//AfxMessageBox(strBuff);
		return FALSE;
	}

	return TRUE;
}

void AxisMotion::InitDlgItem()
{
	int iRet = 0;
	CString strMsg;
	CString strText, strVal, strVerH, strVerM, strVerL;
	int iDllVer = 0;
	int iModel = 0;
	int iDllVer2 = 0;
	int iModel2 = 0;

	m_nMotEvtCnt = 0;
	m_iAxisNum = 0;
	m_iDiNum = 0;
	m_iDoNum = 0;
	m_iAxisNum2 = 0;
	m_iDiNum2 = 0;
	m_iDoNum2 = 0;

	m_BitMapLedG.LoadBitmap(IDB_BTM_LED_GREEN);
	m_BitMapLedR.LoadBitmap(IDB_BTM_LED_RED);
	m_BitMapLedW.LoadBitmap(IDB_BTM_LED_WHITE);

	m_BtmOrgStatus.SetBitmap(m_BitMapLedW);
	m_BtmEzStatus.SetBitmap(m_BitMapLedW);
	m_BtmEmgStatus.SetBitmap(m_BitMapLedW);
	m_BtmInpStatus.SetBitmap(m_BitMapLedW);
	m_BtmAlmStatus.SetBitmap(m_BitMapLedW);
	m_BtmPLmtStatus.SetBitmap(m_BitMapLedW);
	m_BtmNLmtStatus.SetBitmap(m_BitMapLedW);
	m_BtmCclrStatus.SetBitmap(m_BitMapLedW);
	m_BtmSonStatus.SetBitmap(m_BitMapLedW);
	m_BtmRstStatus.SetBitmap(m_BitMapLedW);

	////////////////////////////////////////////////////////////////////////
	//CARD NO. 1

	iRet = pmiGnGetAxesNum( CARD_NO, &m_iAxisNum );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiGnGetDioNum( CARD_NO, &m_iDiNum, &m_iDoNum );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 모델명
	iRet = pmiConGetModel(CARD_NO, &iModel);
	if( ReturnMessage(iRet) == FALSE )
		return;
	switch (iModel)
	{
	case TMC_BA800P:

	case TMC_BA600P:

	case TMC_BA400P:

	case TMC_BA200P:

		strMsg.Format(_T("TMC-BA%d%02dP"), m_iAxisNum, m_iDiNum);
		SetDlgItemText(IDC_EDT_MODEL, strMsg);		

		break;

	case TMC_BB160P:

	case TMC_BB120P:

	case TMC_BB800P:

	case TMC_BB400P:

		strMsg.Format(_T("TMC-BB%d00P"), m_iAxisNum);
		SetDlgItemText(IDC_EDT_MODEL, strMsg);

		break;
	}
	
	// Dll Version//
	iRet = pmiConGetMApiVersion( CARD_NO, &iDllVer );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strText.Format(_T("%ld"), iDllVer);
	strVerH = strText.Left( 1 );
	strVerM = strText.Mid( 5, 1 );
	strVerL = strText.Right( 1 );
	strText = strVerH + '.' + strVerM + '.' + strVerL;
	SetDlgItemText(IDC_EDT_DLL_VER, strText);

	// 축 개수
	strMsg.Format(_T("%d"), m_iAxisNum);
	SetDlgItemText(IDC_EDT_AXIS_NUM, strMsg);

	// 범용 Digital Out 개수
	strMsg.Format(_T("%02d"), m_iDoNum);
	SetDlgItemText(IDC_EDT_DOUT_NUM, strMsg);

	// 범용 Digital In 개수
	strMsg.Format(_T("%02d"), m_iDiNum);
	SetDlgItemText(IDC_EDT_DIN_NUM, strMsg);

	/////////////////////////////////////////////////////////

	m_CbAxisSelect.AddString(L"IN CONVEYOR");		//0
	m_CbAxisSelect.AddString(L"OUT CONVEYOR");	//1

	m_CbAxisSelect.SetCurSel(0);

	m_CbSelectPulseMode.AddString(_T("0 : PLS NEG/DIR HI"));
	m_CbSelectPulseMode.AddString(_T("1 : PLS POS/DIR HI"));
	m_CbSelectPulseMode.AddString(_T("2 : PLS NEG/DIR LOW"));
	m_CbSelectPulseMode.AddString(_T("3 : PLS POS/DIR LOW"));
	m_CbSelectPulseMode.AddString(_T("4 : CW/CCW NEG"));
	m_CbSelectPulseMode.AddString(_T("5 : CW/CCW POS")); //
	m_CbSelectPulseMode.AddString(_T("6 : CW/CCW NEG INV"));
	m_CbSelectPulseMode.AddString(_T("7 : CW/CCW POS INV"));
	m_CbSelectPulseMode.AddString(_T("8 : Two Phase(Out Lead)"));
	m_CbSelectPulseMode.AddString(_T("9 : Two Phase(Dir Lead)"));

	m_CbSelectEncoderMode.AddString(_T("0 : X1(EA/EB)"));
	m_CbSelectEncoderMode.AddString(_T("1 : X2(EA/EB)"));
	m_CbSelectEncoderMode.AddString(_T("2 : X4(EA/EB)"));//
	m_CbSelectEncoderMode.AddString(_T("3 : Up/Down"));

	m_CbHomeMode.AddString(_T("0 : ORG ON -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("1 : ORG ON -> Stop -> Go back(Rev Spd) -> ORG OFF -> Go forward(Rev Spd) -> ORG ON -> Stop")); //
	m_CbHomeMode.AddString(_T("2 : ORG ON -> Slow down(Low Spd) -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("3 : ORG ON -> EZ signal -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("4 : ORG ON -> Stop -> Go back(Rev Spd) -> ORG OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("5 : ORG ON -> Stop -> Go back(High Spd) -> ORG OFF -> EZ signal -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("6 : EL ON -> Stop -> Go back(Rev Spd) -> EL OFF -> Stop"));
	m_CbHomeMode.AddString(_T("7 : EL ON -> Stop -> Go back(Rev Spd) -> EL OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("8 : EL ON -> Stop -> Go back(High Spd) -> EL OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("9 : ORG ON -> Slow down -> Stop -> Go back -> Stop at beginning edge of ORG"));
	m_CbHomeMode.AddString(_T("10 : ORG ON -> EZ signal -> Slow down -> Stop -> Go back -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("11 : ORG ON -> Slow down -> Stop -> Go back (High Spd) -> ORG OFF -> EZ signal -> Slow down -> Stop -> Go forward(High Spd) -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("12 : EL ON -> Stop -> Go back (High Spd) -> EL OFF -> EZ signal -> Slow down -> Stop -> Go forward(High Spd) -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("13 : EZ signal -> Slow down -> Stop"));

	m_CbSpeedMode.AddString(_T("0 : Constant"));
	m_CbSpeedMode.AddString(_T("1 : Trapezoidal"));
	m_CbSpeedMode.AddString(_T("2 : S-Curve"));

	SetDlgItemText(IDC_EDT_INIT_SPEED, _T("1000"));
	SetDlgItemText(IDC_EDT_DRV_SPEED, _T("10000"));
	SetDlgItemText(IDC_EDT_ACC_TIME, _T("100"));
	SetDlgItemText(IDC_EDT_DEC_TIME, _T("100"));

	SetDlgItemText(IDC_EDT_POSITION1, _T("1000"));
	SetDlgItemText(IDC_EDT_POSITION2, _T("2000"));

	SetDlgItemText(IDC_EDT_HOME_OFFSET, _T("0"));

	SetDlgItemText(IDC_EDT_OVERRIDE_SPEEDDATA, _T("50000"));
	SetDlgItemText(IDC_EDT_OVERRIDE_POSITIONDATA, _T("5000000"));

	udtCardStatus.mem = 0;
	udtOldCardStatus.mem = 0;

	RefreshParameter();
}


void AxisMotion::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == 100)
	{
		if (G_SystemModeData.unSystemMode != SYSTEM_AUTO_RUN){
			//m_csAxisPos.Lock();
			PosAndVelDisplay();
			//m_csAxisPos.Unlock();
		}
	}else if(nIDEvent == 200)
	{
		CardStatusDisplay();
		m_fnDisplayShuttleStatus();
	}

	CDialog::OnTimer(nIDEvent);	
}

void AxisMotion::CardStatusDisplay()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Card Satus를 읽어서 Display 합니다.                      //
	// 상태값이 변화가 있으면 변화된 값만 Display 합니다.                   //
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;

	iAxisNo = m_CbAxisSelect.GetCurSel();

	iRet = pmiAxGetMechanical(CARD_NO, iAxisNo, &udtCardStatus.mem);
	if( ReturnMessage(iRet) == FALSE )
		return;

	if(udtCardStatus.mem_bit.ORG != udtOldCardStatus.mem_bit.ORG)	// 상태값이 변화를 확인합니다.
	{
		if(udtCardStatus.mem_bit.ORG == 1)
			m_BtmOrgStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmOrgStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.EZ != udtOldCardStatus.mem_bit.EZ)
	{
		if(udtCardStatus.mem_bit.EZ == 1)
			m_BtmEzStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmEzStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.EMG != udtOldCardStatus.mem_bit.EMG)
	{
		if(udtCardStatus.mem_bit.EMG == 1)
			m_BtmEmgStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmEmgStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.INP != udtOldCardStatus.mem_bit.INP)
	{
		if(udtCardStatus.mem_bit.INP == 1)
			m_BtmInpStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmInpStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.ALM != udtOldCardStatus.mem_bit.ALM)
	{
		if(udtCardStatus.mem_bit.ALM == 1)
			m_BtmAlmStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmAlmStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.P_LMT != udtOldCardStatus.mem_bit.P_LMT)
	{
		if(udtCardStatus.mem_bit.P_LMT == 1)
			m_BtmPLmtStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmPLmtStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.N_LMT != udtOldCardStatus.mem_bit.N_LMT)
	{
		if(udtCardStatus.mem_bit.N_LMT == 1)
			m_BtmNLmtStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmNLmtStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.CLR != udtOldCardStatus.mem_bit.CLR)
	{
		if(udtCardStatus.mem_bit.CLR == 1)
			m_BtmCclrStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmCclrStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.SON != udtOldCardStatus.mem_bit.SON)
	{
		if(udtCardStatus.mem_bit.SON == 1)
			m_BtmSonStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmSonStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.RST != udtOldCardStatus.mem_bit.RST)
	{
		if(udtCardStatus.mem_bit.RST == 1)
			m_BtmRstStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmRstStatus.SetBitmap(m_BitMapLedW);
	}

	udtOldCardStatus.mem = udtCardStatus.mem;	// 이전 상태값을 업데이트 합니다.
}

void AxisMotion::PosAndVelDisplay()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Command, Encoder 위치 및 Command 속도를 Display 합니다.  //
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	double dbCmdPos = 0;
	double dbActPos = 0;
	double dbVelocity = 0;
	CString strVal;
	int iAxisNo;

	//Card no. 1
	for(iAxisNo=0; iAxisNo < m_iAxisNum; iAxisNo++)
	{
		iRet = pmiAxGetCmdPos( CARD_NO, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
			return;
		strVal.Format(_T("%.f"), dbCmdPos);
		m_EdtCmdPos[iAxisNo].SetWindowText(strVal);
		m_dAxisCmdPos[iAxisNo] = dbCmdPos;
		if(iAxisNo < m_iAxisNum)
			m_dAxisCmdPos[iAxisNo]=dbCmdPos;

		iRet = pmiAxGetActPos( CARD_NO, iAxisNo, &dbActPos );
		if( ReturnMessage(iRet) == FALSE )
			return;
		strVal.Format(_T("%.f"), dbActPos);
		m_EdtActPos[iAxisNo].SetWindowText(strVal);
		
		iRet = pmiAxGetCmdVel( CARD_NO, iAxisNo, &dbVelocity );
		if( ReturnMessage(iRet) == FALSE )
			return;
		strVal.Format(_T("%.f"), dbVelocity);
		m_EdtVelocity[iAxisNo].SetWindowText(strVal);
	}
}

void AxisMotion::OnChkOrgLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ORG 논리를 변경 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;

	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ORG_LOGIC);
	iRet = pmiAxSetOrgLevel( axisCard, iAxisNo, iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetOrgLevel( axisCard, iAxisNo, &iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ORG_LOGIC, iIsCheck);
}

void AxisMotion::OnChkEzLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 EZ 논리를 변경 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_EZ_LOGIC);
	iRet = pmiAxSetEzLevel( axisCard, iAxisNo, iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEzLevel( axisCard, iAxisNo, &iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EZ_LOGIC, iIsCheck);
}

void AxisMotion::OnChkEmgLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// EMG 논리를 변경 합니다.												//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;	
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_EMG_LOGIC);
	iRet = pmiGnSetEmgLevel( axisCard, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.	
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EMG_LOGIC, iIsCheck);

}

void AxisMotion::OnChkInpLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 INP 논리와 사용 유무를 변경 합니다.						//
	// Enable이 1로 설정되면 서보에서 Inpotion 신호가 발생해야 동작이		//
	// 완료된 것입니다.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck2 = IsDlgButtonChecked(IDC_CHK_INP_ENABLE);
	iIsCheck1 = IsDlgButtonChecked(IDC_CHK_INP_LOGIC);

	iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck2);
}

void AxisMotion::OnChkAlmLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ALM 논리와 사용 유무를 변경 합니다.						//	
	// Enable이 1로 설정되면 서보에서 알람 신호 발생 시 즉시 정지 합니다.	//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ALM_LOGIC);
	iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ALM_LOGIC, iIsCheck);	
}

void AxisMotion::OnChkLmtLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 리미트 신호 감지 시 정지 방법을 설정 합니다.				//
	// 선택한 축의 +LMT, -LMT 논리를 변경 합니다.							//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_LMT_LOGIC);
	iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_LMT_LOGIC, iIsCheck);	
}

void AxisMotion::OnChkInpEnable() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 INP 사용 유무와 논리를 변경 합니다.						//
	// Enable이 1로 설정되면 서보에서 Inpotion 신호가 발생해야 동작이		//
	// 완료된 것입니다.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck2 = IsDlgButtonChecked(IDC_CHK_INP_ENABLE);
	iIsCheck1 = IsDlgButtonChecked(IDC_CHK_INP_LOGIC);

	iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck2);
}

void AxisMotion::OnChkEncoderDir() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Encoder 값 방향을 설정 합니다							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ENCODER_DIR);

	iRet = pmiAxSetEncDir( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEncDir( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_ENCODER_DIR, iIsCheck);	
}

void AxisMotion::OnChkHomeDir() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Home Move 방향을 설정 합니다.							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iIsCheck = IsDlgButtonChecked(IDC_CHK_HOME_DIR);

	iRet = pmiAxHomeSetDir( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxHomeGetDir( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_HOME_DIR, iIsCheck);	
}

void AxisMotion::OnSelchangeCbAxisSelect() 
{
	// TODO: Add your control notification handler code here
	RefreshParameter();

}

void AxisMotion::OnSelchangeCbPulseMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Pulse Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iMode = m_CbSelectPulseMode.GetCurSel();

	iRet = pmiAxSetPulseType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetPulseType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	m_CbSelectPulseMode.SetCurSel(iMode);	
}

void AxisMotion::OnSelchangeCbEncoderMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Encoder Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iMode = m_CbSelectEncoderMode.GetCurSel();

	iRet = pmiAxSetEncType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEncType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectEncoderMode.SetCurSel(iMode);	
}

void AxisMotion::OnSelchangeCbHomeMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// Home Mode를 변경 합니다.												//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iMode = m_CbHomeMode.GetCurSel();

	iRet = pmiAxHomeSetType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxHomeGetType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbHomeMode.SetCurSel(iMode);	
}

void AxisMotion::SetPosMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Inc Move, Abs Move의 시작 속도, 운전속도, 가속도, 감속도를 설정		//
	// 합니다.																//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");	

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;

	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DEC_TIME, strGetValue);
	dbDecTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::SetJogMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Jog Move의 시작 속도, 운전속도, 가속도, 감속도를 설정				//
	// 합니다.																//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");	

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_JOG_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitJogVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetJogVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}


void AxisMotion::SetHomeMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Home Move의 시작 속도, 운전속도, 가속도, 감속도를 설정합니다. 		//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbRevSpeed = 0;
	double dbAccTime = 0;
	CString strGetValue = _T("");

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;

	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_REV_SPEED, strGetValue);
	dbRevSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxHomeSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxHomeSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbRevSpeed, dbAccTime );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnEstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 즉시 정지 시킵니다.										//
	// 정지 명령어를 사용한 후 반드시 Done() 함수를 사용하여 정지 여부를	//
	// 확인 하십시요.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;

	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;

	iRet = pmiAxEStop( axisCard, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnSstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 감속 정지 시킵니다.										//
	// 정지 명령어를 사용한 후 반드시 Done() 함수를 사용하여 정지 여부를	//
	// 확인 하십시요.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	CString strGetValue = _T("");

	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;

	iRet = pmiAxStop( CARD_NO, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}



void AxisMotion::OnBtnIncmove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 입력한 위치값 만큼 이송합니다.(Inc Move)					//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbPosition = _tstof(strGetValue);

	SetPosMoveSpeed(iAxisNo);

	iRet = pmiAxPosMove( axisCard, iAxisNo, TMC_INC, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnAbsmove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 입력한 위치값까지 이송합니다.(abs Move)					//
	// 현재 위치가 입력한 위치값과 같으면 움직이지 않습니다.				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();

	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbPosition = _tstof(strGetValue);

	SetPosMoveSpeed(iAxisNo);

	iRet = pmiAxPosMove( axisCard, iAxisNo, TMC_ABS, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnHomemove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 원점 복귀 동작을 수행 합니다.(Home Move)				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbOffset = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_HOME_OFFSET, strGetValue);
	dbOffset = _tstof(strGetValue);

	SetHomeMoveSpeed(iAxisNo);

	// Home Move 완료 후 Offset 위치를 설정 합니다.
	iRet = pmiAxHomeSetShiftDist( axisCard, iAxisNo, dbOffset );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// Home Move를 시작 합니다.
	iRet = pmiAxHomeMove( axisCard, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::RefreshParameter()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축에 설정되어 있는 Parameter 값들을 확인합니다.				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	double dbBuff1 = 0;
	double dbBuff2 = 0;
	double dbBuff3 = 0;
	CString strBuff = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	axisCard = CARD_NO;

	// ORG 논리값을 확인 합니다.
	iRet = pmiAxGetOrgLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ORG_LOGIC, iIsCheck1);

	// EZ 논리값을 확인 합니다.
	iRet = pmiAxGetEzLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EZ_LOGIC, iIsCheck1);

	// EMG 논리값을 확인 합니다.
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EMG_LOGIC, iIsCheck1);

	// INP 논리값과 사용 유무를 확인 합니다.
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck2);

	// ALM 논리값과 사용 유무를 확인 합니다.
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ALM_LOGIC, iIsCheck1);	

	// Hardware Limit 논리값을 확인 합니다.
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_LMT_LOGIC, iIsCheck1);

	// Encoder 방향을 확인합니다.
	iRet = pmiAxGetEncDir( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ENCODER_DIR, iIsCheck1);	

	// Home Move 방향을 확인합니다.
	iRet = pmiAxHomeGetDir( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_HOME_DIR, iIsCheck1);	

	// Pulse Mode를 확인합니다.	
	iRet = pmiAxGetPulseType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectPulseMode.SetCurSel(iIsCheck1);

	// Encoder Mode를 확인합니다.
	iRet = pmiAxGetEncType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectEncoderMode.SetCurSel(iIsCheck1);	

	// Home Mode를 확인합니다.	
	iRet = pmiAxHomeGetType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbHomeMode.SetCurSel(iIsCheck1);

	// Max Speed를 확인합니다.
	iRet = pmiAxGetMaxVel( axisCard, iAxisNo, &dbBuff1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.f"), dbBuff1 );
	SetDlgItemText(IDC_EDT_MAX_SPEED, strBuff);

	// Init Speed를 확인합니다.
	iRet = pmiAxGetInitVel( axisCard, iAxisNo, &dbBuff1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.f"), dbBuff1 );
	SetDlgItemText(IDC_EDT_INIT_SPEED, strBuff);

	// Jog Speed를 확인합니다.
	iRet = pmiAxGetJogVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.f"), dbBuff1 );
	SetDlgItemText(IDC_EDT_JOG_SPEED, strBuff);

	// Speed Mode를 확인합니다.
	iRet = pmiAxGetVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2, &dbBuff3 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSpeedMode.SetCurSel(iIsCheck1);

	strBuff.Format( _T("%.f"), dbBuff1 );
	SetDlgItemText(IDC_EDT_DRV_SPEED, strBuff);

	strBuff.Format( _T("%.f"), dbBuff2 );
	SetDlgItemText(IDC_EDT_ACC_TIME, strBuff);

	strBuff.Format( _T("%.f"), dbBuff3 );
	SetDlgItemText(IDC_EDT_DEC_TIME, strBuff);

	// Home Speed Mode를 확인합니다.
	iRet = pmiAxHomeGetVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2, &dbBuff3 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.f"), dbBuff2 );
	SetDlgItemText(IDC_EDT_REV_SPEED, strBuff);
}

void AxisMotion::OnBtnMultiIncmove() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 다축 상대 위치값 이송을 동시에 시작하는 버튼입니다.					//
	// +방향으로 입력한 값 만큼 이송 합니다.								//
	// 0번 축과 1번축을 동시에 시작합니다.									//
	//////////////////////////////////////////////////////////////////////////

	return;

	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[2] = {0};
	double dbarPositionList[2] = {0};
	CString strGetValue = _T("");

	iAxisNum = 2;

	iarAxisList[0] = 0;	// 첫번째 축을 0번축으로 설정합니다.
	iarAxisList[1] = 1;	// 두번태 축을 1번축으로 설정합니다.

	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbarPositionList[0] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.
	GetDlgItemText(IDC_EDT_POSITION2, strGetValue);
	dbarPositionList[1] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.

	SetPosMoveSpeed(iarAxisList[0]);	// 첫번째 축의 이송 속도 프로파일를 설정합니다.
	SetPosMoveSpeed(iarAxisList[1]);	// 두번째 축의 이송 속도 프로파일을 설정합니다.

	// 동시에 이송시작합니다.
	iRet = pmiMxPosMove( CARD_NO, iAxisNum, TMC_INC, iarAxisList, dbarPositionList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiAbsmove() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 다축 절대 위치값 이송을 동시에 시작하는 버튼입니다.					//
	// +방향으로 입력한 값 까지 이송 합니다.								//
	// 현재 CommandPos값이 입혁한 값과 동일하면 더이상 움직이지 않습니다.	//
	// 이 버튼에서는 0번 축과 1번축을 동시에 시작합니다.					//
	//////////////////////////////////////////////////////////////////////////

	return;

	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[2] = {0};
	double dbarPositionList[2] = {0};
	CString strGetValue = _T("");

	iAxisNum = 2;

	iarAxisList[0] = 0;	// 첫번째 축을 0번축으로 설정합니다.
	iarAxisList[1] = 1;	// 두번태 축을 1번축으로 설정합니다.

	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbarPositionList[0] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.
	GetDlgItemText(IDC_EDT_POSITION2, strGetValue);
	dbarPositionList[1] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.

	SetPosMoveSpeed(iarAxisList[0]);	// 첫번째 축의 이송 속도 프로파일를 설정합니다.
	SetPosMoveSpeed(iarAxisList[1]);	// 두번째 축의 이송 속도 프로파일을 설정합니다.

	// 동시에 이송시작합니다.
	iRet = pmiMxPosMove( CARD_NO, iAxisNum, TMC_ABS, iarAxisList, dbarPositionList );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiEstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축들을 동시에 감속 정지 시킵니다.								//
	// 정지 명령어를 사용한 후 반드시 Done() 또는 Multi_Done() 함수를		//
	// 사용하여 정지 여부를 확인 하십시요.									//
	// 이 버튼에서는 모든 축을 급정지 시킵니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[16] = {0};

	iAxisNum = m_iAxisNum;

	for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iarAxisList[AxisNo] = AxisNo;
	}

	iRet = pmiMxEStop( CARD_NO, iAxisNum, iarAxisList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiSstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축들을 동시에 감속정지 시킵니다.								//
	// 정지 명령어를 사용한 후 반드시 Done() 또는 Multi_Done() 함수를		//
	// 사용하여 정지 여부를 확인 하십시요.									//
	// 이 버튼에서는 모든 축을 감속정지 시킵니다.							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[16] = {0};

	iAxisNum = m_iAxisNum;

	for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iarAxisList[AxisNo] = AxisNo;
	}

	iRet = pmiMxStop( CARD_NO, iAxisNum, iarAxisList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnClearPos() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Command 위치 값과 Actual(Encoder) 위치값을 설정합니다.	//
	// 이 버튼은 0으로 설정 합니다.											//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int axisCard;
	//0909_KYS
	iAxisNo = m_CbAxisSelect.GetCurSel();
			//change card no for shuttle
	axisCard = CARD_NO;
#ifdef _DEBUG
		G_AddLog(3,L"OnBtnClearPos; called  %s, %d ", __FILE__, __LINE__);
#endif
     iRet = pmiAxSetCmdPos( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
		iRet = pmiAxSetActPos( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
		iRet = pmiAxSetPosError( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
}

void AxisMotion::OnBtnOverrideSpeed() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축이 이송 중일 때 운전 속도를 변경합니다.						//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbDriveSpeed = 0;	
	CString strValue = _T("");	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_OVERRIDE_SPEEDDATA, strValue);
	dbDriveSpeed = _tstof(strValue);

	iRet = pmiAxModifyVel( axisCard, iAxisNo, dbDriveSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnOverrideAbs() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축이 Position Move 중일 때 이송 위치를 변경합니다.			//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;	
	CString strValue = _T("");	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_OVERRIDE_POSITIONDATA, strValue);
	dbPosition = _tstof(strValue);

	iRet = pmiAxModifyPos( axisCard, iAxisNo, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnCclr() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 편차 카운터를 초기화 시킵니다.							//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	// 선택한 축의 편차 카운터 클리어 신호의 출력 시간을 설정 합니다.
	iRet = pmiAxSetServoCrcTime( axisCard, iAxisNo, 6);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 선택한 축의 편차 카운터 클리어 신호를 출력 합니다.
	// 설정한 시간 동안 On된 후 Off 됩니다.
	iRet = pmiAxSetServoCrcOn( axisCard, iAxisNo );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnSvonOnoff() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo On/Off 신호를 On/Off 합니다.						//
	// 현재 상태가 On이면 Off, Off면 On 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iOnOff = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iRet = pmiAxGetServoOn( axisCard, iAxisNo, &iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iOnOff = (iOnOff == 0) ? 1:0;
	iRet = pmiAxSetServoOn( axisCard, iAxisNo, iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnRstOnoff() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo Alarm Reset 신호를 On/Off 합니다.					//
	// 현재 상태가 On이면 Off, Off면 On 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iOnOff = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	iRet = pmiAxGetServoReset( axisCard, iAxisNo, &iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iOnOff = (iOnOff == 0) ? 1:0;
	iRet = pmiAxSetServoReset( axisCard, iAxisNo, iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnCbnSelchangeCbSpeedMode()
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Speed Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue;	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	axisCard = CARD_NO;
	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DEC_TIME, strGetValue);
	dbDecTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

CString AxisMotion::m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"";
	if(default != 0)
	{
		lpDefault = default;
	}

	bReturn = m_iniLCRAxisData.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";
		//G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}


int AxisMotion::applyAxisData(CString recipe)
{
	CString strRecipe, strRecipeName;
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;


	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == ".lax") //
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniLCRAxisData.SetFileName(finder.GetFilePath());
				//load recipe data in axisData struct
				strValueData = m_fnReadIniFile( SECTION_AXIS_DATA, KEY_IN_STEP,L"");
				axisData.steps = static_cast<int>(_wtof(strValueData));
				for(j=0; j<NUM_STEP ; j++)
				{
					for(i=0; i< NUM_AXIS_GRID_COL ; i++)
					{
						//strValueData = m_fnReadIniFile( ST_LCR_GRID_IN_ROW[j], ST_LCR_GRID_COL[i],L"");
						//axisData.absPos[j][i]= _wtof(strValueData);
					}
				}//for(j=0; j<NUM_STEP ; j++)
				break;
			}
		}//if(strRecipe.Right(4) == ".ini")
	}

	return 0;
}


int AxisMotion::homeLCRAxis(void)
{
	return 0;
}


int AxisMotion::homeShuttle(void)
{
	return 0;
}



BOOL AxisMotion::PreTranslateMessage(MSG* pMsg)
{
	int iRet = 0;
	int iAxisNo = 0;
	int iAxisNoInCard = 0;
	int axisCard;
	switch(pMsg->message)
	{
	case WM_LBUTTONDOWN:
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_PJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			axisCard = CARD_NO;
			SetJogMoveSpeed(iAxisNo);
			iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_P );
		}
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_NJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			axisCard = CARD_NO;
			SetJogMoveSpeed(iAxisNo);
			iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_N );
		}

		break;

	case WM_LBUTTONUP:
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_PJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			axisCard = CARD_NO;
			iRet = pmiAxStop( axisCard, iAxisNo );
		}
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_NJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			axisCard = CARD_NO;
			iRet = pmiAxStop( axisCard, iAxisNo );
		}

		break;

	default:
		break;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////////////


BOOL AxisMotion::m_fnSetPosMoveSpeed(int iAxisNo)
{
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");

	int axisCard;
	int iAxisNoInCard;

	//iMode = 2; //S-curve
	//iMode = 1; //T-curve
	//dbStartSpeed = 0; //1000;

	if (iAxisNo==0)
	{ 
		iMode = 1; //T-curve
		dbStartSpeed = 0; //1000;
	}
	else{
		iMode = 1; //T-curve
		dbStartSpeed = 0; //1000;
		}
	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	switch(iAxisNo){
		case Axis_Transfer:
			dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_AXIS_VEL];	
			dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_AXIS_TACC];
			dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_AXIS_TDEC];
			break;
		
		case Axis_OutCnvyr:
			dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_AXIS_VEL];
			dbAccTime = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_AXIS_TACC];
			dbDecTime = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_AXIS_TDEC];
			break;
	}

	iRet = pmiAxSetInitVel( axisCard, iAxisNoInCard, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxSetVelProf( axisCard, iAxisNoInCard, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	return TRUE;
}


BOOL AxisMotion::m_fnAbsmove(int iAxisNo, double nPos) 
{

	int axisCard;
	int iAxisNoInCard;

	int iRet = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");	
	CString str;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		return FALSE;
	}

	iRet = m_fnSetPosMoveSpeed(iAxisNo);
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxisNo) + L" Set Speed Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	iRet = pmiAxPosMove( axisCard, iAxisNoInCard, TMC_ABS, nPos);
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxisNo) + L" Move Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}


BOOL AxisMotion::m_fnIncmove(int iAxisNo, double nPos)
{

	int axisCard;
	int iAxisNoInCard;

	int iRet = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");
	CString str;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	if (G_SystemModeData.unSystemError != SYSTEM_OK){
		return FALSE;
	}

	iRet = m_fnSetPosMoveSpeed(iAxisNo);
	if (ReturnMessage(iRet) == FALSE){
		str = m_fnGetAxisName(iAxisNo) + L" Set Speed Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	iRet = pmiAxPosMove(axisCard, iAxisNoInCard, TMC_INC, nPos);
	if (ReturnMessage(iRet) == FALSE){
		str = m_fnGetAxisName(iAxisNo) + L" Move Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}


BOOL AxisMotion::m_fnSetHomeMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Home Move의 시작 속도, 운전속도, 가속도, 감속도를 설정합니다. 		//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbRevSpeed = 0;
	double dbAccTime = 0;
	double dbOffset = 0;
	CString strGetValue = _T("");

	int axisCard;
	int iAxisNoInCard;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;
	
	switch(iAxisNo){
		case Axis_Transfer:
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_HOME_DRIVE_SPEED];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_HOME_REV_SPEED];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_HOME_TACC];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_HOME_HOMEMODE];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IN_CONVEYOR_HOME_OFFSET];
			break;

		case Axis_OutCnvyr:
			dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_HOME_DRIVE_SPEED];
			dbRevSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_HOME_REV_SPEED];
			dbAccTime = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_HOME_TACC];
			iMode = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_HOME_HOMEMODE];
			dbOffset = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_OUT_CONVEYOR_HOME_OFFSET];
			break;
	}


	iRet = pmiAxHomeSetInitVel( axisCard, iAxisNoInCard, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxHomeSetVelProf( axisCard, iAxisNoInCard, 2, dbDriveSpeed, dbRevSpeed, dbAccTime );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	
	//////////////////////////////////////////////////////////////////////////
	// 원점 복귀 완료시 카운터 클리어을 클리어 한다.
	//////////////////////////////////////////////////////////////////////////
	iRet = pmiAxHomeSetResetPos ( axisCard, iAxisNoInCard, 1 );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	iRet = pmiAxHomeSetType ( axisCard, iAxisNoInCard, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	// Home Move 완료 후 Offset 위치를 설정 합니다.
	iRet = pmiAxHomeSetShiftDist( axisCard, iAxisNoInCard, dbOffset );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	return TRUE;
}

BOOL AxisMotion::m_fnHomemove(int iAxisNo) 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 원점 복귀 동작을 수행 합니다.(Home Move)		    	//
	//////////////////////////////////////////////////////////////////////////
	m_fnSoftLimitEnable(FALSE, iAxisNo);	

	int iRet = 0;	
	CString strGetValue = _T("");	
	int axisCard;
	int iAxisNoInCard;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	iRet =m_fnSetHomeMoveSpeed(iAxisNo);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = m_fnSoftLimitEnable(FALSE, iAxisNo);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	// Home Move를 시작 합니다.
	iRet = pmiAxHomeMove( axisCard, iAxisNoInCard );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;


	return TRUE;
}

BOOL AxisMotion::m_fnSoftLimitEnable(BOOL bUse, int iAxisNo, BOOL bStopMode)
{
	int axisCard;
	int iAxisNoInCard;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	int iRet = 0;
	if(bUse == FALSE)
	{	
		iRet = pmiAxSetSoftLimitEnable( axisCard, iAxisNoInCard, bUse);
		if(iAxisNo < NUM_AXIS) //LCR
			G_MainWnd->m_PIODlg.LCRLimitDisable = 1;
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;

		return TRUE;
	}

	int nPosMin, nPosMax;

	switch(iAxisNo){

		case Axis_Transfer:
			nPosMin = LIMIT_SHUTTLE_MIN;
			nPosMax = LIMIT_SHUTTLE_MAX;
			break;

		case Axis_OutCnvyr:
			nPosMin = LIMIT_OUT_CNVYR_MIN;
			nPosMax = LIMIT_OUT_CNVYR_MAX;
			break;

	}

	iRet = pmiAxSetSoftLimitPos( axisCard,  iAxisNoInCard, nPosMax, nPosMin);
	if(iAxisNo < NUM_AXIS) //LCR
		G_MainWnd->m_PIODlg.LCRLimitDisable = 0;
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxSetSoftLimitAction( axisCard, iAxisNoInCard, SOFT_LIMIT_E_STOP);

	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxSetSoftLimitEnable( axisCard, iAxisNoInCard, TRUE);

	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	
	return TRUE;

}

int AxisMotion::initAxis(int iAxisNo)
{
	int iRet = 0;
	int axisCard;
	int iAxisNoInCard;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		return 0;
	}

	int iIsCheck;

	// TODO: Add extra initialization here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ALM 논리와 사용 유무를 변경 합니다.
	// Enable이 1로 설정되면 서보에서 알람 신호 발생 시 즉시 정지 합니다.
	//////////////////////////////////////////////////////////////////////////
	//STEP
	if (axisCard == CARD_NO && iAxisNoInCard == AXIS_IN_CNVYR)
	{
		//inp
		iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, emLOGIC_B );

		iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck );
		iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck );

		//alm
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, emLOGIC_B );
		iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );

		//lmt
		iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );

		iRet = pmiAxSetServoAlarmAction ( axisCard, iAxisNoInCard, emESTOP );
		if( iRet != TMC_RV_OK) {
			AfxMessageBox(L"SetServoAlarmAction Failed.");
			return iRet;
		}
	}
	//
	else if (axisCard == CARD_NO && iAxisNoInCard == AXIS_OUT_CNVYR)
	{

		//inp
		iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, emLOGIC_B );

		iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck );
		iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck );

		//alm
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, emLOGIC_B );
		iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );

		//lmt
		iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );

		iRet = pmiAxSetServoAlarmAction ( axisCard, iAxisNoInCard, emESTOP );
		if( iRet != TMC_RV_OK) {
			AfxMessageBox(L"SetServoAlarmAction Failed.");
			return iRet;
		}
	}

	if( iRet != TMC_RV_OK) {
		//AfxMessageBox(_T("알람 레벨 함수 동작이 이상합니다.");
		return iRet;
	}
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo On/Off 신호를 On/Off 합니다
	// 현재 상태가 On이면 Off, Off면 On 합니다.
	//////////////////////////////////////////////////////////////////////////
	//iRet = pmiAxSetServoOn( axisCard, iAxisNoInCard, emTRUE);
	iRet = pmiAxSetServoOn(axisCard, iAxisNoInCard, emFALSE);
	if( iRet != TMC_RV_OK) {
		AfxMessageBox(L"SetServoOn FAILED.");
		return iRet;
	}
	return iRet;
}

int AxisMotion::m_fnAxCheckDone(int iAxisNo, int * nDone, int nWait)
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	do {
		Sleep(100);
		pmiAxCheckDone(axisCard, iAxisNoInCard, nDone /*&iRet*/);
		if(cnt++ >=  nWait) return 0;
		if(*nDone == emRUNNING) Sleep(100);
	}while(*nDone == emRUNNING);
	//*nDone = iRet;
	return 1;
}


int AxisMotion::m_fnAxHomeCheckDone(int iAxisNo, int * nDone, int nWait)
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	do {
		pmiAxHomeCheckDone(axisCard, iAxisNoInCard, nDone /*&iRet*/);
		if(cnt++ >=  nWait) return 0;
		if(*nDone == emRUNNING)Sleep(100);
	}while(*nDone == emRUNNING);
	//*nDone = iRet;

	return TRUE;
}


BOOL AxisMotion::m_fnClearPos(int iAxisNo) 
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	int iRet = 0;
	//for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iRet = pmiAxSetCmdPos( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
		iRet = pmiAxSetActPos( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
		iRet = pmiAxSetPosError( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
	}

	return TRUE;
}


int AxisMotion::m_fnAxisHome(int iAxis)
{
	CString str;
	int iRet = 0;
	int nDone;
	int nWait = 300;//20sec

	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		return FALSE;
	}

	iRet = m_fnHomemove(iAxis); 
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxis) + L"Home Move Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
	if( ReturnMessage(iRet) == FALSE ){
			str = m_fnGetAxisName(iAxis) + L"Home Check Done Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	if(nDone == emSTAND){
#ifdef _DEBUG
		G_AddLog(3,L"m_fnClearPos(%d); called  Line: %d ", iAxis, __LINE__);
#endif
		iRet = m_fnClearPos(iAxis); 
		if( ReturnMessage(iRet) == FALSE ){
				str =m_fnGetAxisName(iAxis) + L"Home Clear Position Error";
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	iRet = m_fnSoftLimitEnable(TRUE, iAxis);
	if( ReturnMessage(iRet) == FALSE ){
		str =m_fnGetAxisName(iAxis) + L"Home Soft Limit Enable Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}

CString AxisMotion::m_fnGetAxisName(int iAxis)
{
	CString strLCRAxis;
	switch(iAxis){
	case 	Axis_Transfer:	strLCRAxis.Format(L"SHUTTLE ");	break;	
	case	Axis_OutCnvyr:strLCRAxis.Format(L"OUT CONVEYOR ");	break;	
	}
	return strLCRAxis;
}


void AxisMotion::OnBnClickedBtnLcrAxisAlarmReset(UINT nID)
{
	int iRet;
	int iAxisNo;
	int iIsCheck;

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"IT CAN BE DONE IN MANUAL MODE",MB_ICONHAND);
		return;
	}

	//iAxisNo = nID - IDC_BTN_LCR_AXIS21;
	iAxisNo = 0 ;
	int axisCard;
	int iAxisNoInCard;

	axisCard = CARD_NO;
	iAxisNoInCard = iAxisNo;

	//emg
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck );
	if(iIsCheck){
		iRet = pmiGnSetEmgLevel( axisCard, emLOGIC_B );
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNoInCard, &iIsCheck );
	if(iIsCheck){
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNoInCard, emLOGIC_A );
	}

	//lmt
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNoInCard, &iIsCheck );
	if(iIsCheck){
		iRet = pmiAxSetLimitLevel( axisCard, iAxisNoInCard, emLOGIC_A );
	}
}

void AxisMotion::OnBnClickedBtnShuttleAlarmReset()
{
	int iRet;
	int iIsCheck;

	//SHUTTLE

	//emg
	iRet = pmiGnGetEmgLevel( CARD_NO, &iIsCheck );
	if(iIsCheck){
		iRet = pmiGnSetEmgLevel( CARD_NO, emLOGIC_B );
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel(CARD_NO, AXIS_IN_CNVYR, &iIsCheck);
	if(iIsCheck){
		iRet = pmiAxSetServoAlarmLevel(CARD_NO, AXIS_IN_CNVYR, emLOGIC_B);
	}

	//lmt
	iRet = pmiAxGetLimitLevel(CARD_NO, AXIS_IN_CNVYR, &iIsCheck);
	if(iIsCheck){
		iRet = pmiAxSetLimitLevel(CARD_NO, AXIS_IN_CNVYR, emLOGIC_B);
	}

}


int AxisMotion::m_fnDisplayShuttleStatus(void)
{
	int iRet = 0;

	//INPUT
	iRet = pmiAxGetMechanical(CARD_NO, AXIS_IN_CNVYR, &udtShuttleStatus.mem);
	if( ReturnMessage(iRet) == FALSE )
	{
		m_btnInCnvyrReset.SetColorChange(WHITE, BLACK);
	}	
	//Status
	if( !udtShuttleStatus.mem_bit.SON && !udtCardStatus.mem_bit.EMG  && !udtShuttleStatus.mem_bit.ALM && 
		!udtShuttleStatus.mem_bit.P_LMT && !udtShuttleStatus.mem_bit.N_LMT)
	{//READY
		m_btnInCnvyrReset.SetColorChange(GREEN, BLACK);
	}else{
		m_btnInCnvyrReset.SetColorChange(RED, BLACK);
	}

	//OUTPUT
	iRet = pmiAxGetMechanical(CARD_NO, AXIS_OUT_CNVYR, &udtShuttleStatus.mem);
	if (ReturnMessage(iRet) == FALSE)
	{
		m_btnOutCnvyrReset.SetColorChange(WHITE, BLACK);
	}
	//Status
	if (!udtShuttleStatus.mem_bit.SON && !udtCardStatus.mem_bit.EMG  && !udtShuttleStatus.mem_bit.ALM &&
		!udtShuttleStatus.mem_bit.P_LMT && !udtShuttleStatus.mem_bit.N_LMT)
	{//READY
		m_btnOutCnvyrReset.SetColorChange(GREEN, BLACK);
	}
	else{
		m_btnOutCnvyrReset.SetColorChange(RED, BLACK);
	}

	//Position
//	double dbActPos;
	//double AXIS_POS_TOLERANCE, shuttleFWD, shuttleRET;
	//shuttleFWD = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD];
	//shuttleRET = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN];
	//AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];
	//iRet = pmiAxGetActPos(CARD_NO, AXIS_IN_CNVYR, &dbActPos);


	
	return 0;
}

void AxisMotion::OnBnClickedBtnOutCnvyorSet()
{
	CString strVal;
	double dIncrement = 0.0f;

	//strVal.Format(_T("%.f"), dbActPos);
	GetDlgItem(IDC_EDIT_CNVYOR_INCREMENT)->GetWindowText(strVal);
	dIncrement = _wtof(strVal);
	//str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
	G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_OUT_INC] = (dIncrement);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[CVYR_OUT_INC], strVal);

	m_staticOutCnvyorInc.SetText(strVal);

}


void AxisMotion::OnBnClickedBtnCnvyorMove()
{
	int iRet;
	int nDone;

	if (G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"IT CAN BE DONE IN MANUAL MODE", MB_ICONHAND);
		return;
	}

	if (IDCANCEL == AfxMessageBox(L"CONVEYOR 1 INCREMENT MOVE ?", MB_OKCANCEL | MB_ICONQUESTION))
		return;

	//clear
	m_fnClearPos(AXIS_OUT_CNVYR);
	iRet = m_fnIncmove(AXIS_OUT_CNVYR, G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_OUT_INC]); ////////////////////////////////////////
	if (iRet == 1)
	{
		iRet = m_fnAxCheckDone(AXIS_OUT_CNVYR, &nDone, 100); //10sec
		if (iRet == 1){//OK
			AfxMessageBox(L"CONVEYOR INC MOVE DONE", MB_ICONASTERISK);
		}
		else{//NG
			AfxMessageBox(L"CONVEYOR INC MOVE FAILED", MB_ICONASTERISK);
		}
	}
	else{
		AfxMessageBox(L"CONVEYOR INC MOVE FAILED", MB_ICONASTERISK);
	}
}

//SET BUTTON ENABLE
void AxisMotion::OnBnClickedButton1()
{
	m_btnOutCnvyorIncSet.EnableWindow(TRUE);
	m_btnAxisSetEnable.EnableWindow(FALSE);
	m_btnAxisSetDisable.EnableWindow(TRUE);
}

//SET BUTTON DISABLE
void AxisMotion::OnBnClickedButton9()
{
	m_btnOutCnvyorIncSet.EnableWindow(FALSE);
	m_btnAxisSetEnable.EnableWindow(TRUE);
	m_btnAxisSetDisable.EnableWindow(FALSE);
}

//IN CNVYR
void AxisMotion::OnBnClickedAxisAlarmReset2()
{
	int iRet;
	int iIsCheck;

	//SHUTTLE

	//emg
	iRet = pmiGnGetEmgLevel(CARD_NO, &iIsCheck);
	if (iIsCheck){
		iRet = pmiGnSetEmgLevel(CARD_NO, emLOGIC_B);
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel(CARD_NO, AXIS_IN_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetServoAlarmLevel(CARD_NO, AXIS_IN_CNVYR, emLOGIC_B);
	}

	//lmt
	iRet = pmiAxGetLimitLevel(CARD_NO, AXIS_IN_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetLimitLevel(CARD_NO, AXIS_IN_CNVYR, emLOGIC_B);
	}
}


void AxisMotion::OnBnClickedAxisAlarmReset()
{
	int iRet;
	int iIsCheck;

	//SHUTTLE

	//emg
	iRet = pmiGnGetEmgLevel(CARD_NO, &iIsCheck);
	if (iIsCheck){
		iRet = pmiGnSetEmgLevel(CARD_NO, emLOGIC_B);
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel(CARD_NO, AXIS_OUT_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetServoAlarmLevel(CARD_NO, AXIS_OUT_CNVYR, emLOGIC_B);
	}

	//lmt
	iRet = pmiAxGetLimitLevel(CARD_NO, AXIS_OUT_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetLimitLevel(CARD_NO, AXIS_OUT_CNVYR, emLOGIC_B);
	}
}


void AxisMotion::OnBnClickedButton10()
{
	m_btnInCnvyorIncSet.EnableWindow(TRUE);
	m_btnAxisSetEnable2.EnableWindow(FALSE);
	m_btnAxisSetDisable2.EnableWindow(TRUE);
}


void AxisMotion::OnBnClickedButton11()
{
	m_btnInCnvyorIncSet.EnableWindow(FALSE);
	m_btnAxisSetEnable2.EnableWindow(TRUE);
	m_btnAxisSetDisable2.EnableWindow(FALSE);
}


void AxisMotion::OnBnClickedBtnCnvyorMove2()
{
	int iRet;
	int nDone;

	if (G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"IT CAN BE DONE IN MANUAL MODE", MB_ICONHAND);
		return;
	}

	if (IDCANCEL == AfxMessageBox(L"CONVEYOR 1 INCREMENT MOVE ?", MB_OKCANCEL | MB_ICONQUESTION))
		return;

	//clear
	m_fnClearPos(AXIS_IN_CNVYR);
	iRet = m_fnIncmove(AXIS_IN_CNVYR, G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_IN_INC]); ////////////////////////////////////////
	if (iRet == 1)
	{
		iRet = m_fnAxCheckDone(AXIS_IN_CNVYR, &nDone, 100); //10sec
		if (iRet == 1){//OK
			AfxMessageBox(L"CONVEYOR INC MOVE DONE", MB_ICONASTERISK);
		}
		else{//NG
			AfxMessageBox(L"CONVEYOR INC MOVE FAILED", MB_ICONASTERISK);
		}
	}
	else{
		AfxMessageBox(L"CONVEYOR INC MOVE FAILED", MB_ICONASTERISK);
	}
}


void AxisMotion::OnBnClickedBtnCnvyorSet2()
{
	CString strVal;
	double dIncrement = 0.0f;

	//strVal.Format(_T("%.f"), dbActPos);
	GetDlgItem(IDC_EDIT_CNVYOR_INCREMENT2)->GetWindowText(strVal);
	dIncrement = _wtof(strVal);
	//str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
	G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_IN_INC] = dIncrement;
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[CVYR_IN_INC], strVal);

	m_staticInCnvyorInc.SetText(strVal);

}


void AxisMotion::OnBnClickedAxisClear2()
{
	m_fnClearPos(AXIS_IN_CNVYR);
}


void AxisMotion::OnBnClickedAxisClear()
{
	m_fnClearPos(AXIS_OUT_CNVYR);
}


void AxisMotion::OnBnClickedAxisAlarmReset3()
{
	int iRet;
	int iIsCheck;

	//emg
	iRet = pmiGnGetEmgLevel(CARD_NO, &iIsCheck);
	if (iIsCheck){
		iRet = pmiGnSetEmgLevel(CARD_NO, emLOGIC_B);
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel(CARD_NO, AXIS_OUT_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetServoAlarmLevel(CARD_NO, AXIS_OUT_CNVYR, emLOGIC_B);
	}

	//lmt
	iRet = pmiAxGetLimitLevel(CARD_NO, AXIS_OUT_CNVYR, &iIsCheck);
	if (iIsCheck){
		iRet = pmiAxSetLimitLevel(CARD_NO, AXIS_OUT_CNVYR, emLOGIC_B);
	}
}
