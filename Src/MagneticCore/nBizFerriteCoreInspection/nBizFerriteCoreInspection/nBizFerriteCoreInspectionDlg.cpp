
// nBizFerriteCoreInspectionDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "nBizFerriteCoreInspection.h"
#include "nBizFerriteCoreInspectionDlg.h"
#include "afxdialogex.h"
#include "PassWordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CnBizFerriteCoreInspectionDlg 대화 상자
extern CMainWnd* G_MainWnd;

CnBizFerriteCoreInspectionDlg::CnBizFerriteCoreInspectionDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CnBizFerriteCoreInspectionDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	memset(paintImageReady, 0x00, sizeof(int)*4);
}

void CnBizFerriteCoreInspectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG_VIEW, m_listLogView);
	DDX_Control(pDX, IDC_PIC, m_pcMainView);
	DDX_Control(pDX, IDC_BTN_AUTO, m_btnAuto);
	DDX_Control(pDX, IDC_BTN_MANU, m_btnManual);
	DDX_Control(pDX, IDC_OPERATOR_MODE, m_OperatorMode);
	DDX_Control(pDX, IDC_STATIC_1, m_static1);
	DDX_Control(pDX, IDC_STATIC_2, m_static2);
	DDX_Control(pDX, IDC_STATIC_3, m_static3);
	DDX_Control(pDX, IDC_STATIC_4, m_static4);
	DDX_Control(pDX, IDC_STATIC_5, m_static5);
	DDX_Control(pDX, IDC_STATIC_MODEL, m_staticModel);
	DDX_Control(pDX, IDC_STATIC_TYPE, m_staticType);
	DDX_Control(pDX, IDC_STATIC_ACCU, m_staticAccu);
	DDX_Control(pDX, IDC_STATIC_LOT, m_staticLOT);
	DDX_Control(pDX, IDC_STATIC_TARGET, m_staticTarget);

	DDX_Control(pDX, IDC_STATIC_TARGET2, m_staticTargetPercent);
	DDX_Control(pDX, IDC_STATIC_7, m_static7);
	DDX_Control(pDX, IDC_STATIC_8, m_static8);
	DDX_Control(pDX, IDC_STATIC_9, m_staticTotal);
	DDX_Control(pDX, IDC_STATIC_19, m_static19);
	DDX_Control(pDX, IDC_STATIC_20, m_staticNGRatio);
	DDX_Control(pDX, IDC_STATIC_10, m_staticOKNG);
	DDX_Control(pDX, IDC_BTN_MODE, m_btnMode);
	DDX_Control(pDX, IDC_BTN_STOP, m_btnStop);

	DDX_Control(pDX, IDC_STATIC_11, m_static11);
	DDX_Control(pDX, IDC_STATIC_12, m_static12);
	DDX_Control(pDX, IDC_TIME_AUTO_START, m_staticTimeMasterStart);
	DDX_Control(pDX, IDC_TIME_ELLAPSED, m_staticTimeMasterElapsec);
	DDX_Control(pDX, IDC_KYS_TEST, m_staticThreadFlag);
	DDX_Control(pDX, IDC_BTN_START, m_btnStart);
	DDX_Control(pDX, IDC_BTN_HOME, m_btnHome);
	DDX_Control(pDX, IDC_ERROR_MSG, m_ErrorMsg);
	DDX_Control(pDX, IDC_STATIC_13, m_staticOK);
	DDX_Control(pDX, IDC_STATIC_14, m_staticNG);
	DDX_Control(pDX, IDC_BTN_HOLD_SHUTTLE, m_btnHoldSHUTTLE);
	DDX_Control(pDX, IDC_BTN_BUZZSTOP, m_btnBuzzStop);
	DDX_Control(pDX, IDC_MASTERTEST_OK, m_masterTest_OK);
	DDX_Control(pDX, IDC_MASTERTEST_NG, m_masterTest_NG);
	DDX_Control(pDX, IDC_BTN_MASTER_OK2, m_masterTest_OK_Start);
	DDX_Control(pDX, IDC_BTN_MASTER_NG2, m_masterTest_NG_Start);
	DDX_Control(pDX, IDC_STATIC_15, m_staticInsp);
	//for (int id = 0; id < CAM_NO; id++)
	for (int id = 0; id < 4; id++)
	{
		DDX_Control(pDX, IDC_BTN_CAM1 + id, m_btnCamStatus[id]);
	}
	DDX_Control(pDX, IDC_STATIC_6, m_static6);
	DDX_Control(pDX, IDC_STATIC_16, m_static16);
	DDX_Control(pDX, IDC_STATIC_17, m_static17);
	DDX_Control(pDX, IDC_STATIC_18, m_static18);
	for (int id = 0; id < 4; id++)
	{
		DDX_Control(pDX, IDC_STATIC_INNER_RING + id, m_staticDefects[id]);
	}

	DDX_Control(pDX, IDC_BTN_HOLD_SHUTTLE2, m_btnOutCnvyrHold);
	DDX_Control(pDX, IDC_LIST_DEFECT_VIEW, m_listDefectView);
	DDX_Control(pDX, IDC_PC_LOGO, m_pcLogo);
	DDX_Control(pDX, IDC_BUTTON2, m_btnClear);
}

BEGIN_MESSAGE_MAP(CnBizFerriteCoreInspectionDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_MESSAGE(UM_UI_UPDATE, &CnBizFerriteCoreInspectionDlg::OnUiUpdateCmd)
	ON_BN_CLICKED(IDC_BTN_AUTO, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnAuto)
	ON_BN_CLICKED(IDC_BTN_MANU, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnManu)
	ON_BN_CLICKED(IDC_BTN_MODE, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMode)
	ON_COMMAND(ID_MENU_SYSTEM, &CnBizFerriteCoreInspectionDlg::OnMenuSystem)
	ON_COMMAND(ID_MENU_MODEL, &CnBizFerriteCoreInspectionDlg::OnMenuModel)
	ON_COMMAND(ID_MENU_AXIS, &CnBizFerriteCoreInspectionDlg::OnMenuAxis)
	ON_COMMAND(ID_MENU_IO, &CnBizFerriteCoreInspectionDlg::OnMenuIo)
	ON_COMMAND(ID_HELP_ABOUT, &CnBizFerriteCoreInspectionDlg::OnHelpAbout)
	ON_COMMAND(ID_FILE_EXIT, &CnBizFerriteCoreInspectionDlg::OnFileExit)
	ON_BN_CLICKED(IDC_BTN_START, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_HOME, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnErrorReset)

	ON_BN_CLICKED(IDC_BTN_HOLD_SHUTTLE, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnHoldSHUTTLE)
	ON_BN_CLICKED(IDC_BTN_BUZZSTOP, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnBuzzstop)
	ON_BN_CLICKED(IDC_BTN_MASTER_OK2, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMasterOk2)
	ON_BN_CLICKED(IDC_BTN_MASTER_NG2, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMasterNg2)
	ON_WM_INPUT()
	ON_COMMAND(ID_ENVIRONMENT_VISIONINSPECTION, &CnBizFerriteCoreInspectionDlg::OnEnvironmentVisioninspection)
	ON_COMMAND_RANGE(IDC_BTN_CAM1, IDC_BTN_CAM4, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnCam)

	ON_BN_CLICKED(IDC_BTN_HOLD_SHUTTLE2, &CnBizFerriteCoreInspectionDlg::OnBnClickedBtnHoldShuttle2)
	ON_BN_CLICKED(IDC_BUTTON2, &CnBizFerriteCoreInspectionDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_CHECK1, &CnBizFerriteCoreInspectionDlg::OnBnCheckedCrossLine)
END_MESSAGE_MAP()


// CnBizFerriteCoreInspectionDlg 메시지 처리기

BOOL CnBizFerriteCoreInspectionDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}


	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);		

	
	G_MainWnd->m_DataHandling.nDayReportNum = 1;

	//SetTimer(TIMER_CONNECT_CHECK, 500, NULL);
	SetTimer(TIMER_IMAGE_DIR, 1000, NULL);	
	SetTimer(TIMER_AUTO_DIR_DELETE, 60000, NULL); //tact time

	m_OperatorMode.SetFont2(20, 8, 7);
	m_static1.SetFont2(20, 8, 7);	m_static1.SetText(L"MODEL", BLACK, RGB(180,190,225));	
	m_static2.SetFont2(20, 8, 7);	m_static2.SetText(L"TYPE", BLACK, RGB(180,190,225));	
	m_static3.SetFont2(20, 8, 7);	m_static3.SetText(L"ACCUMULATION", BLACK, RGB(180,190,225));	
	m_static4.SetFont2(20, 8, 7);	m_static4.SetText(L"LOT NO.", BLACK, RGB(180,190,225));	
	m_static5.SetFont2(20, 8, 7);	m_static5.SetText(L"LOT TARGET", BLACK, RGB(180,190,225));	

	m_static7.SetFont2(20, 8, 7);	m_static7.SetText(L"TOTAL", BLACK, RGB(180,190,225));	
	m_static8.SetFont2(20, 8, 7);	m_static8.SetText(L"OK/NG", BLACK, RGB(180,190,225));	
	m_static19.SetFont2(20, 8, 7);	m_static19.SetText(L"NG RATIO", BLACK, RGB(180, 190, 225));
	m_static11.SetFont2(20, 8, 7);	m_static11.SetText(L"AUTO RUN FROM", BLACK, RGB(180, 190, 225));
	m_static12.SetFont2(20, 8, 7);	m_static12.SetText(L"TACT TIME", BLACK, RGB(180,190,225));	
	m_staticOK.SetFont2(50, 25, 20);	m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
	m_staticNG.SetFont2(50, 25, 20);	m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));

	m_staticInsp.SetFont2(40, 20, 16);	//m_staticInsp.SetText(G_MainWnd->m_DataHandling.m_SystemParam.LINE_ID, BLACK, WHITE); 
	
	m_staticModel.SetFont2(20, 8, 7); m_staticModel.SetText(L"", BLACK, RGB(230,230,230));
	m_staticType.SetFont2(20, 8, 7);	m_staticType.SetText(L"", BLACK, RGB(230,230,230));
	m_staticAccu.SetFont2(20, 8, 7);	m_staticAccu.SetText(L"", BLACK, RGB(230,230,230));
	m_staticLOT.SetFont2(20, 8, 7);		m_staticLOT.SetText(L"", BLACK, RGB(230, 230, 230));
	m_staticTarget.SetFont2(20, 8, 7);	m_staticTarget.SetText(L"", BLACK, RGB(230,230,230));

	m_staticTargetPercent.SetFont2(20, 8, 7);	m_staticTargetPercent.SetText(L"", BLACK, RGB(230, 230, 230));
	m_staticTotal.SetFont2(20, 8, 7); m_staticTotal.SetText(L"", BLACK, RGB(230,230,230));
	m_staticOKNG.SetFont2(20, 8, 7);	m_staticOKNG.SetText(L"", BLACK, RGB(230,230,230));
	m_staticNGRatio.SetFont2(20, 8, 7);	m_staticNGRatio.SetText(L"", BLACK, RGB(230, 230, 230));
	m_ErrorMsg.SetFont2(25, 12, 10);	m_ErrorMsg.SetText(L"", BLACK, RGB(230, 230, 230));

	m_staticTimeMasterStart.SetFont2(20, 8, 7);	m_staticTimeMasterStart.SetText(L"", WHITE, BLACK);
	m_staticTimeMasterElapsec.SetFont2(20, 8, 7);	m_staticTimeMasterElapsec.SetText(L"", WHITE, BLACK);
	m_staticThreadFlag.SetFont2(20, 8, 7);	m_staticThreadFlag.SetText(L"", WHITE, BLACK);

	m_masterTest_OK.SetLedStatesNumber(LIGHT_STATE_SENTINEL);	
	m_masterTest_OK.SetIcon(GRAY_LIGHT, IDI_ICON_NONE, 24, 24);
	m_masterTest_OK.SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 24, 24);
	m_masterTest_OK.SetIcon(RED_LIGHT, IDI_ICON_RED, 24, 24);
	m_masterTest_NG.SetLedStatesNumber(LIGHT_STATE_SENTINEL);	
	m_masterTest_NG.SetIcon(GRAY_LIGHT, IDI_ICON_NONE, 24, 24);
	m_masterTest_NG.SetIcon(RED_LIGHT, IDI_ICON_RED, 24, 24);
	m_masterTest_NG.SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 24, 24);

	m_static6.SetFont2(20, 8, 7);	m_static6.SetText(L"INNER RING", BLACK, RGB(180, 190, 225));
	m_static16.SetFont2(20, 8, 7);	m_static16.SetText(L"BOTTOM FACE", BLACK, RGB(180, 190, 225));
	m_static17.SetFont2(20, 8, 7);	m_static17.SetText(L"OUTER RING", BLACK, RGB(180, 190, 225));
	m_static18.SetFont2(20, 8, 7);	m_static18.SetText(L"TOP FACE", BLACK, RGB(180, 190, 225));

	m_staticDefects[0].SetFont2(20, 8, 7);		m_staticDefects[0].SetText(L"", BLACK, RGB(230, 230, 230));
	m_staticDefects[1].SetFont2(20, 8, 7);		m_staticDefects[1].SetText(L"", BLACK, RGB(230, 230, 230));
	m_staticDefects[2].SetFont2(20, 8, 7);		m_staticDefects[2].SetText(L"", BLACK, RGB(230, 230, 230));
	m_staticDefects[3].SetFont2(20, 8, 7);		m_staticDefects[3].SetText(L"", BLACK, RGB(230, 230, 230));

	m_opMode = MODE_OPERATOR;
	m_OperatorMode.SetText(L"OPERATOR MODE", BLACK, RGB(255,240,255));	
	m_bDrawCrossLine = TRUE;
	CheckDlgButton(IDC_CHECK1, TRUE);
	return TRUE;  
}

void CnBizFerriteCoreInspectionDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}


void CnBizFerriteCoreInspectionDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this);
		//SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);
		
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDC* pDC;
		CRect rect;
		pDC = m_pcMainView.GetDC();
		m_pcMainView.GetClientRect(&rect);
		if (G_MainWnd != NULL && G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData !=NULL)
		{
			//2016.04.04 jec add
			if (paintImageReady[CAM_PCHI] == 0 &&
				paintImageReady[CAM_PCCA] == 0 &&
				paintImageReady[CAM_PCCA2] == 0 &&
				paintImageReady[CAM_PCHI2] == 0 &&
				G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData->LiveGrabCamsRun != 0 &&
				G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA].pImgData->LiveGrabCamsRun != 0 &&
				G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA2].pImgData->LiveGrabCamsRun != 0 &&
				G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI2].pImgData->LiveGrabCamsRun != 0)
			{
				paintImageReady[CAM_PCHI] = 1;
				paintImageReady[CAM_PCCA] = 1;
				paintImageReady[CAM_PCCA2] = 1;
				paintImageReady[CAM_PCHI2] = 1;
			}
			//****************************************
		}

		CRect rect14, rect24, rect34, rect44;

		if (paintImageReady[CAM_PCHI])
		{
			rect14.top = rect.top;
			rect14.bottom = rect.top + static_cast<int>(rect.Height() / 2);
			rect14.left = rect.left;
			rect14.right = rect.left + static_cast<int>(rect.Width() / 2);
			DisplayIplImg(pImgBuf[CAM_PCHI], pDC, rect14, CAM_PCHI);
		}
		if (paintImageReady[CAM_PCCA])
		{
			rect24.top = rect.top;
			rect24.bottom = rect.top + static_cast<int>(rect.Height() / 2);
			rect24.left = rect.left + static_cast<int>(rect.Width() / 2);
			rect24.right = rect.right;
			DisplayIplImg(pImgBuf[CAM_PCCA], pDC, rect24, CAM_PCCA);
		}
		if (paintImageReady[CAM_PCCA2])
		{
			rect34.top = rect.top + static_cast<int>(rect.Height() / 2);
			rect34.bottom = rect.bottom;
			rect34.left = rect.left;
			rect34.right = rect.left + static_cast<int>(rect.Width() / 2);
			DisplayIplImg(pImgBuf[CAM_PCCA2], pDC, rect34, CAM_PCCA2);
		}
		if (paintImageReady[CAM_PCHI2])
		{
			rect44.top = rect.top + static_cast<int>(rect.Height() / 2);
			rect44.bottom = rect.bottom;
			rect44.left = rect.left + static_cast<int>(rect.Width() / 2);
			rect44.right = rect.right;
			DisplayIplImg(pImgBuf[CAM_PCHI2], pDC, rect44, CAM_PCHI2);
		}
		ReleaseDC(pDC);
	}
	CDialogEx::OnPaint();
}


BOOL CnBizFerriteCoreInspectionDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(pMsg->message)
	{
	case WM_KEYDOWN:
		switch(pMsg->wParam)
		{ 
		case VK_RETURN:
			return TRUE;
			break;
		case VK_ESCAPE:			
			return TRUE;
			break;
		case VK_F7:
			break;
		}
	case WM_SYSKEYDOWN:
		switch(pMsg->wParam)
		{ 
		case VK_F4:			
			return TRUE;
			break;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CnBizFerriteCoreInspectionDlg::DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect, int CamNo)
{
	if (pImgIpl == NULL) return;
		     
	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biPlanes=1;
	bitmapInfo.bmiHeader.biCompression=BI_RGB;
	bitmapInfo.bmiHeader.biXPelsPerMeter=100;
	bitmapInfo.bmiHeader.biYPelsPerMeter=100;
	bitmapInfo.bmiHeader.biClrUsed=0;
	bitmapInfo.bmiHeader.biClrImportant=0;
	bitmapInfo.bmiHeader.biSizeImage=0;
	bitmapInfo.bmiHeader.biWidth=pImgIpl->width;
	bitmapInfo.bmiHeader.biHeight=-pImgIpl->height;
	IplImage* tempImage;
	if(pImgIpl->nChannels == 3)
	{
		tempImage = (IplImage*)cvClone(pImgIpl);
		bitmapInfo.bmiHeader.biBitCount=tempImage->depth * tempImage->nChannels;
	}
	else if(pImgIpl->nChannels == 1)
	{
		tempImage =  cvCreateImage(cvGetSize(pImgIpl), IPL_DEPTH_8U, 3);
		cvCvtColor(pImgIpl, tempImage, CV_GRAY2BGR);
		bitmapInfo.bmiHeader.biBitCount=tempImage->depth * tempImage->nChannels;
	}
	//20160319_KYS
	IplImage* LinedImage = cvCloneImage(tempImage);
	if (m_bDrawCrossLine)
	{
		cvLine(LinedImage, cvPoint(LinedImage->width / 2, 0), cvPoint(LinedImage->width / 2, LinedImage->height), CV_RGB(255, 0, 0), 3, 8, 0);
		cvLine(LinedImage, cvPoint(0, LinedImage->height / 2), cvPoint(LinedImage->width, LinedImage->height / 2), CV_RGB(255, 0, 0), 3, 8, 0);
	}
	//
	//
#ifdef kys_0401	
	char *www;
	char *ttt;
	//20160401_kys
	if (CamNo==CAM_PCHI)
	{
		www = "CAM1";
		CvFont FontKYS;
		cvInitFont(&FontKYS, CV_FONT_HERSHEY_DUPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 3, 3, 0, 6);
		cvPutText(LinedImage, www, cvPoint(25, 100), &FontKYS, CV_RGB(255, 0, 0));
		if (G_MainWnd->m_InspectThread.m_vi.m_IsCalibrationErr[0] == 1)
		{
			ttt = "CAL-ERR";
			cvInitFont(&FontKYS, CV_FONT_HERSHEY_TRIPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 1.5, 2, 0, 4);
			cvPutText(LinedImage, ttt, cvPoint(25, 200), &FontKYS, CV_RGB(0, 255, 0));
		}
		cvDrawRect(LinedImage, cvPoint(4, 4), cvPoint(LinedImage->width-4, LinedImage->height-4), CV_RGB(0, 255, 0),3);
		paintImageReady[CAM_PCHI] = 0;
	}
	if (CamNo==CAM_PCCA)
	{
		www = "CAM2";
		CvFont FontKYS;
		cvInitFont(&FontKYS, CV_FONT_HERSHEY_DUPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 3, 3, 0, 6);
		cvPutText(LinedImage, www, cvPoint(25, 100), &FontKYS, CV_RGB(255, 0, 0));
		if (G_MainWnd->m_InspectThread.m_vi.m_IsCalibrationErr[1] == 1)
		{
			ttt = "CAL-ERR";
			cvInitFont(&FontKYS, CV_FONT_HERSHEY_TRIPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 1.5, 2, 0, 4);
			cvPutText(LinedImage, ttt, cvPoint(25, 200), &FontKYS, CV_RGB(0, 255, 0));
		}
		cvDrawRect(LinedImage, cvPoint(4, 4), cvPoint(LinedImage->width - 4, LinedImage->height - 4), CV_RGB(0, 255, 0), 3);
		paintImageReady[CAM_PCCA] = 0;
	}
	if (CamNo==CAM_PCCA2)
	{
		www = "CAM3";
		CvFont FontKYS;
		cvInitFont(&FontKYS, CV_FONT_HERSHEY_DUPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 3, 3, 0, 6);
		cvPutText(LinedImage, www, cvPoint(25, 100), &FontKYS, CV_RGB(255, 0, 0));
		if (G_MainWnd->m_InspectThread.m_vi.m_IsCalibrationErr[2] == 1)
		{
			ttt = "CAL-ERR";
			cvInitFont(&FontKYS, CV_FONT_HERSHEY_TRIPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 1.5, 2, 0, 4);
			cvPutText(LinedImage, ttt, cvPoint(25, 200), &FontKYS, CV_RGB(0, 255, 0));
		}
		cvDrawRect(LinedImage, cvPoint(4, 4), cvPoint(LinedImage->width - 4, LinedImage->height - 4), CV_RGB(0, 255, 0), 3);
		paintImageReady[CAM_PCCA2] = 0;
	}
	if (CamNo==CAM_PCHI2)
	{
		www = "CAM4";
		CvFont FontKYS;
		cvInitFont(&FontKYS, CV_FONT_HERSHEY_DUPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 3, 3, 0, 6);
		cvPutText(LinedImage, www, cvPoint(25, 100), &FontKYS, CV_RGB(255, 0, 0));
		if (G_MainWnd->m_InspectThread.m_vi.m_IsCalibrationErr[3] == 1)
		{
			ttt = "CAL-ERR";
			cvInitFont(&FontKYS, CV_FONT_HERSHEY_TRIPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 1.5, 2, 0, 4);
			cvPutText(LinedImage, ttt, cvPoint(25, 200), &FontKYS, CV_RGB(0, 255, 0));
		}
		cvDrawRect(LinedImage, cvPoint(4, 4), cvPoint(LinedImage->width - 4, LinedImage->height - 4), CV_RGB(0, 255, 0), 3);
		paintImageReady[CAM_PCHI2] = 0;
	}
#endif
	//
	pDC->SetStretchBltMode(COLORONCOLOR);
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 
		0, 0, LinedImage->width, LinedImage->height, LinedImage->imageData, &bitmapInfo,
		DIB_RGB_COLORS, SRCCOPY);
	cvReleaseImage(&LinedImage);
	cvReleaseImage(&tempImage);

	//::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 
	//	0, 0, tempImage->width, tempImage->height, tempImage->imageData, &bitmapInfo, 
	//	DIB_RGB_COLORS, SRCCOPY);
	//cvReleaseImage(&tempImage);
}

void CnBizFerriteCoreInspectionDlg::DisplayImage(IplImage* srcimg, int item)
{
	CWnd* pWnd = GetDlgItem(item);
	CDC* pDCc = pWnd->GetDC();
	CRect rect;
	pWnd->GetClientRect(&rect);
	IplImage* img = cvCreateImage(cvSize(rect.Width(), rect.Height()), srcimg->depth, srcimg->nChannels);
	cvResize(srcimg, img);
	uchar buffer[sizeof(BITMAPINFOHEADER)*1024];
	BITMAPINFO* bmi = (BITMAPINFO*)buffer;
	int bmp_w = 0;
	int bmp_h = 0;
	int bpp = 0;
	bmp_w = img->width;
	bmp_h = img->height;
	bpp = (img->depth & 255)* img->nChannels;
	FillBitmapInfo(bmi, bmp_w, bmp_h, bpp, img->origin);
	int from_x = 0;
	int from_y = 0;
	int sw = 0;
	int sh = 0;
	from_x = MIN(0, bmp_w-1);
	from_y = MIN(0, bmp_h-1);
	sw = MAX(MIN(bmp_w - from_x, rect.Width()), 0);
	sh = MAX(MIN(bmp_h - from_y, rect.Height()), 0);
	SetDIBitsToDevice(pDCc->m_hDC, rect.left, rect.top, sw, sh, from_x, from_y, 
		0, sh, img->imageData + from_y * img->widthStep, bmi, 0);
	cvReleaseImage(&img);
	pWnd->ReleaseDC(pDCc);
}
void CnBizFerriteCoreInspectionDlg::FillBitmapInfo(BITMAPINFO* bmi, int width, int height, int bpp, int origin)
{
	_ASSERT(bmi && width >= 0 && height >=0 && (bpp == 8 || bpp ==24 || bpp ==32));
	BITMAPINFOHEADER* bmih = &(bmi->bmiHeader);
	memset(bmih, 0, sizeof(*bmih));
	bmih->biSize = sizeof(BITMAPINFOHEADER);
	bmih->biWidth = width;
	bmih->biHeight = origin ? abs(height) : -abs(height);
	bmih->biPlanes = 1;
	bmih->biBitCount = (unsigned short)bpp;
	bmih->biCompression = BI_RGB;
	if(bpp == 8){
		RGBQUAD* palette = bmi->bmiColors;
		int i;
		for(i=0;i<256;i++){
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CnBizFerriteCoreInspectionDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CnBizFerriteCoreInspectionDlg::m_fnInit()
{
	CDialog::Create(IDD, this);
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	for (int i = 0; i < 4; i++)
	{
		pImgBuf[i] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	}
	deleteSqc = 0;

}


void CnBizFerriteCoreInspectionDlg::m_fnDeInit()
{

	//DestroyWindow();
	CDialog::OnClose();
}


void CnBizFerriteCoreInspectionDlg::m_fnAddLog(CString strLog)
{
	//CString strCurrentTime;

	/*CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();
	strCurrentTime = ctCurrentTime.Format(_T("%d,%H:%M:%S :"));
*/

	CString			strRet;
	SYSTEMTIME		time;

	::GetLocalTime(&time);

	strRet.Format(L"%02d:%02d:%02d:%03d     ",
		time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);


	strLog = strRet + strLog;

	m_fnUpdateListofLog(strLog.GetBuffer(0));
	strLog.ReleaseBuffer();
}

void CnBizFerriteCoreInspectionDlg::m_fnUpdateListofLog(TCHAR* szLog)
{
	//	int listcount = m_ctrlListView.GetCount();
	if(m_listLogView.GetCount() == 100)
		m_listLogView.DeleteString(99);

	m_listLogView.InsertString(0, szLog);
	m_listLogView.SetCurSel(0);
}


void CnBizFerriteCoreInspectionDlg::m_fnAddLog2(CString strLog)
{
	CString strCurrentTime;

	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();
	strCurrentTime = ctCurrentTime.Format(_T("%d,%H:%M:%S : "));

	strLog = strCurrentTime + strLog;

	m_fnUpdateListofLog2(strLog.GetBuffer(0));
	strLog.ReleaseBuffer();
}

void CnBizFerriteCoreInspectionDlg::m_fnUpdateListofLog2(TCHAR* szLog)
{
	//	int listcount = m_ctrlListView.GetCount();
	if (m_listDefectView.GetCount() == 100)
		m_listDefectView.DeleteString(99);

	m_listDefectView.InsertString(0, szLog);
	m_listLogView.SetCurSel(0);
}

void CnBizFerriteCoreInspectionDlg::OnClose()
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
		OnBnClickedBtnStop();
		OnBnClickedBtnManu();
	}
	else if (G_SystemModeData.unSystemMode == SYSTEM_AUTO){
		OnBnClickedBtnManu();
	}

	Sleep(1000);
	G_SystemModeData.unSystemMode = SYSTEM_EXIT;
	//G_MainWnd->m_DataHandling.m_fnInspectDataReset();
	//20160301_KYS_BEFORE_CONV_RUN_ADD
	G_MainWnd->m_PIODlg.writeDO(0, OUT_BEFORE_CONV_RUN, CARD_IO);
	//LIGHT OFF
	G_MainWnd->m_PIODlg.writeDO(0, OUT_STG4_STOP_OPEN, CARD_IO);
	for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	{
		G_MainWnd->m_InspectThread.turnLight(i, OFF);
	}

	::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CLOSE, 0, 0);
}


HBRUSH CnBizFerriteCoreInspectionDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CnBizFerriteCoreInspectionDlg::OnTimer(UINT_PTR nIDEvent)
{
	CString strVal;
	CTime ctime;
	int day;
	static int LightOffCnt = 0;
//	static int CamRefreshCnt = 0;

	switch (nIDEvent)
	{
		case TIMER_IMAGE_DIR://1 sec
			ctime = CTime::GetCurrentTime();
			day = ctime.GetDay();
			if (day != G_MainWnd->m_DataHandling.m_FlashData.data[TODAY])
			{
				//day update
				G_MainWnd->m_DataHandling.m_FlashData.data[TODAY] = day;
				strVal.Format(L"%d", day);
				G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[TODAY], strVal);
				//Maker image data folder
				G_MainWnd->m_DataHandling.m_fnMakeDirectoryNewDay();

				strVal.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSavePath, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
				G_MainWnd->m_DataHandling.m_fnMakeDirectory(strVal);

				strVal.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSaveNGPath, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
				G_MainWnd->m_DataHandling.m_fnMakeDirectory(strVal);

			}
			//20160401_kys add
			if ( ((G_SystemModeData.unSystemMode != SYSTEM_AUTO_RUN) ||
				(G_MainWnd->m_InspectThread.stStgBatch[IN_CNVYR].holdShuttle == 1) ||
				(G_MainWnd->m_InspectThread.stStgBatch[OUT_CNVYR].holdShuttle == 1)) && LightOffCnt >20)
			{
				G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_1, OFF);
				G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_1, OFF);
				LightOffCnt = 0;
			}
			else
			{
				LightOffCnt++;
			}
//***********************************************
#ifndef _CAM_SKIP
			if (	G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData->LiveGrabCamsRun != 0)
			{
				m_btnCamStatus[CAM_PCHI].SetColorChange(GREEN, BLACK);
			}
			else{
				m_btnCamStatus[CAM_PCHI].SetColorChange(RGB(230, 230, 230), BLACK);
			}

			if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA].pImgData->LiveGrabCamsRun != 0)
			{
				m_btnCamStatus[CAM_PCCA].SetColorChange(GREEN, BLACK);
			}
			else{
				m_btnCamStatus[CAM_PCCA].SetColorChange(RGB(230, 230, 230), BLACK);
			}

			if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA2].pImgData->LiveGrabCamsRun != 0)
			{
				m_btnCamStatus[CAM_PCCA2].SetColorChange(GREEN, BLACK);
			}
			else{
				m_btnCamStatus[CAM_PCCA2].SetColorChange(RGB(230, 230, 230), BLACK);
			}

			if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI2].pImgData->LiveGrabCamsRun != 0)
			{
				m_btnCamStatus[CAM_PCHI2].SetColorChange(GREEN, BLACK);
			}
			else{
				m_btnCamStatus[CAM_PCHI2].SetColorChange(RGB(230, 230, 230), BLACK);
			}
#endif

//************************************************
			//DB row insert every hour
			if (ctime.GetHour() != hourDB)
			{
				hourDB = ctime.GetHour();
				//insert new row
#ifdef DB
				G_MainWnd->m_DataHandling.insertProductionRowDB();
#endif			
			}

			break;

		case	TIMER_AUTO_DIR_DELETE: //1min
			if (deleteSqc == 0){
				//Auto Delete Image file
				G_MainWnd->m_DataHandling.m_fnAutoDelete(DATA_IMAGE_FOLDER, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_AUTO_DELETE]);
				deleteSqc++;
			}
			else if (deleteSqc == 1){
				G_MainWnd->m_DataHandling.m_fnAutoDelete(DATA_IMAGE_NG_FOLDER, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_AUTO_DELETE]);
				deleteSqc++;
			}
#ifdef DB
			else if (deleteSqc == 2){

				G_MainWnd->m_DataHandling.deleteProductionDB(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_DB_PRODUCTION_DELETE]);
			}
			else if (deleteSqc == 3){

				G_MainWnd->m_DataHandling.deleteDefectDB(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_DB_DEFECT_DELETE]);

			}
#endif	
			else{
					deleteSqc = 0;
			}
			break;
	}
	CDialogEx::OnTimer(nIDEvent);
}


afx_msg LRESULT CnBizFerriteCoreInspectionDlg::OnUiUpdateCmd(WPARAM wParam, LPARAM lParam)
{
	if(wParam == UW_CLEAR)
	{
		
	}	
	else if(wParam == UM_TIME_UPDATE)
	{
		//CInterfaceSocket::FromCimTimeData timeData;
		//memcpy((void*)&timeData, (void*)lParam, sizeof(CInterfaceSocket::FromCimTimeData));

		//int nID = timeData.nID;
		//GetDlgItem(nID)->SetWindowText(timeData.chReceiveTime);

		//m_staPanelID.SetText(G_MainWnd->m_DataHandling.strPanelID, WHITE, BLACK);		
		//m_staEQPID.SetText(G_MainWnd->m_DataHandling.strDeviceID.Left(14), WHITE, BLACK);		
	}
	else if(wParam == UM_INIT_UPDATE)
	{
		////////////////////////////////////
		//Production count
		CString strValueData;
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
		m_staticAccu.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
		m_staticTarget.SetText(strValueData, BLACK, RGB(230,230,230));

		m_staticLOT.SetText(G_MainWnd->m_DataHandling.m_FlashData.strdata[LOT_NO], BLACK, RGB(230, 230, 230));


		float fVal = 0.0f;
		fVal = G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT] / 10.0f;
		strValueData.Format(_T("%4.1f %%"), fVal);
//		strValueData.Format(_T("%4.1f %%"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT]*1.0f);
		m_staticTargetPercent.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
		m_staticTotal.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d/%d"), G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK],G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG]);
		m_staticOKNG.SetText(strValueData, BLACK, RGB(230,230,230));

		//2016.04.04 JEC 
		fVal = 0.0f;
		if (G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL] != 0)
		{
			fVal = (1000.0F*G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG] / G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
		}
		strValueData.Format(_T("%4.1f %%"), fVal / 10.0F);
		m_staticNGRatio.SetText(strValueData, BLACK, RGB(230, 230, 230));		

		strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_INNER]);
		m_staticDefects[0].SetText(strValueData, BLACK, RGB(230, 230, 230));
		strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_BOTTOM]);
		m_staticDefects[1].SetText(strValueData, BLACK, RGB(230, 230, 230));
		strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_OUTER]);
		m_staticDefects[2].SetText(strValueData, BLACK, RGB(230, 230, 230));
		strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_TOP]);
		m_staticDefects[3].SetText(strValueData, BLACK, RGB(230, 230, 230));

		//2016.03.31 jec add
		for (int i = 0; i < 4; i++)
		{
			OnBnClickedBtnCam(IDC_BTN_CAM1 + i);
		}
	}
	else if(wParam == UM_STATUS_UPDATE)
	{
		masterResultUIUpdate();
	}
	else if(wParam == UM_TEST_UPDATE)
	{
		//int stg, step;
		//step = (int)(lParam & 0x00FF);
		//stg  = (int)((lParam>>8) & 0x00FF);
		int pass = (int)(lParam & 0x00FF);

		////////////////////////////////////
		//result log data grid
		if (!pass){ //Fail
			fillNGResultLIst();
		}

		for (int j = 0; j < MAX_VI_TEST; j++){
			//clear Vector
			G_MainWnd->m_DataHandling.viRes.vecDefects[j].clear();

		}//for (j = 0; j < MAX_VI_TEST; j++)

		//Production data update
		G_MainWnd->m_DataHandling.updateProductionData(pass);
		testResultUIUpdate();

		G_MainWnd->m_DataHandling.storeVIDataDB(pass);

	}
	else if(wParam == UM_INSPECT_ERROR_UPDATE)
	{
		m_ErrorMsg.SetText(ST_INSPECT_ERROR[(	int)lParam], BLACK, RED);
	}
	else if(wParam == UM_INSPECT_LOG_UPDATE)
	{
		wchar_t log[1024];
		wcscpy_s(log, ST_INSPECT_LOG[(int)lParam]);
		G_AddLog(3, log);
	}
	else if(wParam == UM_AXIS_ERROR_UPDATE)
	{
		m_ErrorMsg.SetText(ST_AXIS_ERROR[(	int)lParam], BLACK, RED);
	}
	else if(wParam == UM_AXIS_LOG_UPDATE)
	{
		wchar_t log[1024];
		wcscpy_s(log, ST_AXIS_LOG[(int)lParam]);
		G_AddLog(3, log);
	}
	else if(wParam == UM_SHIFT_UPDATE)//DATA SHIFT
	{

	}

	return TRUE;
}


void CnBizFerriteCoreInspectionDlg::m_fnTimeCheck(int nTimeIndex)
{
	CTime createImageTime = 0;
	CString  strCreateTime_M, strCreateTime_S;

	createImageTime = CTime::GetCurrentTime();
	strCreateTime_M = createImageTime.Format(L"%M");
	strCreateTime_S = createImageTime.Format(L"%S");

	int nReciveTime = (_wtoi(strCreateTime_M) * 60 )+ _wtoi(strCreateTime_S);

	if (nTimeIndex == TIME_CHECK_EQP_ALIVE)
	{
		m_nReciveAliveTime = nReciveTime;
	}
	else if (nTimeIndex == TIME_CHECK_NOW)					 // Now Check Timer
	{
		m_nNowTime = nReciveTime;
	}
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnAuto()
{
	CString msg;

#ifndef _CAM_SKIP
	if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData->LiveGrabCamsRun != 1)
	{
		AfxMessageBox(_T("CAM1 [CAM_PCHI] NOT Connected !!    Please, Check the CAM1"), MB_ICONHAND); 
		return;
	}
	if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA].pImgData->LiveGrabCamsRun != 1)
	{
		AfxMessageBox(_T("CAM2 [CAM_PCCA] NOT Connected !!    Please, Check the CAM2"), MB_ICONHAND); 
		return;
	}
	if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA2].pImgData->LiveGrabCamsRun != 1)
	{
		AfxMessageBox(_T("CAM3 [CAM_PCCA2] NOT Connected !!    Please, Check the CAM3"), MB_ICONHAND); 
		return;
	}
	if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI2].pImgData->LiveGrabCamsRun != 1)
	{
		AfxMessageBox(_T("CAM4 [CAM_PCHI2] NOT Connected !!     Please, Check the CAM4"), MB_ICONHAND); 
		return;
	}
#endif


	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		if (!G_MainWnd->m_PIODlg.readDI(IN_EM_STOP, CARD_IO)){
			//if(G_MainWnd->m_PIODlg.readDI(IN_EM_STOP, CARD_IO) ){
			AfxMessageBox(_T("EM STOP STATE"), MB_ICONHAND);
		}
		else{
			AfxMessageBox(_T("SYSTEM ERROR STATE"), MB_ICONHAND);
		}
		return;
	}

	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL )
	{

		if (G_MainWnd->m_InspectThread.checkStatus == CHECK_SUSPEND)
			G_MainWnd->m_InspectThread.checkStatus = 0;

		//for (int i = INPUT; i <= OUT_CNVYR; i++)
		for (int i = 0; i < STAGE_THREAD_COUNT; i++)
		{
			do{
				G_MainWnd->m_InspectThread.suspendCnt[i] = G_MainWnd->m_InspectThread.m_pThreads[i]->ResumeThread();
			} while (G_MainWnd->m_InspectThread.suspendCnt[i] > 0);
		}
	}


	////20160316_KYS
	G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM1_1, ON);
	G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_1, ON);
	G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_2, ON);
	G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_2, ON);

	////LIGHT ON
	//for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	//{
	//	G_MainWnd->m_InspectThread.turnLight(i, ON);
	//}
	////
	//OPEN INPUT GATE IF IT'S NOT
	if (!G_MainWnd->m_PIODlg.readDI(IN_LOAD_STOP, CARD_IO))
		G_MainWnd->m_PIODlg.writeDO(1, OUT_LOAD_STOP, CARD_IO);

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
		m_btnStart.SetColorChange(WHITE, BLACK);

	G_MainWnd->m_PIODlg.partPassing = 0;
	//m_btnAuto.EnableWindow(FALSE);
	//m_btnManual.EnableWindow(TRUE);
	G_SystemModeData.unSystemMode = SYSTEM_AUTO;
	m_btnAuto.SetColorChange(GREEN, BLACK);
	m_btnManual.SetColorChange(WHITE, BLACK);


	G_AddLog(3,L"Auto Mode");

}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnManu()
{
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN )
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdAutoRun();
		G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO )
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	//m_btnAuto.EnableWindow(TRUE);
	//m_btnManual.EnableWindow(FALSE);
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	m_btnAuto.SetColorChange(WHITE, BLACK);
	m_btnManual.SetColorChange(GREEN, BLACK);
	m_btnStart.SetColorChange(WHITE, BLACK);
	m_btnStop.SetColorChange(WHITE, BLACK);

	//20160316_KYS
	G_MainWnd->m_PIODlg.writeDO(0, OUT_STG4_STOP_OPEN, CARD_IO);
	//LIGHT_OFF
	for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	{
		G_MainWnd->m_InspectThread.turnLight(i, OFF);
	}


	// OK/NG CLEAR
	m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
	m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));

	G_AddLog(3,L"Manual Mode");
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnStart()
{
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){

		if(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_MASTER_TEST]==1)
		{
			if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] != 1){
				if(nMasterOK != 1){
					AfxMessageBox(_T("Conduct OK Master Test"),MB_ICONHAND);
					return;
				}
			}
			//else if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] != 1 ){ //임시
			//	if(nMasterNG != 1){
			//		AfxMessageBox(_T("Conduct NG Master Test"),MB_ICONHAND);
			//		return;
			//	}
			//}
		}

		////20160316_KYS
		G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM1_1, ON);
		G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_1, ON);
		G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_2, ON);
		G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_2, ON);

		G_MainWnd->m_PIODlg.writeDO(1, OUT_STG4_STOP_OPEN, CARD_IO);

		//for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
		//{
		//	G_MainWnd->m_InspectThread.turnLight(i, ON);
		//}
		////
		G_SystemModeData.unSystemMode = SYSTEM_AUTO_RUN;
		m_btnStart.SetColorChange(GREEN, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.resumeAutoRun();
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
		G_AddLog(3,L"Auto Start");

		//Time display
		CString strTimeAutoRun;
		ctimeAutoRun = CTime::GetCurrentTime();
		strTimeAutoRun = ctimeAutoRun.Format(L"%Y/%m/%d %H:%M:%S");
		m_staticTimeMasterStart.SetText(strTimeAutoRun, WHITE, BLACK);

	}
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnStop()
{
	int iRet, nDone;
	G_MainWnd->m_InspectThread.m_csCheck.Lock();
	G_MainWnd->m_InspectThread.holdAutoRun();
	G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
		G_SystemModeData.unSystemMode = SYSTEM_AUTO;
		m_btnStart.SetColorChange(WHITE, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_AddLog(3,L"AUTO STOP");
	}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){
		m_btnStart.SetColorChange(WHITE, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_AddLog(3,L"MANUAL STOP");
	}

	//G_MainWnd->m_InspectThread.turnLight(COM_LIGHT1, OFF);
	//G_MainWnd->m_InspectThread.turnLight(COM_LIGHT2, OFF);
	//20160316_KYS
	G_MainWnd->m_PIODlg.writeDO(0, OUT_STG4_STOP_OPEN, CARD_IO);
	for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	{
		G_MainWnd->m_InspectThread.turnLight(i, OFF);
	}

	//Axis Stop
	//for(int i = 0; i < 4; i++){
	//	pmiAxCheckDone(CARD_NO, i, &nDone);
	//	if(nDone == emRUNNING)
	//		iRet = pmiAxStop(CARD_NO, i);
	//}


}

void CnBizFerriteCoreInspectionDlg::clearNGResult()
{
	return ;
}

void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMode()
{
	CPassWordDlg PassWordDlg;
	if(m_opMode == MODE_OPERATOR)
	{
#ifndef DEV_MODE
			if(PassWordDlg.DoModal() == TRUE) 
			{	
#endif
				m_opMode = MODE_SUPERVISOR;
				m_OperatorMode.SetText(L"ENGINEER MODE", BLACK, RGB(225,255,255));
				return;
#ifndef DEV_MODE
			}
#endif
	}
	else
	{
		m_opMode = MODE_OPERATOR;	
		m_OperatorMode.SetText(L"OPERATOR MODE", BLACK, RGB(255,240,255));
	}
	return;
}


void CnBizFerriteCoreInspectionDlg::OnMenuSystem()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("It can be opened in Engineer Mode"),MB_ICONHAND);
	}else{
		G_MainWnd->m_SystemSetupDlg.m_fnSystemUILoad();
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_SystemSetupDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_SystemSetupDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
	}
}


void CnBizFerriteCoreInspectionDlg::OnMenuModel()
{
	//load recipe data
	if(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel != L"")
	{
		G_MainWnd->m_RecipeSetDlg.recipeSelected = G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel;
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	}

	if(m_opMode == MODE_OPERATOR)
	{
		//AfxMessageBox(_T("It can be opened in Engineer Mode"),MB_ICONHAND);
		G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.SetEditable(FALSE);
	}else{
		G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.SetEditable(TRUE);
	}

#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_RecipeSetDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_RecipeSetDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
}


void CnBizFerriteCoreInspectionDlg::OnMenuAxis()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("It can be opened in Engineer Mode"),MB_ICONHAND);
	}else{
		G_MainWnd->m_InspectThread.m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
#ifdef DEV_MODE
		::SetWindowPos(G_MainWnd->m_InspectThread.m_AxisDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
		::SetWindowPos(G_MainWnd->m_InspectThread.m_AxisDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif

	}
}


void CnBizFerriteCoreInspectionDlg::OnMenuIo()
{
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
}


void CnBizFerriteCoreInspectionDlg::OnEnvironmentVisioninspection()
{
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_InspectThread.m_vi, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_InspectThread.m_vi, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
}


void CnBizFerriteCoreInspectionDlg::OnHelpAbout()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}


void CnBizFerriteCoreInspectionDlg::OnFileExit()
{
	//Program Exit
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	OnBnClickedBtnManu();
	Sleep(100);
	G_SystemModeData.unSystemMode = SYSTEM_EXIT;

	//LIGHT OFF
	for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	{
		G_MainWnd->m_InspectThread.turnLight(i, OFF);
	}
	::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CLOSE, 0, 0);
}

//Error Reset
void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnErrorReset()
{
	CString strBuff;
	G_MainWnd->m_InspectThread.errorResetRoutine();

	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		for (int i = 1; i < TOTAL_STAGE; i++)
		{
			if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(i) == STEP19){
				G_MainWnd->m_PIODlg.m_btnErrorReset[i].SetColorChange(WHITE, BLACK);
				G_MainWnd->m_InspectThread.m_fnSetBatchStep(i, STEP20); //put it in error reset step
			}
			G_MainWnd->m_PIODlg.m_btnErrorReset[i].SetColorChange(RGB(255, 255, 255), RGB(0, 0, 0));
		}
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}else{
		strBuff.Format(_T(" Error Reset can be done in Manual Mode "));
		AfxMessageBox(strBuff);
	}

}


int CnBizFerriteCoreInspectionDlg::OnSystemInit(void)
{
	//restore inspection status
	G_MainWnd->m_InspectThread.restoreInspectionStatus(); //Inspection Step, Status, Conveyor.Allow/Request

	G_MainWnd->m_InspectThread.m_csCheck.Lock();
	G_MainWnd->m_InspectThread.holdAutoRun();
	G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
	G_MainWnd->m_InspectThread.m_csCheck.Unlock();

	//model
	G_MainWnd->m_RecipeSetDlg.applyRecipeInProduction(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	//VI Dlg

	if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData->LiveGrabCamsRun == 1)
		G_MainWnd->m_InspectThread.m_vi.applyRecipeInProduction();
	
	G_MainWnd->m_InspectThread.m_vi.GetDlgItem(IDC_STATIC_VI_PNO)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	//apply recipe in LCR Axis Recipe
	G_MainWnd->m_InspectThread.m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	//Main Dlg
	m_staticInsp.SetText(G_MainWnd->m_DataHandling.m_SystemParam.LINE_ID, BLACK, WHITE);
	m_staticModel.SetText(G_MainWnd->m_RecipeSetDlg.part_Test.part_model);
	m_staticType.SetText(G_MainWnd->m_RecipeSetDlg.part_Test.part_type);
	m_opMode = MODE_OPERATOR;
	m_OperatorMode.SetText(L"OPERATOR MODE", BLACK, RGB(255,240,255));
	if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] == 1){
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GREEN_LIGHT);
	}else{
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GRAY_LIGHT);
	}
	if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] == 1){
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GREEN_LIGHT);
	}else{
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GRAY_LIGHT);
	}

	for (int i = 0; i < MAX_VI_TEST; i++)
	{
		//2016.04.06 jec add
		G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[i].pImgData->LiveCheck[i] = 4;
	}

	//manual mode
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	G_SystemModeData.unSystemError = SYSTEM_OK;
	m_btnAuto.SetColorChange(WHITE, BLACK);
	m_btnManual.SetColorChange(GREEN, BLACK);
	m_btnStart.SetColorChange(WHITE, BLACK);
	m_btnStop.SetColorChange(WHITE, BLACK);
	G_AddLog(3,L"Manual Mode");

	return 0;
}


//hold shuttle to stop all inspect batch finished
void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnHoldSHUTTLE()
{
	if (G_MainWnd->m_InspectThread.stStgBatch[IN_CNVYR].holdShuttle == 0)
	{
		G_MainWnd->m_InspectThread.stStgBatch[IN_CNVYR].holdShuttle = 1;
		m_btnHoldSHUTTLE.SetColorChange(GREEN, BLACK);

	}else{
		G_MainWnd->m_InspectThread.stStgBatch[IN_CNVYR].holdShuttle = 0;
		m_btnHoldSHUTTLE.SetColorChange(WHITE, BLACK);
	}
	
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnBuzzstop()
{
	G_MainWnd->m_PIODlg.buzzstop = 1;
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMasterOk2()
{
	if(nMasterOK != 1){
		nMasterOK = 1;
		m_masterTest_OK_Start.SetColorChange(GREEN, BLACK);
	}
	else{
		nMasterOK = 0;
		m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
	}
}

void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnMasterNg2()
{
		if(nMasterNG != 1){
			nMasterNG = 1;
			m_masterTest_NG_Start.SetColorChange(GREEN, BLACK);
		}
		else{
			nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
		}
}

void CnBizFerriteCoreInspectionDlg::StartEventCapture()
{
	RAWINPUTDEVICE rawInputDev[1]; 
	ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*1);
	// 키보드 RAWINPUTDEVICE 구조체 설정	
	rawInputDev[0].usUsagePage = 0x01;
	rawInputDev[0].usUsage = 0x06;
	rawInputDev[0].dwFlags = RIDEV_INPUTSINK;
	rawInputDev[0].hwndTarget = this->GetSafeHwnd();
	if( FALSE == RegisterRawInputDevices(rawInputDev, 1, sizeof(RAWINPUTDEVICE)) )
	{
		CString str;
		str.Format(_T("RegisterRawInputDevices Error %d"), GetLastError());
		G_AddLog(3,str.GetBuffer());
		str.ReleaseBuffer();
	}
}

void CnBizFerriteCoreInspectionDlg::EndEventCapture()
{
	RAWINPUTDEVICE rawInputDev[1]; 
	ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*1);
	// Keyboard RAWINPUTDEVICE Struct Setting	
	rawInputDev[0].usUsagePage = 0x01;
	rawInputDev[0].usUsage = 0x06;
	rawInputDev[0].dwFlags = RIDEV_REMOVE; //<--
	rawInputDev[0].hwndTarget = this->GetSafeHwnd();

	if( FALSE == RegisterRawInputDevices(rawInputDev, 1, sizeof(RAWINPUTDEVICE)) )
	{
		CString str;
		str.Format(_T("RegisterRawInputDevices Error %d"), GetLastError());
		G_AddLog(3,str.GetBuffer());
		str.ReleaseBuffer();
	}
}

void CnBizFerriteCoreInspectionDlg::OnRawInput(UINT nInputcode, HRAWINPUT hRawInput)
{
#ifdef _FI
	UINT   dwSize = 40;
	static BYTE lpb[40];
	wchar_t c;

	GetRawInputData((HRAWINPUT)hRawInput, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));
	RAWINPUT* raw = (RAWINPUT*)lpb;
	if( raw->header.dwType == RIM_TYPEKEYBOARD && ! raw->data.keyboard.Flags && raw->data.keyboard.VKey >= 0x20) 
	{
		switch(raw->data.keyboard.VKey)
		{
		case 0xBC:
			c = ',';
			break;
		case 0xBD:
			c = '-';
			break;
		case 0xBE:
			c = '.';
			break;
		case 0xBF:
			c = '/';
			break;
		default:
			c = (char)(raw->data.keyboard.VKey & 0xFF);
			break;
		}

	} 
	CDialogEx::OnRawInput(nInputcode, hRawInput);
#endif
}


void CnBizFerriteCoreInspectionDlg::testResultUIUpdate(void)
{
	int stage;
	CTime ctCurrentTime;

	stage = OUT_NGOUT;

	ctCurrentTime = CTime::GetCurrentTime();

	////////////////////////////////////
	//OK NG display
	if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){ //PASS
		m_staticOK.SetText(L"OK", BLACK, GREEN);
		m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));
	}else{
		m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
		m_staticNG.SetText(L"NG", BLACK, RED);
	}
	////////////////////////////////////
	//Production count
	CString strValueData;
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	m_staticAccu.SetText(strValueData, BLACK, RGB(230,230,230));
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	m_staticTarget.SetText(strValueData, BLACK, RGB(230,230,230));

	float fVal = 0.0f;
	fVal = G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT] / 10.0f;
	strValueData.Format(_T("%4.1f %%"), fVal);
//	strValueData.Format(_T("%4.1f %%"), G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT] * 1.0f);
	m_staticTargetPercent.SetText(strValueData, BLACK, RGB(230, 230, 230));
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
	m_staticTotal.SetText(strValueData, BLACK, RGB(230,230,230));
	strValueData.Format(_T("%d/%d"), G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK],G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG]);
	m_staticOKNG.SetText(strValueData, BLACK, RGB(230,230,230));

	//2016.04.04 JEC 
	fVal = 0.0f;
	if (G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL] != 0)
	{
		fVal = (1000.0F*G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG] / G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
	}
	strValueData.Format(_T("%4.1f %%"), fVal / 10.0F);
	m_staticNGRatio.SetText(strValueData, BLACK, RGB(230, 230, 230));

	/////////////////////////////////////
	// Defect Position
	strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_INNER]);
	m_staticDefects[0].SetText(strValueData, BLACK, RGB(230, 230, 230));
	strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_BOTTOM]);
	m_staticDefects[1].SetText(strValueData, BLACK, RGB(230, 230, 230));
	strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_OUTER]);
	m_staticDefects[2].SetText(strValueData, BLACK, RGB(230, 230, 230));
	strValueData.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_TOP]);
	m_staticDefects[3].SetText(strValueData, BLACK, RGB(230, 230, 230));
	


}


void CnBizFerriteCoreInspectionDlg::masterResultUIUpdate(void)
{
	int stage;

	stage = OUT_NGOUT;

	if(nMasterOK == 1){
		if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){//OK
			m_masterTest_OK.SetLedState(GREEN_LIGHT);
			nMasterOK = 0;
			m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 1;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"1"); 
			//AfxMessageBox(_T("양품 마스터 시험 결과 OK입니다"),MB_ICONASTERISK); 
			m_ErrorMsg.SetText(_T("OK Master Test Result Passed"), BLACK, GREEN); //RGB(230,230,230)
		}else{//NG
			m_masterTest_OK.SetLedState(RED_LIGHT);
			//nMasterOK = 0;
			m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 0;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"0"); 
			//AfxMessageBox(_T("양품 마스터 시험 결과 NG입니다"),MB_ICONHAND);
			m_ErrorMsg.SetText(_T("OK Master Test Result Failed"), BLACK, RED);
		}
	}
	if(nMasterNG == 1){
		if(!G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){//NG
			m_masterTest_NG.SetLedState(GREEN_LIGHT);
			nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 1;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"1"); 
			//AfxMessageBox(_T("불량 마스터 시험 결과 OK입니다"),MB_ICONASTERISK); 
			m_ErrorMsg.SetText(_T("NG Master Test Result Passed"), BLACK, RED);
		}else{//OK
			m_masterTest_NG.SetLedState(RED_LIGHT);
			//nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 0;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"0"); 
			//AfxMessageBox(_T("불량 마스터 시험 결과 NG입니다"),MB_ICONHAND);
			m_ErrorMsg.SetText(_T("NG Master Test Result Failed"), BLACK, GREEN);
		}
	}
}


int CnBizFerriteCoreInspectionDlg::fillNGResultLIst()
{
	int stage = OUT_NGOUT;
	CTime ctCurrentTime;

	ctCurrentTime = CTime::GetCurrentTime();
	CString strValue, str;
	GV_ITEM Item;
	Item.nFormat = DT_VCENTER;

	int i, j;

	//G_MainWnd->m_DataHandling.viRes.m_csVIResult[CAM_PCHI].Lock();
	for (j = 0; j < MAX_VI_TEST; j++)
	{

		if (G_MainWnd->m_DataHandling.viRes.results[j] == VI_NG) //has a defect
		{
			size_t size = G_MainWnd->m_DataHandling.viRes.vecDefects[j].size();
			for (i = 0; i < size; i++)
			{

				//vector<VI_DEFECT>::iterator it
				VI_DEFECT it = G_MainWnd->m_DataHandling.viRes.vecDefects[j].at(i);
				//Main Dlg NG Grid display

				if (it.surface > 0)
				{
					G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_INNER + it.surface - 1]++;
					str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_INNER + it.surface - 1]);
					G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[DEFECT_INNER + it.surface - 1], str);
				}

#ifndef NO_DEFECT_DISPLAY
				//Part ID.
				strValue.Format(L" [%d]", G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);

				//Type
				str.Format(L" %s ", ST_VI_DEFECT_TYPE[it.type]);
				strValue += str;

				Item.col = 3;//Surface
				if (it.surface > 0)
				{
					strValue += ST_VI_SURF[it.surface];
				}

				Item.col = 4;//Coordinates
				str.Format(L" [%d,%d], [%d,%d]",
					it.coords[VI_LEFT],
					it.coords[VI_TOP],
					it.coords[VI_RIGHT],
					it.coords[VI_BOTTOM]);
				strValue += str;

				Item.col = 5;//Size
				str.Format(L"%d", it.size);
				strValue += str;

				m_fnAddLog2(strValue);
#endif
#ifdef DB
				//DB Store
				G_MainWnd->m_DataHandling.insertDefectDB(G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU],
					it.surface,
					it.type,
					it.coords[VI_LEFT], it.coords[VI_TOP], it.coords[VI_RIGHT], it.coords[VI_BOTTOM],
					it.size);
#endif

			}//for (vector<VI_DEFECT>::iterator it 

		}//if (G_MainWnd->m_DataHandling.viRes.results[j] == VI_NG) //has a defect


	}//for (j = 0; j < MAX_VI_TEST; j++)
	//G_MainWnd->m_DataHandling.viRes.m_csVIResult[CAM_PCHI].Unlock();

	return 0;
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnCam(UINT nID)
{
	BOOL bRes = TRUE;
	int viIdx = nID - IDC_BTN_CAM1;

	//2016.03.31 jec edit
#ifndef _CAM_SKIP
	if (viIdx == CAM_PCHI )
	{
		if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI].pImgData->LiveGrabCamsRun !=0)
		{
			m_btnCamStatus[CAM_PCHI].SetColorChange(GREEN, BLACK);
		}
		else{
			m_btnCamStatus[CAM_PCHI].SetColorChange(RGB(230, 230, 230), BLACK); 
		}
	}
	else if (viIdx == CAM_PCCA)
	{
		if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA].pImgData->LiveGrabCamsRun != 0)
		{
			m_btnCamStatus[CAM_PCCA].SetColorChange(GREEN, BLACK);
		}
		else{
			m_btnCamStatus[CAM_PCCA].SetColorChange(RGB(230, 230, 230), BLACK); 
		}
	}
	else if (viIdx == CAM_PCCA2)
	{
		if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCCA2].pImgData->LiveGrabCamsRun != 0)
		{
			m_btnCamStatus[CAM_PCCA2].SetColorChange(GREEN, BLACK);
		}
		else{
			m_btnCamStatus[CAM_PCCA2].SetColorChange(RGB(230, 230, 230), BLACK); 
		}

	}
	else if (viIdx == CAM_PCHI2)
	{
		if (G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[CAM_PCHI2].pImgData->LiveGrabCamsRun != 0)
		{
			m_btnCamStatus[CAM_PCHI2].SetColorChange(GREEN, BLACK);
		}
		else{
			m_btnCamStatus[CAM_PCHI2].SetColorChange(RGB(230, 230, 230), BLACK); 
		}	
	}

#endif

}


void CnBizFerriteCoreInspectionDlg::OnBnClickedBtnHoldShuttle2()
{
	if (G_MainWnd->m_InspectThread.stStgBatch[OUT_CNVYR].holdShuttle == 0)
	{
		G_MainWnd->m_InspectThread.stStgBatch[OUT_CNVYR].holdShuttle = 1;
		m_btnOutCnvyrHold.SetColorChange(GREEN, BLACK);

	}
	else{
		G_MainWnd->m_InspectThread.stStgBatch[OUT_CNVYR].holdShuttle = 0;
		m_btnOutCnvyrHold.SetColorChange(WHITE, BLACK);
	}
}


void CnBizFerriteCoreInspectionDlg::OnBnClickedButton2()
{
	CString strBuff;
	if (G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		strBuff.Format(_T(" All Clear can be done in Manual Mode "));
		AfxMessageBox(strBuff);
		return;
	}

	if (IDCANCEL == AfxMessageBox(_T("ALL CLEAR will reset Batch step and status to initial state. Want to preceed?"), MB_OKCANCEL | MB_ICONQUESTION))
		return;

	G_MainWnd->m_InspectThread.m_fnClearParts();
}
void CnBizFerriteCoreInspectionDlg::OnBnCheckedCrossLine()
{
	m_bDrawCrossLine = !m_bDrawCrossLine;

	paintImageReady[CAM_PCHI] = 1;
	paintImageReady[CAM_PCCA] = 1;
	paintImageReady[CAM_PCCA2] = 1;
	paintImageReady[CAM_PCHI2] = 1;
	OnPaint();
}