#pragma once

#define      SYSTEM_ROOT_FOLDER			L"D:\\nBizSystem\\"
#define		SYSTEM_PARAM_PATH			L"D:\\nBizSystem\\SystemSetting.ini"
#define		RECIPE_PATH						L"D:\\nBizSystem\\Recipe.ini"

#define		VISION_FOLDER							L"D:\\nBizSystem\\Vision\\"
#define		VISION_MODEL_SAMPLE_FOLDER				L"D:\\nBizSystem\\Vision\\ModelSample\\"

#define      DATA_ROOT_FOLDER				L"D:\\nBizData\\"
#define      DATA_NG_FOLDER				L"D:\\nBizData\\NG\\"
#define      RESULT_DATA_FOLDER			L"D:\\nBizData\\ResultData\\"
#define      IMAGE_SAVE_FOLDER			L"D:\\nBizData\\Image\\"


#define OK_Exel			L"OK"
#define NG_Exel			L"NG"
#define NONE_Exel		L"NONE"
#define	GCHP_Exel		L"GCHP"
#define	TSIO_Exel		L"TSIO"
#define	TSIM_Exel		L"TSIM"
#define	PCUP_Exel		L"PCUP"

/////////////////////////////////////////////////////////////////////////////////////////////////////
#define		SYSTEM_FOLDER								L"D:\\nBizSystem\\System\\"
#define		SYSTEM_FOLDER_FILES						L"D:\\nBizSystem\\System\\*.*"
#define		SYSTEM_PARAM_INI_PATH					L"D:\\nBizSystem\\System\\System.ini"

#define      SYSTEM_LOG_FOLDER						L"D:\\nBizSystem\\Process_Log\\"
#define      SYSTEM_ALARM_LOG_FOLDER				L"D:\\nBizSystem\\Alarm_Log\\"

#define		RECIPE_FOLDER								L"D:\\nBizSystem\\Recipe\\"
#define		RECIPE_FOLDER_BLANK						L"D:\\nBizSystem\\Recipe\\Recipe.ini"
#define		RECIPE_FOLDER_PATH						L"D:\\nBizSystem\\Recipe\\*.*"
#define		PASSWORD_PATH								L"D:\\nBizSystem\\System\\"
#define		TMC_FILE_NAME								L"D:\\nBizSystem\\System\\Default.prm"

/////////////////////////////////////////////////////////////////////////////////////////////////////

#define      DATA_ROOT_FOLDER							L"D:\\nBizData\\"
#define      DATA_ROOT_NG_FOLDER					L"D:\\nBizData\\NG\\"
#define      RESULT_DATA_FOLDER						L"D:\\nBizData\\ResultData\\"

///////////////////////////////////////////////////////////////////////////////////////////////////

#define      DATA_IMAGE_FOLDER						L"D:\\nBizData\\Image\\"
#define      DATA_IMAGE_NG_FOLDER					L"D:\\nBizData\\NG_Image\\"

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//##############################################################//
//

///////////////////////////////////////////////////////////////////////////////////
// AXIS ERROR LOG

					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 0 );
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 0 );

const CString ST_AXIS_ERROR[] = {
	//  AXIS					0
L" AXIS POSITION CHECK ERROR",
L" READY TO HOME CHECK DONE TIME OUT ERROR",
L" READY TO HOME MOVE ERROR",
L"SYS INIT SHUTTLE READY ERROR",
L"AXIS HOME TO READY MOVE ERROR: NOT IN HOME POSITION",
L"AXIS HOME TO READY ABS MOVE ERROR",
L"ERROR_0",
L"ERROR_0",
L" AXIS HOME CLEAR ERROR",
L" AXIS IN CNVYR POSITION CLEAR ERROR",

	//  AXIS					10
L" AXIS OUT CNVYR POSITION CLEAR ERROR",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",

	//  AXIS					20
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",

	//  AXIS					30
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
};


const CString ST_AXIS_LOG[] = {
//  AXIS					0
L"SYS INIT  AXIS POSITION CHECK ERROR",
L" READY TO HOME CHECK DONE TIME OUT ERROR",
L" READY TO HOME MOVE ERROR",
L"SYS INIT SHUTTLE READY ERROR",
L"AXIS HOME TO READY MOVE ERROR: NOT IN HOME POSITION",
L"AXIS HOME TO READY ABS MOVE ERROR",
L"AXIS HOME TO READY ABS MOVE CHECK DONE ERROR",
L" AXIS HOME TO READY MOVE SUCCEDED",
L" AXIS CLEAR ERROR",
L" AXIS IN CNVYR POSITION CLEAR ERROR",

	//  AXIS					10
L" AXIS OUT CNVYR POSITION CLEAR ERROR",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",

	//  AXIS					20
L"SYS INIT SHUTTLE READY DONE",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",

	//  AXIS					30
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
};

/////////////////////////////////////////////////////////////////////////////
// Inspection Error & Log message
//
//  LOAD					00 ~ 09
//  						10
//  						20
//  						30
//							40
//  						50
//  VI						60
//  UNLOAD				70
//  SHUTTLE			80
//							90
//
//

					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 0 );
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 0 );


const CString ST_INSPECT_ERROR[] = {
//  LOAD					00
L"LOAD STOPPER OPEN ERROR",
L"LOAD STOPPER CLOSE ERROR",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  					10
L"IN CONVEYOR CENTERING FWD ERROR",
L"IN CONVEYOR CENTERING RET ERROR",
L"OUT CONVEYOR CENTERING FWD ERROR",
L"OUT CONVEYOR CENTERING RET ERROR",
L"OUT CONVEYOR STOPPER OPEN ERROR",
L"OUT CONVEYOR STOPPER OPEN TIME OUT ERROR",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  					20
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  					30
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  				40
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  						50
L"OUT_VI1 Step 3 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"OUT_VI2 Step 3 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"OUT_VI2 Step 4 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"ERROR_0",
L"Camera 1 Grab Time Out Error",
L"Camera 2 Grab Time Out Error",
L"Camera 1 Grab Error",
L"Camera 2 Grab Error",
L"STG8 NG OUT FWD ERROR",
L"STG8 NG OUT RET ERROR",
//  VI						60
L"CAM1 VI TIME OUT ERROR",
L"CAM1 VISION INSPECTION ERROR",
L"Camera Connection Error",
L"Camera Serial No Unmatched",
L"Camera InitGrab Error",
L"Camera Grab Error",
L"CAM2 UP VI TIME OUT ERROR",
L"CAM2 UP VISION INSPECTION ERROR",
L"CAM2 DOWN VI TIME OUT ERROR",
L"CAM2 DOWN VISION INSPECTION ERROR",
//  UNLOAD				70
L"STG8 NG OUT FWD TIMEOUT ERROR",
L"STG8 NG OUT RET TIMEOUT ERROR",
L"ERROR_0",
L"UNLOAD CONVEYOR RUN ERROR",
L"VISION INSPECTION ERROR",
L"VISION INSPECTION RESULT TIMEOUT",
L"CAM2 VISION INSPECTION ERROR",
L"NGOUT VISION INSPECTION RESULT TIMEOUT",
L"ERROR_0",
L"ERROR_0",
//  CONVEYOR			80
L"IN CONVEYOR 1 PITCH RUN ERROR",
L"IN CONVEYOR 1 PITCH RUN TIME OUT ERROR",
L"OUT CONVEYOR 1 PITCH RUN ERROR",
L"OUT CONVEYOR 1 PITCH RUN TIME OUT ERROR",
L"OUT CONVEYOR GET POS. ERROR",
L"OUT CONVEYOR STG4 STOPPER STOP ERROR",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
//  OUT CV			90
L"STG5 STOPPER OPEN FAIL",
L"STG5 STOPPER CLOSE FAIL",
L"OUT CONVEYOR INC MOVE ERROR",
L"STG5 STOPPER OPEN TIMEOUT ERROR",
L"STG5 STOPPER CLOSE TIMEOUT ERROR",
L"STG6 CENTERING FWD FAIL",
L"STG6 CENTERING RET FAIL",
L"STG6 CENTERING FWD TIMEOUT ERROR",
L"STG6 CENTERING RET TIMEOUT ERROR",
L"ERROR_0",
//							100
L"CAM3 NO CORE DETECT",
L"LIGHT CURTAIN INTERRUPTED",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",

L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
L"ERROR_0",
};


const CString ST_INSPECT_LOG[] = {
//  LOAD					00
L"LOAD STOPPER OPEN ERROR",
L"LOAD STOPPER CLOSE ERROR",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//  					10
L"IN CONVEYOR CENTERING FWD ERROR",
L"IN CONVEYOR CENTERING RET ERROR",
L"OUT CONVEYOR CENTERING FWD ERROR",
L"OUT CONVEYOR CENTERING RET ERROR",
L"OUT CONVEYOR STOPPER OPEN ERROR",
L"OUT CONVEYOR STOPPER OPEN TIME OUT ERROR",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//  					20
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//  					30
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//  				40
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//  						50
L"OUT_VI1 Step 3 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"OUT_VI2 Step 3 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"OUT_VI2 Step 4 not TH_VI_CLEARed Time out. Clear All Parts on Conveyor and click PART CLEAR button.",
L"LOG_0",
L"Camera 1 Grab Time Out Error",
L"Camera 2 Grab Time Out Error",
L"LOG_0",
L"LOG_0",
L"STG8 NG OUT FWD ERROR",
L"STG8 NG OUT RET ERROR",
//  VI						60
L"CAM1 VI TIME OUT ERROR",
L"CAM1 VISION INSPECTION ERROR",
L"Camera Connection Error",
L"Camera Serial No Unmatched",
L"Camera InitGrab Error",
L"Camera Grab Error",
L"CAM2 UP VI TIME OUT ERROR",
L"CAM2 UP VISION INSPECTION ERROR",
L"CAM2 DOWN VI TIME OUT ERROR",
L"CAM2 DOWN VISION INSPECTION ERROR",
//  UNLOAD				70
L"STG8 NG OUT FWD TIMEOUT ERROR",
L"STG8 NG OUT RET TIMEOUT ERROR",
L"LOG_0",
L"UNLOAD CONVEYOR RUN ERROR",
L"VISION INSPECTION ERROR",
L"VISION INSPECTION RESULT TIMEOUT",
L"CAM2 VISION INSPECTION ERROR",
L"CAM2 VISION INSPECTION RESULT TIMEOUT",
L"LOG_0",
L"LOG_0",
//  CONVEYOR			80
L"IN CONVEYOR 1 PITCH RUN ERROR",
L"IN CONVEYOR 1 PITCH RUN TIME OUT ERROR",
L"OUT CONVEYOR 1 PITCH RUN ERROR",
L"OUT CONVEYOR 1 PITCH RUN TIME OUT ERROR",
L"OUT CONVEYOR GET POS. ERROR",
L"OUT CONVEYOR STG4 STOPPER STOP ERROR",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
//                   		90
L"STG5 STOPPER OPEN FAIL",
L"STG5 STOPPER CLOSE FAIL",
L"OUT CONVEYOR INC MOVE ERROR",
L"STG5 STOPPER OPEN TIMEOUT ERROR",
L"STG5 STOPPER CLOSE TIMEOUT ERROR",
L"STG6 CENTERING FWD FAIL",
L"STG6 CENTERING RET FAIL",
L"STG6 CENTERING FWD TIMEOUT ERROR",
L"STG6 CENTERING RET TIMEOUT ERROR",
L"LOG_0",
//							100
L"CAM3 NO CORE DETECT",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",

L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
L"LOG_0",
};



