#pragma once
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "1.Common\RoundButton\RoundButton2.h"
#include "afxwin.h"
#include <queue>

//// Include files to use the PYLON API.
//#include <pylon/PylonIncludes.h>
//#include <pylon/gige/BaslerGigEInstantCamera.h>
//#ifdef PYLON_WIN_BUILD
//#    include <pylon/PylonGUI.h>
//#endif
//
//// Settings to use Basler GigE cameras
//#include <pylon/gige/BaslerGigECamera.h>
//typedef Pylon::CBaslerGigECamera Camera_t;
//using namespace Basler_GigECameraParams;
//using namespace Basler_GigEStreamParams;

#define CAM_NO				4

//
#define CAM_PCHI			0    //INNER
#define CAM_PCCA			1    //UPPER
#define CAM_PCCA2			2    //OUTER
#define CAM_PCHI2			3    //LOWER

#define VI_NO					4

#define VI_CLEAR			0
#define VI_OK				1
#define VI_NG				2
#define VI_ERROR			3

#define LIVE_SIZE_W		1624
#define LIVE_SIZE_H		1234
#define CAM_LIVE_IMG_MAP_NAME		L"LIVE_IMG"

// Namespace for using cout.
using namespace std;

#define UM_VI							WM_USER + 300
#define UM_VI2							WM_USER + 400
#define UM_VI3							WM_USER + 500
#define UM_VI4							WM_USER + 600
#define UW_GRAB						0

#define UW_VINSPECTION			1

#define UW_VI_1				1 //INNER,PCHI
#define UW_VI_2				2 //UPPER,PCCA
#define UW_VI_3				3 //OUTER,PCCA2
//20160307_KYS_ADD
#define UW_VI_4				4 //LOWER,PCHI2

#define UM_VI_CLEAR					0
#define UM_VI_ONGOING				1
#define UM_VI_GRABBED				2
#define UM_VI_DONE					4
#define UM_VI_ERROR					8
#define UM_VI_GRAB_ERROR		0x10

//Cam Status
#define CAM_CLOSE				0
#define CAM_INSTANCE			1
#define CAM_OPEN				2

//VI THREADS
#define TH_VI_CLEAR			0 //
#define TH_VI_INSPECT		1 //
//#define TH_VI_					2 //

//CAM THREADS
#define TH_CAM_CLEAR			0 //
#define TH_CAM_CAPTURE		1 //

#define CAM_THREAD_COUNT 4

typedef struct _VI_MSG
{
	int BUSY; 
	int RSP[10]; //MESSAGE No.
	_VI_MSG(){	memset(this,0x00, sizeof(VI_MSG));}
}VI_MSG;

using namespace std;

#pragma pack(1)
typedef struct SMP_LIVE_TAG
{
	char LiveImage[4][LIVE_SIZE_W * LIVE_SIZE_H];
	int LiveGrabCamsRun;
	double AFValueCam[4];
	int LiveCheck[4];
	wchar_t wcPNo[64];
	BOOL paramSet[4];

	SMP_LIVE_TAG()
	{
		memset(this, 0x00, sizeof(SMP_LIVE_TAG));
	}
}SMP_LIVE;
#pragma pack()

typedef struct DisplayLive_AGT
{
	BOOL	bLiveRun;
	int		CamIndex;
	bool flagCapture[4];
	IplImage *m_ViewImage;
	SMP_LIVE *pImgData;

	DisplayLive_AGT()
	{
		memset(this, 0x00, sizeof(DisplayLive_AGT));
	}
}DsLive;


class CVisionInspection :public CDialogEx
{
	DECLARE_DYNAMIC(CVisionInspection)

public:
	CVisionInspection(CWnd* pParent = NULL);
	~CVisionInspection(void);

	queue<VI_DATA> queViData[MAX_VI_TEST];
	vector<VI_RESULT> queViResults[MAX_VI_TEST];

	enum { IDD = IDD_VISION };
	virtual BOOL OnInitDialog();
	void m_fnInit();
	void m_fnDeInit();
	int swLight[CAM_NO*2];

	int statusCam[CAM_NO];

protected:

	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()
	
	NIPJob m_dJob;

	void drawImage();
	void drawRect();
	void drawRect(int coords[4]);

	//Temp
	int points[4]; //might be less than max 32 defects in a image , memory fault_

public:

	IplImage *pRawBuf[4];
	IplImage *pImgBuf[4];
	IplImage *pOutBuf[4];
	int nPass[4];

	int m_nNoCoreCount;

#ifdef VIRTUAL_RUN
	IplImage *pRawBuf2[3];
#endif

	size_t ImageSize;

	int pylonGrab(int idx);

	int pylonSetParam(int idx);

	CStatic m_pic;
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	afx_msg void OnBnClickedLight(UINT nID);
	afx_msg void OnBnClickedBtnGrab(UINT nID);
	afx_msg void OnBnClickedBtnInspection(UINT nID);
	afx_msg void OnBnClickedBtnGrabSave(UINT nID);

	void SetUMMsgResponse(int msg, int rsp);
	int GetUMMsgResponse(int msg);

	bool LoadNIPJob();
	bool DoVisionInspection(wstring strComp, int nCamId, bool &bFoundDefect, bool &bFoundCore, bool &bLoadImage, VI_DATA vd, VI_RESULT& vr);

protected:
	afx_msg LRESULT OnUmVi(WPARAM wParam, LPARAM lParam);

	VI_MSG msgRsp;
public:
	CRoundButton2 m_btnLight[8];
	CRoundButton2 m_btnGrab[4];
	CRoundButton2 m_btnInspection[4];

	int m_fnSaveImage(int cam, int NG, long id);
	int m_fnSaveImage2(int cam, int NG, long id, IplImage *pImgBuf, wchar_t *szImageFileName = 0x00);

	CGridCtrl m_GridCamSetting;
	CRoundButton2 m_btnVIGridEnable;
	CRoundButton2 m_btnVIGridDisable;
	CRoundButton2 m_btnVIGridApply;
	int initGridCamSetting();
	int fillGridCamSetting(CString recipe);
	int applyGridCamSetting();
	afx_msg void OnBnClickedBtnViGridEnable();
	afx_msg void OnBnClickedBtnViGridDisable();
	afx_msg void OnBnClickedViApply();
	int saveCamSettingRecipe(CString recipe);
	int renewCamSettingParam();
	int pylonSetParam2();
	int pylonSetParam3();
	CRoundButton2 m_btnGrabSave[4];
	void clearVIResults();
	void applyRecipeInProduction();

	//Thread
	int m_fnBeginVIThread(void);
	int m_fnEndVIThread(void);
	CWinThread* m_pThreadsVI[CAM_THREAD_COUNT];
	volatile bool isThreadRunningVI[CAM_THREAD_COUNT];
//	bool m_bViProcess[4];
	int	suspendCntVI[4]; //thread 
	int retryVIreq[4];
	int flagThreadsVI[4];
	long partId[4];

	//Camera Thread
	int m_fnBeginCamThread(void);
	int m_fnEndCamThread(void);
	int		suspendCntCam[4]; //thread 
	int flagThreadsImage[4];
	CCriticalSection csData[4];
	CCriticalSection csResult[4];
	CCriticalSection csDisplay[4];

	HANDLE	hLIMapping;
	HANDLE	hMapRead;

	CWinThread *m_pLiveThread[4];
	DsLive	m_LiveHandls[4];
	SMP_LIVE *m_pImgData;

	int m_IsCalibrationErr[4];
	void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect, int CamIdx);
};

