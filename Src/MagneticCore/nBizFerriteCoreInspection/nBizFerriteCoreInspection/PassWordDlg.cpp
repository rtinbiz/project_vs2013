// PassWordDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "nBizFerriteCoreInspection.h"
#include "PassWordDlg.h"

//typedef BOOL(WINAPI *SLWA)(HWND,COLORREF,BYTE,DWORD);
//#define  WS_EX_LAYERED 0x00080000    // Long 80000
//#define  LWA_ALPHA  0x00000002           //Long 2

// CPassWordDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPassWordDlg, CDialog)

extern CMainWnd	*G_MainWnd;

CPassWordDlg::CPassWordDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPassWordDlg::IDD, pParent)
	, m_strEditPW(_T(""))
{

}

CPassWordDlg::~CPassWordDlg()
{
}

void CPassWordDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PW, m_strEditPW);
}


BEGIN_MESSAGE_MAP(CPassWordDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTNOK, &CPassWordDlg::OnBnClickedBtnok)
	ON_EN_CHANGE(IDC_EDIT_PW, &CPassWordDlg::OnEnChangeEditPw)
	ON_BN_CLICKED(IDC_BTN_EXIT, &CPassWordDlg::OnBnClickedBtnExit)	
END_MESSAGE_MAP()


// CPassWordDlg 메시지 처리기입니다.


BOOL CPassWordDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(pMsg->message)
	{
	case WM_KEYDOWN:
		switch(pMsg->wParam)
		{ 
		case VK_RETURN:
			OnBnClickedBtnok();			
			return TRUE;
		case VK_ESCAPE:			
			return TRUE;;
		}
	case WM_SYSKEYDOWN:
		switch(pMsg->wParam)
		{ 
			case VK_F4:			
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CPassWordDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_EDIT_PW)->SetActiveWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

HBRUSH CPassWordDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;

	switch (nCtlColor)
	{ 
	case CTLCOLOR_DLG:  // 다이얼로그
		hbr = pMainWnd->m_DILEDalog_Brush;
		pDC->SetBkColor(RGB(235,235,235));
		break;
	case CTLCOLOR_STATIC:  // 스태틱컨트롤
		hbr = pMainWnd->m_Static_Brush;
		pDC->SetBkColor(RGB(225,225,225));		
		pDC->SetTextColor(RGB(30,30,145));
		break;
	default:            
		break;
	}    
	return hbr;
}

void CPassWordDlg::OnBnClickedBtnok()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	if(m_strEditPW == G_MainWnd->m_DataHandling.m_SystemParam.sPassword)
	{
		m_strEditPW = "";
		UpdateData(FALSE);
		CDialog::OnOK();
	}
	else
		AfxMessageBox(_T("PassWord 오류"), MB_TOPMOST);

}

void CPassWordDlg::OnEnChangeEditPw()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialog::OnInitDialog() 함수를 재지정하고  마스크에 OR 연산하여 설정된
	// ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출해야만
	// 해당 알림 메시지를 보냅니다.


	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CPassWordDlg::OnBnClickedBtnExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog::OnCancel();
}
