#include "stdafx.h"
#include "PIO.h"
#include "nBizFerriteCoreInspection.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(PIO, CDialogEx)

extern CMainWnd	*G_MainWnd;


//PC 시스템에 장착된 총 보드 개수를 의미합니다.
#define MAX_BOARD_CNT 1

#pragma comment(lib,"C:\\Program Files\\Alpha Motion\\DigitalPro\\MApi(Library)\\Library\\tmcDApiAed_x64.lib")

//2016.04.06 jec add
UINT m_fnThreadDIODataUpdate(LPVOID lParam);

PIO::PIO(CWnd* pParent /*=NULL*/)
	: CDialogEx(PIO::IDD, pParent)
{
	InitializeCriticalSection(&m_criDIO);	
	memset(DI_prev, 0x00, sizeof(int)*IO_NUM);
	memset(DI, 0x00, sizeof(int)*IO_NUM);
	memset(DO, 0x00, sizeof(int)*IO_NUM);
	buzzstop = 0;

	//2016.04.06 jec add
	m_pThreads = NULL;
}

PIO::~PIO(void)
{
	LeaveCriticalSection(&m_criDIO);
	DeleteCriticalSection(&m_criDIO);
}


void PIO::DoDataExchange(CDataExchange* pDX)
{
	int id;
	CDialogEx::DoDataExchange(pDX);
	for (id = 0; id < 64; id++)
	{
		DDX_Control(pDX, IDC_CHK_DI_0 + id, m_DILED[id]);
		DDX_Control(pDX, IDC_CHK_DO_0 + id, m_DOLED[id]);
	}

	for (id = 0; id < 64; id++)
	{
		DDX_Control(pDX, IDC_CHK_DI_0 + id, m_DILED[id]);
		DDX_Control(pDX, IDC_CHK_DO_0 + id, m_DOLED[id]);
	}

	//for (id = 0; id <= OUT_STOPPER; id++) //  DO VI0,VI3 BUTTON ADD!!!  20160315_KYS
	//{
	//	DDX_Control(pDX, IDC_BTN_BATCH_START0 + id, m_btnStart[id]);
	//	DDX_Control(pDX, IDC_BTN_BATCH_STOP0 + id, m_btnStop[id]);
	//	DDX_Control(pDX, IDC_BTN_STEP0 + id, m_btnStep[id]);
	//	DDX_Control(pDX, IDC_BTN_ERROR_RESET0 + id, m_btnErrorReset[id]);
	//	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR0 + id, m_btnStepClear[id]);
	//}

	//for (id = OUT_CENTERING; id <= IN_CNVYR-1; id++) //TOTAL_STAGE,,,,DO VI0,VI3 BUTTON ADD!!!  20160315_KYS
	//{
	//	DDX_Control(pDX, IDC_BTN_BATCH_START6 + id - OUT_CENTERING, m_btnStart[id]);
	//	DDX_Control(pDX, IDC_BTN_BATCH_STOP6 + id - OUT_CENTERING, m_btnStop[id]);
	//	DDX_Control(pDX, IDC_BTN_STEP6 + id - OUT_CENTERING, m_btnStep[id]);
	//	DDX_Control(pDX, IDC_BTN_ERROR_RESET10 + id - OUT_CENTERING, m_btnErrorReset[id]);
	//	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR10 + id - OUT_CENTERING, m_btnStepClear[id]);
	//}

	DDX_Control(pDX, IDC_BTN_BATCH_START0, m_btnStart[0]);
	DDX_Control(pDX, IDC_BTN_BATCH_START1, m_btnStart[1]);
	DDX_Control(pDX, IDC_BTN_BATCH_START2, m_btnStart[2]);
	DDX_Control(pDX, IDC_BTN_BATCH_START3, m_btnStart[3]);
	DDX_Control(pDX, IDC_BTN_BATCH_START4, m_btnStart[4]);
	DDX_Control(pDX, IDC_BTN_BATCH_START5, m_btnStart[5]);
	DDX_Control(pDX, IDC_BTN_BATCH_START6, m_btnStart[6]);
	DDX_Control(pDX, IDC_BTN_BATCH_START7, m_btnStart[7]);
	DDX_Control(pDX, IDC_BTN_BATCH_START8, m_btnStart[8]);
	DDX_Control(pDX, IDC_BTN_BATCH_START9, m_btnStart[9]);
	DDX_Control(pDX, IDC_BTN_BATCH_START10, m_btnStart[10]);
	DDX_Control(pDX, IDC_BTN_BATCH_START11, m_btnStart[11]);
	
	DDX_Control(pDX, IDC_BTN_BATCH_STOP0, m_btnStop[0]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP1, m_btnStop[1]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP2, m_btnStop[2]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP3, m_btnStop[3]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP4, m_btnStop[4]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP5, m_btnStop[5]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP6, m_btnStop[6]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP7, m_btnStop[7]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP8, m_btnStop[8]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP9, m_btnStop[9]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP10, m_btnStop[10]);
	DDX_Control(pDX, IDC_BTN_BATCH_STOP11, m_btnStop[11]);

	DDX_Control(pDX, IDC_BTN_STEP0, m_btnStep[0]);
	DDX_Control(pDX, IDC_BTN_STEP1, m_btnStep[1]);
	DDX_Control(pDX, IDC_BTN_STEP2, m_btnStep[2]);
	DDX_Control(pDX, IDC_BTN_STEP3, m_btnStep[3]);
	DDX_Control(pDX, IDC_BTN_STEP4, m_btnStep[4]);
	DDX_Control(pDX, IDC_BTN_STEP5, m_btnStep[5]);
	DDX_Control(pDX, IDC_BTN_STEP6, m_btnStep[6]);
	DDX_Control(pDX, IDC_BTN_STEP7, m_btnStep[7]);
	DDX_Control(pDX, IDC_BTN_STEP8, m_btnStep[8]);
	DDX_Control(pDX, IDC_BTN_STEP9, m_btnStep[9]);
	DDX_Control(pDX, IDC_BTN_STEP10, m_btnStep[10]);
	DDX_Control(pDX, IDC_BTN_STEP11, m_btnStep[11]);

	DDX_Control(pDX, IDC_BTN_ERROR_RESET0, m_btnErrorReset[0]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET1, m_btnErrorReset[1]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET2, m_btnErrorReset[2]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET3, m_btnErrorReset[3]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET4, m_btnErrorReset[4]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET5, m_btnErrorReset[5]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET6, m_btnErrorReset[6]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET7, m_btnErrorReset[7]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET8, m_btnErrorReset[8]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET9, m_btnErrorReset[9]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET10, m_btnErrorReset[10]);
	DDX_Control(pDX, IDC_BTN_ERROR_RESET11, m_btnErrorReset[11]);

	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR0, m_btnStepClear[0]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR1, m_btnStepClear[1]);
    DDX_Control(pDX, IDC_BTN_BATCH_CLEAR2, m_btnStepClear[2]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR3, m_btnStepClear[3]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR4, m_btnStepClear[4]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR5, m_btnStepClear[5]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR6, m_btnStepClear[6]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR7, m_btnStepClear[7]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR8, m_btnStepClear[8]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR9, m_btnStepClear[9]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR10, m_btnStepClear[10]);
	DDX_Control(pDX, IDC_BTN_BATCH_CLEAR11, m_btnStepClear[11]);


	DDX_Control(pDX, IDC_BTN_OUT_CNVR_START, m_btnOutCnvyrStart);
	DDX_Control(pDX, IDC_BTN_OUT_CNVR_STOP, m_btnOutCnvyrStop);
	DDX_Control(pDX, IDC_BTN_OUT_CNVR_STEP, m_btnOutCnvyrStep);
	DDX_Control(pDX, IDC_BTN_OUT_CNVR_RESET, m_btnOutCnvyrErrorReset);
	DDX_Control(pDX, IDC_BTN_OUT_CNVR_CLEAR, m_btnOutCnvyrClear);

	DDX_Control(pDX, IDC_BTN_ALL_CLEAR, m_btnAllClear);
}


BEGIN_MESSAGE_MAP(PIO, CDialogEx)

	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_COMMAND_RANGE(IDC_CHK_DO_0, IDC_CHK_DO_63, &PIO::OnBnClickedChkDO)

	ON_COMMAND_RANGE(IDC_BTN_BATCH_START0, IDC_BTN_BATCH_START5, &PIO::OnBnClickedBtnBatchStart)
	ON_COMMAND_RANGE(IDC_BTN_BATCH_STOP0, IDC_BTN_BATCH_STOP5, &PIO::OnBnClickedBtnBatchStop)
	ON_COMMAND_RANGE(IDC_BTN_STEP0, IDC_BTN_STEP5, &PIO::OnBnClickedBtnStep)
	ON_COMMAND_RANGE(IDC_BTN_ERROR_RESET0, IDC_BTN_ERROR_RESET5, &PIO::OnBnClickedBtnErrorReset)
	ON_COMMAND_RANGE(IDC_BTN_BATCH_CLEAR0, IDC_BTN_BATCH_CLEAR5, &PIO::OnBnClickedBtnBatchClear)

	ON_COMMAND_RANGE(IDC_BTN_BATCH_START6, IDC_BTN_BATCH_START9, &PIO::OnBnClickedBtnBatchStart6)
	ON_COMMAND_RANGE(IDC_BTN_BATCH_STOP6, IDC_BTN_BATCH_STOP9, &PIO::OnBnClickedBtnBatchStop6)
	ON_COMMAND_RANGE(IDC_BTN_STEP6, IDC_BTN_STEP9, &PIO::OnBnClickedBtnStep6)
	ON_COMMAND_RANGE(IDC_BTN_ERROR_RESET10, IDC_BTN_ERROR_RESET13, &PIO::OnBnClickedBtnErrorReset6)
	ON_COMMAND_RANGE(IDC_BTN_BATCH_CLEAR10, IDC_BTN_BATCH_CLEAR13, &PIO::OnBnClickedBtnBatchClear6)



	ON_BN_CLICKED(IDC_BTN_OUT_CNVR_START, &PIO::OnBnClickedBtnOutCnvrStart)
	ON_BN_CLICKED(IDC_BTN_OUT_CNVR_STOP, &PIO::OnBnClickedBtnOutCnvrStop)
	ON_BN_CLICKED(IDC_BTN_OUT_CNVR_STEP, &PIO::OnBnClickedBtnOutCnvrStep)
	ON_BN_CLICKED(IDC_BTN_OUT_CNVR_RESET, &PIO::OnBnClickedBtnOutCnvrReset)
	ON_BN_CLICKED(IDC_BTN_OUT_CNVR_CLEAR, &PIO::OnBnClickedBtnOutCnvrClear)
	ON_BN_CLICKED(IDC_BTN_ALL_CLEAR, &PIO::OnBnClickedBtnAllClear)
END_MESSAGE_MAP()

BOOL PIO::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();




	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	for(int i = 0; i < 64; i++)
	{
		m_DILED[i].SetLedStatesNumber(LIGHT_STATE_SENTINEL);
		m_DILED[i].SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 16, 16);
		m_DILED[i].SetIcon(GRAY_LIGHT, IDI_ICON_TRANSPARENT, 16, 16);

		m_DOLED[i].SetLedStatesNumber(LIGHT_STATE_SENTINEL);
		m_DOLED[i].SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 16, 16);
		m_DOLED[i].SetIcon(GRAY_LIGHT, IDI_ICON_TRANSPARENT, 16, 16);
	}

	initIOLEDLabel();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



int PIO::initIOLEDLabel(void)
{
	int i;
	//clear
	for( i = 0; i < 64; i++)
	{
		m_DILED[i].SetWindowText(L"");
		m_DOLED[i].SetWindowText(L"");
	}

	for (i = 0; i <= IN_UNLOAD_NG_OUT_RET; i++)
	{
		m_DILED[i].SetWindowText(ST_FI_DI[i]);
	}
	for (i = 0; i <= OUT_UNLOAD_NG_OUT_RET; i++)
	{
		m_DOLED[i].SetWindowText(ST_FI_DO[i]);
	}

	return 0;
}

void PIO::m_fnInit(void)
{
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);	
	//2016.04.06 jec comment
	//SetTimer(WM_TIMER_IO, 10, NULL);	//10ms
	SetTimer(WM_TIMER_100mS, 100, NULL);	//100ms
	twlamp = 0;
	LCRLimitDisable = 0;
	wchar_t step[5];
	int nStep;
	for (int i = INPUT; i < OUT_CNVYR-2; i++) // 20160315_KYS_BUTTON_CREATE!!!
	{
		nStep = G_MainWnd->m_DataHandling.m_FlashData.data[STEP_INPUT + i];
		swprintf_s(step, L"%d", nStep);
		m_btnStepClear[i].SetWindowText(step);
	}

	//OUT_CNVYR
	//2016.04.06 jec add
	isThreadRunning = TRUE;
	m_pThreads = AfxBeginThread(m_fnThreadDIODataUpdate, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0, NULL); // THREAD_PRIORITY_NORMAL

	partPassing = 0;
}

void PIO::m_fnDeInit(void)
{
	//2016.04.06 jec comment
	//KillTimer(WM_TIMER_IO);	//10ms
	KillTimer(WM_TIMER_100mS);	//0.5s
	Sleep(100);

	// Load 된 Motion Control Board를 Unload 합니다.
	//TMC-AxxxxP 시리즈용 함수.
	//AIO_UnloadDevice();

	//AIO_pmiSysUnload();

	//2016.04.06 jec add
	isThreadRunning = FALSE;
	::WaitForSingleObject(m_pThreads->m_hThread, 5000); 
	m_pThreads = NULL;

}

void PIO::OnTimer(UINT_PTR nIDEvent)
{

//	int i;
//	int ret;
	unsigned int m = 0x00000001;
//	unsigned int m2;
//	int iRet;
	CString str;

	switch (nIDEvent)
	{
		case WM_TIMER_IO: //10ms
//
//#ifndef VIRTUAL_RUN
//
//			//TMC-AxxxxP 시리즈용 함수.
//			//AIO_GetDIDWord(CARD_IO, 0, &m_uiGetDin32Ch);
//			//AIO_GetDIDWord(CARD_IO, 1, &m_uiGetDin32Ch2);
//			//AIO_GetDODWord(CARD_IO, 0, &m_uiGetDout32Ch);
//			//AIO_GetDODWord(CARD_IO, 1, &m_uiGetDout32Ch2);
//
//			ret = pmiDiGetData(CARD_IO, 0, &m_uiGetDin32Ch); //TMC_RV_OK
//			ret = pmiDiGetData(CARD_IO, 1, &m_uiGetDin32Ch2);
//			ret = pmiDoGetData(CARD_IO, 0, &m_uiGetDout32Ch);
//			ret = pmiDoGetData(CARD_IO, 1, &m_uiGetDout32Ch2);
//
//			//LED status display
//			//EnterCriticalSection(&m_criDIO);
//			for(i = 0; i < 32; i++)
//			{
//				m2 = m<<i;
//				if( (m_uiGetDin32Ch & m2) == m2 ){DI[i]=1; m_DILED[i].SetLedState(GREEN_LIGHT);}else	{DI[i]=0; m_DILED[i].SetLedState(GRAY_LIGHT);}
//				if( (m_uiGetDin32Ch2 & m2) == m2 ){DI[32+i]=1; m_DILED[32+i].SetLedState(GREEN_LIGHT);}else{DI[32+i]=0; m_DILED[32+i].SetLedState(GRAY_LIGHT);}
//				if( (m_uiGetDout32Ch & m2) == m2 ){DO[i]=1;	m_DOLED[i].SetLedState(GREEN_LIGHT);}else{DO[i]=0; m_DOLED[i].SetLedState(GRAY_LIGHT);}
//				if( (m_uiGetDout32Ch2 & m2) == m2 ){DI[32+i]=1; m_DOLED[32+i].SetLedState(GREEN_LIGHT);}else	{DI[32+i]=0; m_DOLED[32+i].SetLedState(GRAY_LIGHT);}
//			}
//			//LeaveCriticalSection(&m_criDIO);
//#endif
//
//			//timer 
//			for(i = 0; i < IO_NUM; i++)
//			{
//				if(timer[i].On == 1 ){
//					if( timer[i].Accu <  timer[i].Set) 
//						timer[i].Accu++;
//					else if(timer[i].Accu ==  timer[i].Set)
//						timer[i].OTE = 1;
//				}
//			}
//
//			//EM STOP : Negative Edge
//			if(!LCRLimitDisable)
//			if(readDI_prev(IN_EM_STOP, CARD_IO) && !readDI(IN_EM_STOP, CARD_IO)){
//			//if(!readDI_prev(IN_EM_STOP, CARD_IO) && readDI(IN_EM_STOP, CARD_IO)){
//				G_MainWnd->m_InspectThread.EMStop();
//			}
//
//			////EDGE DETECT
//			//if (partPassing == 0 && readDI_prev(IN_LOAD_DETECT, CARD_IO) == 0 && readDI(IN_LOAD_DETECT, CARD_IO) == 1)
//			//{
//			//	partPassing = 1;
//			//}
//
//			///////////////////////////////////////////////////////////////////////////////////////////
//			////INPUT PART PASSING
//			//if (partPassing == 1 && readDI(IN_LOAD_STOP, CARD_IO) != 1)
//			//{
//			//	if (!timer[IN_LOAD_DETECT].On){
//			//		timer[IN_LOAD_DETECT].On = 1;
//			//		timer[IN_LOAD_DETECT].Set = G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_INPUT_CLOSE_DELAY];
//			//	}
//			//	else{
//			//		if (timer[IN_LOAD_DETECT].OTE == 1)
//			//		{
//			//			partPassing = 0;
//			//			iRet = writeDO(1, OUT_LOAD_STOP, CARD_IO);
//			//			if (iRet == TMC_RV_OK){
//
//			//			}
//			//			else{
//			//				str.Format(L"IO Write Error: Address: LOAD STEP3: OUT_LOAD_STOP");
//			//				G_AddLog(3, str.GetBuffer());
//			//				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
//			//			}
//			//			timer[IN_LOAD_DETECT].clear();
//			//		}
//			//	}
//			//}//if (partPassing)
//
//
//			//Edge detect
//			memcpy(DI_prev, DI, sizeof(int)*IO_NUM);
//
//			//general DIO 
//			timerCheckDIO();
//
//#ifdef VIRTUAL_RUN
//			virtualDIO();
//#endif

			break;

		//No Use
		case WM_TIMER_100mS:
			timerTowerLamp();

			break;

		default:

			break;


	}//switch (nIDEvent)

	CDialogEx::OnTimer(nIDEvent);
}


void PIO::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}

void PIO::OnBnClickedChkDO(UINT nID)
{
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(_T("It needs to be Manual Mode."),MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR){
		AfxMessageBox(_T("It needs to be in Engineer Mode."),MB_ICONHAND);
		return;
	}
	//2016,03,31 jec add
	if (nID == IDC_CHK_DO_26) return;

	unsigned int m_uiOutStatus;
	//U16 m_wOutStatus;
	int nDO = nID - IDC_CHK_DO_0;
	int iRet = 0;
	iRet = pmiDoGetBit(CARD_IO, nDO, &m_uiOutStatus);
	//AIO_GetDOBit(CARD_IO, nDO, &m_wOutStatus);
	if(iRet == TMC_RV_OK)
	{
		if( m_uiOutStatus == FALSE )
		//if( m_wOutStatus == 0 )
		{
			iRet = pmiDoSetBit(CARD_IO, nDO, TMC_BIT_ON);
			//AIO_PutDOBit(CARD_IO, nDO, CMD_ON);
		}else{
			iRet = pmiDoSetBit(CARD_IO, nDO, TMC_BIT_OFF);
			//AIO_PutDOBit(CARD_IO, nDO, CMD_OFF);
		}
	}
}

//50ms Time Interval
int PIO::timerAutoRunDIO(void)
{


	///////////////////////////////////////////////////////////////////////////////////////////
	//

	return 0;
}

int PIO::readDI(int address, int board)
{
	int val;
	//EnterCriticalSection(&m_criDIO);
	val = DI[address];
	//DI_prev[address] = val;
	//LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::readDI_prev(int address, int board)
{
	int val;
	//EnterCriticalSection(&m_criDIO);
	val = DI_prev[address];
	//LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::readDO(int address, int board)
{
	int val;
	//EnterCriticalSection(&m_criDIO);
	val = DO[address];
	//LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::writeDO(int val, int address, int board)
{	
	//U16 iRet;
	int iRet;
//	unsigned int iVal;
	CString str;

#ifndef VIRTUAL_RUN

	//EnterCriticalSection(&m_criDIO);
	iRet = pmiDoSetBit(board, address, val); //TMC_BIT_ON
	//TMC-AxxxxP 시리즈용 함수.
	//AIO_PutDOBit(CARD_IO, address, val);
	//LeaveCriticalSection(&m_criDIO);
	/*
	Sleep(5);
	//AIO_GetDOBit(CARD_IO, address, &iVal);
	iRet = pmiDoGetBit(board, address, &iVal); //TMC_BIT_ON

	if(val != (int)(iVal & 0xFFFF))
	{
#ifndef DEV_MODE
		str.Format(L"IO Write Error: Address:%d, Value:%d", address, val);
		G_AddLog(3, str.GetBuffer()); 
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str , BLACK, RED);
		str.ReleaseBuffer();
#endif
		return 0;
	}
	*/
	return TMC_RV_OK;
	
#else
	DO[address] = val;
	Sleep(5);
	return TMC_RV_OK;

#endif

}

int PIO::timerTowerLamp(void)
{
//	int iRet;
	CString str;

	/////////////////////////////////////////////////////////////////////////////////////////
	// Tower Lamp
	if (G_MainWnd->m_InspectThread.checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR || !readDI(IN_EM_STOP, CARD_IO)){
		if (++twlamp >= 10){
			twlamp = 0;
			if (!readDO(OUT_TOWER_RED, CARD_IO)){
				writeDO(1, OUT_TOWER_RED, CARD_IO);
#ifndef DEV_MODE
				if (!buzzstop) writeDO(1, OUT_BUZZER, CARD_IO);
#endif
			}
			else{
				writeDO(0, OUT_TOWER_RED, CARD_IO);
				writeDO(0, OUT_BUZZER, CARD_IO);
			}
		}
	}
	else{
		if (readDO(OUT_TOWER_RED, CARD_IO))
			writeDO(0, OUT_TOWER_RED, CARD_IO);
		if (readDO(OUT_BUZZER, CARD_IO))
			writeDO(0, OUT_BUZZER, CARD_IO);

		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			if (!readDO(OUT_TOWER_GREEN, CARD_IO))
				writeDO(1, OUT_TOWER_GREEN, CARD_IO);
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_AUTO){
			if (++twlamp >= 10){
				twlamp = 0;
				if (!readDO(OUT_TOWER_GREEN, CARD_IO))
					writeDO(1, OUT_TOWER_GREEN, CARD_IO);
				else
					writeDO(0, OUT_TOWER_GREEN, CARD_IO);
			}
			if (readDO(OUT_TOWER_YELLOW, CARD_IO))
				writeDO(0, OUT_TOWER_YELLOW, CARD_IO);
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL){
			if (!readDO(OUT_TOWER_YELLOW, CARD_IO))
				writeDO(1, OUT_TOWER_YELLOW, CARD_IO);
			if (readDO(OUT_TOWER_GREEN, CARD_IO))
				writeDO(0, OUT_TOWER_GREEN, CARD_IO);
		}
	}

#ifndef 	VIRTUAL_RUN
	if (!readDI(IN_AIR_SENSOR, CARD_IO))
	{
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			G_MainWnd->m_ServerDlg.OnBnClickedBtnStop();
			G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_AUTO){
			G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

		}
		if (G_SystemModeData.unSystemError != SYSTEM_ERROR){
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			G_AddLog(3, L"AIR PRESSURE LOW. CHECK AIR PRESSURE");
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"AIR PRESSURE LOW. CHECK AIR PRESSURE", BLACK, RED);
		}

	}
#endif

//20160301_BEFORE_CONV_RUN
	if (//G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN&&
		G_SystemModeData.unSystemError == SYSTEM_OK
		)
	{
		if (readDO(OUT_BEFORE_CONV_RUN, CARD_IO))
		writeDO(0, OUT_BEFORE_CONV_RUN, CARD_IO);
	}
	else
	{
		if (!readDO(OUT_BEFORE_CONV_RUN, CARD_IO))
		writeDO(1, OUT_BEFORE_CONV_RUN, CARD_IO);
	}
///////////////////////
	return 0;
}


int PIO::timerCheckDIO(void)
{
	int iRet;
	CString str;

	///////////////////////////////////////////////////////////////////////////////////////////
	////INPUT PART PASSING
	//if (partPassing == 1 && readDI(IN_LOAD_STOP, CARD_IO) != 1)
	//{
	//	if (!timer[IN_LOAD_DETECT].On){
	//		timer[IN_LOAD_DETECT].On = 1;
	//		//timer[IN_LOAD_DETECT].Set = T_WAIT_LOAD_OPEN; // * 100ms
	//		timer[IN_LOAD_DETECT].Set = G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_INPUT_CLOSE_DELAY];
	//	}
	//	else{
	//		if (timer[IN_LOAD_DETECT].OTE == 1)
	//		{
	//			partPassing = 0;
	//			iRet = writeDO(1, OUT_LOAD_STOP, CARD_IO);
	//			if (iRet == TMC_RV_OK){

	//			}
	//			else{
	//				str.Format(L"IO Write Error: Address: LOAD STEP3: OUT_LOAD_STOP");
	//				G_AddLog(3, str.GetBuffer());
	//				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
	//			}
	//			timer[IN_LOAD_DETECT].clear();
	//		}
	//	}
	//}//if (partPassing)

	/////////////////////////////////////////////////////////////////////////////////////////
	//turn off DO when DI on 

	//LOAD
	if (readDO(OUT_LOAD_STOP, CARD_IO) && readDI(IN_LOAD_STOP, CARD_IO))
	{
		if (!timer[IN_LOAD_STOP].On){
			timer[IN_LOAD_STOP].On = 1;
			timer[IN_LOAD_STOP].Set = T_WAIT; //
		}
		else{
			if (timer[IN_LOAD_STOP].OTE == 1)
			{
				iRet = writeDO(0, OUT_LOAD_STOP, CARD_IO);
				timer[IN_LOAD_STOP].clear();
			}
		}
	}//if(readDO(

	if (readDO(OUT_LOAD_OPEN, CARD_IO) && readDI(IN_LOAD_OPEN, CARD_IO))
	{
		if (!timer[IN_LOAD_OPEN].On){
			timer[IN_LOAD_OPEN].On = 1;
			timer[IN_LOAD_OPEN].Set = 1; //
		}
		else{
			if (timer[IN_LOAD_OPEN].OTE == 1)
			{
				iRet = writeDO(0, OUT_LOAD_OPEN, CARD_IO);
				timer[IN_LOAD_OPEN].clear();
			}
		}
	}//if(readDO(

	//STG1_CTR_FWD
	if (readDO(OUT_STG1_CTR_FWD, CARD_IO) && readDI(IN_STG1_CTR_FWD, CARD_IO))
	{
		if (!timer[IN_STG1_CTR_FWD].On){
			timer[IN_STG1_CTR_FWD].On = 1;
			timer[IN_STG1_CTR_FWD].Set = T_WAIT_2X;
		}
		else{
			if (timer[IN_STG1_CTR_FWD].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG1_CTR_FWD, CARD_IO);
				//
				timer[IN_STG1_CTR_FWD].clear();
			}
		}
	}//

	//STG1_CTR_RET
	if (readDO(OUT_STG1_CTR_RET, CARD_IO) && readDI(IN_STG1_CTR_RET, CARD_IO))
	{
		if (!timer[IN_STG1_CTR_RET].On){
			timer[IN_STG1_CTR_RET].On = 1;
			timer[IN_STG1_CTR_RET].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG1_CTR_RET].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG1_CTR_RET, CARD_IO);
				//
				timer[IN_STG1_CTR_RET].clear();
			}
		}
	}//

	//STG4_STOP_OPEN
	if (readDO(OUT_STG4_STOP_OPEN, CARD_IO) && readDI(IN_STG4_STOP_OPEN, CARD_IO))
	{
		if (!timer[IN_STG4_STOP_OPEN].On){
			timer[IN_STG4_STOP_OPEN].On = 1;
			timer[IN_STG4_STOP_OPEN].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG4_STOP_OPEN].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG4_STOP_OPEN, CARD_IO);
				//
				timer[IN_STG4_STOP_OPEN].clear();
			}
		}
	}//

	//STG4_STOP_CLOSE
	if (readDO(OUT_STG4_STOP_CLOSE, CARD_IO) && readDI(IN_STG4_STOP_CLOSE, CARD_IO))
	{
		if (!timer[IN_STG4_STOP_CLOSE].On){
			timer[IN_STG4_STOP_CLOSE].On = 1;
			timer[IN_STG4_STOP_CLOSE].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG4_STOP_CLOSE].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG4_STOP_CLOSE, CARD_IO);
				//
				timer[IN_STG4_STOP_CLOSE].clear();
			}
		}
	}//

	//STG5_CTR_FWD
	if (readDO(OUT_STG5_CTR_FWD, CARD_IO) && readDI(IN_STG5_CTR_FWD, CARD_IO))
	{
		if (!timer[IN_STG5_CTR_FWD].On){
			timer[IN_STG5_CTR_FWD].On = 1;
			timer[IN_STG5_CTR_FWD].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG5_CTR_FWD].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG5_CTR_FWD, CARD_IO);
				//
				timer[IN_STG5_CTR_FWD].clear();
			}
		}
	}//

	//STG5_CTR_RET
	if (readDO(OUT_STG5_CTR_RET, CARD_IO) && readDI(IN_STG5_CTR_RET, CARD_IO))
	{
		if (!timer[IN_STG5_CTR_RET].On){
			timer[IN_STG5_CTR_RET].On = 1;
			timer[IN_STG5_CTR_RET].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG5_CTR_RET].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG5_CTR_RET, CARD_IO);
				//
				timer[IN_STG5_CTR_RET].clear();
			}
		}
	}//

	//STG6_CAM_DOWN
	if (readDO(OUT_STG6_CAM_DOWN, CARD_IO) && readDI(IN_STG6_CAM_DOWN, CARD_IO))
	{
		if (!timer[IN_STG6_CAM_DOWN].On){
			timer[IN_STG6_CAM_DOWN].On = 1;
			timer[IN_STG6_CAM_DOWN].Set = T_WAIT; 
		}
		else{
			if (timer[IN_STG6_CAM_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG6_CAM_DOWN, CARD_IO);
				//
				timer[IN_STG6_CAM_DOWN].clear();
			}
		}
	}//if(readDO(OUT_STG6_CAM_DOWN, CARD_IO) &&

	//OUT_STG6_CAM_UP
	if (readDO(OUT_STG6_CAM_UP, CARD_IO) && readDI(IN_STG6_CAM_UP, CARD_IO))
	{
		if (!timer[IN_STG6_CAM_UP].On){
			timer[IN_STG6_CAM_UP].On = 1;
			timer[IN_STG6_CAM_UP].Set = T_WAIT;
		}
		else{
			if (timer[IN_STG6_CAM_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_STG6_CAM_UP, CARD_IO);
				//
				timer[IN_STG6_CAM_UP].clear();
			}
		}
	}//if(readDO(OUT_STG6_CAM_UP, CARD_IO) &&


	//OUT_UNLOAD_NG_OUT_FWD
	if (readDO(OUT_UNLOAD_NG_OUT_FWD, CARD_IO) && readDI(IN_UNLOAD_NG_OUT_FWD, CARD_IO))
	{
		//if (!timer[IN_UNLOAD_NG_OUT_FWD].On){
		//	timer[IN_UNLOAD_NG_OUT_FWD].On = 1;
		//	timer[IN_UNLOAD_NG_OUT_FWD].Set = T_WAIT;
		//}
		//else{
		//	if (timer[IN_UNLOAD_NG_OUT_FWD].OTE == 1)
		//	{
		//		iRet = writeDO(0, OUT_UNLOAD_NG_OUT_FWD, CARD_IO);
		//		//
		//		timer[IN_UNLOAD_NG_OUT_FWD].clear();
		//	}
		//}
		iRet = writeDO(0, OUT_UNLOAD_NG_OUT_FWD, CARD_IO);
		iRet = writeDO(1, OUT_UNLOAD_NG_OUT_RET, CARD_IO);


	}//if(readDO(OUT_UNLOAD_NG_OUT_FWD, CARD_IO) &&

	//OUT_UNLOAD_NG_OUT_RET
	if (readDO(OUT_UNLOAD_NG_OUT_RET, CARD_IO) && readDI(IN_UNLOAD_NG_OUT_RET, CARD_IO))
	{
		if (!timer[IN_UNLOAD_NG_OUT_RET].On){
			timer[IN_UNLOAD_NG_OUT_RET].On = 1;
			timer[IN_UNLOAD_NG_OUT_RET].Set = T_WAIT;
		}
		else{
			if (timer[IN_UNLOAD_NG_OUT_RET].OTE == 1)
			{
				iRet = writeDO(0, OUT_UNLOAD_NG_OUT_RET, CARD_IO);
				//
				timer[IN_UNLOAD_NG_OUT_RET].clear();
			}
		}
	}//if(readDO(OUT_UNLOAD_NG_OUT_RET, CARD_IO) &&

	return 0;
}


int PIO::virtualDIO(void)
{
	int iRet;
	CString str;

	/////////////////////////////////////////////////////////////////////////////////////////
	//DO turns DI on / off

	//LOAD
	if (readDO(OUT_LOAD_STOP, CARD_IO))
	{
		DI[IN_LOAD_STOP] = 1;
	}//if(readDO(

	if (readDO(OUT_LOAD_OPEN, CARD_IO))
	{
		DI[IN_LOAD_OPEN] = 1;
	}//if(readDO(

	//STG1_CTR_FWD
	if (readDO(OUT_STG1_CTR_FWD, CARD_IO) )
	{
		DI[IN_STG1_CTR_FWD] = 1;
	}//

	//STG1_CTR_RET
	if (readDO(OUT_STG1_CTR_RET, CARD_IO))
	{
		DI[IN_STG1_CTR_RET] = 1;
	}//

	//STG4_STOP_OPEN
	if (readDO(OUT_STG4_STOP_OPEN, CARD_IO))
	{
		DI[IN_STG4_STOP_OPEN] = 1;
	}//

	//STG4_STOP_CLOSE
	if (readDO(OUT_STG4_STOP_CLOSE, CARD_IO))
	{
		DI[IN_STG4_STOP_CLOSE] = 1;
	}//

	//STG5_CTR_FWD
	if (readDO(OUT_STG5_CTR_FWD, CARD_IO))
	{
		DI[IN_STG5_CTR_FWD] = 1;
	}//

	//STG5_CTR_RET
	if (readDO(OUT_STG5_CTR_RET, CARD_IO))
	{
		DI[IN_STG5_CTR_RET] = 1;
	}//

	//STG6_CAM_DOWN
	if (readDO(OUT_STG6_CAM_DOWN, CARD_IO))
	{
		DI[IN_STG6_CAM_DOWN] = 1;
	}//if(readDO(OUT_STG6_CAM_DOWN, CARD_IO) &&

	//OUT_STG6_CAM_UP
	if (readDO(OUT_STG6_CAM_UP, CARD_IO))
	{
		DI[IN_STG6_CAM_UP] = 1;
	}//if(readDO(OUT_STG6_CAM_UP, CARD_IO) &&


	//OUT_UNLOAD_NG_OUT_FWD
	if (readDO(OUT_UNLOAD_NG_OUT_FWD, CARD_IO))
	{
		iRet = writeDO(0, OUT_UNLOAD_NG_OUT_FWD, CARD_IO);
		iRet = writeDO(1, OUT_UNLOAD_NG_OUT_RET, CARD_IO);
	}//if(readDO(OUT_UNLOAD_NG_OUT_FWD, CARD_IO) &&

	//OUT_UNLOAD_NG_OUT_RET
	if (readDO(OUT_UNLOAD_NG_OUT_RET, CARD_IO))
	{
		DI[IN_UNLOAD_NG_OUT_RET] = 1;
	}//if(readDO(OUT_UNLOAD_NG_OUT_RET, CARD_IO) &&

	return 0;
}




int PIO::home(void)
{
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{


	}
	return 0;
}


void PIO::OnBnClickedBtnBatchStart(UINT nID)
{
	int batch = nID - IDC_BTN_BATCH_START0;

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0){
			G_MainWnd->m_InspectThread.setBatchStatus(batch, CLEAR);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		}

		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22)
			G_MainWnd->m_InspectThread.continueBatch(batch);
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode."), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnBatchStop(UINT nID)
{
	int iRet;
	int batch = nID - IDC_BTN_BATCH_STOP0;

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdBatch(batch);
		if (batch == IN_CNVYR){//IN_CNVYR Axis stop
			iRet = pmiAxStop(CARD_NO, AXIS_IN_CNVYR);
			if (iRet != TMC_RV_OK){
				//Error

			}
		}
		if (batch == OUT_CNVYR){//OUT_CNVYR Axis stop
			iRet = pmiAxStop(CARD_NO, AXIS_OUT_CNVYR);
			if (iRet != TMC_RV_OK){
				//Error

			}
		}
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode. "), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnStep(UINT nID)
{
	int batch = nID - IDC_BTN_STEP0;
	int ret;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0){
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		}
		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22){
			G_MainWnd->m_InspectThread.continueBatch(batch);
		}
		else{
			switch (batch){
				case INPUT:				{ ret = G_MainWnd->m_InspectThread.stg0Input(); }break;
				case IN_CENTERING:		{ ret = G_MainWnd->m_InspectThread.stg1Center(); }break;
				case IN_VI1:				{ ret = G_MainWnd->m_InspectThread.stg2VI1(); }break;
				case IN_BUFFER:			{ ret = G_MainWnd->m_InspectThread.stg3Buffer(); }break;  
				case OUT_FLIPDROP:	{ ret = G_MainWnd->m_InspectThread.stg4Flip(); }break;
				case OUT_STOPPER:		{ ret = G_MainWnd->m_InspectThread.stg5Stopper(); }break;
				case OUT_CENTERING:	{ ret = G_MainWnd->m_InspectThread.stg6Center(); }break;
				case OUT_VI2:			{ ret = G_MainWnd->m_InspectThread.stg7VI2(); }break;
				case OUT_NGOUT:		{ ret = G_MainWnd->m_InspectThread.stg8NGout(); }break;
				case IN_CNVYR:			{ ret = G_MainWnd->m_InspectThread.stg9InConveyorRun(); }break;
				case OUT_CNVYR:		{ ret = G_MainWnd->m_InspectThread.stg10OutConveyorRun(); }break;
			}
		}
		G_MainWnd->m_InspectThread.batchTimer();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnErrorReset(UINT nID)
{
	int batch = nID - IDC_BTN_ERROR_RESET0;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP19){
			m_btnErrorReset[batch].SetColorChange(WHITE, BLACK);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP20); //put it in error reset step
		}
		//m_btnErrorReset[batch].SetColorChange(RGB(200, 153, 204),RGB(0,0,0)); 
		m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnBatchClear(UINT nID)
{
	int batch = nID - IDC_BTN_BATCH_CLEAR0;
	CString str;
	if (IDCANCEL == AfxMessageBox(_T("BATCH CLEAR will reset Batch step and status to initial state. Want to preceed?"), MB_OKCANCEL | MB_ICONQUESTION))
		return;
	G_MainWnd->m_InspectThread.stStgBatch[batch].status = CLEAR;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetPrevBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.setBatchStatus(batch, CLEAR);
		if (batch < OUT_FLIPDROP){
			G_MainWnd->m_InspectThread.m_fnAllowConveyor(IN_CNVYR, TRUE, batch);
			G_MainWnd->m_InspectThread.m_fnRequestConveyor(IN_CNVYR, FALSE, batch);
		}
		if (batch >= OUT_FLIPDROP && batch < IN_CNVYR){
			G_MainWnd->m_InspectThread.m_fnAllowConveyor(OUT_CNVYR, TRUE, batch);
			G_MainWnd->m_InspectThread.m_fnRequestConveyor(OUT_CNVYR, FALSE, batch);

			//Inspection Part Data Clear
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCHI] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCHI].clear();
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCCA] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCCA].clear();
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCCA2] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCCA2].clear();

		}
		
		//m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
		m_btnStepClear[batch].SetWindowText(L"0");
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}

////OUT_VI2 - OUT_CNVYR

void PIO::OnBnClickedBtnBatchStart6(UINT nID)
{
	int batch = nID - IDC_BTN_BATCH_START6 + 6;

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0)
		{
			G_MainWnd->m_InspectThread.setBatchStatus(batch, CLEAR);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		}
		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22)
			G_MainWnd->m_InspectThread.continueBatch(batch);
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode."), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnBatchStop6(UINT nID)
{
	int iRet;
	int batch = nID - IDC_BTN_BATCH_STOP6 + 6;

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdBatch(batch);
		if (batch == IN_CNVYR){//IN_CNVYR Axis stop
			iRet = pmiAxStop(CARD_NO, AXIS_IN_CNVYR);
			if (iRet != TMC_RV_OK){
				//Error

			}
		}
		if (batch == OUT_CNVYR){//OUT_CNVYR Axis stop
			iRet = pmiAxStop(CARD_NO, AXIS_OUT_CNVYR);
			if (iRet != TMC_RV_OK){
				//Error

			}
		}
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode. "), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnStep6(UINT nID)
{
	int batch = nID - IDC_BTN_STEP6 + 6;
	int ret;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0){
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		}
		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22){
			G_MainWnd->m_InspectThread.continueBatch(batch);
		}
		else{
			switch (batch){
			case INPUT:				{ ret = G_MainWnd->m_InspectThread.stg0Input(); }break;
			case IN_CENTERING:		{ ret = G_MainWnd->m_InspectThread.stg1Center(); }break;
			case IN_VI1:				{ ret = G_MainWnd->m_InspectThread.stg2VI1(); }break;
			case OUT_FLIPDROP:	{ ret = G_MainWnd->m_InspectThread.stg4Flip(); }break;
			case OUT_STOPPER:		{ ret = G_MainWnd->m_InspectThread.stg5Stopper(); }break;
			case OUT_CENTERING:	{ ret = G_MainWnd->m_InspectThread.stg6Center(); }break;
			case OUT_VI2:			{ ret = G_MainWnd->m_InspectThread.stg7VI2(); }break;
			case OUT_NGOUT:		{ ret = G_MainWnd->m_InspectThread.stg8NGout(); }break;
			case IN_CNVYR:			{ ret = G_MainWnd->m_InspectThread.stg9InConveyorRun(); }break;
			case OUT_CNVYR:		{ ret = G_MainWnd->m_InspectThread.stg10OutConveyorRun(); }break;
			}
		}
		G_MainWnd->m_InspectThread.batchTimer();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnErrorReset6(UINT nID)
{
	int batch = nID - IDC_BTN_ERROR_RESET10 + 6;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP19){
			m_btnErrorReset[batch].SetColorChange(WHITE, BLACK);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP20); //put it in error reset step
		}
		//m_btnErrorReset[batch].SetColorChange(RGB(200, 153, 204),RGB(0,0,0)); 
		m_btnErrorReset[batch].SetColorChange(RGB(255, 255, 255), RGB(0, 0, 0));
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnBatchClear6(UINT nID)
{
	int batch = nID - IDC_BTN_BATCH_CLEAR10 + 6;

	if (IDCANCEL == AfxMessageBox(_T("BATCH CLEAR will reset Batch step and status to initial state. Want to preceed?"), MB_OKCANCEL | MB_ICONQUESTION))
		return;
	G_MainWnd->m_InspectThread.stStgBatch[batch].status = CLEAR;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetPrevBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.setBatchStatus(batch, CLEAR);
		if (batch <= OUT_FLIPDROP){
			G_MainWnd->m_InspectThread.m_fnAllowConveyor(IN_CNVYR, TRUE, batch);
			G_MainWnd->m_InspectThread.m_fnRequestConveyor(IN_CNVYR, FALSE, batch);
		}
		if (batch >= OUT_FLIPDROP && batch < IN_CNVYR){
			G_MainWnd->m_InspectThread.m_fnAllowConveyor(OUT_CNVYR, TRUE, batch);
			G_MainWnd->m_InspectThread.m_fnRequestConveyor(OUT_CNVYR, FALSE, batch);

			//Inspection Part Data Clear
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCHI] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCHI].clear();
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCCA] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCCA].clear();
			G_MainWnd->m_InspectThread.m_PartData[batch].results[CAM_PCCA2] = VI_CLEAR;
			//G_MainWnd->m_InspectThread.m_PartData[batch].vecDefects[CAM_PCCA2].clear();
		}



		//m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
		m_btnStepClear[batch].SetWindowText(L"0");
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}

//////

int PIO::m_fnInitDIO()
{

	// Motion Control Board를 Load 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int nRet = 0;
	int nConNum = 0;

	CString strMsg = _T("");

	//TMC-AxxxxP 시리즈용 함수.
	//nRet = AIO_LoadDevice();

	//TMC-BxxxxP 시리즈용 함수.
	//nRet = AIO_pmiSysLoad( TMC_FALSE, &nConNum );
	//nRet = AIO_pmiSysLoad( TMC_TRUE, &nConNum );

	//TMC-AxxxxP 시리즈용으로 사용하고자 할 경우.
	//if( nRet >= 0 )

	//TMC-BxxxxP 시리즈용으로 사용하고자 할 경우.
	if (nConNum >= 1)
	{
		//MessageBox(_T("Success!!!"));
	}
	else
	{
		// Motion Conrol Board가 정상적으로 Load 되지 않았을때
		// 메세지 창에 에러 Load 에러 값을 표시 합니다.
		strMsg.Format(_T("Motion Control Board IO Load Fail\nError No. = %d"), nRet);
		MessageBox(strMsg);
		return FALSE;
	}

	//unsigned long dwModel, dwComm, dwDiNum, dwDoNum = 0;

	//TMC-AxxxxP 시리즈용 함수.
	//AIO_BoardInfo(CARD_IO, &dwModel, &dwComm, &dwDiNum, &dwDoNum);
	//G_AddLog(3, L"IO Card info: dwModel: %d, dwComm: %d, dwDiNum: %d, dwDoNum:%d", dwModel, dwComm, dwDiNum, dwDoNum);

	// if( MAX_BOARD_CNT != nConNum )
	return 0;
}


//OUT CONVEYOR
void PIO::OnBnClickedBtnOutCnvrStart()
{
	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CNVYR) == STEP0)
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(OUT_CNVYR, STEP1);
		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CNVYR) == STEP22)
			G_MainWnd->m_InspectThread.continueBatch(OUT_CNVYR);
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode."), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnOutCnvrStop()
{
	int iRet;

	if (G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdBatch(OUT_CNVYR);

		if (OUT_CNVYR == OUT_CNVYR){//OUT_CNVYR Axis stop
			iRet = pmiAxStop(CARD_NO, AXIS_OUT_CNVYR);
			if (iRet != TMC_RV_OK){
				//Error

			}
		}
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be AUTO (STOP) Mode. "), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnOutCnvrStep()
{
	int ret;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CNVYR) == STEP0){
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(OUT_CNVYR, STEP1);
		}
		else if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CNVYR) == STEP22){
			G_MainWnd->m_InspectThread.continueBatch(OUT_CNVYR);
		}
		else{
			ret = G_MainWnd->m_InspectThread.stg10OutConveyorRun();
		}
		G_MainWnd->m_InspectThread.batchTimer();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnOutCnvrReset()
{
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if (G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CNVYR) == STEP19){
			m_btnOutCnvyrErrorReset.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(OUT_CNVYR, STEP20); //put it in error reset step
		}
		//m_btnOutCnvyrErrorReset.SetColorChange(RGB(200, 153, 204),RGB(0,0,0)); 
		m_btnOutCnvyrErrorReset.SetColorChange(RGB(255, 255, 255), RGB(0, 0, 0));
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnOutCnvrClear()
{
	CString str;
	if (IDCANCEL == AfxMessageBox(_T("BATCH CLEAR will reset Batch step and status to initial state. Want to preceed?"), MB_OKCANCEL | MB_ICONQUESTION))
		return;
	G_MainWnd->m_InspectThread.stStgBatch[OUT_CNVYR].status = CLEAR;
	if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.m_fnSetBatchStep(OUT_CNVYR, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetPrevBatchStep(OUT_CNVYR, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(OUT_CNVYR, STEP0);
		G_MainWnd->m_InspectThread.setBatchStatus(OUT_CNVYR, CLEAR);
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
		m_btnStepClear[OUT_CNVYR].SetWindowText(L"0");
	}
	else{
		AfxMessageBox(_T("It needs to be MANUAL Mode"), MB_ICONINFORMATION);
	}
}

// ALL STAGE CLEAR
void PIO::OnBnClickedBtnAllClear()
{
	CString strBuff;
	if (G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		strBuff.Format(_T(" All Clear can be done in Manual Mode "));
		AfxMessageBox(strBuff);
		return;
	}
	if (IDCANCEL == AfxMessageBox(_T("ALL CLEAR will reset Batch step and status to initial state. Want to preceed?"), MB_OKCANCEL | MB_ICONQUESTION))
		return;
	
	G_MainWnd->m_InspectThread.m_fnClearParts();

}

//2016.04.06 jec add
UINT m_fnThreadDIODataUpdate(LPVOID lParam)
{
	PIO *pDIO = (PIO *) lParam;

	int ret = 0L;
	unsigned int m2=0;
	unsigned int m = 0x00000001;
	int i = 0;
	while (pDIO->isThreadRunning)
	{
#ifndef VIRTUAL_RUN

		ret = pmiDiGetData(CARD_IO, 0, &pDIO->m_uiGetDin32Ch); //TMC_RV_OK
		ret = pmiDiGetData(CARD_IO, 1, &pDIO->m_uiGetDin32Ch2);
		ret = pmiDoGetData(CARD_IO, 0, &pDIO->m_uiGetDout32Ch);
		ret = pmiDoGetData(CARD_IO, 1, &pDIO->m_uiGetDout32Ch2);

		//LED status display
		//EnterCriticalSection(&m_criDIO);
		for(i = 0; i < 32; i++)
		{
			m2 = m<<i;
			if( (pDIO->m_uiGetDin32Ch & m2) == m2 ){pDIO->DI[i]=1; pDIO->m_DILED[i].SetLedState(GREEN_LIGHT);}else	{pDIO->DI[i]=0; pDIO->m_DILED[i].SetLedState(GRAY_LIGHT);}
			if( (pDIO->m_uiGetDin32Ch2 & m2) == m2 ){pDIO->DI[32+i]=1; pDIO->m_DILED[32+i].SetLedState(GREEN_LIGHT);}else{pDIO->DI[32+i]=0; pDIO->m_DILED[32+i].SetLedState(GRAY_LIGHT);}
			if( (pDIO->m_uiGetDout32Ch & m2) == m2 ){pDIO->DO[i]=1;	pDIO->m_DOLED[i].SetLedState(GREEN_LIGHT);}else{pDIO->DO[i]=0; pDIO->m_DOLED[i].SetLedState(GRAY_LIGHT);}
			if( (pDIO->m_uiGetDout32Ch2 & m2) == m2 ){pDIO->DI[32+i]=1; pDIO->m_DOLED[32+i].SetLedState(GREEN_LIGHT);}else	{pDIO->DI[32+i]=0; pDIO->m_DOLED[32+i].SetLedState(GRAY_LIGHT);}
		}
		//LeaveCriticalSection(&m_criDIO);
#endif

		//timer 
		for(i = 0; i < IO_NUM; i++)
		{
			if(pDIO->timer[i].On == 1 ){
				if( pDIO->timer[i].Accu <  pDIO->timer[i].Set) 
					pDIO->timer[i].Accu++;
				else if(pDIO->timer[i].Accu ==  pDIO->timer[i].Set)
					pDIO->timer[i].OTE = 1;
			}
		}

		//EM STOP : Negative Edge
		if(!pDIO->LCRLimitDisable)
			if(pDIO->readDI_prev(IN_EM_STOP, CARD_IO) && !pDIO->readDI(IN_EM_STOP, CARD_IO)){
				G_MainWnd->m_InspectThread.EMStop();
			}

			//Edge detect
			memcpy(pDIO->DI_prev, pDIO->DI, sizeof(int)*IO_NUM);

			//general DIO 
			pDIO->timerCheckDIO();

#ifdef VIRTUAL_RUN
			pDIO->virtualDIO();
#endif
	
		Sleep(10);
	}

return ret;
}
