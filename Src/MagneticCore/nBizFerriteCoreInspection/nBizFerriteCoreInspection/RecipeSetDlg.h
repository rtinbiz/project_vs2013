#pragma once
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "1.Common\Grid64\NewCellTypes\gridcellcheck.h"
#include "afxwin.h"
#include "1.Common/IniClass/BIniFile.h"
#include "PathDefine.h"
#include "1.Common/LogMng/LogMng.h"
#include "1.Common\RoundButton\RoundButton2.h"


typedef struct _PART_TEST_INFO
{
	wchar_t part_model[256];
	wchar_t part_type[256];
	wchar_t lot[256];

	_PART_TEST_INFO(){	memset(this,0x00, sizeof(PART_TEST_INFO));}

}PART_TEST_INFO;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

class CRecipeSetDlg : public CDialogEx
{

	DECLARE_DYNAMIC(CRecipeSetDlg)

public:
	CRecipeSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRecipeSetDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_RECIPE_SETTING };

	PART_TEST_INFO part_Test;

//////////////////////////////////////////////////////////////////////////////////////////////////

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick2(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick2(NMHDR *pNotifyStruct, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()

public:
	CString recipeSelected;
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	int initGridModel(void);
	CGridCtrl m_GridModel;
	CGridCtrl m_GridModelRecipe;
	int fillGridModel(void);

	afx_msg void OnBnClickedBtnApply();
	int applyRecipeInProduction(CString recipe);

	CBIniFile		m_iniRecipe;

	BOOL m_fnWriteIniFile(LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString);
	CString m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default); 
	afx_msg void OnBnClickedBtnRecipeDelete();
	afx_msg void OnBnClickedBtnRecipeNew();
	afx_msg void OnBnClickedBtnRecipeSave();

	afx_msg void OnBnClickedButton2();
	CRoundButton2 m_btnRecipeDelete;
	CRoundButton2 m_btnRecipeNew;
	CRoundButton2 m_btnRecipeSave;
	CRoundButton2 m_btnRecipeApply;
	CRoundButton2 m_btnResetAccu;
	CRoundButton2 m_btnResetProductionCount;
	afx_msg void OnBnClickedButton3();
	CRoundButton2 m_btnSetLotTarget;
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton4();
	CRoundButton2 m_btnSetLotNo;
	CRoundButton2 m_btnDefectClear;
	afx_msg void OnBnClickedButton8();
	int saveRecipe(CString recipe);
	int clearGridModelRecipe(void);
	int initGridModelRecipe(void);
	int fillGridModelRecipe(CString recipe);
};

