﻿#pragma once

#include "CommThread.h"
#include "stdAfx.h"
#include "DataHandling.h"

#define MODE_READ							0    //CONNECTION CHECK...
//#define MODE_INIT						1
#define CH_1									1
#define CH_2									2
#define CH_3									3
#define CH_4									4

#define LIGHT_CAM1_1						0		
#define LIGHT_CAM1_2						1
#define LIGHT_CAM2_1						2
#define LIGHT_CAM2_2						3
#define LIGHT_CAM3_1						4
#define LIGHT_CAM3_2						5
#define LIGHT_CAM4_1						6
#define LIGHT_CAM4_2						7
#define LIGHT_ALL_COUNT						8

#define COMM_PORT_COUNT			1+2		//Light Control



//*****LIGHT_CONTROL_DEFINE*****//
////////////////////////////////////////////////////////////////////////////////////
// PORT0: CH1 -> PCHI(#1CAM),	             CH2 -> PCCA2(#3CAM)
// PORT1: CH1 -> PCCA(#2CAM_UPPER),	 CH2 -> PCCA(#2CAM_LOWER)
// PORT2: CH1 -> PCHI2(#4CAM_UPPER),	 CH2 -> PCHI2(#4CAM_LOWER)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********THIS AREA SETTING AREA***********
//PORT
#define LIGHT_CAM1_1_PORT				0
#define LIGHT_CAM1_2_PORT				0  //NOT USE
#define LIGHT_CAM2_1_PORT				1
#define LIGHT_CAM2_2_PORT				1
#define LIGHT_CAM3_1_PORT				0
#define LIGHT_CAM3_2_PORT				0 //NOT USE
#define LIGHT_CAM4_1_PORT				2 //NOT USE
#define LIGHT_CAM4_2_PORT				2 //NOT USE
//CH
#define LIGHT_CAM1_1_CH				1
#define LIGHT_CAM1_2_CH				2  //NOT USE
#define LIGHT_CAM2_UPPER_CH			1	
#define LIGHT_CAM2_LOWER_CH			2
#define LIGHT_CAM3_1_CH				2	
#define LIGHT_CAM3_2_CH				1 //NOT USE	  
#define LIGHT_CAM4_UPPER_CH			1 //NOT USE
#define LIGHT_CAM4_LOWER_CH			2 //NOT USE
//NOT USE LIGHT
static const int g_IsLIGHT_NOT_USE[] =
{
	LIGHT_CAM1_2,
	LIGHT_CAM3_2,
	//LIGHT_CAM4_1,
	//LIGHT_CAM4_2,
};
//********THIS AREA SETTING AREA***********
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static const int g_IsLIGHT_CAM_PARAM[] =
{
	RP_CAM1_LIGHT_SOURCE_CH1,
	RP_CAM1_LIGHT_SOURCE_CH2,
	RP_CAM2_LIGHT_SOURCE_CH1,
	RP_CAM2_LIGHT_SOURCE_CH2,
	RP_CAM3_LIGHT_SOURCE_CH1,
	RP_CAM3_LIGHT_SOURCE_CH2,
	RP_CAM4_LIGHT_SOURCE_CH1,
	RP_CAM4_LIGHT_SOURCE_CH2,
};
static const int g_IsLIGHT_CAM_PORT[] =
{
	LIGHT_CAM1_1_PORT,
	LIGHT_CAM1_2_PORT,
	LIGHT_CAM2_1_PORT,
	LIGHT_CAM2_2_PORT,
	LIGHT_CAM3_1_PORT,
	LIGHT_CAM3_2_PORT,
	LIGHT_CAM4_1_PORT,
	LIGHT_CAM4_2_PORT,
};
static const int g_IsLIGHT_CAM_CHANNEL[] =
{
	LIGHT_CAM1_1_CH,
	LIGHT_CAM1_2_CH,
	LIGHT_CAM2_UPPER_CH,
	LIGHT_CAM2_LOWER_CH,
	LIGHT_CAM3_1_CH,
	LIGHT_CAM3_2_CH,
	LIGHT_CAM4_UPPER_CH,
	LIGHT_CAM4_LOWER_CH,
};
enum
{
	TIMER_LIGHT_READ = WM_USER+1,
};

// CameraView On, Off
class CSerialInterface : public CWnd
{
	DECLARE_DYNAMIC(CSerialInterface)

public:
	CSerialInterface();
	~CSerialInterface(void);	

	int m_nComportIndex[COMM_PORT_COUNT];
	CCommThread m_CommThread[COMM_PORT_COUNT];

	BOOL m_fnSendToComportLight(int port, int nMode, int nValue = 0);
//	BOOL m_fnSendToComportLight2(int port, int nValueCH1 = 0, int nValueCH2 = 0);

public:		
	LRESULT OnCommRead(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

public:
	void m_fnInit();
	CRITICAL_SECTION csComm;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int processCommData(int port);
	int processLightComm(int port);
	int m_fnDeInit(void);
	int processComm1(void);
	int processComm2(void);
	int processComm3(void);
	int processComm4(void);
	int processComm5(void);
	int processComm6(void);
	int processComm7(void);

protected:
	//static const int m_IsLIGHT_NOT_USE[];
	//static const int m_IsLIGHT_CAM_PORT[LIGHT_ALL_COUNT];
	//static const int m_IsLIGHT_CAM_CHANNEL[LIGHT_ALL_COUNT];
	//static const int m_IsLIGHT_CAM_PARAM[LIGHT_ALL_COUNT];

public:
	void LIGHTONOFF(int idx, int sw);
//	void LIGHTONOFF2(int idx, int sw);
	//int swLight[LIGHT_ALL_COUNT];
};

