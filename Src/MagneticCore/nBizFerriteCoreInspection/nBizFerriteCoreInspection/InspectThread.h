#pragma once


#include "DataHandling.h"
#include "SerialInterface.h"
#include "nBizFerriteCoreInspectionDlg.h"
#include "RecipeSetDlg.h"

#include "PIO.h"
#include "AxisMotion.h"
#include "VisionInspection.h"

//#define DEBUG_PRINT_TIME

#define WM_TIMER_100m	WM_USER + 210
#define WM_TIMER_1S		WM_TIMER_100m + 1

#define CHECK_HOLD		1
#define CHECK_SUSPEND	2
#define CHECK_NG			3

////////////////////////////////////////////////////////////////////////////////////////////////

#define INPUT						0 //INPUT
#define IN_CENTERING			1 //CENTERING
#define IN_VI0						2//CAM1 PCHI INNER	
#define IN_VI1						3 //CAM2 PCCA UPPER
#define IN_BUFFER				4 //
#define OUT_FLIPDROP			5 //FLIPDROP
#define OUT_STOPPER			6 //STOPPER
#define OUT_CENTERING		7 //CENTERING
#define OUT_VI2					8 //CAM2
#define OUT_VI3					9	//
#define OUT_NGOUT				10 //NGOUT
#define IN_CNVYR				11 //IN CONVEYOR
#define OUT_CNVYR				12 //OUT CONVEYOR
//160307_KYS_ADD_IN_VI0, OUT_VI3
//#define IN_VI0					11 //CAM0
//#define OUT_VI3					12 //CAM3


#define NO_STAGE				9+2  // FOR part data shift
#define TOTAL_STAGE			11+2		//+ VI ADD

//160222_kys_1_add
//#define TOTAL_BUFFER_SIZE		19		//+ CONVEYOR
#define TOTAL_BUFFER_SIZE		20		//+ CONVEYOR


#define CNVYR_INPUT				0x0001
#define CNVYR_IN_CTR				0x0002
#define CNVYR_IN_VI0				0x0004
#define CNVYR_IN_VI1				0x0008
#define CNVYR_IN_BUFFER			0x0010
#define CNVYR_OUT_FLIP			0x0100
#define CNVYR_OUT_STOP			0x0200
#define CNVYR_OUT_CTR			0x0400
#define CNVYR_OUT_VI2			0x0800
#define CNVYR_OUT_VI3			0x1000
#define CNVYR_OUT_NGOUT		0x2000
#define CNVYR_OUT_BUFFER		0x4000
//160307_KYS_ADD_IN_VI0, OUT_VI3
//#define CNVYR_IN_VI0				0x0400
//#define CNVYR_OUT_VI3			0x0800
//
#define CNVYR_IN_ALL				0x001F
#define CNVYR_OUT_ALL			0x3F00


//BATCH TIME OUT
#define T_WAIT						2
#define T_WAIT_2X					T_WAIT*2
#define T_WAIT_3X					T_WAIT*3
#define T_WAIT_5X					T_WAIT*5
#define T_WAIT_10X				T_WAIT*10
#define T_WAIT_1SEC				100
#define T_WAIT_2SEC				200
#define T_WAIT_5SEC				500
#define T_WAIT_TIMEOUT			1000
#define T_WAIT_TIMEOUT_2X	T_WAIT_TIMEOUT*2

#define T_WAIT_LOAD_OPEN		5 //ADJUST
#define T_WAIT_IN_ARRIVE_GRIP	1000 //10 SEC

////////////////////////////////////////////////////////////////////////////////////////////////

enum
{
	STEP0, //READY (WAIT TO BE TRIGGERED)
	STEP1, // START (CHECK IF ALRIGHT TO START)
	STEP2, // STEP 1
	STEP3, // STEP 2
	STEP4, //
	STEP5, // 
	STEP6, // TEST
	STEP7,
	STEP8,
	STEP9,
	STEP10,
	STEP11,
	STEP12,
	STEP13, // 

	STEP14, // 
	STEP15, // BATCH END AND FINISH
	STEP16, // AUTO END , 
	STEP17, // SHUTTLE REQUEST AND WAIT

	STEP18,
	STEP19,  //ERROR STATE
	STEP20,   //ERROR RESET

	STEP21,
	STEP22,	 //HOLD

	STEP23, //
	STEP24, //
	STEP25, //
	STEP26, //
	STEP27, //
	STEP28, //
	STEP29, // 

	STEP30, //
	STEP31, // 
	STEP32, // 
	STEP33, // 
	STEP34, //
	STEP35, // 
	STEP36, // 
	STEP37, //
	STEP38, //
	STEP39, //

};

const int ST_IN_CNVYR_INTERLOCK[] =
{
	CNVYR_INPUT,
	CNVYR_IN_CTR,
	CNVYR_IN_VI0,
	CNVYR_IN_VI1,
	CNVYR_IN_BUFFER,
	CNVYR_OUT_FLIP,
	CNVYR_OUT_STOP,
	CNVYR_OUT_CTR,
	CNVYR_OUT_VI2,
	CNVYR_OUT_VI3,
	CNVYR_OUT_NGOUT,
	0,0, //Conveyor
	CNVYR_OUT_BUFFER,
	//20160315_KYS_ADD
};


//Batch Status 
#define CLEAR				0
#define FINISHED			1
#define CONTINUE		2
#define PART				4	//PART ���� 
#define RUN				8	//SHUTTLE interlock
#define VI2_1				0x10 
#define VI2_2				0x20
#define ALIGNED			0x40

#define BATCH_RETRY	3

#define IN_CNVYR_TURN		0
#define OUT_CNVYR_TURN		1

#define STAGE_THREAD_COUNT 9
#define STAGE_THREAD_INPUT 0 
#define STAGE_THREAD_INPUT_BUFFER 1
#define STAGE_THREAD_FLIP_DROP 2
#define STAGE_THREAD_OUT_CENTERING 3 
#define STAGE_THREAD_CONVEYER 4
#define STAGE_THREAD_CAM_0 5
#define STAGE_THREAD_CAM_1 6 
#define STAGE_THREAD_CAM_2 7 
#define STAGE_THREAD_CAM_3 8 

class CInspectThread : public CWnd //: public CSeqTimeMng
{
public:
	CInspectThread(void);
	~CInspectThread(void);

	typedef struct STG_BATCH
	{
		int status;
		int step;

		int data[5];
		double dbdata[5];

		//SHUTTLE
		int		allow;
		int		request;
		int		interlockshuttle;  //if set, other batch shouldn't move
		int		holdShuttle;	//
		int		myturn;	//
		int		partID, partID2;

		TIMER timer;
		COUNTER counter;
		int retry;

		STG_BATCH(){	memset(this,0x00, sizeof(STG_BATCH));}
	};

	STG_BATCH stStgBatch[TOTAL_STAGE];

	/////////////////////////////////////////////////////////////////////////////////////////
	//PART INSPECTION DATA
	typedef struct PART_DATA
	{
		//int model_no;
		//int part_no;

		int part; //4: PART, 0: no part
		long id;

		int idxVIResult[MAX_VI_TEST]; //
		int results[MAX_VI_TEST]; //
		//vector<VI_DEFECT> vecDefects[MAX_VI_TEST];

		int finalPass; //1: Pass

		PART_DATA(){	memset(this,0x00, sizeof(PART_DATA));}
		void Reset(){memset(this,0x00, sizeof(PART_DATA));}
		void CopyFrom(PART_DATA *from){memcpy(this, (void *)from, sizeof(PART_DATA));}
	}; 

	PART_DATA m_PartData[TOTAL_STAGE] ;
	vector<VI_DEFECT> m_PartDataVecDefects[TOTAL_STAGE][MAX_VI_TEST];

	PART_DATA m_PartDataBuffer[TOTAL_BUFFER_SIZE];
	vector<VI_DEFECT> m_PartDataBufferVecDefects[TOTAL_BUFFER_SIZE][MAX_VI_TEST];

	long PartID;

	AxisMotion				m_AxisDlg;
	CVisionInspection		m_vi;

	int m_fnInit();
	int m_fnInit(CDataHandling				* pDataHandling, 
					CSerialInterface			*pSerialInterface, 
					CRecipeSetDlg				*pRecipeSetDlg,

					PIO							*pPIODlg
					//AxisMotion					*pAxisDlg
					);
	int m_fnClearPartData();
	int m_fnBeginInspectThread();
	void m_fnDeInit();

	int			m_nSeqMode;	// loading or unloading
	int			m_nScanCount;
	int			m_nuTotalScanCount;
	
	//batch step
	void			m_fnClearBatchStep(int batch);
	void			m_fnSetBatchStep(int nbatch, int nStep);
	int				m_fnGetBatchStep(int batch);
	int				m_fnGetPrevBatchStep(int batch);
	void			m_fnSetPrevBatchStep(int batch, int nStep);
	void			m_fnSetOldBatchStep(int batch, int nOldStep);
	int				m_fnGetOldBatchStep(int batch);
	BOOL			m_fnCheckBatchStep(int batch);
	int				m_fnRetryBatchStep(int batch);

	int				m_fnAllowConveyor(int cnvyr, bool allow, int batch);
	int				m_fnRequestConveyor(int cnvyr, bool request, int batch);

	
protected:

	//DECLARE_MESSAGE_MAP()

	CDataHandling			*m_pDataHandling;
	CSerialInterface		*m_pSerialInterface;
	CRecipeSetDlg			*m_pRecipeSetDlg;	


	//AxisMotion				*m_pAxis;

	int			m_nAutoMoveCount;
	int			m_nAutoInFocusCount;
	int			m_AFRetryCount;		
	
	void		m_fnCheck();

	void		m_fnCheckUnLoading();
	void		m_fnScanCountPlus();

	int				m_nCurStep[TOTAL_STAGE];
	int				m_nPrevStep[TOTAL_STAGE];
	int				m_nOldStep[TOTAL_STAGE];


	//batch run time
	ULONGLONG tickTack[TOTAL_STAGE];

	CCriticalSection m_csCnvyrAllow[2]; //input, output conveyor
	CCriticalSection m_csCnvyrRequest[2]; //input, output conveyor

public:
	int		checkStatus; //true: suspend
	int		suspendCount; //thread 

	CCriticalSection m_csCheck;

	int m_fnClearPartData(int batch);
	int m_fnShiftPartDataInConveyor();
	int m_fnShiftPartDataOutConveyor();
	int m_fnShiftPartDataTest();

	int m_fnClearPartDataAll(void);
	int m_fnInitPartData(int batch, int model_no, int part_no);

	//Conveyor Transfer
	int stg0Input(void);
	int stg1Center(void);
	int stg2VI1(void);
	int stg3Buffer(void);
	int stg4Flip(void);
	int stg5Stopper(void);
	int stg6Center(void);
	int stg7VI2(void);
	int stg8NGout(void);
	int stg9InConveyorRun(void);
	int stg10OutConveyorRun(void);
	//20160307_ADD
	int stg11VI0(void);
	int stg12VI3(void);
	//
	int EMStop(void);
	int holdAutoRun(void);
	int resumeAutoRun(void);
	int holdBatch(int batch);
	int resetBatch(int batch);
	int continueBatch(int batch);
	int batchTimer(void);
	void turnLight(int idx, int sw);
	int summarizeResult(void);
	int setBatchStatus(int batch, int status);
	int getBatchStatus(int batch);

	int restoreInspectionStatus(void);
	int errorRoutine(void);
	int errorResetRoutine(void);
	void m_fnSetInspectionStep(int batch, int nStep);

	CTime ctUnloadTime;

	DWORD tickPrev;

	int showNGResult(void);

	//Thread
	int m_fnBeginThread(void);
	//void m_fnThreadStg0(LPVOID lParam);
	CWinThread* m_pThreads[STAGE_THREAD_COUNT];
	volatile bool isThreadRunning[STAGE_THREAD_COUNT];
	int		suspendCnt[STAGE_THREAD_COUNT]; //thread 

	PIO						*m_pPIO;

	int m_fnEndThread();
	int m_fnClearParts();

#ifdef DEBUG_PRINT_TIME
	void DebugPrint(const wchar_t* szDebugString, ...);
#endif
};





