
// nBizFerriteCoreInspectionDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "resource.h"
#include "1.Common\grid64\gridctrl.h"
#include "1.Common\roundbutton\roundbutton2.h"
#include "1.Common\roundbutton\roundbuttonstyle.h"
#include "1.Common\Led\LedButton.h"
#include "1.Common\ColorStatic\ColorStatic2.h"
#include "1.Common\roundbutton\roundbutton2.h"
#include "1.Common\led\ledbutton.h"


#define UM_UI_UPDATE  WM_USER + 1
#define UW_CLEAR					0
#define UW_RESULT_CAM			1
#define UM_TIME_UPDATE			2
#define UM_INIT_UPDATE			3
#define UM_STATUS_UPDATE		4
#define UM_TEST_UPDATE			5

#define UM_INSPECT_ERROR_UPDATE		6
#define UM_INSPECT_LOG_UPDATE			7

#define UM_AXIS_ERROR_UPDATE		8
#define UM_AXIS_LOG_UPDATE			9

#define UM_SHIFT_UPDATE			10 //TEST RESULT SHIFT UPDTE

#define TIMER_UI_UPDATE			WM_USER+100
#define TIMER_CONNECT_CHECK		WM_USER+101

#define MODE_OPERATOR				0
#define MODE_SUPERVISOR			1

#define TIMER_IMAGE_DIR				11
#define TIMER_AUTO_DIR_DELETE	12

enum
{
	TIME_CHECK_NOW=0,
	TIME_CHECK_EQP_ALIVE,	
};

// CnBizFerriteCoreInspectionDlg 대화 상자
class CnBizFerriteCoreInspectionDlg : public CDialogEx
{
// 생성입니다.
public:
	CnBizFerriteCoreInspectionDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
	enum { IDD = IDD_nBizFerriteCoreInspection_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

	int m_nReciveAliveTime;
	int m_nNowTime;

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
    
	int nDayReportCount;

	CListBox m_listLogView;

	IplImage * pImgBuf[4];
	int paintImageReady[4];


	void m_fnInit();
	void m_fnDeInit();
	void m_fnAddLog(CString strLog);
	void m_fnUpdateListofLog(TCHAR* szLog);
	void m_fnTimeCheck(int nTimeIndex);
	void FillBitmapInfo(BITMAPINFO* bmi, int width, int height, int bpp, int origin);
	void DisplayImage(IplImage* srcimg, int item);
	void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect,int CamNo);

	void m_fnAddLog2(CString strLog);
	void m_fnUpdateListofLog2(TCHAR* szLog);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnClose();

	CStatic m_pcMainView;
	CGridCtrl m_ctrlGridResult;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

protected:

	int hourDB;
	int deleteSqc;

	afx_msg LRESULT OnUiUpdateCmd(WPARAM wParam, LPARAM lParam);

public:

	int m_opMode; //operator/engineer

	CRoundButton2 m_btnAuto;
	CRoundButton2 m_btnManual;
	afx_msg void OnBnClickedBtnAuto();
	afx_msg void OnBnClickedBtnManu();

	CColorStatic2 m_OperatorMode;
	CRoundButton2 m_btnLogin;
	afx_msg void OnBnClickedBtnMode();
	afx_msg void OnMenuSystem();
	afx_msg void OnMenuModel();

	afx_msg void OnMenuAxis();
	afx_msg void OnMenuIo();
	afx_msg void OnHelpAbout();
	afx_msg void OnFileExit();
	CColorStatic2 m_static1;
	CColorStatic2 m_static2;
	CColorStatic2 m_static3;
	CColorStatic2 m_static4;
	CColorStatic2 m_static5;
	CColorStatic2 m_staticModel;
	CColorStatic2 m_staticType;
	CColorStatic2 m_staticAccu;
	CColorStatic2 m_staticLOT;
	CColorStatic2 m_staticTarget;

	CColorStatic2 m_staticTargetPercent;
	CColorStatic2 m_static7;
	CColorStatic2 m_static8;
	CColorStatic2 m_static19;
	CColorStatic2 m_staticTotal;
	CColorStatic2 m_staticOKNG;
	CColorStatic2 m_staticNGRatio;
	CColorStatic2 m_ErrorMsg;

	CRoundButton2 m_btnMode;
	CRoundButton2 m_btnStop;

	CColorStatic2 m_static11;
	CColorStatic2 m_static12;
	CColorStatic2 m_staticTimeMasterStart;
	CColorStatic2 m_staticTimeMasterElapsec;
	CColorStatic2 m_staticThreadFlag;
	CRoundButton2 m_btnStart;
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
	CRoundButton2 m_btnHome;
	afx_msg void OnBnClickedBtnErrorReset();
	int OnSystemInit(void);

	CColorStatic2 m_staticOK;
	CColorStatic2 m_staticNG;


	afx_msg void OnBnClickedBtnHoldSHUTTLE();
	CRoundButton2 m_btnHoldSHUTTLE;
	afx_msg void OnBnClickedBtnBuzzstop();
	CRoundButton2 m_btnBuzzStop;
	CLedButton m_masterTest_OK;
	CLedButton m_masterTest_NG;
	CRoundButton2 m_masterTest_OK_Start;
	CRoundButton2 m_masterTest_NG_Start;
	afx_msg void OnBnClickedBtnMasterOk2();
	afx_msg void OnBnClickedBtnMasterNg2();
	int nMasterOK;//flag Master Test OK
	int nMasterNG;

	CTime ctimeAutoRun;
	afx_msg void OnRawInput(UINT nInputcode, HRAWINPUT hRawInput);

	void StartEventCapture();
	void EndEventCapture();
	void clearNGResult();
	void testResultUIUpdate(void);
	void masterResultUIUpdate(void);

	afx_msg void OnEnvironmentVisioninspection();
	CColorStatic2 m_staticInsp;
	CColorStatic2 m_static6;
	CColorStatic2 m_static16;
	CColorStatic2 m_static17;
	CColorStatic2 m_static18;
	CColorStatic2 m_staticDefects[4];
	int fillNGResultLIst();
	CRoundButton2 m_btnCamStatus[4];
	afx_msg void OnBnClickedBtnCam(UINT nID);
	CRoundButton2 m_btnOutCnvyrHold;
	CListBox m_listDefectView;
	afx_msg void OnBnClickedBtnHoldShuttle2();
	CStatic m_pcLogo;
	CRoundButton2 m_btnClear;
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnCheckedCrossLine();
	bool m_bDrawCrossLine;
};
