#include "stdafx.h"
#include "DataHandling.h"
#include "MainWnd.h"

#pragma comment(lib,"sqlite\\sqlite3.lib")

extern CMainWnd* G_MainWnd;

CDataHandling::CDataHandling(void)
{	
	memset(m_chImageSavePath, 0x00, MAX_PATH);
	memset( m_chImageSaveNGPath, 0x00, MAX_PATH);
}

CDataHandling::~CDataHandling(void)
{
}

void CDataHandling::m_fnInit()
{
	CString str;
	m_fnMakeDirectory(SYSTEM_FOLDER);

	m_iniSystemParam.SetFileName(SYSTEM_PARAM_INI_PATH);
	m_iniRecipe.SetFileName(RECIPE_FOLDER);

 	m_LogMng.m_fnSetInitInfo(SYSTEM_LOG_FOLDER,L"ProcessLog");
	m_DayReportMng.m_fnSetInitInfo(RESULT_DATA_FOLDER,L"DayReportLog");
 	m_AlarmMng.m_fnSetInitInfo(SYSTEM_ALARM_LOG_FOLDER,L"AlarmLog");

	m_fnSystemParamLoad();
	loadFlashData();

	bAutoDeleteState = false;
	//////////////////////////////////////////////     
    
	m_fnMakeDirectoryNewDay();

	str.Format(L"%s\\%s", m_chImageSavePath, m_SystemParam.ProductionModel);
	G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

	str.Format(L"%s\\%s", m_chImageSaveNGPath, m_SystemParam.ProductionModel);
	G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

	sqliteInit();
}

void CDataHandling::m_fnWriteIniFile(UINT nIndex, LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString) 
{
	BOOL bReturn = FALSE;

	
	if(nIndex == SYSTEM_PARAM)
	{
		bReturn = m_iniSystemParam.WriteProfileString(lpszSection, lpszKeyName, lpString);
	}
	else if(nIndex == RECIPE)
	{
		bReturn = m_iniRecipe.WriteProfileString(lpszSection, lpszKeyName, lpString);
	}
	

	if(bReturn == FALSE)
	{
		CString strMessage;
		
		if(nIndex == SYSTEM_PARAM)
			strMessage = "SYSTEM_PARAM";
		else if(nIndex == RECIPE)
			strMessage = "RECIPE";		

		G_AddLog(3,L"%s 파일 기록 Error : 섹션%s, 키네임%s, 스트링%s", strMessage, lpszSection, lpszKeyName, lpString);

	}	
}

CString CDataHandling::m_fnReadIniFile(UINT nIndex, LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR defalut) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"0";
	if(defalut != 0)
	{
		lpDefault = defalut;
	}


	if(nIndex == SYSTEM_PARAM)
	{
		bReturn = m_iniSystemParam.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	}
	else if(nIndex == RECIPE)
	{
		bReturn = m_iniRecipe.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	}

	if(bReturn == FALSE)
	{
		CString strMessage;
		if(nIndex == SYSTEM_PARAM)
			strMessage = "SYSTEM_PARAM";
		else if(nIndex == RECIPE)
			strMessage = "RECIPE";		
		G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}


void CDataHandling::m_fnCvPutImage(IplImage* psrcImage, IplImage* pdstImage, int nX, int nY)
{
	int nHeight, nWidth; 
	unsigned char chValue = 0;

	for(nHeight=0; nHeight<pdstImage->height; nHeight++)
	{
		for(nWidth=0; nWidth<pdstImage->widthStep * 3; nWidth = nWidth + pdstImage->nChannels * 3) // 상하 반전
		{
			chValue = pdstImage->imageData[nHeight * pdstImage->widthStep + nWidth/3];

			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 0] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 1] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 2] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
		}
	}  	
}

void CDataHandling::m_fnSystemParamLoad()
{	
	CString strValueData; 	
	
	//strParam
 	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_PASSWORD);	
	_tcscpy_s(m_SystemParam.sPassword, (LPCTSTR)strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SELECTED_RECIPE);
	_tcscpy_s(m_SystemParam.ProductionModel, (LPCTSTR)strValueData); //Part No. 

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_CAM_0_SERIAL);
	_tcscpy_s(m_SystemParam.CamSerialNo[0] , (LPCTSTR)strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_CAM_1_SERIAL);
	_tcscpy_s(m_SystemParam.CamSerialNo[1], (LPCTSTR)strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_CAM_2_SERIAL);
	_tcscpy_s(m_SystemParam.CamSerialNo[2], (LPCTSTR)strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_LINE_ID);
	_tcscpy_s(m_SystemParam.LINE_ID, (LPCTSTR)strValueData);
	
	//fParam
	for (int i = 0; i <= SYS_PARAM_FLOAT; i++)
	{ 		
		strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_TABLE_SYSTEM_0_COL[ SYS_PARAM_STRING + i ]);
		m_SystemParam.fParam[i] = _wtof(strValueData);
	}

	//iParam
	for (int i = 0; i <= SYS_PARAM_INT; i++)
	{ 		
		strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_TABLE_SYSTEM_0_COL[SYS_PARAM_STRING + SYS_PARAM_FLOAT + i]);
		m_SystemParam.iParam[i] = _wtoi(strValueData);
	}

} 
// 
void CDataHandling::m_fnFileWriteRecipeID(char *chRecipeID)
{
	CString strValueData; //OLD Data를 저장하고 NOW Data를 Write한다
	if(chRecipeID != NULL)
	{
		//strValueData = m_fnReadIniFile(RECIPE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW);
		//m_fnWriteIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_OLD, strValueData);

		//m_fnWriteIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW, chRecipeID);
	}
}
// 
CString CDataHandling::m_fnFileReadRecipeID(BOOL bFullPath)
{
 	CString strValueData;
// 	strValueData = m_fnReadIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW);
// 	CString strFullPath;
// 	strFullPath.Format(_T("%s%s.ini", RECIPE_FOLDER, strValueData);
// 	if(bFullPath)
// 		return strFullPath;


	return strValueData;
}

BOOL CDataHandling::m_fnMakeDirectory(CString strPathName)
{
	CString		strTemp, strMakePath;
	int			nTemp, nPos, nLen;
	wchar_t		pBuff[MAX_PATH];
	BOOL		fTemp;

	strPathName.TrimLeft(' ');
	strPathName.TrimRight(' ');

	strPathName.TrimLeft('\\');
	strPathName.TrimRight('\\');

	nPos=m_fnInPos(strPathName,'\\');

	if(nPos<0)
	{
		GetCurrentDirectory(MAX_PATH,pBuff);
		strMakePath.Format(pBuff);
		strMakePath=strMakePath + _T("\\") + strPathName;
		fTemp=CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
		strMakePath.ReleaseBuffer();

		if(fTemp)
			return TRUE;
		else
		{
			nTemp=GetLastError();

			if(nTemp == ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
				return TRUE;
			else
				return FALSE;
		}
	}

	do
	{
		nLen=strPathName.GetLength();
		if(strMakePath.GetLength()==0)
			strMakePath=strPathName.Left(nPos);
		else
			strMakePath=strMakePath + _T("\\") + strPathName.Left(nPos);
		strPathName=strPathName.Right(nLen-nPos-1);

		fTemp = CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
		strMakePath.ReleaseBuffer();

		if(!fTemp)
		{
			nTemp = GetLastError();

			if(nTemp != ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
				return FALSE;
		}

		nPos = m_fnInPos(strPathName,'\\');
		if(nPos<0)
		{
			strMakePath=strMakePath + _T("\\") + strPathName;
			fTemp=CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
			strMakePath.ReleaseBuffer();

			if(fTemp)
				return TRUE;
			else
			{
				nTemp = GetLastError();
				if(nTemp == ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
					return TRUE;
				else
					return FALSE;
			}
		}
	} while(nPos>=0);

	return TRUE;
}

int CDataHandling::m_fnInPos(CString strBase, char cChar, int nStartPos)
{
	int		nLen, i;

	if(nStartPos<0)
		return -1;

	nLen=strBase.GetLength();

	if(nStartPos>(nLen-1))
		return -1;

	for(i=nStartPos;i<nLen;i++)
	{
		if(strBase.GetAt(i)==cChar)
			return i;
	}

	return -1;
}

void CDataHandling::m_fnInspectDataReset()
{
	//BOOL bRes = FALSE;
	/////bRes = G_MainWnd->m_BaslerLiveView.m_fnThreadSuspend();	
	//while(1)
	//{
	//	Sleep(10);
	//	if(G_MainWnd->m_BaslerLiveView.m_nThreadStop == TRUE)   // Thread 멈춤 확인
	//	{				
	//		break;
	//	}
	//}
	//G_MainWnd->m_BaslerLiveView.m_fnThreadResume();
}


int CDataHandling::systemRestore(void)
{
	return 0;
}

void CDataHandling::m_fnDayReportWrite(bool bResult)
{
	CStdioFile file;
	CFileException ex;
	CString strFileName, strData,strHeader;
	CString strTempDay, strTempTime;
	CTime ctCurrentTime;
	CFileFind finder;
	int nDayCount = 0;

	int stage;

	stage = OUT_NGOUT;

	ctCurrentTime = CTime::GetCurrentTime();
	strTempDay = ctCurrentTime.Format(_T("%Y%m%d"));
	strTempTime = ctCurrentTime.Format(_T("%Y/%m/%d %H:%M:%S"));

	strFileName.Format(_T("%s%s_%s.csv"), RESULT_DATA_FOLDER, strTempDay,G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	CString strTest;
	BOOL bWorking = finder.FindFile(strFileName);

	TCHAR sz[256];
	if(!bWorking)
	{
		if(file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite, &ex)){
			G_MainWnd->m_ServerDlg.nDayReportCount = 0;
			//header 
#ifdef _FI
			strHeader.Format(L"MODEL:,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n", G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
			file.WriteString(strHeader);
			file.WriteString(L",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
			file.WriteString(L",TIME,MODEL,LOT NO.,SERIAL NO.,LCR(uH),,,,,,,,,,,,,OSC,DCR(mOhm),,,,,,HIPOT(mA),,,,,IR(MOhm),,,,,VI,RESULT,NG ITEM\n");
			file.WriteString(L",,,,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,1,2,3,4,5,6,1,2,3,4,5,1,2,3,4,5,1,,\n");
#else
			strHeader.Format(L"MODEL:,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n", G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
			file.WriteString(strHeader);
			file.WriteString(L",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
			file.WriteString(L",TIME,MODEL,LCR(uH),,,,,,,,,,,,,DCR(mOhm),,,,,,RESULT,NG ITEM\n");
			file.WriteString(L",,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,,\n");
#endif
			//test result
			file.WriteString(strData);
			G_MainWnd->m_ServerDlg.nDayReportCount++;
			file.SeekToEnd(); 
			file.Close(); 
		}else{
			ex.GetErrorMessage(sz, 256);
			G_AddLog(3,_T("%s %d %s"), __FILE__, __LINE__, sz);
		}

	}
	else
	{
		if(G_MainWnd->m_ServerDlg.nDayReportCount == 0)
		{
			//m_fnDayReportFileRead();
		}
		G_MainWnd->m_ServerDlg.nDayReportCount++;
		if(file.Open(strFileName, CFile::modeReadWrite, &ex)){
			file.SeekToEnd();
			file.WriteString(strData);
			file.Close(); 
		}else{
			ex.GetErrorMessage(sz, 256);
			G_AddLog(3,L"%s %d %s", __FILE__, __LINE__, sz);
		}
	}

}


int DeleteDirectory(const std::wstring &refcstrRootDirectory,
	bool              bDeleteSubdirectories = true)
{
	bool            bSubdirectory = false;       // Flag, indicating whether
	// subdirectories have been found
	HANDLE          hFile;                       // Handle to directory
	std::wstring     strFilePath;                 // Filepath
	std::wstring     strPattern;                  // Pattern
	WIN32_FIND_DATA FileInformation;             // File information

	strPattern = refcstrRootDirectory + _T("\\*.*");
	hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(FileInformation.cFileName[0] != '.')
			{
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + _T("\\") + FileInformation.cFileName;

				if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(bDeleteSubdirectories)
					{
						// Delete subdirectory
						int iRC = DeleteDirectory(strFilePath, bDeleteSubdirectories);
						if(iRC)
							return iRC;
					}
					else
						bSubdirectory = true;
				}
				else
				{
					// Set file attributes
					if(::SetFileAttributes(strFilePath.c_str(),
						FILE_ATTRIBUTE_NORMAL) == FALSE)
						return ::GetLastError();

					// Delete file
					if(::DeleteFile(strFilePath.c_str()) == FALSE)
						return ::GetLastError();
				}
			}
		} while(::FindNextFile(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if(dwError != ERROR_NO_MORE_FILES)
			return dwError;
		else
		{
			if(!bSubdirectory)
			{
				// Set directory attributes
				if(::SetFileAttributes(refcstrRootDirectory.c_str(),
					FILE_ATTRIBUTE_NORMAL) == FALSE)
					return ::GetLastError();

				// Delete directory
				if(::RemoveDirectory(refcstrRootDirectory.c_str()) == FALSE)
					return ::GetLastError();
			}
		}
	}

	return 0;
}

//AUEODELETE ADD_KYS_20160412
int leap(int year)
{
	if (year % 4 == 0 && year % 100 != 0)
		return 1;
	else if (year % 400 == 0 && year % 4000 != 0)
		return 1;
	else
		return 0;
}

int endMonth(int y, int m)
{
	int month[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	if (leap(y) == 1) 
		month[1] = 29;
	return month[m - 1];
}

ST_YMD* DateBefore(ST_YMD* cDate, int days)
{
	for (int i = 0; i<days - 1; i++)
	{
		if (cDate->day != 1)
			cDate->day = cDate->day - 1;
		else if (cDate->month != 1)
		{
			cDate->month = cDate->month - 1;
			cDate->day = endMonth(cDate->year, cDate->month);
		}
		else
		{
			cDate->year = cDate->year - 1;
			cDate->month = 12;
			cDate->day = endMonth(cDate->year, cDate->month);
		}
	}
	return cDate;
}
//

void CDataHandling::m_fnAutoDelete(CString strDeletePath, int nDay)
{
	CString strTempDay, strTempTime,  strTempTime2;
	CTime ctCurrentTime, ctDay;
	ctCurrentTime = CTime::GetCurrentTime();
	strTempTime = ctCurrentTime.Format(L"%Y%m%d%H%M%S");
	strTempDay = ctCurrentTime.Format(L"%Y%m%d");

	//AUEODELETE ADD_KYS_20160412
	ST_RESULT_DateBefore abc;
	ST_YMD *cPDate = &abc;
	CString Date;
	int days = nDay;
	if (nDay <= 0)
		nDay = 7;   //Default 1 WeekEnd
	else
	{
		cPDate->year = _wtoi(strTempDay.Left(4));
		cPDate->month = _wtoi(strTempDay.Mid(4, 2));
		cPDate->day = _wtoi(strTempDay.Right(2));
		DateBefore(cPDate, days);
		Date.Format(L"%04d%02d%02d", abc.year, abc.month, abc.day);
	}
	//

	G_MainWnd->m_DataHandling.bAutoDeleteState = true;
	CString Command;
	CFileFind finderDay, finderTime;

	CString strFolderPathDay, strFolderPathTime;

	strFolderPathDay.Format(L"%s\\*.*",strDeletePath);
	strFolderPathTime.Format(L"%s%s\\*.*",strDeletePath, strTempDay);

	//strFolderPath.Format(_T("D:\\MIS_FTP_Down_Test\\%s\\*.*", strTempDay);
	//BOOL bWorking = finder.FindFile("D:\\MIS_FTP_Down_Test\\ctCurrentTime\\*.*");

	BOOL bWorking = finderDay.FindFile(strFolderPathDay);
	std::wstring s;
	int cnt = 0;
	while (bWorking)
	{
		bWorking = finderDay.FindNextFile();

		if (finderDay.IsDots())
			continue;

		if (finderDay.IsDirectory())
		{
			CString strPath1 = finderDay.GetFilePath();

			if (_wtoi(strPath1.Right(8))  < _wtoi(Date))
			{
				s.clear() ;
				s.append((LPCTSTR)strPath1);
				DeleteDirectory(s);
				cnt++;
			}
			//else if(_wtoi(strTempDay) - _wtoi(strPath1.Right(8)) >= nDay)
			//{
			//	strPath1.Format(L"%s\\*.*", strPath1);
			//	BOOL bWorkingTime = finderTime.FindFile(strPath1);

			//	while (bWorkingTime)
			//	{
			//		bWorkingTime = finderTime.FindNextFile();

			//		if (finderTime.IsDots())
			//			continue;

			//		if (finderTime.IsDirectory())
			//		{
			//			CString strPath = finderTime.GetFilePath();
			//			CTime tmCreated;
			//			finderTime.GetCreationTime(tmCreated);
			//			strTempTime2 = tmCreated.Format("%Y%m%d%H%M%S");

			//			if(_wtoi(strTempTime.Right(8)) - _wtoi(strTempTime2.Right(8)) < 30000)
			//			{
			//				//Command.Format(_T("rmdir %s  /q /s", strPath);
			//				//system(Command); 
			//				s.clear() ;
			//				s.append((LPCTSTR)strPath);
			//				DeleteDirectory(s);
			//				cnt++;
			//				if(cnt > 10) break;
			//			}
			//		}
			//	}
			//	finderTime.Close();
			//}
		}
	}
	finderDay.Close();
	//s = "";
	//s.shrink_to_fit();
	s.clear() ;
	G_MainWnd->m_DataHandling.bAutoDeleteState = false;
}


int CDataHandling::loadFlashData(void)
{
	BOOL bReturn = FALSE;
	CString strValueData;
	CString str;

	int i;

	for( i = 0; i< NO_FLASH_INTDATA; i++)
	{
			strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[i],L"0");
			m_FlashData.data[i] = _wtoi(strValueData);
	}
	for( i = 0; i< NO_FLASH_DBDATA; i++)
	{
		strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[i],L"0");
		m_FlashData.dbdata[i] = _wtof(strValueData);
	}
	for (i = 0; i< NO_FLASH_STRDATA; i++)
	{
		strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_STRING[i], L"0");
		wcscpy(m_FlashData.strdata[i], strValueData.GetBuffer()); strValueData.ReleaseBuffer();
	}
//2016,04,03 jec add
	m_FlashData.data[LOT_PERCENT] = static_cast<int>(1000.0f*m_FlashData.data[COUNT_OK] / m_FlashData.data[LOT_TARGET]);

	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

	return 0;
}

int CDataHandling::updateProductionData(int pass)
{


	//update
	m_FlashData.data[TEST_COUNT_ACCU]++; if (m_FlashData.data[TEST_COUNT_ACCU] > 0x7FFFFFFF)m_FlashData.data[TEST_COUNT_ACCU] = 0;
	m_FlashData.data[COUNT_TOTAL]++; if (m_FlashData.data[COUNT_TOTAL] > 0x7FFFFFFF)m_FlashData.data[COUNT_TOTAL] = 0;
	if(pass){ 
		m_FlashData.data[COUNT_OK]++; if (m_FlashData.data[COUNT_OK] > 0x7FFFFFFF)m_FlashData.data[COUNT_OK] = 0;
	}else{
		m_FlashData.data[COUNT_NG]++; if (m_FlashData.data[COUNT_NG] > 0x7FFFFFFF)m_FlashData.data[COUNT_NG] = 0;
	}
	m_FlashData.data[LOT_PERCENT] = static_cast<int>(1000.0f*m_FlashData.data[COUNT_OK] / m_FlashData.data[LOT_TARGET]);

	BOOL bReturn = FALSE;
	CString str;

	str.Format(L"%d",m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	str.Format(L"%d",m_FlashData.data[COUNT_OK]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT6)->SetWindowText(str);

	// save
	int i;
	for (i = TEST_COUNT_ACCU; i <= COUNT_NG; i++)
	{
		str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
		m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[i],str); 
	}

	return 0;
}

int CDataHandling::updateProductionData_db(int pass)
{
	//update
	m_FlashData.data[TEST_COUNT_ACCU]++;
	m_FlashData.data[COUNT_TOTAL]++;
	if (pass){
		m_FlashData.data[COUNT_OK]++;
	}
	else{
		m_FlashData.data[COUNT_NG]++;
	}
	m_FlashData.data[LOT_PERCENT] = static_cast<int>(100.0f*m_FlashData.data[COUNT_OK] / m_FlashData.data[LOT_TARGET]);

	BOOL bReturn = FALSE;
	CString str;

	str.Format(L"%d", m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	str.Format(L"%d", m_FlashData.data[COUNT_OK]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT6)->SetWindowText(str);

	// save in db






	//int i;
	//for (i = TEST_COUNT_ACCU; i <= COUNT_NG; i++)
	//{
	//	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[i]);
	//	m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[i], str);
	//}

	return 0;
}

//when it's a new day
void CDataHandling::m_fnMakeDirectoryNewDay()
{
	memset(m_chImageSavePath, 0x00, MAX_PATH);
	memset(m_chImageSaveNGPath, 0x00, MAX_PATH);

	CString strCreateDirectory, strCreateDay, CamDirectory, strNgCreateDay, strNgCreateDirectory, strAlignCreateDay, strAlignCreateDirectory;
	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();

	strCreateDay.Format(L"%s%s", DATA_IMAGE_FOLDER, ctCurrentTime.Format(L"%Y%m%d"));
	m_fnMakeDirectory(strCreateDay);
	strCreateDay.ReleaseBuffer();
	memcpy(m_chImageSavePath, strCreateDay, strCreateDay.GetLength()*sizeof(wchar_t));

	strNgCreateDay.Format(L"%s%s", DATA_IMAGE_NG_FOLDER, ctCurrentTime.Format(L"%Y%m%d"));
	m_fnMakeDirectory(strNgCreateDay);
	strNgCreateDay.ReleaseBuffer();
	memcpy(m_chImageSaveNGPath, strNgCreateDay, strNgCreateDay.GetLength()*sizeof(wchar_t));

}


int CDataHandling::sqliteInit()
{
	int res;
	res = sqlite3_open("D:\\nBizData\\DB\\DB_CORE", &db);
	if (res == 0){
		res_DB = DB_OK;
		G_AddLog(3, L"DB Opened D:\\nBizData\\DB\\DB_CORE");
	}
	else{
		res_DB = DB_ERROR;
		G_AddLog(3, L"DB Open Failed D:\\nBizData\\DB\\DB_CORE");
	}

	//PRODUCTIONS
	res = sqlite3_prepare_v2(db, "SELECT * FROM PRODUCTION WHERE PNO = ?1 AND DATE = ?2 ;", -1, &stmtSelectProduction, NULL);
	res = sqlite3_prepare_v2(db, "INSERT INTO PRODUCTION(LINE_ID, DATE, PNO, TOTAL, OK, NG) VALUES(?1, ?2, ?3, ?4, ?5, ?6);", -1, &stmtInsertProduction, NULL);
	res = sqlite3_prepare_v2(db, "UPDATE PRODUCTION SET TOTAL = ?1, OK  = ?2,  NG  = ?3  WHERE NO = ?4;", -1, &stmtUpdateProduction, NULL);
	res = sqlite3_prepare_v2(db, "DELETE FROM PRODUCTION WHERE DATE < datetime(?1);", -1, &stmtDeleteProduction, NULL);

	//DEFECTS
	res = sqlite3_prepare_v2(db, "INSERT INTO DEFECTS(PARTID, SURFACE, TYPE, X1, Y1, X2, Y2, SIZE, DATETIME) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9);", -1, &stmtInsertDefect, NULL);
	res = sqlite3_prepare_v2(db, "DELETE FROM DEFECTS WHERE DATETIME < datetime(?1);", -1, &stmtDeleteDefect, NULL);


	//Init
	rowProd = -1;
	TOTAL = 0;
	OK = 0;
	NG = 0;
#ifdef DB
	if (res_DB == DB_OK && res == 0)
	{
		insertProductionRowDB();
	}
#endif
	return 0;
}




int CDataHandling::insertDefectDB(int PARTID, int SURFACE, int TYPE, int X1, int Y1, int X2, int Y2, int SIZE)
{
	if (res_DB != DB_OK)return -1;

	int res;
	CTime ctime = CTime::GetCurrentTime();
	CString strTime = ctime.Format("%Y-%m-%d %H:%M:%S");
	//DEFECTS
	//res = sqlite3_prepare_v2(db, "INSERT INTO DEFECT(PARTID, SURFACE, TYPE, X1, Y1, X2, Y2, SIZE) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8);", -1, &stmtInsertDefect, NULL);
	res = sqlite3_bind_int(stmtInsertDefect, 1, PARTID);
	res = sqlite3_bind_int(stmtInsertDefect, 2, SURFACE);
	res = sqlite3_bind_int(stmtInsertDefect, 3, TYPE);
	res = sqlite3_bind_int(stmtInsertDefect, 4, X1);
	res = sqlite3_bind_int(stmtInsertDefect, 5, Y1);
	res = sqlite3_bind_int(stmtInsertDefect, 6, X2);
	res = sqlite3_bind_int(stmtInsertDefect, 7, Y2);
	res = sqlite3_bind_int(stmtInsertDefect, 8, SIZE);
	char* pMultibyte = ConvertUnicodeToMultybyte2(strTime);
	res = sqlite3_bind_text(stmtInsertDefect, 9, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	res = sqlite3_step(stmtInsertDefect);
	if (res == SQLITE_DONE){

	}
	else{

	}
	sqlite3_reset(stmtInsertDefect);
	return 0;
}


int CDataHandling::insertProductionDB(CString PNO, int TOTAL, int OK, int NG)
{
	if (res_DB != DB_OK)return -1;

	int res;
	char* pMultibyte;
	CTime ctime = CTime::GetCurrentTime();
	//CString strTime = ctime.Format("%Y-%m-%d %H:%M:%S");
	CString strTime = ctime.Format("%Y-%m-%d %H:00:00"); // everyhour

	//res = sqlite3_prepare_v2(db, "INSERT INTO PRODUCTION(LINEID, DATE, PNO, TOTAL, OK, NG) VALUES(?1, ?2, ?3, ?4, ?5, ?6);", -1, &stmtInsertProduction, NULL);
	pMultibyte = ConvertUnicodeToMultybyte2(m_SystemParam.LINE_ID);
	res = sqlite3_bind_text(stmtInsertProduction, 1, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	pMultibyte = ConvertUnicodeToMultybyte2(strTime);
	res = sqlite3_bind_text(stmtInsertProduction, 2, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	pMultibyte = ConvertUnicodeToMultybyte2(PNO);
	res = sqlite3_bind_text(stmtInsertProduction, 3, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	res = sqlite3_bind_int(stmtInsertProduction, 4, TOTAL);
	res = sqlite3_bind_int(stmtInsertProduction, 5, OK);
	res = sqlite3_bind_int(stmtInsertProduction, 6, NG);

	res = sqlite3_step(stmtInsertProduction);
	if (res == SQLITE_DONE){
#ifdef DEV_MODE
		G_AddLog(1, L"Insert Production Done");
#endif
		TOTAL = 0;
		OK = 0;
		NG = 0;
	}
	else{
#ifdef DEV_MODE
		G_AddLog(1, L"Insert Production Failed");
#endif
	}
	sqlite3_reset(stmtInsertProduction);
	return res;
}

int CDataHandling::selectProductionDB(CString PNO)
{
	if (res_DB != DB_OK)return -1;

	int res;
	int ret = 0;
	char* pMultibyte;
	CTime ctime = CTime::GetCurrentTime();
	CString strTime = ctime.Format("%Y-%m-%d %H:00:00");

	//res = sqlite3_prepare_v2(db, "SELECT * FROM PRODUCTION WHERE PNO = ?1 AND DATE = ?2 ;", -1, &stmtSelectProduction, NULL);
	pMultibyte = ConvertUnicodeToMultybyte2(PNO);
	res = sqlite3_bind_text(stmtSelectProduction, 1, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	pMultibyte = ConvertUnicodeToMultybyte2(strTime);
	res = sqlite3_bind_text(stmtSelectProduction, 2, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	res = sqlite3_step(stmtSelectProduction);
	if (res == SQLITE_ROW){
		ret = sqlite3_column_int(stmtSelectProduction, 0);
#ifdef DEV_MODE
		//G_AddLog(1, L"Select Production got a row [%d]", ret);
#endif
	}
	else{
#ifdef DEV_MODE
		//G_AddLog(1, L"Select Production returns no row.");
#endif
		ret = 0;
	}
	sqlite3_reset(stmtSelectProduction);

	return ret;
}


int CDataHandling::updateProductionDB(int TOTAL, int OK, int NG, int NO)
{
	if (res_DB != DB_OK)return -1;

	int ret;
	int res;
	//char str[128];
	//CTime ctime = CTime::GetCurrentTime();
	//CString strTime = ctime.Format("%Y-%m-%d %H:00:00");

	//res = sqlite3_prepare_v2(db, "UPDATE PRODUCTION SET TOTAL = ?1 OK  = ?2  NG  = ?3  WHERE NO = ?4;", -1, &stmtUpdateProduction, NULL);
	res = sqlite3_bind_int(stmtUpdateProduction, 1, TOTAL);
	res = sqlite3_bind_int(stmtUpdateProduction, 2, OK);
	res = sqlite3_bind_int(stmtUpdateProduction, 3, NG);
	res = sqlite3_bind_int(stmtUpdateProduction, 4, NO);

	res = sqlite3_step(stmtUpdateProduction);
	if (res == SQLITE_DONE){
#ifdef DEV_MODE
		//G_AddLog(1, L"Select Production got a row");
#endif
		ret = 1;
	}
	else{
#ifdef DEV_MODE
		G_AddLog(1, L"Select Production returns no row.");
#endif
		ret = 0;
	}
	sqlite3_reset(stmtUpdateProduction);

	return ret;
}


int CDataHandling::insertProductionRowDB()
{
	int res;
	//get the row No. with same part no. and hour
	rowProd = selectProductionDB(m_SystemParam.ProductionModel); //Part No.
	if (rowProd == 0){// insert new 
		res = insertProductionDB(m_SystemParam.ProductionModel, 0, 0, 0);
		if (res == SQLITE_DONE){//get the row No. 
			rowProd = selectProductionDB(m_SystemParam.ProductionModel); //Part No.
		}
	}
	return 0;
}


int CDataHandling::deleteProductionDB(int month)
{
	int res;
	CTime ctime = CTime::GetCurrentTime();
	int hour = ctime.GetHour();
	int mon;
	if (month == 0) mon = 12; else mon = month;
	CTimeSpan tspan(mon*30, 0, 0, 0); 
	CTime ctime2 = ctime - tspan;
	CString str2 = ctime2.Format("%Y-%m-%d %H:%M:%S");
	char* pMultibyte = ConvertUnicodeToMultybyte2(str2);
	//Delete old records
	//res = sqlite3_prepare_v2(db, "DELETE FROM LABLE WHERE DATE < datetime(?1);", -1, &stmtDeleteProduction, NULL);
	res = sqlite3_bind_text(stmtDeleteProduction, 1, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	res = sqlite3_step(stmtDeleteProduction);
	if (res == SQLITE_DONE){
//#ifdef DEV_MODE
//		G_AddLog(3, L"deleteProductionDB done");
//#endif
	}
	sqlite3_reset(stmtDeleteProduction);


	return 0;
}


int CDataHandling::deleteDefectDB(int day)
{
	int res;
	CTime ctime = CTime::GetCurrentTime();
	int hour = ctime.GetHour();
	int d;
	if (day == 0)d = 180; else d = day;
	CTimeSpan tspan(d, 0, 0, 0); 
	CTime ctime2 = ctime - tspan;
	CString str2 = ctime2.Format("%Y-%m-%d %H:%M:%S");

	//Delete old records
	//res = sqlite3_prepare_v2(db, "DELETE FROM LABLE WHERE DATE < datetime(?1);", -1, &stmtDeleteDefect, NULL);
	char* pMultibyte = ConvertUnicodeToMultybyte2(str2);
	res = sqlite3_bind_text(stmtDeleteDefect, 1, pMultibyte, -1, SQLITE_STATIC);
	delete[] pMultibyte;
	res = sqlite3_step(stmtDeleteDefect);
	if (res == SQLITE_DONE){
//#ifdef DEV_MODE
//		G_AddLog(3, L"deleteDefectDB done");
//#endif
	}
	sqlite3_reset(stmtDeleteDefect);
	return 0;
}


int CDataHandling::storeVIDataDB(int pass)
{
	//DB save
	if (res_DB == DB_OK && rowProd > 0)
	{
		if (pass){
			TOTAL++;
			OK++;
#ifdef DB
			updateProductionDB(TOTAL, OK, NG, rowProd);
#endif
		}
		else{
			TOTAL++;
			NG++;
#ifdef DB
			updateProductionDB(TOTAL, OK, NG, rowProd);
#endif
		}
	}
	return 0;
}
