﻿#include "stdafx.h"
#include "SerialInterface.h"
#include "CommThread.h"
#include "MainWnd.h"


HWND g_hCommWnd;

IMPLEMENT_DYNAMIC(CSerialInterface, CWnd)

extern CMainWnd	*G_MainWnd;

CSerialInterface::CSerialInterface()
{
	// Å¬·¡½º »ý¼º **
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	wndclass.style			= CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= AfxWndProc;
	wndclass.hInstance		= AfxGetInstanceHandle();
	wndclass.hIcon			= NULL;
	wndclass.hCursor		= AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName	= NULL;
	wndclass.lpszClassName	= L"Serial232";

	AfxRegisterClass(&wndclass);

	CreateEx(NULL,L"Serial232", NULL, NULL, CRect(0,0,0,0), NULL, NULL, NULL);	

	g_hCommWnd = this->m_hWnd;			
	
 	//m_CommThread.m_pSerialInterface = this;
	// Å¬·¡½º »ý¼º ##		

	InitializeCriticalSection(&csComm);
}	

CSerialInterface::~CSerialInterface(void)
{
	memset(m_nComportIndex, 0x00, sizeof(int) * COMM_PORT_COUNT);
	
	DeleteCriticalSection(&csComm);		
}

BEGIN_MESSAGE_MAP(CSerialInterface, CWnd)	
	ON_MESSAGE(WM_COMM_READ,	OnCommRead)
	ON_WM_TIMER()
END_MESSAGE_MAP()


void CSerialInterface::m_fnInit()
{	
	BOOL bRes = TRUE;

	G_MainWnd->m_SystemSetupDlg.m_fnUpdateComport(); 
	//m_nComportIndex[0] = 0;

	for(int i= 0; i< COMM_PORT_COUNT; i++)
	{
		bRes = true;
		if(!m_CommThread[i].m_bConnected)
		{
			if(m_CommThread[i].m_fnInitialize(m_nComportIndex[i]) == FALSE)  //
			{
				G_AddLog(3,L"Comm Port %d Connection Failed", m_nComportIndex[i]+1); 
				bRes = FALSE;
			}
			else
			{
				G_AddLog(3,L"Comm Port %d Connected", m_nComportIndex[i]+1); 
				//Vision Light
				//if(i == 0)
				//{
				//	KillTimer(TIMER_LIGHT_READ);
				//	SetTimer(TIMER_LIGHT_READ, 500, NULL);
				//}
			}
			if(bRes == FALSE)
			{
				//G_AlarmListAdd(ALARM_3001, D_3001);
			}
		}
	}


}


int CSerialInterface::m_fnDeInit(void)
{

	for(int i= 0; i< COMM_PORT_COUNT; i++)
	{
		if(m_CommThread[i].m_bConnected)
			m_CommThread[i].ClosePort();
		m_CommThread[i].m_bConnected = false;
	}
	return 0;
}



//long CSerialInterface::OnCommRead(WPARAM wParam, LPARAM lParam)
LRESULT CSerialInterface::OnCommRead(WPARAM wParam, LPARAM lParam)
{
	int i, j;
	if(G_SystemModeData.unSystemMode == SYSTEM_EXIT)
	{
		return 0;
	}

	int nPortID = 0;
	nPortID = (int)wParam;

	for(i = 0; i< COMM_PORT_COUNT; i++)
	{
		if(m_nComportIndex[i] == nPortID) //m_CommThread
		{
			BYTE aByte; //µ¥ÀÌÅÍ¸¦ ÀúÀåÇÒ º¯¼ö		
			CString strReadByte;
			CString strCRLF;
			strCRLF.Format(L"%c%c",13, 10);

			int iSize =  m_CommThread[i].m_QueueRead.GetSize(); //Æ÷Æ®·Î µé¾î¿Â µ¥ÀÌÅÍ °¹¼ö

			if(iSize != 0) // Size°¡ 0ÀÌ¸é ¹«½ÃÇÑ´Ù.
			{

				for(j  = 0 ; j < iSize; j++)//µé¾î¿Â °¹¼ö ¸¸Å­ µ¥ÀÌÅÍ¸¦ ÀÐ¾î ¿Í È­¸é¿¡ º¸¿©ÁÜ
				{				
					m_CommThread[i].m_QueueRead.GetByte(&aByte);//Å¥¿¡¼­ µ¥ÀÌÅÍ ÇÑ°³¸¦ ÀÐ¾î¿È
					strReadByte.Format(L"%c", aByte);
					m_CommThread[i].m_strReciveString += strReadByte;
				}

				//process commport data for each thread
				processCommData(i);

			}
		}
	}




	return 0;
}

void CSerialInterface::OnTimer(UINT_PTR nIDEvent)
{	
	if(nIDEvent == TIMER_LIGHT_READ)
	{
		//m_fnSendToComportLight(MODE_READ);
	}

	CWnd::OnTimer(nIDEvent);
}


BOOL CSerialInterface::m_fnSendToComportLight(int port, int nMode, int nValue)
{
	int nSent;
	CString strMessage;
	BYTE byPort[256];
	CString strCR;
	CString strLF;
	Sleep(10); //ysKIM ONE SHOT

	memset(byPort, 0x00, 256);

	strCR.Format(L"%c",13);
	strLF.Format(L"%c",10);
	if(nMode == MODE_READ)
	{
		strMessage.Format(L"nLPD20 0 0 ");
	}
	else if(nMode == CH_1)
	{		
		strMessage.Format(L"nLPD20 1 %d ", nValue);
	}
	else if(nMode == CH_2)
	{
		strMessage.Format(L"nLPD20 2 %d ", nValue);
	}

	strMessage = strMessage + strCR + strLF;		
	//memcpy(byPort, strMessage, strMessage.GetLength());
	char* pMultibyte = ConvertUnicodeToMultybyte2(strMessage);
	memcpy(byPort, pMultibyte, strMessage.GetLength()); //Unicode
	delete[] pMultibyte;
//	EnterCriticalSection(&csComm);
	nSent = m_CommThread[port].WriteComm(byPort, strMessage.GetLength());
//	LeaveCriticalSection(&csComm);

	return TRUE;
}

/////// 2016,03,26 jed add start
//BOOL CSerialInterface::m_fnSendToComportLight2(int port, int nValueCH1, int nValueCH2)
//{
//	int nSent;
//	CString strMessage;
//	BYTE byPort[256];
//	CString strCR;
//	CString strLF;
////	Sleep(100); //ysKIM ONE SHOT
//
//	memset(byPort, 0x00, 256);
//
//	strCR.Format(L"%c", 13);
//	strLF.Format(L"%c", 10);
//
//	strMessage.Format(L"nCORE0 1 %d  %d  ", nValueCH1, nValueCH2);
//
//	strMessage = strMessage + strCR + strLF;
//	//memcpy(byPort, strMessage, strMessage.GetLength());
//	char* pMultibyte = ConvertUnicodeToMultybyte2(strMessage);
//	memcpy(byPort, pMultibyte, strMessage.GetLength()); //Unicode
//	delete[] pMultibyte;
//	//	EnterCriticalSection(&csComm);
//	nSent = m_CommThread[port].WriteComm(byPort, strMessage.GetLength());
//	//	LeaveCriticalSection(&csComm);
//
//	return TRUE;
//}
/////// 2016,03,26 jed add end

int CSerialInterface::processCommData(int port)
{
	switch(port)
	{
		//Comm1,2,3 Light control
		case 0:	
		case 1:	
		case 2:	processLightComm(port); 	break;

		case 3:	processComm3();		break;
		case 4:	processComm4();		break;
		case 5:	processComm5();		break;
		case 6:	processComm6();		break;
		case 7:	processComm7();		break;
		default:								break;
	}
	return 0;
}

int CSerialInterface::processLightComm(int port)
{
	CString strCRLF;
	strCRLF.Format(L"%c%c",13, 10);

	if(m_CommThread[port].m_strReciveString.GetLength() > 2)
	{
		if(m_CommThread[port].m_strReciveString.Right(2) == strCRLF)
		{
			m_CommThread[port].m_strReciveString.TrimRight();
			m_CommThread[port].m_strReciveString = m_CommThread[port].m_strReciveString.Mid(m_CommThread[port].m_strReciveString.Find(L" ") +1);						

			G_MainWnd->m_DataHandling.m_LightValueData.nLight1   = _wtoi(m_CommThread[port].m_strReciveString.Left(m_CommThread[port].m_strReciveString.Find(L" ")));
			G_MainWnd->m_DataHandling.m_LightValueData.nLight2 = _wtoi(m_CommThread[port].m_strReciveString.Mid(m_CommThread[port].m_strReciveString.Find(L" ")));

			m_CommThread[port].m_strReciveString = "";
			m_CommThread[port].m_QueueRead.Clear();
		}
	}

	return 0;
}


int CSerialInterface::processComm1()
{
	if(m_CommThread[1].m_strReciveString.GetLength() > 2)
	{
		m_CommThread[1].m_strReciveString = _T("");
		m_CommThread[1].m_QueueRead.Clear();
	}	
	return 0;
}
int CSerialInterface::processComm2(){

	return 0;
}
int CSerialInterface::processComm3(){

	return 0;
}
int CSerialInterface::processComm4(){

	return 0;
}
int CSerialInterface::processComm5(){

	return 0;
}
int CSerialInterface::processComm6(){

	return 0;
}
int CSerialInterface::processComm7(){

	return 0;
}

void CSerialInterface::LIGHTONOFF(int idx, int sw)
{
	//System param
	//if(sw){
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LIGHT_1 + idx]);
	//}else{
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, 0);
	//}

	//Recipe param
	//if (sw){
	//	if (idx == 0)
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE]);
	//	else
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE]);
	//}
	//else{
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, 0);
	//}
	
	for (int i = 0; i < sizeof(g_IsLIGHT_NOT_USE) / sizeof(g_IsLIGHT_NOT_USE[0]); i++)
	{
		if (idx == g_IsLIGHT_NOT_USE[i])
			return;
	}


	if (sw)
		{
			G_MainWnd->m_SerialInterface.m_fnSendToComportLight
				(g_IsLIGHT_CAM_PORT[idx],
				g_IsLIGHT_CAM_CHANNEL[idx],
				G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]]
				);
			G_MainWnd->m_InspectThread.m_vi.swLight[idx] = ON;				
		}
		else
		{
			G_MainWnd->m_SerialInterface.m_fnSendToComportLight
				(g_IsLIGHT_CAM_PORT[idx],
				g_IsLIGHT_CAM_CHANNEL[idx],
				0
				);
			G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
		}

	return;
}

/////// 2016,03,26 jed add
//void CSerialInterface::LIGHTONOFF2(int idx, int sw)
//{
//	for (int i = 0; i < sizeof(g_IsLIGHT_NOT_USE) / sizeof(g_IsLIGHT_NOT_USE[0]); i++)
//	{
//		if (idx == g_IsLIGHT_NOT_USE[i])
//			return;
//	}
//
//	if (sw)
//	{
//		if (idx % 2 == 0)
//		{
//			if (idx == 4)
//			{
//				if (G_MainWnd->m_InspectThread.m_vi.swLight[0] == OFF)
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]]
//						);
//				}
//				else
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[0]],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]]
//						);
//				}
//			}
//			else if (idx == 0)
//			{
//				if (G_MainWnd->m_InspectThread.m_vi.swLight[4] == OFF)
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]],
//						0
//						);
//				}
//				else
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[4]]
//						);
//				}
//			}
//			else
//			{
//				G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//					(g_IsLIGHT_CAM_PORT[idx],
//					G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]],
//					G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx + 1]]
//					);
//			}
//			G_MainWnd->m_InspectThread.m_vi.swLight[idx] = ON;
//		}
//		else
//		{
//			G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//				(g_IsLIGHT_CAM_PORT[idx],
//				G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx-1]],
//				G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx]]
//				);
//			G_MainWnd->m_InspectThread.m_vi.swLight[idx] = ON;
//		}
//	}
//	else
//	{
//		if (idx % 2 == 0)
//		{
//			if (idx == 4)
//			{
//				if (G_MainWnd->m_InspectThread.m_vi.swLight[0] == OFF)
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						0
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//				else
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[0]],
//						0
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//			}
//			else if (idx == 0)
//			{
//				if (G_MainWnd->m_InspectThread.m_vi.swLight[4] == OFF)
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						0
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//				else
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[4]]
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//			}
//			else
//			{
//				if (G_MainWnd->m_InspectThread.m_vi.swLight[idx + 1] == OFF)
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						0
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//				else
//				{
//					G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//						(g_IsLIGHT_CAM_PORT[idx],
//						0,
//						G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx + 1]]
//						);
//					G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//				}
//
//			}
//		}
//		else
//		{
//			if (G_MainWnd->m_InspectThread.m_vi.swLight[idx - 1] == OFF)
//			{
//				G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//					(g_IsLIGHT_CAM_PORT[idx],
//					0,
//					0
//					);
//				G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//			}
//			else
//			{
//				G_MainWnd->m_SerialInterface.m_fnSendToComportLight2
//					(g_IsLIGHT_CAM_PORT[idx],
//					G_MainWnd->m_DataHandling.m_RecipeParam.iParam[g_IsLIGHT_CAM_PARAM[idx - 1]],
//					0
//					);
//				G_MainWnd->m_InspectThread.m_vi.swLight[idx] = OFF;
//			}
//		}
//	}
//	return;
//}
/////// 2016,03,26 jed add