#include "stdafx.h"
#include "RecipeSetDlg.h"
#include "nBizFerriteCoreInspection.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(CRecipeSetDlg, CDialogEx)

extern CMainWnd	*G_MainWnd;

CRecipeSetDlg::CRecipeSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRecipeSetDlg::IDD, pParent)
{
}

CRecipeSetDlg::~CRecipeSetDlg(void)
{
}


void CRecipeSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_MODEL, m_GridModel);
	DDX_Control(pDX, IDC_GRID_MODEL_RECIPE, m_GridModelRecipe);
	DDX_Control(pDX, IDC_BTN_RECIPE_DELETE, m_btnRecipeDelete);
	DDX_Control(pDX, IDC_BTN_RECIPE_NEW, m_btnRecipeNew);
	DDX_Control(pDX, IDC_BTN_RECIPE_SAVE, m_btnRecipeSave);
	DDX_Control(pDX, IDC_BTN_APPLY, m_btnRecipeApply);
	DDX_Control(pDX, IDC_BUTTON2, m_btnResetAccu);
	DDX_Control(pDX, IDC_BUTTON3, m_btnResetProductionCount);
	DDX_Control(pDX, IDC_BUTTON5, m_btnSetLotTarget);
	DDX_Control(pDX, IDC_BUTTON4, m_btnSetLotNo);
	DDX_Control(pDX, IDC_BUTTON8, m_btnDefectClear);
}


BEGIN_MESSAGE_MAP(CRecipeSetDlg, CDialogEx)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_MODEL, OnGridDblClick)
	ON_NOTIFY(NM_CLICK, IDC_GRID_MODEL, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_MODEL_RECIPE, OnGridDblClick2)
	ON_NOTIFY(NM_CLICK, IDC_GRID_MODEL_RECIPE, OnGridClick2)
	ON_BN_CLICKED(IDC_BTN_APPLY, &CRecipeSetDlg::OnBnClickedBtnApply)
	ON_BN_CLICKED(IDC_BTN_RECIPE_DELETE, &CRecipeSetDlg::OnBnClickedBtnRecipeDelete)
	ON_BN_CLICKED(IDC_BTN_RECIPE_NEW, &CRecipeSetDlg::OnBnClickedBtnRecipeNew)
	ON_BN_CLICKED(IDC_BTN_RECIPE_SAVE, &CRecipeSetDlg::OnBnClickedBtnRecipeSave)
	ON_BN_CLICKED(IDC_BUTTON2, &CRecipeSetDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CRecipeSetDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &CRecipeSetDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON4, &CRecipeSetDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON8, &CRecipeSetDlg::OnBnClickedButton8)
END_MESSAGE_MAP()


void CRecipeSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}

void CRecipeSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//find selected production recipe
	recipeSelected = m_GridModel.GetCell(pItem->iRow, pItem->iColumn)->GetText();
	GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(recipeSelected);
	//load recipe data
	clearGridModelRecipe();
	fillGridModelRecipe(recipeSelected);
}

void CRecipeSetDlg::OnGridDblClick2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Double Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

void CRecipeSetDlg::OnGridClick2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

BOOL CRecipeSetDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initGridModel();
	fillGridModel();

	initGridModelRecipe();
	fillGridModelRecipe(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	applyRecipeInProduction(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	G_MainWnd->m_ServerDlg.m_staticType.SetText(part_Test.part_type);

	CString str;
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK]);
	GetDlgItem(IDC_EDIT6)->SetWindowText(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	GetDlgItem(IDC_EDIT4)->SetWindowText(str);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CRecipeSetDlg::m_fnInit(void)
{
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	
	//m_fnRecipeLoad();

}


int CRecipeSetDlg::initGridModel(void)
{
	try {
		m_GridModel.SetRowCount(30);
		m_GridModel.SetColumnCount(2);
		m_GridModel.SetFixedRowCount(1);		
		m_GridModel.SetFixedColumnCount(1);
		m_GridModel.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridModel.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridModel.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                         PART NO.                           "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"%d", row);					
					}
				}
			}
			m_GridModel.SetItem(&Item);
		}
	}

	m_GridModel.AutoSize(GVS_BOTH);
	m_GridModel.SetTextColor(RGB(0, 0, 105));
	//m_GridModel.ExpandToFit(true);
	//m_GridModel.AutoSizeColumns();
	m_GridModel.SetEditable(FALSE);
	m_GridModel.Refresh();

	return 0;
}

int CRecipeSetDlg::fillGridModel(void)
{
	int row = 1;

	CString strRecipe, strRecipeName;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			m_GridModel.GetCell(row++, 1)->SetText(strRecipeName);
		}//
	}
	strRecipe.Empty();
	strRecipeName.Empty();
	m_GridModel.Refresh();
	return 0;
}


void CRecipeSetDlg::OnBnClickedBtnApply()
{
	CString strVal;

	if (G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"Model can be changed in Engineer Mode", MB_ICONHAND);
		return;
	}

	if (G_SystemModeData.unSystemMode != SYSTEM_MANUAL)
	{
		AfxMessageBox(L"Model can be changed in Manual Mode", MB_ICONHAND);
		return;
	}

	if(recipeSelected != L"")
	{
		//selected recipe 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SELECTED_RECIPE, recipeSelected);	
		//G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel = recipe;
		wcscpy_s(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel, (LPCTSTR)recipeSelected);
		// set the recipe test data 

		//manual mode
		applyRecipeInProduction(recipeSelected);
		//close dlg
		//apply recipe in LCR Axis Recipe
		G_MainWnd->m_InspectThread.m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(recipeSelected);

		//Main Dlg
		G_MainWnd->m_ServerDlg.m_staticModel.SetText(part_Test.part_model);
		G_MainWnd->m_ServerDlg.m_staticType.SetText(part_Test.part_type);

		//VI Dlg
		G_MainWnd->m_InspectThread.m_vi.GetDlgItem(IDC_STATIC_VI_PNO)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_InspectThread.m_vi.fillGridCamSetting(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_InspectThread.m_vi.renewCamSettingParam();
		G_MainWnd->m_InspectThread.m_vi.pylonSetParam2();

		//Master test reset
		G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 0;
		G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 0;
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GRAY_LIGHT);
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GRAY_LIGHT);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"0"); 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"0"); 

		//Maker image data folder
		G_MainWnd->m_DataHandling.m_fnMakeDirectoryNewDay();
		strVal.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSavePath, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_DataHandling.m_fnMakeDirectory(strVal);

		strVal.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSaveNGPath, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_DataHandling.m_fnMakeDirectory(strVal);

		//InspectThread
		G_MainWnd->m_InspectThread.m_fnClearPartData();

		ShowWindow(SW_HIDE);
	}
}

int CRecipeSetDlg::applyRecipeInProduction(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	BOOL bReturn = FALSE;
	CString strValueData;



	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == ".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniRecipe.SetFileName(finder.GetFilePath());
				bReturn =true; //found
				break;
			}
		}
	}

	if(bReturn)
	{
		memset(part_Test.part_model, 0x00, 256);
		memset(part_Test.part_type, 0x00, 256);
		if(strRecipeName.GetLength() < 256)
			memcpy(part_Test.part_model, strRecipeName, strRecipeName.GetLength()*sizeof(wchar_t));
		else
			G_AddLog(3,L"if(strRecipeName.GetLength() < 256) < 256) fail  %s, %d ", __FILE__, __LINE__);
		//Part type
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEY_TYPE, L"");
		if (strValueData.GetLength() < 256)
			memcpy(part_Test.part_type, strValueData, strValueData.GetLength()*sizeof(wchar_t));
		else
			G_AddLog(3, L"if(strValueData.GetLength() < 256) fail  ");
		GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(part_Test.part_type);

		// get data from ini file
		// strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"");

		int nSize = sizeof(ST_RECIPE_ITEM_RCP_DLG) / 8 + 1;

		for (int row = 1; row < nSize; row++)
		{
			strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, ST_RECIPE_ITEM_RCP_DLG[row - 1], L"");
			if (strValueData == "")
				continue;
			m_GridModelRecipe.SetItemText(row, 2, strValueData);
		}

		//READ INTO RECIPE PARAM STRUCT RCP DLG 6 ITEM
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_PNO, L"");
		wcscpy_s(G_MainWnd->m_DataHandling.m_RecipeParam.strParam[RP_PNO], strValueData);
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_CORE_OD, L"");
		G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CORE_OD] = _wtof(strValueData);
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_CORE_ID, L"");
		G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CORE_ID] = _wtof(strValueData);
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_CORE_HEIGHT, L"");
		G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CORE_HEIGHT] = _wtof(strValueData);
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_CORE_COLOR, L"");
		wcscpy_s(G_MainWnd->m_DataHandling.m_RecipeParam.strParam[RP_CORE_COLOR], strValueData);
		strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEYNAME_INPUT_CLOSE_DELAY, L"");
		G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_INPUT_CLOSE_DELAY] = _wtoi(strValueData);
		
		//APPLY ****************************


		//Vision Inspection Cam Setting
		G_MainWnd->m_InspectThread.m_vi.fillGridCamSetting(recipe);
#ifndef _VI_SKIP
		wstring strModel = G_MainWnd->m_RecipeSetDlg.part_Test.part_model;
		wchar_t szText[256];
		if (!G_MainWnd->m_InspectThread.m_vi.LoadNIPJob()) {
			wsprintf(szText, L"Vision Inspection Data File Load Failed : %s", strModel.c_str());
			G_AddLog(3, szText);
			return false;
		}
#endif

	}
	return 0;
}

BOOL CRecipeSetDlg::m_fnWriteIniFile(LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString) 
{
	BOOL bReturn = FALSE;

	bReturn = m_iniRecipe.WriteProfileString(lpszSection, lpszKeyName, lpString);
	
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";		
		//G_AddLog(3,L"%s 파일 기록 Error : 섹션%s, 키네임%s, 스트링%s", strMessage, lpszSection, lpszKeyName, lpString);
	}	
	return bReturn;
}

CString CRecipeSetDlg::m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"";
	if(default != 0)
	{
		lpDefault = default;
	}

	bReturn = m_iniRecipe.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";
		//G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}



void CRecipeSetDlg::OnBnClickedBtnRecipeDelete()
{
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"IT CAN BE DONE IN ENGINEER MODE",MB_ICONHAND);
	}else{

		CString modelSelected;
		GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(modelSelected);


		CString strPath = RECIPE_FOLDER;
		strPath.Format(L"%s%s.ini", RECIPE_FOLDER, modelSelected);

		CString strMsg;
		strMsg.Format(L"%s File , do you want delete?", modelSelected);

		if (AfxMessageBox(strMsg, MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDYES)
		{

			DeleteFile(strPath);
			initGridModel();
			fillGridModel();
			fillGridModelRecipe(modelSelected);
		}

	}
}


void CRecipeSetDlg::OnBnClickedBtnRecipeNew()
{	
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"IT CAN BE DONE IN ENGINEER MODE",MB_ICONHAND);
	}else{
		GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(L"");
		GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(L"");
		AfxMessageBox(L"INPUT NEW PART NO. AND DESCRIPTION",MB_ICONINFORMATION);
	}
}


void CRecipeSetDlg::OnBnClickedBtnRecipeSave()
{
	CString modelSelected;
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"IT CAN BE DONE IN ENGINEER MODE",MB_ICONHAND);
	}else{
		GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(modelSelected);
		if(modelSelected !="")
		{
			saveRecipe(modelSelected);
		}
		fillGridModel();
	}
}

//reset accumulation count
void CRecipeSetDlg::OnBnClickedButton2()
{
	CString str;
	G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU] = 0;

	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_TEST_COUNT_ACCU,str);

	GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);
}

//reset production count
void CRecipeSetDlg::OnBnClickedButton3()
{
	CString str;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG] = 0;

	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_TOTAL,str); 
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_OK,str); 
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_NG,str); 

	GetDlgItem(IDC_EDIT6)->SetWindowText(str);
	//GetDlgItem(IDC_EDIT3)->SetWindowText(str);

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);
}

//m_btnSetLotTarget
void CRecipeSetDlg::OnBnClickedButton5()
{
	CString str;
	GetDlgItem(IDC_EDIT4)->GetWindowText(str);
	G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET] = _wtoi(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_LOT_TARGET,str); 

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

}

//Set Lot No.
void CRecipeSetDlg::OnBnClickedButton4()
{
	CString str;
	GetDlgItem(IDC_EDIT5)->GetWindowText(str);
	wcscpy(G_MainWnd->m_DataHandling.m_FlashData.strdata[LOT_NO], str.GetBuffer()); str.ReleaseBuffer();
	//str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LOT_NO]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_LOT_NO, str);

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

}

//
void CRecipeSetDlg::OnBnClickedButton8()
{
	CString str;

	G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_INNER] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_BOTTOM] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_OUTER] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[DEFECT_TOP] = 0;

	str.Format(L"%d", 0);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_DEFECT_INNER, str);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_DEFECT_BOTTOM, str);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_DEFECT_OUTER, str);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_DEFECT_TOP, str);

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

}



int CRecipeSetDlg::saveRecipe(CString recipe)
{
	//find recipe
	CString strRecipe, strRecipeName, strRecipeFullPath;
	bool filefound = false;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if (strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength() - 4);

			if (strRecipeName == recipe)
			{
				strRecipeFullPath = (LPCTSTR)finder.GetFilePath();
				m_iniRecipe.SetFileName(strRecipeFullPath);
				filefound = true;
				break;
			}
		}//
	}

	//if not make new recipe
	if (!filefound)
	{
		strRecipeFullPath = RECIPE_FOLDER + recipe + L".ini";
		m_iniRecipe.SetFileName(strRecipeFullPath);
	}

	//save recipe data from grid
	BOOL bReturn = FALSE;
	CString strValueData;

	// ....................
	GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(strValueData);
	bReturn = m_fnWriteIniFile(SECTION_RECIPE_DATA, KEY_MODEL, strValueData);
	GetDlgItem(IDC_EDIT_TYPE)->GetWindowText(strValueData);
	bReturn = m_fnWriteIniFile(SECTION_RECIPE_DATA, KEY_TYPE, strValueData);

	GV_ITEM Item;
	Item.col = 2;

	for (int row = 1; row < m_GridModelRecipe.GetRowCount(); row++)
	{
		strValueData = m_GridModelRecipe.GetItemText(row, Item.col);
		if (strValueData == "")
			continue;
		bReturn = m_fnWriteIniFile(SECTION_RECIPE_DATA, ST_RECIPE_ITEM_RCP_DLG[row - 1], strValueData);
	}

	return 0;
}


int CRecipeSetDlg::initGridModelRecipe(void)
{
	//Grid init
	int nSize ;

	// Recipe param grid 
	try {
		nSize = sizeof(ST_RECIPE_ITEM_RCP_DLG) / 8 + 1;
		m_GridModelRecipe.SetRowCount(nSize);
		m_GridModelRecipe.SetColumnCount(3);
		m_GridModelRecipe.SetFixedRowCount(1);
		m_GridModelRecipe.SetFixedColumnCount(2);
		m_GridModelRecipe.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridModelRecipe.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridModelRecipe.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT | GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if (row == 0)
			{
				Item.nFormat = DT_VCENTER | GVIF_FORMAT;
				if (col == 0)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T(" NUM "));
				}
				else if (col == 1)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T("                   RECIPE PARAM                    "));
				}
				else if (col == 2)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T("                  VALUE                  "));
				}

			}//if(row == 0)
			else
			{
				if (col == 0)
				{
					if (row != 0)
					{
						Item.nFormat = DT_VCENTER | GVIF_FORMAT | DT_CENTER;
						Item.strText.Format(L"%d", row);
					}
				}
				else if (col == 1)
				{
					Item.nFormat = DT_VCENTER | GVIF_FORMAT;
					int nSize = sizeof(ST_RECIPE_ITEM_RCP_DLG) / 8;
					if (row - 1 < nSize)
						Item.strText.Format(L"%s", ST_RECIPE_ITEM_RCP_DLG[row - 1]);
				}
			}//if(row == 0)
			m_GridModelRecipe.SetItem(&Item);
		}
	}

	//m_GridModelRecipe.AutoSize(GVS_BOTH);
	m_GridModelRecipe.SetTextColor(RGB(0, 0, 105));
	m_GridModelRecipe.AutoSizeColumns();
	//m_GridModelRecipe.ExpandToFit(true);

	m_GridModelRecipe.SetEditable(FALSE);
	m_GridModelRecipe.Refresh();


	return 0;
}



//load Recipe Selected in GridModel
int CRecipeSetDlg::fillGridModelRecipe(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	BOOL bReturn = FALSE;
	CString strValueData;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if (strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength() - 4);
			if (strRecipeName == recipe)
			{
				m_iniRecipe.SetFileName(finder.GetFilePath());
				//load recipe data in GridModeRecipe
				strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEY_MODEL, L"");
				GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(strValueData);
				strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, KEY_TYPE, L"");
				GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(strValueData);

				int nSize = sizeof(ST_RECIPE_ITEM_RCP_DLG) / 8 + 1;

				for (int row = 1; row < nSize; row++)
				{
					//strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_RECIPE_ITEM_RCP_DLG[row - 1]);
					strValueData = m_fnReadIniFile(SECTION_RECIPE_DATA, ST_RECIPE_ITEM_RCP_DLG[row - 1], L"");
					if (strValueData == "")
						continue;
					m_GridModelRecipe.SetItemText(row, 2, strValueData);
				}
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}
	m_GridModelRecipe.Refresh();
	return 0;
}


int CRecipeSetDlg::clearGridModelRecipe(void)
{
	GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(L"");
	GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(L"");
	return 0;
}