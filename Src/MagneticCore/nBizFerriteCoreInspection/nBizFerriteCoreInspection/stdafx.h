
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once
#pragma warning (disable : 4005)
#pragma warning (disable:4819)
#pragma warning (disable:4996)

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.

#include <afxcview.h>
#include <afxdisp.h>        // MFC 자동화 클래스입니다.

#include <vector>
#include <iostream>

//Memory Leak
#define _CRTDBG_MAP_ALLOC 
#include <stdlib.h> 
#include <crtdbg.h>

using namespace std;

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include <afxsock.h>            // MFC 소켓 확장

/*
#include "opencv/cv.h"
#include "opencv/cxcore.h"
#include "opencv/highgui.h"
*/

#include "NIPO.h"
#pragma comment (lib, "NIPO.lib")


#ifdef _UNICODE
#define tstring std::wstring
#else
#define tstring std::wstring
#endif


extern void G_AddDayReportLog(char* chPanelID, ...);
extern void G_AddLog(int nLogLevel, TCHAR* chLog, ...);
extern void G_AlarmListAdd(int nAlarmID, char* pAlarmData, int nAlarmCD = 2);

#define ON 1
#define OFF 0

enum
{
	SYSTEM_AUTO = 1000,
	SYSTEM_MANUAL = 1001,
	SYSTEM_AUTO_RUN,

	SYSTEM_BLUE,
	SYSTEM_INIT_END,

	SYSTEM_ERROR,
	SYSTEM_OK,
	SYSTEM_EXIT,
};


struct G_SystemModeStruct {	
	unsigned short unSystemMode;
	unsigned short unSystemInit;
	unsigned short unSystemError;
	unsigned short unLensIndex;
	unsigned short unLensIndexCMD;	
	unsigned short unUIMode;

	G_SystemModeStruct()
	{
		memset(this, 0x00, sizeof(G_SystemModeStruct));
	}
};

extern G_SystemModeStruct G_SystemModeData;

#define DIALOG_ALARM_COLOR			RGB(235,0,0)
#define DIALOG_BG_COLOR				RGB(60, 60, 60)		//RGB(235,235,235)
#define STATIC_BG_COLOR				RGB(50, 50, 50)		// RGB(225,225,225)
#define STATIC_TX_COLOR				RGB(180,180,180)	//RGB(30,30,145)
#define DIALOG_INIT_COLOR			RGB(153, 255,255)	//RGB(153, 153, 153)
#define ENABLE_UI_COLOR				RGB(80, 80, 150)
#define DISABLE_UI_COLOR			RGB(180, 180, 180)
#define ENABLE_UI_FONT				WHITE
#define DISABLE_UI_FONT				RGB(30,30,145)


#define _VI_ALIGN

//#ifdef _DEBUG
//#define NO_CORE_RUN
//#define VIRTUAL_RUN		
//#define VI_MEM_TEST2 
//#define VI_MEM_TEST //
//#define _CAM_SKIP
//#define _VI_SKIP
//#define DEV_MODE
//#define VI_SQC
#define NO_DEFECT_DISPLAY
//#define DB
//#endif


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

char chResult[1024];
wchar_t wcResult[1024];

//CCriticalSection m_csCNM;
char* ConvertUnicodeToMultybyte(CString strUnicode);
char* ConvertUnicodeToMultybyte2(CString strUnicode);
wchar_t* ConvertMultybyteToUnicode(const char *chText);

void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect);

//Vision Inspection

#define MAX_VI_TEST		4
#define MAX_VI_DEFECT	5

//VISION INSPECTION NG INFOS

#define VI_LEFT			0 //X1
#define VI_TOP			1 //Y1
#define VI_RIGHT			2 //X2
#define VI_BOTTOM		3 //Y2

#define NG_SURF_IN	1
#define NG_SURF_BTM	2
#define NG_SURF_OUT	3
#define NG_SURF_TOP	4

#define NG_TYP_SCRETCH				0
#define NG_TYP_BROKEN_CHARACTER		1
#define NG_TYP_CALIBRATION_FAIL		2
#define NG_TYP_NOT_LOAD_IMAGE		3
#define NG_TYP_TEST	100

#define NG_CAL_ERR	 1
#define NG_CAL_ERR_NO_IMAGE 2

const CString ST_VI_SURF[] = {
	L"",
	L"INNER",
	L"BOTTOM",
	L"OUTER",
	L"TOP",
};

const CString ST_VI_DEFECT_TYPE[] = {
	L"NG_TYP_SCRETCH",
	L"NG_TYP_BROKEN_CHARACTER",
	L"NG_TYP_3",
	L"NG_TYP_4",
};

typedef struct _VI_DEFECT
{
	int surface;
	int type;
	int coords[4];
	int size;
	_VI_DEFECT(){ memset(this, 0x00, sizeof(VI_DEFECT)); }
}VI_DEFECT;


//VISION INSPECTION RESULT
typedef struct _VI_RESULTS
{
	//CCriticalSection m_csVIResult[MAX_VI_TEST - 1]; //2 cams 

	int results[MAX_VI_TEST]; // VI_CLEAR, VI_OK, VI_NG, _VI_ERROR
	vector<VI_DEFECT> vecDefects[MAX_VI_TEST];

	_VI_RESULTS(){ memset(this, 0x00, sizeof(VI_RESULTS)); }
}VI_RESULTS;


//Vector 
typedef struct _VI_DATA
{
	long id;		//Part ID
	IplImage *pImgBuf;
	//vector<VI_DEFECT> vecDefects;
	wchar_t szImageFileName[128];

	_VI_DATA(){ memset(this, 0x00, sizeof(VI_DATA)); }
}VI_DATA;

typedef struct _VI_RESULT
{
	long id;		//Part ID
	int result;	// VI_CLEAR, VI_OK, VI_NG, _VI_ERROR
	vector<VI_DEFECT> *vecDefects;

	_VI_RESULT(){ memset(this, 0x00, sizeof(VI_RESULT)); }
}VI_RESULT;

#define kys_0401