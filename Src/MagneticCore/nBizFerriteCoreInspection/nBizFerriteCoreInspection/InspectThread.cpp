#include "stdafx.h"
#include "InspectThread.h"
#include "MainWnd.h"
#include <ctime>

// to enable 'DEBUG_PRINT_TIME' option, uncomment it in the "InspectThread.h" file
#ifdef DEBUG_PRINT_TIME
DWORD gConveyerMoveStartTime[2] = { 0, };
DWORD gConveyerMovingTime[2] = { 0, };
DWORD gConveyerMoveEndTime[2] = { 0, };
DWORD gMaxFullTime[2] = { 0, };
DWORD gMaxReadyTime[2] = { 0, };
DWORD gMaxStepTime[2] = { 0, };
int gMaxFullTimeId[2] = { 0, };
static DWORD dStepStartTime[TOTAL_STAGE] = { 0, };

#define GET_BATCH_NAME(batch, strBatch) \
	switch (batch) { \
	case INPUT: strBatch = L"INPUT"; break; \
	case IN_CENTERING: strBatch = L"IN_CENTERING"; break; \
	case IN_VI0: strBatch = L"IN_VI0"; break; \
	case IN_VI1: strBatch = L"IN_VI1"; break; \
	case IN_BUFFER: strBatch = L"IN_BUFFER"; break; \
	case OUT_FLIPDROP: strBatch = L"OUT_FLIPDROP"; break;\
	case OUT_STOPPER: strBatch = L"OUT_STOPPER"; break; \
	case OUT_CENTERING: strBatch = L"OUT_CENTERING"; break; \
	case OUT_VI2: strBatch = L"OUT_VI2"; break; \
	case OUT_VI3: strBatch = L"OUT_VI3"; break; \
	case OUT_NGOUT: strBatch = L"OUT_NGOUT"; break; \
	case IN_CNVYR: strBatch = L"IN_CNVYR"; break; \
	case OUT_CNVYR: strBatch = L"OUT_CNVYR"; break; \
	}
#endif

extern CMainWnd	*G_MainWnd;

UINT m_fnThreadStgInput(LPVOID lParam);
UINT m_fnThreadStgInputBuffer(LPVOID lParam);
UINT m_fnThreadStgOutCentering(LPVOID lParam);
UINT m_fnThreadStgConveyer(LPVOID lParam);
UINT m_fnThreadStgFlipDrop(LPVOID lParam);
UINT m_fnThreadStgCam0(LPVOID lParam);  //CAM0
UINT m_fnThreadStgCam1(LPVOID lParam);	//CAM1
UINT m_fnThreadStgCam2(LPVOID lParam);	//CAM2
UINT m_fnThreadStgCam3(LPVOID lParam);	//CAM3


CInspectThread::CInspectThread(void)
{
	m_nSeqMode = 0;
	m_nScanCount = 1;
	checkStatus = 0;
	suspendCount = 0;
}

CInspectThread::~CInspectThread(void)
{
	
}


int CInspectThread::m_fnInit()
{

	return 0;
}

int CInspectThread::m_fnInit(CDataHandling* pDataHandling, 
	CSerialInterface		*pSerialInterface, 
	CRecipeSetDlg			*pRecipeSetDlg,
	PIO						*pPIODlg
	//AxisMotion				*pAxisDlg
	)
{
	m_pDataHandling = pDataHandling;
	m_pSerialInterface = pSerialInterface;
	m_pRecipeSetDlg = pRecipeSetDlg ;	
	
	m_pPIO = pPIODlg ;	
	//m_pAxis = pAxisDlg ;	

	m_AxisDlg.m_fnInit();
	m_vi.m_fnInit();

	memset(m_nCurStep, 0x00, sizeof(int)*TOTAL_STAGE);
	memset(m_nPrevStep, 0x00, sizeof(int)*TOTAL_STAGE);
	memset(m_nOldStep, 0x00, sizeof(int)*TOTAL_STAGE);

	for (int i = INPUT; i <= OUT_CNVYR; i++)
	{
		m_nCurStep[i] = G_MainWnd->m_DataHandling.m_FlashData.data[STEP_INPUT + i];
	}

	memset(&stStgBatch, 0x00, TOTAL_STAGE*sizeof(STG_BATCH));
	m_fnClearPartData();
	
	PartID = 0;

	m_fnBeginThread();

	return 0;
}

int CInspectThread::m_fnClearPartData()
{
	PartID = 0;
	memset(&m_PartData, 0x00, TOTAL_STAGE*sizeof(PART_DATA));
	memset(&m_PartDataBuffer, 0x00, TOTAL_BUFFER_SIZE*sizeof(PART_DATA));
	return 0;
}

//NOT USE
int CInspectThread::m_fnBeginInspectThread()
{
	do{
		suspendCnt[STAGE_THREAD_INPUT] = m_pThreads[STAGE_THREAD_INPUT]->ResumeThread();
	} while (suspendCnt[STAGE_THREAD_INPUT] > 0);

	return 0;
}
//
void CInspectThread::m_fnDeInit()
{
	m_AxisDlg.m_fnDeInit();

	m_vi.m_fnDeInit();
	m_fnEndThread();

}


void CInspectThread::m_fnScanCountPlus()
{
	CString strScanCountText;
	strScanCountText.Format(L"%d", m_nScanCount);

	CMainWnd *pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	m_nScanCount++;

	//pMainWnd->m_ServerDlg.m_ctrlScanCount.SetText(strScanCountText, RED);
}

//////////////////////////////////////////////////////////////////////////////////////////
//BATCH STEP

void CInspectThread::m_fnClearBatchStep(int batch)
{
	m_nCurStep[batch]	= 0;
	m_nPrevStep[batch]	= 0;	
}

void CInspectThread::m_fnSetBatchStep(int batch, int nStep)
{
	CString str;
	m_nPrevStep[batch] = m_nCurStep[batch];
	m_nCurStep[batch] = nStep;
	G_MainWnd->m_DataHandling.m_FlashData.data[STEP_INPUT + batch] = m_nCurStep[batch];
	wchar_t step[5];
	swprintf_s(step,L"%d",nStep);
	//STORE STEP IN FLASH DATA
	if(!(nStep == STEP22 || nStep == STEP19 || nStep == STEP20 )){
		str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[STEP_INPUT + batch]);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[STEP_INPUT + batch], str);
	}
	if (batch != OUT_CNVYR)
		G_MainWnd->m_PIODlg.m_btnStepClear[batch].SetWindowText(step);
	else if (batch == OUT_CNVYR)
		G_MainWnd->m_PIODlg.m_btnOutCnvyrClear.SetWindowText(step);

	//debug
	//G_AddLog(1, L"m_fnSetBatchStep batch:%d, step:%d  ", batch, nStep);

}

int	CInspectThread::m_fnGetBatchStep(int batch)
{
	return m_nCurStep[batch];
}

int	CInspectThread::m_fnGetPrevBatchStep(int batch)
{
	return m_nPrevStep[batch];
}

void	CInspectThread::m_fnSetPrevBatchStep(int batch, int nStep)
{

	m_nPrevStep[batch] = nStep;
}

void CInspectThread::m_fnSetOldBatchStep(int batch, int nOldStep)
{
	m_nOldStep[batch] = nOldStep;
}

int		CInspectThread::m_fnGetOldBatchStep(int batch)
{
	return m_nOldStep[batch];
}

BOOL CInspectThread::m_fnCheckBatchStep(int batch)
{
	if(m_nCurStep[batch] == m_nPrevStep[batch])
		return FALSE;
	else
	{
		m_nPrevStep[batch] = m_nCurStep[batch];		
		return TRUE;
	}
}

int	CInspectThread::m_fnRetryBatchStep(int batch)
{
	return m_nCurStep[batch];
}


int	CInspectThread::m_fnAllowConveyor(int cnvyr, bool allow, int batch)
{
	int idx;
	CString str;
	//int bit = 0x0001 << batch;
	int bit = ST_IN_CNVYR_INTERLOCK[batch];
	idx = (cnvyr == IN_CNVYR)?0:1;
	m_csCnvyrAllow[idx].Lock();
	if (allow){
		stStgBatch[cnvyr].allow |= bit;
	}
	else{
		stStgBatch[cnvyr].allow &= ~bit;
	}

#ifdef DEBUG_PRINT_TIME
	wstring strBatch;

	GET_BATCH_NAME(batch, strBatch);

	wstring strCnvyr;
	switch (cnvyr) {
	case IN_CNVYR: strCnvyr = L"IN_CNVYR"; break;
	case OUT_CNVYR: strCnvyr = L"OUT_CNVYR"; break;
	}

	DWORD nCurTime = ::GetTickCount();
	if (allow) {
		DWORD nReadyAndStepTime = nCurTime - gConveyerMoveEndTime[idx];
		DWORD nStepTime = nCurTime - dStepStartTime[batch];
		DWORD nReadyTime = nReadyAndStepTime - nStepTime;
		DWORD nFullTime = nReadyAndStepTime + gConveyerMovingTime[idx];

		DebugPrint(L"## Allow Set, Cnvyr(%d, %s), Batch(%d, %s), Time : %d ms (Move : %d ms, Ready : %d ms, Step : %d ms)", cnvyr, strCnvyr.c_str(), batch, strBatch.c_str(), nFullTime, gConveyerMovingTime[idx], nReadyTime, nStepTime);

		if (nFullTime > gMaxFullTime[idx]) {
			gMaxFullTimeId[idx] = batch;
			gMaxFullTime[idx] = nFullTime;
			gMaxReadyTime[idx] = nReadyTime;
			gMaxStepTime[idx] = nStepTime;
		}

		int nAllBits;
		if (cnvyr == IN_CNVYR) {
			nAllBits = CNVYR_IN_ALL;
		}
		else {
			nAllBits = CNVYR_OUT_ALL;
		}

		if ((stStgBatch[cnvyr].allow & nAllBits) == nAllBits) {
			GET_BATCH_NAME(gMaxFullTimeId[idx], strBatch);

			DebugPrint(L"[[ All Allow Set !! Max : Batch(%d, %s), Time : %d ms (Move : %d ms, Ready : %d ms, Step : %d ms) ]]", gMaxFullTimeId[idx], strBatch.c_str(), gMaxFullTime[idx], gConveyerMovingTime[idx], gMaxReadyTime[idx], gMaxStepTime[idx]);
		}
		else {
			for (int i = 0; i < TOTAL_STAGE; i++) {
				int bit = ST_IN_CNVYR_INTERLOCK[i] & nAllBits;

				if (bit == 0) {
					continue;
				}

				if (!(stStgBatch[cnvyr].allow & bit)) {
					GET_BATCH_NAME(i, strBatch);

					nStepTime = nCurTime - dStepStartTime[i];
					nReadyTime = nReadyAndStepTime - nStepTime;
					DebugPrint(L"    > Not Yet, Cnvyr(%d, %s), Batch(%d, %s), Time : %d ms (Move : %d ms, Ready : %d ms, Step : %d ms)", cnvyr, strCnvyr.c_str(), i, strBatch.c_str(), nFullTime, gConveyerMovingTime[idx], nReadyTime, nStepTime);
				}
			}
		}
	}
	else {
		dStepStartTime[batch] = nCurTime;
		DWORD nReadyTime = nCurTime - gConveyerMoveEndTime[idx];
		DWORD nFullTime = nReadyTime + gConveyerMovingTime[idx];
		DebugPrint(L"Allow Clear, Cnvyr(%d, %s), Batch(%d, %s), Time : %d ms (Move : %d ms, Ready : %d ms)", cnvyr, strCnvyr.c_str(), batch, strBatch.c_str(), nFullTime, gConveyerMovingTime[idx], nReadyTime);
	}
#endif

	m_csCnvyrAllow[idx].Unlock();

	//if (cnvyr == IN_CNVYR){
	//	G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_ALLOW] = stStgBatch[cnvyr].allow;
	//	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_ALLOW]);
	//	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[IN_CNVYR_ALLOW], str);
	//}
	//else if (cnvyr == OUT_CNVYR){
	//	G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_ALLOW] = stStgBatch[cnvyr].allow;
	//	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_ALLOW]);
	//	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[OUT_CNVYR_ALLOW], str);
	//}

	return 0;
}


int	CInspectThread::m_fnRequestConveyor(int cnvyr, bool request, int batch)
{
	int idx;
	CString str;
	//int bit = 0x0001 << batch;
	idx = (cnvyr == IN_CNVYR) ? 0 : 1;
	m_csCnvyrRequest[idx].Lock();
	int bit = ST_IN_CNVYR_INTERLOCK[batch];
	if (request){
		stStgBatch[cnvyr].request |= bit;
	}
	else{
		stStgBatch[cnvyr].request &= ~bit;
	}
	m_csCnvyrRequest[idx].Unlock();

	//if (cnvyr == IN_CNVYR){
	//	G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_REQUEST] = stStgBatch[cnvyr].request;
	//	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_REQUEST]);
	//	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[IN_CNVYR_REQUEST], str);
	//}
	//else if (cnvyr == OUT_CNVYR){
	//	G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_REQUEST] = stStgBatch[cnvyr].request;
	//	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_REQUEST]);
	//	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[OUT_CNVYR_REQUEST], str);
	//}

	return 0;
}


void CInspectThread::m_fnSetInspectionStep(int batch, int nStep)
{
	CString str;
	stStgBatch[batch].step = nStep; 
	//G_MainWnd->m_DataHandling.m_FlashData.data[LCR_INSPECT_STEP] = stLCRBatch.LCR_test_step;
	str.Format(L"%d",stStgBatch[batch].step);
	//G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LCR_INSPECT_STEP],str); 		
}

//Part Inspection Data Shift in SHUTTLE
int CInspectThread::m_fnShiftPartDataInConveyor()
{
	int i;

	memcpy((void *)&m_PartData[OUT_FLIPDROP], (void *)&m_PartData[IN_BUFFER], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[IN_BUFFER], (void *)&m_PartDataBuffer[6], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[9], (void *)&m_PartDataBuffer[8], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[8], (void *)&m_PartDataBuffer[7], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[7], (void *)&m_PartDataBuffer[6], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[6], (void *)&m_PartData[IN_VI1], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[IN_VI1], (void *)&m_PartData[IN_VI0], sizeof(PART_DATA));
	//memcpy((void *)&m_PartData[IN_VI1], (void *)&m_PartDataBuffer[3], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[5], (void *)&m_PartDataBuffer[4], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[4], (void *)&m_PartDataBuffer[3], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[3], (void *)&m_PartData[IN_CENTERING], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[IN_VI0], (void *)&m_PartData[IN_CENTERING], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[IN_CENTERING], (void *)&m_PartDataBuffer[0], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[2], (void *)&m_PartDataBuffer[1], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[1], (void *)&m_PartDataBuffer[0], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[0], (void *)&m_PartData[INPUT], sizeof(PART_DATA));
	memset((void *)&m_PartData[INPUT], 0x00, sizeof(PART_DATA));

	for (i = 0; i<MAX_VI_TEST; i++)
	{
		m_PartDataVecDefects[OUT_FLIPDROP][i] = move(m_PartDataVecDefects[IN_BUFFER][i]);
		m_PartDataVecDefects[IN_BUFFER][i] = move(m_PartDataBufferVecDefects[6][i]);
		//m_PartDataBufferVecDefects[9][i] = move(m_PartDataBufferVecDefects[8][i]);
		//m_PartDataBufferVecDefects[8][i] = move(m_PartDataBufferVecDefects[7][i]);
		//m_PartDataBufferVecDefects[7][i] = move(m_PartDataBufferVecDefects[6][i]);
		m_PartDataBufferVecDefects[6][i] = move(m_PartDataVecDefects[IN_VI1][i]);
		m_PartDataVecDefects[IN_VI1][i] = move(m_PartDataVecDefects[IN_VI0][i]);
		//m_PartDataVecDefects[IN_VI1][i] = move(m_PartDataBufferVecDefects[3][i]);
		//m_PartDataBufferVecDefects[5][i] = move(m_PartDataBufferVecDefects[4][i]);
		//m_PartDataBufferVecDefects[4][i] = move(m_PartDataBufferVecDefects[3][i]);
		//m_PartDataBufferVecDefects[3][i] = move(m_PartDataVecDefects[IN_CENTERING][i]);
		m_PartDataVecDefects[IN_VI0][i] = move(m_PartDataVecDefects[IN_CENTERING][i]);
		m_PartDataVecDefects[IN_CENTERING][i] = move(m_PartDataBufferVecDefects[0][i]);
		//m_PartDataBufferVecDefects[2][i] = move(m_PartDataBufferVecDefects[1][i]);
		//m_PartDataBufferVecDefects[1][i] = move(m_PartDataBufferVecDefects[0][i]);
		m_PartDataBufferVecDefects[0][i] = move(m_PartDataVecDefects[INPUT][i]);
		m_PartDataVecDefects[INPUT][i].clear();
	}

	return 0;
}

int CInspectThread::m_fnShiftPartDataOutConveyor()
{
	int i;

	memcpy((void *)&m_PartData[OUT_NGOUT], (void *)&m_PartDataBuffer[18], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[19], (void *)&m_PartDataBuffer[18], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[18], (void *)&m_PartDataBuffer[17], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[18], (void *)&m_PartData[OUT_VI3], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[OUT_VI3], (void *)&m_PartDataBuffer[17], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[17], (void *)&m_PartDataBuffer[16], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[16], (void *)&m_PartDataBuffer[15], sizeof(PART_DATA));
	//memcpy((void *)&m_PartData[OUT_VI2], (void *)&m_PartDataBuffer[15], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[15], (void *)&m_PartData[OUT_VI2], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[OUT_VI2], (void *)&m_PartDataBuffer[14], sizeof(PART_DATA));
	//memcpy((void *)&m_PartDataBuffer[15], (void *)&m_PartDataBuffer[14], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[14], (void *)&m_PartData[OUT_CENTERING], sizeof(PART_DATA));
	memcpy((void *)&m_PartData[OUT_CENTERING], (void *)&m_PartDataBuffer[13], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[13], (void *)&m_PartDataBuffer[12], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[12], (void *)&m_PartDataBuffer[11], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[11], (void *)&m_PartDataBuffer[10], sizeof(PART_DATA));
	memcpy((void *)&m_PartDataBuffer[10], (void *)&m_PartData[OUT_FLIPDROP], sizeof(PART_DATA));
	memset((void *)&m_PartData[OUT_FLIPDROP], 0x00, sizeof(PART_DATA));

	for (i = 0; i<MAX_VI_TEST; i++)
	{
		m_PartDataVecDefects[OUT_NGOUT][i] = move(m_PartDataBufferVecDefects[18][i]);
		//m_PartDataBufferVecDefects[19][i] = move(m_PartDataBufferVecDefects[18][i]);
//		m_PartDataBufferVecDefects[18][i] = move(m_PartDataBufferVecDefects[17][i]);
		m_PartDataBufferVecDefects[18][i] = move(m_PartDataVecDefects[OUT_VI3][i]);
		//m_PartDataBufferVecDefects[17][i] = move(m_PartDataBufferVecDefects[16][i]);
		m_PartDataVecDefects[OUT_VI3][i] = move(m_PartDataBufferVecDefects[17][i]);
		//m_PartDataBufferVecDefects[16][i] = move(m_PartDataVecDefects[OUT_VI2][i]);
		m_PartDataBufferVecDefects[17][i] = move(m_PartDataBufferVecDefects[16][i]);
		m_PartDataBufferVecDefects[16][i] = move(m_PartDataBufferVecDefects[15][i]);
		m_PartDataBufferVecDefects[15][i] = move(m_PartDataVecDefects[OUT_VI2][i]);
		m_PartDataVecDefects[OUT_VI2][i] = move(m_PartDataBufferVecDefects[14][i]);
		//m_PartDataBufferVecDefects[15][i] = move(m_PartDataBufferVecDefects[14][i]);
		m_PartDataBufferVecDefects[14][i] = move(m_PartDataVecDefects[OUT_CENTERING][i]);
		m_PartDataVecDefects[OUT_CENTERING][i] = move(m_PartDataBufferVecDefects[13][i]);
		m_PartDataBufferVecDefects[13][i] = move(m_PartDataBufferVecDefects[12][i]);
		m_PartDataBufferVecDefects[12][i] = move(m_PartDataBufferVecDefects[11][i]);
		m_PartDataBufferVecDefects[11][i] = move(m_PartDataBufferVecDefects[10][i]);
		m_PartDataBufferVecDefects[10][i] = move(m_PartDataVecDefects[OUT_FLIPDROP][i]);
		m_PartDataVecDefects[OUT_FLIPDROP][i].clear();
	}

	return 0;
}

int CInspectThread::m_fnShiftPartDataTest()
{
	int i,j,k;

	for ( i = 0; i < 10000; i++)
	{

		for (k = 0; k < MAX_VI_TEST; k++)
		{
			m_PartDataVecDefects[OUT_NGOUT][k].clear();
		}

		m_fnShiftPartDataOutConveyor();
		m_fnShiftPartDataInConveyor();

		memset((void *)&m_PartDataBuffer[0], 0x00, sizeof(PART_DATA));

		for ( j = 0; j < 4; j++)
		{
			VI_DEFECT def;

			def.type = NG_TYP_TEST;
			def.surface = NG_SURF_OUT;
			def.coords[VI_LEFT] = 0;
			def.coords[VI_TOP] = 0;
			def.coords[VI_RIGHT] = 1;
			def.coords[VI_BOTTOM] = 1;
			def.size = 1;
			m_PartDataVecDefects[INPUT][j].push_back(def);
			m_PartData[INPUT].results[j] = 1;

		}

	}

	for (j = 0; i < TOTAL_STAGE; i++)
	{
		for (i = 0; i < MAX_VI_TEST; i++)
		{
			m_PartDataVecDefects[j][i].clear();
		}
	}

	for (j = 0; i < TOTAL_BUFFER_SIZE; i++)
	{
		for (i = 0; i < MAX_VI_TEST; i++)
		{
			m_PartDataBufferVecDefects[j][i].clear();
		}
	}


	return 0;

}



int CInspectThread::m_fnClearPartData(int batch)
{
	memset((void *)&m_PartData[batch], 0x00, sizeof(PART_DATA));
	//m_PartData[batch].Reset();
	return 0;
}

int CInspectThread::m_fnClearPartDataAll(void)
{
	memset((void *)m_PartData, 0x00, sizeof(m_PartData)*NO_STAGE);
	return 0;
}

int CInspectThread::m_fnInitPartData(int batch, int model_no, int part_no)
{
	m_PartData[batch].Reset();
	//m_PartData[batch].model_no = model_no;
	//m_PartData[batch].part_no = part_no;
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////
//AUTO START

/////////////////////////////////////////////////////////////////////////////////////////////////////

int CInspectThread::stg0Input(void){

	int iRet;
	CString str;

	switch (m_fnGetBatchStep(INPUT))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(INPUT) != STEP0)
		{
			m_fnSetBatchStep(INPUT, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_fnGetPrevBatchStep(INPUT) != STEP1)
		{
			m_fnAllowConveyor(IN_CNVYR, FALSE, INPUT);
			m_fnSetBatchStep(INPUT, STEP1);
		}
		else
		{
			if (getBatchStatus(IN_CNVYR) != RUN){

				if (m_pPIO->readDI(OUT_LOAD_STOP, CARD_IO) == 1 && m_pPIO->readDI(IN_LOAD_DETECT, CARD_IO) == 1)
				{
					m_PartData[INPUT].part = PART;
					m_PartData[INPUT].id = PartID++;
					G_MainWnd->m_PIODlg.partPassing = 0;

					stStgBatch[INPUT].retry = 0;
					m_fnSetBatchStep(INPUT, STEP2);
				}
				else
					m_fnSetBatchStep(INPUT, STEP14);

			}
		}
		break;

	case STEP2: //stopper open
		if (m_fnGetPrevBatchStep(INPUT) != STEP2)
		{
			iRet = m_pPIO->writeDO(1, OUT_LOAD_OPEN, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(INPUT, STEP2);
			}
			else{
				if (stStgBatch[INPUT].retry++ > BATCH_RETRY){
					stStgBatch[INPUT].retry = 0;
					str.Format(L"IO Write Error: Address: LOAD STEP2: OUT_LOAD_OPEN");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(INPUT, STEP19); break;
				}
			}
		}
		else
		{
#ifndef VIRTUAL_RUN
			if (m_pPIO->readDI(IN_LOAD_OPEN, CARD_IO) == 1)
#else
			if (1)
#endif
			{
				if (!stStgBatch[INPUT].timer.On){
					stStgBatch[INPUT].retry = 0;
					stStgBatch[INPUT].timer.On = 1;
					stStgBatch[INPUT].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[INPUT].timer.OTE == 1)
					{
						stStgBatch[INPUT].timer.clear();
						stStgBatch[INPUT].counter.clear();
						m_fnSetBatchStep(INPUT, STEP14);
					}
				}
			}
			else{
				if (!stStgBatch[INPUT].counter.On){
					stStgBatch[INPUT].counter.On = 1;
					stStgBatch[INPUT].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[INPUT].counter.OTE == 1)
					{
						stStgBatch[INPUT].timer.clear();
						stStgBatch[INPUT].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 00);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 00);
						m_fnSetBatchStep(INPUT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;


	case STEP4: //DETECT PART PASSING
		if (m_fnGetPrevBatchStep(INPUT) != STEP4)
		{
			////EDGE DETECT
			//if (m_pPIO->partPassing == 1)
			//{
			//	m_fnSetBatchStep(INPUT, STEP4);
			//}
			//Wait till PIO close stopper and put you in step6

		}
		else
		{

		}
		break;

	case STEP5: //STOPPER CLOSE
		if (m_fnGetPrevBatchStep(INPUT) != STEP5)
		{
			if (!m_pPIO->readDI(IN_LOAD_STOP, CARD_IO))
			{
				iRet = m_pPIO->writeDO(1, OUT_LOAD_STOP, CARD_IO);
			}
			if (iRet != TMC_RV_OK){
				str.Format(L"IO Write Error: Address: LOAD STEP3: OUT_LOAD_STOP");
				G_AddLog(3, str.GetBuffer());
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				m_fnSetBatchStep(INPUT, STEP19); break;
			}
			m_fnSetBatchStep(INPUT, STEP5);
		}
		else
		{
			//if (m_pPIO->readDI(IN_LOAD_STOP, CARD_IO) == 1 )//CLOSE
			if (m_pPIO->readDI(IN_LOAD_OPEN, CARD_IO) == 0)//CLOSE
			{
				if (!stStgBatch[INPUT].timer.On){
					stStgBatch[INPUT].timer.On = 1;
					stStgBatch[INPUT].timer.Set = T_WAIT; //
				}
				else{
					if (stStgBatch[INPUT].timer.OTE == 1)
					{
						stStgBatch[INPUT].timer.clear();
						stStgBatch[INPUT].counter.clear();
						m_fnSetBatchStep(INPUT, STEP6);
					}
				}
			}
			else{
				if (!stStgBatch[INPUT].counter.On){
					stStgBatch[INPUT].counter.On = 1;
					stStgBatch[INPUT].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[INPUT].counter.OTE == 1)
					{
						stStgBatch[INPUT].timer.clear();
						stStgBatch[INPUT].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 01);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 01);
						m_fnSetBatchStep(INPUT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(INPUT, STEP16);
		else
			m_fnSetBatchStep(INPUT, STEP15);
		break;

	case STEP15: //IN_CENTERING MANUAL END
		setBatchStatus(INPUT, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, INPUT);
		m_fnSetBatchStep(INPUT, STEP0); //
		break;

	case STEP16: //IN_CENTERING AUTO END

		//m_PartData[INPUT].part = PART;
		//m_PartData[INPUT].id = PartID++;
		//G_MainWnd->m_PIODlg.partPassing = 0;

		setBatchStatus(INPUT, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, INPUT);
		m_fnRequestConveyor(IN_CNVYR, TRUE, INPUT);
		m_fnSetBatchStep(INPUT, STEP17); //
		break;
	case STEP17: //IN_CENTERING WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[IN_CNVYR].request == 0)
		{
			m_fnSetBatchStep(INPUT, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(INPUT) != STEP19)
		{
			m_fnSetOldBatchStep(INPUT, m_fnGetPrevBatchStep(INPUT));
			m_fnSetBatchStep(INPUT, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[INPUT].SetColorChange(RED, RGB(0, 0, 0));
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();

		//error reset
		m_PartData[INPUT].part = 0;
		G_MainWnd->m_PIODlg.partPassing = 0;
		m_fnSetBatchStep(INPUT, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(INPUT))

	return 0;

}

int CInspectThread::stg1Center(void){

	CString str;
	int iRet;

	switch (m_fnGetBatchStep(IN_CENTERING))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(IN_CENTERING) != STEP0)
		{
			m_fnSetBatchStep(IN_CENTERING, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[IN_CENTERING].part == PART 
#ifdef _VI_ALIGN
			|| m_PartData[IN_VI1].part == PART 
			|| m_PartDataBuffer[0].part == PART
			|| m_PartDataBuffer[3].part == PART
			|| m_PartData[IN_VI0].part == PART
#endif
			) //PART OR CLEAR
		{
			//stStgBatch[IN_CNVYR].allow &= ~CNVYR_IN_CTR;
			m_fnAllowConveyor(IN_CNVYR, FALSE, IN_CENTERING);
			m_fnSetBatchStep(IN_CENTERING, STEP3);
			stStgBatch[IN_CENTERING].retry = 0;
		}
		else{
			m_fnSetBatchStep(IN_CENTERING, STEP14);
		}

		break;


	case STEP3: //CENTERING FWD
		if (m_fnGetPrevBatchStep(IN_CENTERING) != STEP3)
		{
			iRet = m_pPIO->writeDO(1, OUT_STG1_CTR_FWD, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(IN_CENTERING, STEP3);
			}
			else{
				if (stStgBatch[IN_CENTERING].retry++ > BATCH_RETRY){
					stStgBatch[IN_CENTERING].retry = 0;
					str.Format(L"IO Write Error: Address: IN_CENTERING STEP2: OUT_STG1_CTR_FWD");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(IN_CENTERING, STEP19); break;
				}
			}
			//
		}
		else
		{
			if (m_pPIO->readDI(IN_STG1_CTR_FWD, CARD_IO) == 1
				)
			{
				if (!stStgBatch[IN_CENTERING].timer.On){
					stStgBatch[IN_CENTERING].timer.On = 1;
					stStgBatch[IN_CENTERING].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[IN_CENTERING].timer.OTE == 1)
					{
						stStgBatch[IN_CENTERING].timer.clear();
						stStgBatch[IN_CENTERING].counter.clear();
						stStgBatch[IN_CENTERING].retry = 0;
						m_fnSetBatchStep(IN_CENTERING, STEP4);

					}
				}
			}
			else{
				if (!stStgBatch[IN_CENTERING].counter.On){
					stStgBatch[IN_CENTERING].counter.On = 1;
					stStgBatch[IN_CENTERING].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[IN_CENTERING].counter.OTE == 1)
					{
						stStgBatch[IN_CENTERING].timer.clear();
						stStgBatch[IN_CENTERING].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 10);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 10);
						m_fnSetBatchStep(IN_CENTERING, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;


	case STEP4: //CENTERING RET
		if (m_fnGetPrevBatchStep(IN_CENTERING) != STEP4)
		{
			iRet = m_pPIO->writeDO(1, OUT_STG1_CTR_RET, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(IN_CENTERING, STEP4);
				//setBatchStatus(IN_CENTERING, ALIGNED);
			}
			else{
				if (stStgBatch[IN_CENTERING].retry++ > BATCH_RETRY){
					stStgBatch[IN_CENTERING].retry = 0;
					str.Format(L"IO Write Error: Address: IN_CENTERING STEP4: OUT_STG1_CTR_RET");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(IN_CENTERING, STEP19); break;
				}
			}
			
		}
		else
		{
			if (m_pPIO->readDI(IN_STG1_CTR_RET, CARD_IO) == 1
				)
			{
				if (!stStgBatch[IN_CENTERING].timer.On){
					stStgBatch[IN_CENTERING].timer.On = 1;
					stStgBatch[IN_CENTERING].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[IN_CENTERING].timer.OTE == 1)
					{
						stStgBatch[IN_CENTERING].timer.clear();
						stStgBatch[IN_CENTERING].counter.clear();
						stStgBatch[IN_CENTERING].retry = 0;
						m_fnSetBatchStep(IN_CENTERING, STEP14);
					}
				}
			}
			else{
				if (!stStgBatch[IN_CENTERING].counter.On){
					stStgBatch[IN_CENTERING].counter.On = 1;
					stStgBatch[IN_CENTERING].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[IN_CENTERING].counter.OTE == 1)
					{
						stStgBatch[IN_CENTERING].timer.clear();
						stStgBatch[IN_CENTERING].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 11);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 11);
						m_fnSetBatchStep(IN_CENTERING, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;


	case STEP14: //
#ifdef _VI_ALIGN
		if(m_PartData[IN_VI1].part == PART)
			m_fnSetBatchStep(IN_VI1, STEP1);
		if (m_PartData[IN_VI0].part == PART)
			m_fnSetBatchStep(IN_VI0, STEP1);
#endif
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(IN_CENTERING, STEP16);
		else
			m_fnSetBatchStep(IN_CENTERING, STEP15);
		break;

	case STEP15: //IN_CENTERING MANUAL END
		setBatchStatus(IN_CENTERING, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_CENTERING);
		m_fnSetBatchStep(IN_CENTERING, STEP0); //
		break;

	case STEP16: //IN_CENTERING AUTO END
		setBatchStatus(IN_CENTERING, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_CENTERING);
		m_fnRequestConveyor(IN_CNVYR, TRUE, IN_CENTERING);
		m_fnSetBatchStep(IN_CENTERING, STEP17); //
		break;
	case STEP17: //IN_CENTERING WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[IN_CNVYR].request == 0)
		{
			m_fnSetBatchStep(IN_CENTERING, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(IN_CENTERING) != STEP19)
		{
			m_fnSetOldBatchStep(IN_CENTERING, m_fnGetPrevBatchStep(IN_CENTERING));
			m_fnSetBatchStep(IN_CENTERING, STEP19);
			errorRoutine();
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		if (m_pPIO->readDI(IN_STG1_CTR_FWD, CARD_IO) == 1)
		{
			iRet = m_pPIO->writeDO(1, OUT_STG1_CTR_RET, CARD_IO);
		}
		m_fnSetBatchStep(IN_CENTERING, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(IN_CENTERING))

	return 0;

}

////////////////////
//INNER//PCHI
//20160307_KYS_ADD
int CInspectThread::stg11VI0(void){

	CString str;

	switch (m_fnGetBatchStep(IN_VI0))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(IN_VI0) != STEP0)
		{
			m_fnSetBatchStep(IN_VI0, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[IN_VI0].part == PART) //PART OR CLEAR
		{
			//stStgBatch[IN_CNVYR].allow &= ~CNVYR_IN_VI0;
			//m_vi.m_LiveHandls[CAM_PCHI].flagCapture[CAM_PCHI] = TRUE;

			m_fnAllowConveyor(IN_CNVYR, FALSE, IN_VI0);
			m_fnSetBatchStep(IN_VI0, STEP3);
			stStgBatch[IN_VI0].retry = 0;
			//////LIGHT_ON_20160325_KYS
			//turnLight(LIGHT_CAM1_1, ON);
		}
		else{
			m_fnSetBatchStep(IN_VI0, STEP14);
		}

		break;

	case STEP3: //VI
		if (m_fnGetPrevBatchStep(IN_VI0) != STEP3)
		{
			if (m_PartData[IN_VI0].part == PART)
			{
				if (m_vi.flagThreadsVI[CAM_PCHI] == TH_VI_CLEAR){
					m_fnSetBatchStep(IN_VI0, STEP3);
					stStgBatch[IN_VI0].retry = 0;
					//G_AddLog(3, L"PostMessage Grab UW_VI_1 [SEND] ");
					::PostMessage(m_vi.m_hWnd, UM_VI, (WPARAM)UW_VI_1, m_PartData[IN_VI0].id); //STG11 VISION INSPECTION REQUEST
					if (!stStgBatch[IN_VI0].timer.On){
						stStgBatch[IN_VI0].timer.On = 1;
						stStgBatch[IN_VI0].timer.Set = T_WAIT_TIMEOUT_2X;//Time out
					}
				}

				//THREAD DEAD?
				if (!stStgBatch[IN_VI0].counter.On){
					stStgBatch[IN_VI0].counter.On = 1;
					stStgBatch[IN_VI0].counter.Set = T_WAIT_TIMEOUT_2X * 2;//Time out
				}
				else{
					if (stStgBatch[IN_VI0].counter.OTE == 1)//TIME OUT
					{
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50);
						stStgBatch[IN_VI0].counter.clear();
						m_fnSetBatchStep(IN_VI0, STEP19);
					}
				}
			}
			else{
				m_fnSetBatchStep(IN_VI0, STEP14);
			}
		}
		else
		{
			if (m_vi.GetUMMsgResponse(UW_VI_1) == UM_VI_GRABBED /* || m_vi.GetUMMsgResponse(UW_VI_4) == UM_VI_DONE */) //IMAGE GRABBED
			{//
				//////LIGHT_ON_20160325_KYS
				//turnLight(LIGHT_CAM1_1, OFF);
				//G_AddLog(3, L"PostMessage Grab UW_VI_1 [GRAB] "); 

				stStgBatch[IN_VI0].timer.clear();
				stStgBatch[IN_VI0].counter.clear();
				m_vi.SetUMMsgResponse(UW_VI_1, UM_VI_CLEAR);
				m_fnSetBatchStep(IN_VI0, STEP14);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_1) == UM_VI_ERROR){//ERROR
				//Error
				stStgBatch[IN_VI0].timer.clear();
				stStgBatch[IN_VI0].counter.clear();
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 61);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 61);
				m_fnSetBatchStep(IN_VI0, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_1, UM_VI_CLEAR);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_1) == UM_VI_GRAB_ERROR){//GRAB ERROR //RETRY 
				//Error
				if (stStgBatch[IN_VI0].retry++ > BATCH_RETRY){
					stStgBatch[IN_VI0].timer.clear();
					stStgBatch[IN_VI0].counter.clear();
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 56);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 56);
					m_fnSetBatchStep(IN_VI0, STEP19);
					m_vi.SetUMMsgResponse(UW_VI_1, UM_VI_CLEAR);
				}
				else{
					//retry
					m_fnSetPrevBatchStep(IN_VI0, STEP1);
				}
			}

			if (stStgBatch[IN_VI0].timer.OTE == 1)//TIME OUT
			{
				stStgBatch[IN_VI0].timer.clear();
				stStgBatch[IN_VI0].counter.clear();
				//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 54);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 54);
				m_fnSetBatchStep(IN_VI0, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_1, UM_VI_CLEAR);
			}
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(IN_VI0, STEP16);
		else
			m_fnSetBatchStep(IN_VI0, STEP15);
		break;

	case STEP15: //IN_VI0 MANUAL END
		setBatchStatus(IN_VI0, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_VI0);
		m_fnSetBatchStep(IN_VI0, STEP0); //
		break;

	case STEP16: //IN_VI0 AUTO END
		setBatchStatus(IN_VI0, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_VI0);
		m_fnRequestConveyor(IN_CNVYR, TRUE, IN_VI0);
		m_fnSetBatchStep(IN_VI0, STEP17); //
		////LIGHT_ON_20160325_KYS
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM1_1, OFF);
		////
		break;
	case STEP17: //IN_VI0 WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[IN_CNVYR].request == 0)
		{
			m_fnSetBatchStep(IN_VI0, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(IN_VI0) != STEP19)
		{
			m_fnSetOldBatchStep(IN_VI0, m_fnGetPrevBatchStep(IN_VI0));
			m_fnSetBatchStep(IN_VI0, STEP19);
			errorRoutine();
			////LIGHT_ON_20160325_KYS
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM1_1, OFF);
			////
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		m_fnSetBatchStep(IN_VI0, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(IN_VI0))

	return 0;

}
////////////////////////////////////////////////
//UPPER//PCCA
//20160307_KYS_ADD

int CInspectThread::stg2VI1(void){

	CString str;

	switch (m_fnGetBatchStep(IN_VI1))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(IN_VI1) != STEP0)
		{
			m_fnSetBatchStep(IN_VI1, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[IN_VI1].part == PART) //PART OR CLEAR
		{
			//m_vi.m_LiveHandls[CAM_PCCA].flagCapture[CAM_PCCA] = TRUE;
			//stStgBatch[IN_CNVYR].allow &= ~CNVYR_IN_VI1;
			m_fnAllowConveyor(IN_CNVYR, FALSE, IN_VI1);
			m_fnSetBatchStep(IN_VI1, STEP3);
			stStgBatch[IN_VI1].retry = 0;
			//////LIGHT_ON_20160325_KYS
			//turnLight(LIGHT_CAM2_1, ON);
			//turnLight(LIGHT_CAM2_2, ON);
		}
		else{
			m_fnSetBatchStep(IN_VI1, STEP14);
		}

		break;

	case STEP3: //VI
		if (m_fnGetPrevBatchStep(IN_VI1) != STEP3)
		{
			if (m_PartData[IN_VI1].part == PART)
			{
				if (m_vi.flagThreadsVI[CAM_PCCA] == TH_VI_CLEAR){
					m_fnSetBatchStep(IN_VI1, STEP3);
					stStgBatch[IN_VI1].retry = 0;
					//G_AddLog(3, L"PostMessage Grab UW_VI_2 [SEND] ");
					::PostMessage(m_vi.m_hWnd, UM_VI, (WPARAM)UW_VI_2, m_PartData[IN_VI1].id); //STG1 VISION INSPECTION REQUEST
					if (!stStgBatch[IN_VI1].timer.On){
						stStgBatch[IN_VI1].timer.On = 1;
						stStgBatch[IN_VI1].timer.Set = T_WAIT_TIMEOUT_2X;//Time out
					}
				}

				//THREAD DEAD?
				if (!stStgBatch[IN_VI1].counter.On){
					stStgBatch[IN_VI1].counter.On = 1;
					stStgBatch[IN_VI1].counter.Set = T_WAIT_TIMEOUT_2X*2;//Time out
				}
				else{
					if (stStgBatch[IN_VI1].counter.OTE == 1)//TIME OUT
					{
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50);
						stStgBatch[IN_VI1].counter.clear();
						m_fnSetBatchStep(IN_VI1, STEP19);
					}
				}
			}
			else{
				m_fnSetBatchStep(IN_VI1, STEP14);
			}
		}
		else
		{
			if (m_vi.GetUMMsgResponse(UW_VI_2) == UM_VI_GRABBED /* || m_vi.GetUMMsgResponse(UW_VI_1) == UM_VI_DONE */) //IMAGE GRABBED
			{//
				//////LIGHT_ON_20160325_KYS
				turnLight(LIGHT_CAM2_1, OFF);
//				turnLight(LIGHT_CAM2_2, OFF);
				//G_AddLog(3, L"PostMessage Grab UW_VI_2 [GRAB] ");

				stStgBatch[IN_VI1].timer.clear();
				stStgBatch[IN_VI1].counter.clear();
				m_vi.SetUMMsgResponse(UW_VI_2, UM_VI_CLEAR);
				m_fnSetBatchStep(IN_VI1, STEP14);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_2) == UM_VI_ERROR){//ERROR
				//Error
				stStgBatch[IN_VI1].timer.clear();
				stStgBatch[IN_VI1].counter.clear();
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 61);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 61);
				m_fnSetBatchStep(IN_VI1, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_2, UM_VI_CLEAR);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_2) == UM_VI_GRAB_ERROR){//GRAB ERROR //RETRY 
				//Error
				if (stStgBatch[IN_VI1].retry++ > BATCH_RETRY){
					stStgBatch[IN_VI1].timer.clear();
					stStgBatch[IN_VI1].counter.clear();
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 56);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 56);
					m_fnSetBatchStep(IN_VI1, STEP19);
					m_vi.SetUMMsgResponse(UW_VI_2, UM_VI_CLEAR);
				}
				else{
					//retry
					m_fnSetPrevBatchStep(IN_VI1, STEP1);
				}
			}

			if (stStgBatch[IN_VI1].timer.OTE == 1)//TIME OUT
			{
				stStgBatch[IN_VI1].timer.clear();
				stStgBatch[IN_VI1].counter.clear();
				//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 54);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 54);
				m_fnSetBatchStep(IN_VI1, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_2, UM_VI_CLEAR);
			}
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(IN_VI1, STEP16);
		else
			m_fnSetBatchStep(IN_VI1, STEP15);
		break;

	case STEP15: //IN_VI1 MANUAL END
		setBatchStatus(IN_VI1, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_VI1);
		m_fnSetBatchStep(IN_VI1, STEP0); //
		break;

	case STEP16: //IN_VI1 AUTO END
		setBatchStatus(IN_VI1, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_VI1);
		m_fnRequestConveyor(IN_CNVYR, TRUE, IN_VI1);
		m_fnSetBatchStep(IN_VI1, STEP17); //
		//LIGHT_ON_20160325_KYS
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_1, OFF);
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_2, OFF);
		//
		break;
	case STEP17: //IN_VI1 WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[IN_CNVYR].request == 0)
		{
			m_fnSetBatchStep(IN_VI1, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(IN_VI1) != STEP19)
		{
			m_fnSetOldBatchStep(IN_VI1, m_fnGetPrevBatchStep(IN_VI1));
			m_fnSetBatchStep(IN_VI1, STEP19);
			errorRoutine();
			//LIGHT_ON_20160325_KYS
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_1, OFF);
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM2_2, OFF);
			//
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		m_fnSetBatchStep(IN_VI1, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(IN_VI1))

	return 0;

}

int CInspectThread::stg3Buffer(void){

	CString str;

	switch (m_fnGetBatchStep(IN_BUFFER))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(IN_BUFFER) != STEP0)
		{
			m_fnSetBatchStep(IN_BUFFER, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		//stStgBatch[IN_CNVYR].allow &= ~IN_BUFFER;
		m_fnAllowConveyor(IN_CNVYR, FALSE, IN_BUFFER);
		if (m_PartData[IN_BUFFER].part == PART){
			m_fnSetBatchStep(IN_BUFFER, STEP2);
		}
		else{
			m_fnSetBatchStep(IN_BUFFER, STEP14);
		}
		break;

	case STEP2: //get VI1 result
		if (m_fnGetPrevBatchStep(IN_BUFFER) != STEP2)
		{
			stStgBatch[IN_BUFFER].timer.On = 1;
			stStgBatch[IN_BUFFER].timer.Set = T_WAIT_TIMEOUT;
			m_PartDataVecDefects[IN_BUFFER][CAM_PCHI].clear();
			m_fnSetBatchStep(IN_BUFFER, STEP2);
		}
		else
		{
			if (!m_vi.queViResults[CAM_PCHI].empty())
			{
				//
				for (int i = 0; i < m_vi.queViResults[CAM_PCHI].size(); i++) {
					VI_RESULT &vr = m_vi.queViResults[CAM_PCHI][i];
					bool bRemove = false;

					if (vr.id == m_PartData[IN_BUFFER].id){
						m_PartData[IN_BUFFER].results[CAM_PCHI] = vr.result;
						m_PartDataVecDefects[IN_BUFFER][CAM_PCHI] = move(*vr.vecDefects);
						vr.vecDefects->clear();
						delete vr.vecDefects;

						stStgBatch[IN_BUFFER].timer.clear();
						stStgBatch[IN_BUFFER].partID = vr.id;
						m_fnSetBatchStep(IN_BUFFER, STEP3);

						bRemove = true;
					}

					else if (vr.id < m_PartData[IN_BUFFER].id) {//get next one
						//vr.vecDefects.clear();
#ifdef DEV_MODE
						G_AddLog(3, L"step 2 step 2 stStgBatch[IN_BUFFER].partID:%d,  vr.id %d < m_PartData[IN_BUFFER].id %d", stStgBatch[IN_BUFFER].partID, vr.id, m_PartData[IN_BUFFER].id);
#endif
						bRemove = true;
					}


/*				else if (vr.id > m_PartData[IN_BUFFER].id){//skipped
										m_PartData[IN_BUFFER].results[CAM_PCHI] = VI_NG;
										stStgBatch[IN_BUFFER].timer.clear();
										#ifdef DEV_MODE
										G_AddLog(3, L"step 2 stStgBatch[IN_BUFFER].partID:%d, vr.id %d > m_PartData[IN_BUFFER].id %d", stStgBatch[IN_BUFFER].partID, vr.id, m_PartData[IN_BUFFER].id);
										#endif
										m_fnSetBatchStep(IN_BUFFER, STEP3);
										}
*/
					if (bRemove) {
						auto it = find_if(m_vi.queViResults[CAM_PCHI].begin(), m_vi.queViResults[CAM_PCHI].end(), [&vr](VI_RESULT &vr2) { return vr.id == vr2.id; });
						if (it != m_vi.queViResults[CAM_PCHI].end()) {
							m_vi.csResult[CAM_PCHI].Lock();
							m_vi.queViResults[CAM_PCHI].erase(it);
							m_vi.csResult[CAM_PCHI].Unlock();
						}
						break;
					}
				}
			}

			if (stStgBatch[IN_BUFFER].timer.OTE == 1)
			{
				stStgBatch[IN_BUFFER].timer.clear();
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 60);
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 60);
				//m_fnSetBatchStep(IN_BUFFER, STEP19);
				G_AddLog(3, L" step 2 stStgBatch[IN_BUFFER].timeout [stStgBatch[IN_BUFFER].partID: %d] ", stStgBatch[IN_BUFFER].partID);
				//
				m_PartData[IN_BUFFER].results[CAM_PCHI] = VI_NG;
				m_PartData[IN_BUFFER].results[CAM_PCHI2] = VI_NG;
				m_fnSetBatchStep(IN_BUFFER, STEP14);
			}
		}
		break;

	case STEP3: //get VI1 result 2
		if (m_fnGetPrevBatchStep(IN_BUFFER) != STEP3)
		{
			stStgBatch[IN_BUFFER].timer.On = 1;
			stStgBatch[IN_BUFFER].timer.Set = T_WAIT_TIMEOUT;
			m_PartDataVecDefects[IN_BUFFER][CAM_PCCA].clear();
			m_fnSetBatchStep(IN_BUFFER, STEP3);
		}
		else
		{
			if (!m_vi.queViResults[CAM_PCCA].empty())
			{
				//
				for (int i = 0; i < m_vi.queViResults[CAM_PCCA].size(); i++) {
					VI_RESULT &vr = m_vi.queViResults[CAM_PCCA][i];
					bool bRemove = false;

					if (vr.id == m_PartData[IN_BUFFER].id){
						m_PartData[IN_BUFFER].results[CAM_PCCA] = vr.result;
						m_PartDataVecDefects[IN_BUFFER][CAM_PCCA] = move(*vr.vecDefects);
						vr.vecDefects->clear();
						delete vr.vecDefects;

						stStgBatch[IN_BUFFER].timer.clear();
						stStgBatch[IN_BUFFER].partID = vr.id;
						m_fnSetBatchStep(IN_BUFFER, STEP14);

						bRemove = true;
					}

					else if (vr.id < m_PartData[IN_BUFFER].id) {//get next one
						//vr.vecDefects.clear();
#ifdef DEV_MODE
						G_AddLog(3, L"step 3 stStgBatch[IN_BUFFER].partID2:%d, vr.id %d < m_PartData[IN_BUFFER].id %d", stStgBatch[IN_BUFFER].partID2, vr.id, m_PartData[IN_BUFFER].id);
#endif
						bRemove = true;
					}


					/*				else if (vr.id > m_PartData[IN_BUFFER].id){//skipped
					m_PartData[IN_BUFFER].results[CAM_PCHI] = VI_NG;
					stStgBatch[IN_BUFFER].timer.clear();
					#ifdef DEV_MODE
					G_AddLog(3, L"step 2 stStgBatch[IN_BUFFER].partID:%d, vr.id %d > m_PartData[IN_BUFFER].id %d", stStgBatch[IN_BUFFER].partID, vr.id, m_PartData[IN_BUFFER].id);
					#endif
					m_fnSetBatchStep(IN_BUFFER, STEP14);
					}
					*/
					if (bRemove) {
						auto it = std::find_if(m_vi.queViResults[CAM_PCCA].begin(), m_vi.queViResults[CAM_PCCA].end(), [&vr](VI_RESULT &vr2) { return vr.id == vr2.id; });
						if (it != m_vi.queViResults[CAM_PCCA].end()) {
							m_vi.csResult[CAM_PCCA].Lock();
							m_vi.queViResults[CAM_PCCA].erase(it);
							m_vi.csResult[CAM_PCCA].Unlock();
						}
						break;
					}
				}
			}

			if (stStgBatch[IN_BUFFER].timer.OTE == 1)
			{
				G_AddLog(3, L" step 3 stStgBatch[IN_BUFFER].timeout stStgBatch[IN_BUFFER].partID2: %d", stStgBatch[IN_BUFFER].partID2);
				stStgBatch[IN_BUFFER].timer.clear();
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 60);
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 60);
				//m_fnSetBatchStep(IN_BUFFER, STEP19);

				//
				m_PartData[IN_BUFFER].results[CAM_PCHI] = VI_NG;
				m_PartData[IN_BUFFER].results[CAM_PCCA] = VI_NG;
				m_fnSetBatchStep(IN_BUFFER, STEP14);
			}
		}
		break;

	case STEP14: //
#ifdef		VI_SQC
		//CString str;
		str.Format(L"IN_BUFFER id: %d  [ %d, %d ]", m_PartData[IN_BUFFER].id, m_PartData[IN_BUFFER].results[CAM_PCHI], m_PartData[IN_BUFFER].results[CAM_PCHI2]);
		G_AddLog(3, str.GetBuffer());
		str.ReleaseBuffer();
#endif

		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			m_fnSetBatchStep(IN_BUFFER, STEP16);
		}
		else{
			m_fnSetBatchStep(IN_BUFFER, STEP15);
		}
		break;

	case STEP15: //IN_CENTERING MANUAL END
		setBatchStatus(IN_BUFFER, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_BUFFER);
		m_fnSetBatchStep(IN_BUFFER, STEP0); //
		break;

	case STEP16: //IN_CENTERING AUTO END
		setBatchStatus(IN_BUFFER, FINISHED);
		m_fnAllowConveyor(IN_CNVYR, TRUE, IN_BUFFER);
		m_fnRequestConveyor(IN_CNVYR, TRUE, IN_BUFFER);
		m_fnSetBatchStep(IN_BUFFER, STEP17); //
		break;
	case STEP17: //WAIT SHUTTLE END
		if (stStgBatch[IN_CNVYR].request == 0)
		{
			m_fnSetBatchStep(IN_BUFFER, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(IN_BUFFER) != STEP19)
		{
			m_fnSetOldBatchStep(IN_BUFFER, m_fnGetPrevBatchStep(IN_BUFFER));
			m_fnSetBatchStep(IN_BUFFER, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[IN_BUFFER].SetColorChange(RED, RGB(0, 0, 0));
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();

		m_fnSetBatchStep(IN_BUFFER, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(IN_BUFFER))

	return 0;

}

int CInspectThread::stg4Flip(void){

	CString str;

	switch (m_fnGetBatchStep(OUT_FLIPDROP))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_FLIPDROP) != STEP0)
		{
			m_fnSetBatchStep(OUT_FLIPDROP, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		//stStgBatch[IN_CNVYR].allow &= ~OUT_FLIPDROP;
		m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_FLIPDROP);
		if (m_PartData[OUT_FLIPDROP].part == PART){
			m_fnSetBatchStep(OUT_FLIPDROP, STEP14);
		}
		else{
			m_fnSetBatchStep(OUT_FLIPDROP, STEP15);
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			if (m_PartData[OUT_FLIPDROP].part == PART){
				m_fnSetBatchStep(OUT_FLIPDROP, STEP16);
			}
			else{
				m_fnSetBatchStep(OUT_FLIPDROP, STEP15);
			}
		}
		else
			m_fnSetBatchStep(OUT_FLIPDROP, STEP15);
		break;

	case STEP15: //IN_CENTERING MANUAL END
		setBatchStatus(OUT_FLIPDROP, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_FLIPDROP);
		m_fnSetBatchStep(OUT_FLIPDROP, STEP0); //
		break;

	case STEP16: //IN_CENTERING AUTO END
		setBatchStatus(OUT_FLIPDROP, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_FLIPDROP);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_FLIPDROP);
		m_fnSetBatchStep(OUT_FLIPDROP, STEP17); //
		break;
	case STEP17: //WAIT SHUTTLE END
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_FLIPDROP, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_FLIPDROP) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_FLIPDROP, m_fnGetPrevBatchStep(OUT_FLIPDROP));
			m_fnSetBatchStep(OUT_FLIPDROP, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[OUT_FLIPDROP].SetColorChange(RED, RGB(0, 0, 0));
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();

		m_fnSetBatchStep(OUT_FLIPDROP, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_FLIPDROP))

	return 0;

}

int CInspectThread::stg5Stopper(void){


	int iRet;
	CString str;

	switch (m_fnGetBatchStep(OUT_STOPPER))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_STOPPER) != STEP0)
		{
			m_fnSetBatchStep(OUT_STOPPER, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[OUT_STOPPER].part == PART){
			m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_STOPPER);
			stStgBatch[OUT_STOPPER].retry = 0;
			m_fnSetBatchStep(OUT_STOPPER, STEP2);
		}
		else{
			m_fnSetBatchStep(OUT_STOPPER, STEP15);
			//m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_STOPPER);
			//m_fnSetBatchStep(OUT_STOPPER, STEP2);
		}
		break;


	case STEP2: //stopper open
		if (m_fnGetPrevBatchStep(OUT_STOPPER) != STEP2)
		{
			//OPEN STG4 STOPPER
			iRet = m_pPIO->writeDO(1, OUT_STG4_STOP_OPEN, CARD_IO);
			if (iRet == TMC_RV_OK){
				stStgBatch[OUT_STOPPER].retry = 0;
				m_fnSetBatchStep(OUT_STOPPER, STEP2);
			}
			else{
				if (stStgBatch[OUT_STOPPER].retry++ > BATCH_RETRY){
					stStgBatch[OUT_STOPPER].retry = 0;
					str.Format(L"IO Write Error: Address: OUT_STOPPER STEP2: OUT_STG4_STOP_OPEN");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(OUT_STOPPER, STEP19); 
					break;
				}
			}
		}
		else
		{
			if (m_pPIO->readDI(IN_STG4_STOP_OPEN, CARD_IO) == 1)
			{
				if (!stStgBatch[OUT_STOPPER].timer.On){
					stStgBatch[OUT_STOPPER].timer.On = 1;
					stStgBatch[OUT_STOPPER].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[OUT_STOPPER].timer.OTE == 1)
					{
						stStgBatch[OUT_STOPPER].timer.clear();
						stStgBatch[OUT_STOPPER].counter.clear();
						m_fnSetBatchStep(OUT_STOPPER, STEP14);
					}
				}
			}
			else{
				if (!stStgBatch[OUT_STOPPER].counter.On){
					stStgBatch[OUT_STOPPER].counter.On = 1;
					stStgBatch[OUT_STOPPER].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[OUT_STOPPER].counter.OTE == 1)
					{
						stStgBatch[OUT_STOPPER].timer.clear();
						stStgBatch[OUT_STOPPER].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 15);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 15);
						m_fnSetBatchStep(OUT_STOPPER, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(OUT_STOPPER, STEP16);
		else
			m_fnSetBatchStep(OUT_STOPPER, STEP15);
		break;

	case STEP15:
		setBatchStatus(OUT_STOPPER, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_STOPPER);
		m_fnSetBatchStep(OUT_STOPPER, STEP0); //
		break;

	case STEP16: 
		setBatchStatus(OUT_STOPPER, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_STOPPER);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_STOPPER);
		m_fnSetBatchStep(OUT_STOPPER, STEP17); //
		break;
	case STEP17: //IN_CENTERING WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_STOPPER, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_STOPPER) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_STOPPER, m_fnGetPrevBatchStep(OUT_STOPPER));
			m_fnSetBatchStep(OUT_STOPPER, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[OUT_STOPPER].SetColorChange(RED, RGB(0, 0, 0));
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();

		m_fnSetBatchStep(OUT_STOPPER, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_STOPPER))

	return 0;

}

int CInspectThread::stg6Center(void){

	CString str;
	int iRet;

	switch (m_fnGetBatchStep(OUT_CENTERING))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_CENTERING) != STEP0)
		{
			m_fnSetBatchStep(OUT_CENTERING, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[OUT_CENTERING].part == PART
#ifdef _VI_ALIGN
			|| m_PartDataBuffer[13].part == PART
			|| m_PartDataBuffer[14].part == PART
			|| m_PartDataBuffer[15].part == PART
			|| m_PartDataBuffer[16].part == PART
			|| m_PartDataBuffer[17].part == PART
			|| m_PartData[OUT_VI2].part == PART
			|| m_PartData[OUT_VI3].part == PART
#endif
			) //PART OR CLEAR
		{
			//stStgBatch[OUT_CNVYR].allow &= ~CNVYR_OUT_CTR;
			m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_CENTERING);
			stStgBatch[OUT_CENTERING].retry = 0;
			m_fnSetBatchStep(OUT_CENTERING, STEP3);
		}
		else{
			m_fnSetBatchStep(OUT_CENTERING, STEP14);
		}
		break;

	case STEP3: //CENTERING FWD
		if (m_fnGetPrevBatchStep(OUT_CENTERING) != STEP3)
		{
			if (stStgBatch[OUT_CENTERING].timer.On)
			{
				G_AddLog(3, L"CENTERING TIMER ALREADY ON(FWD)");
			}
			stStgBatch[OUT_CENTERING].timer.clear();
			stStgBatch[OUT_CENTERING].counter.clear();
			iRet = m_pPIO->writeDO(1, OUT_STG5_CTR_FWD, CARD_IO);
			if (iRet == TMC_RV_OK){
				stStgBatch[OUT_CENTERING].retry = 0;
				m_fnSetBatchStep(OUT_CENTERING, STEP3);
			}
			else{
				G_AddLog(3, L"CENTERING FWD iRet not OK]");
				if (stStgBatch[OUT_CENTERING].retry++ > BATCH_RETRY){
					stStgBatch[OUT_CENTERING].retry = 0;
					str.Format(L"IO Write Error: Address: OUT_CENTERING STEP3: OUT_STG5_CTR_FWD");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(OUT_CENTERING, STEP19);
				}
			}
		}
		else
		{
			if (m_pPIO->readDI(IN_STG5_CTR_FWD, CARD_IO) == 1)
			{
				if (!stStgBatch[OUT_CENTERING].timer.On){
					stStgBatch[OUT_CENTERING].timer.On = 1;
					stStgBatch[OUT_CENTERING].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[OUT_CENTERING].timer.OTE == 1)
					{
						stStgBatch[OUT_CENTERING].timer.clear();
						stStgBatch[OUT_CENTERING].counter.clear();
						m_fnSetBatchStep(OUT_CENTERING, STEP4);
					}
				}
			}
			else{
				if (!stStgBatch[OUT_CENTERING].counter.On){
					stStgBatch[OUT_CENTERING].counter.On = 1;
					stStgBatch[OUT_CENTERING].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[OUT_CENTERING].counter.OTE == 1)
					{
						stStgBatch[OUT_CENTERING].timer.clear();
						stStgBatch[OUT_CENTERING].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 97);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 97);
						m_fnSetBatchStep(OUT_CENTERING, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;

	case STEP4: //CENTERING RET
		if (m_fnGetPrevBatchStep(OUT_CENTERING) != STEP4)
		{
			if (stStgBatch[OUT_CENTERING].timer.On)
			{
				G_AddLog(3, L"CENTERING TIMER ALREADY ON(RET)");
			}
			stStgBatch[OUT_CENTERING].timer.clear();
			stStgBatch[OUT_CENTERING].counter.clear();
			iRet = m_pPIO->writeDO(1, OUT_STG5_CTR_RET, CARD_IO);
			if (iRet == TMC_RV_OK){
				stStgBatch[OUT_CENTERING].retry = 0;
				m_fnSetBatchStep(OUT_CENTERING, STEP4);
			}
			else{
				G_AddLog(3, L"CENTERING RET iRet not OK]");
				if (stStgBatch[OUT_CENTERING].retry++ > BATCH_RETRY){
					stStgBatch[OUT_CENTERING].retry = 0;
					str.Format(L"IO Write Error: Address: OUT_CENTERING STEP4: OUT_STG5_CTR_RET");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(OUT_CENTERING, STEP19);
				}
			}
			
		}
		else
		{
			if (m_pPIO->readDI(IN_STG5_CTR_RET, CARD_IO) == 1
				)
			{
				if (!stStgBatch[OUT_CENTERING].timer.On){
					stStgBatch[OUT_CENTERING].timer.On = 1;
					stStgBatch[OUT_CENTERING].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[OUT_CENTERING].timer.OTE == 1)
					{
						stStgBatch[OUT_CENTERING].timer.clear();
						stStgBatch[OUT_CENTERING].counter.clear();
						m_fnSetBatchStep(OUT_CENTERING, STEP14);
					}
				}
			}
			else{
				if (!stStgBatch[OUT_CENTERING].counter.On){
					stStgBatch[OUT_CENTERING].counter.On = 1;
					stStgBatch[OUT_CENTERING].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[OUT_CENTERING].counter.OTE == 1)
					{
						stStgBatch[OUT_CENTERING].timer.clear();
						stStgBatch[OUT_CENTERING].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 98);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 98);
						m_fnSetBatchStep(OUT_CENTERING, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;

	case STEP14: //
#ifdef _VI_ALIGN
		if (m_PartData[OUT_VI2].part == PART)
			m_fnSetBatchStep(OUT_VI2, STEP1);
		if (m_PartData[OUT_VI3].part == PART)
			m_fnSetBatchStep(OUT_VI3, STEP1);
#endif		
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(OUT_CENTERING, STEP16);
		else
			m_fnSetBatchStep(OUT_CENTERING, STEP15);
		break;

	case STEP15: //OUT_CENTERING MANUAL END
		setBatchStatus(OUT_CENTERING, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_CENTERING);
		m_fnSetBatchStep(OUT_CENTERING, STEP0); //
		break;

	case STEP16: //OUT_CENTERING AUTO END
		setBatchStatus(OUT_CENTERING, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_CENTERING);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_CENTERING);
		m_fnSetBatchStep(OUT_CENTERING, STEP17); //
		break;
	case STEP17: //OUT_CENTERING WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_CENTERING, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_CENTERING) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_CENTERING, m_fnGetPrevBatchStep(OUT_CENTERING));
			m_fnSetBatchStep(OUT_CENTERING, STEP19);
			errorRoutine();
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		if (m_pPIO->readDI(IN_STG5_CTR_FWD, CARD_IO) == 1)
		{
			iRet = m_pPIO->writeDO(1, OUT_STG5_CTR_RET, CARD_IO);
		}
		m_fnSetBatchStep(OUT_CENTERING, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_CENTERING))

	return 0;
}

////////////////////
//UPPER
//20160307_KYS_ADD
int CInspectThread::stg7VI2(void){

	CString str;

	switch (m_fnGetBatchStep(OUT_VI2))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_VI2) != STEP0)
		{
			m_fnSetBatchStep(OUT_VI2, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[OUT_VI2].part == PART) //PART OR CLEAR
		{
			//stStgBatch[IN_CNVYR].allow &= ~CNVYR_OUT_VI2;
			//m_vi.m_LiveHandls[CAM_PCCA2].flagCapture[CAM_PCCA2] = TRUE;

			m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_VI2);
			m_fnSetBatchStep(OUT_VI2, STEP3);
			stStgBatch[OUT_VI2].retry = 0;
			//LIGHT_ON_20160325_KYS
			//turnLight(LIGHT_CAM3_1, ON);
		}
		else{
			m_fnSetBatchStep(OUT_VI2, STEP14);
		}

		break;

	case STEP3: //VI
		if (m_fnGetPrevBatchStep(OUT_VI2) != STEP3)
		{
			if (m_PartData[OUT_VI2].part == PART)
			{
				if (m_vi.flagThreadsVI[CAM_PCCA2] == TH_VI_CLEAR){
					m_fnSetBatchStep(OUT_VI2, STEP3);
					stStgBatch[OUT_VI2].retry = 0;
					//G_AddLog(3, L"PostMessage Grab UW_VI_3 [SEND] ");
					::PostMessage(m_vi.m_hWnd, UM_VI, (WPARAM)UW_VI_3, m_PartData[OUT_VI2].id); //STG7 VISION INSPECTION REQUEST
					if (!stStgBatch[OUT_VI2].timer.On){
						stStgBatch[OUT_VI2].timer.On = 1;
						stStgBatch[OUT_VI2].timer.Set = T_WAIT_TIMEOUT_2X;//Time out
					}
				}

				//THREAD DEAD?
				if (!stStgBatch[OUT_VI2].counter.On){
					stStgBatch[OUT_VI2].counter.On = 1;
					stStgBatch[OUT_VI2].counter.Set = T_WAIT_TIMEOUT_2X * 2;//Time out
				}
				else{
					if (stStgBatch[OUT_VI2].counter.OTE == 1)//TIME OUT
					{
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50);
						stStgBatch[OUT_VI2].counter.clear();
						m_fnSetBatchStep(OUT_VI2, STEP19);
					}
				}
			}
			else{
				m_fnSetBatchStep(OUT_VI2, STEP14);
			}
		}
		else
		{
			if (m_vi.GetUMMsgResponse(UW_VI_3) == UM_VI_GRABBED /* || m_vi.GetUMMsgResponse(UW_VI_2) == UM_VI_DONE */) //IMAGE GRABBED
			{//
				//LIGHT_ON_20160325_KYS
//				turnLight(LIGHT_CAM3_1, OFF);
				//G_AddLog(3, L"PostMessage Grab UW_VI_3 [GRAB] ");

				stStgBatch[OUT_VI2].timer.clear();
				stStgBatch[OUT_VI2].counter.clear();
				m_vi.SetUMMsgResponse(UW_VI_3, UM_VI_CLEAR);
				m_fnSetBatchStep(OUT_VI2, STEP14);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_3) == UM_VI_ERROR){//ERROR
				//Error
				stStgBatch[OUT_VI2].timer.clear();
				stStgBatch[OUT_VI2].counter.clear();
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 61);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 61);
				m_fnSetBatchStep(OUT_VI2, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_3, UM_VI_CLEAR);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_3) == UM_VI_GRAB_ERROR){//GRAB ERROR //RETRY 
				//Error
				if (stStgBatch[OUT_VI2].retry++ > BATCH_RETRY){
					stStgBatch[OUT_VI2].timer.clear();
					stStgBatch[OUT_VI2].counter.clear();
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 56);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 56);
					m_fnSetBatchStep(OUT_VI2, STEP19);
					m_vi.SetUMMsgResponse(UW_VI_3, UM_VI_CLEAR);
				}
				else{
					//retry
					m_fnSetPrevBatchStep(OUT_VI2, STEP1);
				}
			}

			if (stStgBatch[OUT_VI2].timer.OTE == 1)//TIME OUT
			{
				stStgBatch[OUT_VI2].timer.clear();
				stStgBatch[OUT_VI2].counter.clear();
				//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 54);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 54);
				m_fnSetBatchStep(OUT_VI2, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_3, UM_VI_CLEAR);
			}
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(OUT_VI2, STEP16);
		else
			m_fnSetBatchStep(OUT_VI2, STEP15);
		break;

	case STEP15: //OUT_VI2 MANUAL END
		setBatchStatus(OUT_VI2, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_VI2);
		m_fnSetBatchStep(OUT_VI2, STEP0); //
		break;

	case STEP16: //OUT_VI2 AUTO END
		setBatchStatus(OUT_VI2, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_VI2);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_VI2);
		m_fnSetBatchStep(OUT_VI2, STEP17); //
		//LIGHT_ON_20160325_KYS
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_1, OFF);
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_2, OFF);
		//
		break;
	case STEP17: //OUT_VI2 WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_VI2, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_VI2) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_VI2, m_fnGetPrevBatchStep(OUT_VI2));
			m_fnSetBatchStep(OUT_VI2, STEP19);
			errorRoutine();
			//LIGHT_ON_20160325_KYS
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_1, OFF);
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM3_2, OFF);
			//
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		m_fnSetBatchStep(OUT_VI2, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_VI2))

	return 0;

}
////////////////////////////////////////////////
////////////////////
//OUTER
//20160307_KYS_ADD
int CInspectThread::stg12VI3(void){

	CString str;

	switch (m_fnGetBatchStep(OUT_VI3))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_VI3) != STEP0)
		{
			m_fnSetBatchStep(OUT_VI3, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		if (m_PartData[OUT_VI3].part == PART) //PART OR CLEAR
		{
			//stStgBatch[IN_CNVYR].allow &= ~CNVYR_OUT_VI3;
			//m_vi.m_LiveHandls[CAM_PCHI2].flagCapture[CAM_PCHI2] = TRUE;

			m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_VI3);
			m_fnSetBatchStep(OUT_VI3, STEP3);
			stStgBatch[OUT_VI3].retry = 0;

			//LIGHT_ON_20160325_KYS
			//turnLight(LIGHT_CAM4_1, ON);
			//turnLight(LIGHT_CAM4_2, ON);
		}
		else{
			m_fnSetBatchStep(OUT_VI3, STEP14);
		}

		break;

	case STEP3: //VI
		if (m_fnGetPrevBatchStep(OUT_VI3) != STEP3)
		{
			if (m_PartData[OUT_VI3].part == PART)
			{
				if (m_vi.flagThreadsVI[CAM_PCHI2] == TH_VI_CLEAR){
					m_fnSetBatchStep(OUT_VI3, STEP3);
					stStgBatch[OUT_VI3].retry = 0;
					//G_AddLog(3, L"PostMessage Grab UW_VI_4 [SEND] ");
					::PostMessage(m_vi.m_hWnd, UM_VI, (WPARAM)UW_VI_4, m_PartData[OUT_VI3].id); //STG12 VISION INSPECTION REQUEST
					if (!stStgBatch[OUT_VI3].timer.On){
						stStgBatch[OUT_VI3].timer.On = 1;
						stStgBatch[OUT_VI3].timer.Set = T_WAIT_TIMEOUT_2X;//Time out
					}
				}

				//THREAD DEAD?
				if (!stStgBatch[OUT_VI3].counter.On){
					stStgBatch[OUT_VI3].counter.On = 1;
					stStgBatch[OUT_VI3].counter.Set = T_WAIT_TIMEOUT_2X * 2;//Time out
				}
				else{
					if (stStgBatch[OUT_VI3].counter.OTE == 1)//TIME OUT
					{
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50);
						stStgBatch[OUT_VI3].counter.clear();
						m_fnSetBatchStep(OUT_VI3, STEP19);
					}
				}
			}
			else{
				m_fnSetBatchStep(OUT_VI3, STEP14);
			}
		}
		else
		{
			if (m_vi.GetUMMsgResponse(UW_VI_4) == UM_VI_GRABBED /* || m_vi.GetUMMsgResponse(UW_VI_3) == UM_VI_DONE */) //IMAGE GRABBED
			{//
				//LIGHT_ON_20160325_KYS
				turnLight(LIGHT_CAM4_1, OFF);
//				turnLight(LIGHT_CAM4_2, OFF);
				//G_AddLog(3, L"PostMessage Grab UW_VI_4 [GRAB] ");

				stStgBatch[OUT_VI3].timer.clear();
				stStgBatch[OUT_VI3].counter.clear();
				m_vi.SetUMMsgResponse(UW_VI_4, UM_VI_CLEAR);
				m_fnSetBatchStep(OUT_VI3, STEP14);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_4) == UM_VI_ERROR){//ERROR
				//Error
				stStgBatch[OUT_VI3].timer.clear();
				stStgBatch[OUT_VI3].counter.clear();
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 61);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 61);
				m_fnSetBatchStep(OUT_VI3, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_4, UM_VI_CLEAR);
			}
			else if (m_vi.GetUMMsgResponse(UW_VI_4) == UM_VI_GRAB_ERROR){//GRAB ERROR //RETRY 
				//Error
				if (stStgBatch[OUT_VI3].retry++ > BATCH_RETRY){
					stStgBatch[OUT_VI3].timer.clear();
					stStgBatch[OUT_VI3].counter.clear();
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 56);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 56);
					m_fnSetBatchStep(OUT_VI3, STEP19);
					m_vi.SetUMMsgResponse(UW_VI_4, UM_VI_CLEAR);
				}
				else{
					//retry
					m_fnSetPrevBatchStep(OUT_VI3, STEP1);
				}
			}

			if (stStgBatch[OUT_VI3].timer.OTE == 1)//TIME OUT
			{
				stStgBatch[OUT_VI3].timer.clear();
				stStgBatch[OUT_VI3].counter.clear();
				//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 54);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 54);
				m_fnSetBatchStep(OUT_VI3, STEP19);
				m_vi.SetUMMsgResponse(UW_VI_4, UM_VI_CLEAR);
			}
		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(OUT_VI3, STEP16);
		else
			m_fnSetBatchStep(OUT_VI3, STEP15);
		break;

	case STEP15: //OUT_VI3 MANUAL END
		setBatchStatus(OUT_VI3, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_VI3);
		m_fnSetBatchStep(OUT_VI3, STEP0); //
		break;

	case STEP16: //OUT_VI3 AUTO END
		setBatchStatus(OUT_VI3, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_VI3);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_VI3);
		m_fnSetBatchStep(OUT_VI3, STEP17); //
		//LIGHT_ON_20160325_KYS
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_1, OFF);
		//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_2, OFF);
		//
		break;
	case STEP17: //OUT_VI3 WAIT SHUTTLE END
		//wait until SHUTTLE end
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_VI3, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_VI3) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_VI3, m_fnGetPrevBatchStep(OUT_VI3));
			m_fnSetBatchStep(OUT_VI3, STEP19);
			errorRoutine();
			//LIGHT_ON_20160325_KYS
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_1, OFF);
			//G_MainWnd->m_InspectThread.turnLight(LIGHT_CAM4_2, OFF);
			//
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset
		m_fnSetBatchStep(OUT_VI3, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_VI3))

	return 0;

}
////////////////////////////////////////////////



int CInspectThread::stg8NGout(void){

	int iRet;
	CString str;

	switch (m_fnGetBatchStep(OUT_NGOUT))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP0)
		{
			m_fnSetBatchStep(OUT_NGOUT, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //
		m_fnAllowConveyor(OUT_CNVYR, FALSE, OUT_NGOUT);
		if (m_PartData[OUT_NGOUT].part != PART){
			m_fnSetBatchStep(OUT_NGOUT, STEP14);
		}
		else{
			m_fnSetBatchStep(OUT_NGOUT, STEP3);
		}
		break;

	case STEP2: //

		break;

	case STEP3: //get VI result from stg3
		if (m_PartData[OUT_NGOUT].part != PART){
			m_fnSetBatchStep(OUT_NGOUT, STEP14);
		}
		else{
			if (!stStgBatch[OUT_NGOUT].timer.On){
				stStgBatch[OUT_NGOUT].timer.On = 1;
				stStgBatch[OUT_NGOUT].timer.Set = T_WAIT_TIMEOUT;
				//m_PartData[OUT_NGOUT].results[CAM_PCHI] = VI_CLEAR;
				m_PartData[OUT_NGOUT].results[CAM_PCCA2] = VI_CLEAR;
				//m_PartData[OUT_NGOUT].results[CAM_PCHI2] = VI_CLEAR;
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI].clear();
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA].clear();
				m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA2].clear();
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI2].clear();
			}
			else{
				//Get STG7 result

				if (!m_vi.queViResults[CAM_PCCA2].empty())
				{
					//
					for (int i = 0; i < m_vi.queViResults[CAM_PCCA2].size(); i++) {
						VI_RESULT &vr = m_vi.queViResults[CAM_PCCA2][i];
						bool bRemove = false;

						if (vr.id == m_PartData[OUT_NGOUT].id){
							m_PartData[OUT_NGOUT].results[CAM_PCCA2] = vr.result;
							m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA2] = move(*vr.vecDefects);
							vr.vecDefects->clear();
							delete vr.vecDefects;

							stStgBatch[OUT_NGOUT].timer.clear();
							stStgBatch[OUT_NGOUT].partID = vr.id;
							m_fnSetBatchStep(OUT_NGOUT, STEP4);

							bRemove = true;
						}

						else if (vr.id < m_PartData[OUT_NGOUT].id) {//get next one
							//vr.vecDefects.clear();
#ifdef DEV_MODE
							G_AddLog(3, L"STEP 3 stStgBatch[OUT_NGOUT].partID: %d, vr.id %d < m_PartData[OUT_NGOUT].id %d ", stStgBatch[OUT_NGOUT].partID, vr.id, m_PartData[OUT_NGOUT].id);
#endif
							bRemove = true;
						}

						if (bRemove) {
							auto it = find_if(m_vi.queViResults[CAM_PCCA2].begin(), m_vi.queViResults[CAM_PCCA2].end(), [&vr](VI_RESULT &vr2) { return vr.id == vr2.id; });
							if (it != m_vi.queViResults[CAM_PCCA2].end()) {
								m_vi.csResult[CAM_PCCA2].Lock();
								m_vi.queViResults[CAM_PCCA2].erase(it);
								m_vi.csResult[CAM_PCCA2].Unlock();
							}
							break;
						}
					}
				}

				if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
				{
					G_AddLog(3, L"OUT_NGOUT step 3 stStgBatch[OUT_NGOUT].timeout stStgBatch[OUT_NGOUT].partID: %d", stStgBatch[OUT_NGOUT].partID);
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 77);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 77);
					//m_fnSetBatchStep(OUT_NGOUT, STEP19);
					stStgBatch[OUT_NGOUT].timer.clear();
					m_PartData[OUT_NGOUT].results[CAM_PCCA2] = VI_NG;
					m_fnSetBatchStep(OUT_NGOUT, STEP4);

				}
			}//if (!stStgBatch[OUT_NGOUT].timer.On){
		}

		break;

	case STEP4: //get VI result from stg3
		if (m_PartData[OUT_NGOUT].part != PART){
			m_fnSetBatchStep(OUT_NGOUT, STEP14);
		}
		else{
			if (!stStgBatch[OUT_NGOUT].timer.On){
				stStgBatch[OUT_NGOUT].timer.On = 1;
				stStgBatch[OUT_NGOUT].timer.Set = T_WAIT_TIMEOUT;
				m_PartData[OUT_NGOUT].results[CAM_PCHI2] = VI_CLEAR;
				m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI2].clear();
				//****************************
				//*******************************
			}
			else{
				//Get STG7 result

				if (!m_vi.queViResults[CAM_PCHI2].empty())
				{
					//
					for (int i = 0; i < m_vi.queViResults[CAM_PCHI2].size(); i++) {
						VI_RESULT &vr = m_vi.queViResults[CAM_PCHI2][i];
						bool bRemove = false;

						if (vr.id == m_PartData[OUT_NGOUT].id){
							m_PartData[OUT_NGOUT].results[CAM_PCHI2] = vr.result;
							m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI2] = move(*vr.vecDefects);
							vr.vecDefects->clear();
							delete vr.vecDefects;

#ifdef	 VI_SQC
							CString str;
							str.Format(L"NGOUT_PCCA step4 id: %d, [%d, %d : %d, %d]", vr.id,
								m_PartData[OUT_NGOUT].results[CAM_PCHI], m_PartData[OUT_NGOUT].results[CAM_PCHI2], m_PartData[OUT_NGOUT].results[CAM_PCCA], m_PartData[OUT_NGOUT].results[CAM_PCHI2]);
							G_AddLog(3, str.GetBuffer());
							str.ReleaseBuffer();
#endif
							stStgBatch[OUT_NGOUT].partID2 = vr.id;
							stStgBatch[OUT_NGOUT].timer.clear();
							m_fnSetBatchStep(OUT_NGOUT, STEP5);

							bRemove = true;
						}

						else if (vr.id < m_PartData[OUT_NGOUT].id) {//get next one
							//vr.vecDefects.clear();
#ifdef DEV_MODE
							G_AddLog(3, L"NGOUT_PCHI2 STEP 4 stStgBatch[OUT_NGOUT].partID2:%d, vr.id %d < m_PartData[OUT_NGOUT].id %d ", stStgBatch[OUT_NGOUT].partID2, vr.id, m_PartData[OUT_NGOUT].id);
#endif
							bRemove = true;
						}

						if (bRemove) {
							auto it = find_if(m_vi.queViResults[CAM_PCHI2].begin(), m_vi.queViResults[CAM_PCHI2].end(), [&vr](VI_RESULT &vr2) { return vr.id == vr2.id; });
							if (it != m_vi.queViResults[CAM_PCHI2].end()) {
								m_vi.csResult[CAM_PCHI2].Lock();
								m_vi.queViResults[CAM_PCHI2].erase(it);
								m_vi.csResult[CAM_PCHI2].Unlock();
							}
							break;
						}
					}
				}

				if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
				{
					G_AddLog(3, L"OUT_NGOUT step 4 stStgBatch[OUT_NGOUT].timeout stStgBatch[OUT_NGOUT].partID2: %d", stStgBatch[OUT_NGOUT].partID2);
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 77);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 77);
					//m_fnSetBatchStep(OUT_NGOUT, STEP19);
					stStgBatch[OUT_NGOUT].timer.clear();
					m_PartData[OUT_NGOUT].results[CAM_PCHI2] = VI_NG;
					//m_PartData[OUT_NGOUT].results[CAM_PCHI22] = VI_NG;
					m_fnSetBatchStep(OUT_NGOUT, STEP5);

				}
			}//if (!stStgBatch[OUT_NGOUT].timer.On){
		}

		break;


		//
	case STEP5: //RESULT DISPLAY
		if (m_PartData[OUT_NGOUT].part == PART)
		{
			if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP5)
			{
				m_fnSetBatchStep(OUT_NGOUT, STEP5);
				int pass = summarizeResult();

				//G_MainWnd->m_DataHandling.viRes.m_csVIResult[0].Lock();
				memcpy(G_MainWnd->m_DataHandling.viRes.results, m_PartData[OUT_NGOUT].results, MAX_VI_TEST*sizeof(int));
//#ifndef NO_DEFECT_DISPLAY
				G_MainWnd->m_DataHandling.viRes.vecDefects[CAM_PCHI] = move(m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI]);
				G_MainWnd->m_DataHandling.viRes.vecDefects[CAM_PCCA] = move(m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA]);
				G_MainWnd->m_DataHandling.viRes.vecDefects[CAM_PCCA2] = move(m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA2]);
				G_MainWnd->m_DataHandling.viRes.vecDefects[CAM_PCHI2] = move(m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI2]);
//#else
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI].clear();
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA].clear();
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCCA2].clear();
				//m_PartDataVecDefects[OUT_NGOUT][CAM_PCHI2].clear();
//#endif
				//G_MainWnd->m_DataHandling.viRes.m_csVIResult[0].Unlock();

				//G_MainWnd->m_DataHandling.m_fnDayReportWrite(TRUE);
				//Data Store and Display
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, pass);

				//if (G_MainWnd->m_ServerDlg.nMasterOK == 1 || G_MainWnd->m_ServerDlg.nMasterNG == 1){
				//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_STATUS_UPDATE, NULL);
				//}

				if (pass){//Pass
					m_fnSetBatchStep(OUT_NGOUT, STEP14);
				}
				else{//Fail
					stStgBatch[OUT_NGOUT].retry = 0;
					m_fnSetBatchStep(OUT_NGOUT, STEP9);
				}
			}

		}
		else{
			if (!stStgBatch[OUT_NGOUT].timer.On){
				stStgBatch[OUT_NGOUT].timer.On = 1;
				stStgBatch[OUT_NGOUT].timer.Set = 3;
			}
			else{
				if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
				{
					stStgBatch[OUT_NGOUT].timer.clear();
					m_fnSetBatchStep(OUT_NGOUT, STEP14);
				}
			}
		}//
		break;

	case STEP6: //NG OUT FWD
		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP6)
		{
			iRet = m_pPIO->writeDO(1, OUT_UNLOAD_NG_OUT_FWD, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(OUT_NGOUT, STEP6);
			}
			else{
				if (stStgBatch[OUT_NGOUT].retry++ > BATCH_RETRY){
					stStgBatch[OUT_NGOUT].retry = 0;
					str.Format(L"IO Write Error: Address: OUT_NGOUT STEP6: OUT_UNLOAD_NG_OUT_FWD");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(OUT_NGOUT, STEP19);
				}
			}
		}
		else
		{	//wait till fwd
			if (m_pPIO->readDI(IN_UNLOAD_NG_OUT_FWD, CARD_IO) == 1)
			{
				//TIMER 0.5S
				if (!stStgBatch[OUT_NGOUT].timer.On){
					stStgBatch[OUT_NGOUT].timer.On = 1;
					stStgBatch[OUT_NGOUT].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
					{
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						m_fnSetBatchStep(OUT_NGOUT, STEP7);
						//G_AddLog(3, L"OUT_NGOUT NG OUT FWD ");
					}
				}
			}
			else{
				if (!stStgBatch[OUT_NGOUT].counter.On){
					stStgBatch[OUT_NGOUT].counter.On = 1;
					stStgBatch[OUT_NGOUT].counter.Set = T_WAIT_TIMEOUT_2X;
				}
				else{
					if (stStgBatch[OUT_NGOUT].counter.OTE == 1)
					{
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 70);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 70);
						m_fnSetBatchStep(OUT_NGOUT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_OUT_NGOUT_LIFT_UP, CARD_IO)==1)//UP
		}
		break;

	case STEP7: //NG OUT RET
		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP7)
		{
			iRet = m_pPIO->writeDO(1, OUT_UNLOAD_NG_OUT_RET, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(OUT_NGOUT, STEP7);
			}
			else{
				if (stStgBatch[OUT_NGOUT].retry++ > BATCH_RETRY){
					stStgBatch[OUT_NGOUT].retry = 0;
					str.Format(L"IO Write Error: Address: OUT_NGOUT STEP7: OUT_UNLOAD_NG_OUT_RET");
					G_AddLog(3, str.GetBuffer());
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(OUT_NGOUT, STEP19);
				}
			}
		}
		else
		{	//wait till ret
			if (m_pPIO->readDI(IN_UNLOAD_NG_OUT_RET, CARD_IO) == 1)
			{
				//TIMER 0.5S
				if (!stStgBatch[OUT_NGOUT].timer.On){
					stStgBatch[OUT_NGOUT].timer.On = 1;
					stStgBatch[OUT_NGOUT].timer.Set = 2;
				}
				else{
					if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
					{
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						m_fnSetBatchStep(OUT_NGOUT, STEP14);
						//G_AddLog(3, L"OUT_NGOUT NG OUT RET ");
					}
				}
			}
			else{
				if (!stStgBatch[OUT_NGOUT].counter.On){
					stStgBatch[OUT_NGOUT].counter.On = 1;
					stStgBatch[OUT_NGOUT].counter.Set = T_WAIT_TIMEOUT;// 20sec
				}
				else{
					if (stStgBatch[OUT_NGOUT].counter.OTE == 1)
					{
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 71);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 71);
						m_fnSetBatchStep(OUT_NGOUT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_OUT_NGOUT_LIFT_UP, CARD_IO)==1)//UP
		}
		break;


	case STEP9: //NG OUT FWD
		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP9)
		{
			iRet = m_pPIO->writeDO(1, OUT_UNLOAD_NG_OUT_FWD, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(OUT_NGOUT, STEP9);
			}
			else{
				if (stStgBatch[OUT_NGOUT].retry++ > 3){
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 58);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 58);
					m_fnSetBatchStep(OUT_NGOUT, STEP19); break;
				}
			}
		}
		else
		{	//wait till fwd

#ifndef VIRTUAL_RUN

				//TIMER 0.5S
				if (!stStgBatch[OUT_NGOUT].timer.On){
					stStgBatch[OUT_NGOUT].timer.On = 1;
					stStgBatch[OUT_NGOUT].timer.Set = T_WAIT_3X;
				}
				else{
					if (m_pPIO->readDI(IN_UNLOAD_NG_OUT_FWD, CARD_IO) == 1) //RET BACK
					{
						//2016.04.05 jec add
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						////////////
						m_fnSetBatchStep(OUT_NGOUT, STEP10);
					}

					if (stStgBatch[OUT_NGOUT].timer.OTE == 1)
					{
						stStgBatch[OUT_NGOUT].timer.clear();
						stStgBatch[OUT_NGOUT].counter.clear();
						m_fnSetBatchStep(OUT_NGOUT, STEP10);
					}
				}
#else
			m_fnSetBatchStep(OUT_NGOUT, STEP14);
#endif

		}
		break;


	case STEP10: //NG OUT RET CHECK

		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP10)
		{
			if (!stStgBatch[OUT_NGOUT].counter.On){
				stStgBatch[OUT_NGOUT].counter.On = 1;
				stStgBatch[OUT_NGOUT].counter.Set = T_WAIT_TIMEOUT_2X;
			}
			m_fnSetBatchStep(OUT_NGOUT, STEP10);
		}
		else
		{

			if (m_pPIO->readDI(IN_UNLOAD_NG_OUT_RET, CARD_IO) == 1) //RET BACK
			{
				//2016.04.05 jec add
				stStgBatch[OUT_NGOUT].timer.clear();
				stStgBatch[OUT_NGOUT].counter.clear();
				////////////
				m_fnSetBatchStep(OUT_NGOUT, STEP14);
			}

			if (stStgBatch[OUT_NGOUT].counter.OTE == 1)
			{
				stStgBatch[OUT_NGOUT].timer.clear();
				stStgBatch[OUT_NGOUT].counter.clear();
				//timeout error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 71);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 71);
				m_fnSetBatchStep(OUT_NGOUT, STEP19); break;
			}

		}
		break;

	case STEP14: //
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(OUT_NGOUT, STEP16);
		else
			m_fnSetBatchStep(OUT_NGOUT, STEP15);
		break;

	case STEP15: //OUT_NGOUT MANUAL END
		setBatchStatus(OUT_NGOUT, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_NGOUT);
		m_fnSetBatchStep(OUT_NGOUT, STEP0); //
		break;

	case STEP16: //OUT_NGOUT AUTO END
		setBatchStatus(OUT_NGOUT, FINISHED);
		m_fnAllowConveyor(OUT_CNVYR, TRUE, OUT_NGOUT);
		m_fnRequestConveyor(OUT_CNVYR, TRUE, OUT_NGOUT);
		m_fnSetBatchStep(OUT_NGOUT, STEP17); //
		break;
	case STEP17: //OUT_NGOUT WAIT TRANSFER END
		//wait until TRANSFER end
		if (stStgBatch[OUT_CNVYR].request == 0)
		{
			m_fnSetBatchStep(OUT_NGOUT, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_NGOUT) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_NGOUT, m_fnGetPrevBatchStep(OUT_NGOUT));
			m_fnSetBatchStep(OUT_NGOUT, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[OUT_NGOUT].SetColorChange(RED, RGB(0, 0, 0));
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset

		m_fnSetBatchStep(OUT_NGOUT, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_NGOUT))

	return 0;

}

int CInspectThread::stg9InConveyorRun(void){

	CString str;
	int iRet;
	int nDone;
	double dbActPos;

	switch (m_fnGetBatchStep(IN_CNVYR))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(IN_CNVYR) != STEP0)
		{
			m_fnSetBatchStep(IN_CNVYR, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
			setBatchStatus(IN_CNVYR, RUN);
			if (
				m_pPIO->readDI(IN_LOAD_DETECT, CARD_IO) == 1 &&
				//m_pPIO->readDI(IN_LOAD_DETECT, CARD_IO) == 0 &&
				m_pPIO->readDI(IN_LOAD_STOP, CARD_IO) == 1)
				m_fnSetBatchStep(IN_CNVYR, STEP2);
			else{
				m_fnSetBatchStep(IN_CNVYR, STEP3);
			}
		break;


	case STEP2: //stopper open
		if (m_fnGetPrevBatchStep(IN_CNVYR) != STEP2)
		{
			iRet = m_pPIO->writeDO(1, OUT_LOAD_OPEN, CARD_IO);
			if (iRet == TMC_RV_OK){
				m_fnSetBatchStep(IN_CNVYR, STEP2);
			}
			else{
				str.Format(L"IO Write Error: Address: LOAD STEP2: OUT_LOAD_OPEN");
				G_AddLog(3, str.GetBuffer());
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				m_fnSetBatchStep(IN_CNVYR, STEP19);
			}
		}
		else
		{
			if (m_pPIO->readDI(IN_LOAD_OPEN, CARD_IO) == 1)
			{
				if (!stStgBatch[IN_CNVYR].timer.On){
					stStgBatch[IN_CNVYR].timer.On = 1;
					stStgBatch[IN_CNVYR].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[IN_CNVYR].timer.OTE == 1)
					{
						stStgBatch[IN_CNVYR].timer.clear();
						stStgBatch[IN_CNVYR].counter.clear();
						m_fnSetBatchStep(IN_CNVYR, STEP3);
					}
				}
			}
			else{
				if (!stStgBatch[IN_CNVYR].counter.On){
					stStgBatch[IN_CNVYR].counter.On = 1;
					stStgBatch[IN_CNVYR].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[IN_CNVYR].counter.OTE == 1)
					{
						stStgBatch[IN_CNVYR].timer.clear();
						stStgBatch[IN_CNVYR].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 00);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 00);
						m_fnSetBatchStep(IN_CNVYR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;



	case STEP3: //IN CONVEYOR I PITCH RUN
		if (m_fnGetPrevBatchStep(IN_CNVYR) != STEP3)
		{
#ifndef VIRTUAL_RUN
			//clear
			//RUN
			//////LIGHT_ON_20160325_KYS
//			turnLight(LIGHT_CAM1_1, OFF);
			turnLight(LIGHT_CAM2_1, ON);
//			turnLight(LIGHT_CAM2_2, OFF);
//			m_pPIO->writeDO(1, OUT_STG4_STOP_OPEN, CARD_IO);

			////

#ifdef DEBUG_PRINT_TIME
			gConveyerMoveStartTime[AXIS_IN_CNVYR] = ::GetTickCount();
#endif
			m_AxisDlg.m_fnClearPos(AXIS_IN_CNVYR);
			iRet = m_AxisDlg.m_fnIncmove(AXIS_IN_CNVYR, G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_IN_INC]);
#else
			iRet = TMC_RV_OK;
#endif
			if (iRet != TMC_RV_OK)
			{//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 80);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 80);
				m_fnSetBatchStep(IN_CNVYR, STEP19); break;
			}
			else{
				m_fnSetBatchStep(IN_CNVYR, STEP3);
			}
		}
		else{
			if (!stStgBatch[IN_CNVYR].timer.On){
				stStgBatch[IN_CNVYR].timer.On = 1;
				stStgBatch[IN_CNVYR].timer.Set = T_WAIT_TIMEOUT_2X*2;//TIME OUT
			}
			else{
#ifndef VIRTUAL_RUN
				pmiAxCheckDone(CARD_NO, AXIS_IN_CNVYR, &nDone);
#else
				nDone = emSTAND;
#endif
				if (nDone == emRUNNING){
					// position check to begin stg3vi
					iRet = pmiAxGetActPos(CARD_NO, AXIS_IN_CNVYR, &dbActPos); //
					if (iRet == TRUE)	{
						if (dbActPos >= G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_INPUT_CLOSE_DELAY])
						{
							//CLOSE STG0 INPUT GATE
							if (!m_pPIO->readDI(IN_LOAD_STOP, CARD_IO))
							{
								//m_PartData[INPUT].part = PART;
								//m_PartData[INPUT].id = PartID++;
								//G_MainWnd->m_PIODlg.partPassing = 0;

								iRet = m_pPIO->writeDO(1, OUT_LOAD_STOP, CARD_IO);
							} 
							//
							//
							//if (iRet != TMC_RV_OK){//fail
							//}
						}
					}

					if (stStgBatch[IN_CNVYR].timer.OTE == 1)
					{
						stStgBatch[IN_CNVYR].timer.clear();
						//Error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 81);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 81);
						m_fnSetBatchStep(IN_CNVYR, STEP19); break;
					}
				}
				else if (nDone == emSTAND){//stopped
					////LIGHT_ON_20160325_KYS
//					turnLight(LIGHT_CAM1_1, ON);
//					turnLight(LIGHT_CAM2_1, ON);
//					turnLight(LIGHT_CAM2_2, ON);
//					m_pPIO->writeDO(0, OUT_STG4_STOP_OPEN, CARD_IO);
					//
					stStgBatch[IN_CNVYR].timer.clear();
					//Data Shift
					m_fnShiftPartDataInConveyor();
					m_PartData[INPUT].part = 0;
#ifndef VIRTUAL_RUN
					//Clear position
					iRet = m_AxisDlg.m_fnClearPos(AXIS_IN_CNVYR);
#else
					iRet = TMC_RV_OK;
#endif
					if (iRet != TMC_RV_OK)
					{//Error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 9);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 9);
						m_fnSetBatchStep(IN_CNVYR, STEP19); break;
					}

					m_fnSetBatchStep(IN_CNVYR, STEP11);

#ifdef DEBUG_PRINT_TIME
					int nConveyerIndex = AXIS_IN_CNVYR;
					gMaxFullTimeId[nConveyerIndex] = 0;
					gMaxFullTime[nConveyerIndex] = 0;
					gMaxReadyTime[nConveyerIndex] = 0;
					gMaxStepTime[nConveyerIndex] = 0;
					gConveyerMoveEndTime[nConveyerIndex] = ::GetTickCount();
					gConveyerMovingTime[nConveyerIndex] = gConveyerMoveEndTime[nConveyerIndex] - gConveyerMoveStartTime[AXIS_IN_CNVYR];
					DebugPrint(L"==== IN_CNVRY Move Time : %d ms ====", gConveyerMovingTime[nConveyerIndex]);
#endif
				}
			}
		}//if(m_fnGetPrevBatchStep(IN_CNVYR) != STEP3) 
		break;

	case STEP4: //

		break;

	case STEP11: //END
		//2016,03,28 jec add
		if (m_PartData[IN_VI1].part != PART && m_PartData[IN_VI0].part != PART)
			turnLight(LIGHT_CAM2_1, OFF);

		stStgBatch[IN_CNVYR].request = 0;
		setBatchStatus(INPUT, CLEAR);
		setBatchStatus(IN_CENTERING, CLEAR);
		setBatchStatus(IN_VI0, CLEAR);
		setBatchStatus(IN_VI1, CLEAR);
		setBatchStatus(IN_BUFFER, CLEAR);
		setBatchStatus(IN_CNVYR, CLEAR);
		stStgBatch[IN_CNVYR].myturn = OUT_CNVYR_TURN;
		m_fnSetBatchStep(IN_CNVYR, STEP0);
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(IN_CNVYR) != STEP19)
		{
			m_fnSetOldBatchStep(IN_CNVYR, m_fnGetPrevBatchStep(IN_CNVYR));
			m_fnSetBatchStep(IN_CNVYR, STEP19);
			errorRoutine();
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset

		m_fnSetBatchStep(IN_CNVYR, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(IN_CNVYR))

	return 0;

}

//WITH STG4 STOPPER CONTROL
int CInspectThread::stg10OutConveyorRun(void){

	CString str;
	int iRet;
	int nDone;
	double dbActPos;

	////Tact Time
	WCHAR tact[32];
	DWORD tick1;
	DWORD millisec;

	switch (m_fnGetBatchStep(OUT_CNVYR))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(OUT_CNVYR) != STEP0)
		{
			m_fnSetBatchStep(OUT_CNVYR, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		setBatchStatus(OUT_CNVYR, RUN);
		m_fnSetBatchStep(OUT_CNVYR, STEP3); //because OUT_STOPPER is removed
//		m_fnSetBatchStep(OUT_CNVYR, STEP2);
		break;

	case STEP2: //stg4 stopper open CHECK
		if (m_PartData[OUT_STOPPER].part == PART &&  m_pPIO->readDI(IN_STG4_STOP_OPEN, CARD_IO) != 1)
		{
			m_fnSetBatchStep(OUT_CNVYR, STEP3);
			//stopper
			//m_fnSetBatchStep(OUT_CNVYR, STEP5);
		}
		else{
			m_fnSetBatchStep(OUT_CNVYR, STEP3);
		}
		break;

	case STEP3: //OUT CONVEYOR I PITCH RUN
		if (m_fnGetPrevBatchStep(OUT_CNVYR) != STEP3)
		{

#ifndef VIRTUAL_RUN
			//clear
			m_AxisDlg.m_fnClearPos(AXIS_OUT_CNVYR);
			//RUN
			//LIGHT_ON_20160325_KYS
//			turnLight(LIGHT_CAM3_1, OFF);
			turnLight(LIGHT_CAM4_1, ON);
//			turnLight(LIGHT_CAM4_2, OFF);
			//

#ifdef DEBUG_PRINT_TIME
			gConveyerMoveStartTime[AXIS_OUT_CNVYR] = ::GetTickCount();
#endif

			iRet = m_AxisDlg.m_fnIncmove(AXIS_OUT_CNVYR, G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_OUT_INC]);
#else
			iRet = TMC_RV_OK;
#endif
			if (iRet != TMC_RV_OK)
			{//Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 82);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 82);
				m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
			}
			else{
				m_fnSetBatchStep(OUT_CNVYR, STEP3);
			}
		}
		else{
			if (!stStgBatch[OUT_CNVYR].timer.On){
				stStgBatch[OUT_CNVYR].timer.On = 1;
				stStgBatch[OUT_CNVYR].timer.Set = T_WAIT_TIMEOUT_2X * 2;//TIME OUT
			}
			else{
#ifndef VIRTUAL_RUN
				pmiAxCheckDone(CARD_NO, AXIS_OUT_CNVYR, &nDone);
#else
				nDone = emSTAND;
#endif
				if (nDone == emRUNNING){
					//STG4 STOPPER CLOSE 
					// position check to close stopper
					iRet = pmiAxGetActPos(CARD_NO, AXIS_OUT_CNVYR, &dbActPos); //
					if (iRet == TRUE)	{
						//if (dbActPos >= G_MainWnd->m_DataHandling.m_FlashData.dbdata[CVYR_OUT_STOPPER_CLOSE_INC])
						if (dbActPos >= G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_SHUTTLE_POS_OUTOF_STG3])
						{
							//CLOSE STG4 STOPPER
							//iRet = m_pPIO->writeDO(1, OUT_STG4_STOP_CLOSE, CARD_IO);
							//
							if (iRet != TMC_RV_OK){//fail
								//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 85);
								//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 85);
								//m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
							}
						}
					}
					//else{//Error
					//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 85);
					//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 85);
					//	m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
					//}

					if (stStgBatch[OUT_CNVYR].timer.OTE == 1)
					{
						stStgBatch[OUT_CNVYR].timer.clear();
						//Error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 83);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 83);
						m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
					}
				}
				else if (nDone == emSTAND){//stopped
					////LIGHT_ON_20160325_KYS
//					turnLight(LIGHT_CAM3_1, ON);
//					turnLight(LIGHT_CAM4_1, ON);
//					turnLight(LIGHT_CAM4_2, ON);       
					//Sleep(50); // FK jec
					//Sleep(5); // FK kys
					//////////////////////////////////////////////////////////////////////
					stStgBatch[OUT_CNVYR].timer.clear();
					//Data Shift
					m_fnShiftPartDataOutConveyor();
#ifndef VIRTUAL_RUN
					//Clear position
					iRet = m_AxisDlg.m_fnClearPos(AXIS_OUT_CNVYR);
#else
					iRet = TMC_RV_OK;
#endif
					if (iRet != TMC_RV_OK)
					{//Error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 10);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 10);
						m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
					}

					m_fnSetBatchStep(OUT_CNVYR, STEP11);

#ifdef DEBUG_PRINT_TIME
					int nConveyerIndex = AXIS_OUT_CNVYR;
					gMaxFullTimeId[nConveyerIndex] = 0;
					gMaxFullTime[nConveyerIndex] = 0;
					gMaxReadyTime[nConveyerIndex] = 0;
					gMaxStepTime[nConveyerIndex] = 0;
					gConveyerMoveEndTime[nConveyerIndex] = ::GetTickCount();
					gConveyerMovingTime[nConveyerIndex] = gConveyerMoveEndTime[nConveyerIndex] - gConveyerMoveStartTime[AXIS_OUT_CNVYR];
					DebugPrint(L"==== OUT_CNVRY Move Time : %d ms ====", gConveyerMovingTime[nConveyerIndex]);
#endif
				}
			}
		}
		break;

	case STEP5: //OPEN STG4 STOPPER
		if (m_fnGetPrevBatchStep(OUT_CNVYR) != STEP5)
		{
			//OPEN STG4 STOPPER
			iRet = m_pPIO->writeDO(1, OUT_STG4_STOP_OPEN, CARD_IO);
			if (iRet != TMC_RV_OK){//fail
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 90);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 90);
				m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
			}
			m_fnSetBatchStep(OUT_CNVYR, STEP5);
		}
		else
		{
			if (m_pPIO->readDI(IN_STG4_STOP_OPEN, CARD_IO) == 1
				)
			{
				if (!stStgBatch[OUT_CNVYR].timer.On){
					stStgBatch[OUT_CNVYR].timer.On = 1;
					stStgBatch[OUT_CNVYR].timer.Set = T_WAIT;
				}
				else{
					if (stStgBatch[OUT_CNVYR].timer.OTE == 1)
					{
						stStgBatch[OUT_CNVYR].timer.clear();
						stStgBatch[OUT_CNVYR].counter.clear();
						m_fnSetBatchStep(OUT_CNVYR, STEP3);
					}
				}
			}
			else{
				if (!stStgBatch[OUT_CNVYR].counter.On){
					stStgBatch[OUT_CNVYR].counter.On = 1;
					stStgBatch[OUT_CNVYR].counter.Set = T_WAIT_TIMEOUT;
				}
				else{
					if (stStgBatch[OUT_CNVYR].counter.OTE == 1)
					{
						stStgBatch[OUT_CNVYR].timer.clear();
						stStgBatch[OUT_CNVYR].counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 90);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 90);
						m_fnSetBatchStep(OUT_CNVYR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(
		}
		break;

	case STEP11: //END
	{//Tact time display
		tick1 = GetTickCount();
		millisec = tick1 - tickPrev;
		tickPrev = tick1;
		double TactJudge = 0.3f;
		//strTact = ctimeTact.Format(L"%H:%M:%S");
		wsprintf(tact, L"%d.%03d ", static_cast<int>(millisec / 1000), static_cast<int>(millisec % 1000));
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
				G_AddLog(2, L"TACT ************ [ %s ]", tact);
		//20160226_kys_tact time display add
		TactJudge = _wtof(tact);
		if (TactJudge >= 1.500)
		{
			G_MainWnd->m_ServerDlg.m_staticTimeMasterElapsec.SetText(tact, RED, BLACK);
		}
		else
		{
			G_MainWnd->m_ServerDlg.m_staticTimeMasterElapsec.SetText(tact, WHITE, BLACK);
		}
		//2016,03,28 jec add
		if (m_PartDataBuffer[17].part != PART && m_PartData[OUT_VI3].part != PART)
			turnLight(LIGHT_CAM4_1, OFF);

		stStgBatch[OUT_CNVYR].request = 0;
		stStgBatch[IN_CNVYR].myturn = IN_CNVYR_TURN;
		setBatchStatus(OUT_FLIPDROP, CLEAR);
		setBatchStatus(OUT_STOPPER, CLEAR);
		setBatchStatus(OUT_CENTERING, CLEAR);
		setBatchStatus(OUT_VI2, CLEAR);
		setBatchStatus(OUT_VI3, CLEAR);
		setBatchStatus(OUT_NGOUT, CLEAR);
		setBatchStatus(OUT_CNVYR, CLEAR);
		m_fnSetBatchStep(OUT_CNVYR, STEP0);
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	}
	case STEP19:  //ERROR STATE
		if (m_fnGetPrevBatchStep(OUT_CNVYR) != STEP19)
		{
			m_fnSetOldBatchStep(OUT_CNVYR, m_fnGetPrevBatchStep(OUT_CNVYR));
			m_fnSetBatchStep(OUT_CNVYR, STEP19);
			errorRoutine();
		}
		else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset

		m_fnSetBatchStep(OUT_CNVYR, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OUT_CNVYR))

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

int CInspectThread::EMStop(void)
{
	int  iRet;

	//Axis Stop
	for(int i = 0; i < 2; i++){
		//pmiAxCheckDone(CARD_NO, i, &nDone);
		//if(nDone == emRUNNING)
		//	iRet = pmiAxStop(CARD_NO, i);	
		 iRet = pmiAxEStop(CARD_NO, i);			//
	}

	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN ){
		G_MainWnd->m_ServerDlg.OnBnClickedBtnStop();
		G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
	}else if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){
		G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
	}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

	}

	//
	CString str;
	str.Format(L"EM STOP");
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
	G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
	G_SystemModeData.unSystemError = SYSTEM_ERROR;

	return 0;
}

int CInspectThread::holdAutoRun(void)
{
	//for (int i = 0; i < TOTAL_STAGE; i++)
	for (int i = 1; i < TOTAL_STAGE; i++)
	{
		holdBatch(i);
	}
	return 0;
}

int CInspectThread::resumeAutoRun(void)
{
	for (int i = 0; i < TOTAL_STAGE; i++)
	//for (int i = 1; i < TOTAL_STAGE; i++)
	{
		continueBatch(i);
	}
	return 0;
}

int CInspectThread::resetBatch(int batch)
{
	CString strBuff;
	//check if it ok to reset in manual mode

	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		m_fnSetBatchStep(batch, STEP0); 
		if (batch <= IN_VI1){ //IN CONVEYOR
			stStgBatch[IN_CNVYR].allow |= ST_IN_CNVYR_INTERLOCK[batch]; //allow CNVYR
			stStgBatch[IN_CNVYR].request &= ~ST_IN_CNVYR_INTERLOCK[batch]; //request CNVYR
		}
		else{ //OUT CONVEYOR
			stStgBatch[OUT_CNVYR].allow |= ST_IN_CNVYR_INTERLOCK[batch]; //allow CNVYR
			stStgBatch[OUT_CNVYR].request &= ~ST_IN_CNVYR_INTERLOCK[batch]; //request CNVYR
		}
	}
	else
	{
		//can not reset in auto mode
		strBuff.Format(_T(" Reset in Manual Mode "));
		AfxMessageBox(strBuff);
	}
	return 0;
}

int CInspectThread::holdBatch(int batch)
{

	if(m_fnGetBatchStep(batch) !=STEP0 && m_fnGetBatchStep(batch) !=STEP22 &&  m_fnGetBatchStep(batch) !=STEP19&&  m_fnGetBatchStep(batch) !=STEP20){
		m_fnSetBatchStep(batch, STEP22); //put in hold status
		m_fnSetOldBatchStep(batch, m_fnGetPrevBatchStep(batch));
		stStgBatch[batch].timer.clear();	
	}
	return 0;
}

int CInspectThread::continueBatch(int batch)
{
	if(m_fnGetBatchStep(batch) == STEP22){
		m_fnSetBatchStep(batch, m_fnGetOldBatchStep(batch)); //put it back in old step
		if(m_fnGetOldBatchStep(batch) == STEP17)
			m_fnSetBatchStep(batch, STEP16); // request SHUTTLE
	}
	if (m_fnGetBatchStep(batch) == STEP0 && getBatchStatus(batch) == FINISHED ) //m_PartData[batch].part == PART &&
	{
		m_fnSetBatchStep(batch, STEP16); // request SHUTTLE
	}
	return 0;
}

int CInspectThread::batchTimer(void)//100ms
{	
	for (int i = 0; i < TOTAL_STAGE; i++)
	{
		if(stStgBatch[i].timer.On == 1 ){
			if( stStgBatch[i].timer.Accu <  stStgBatch[i].timer.Set) 
				stStgBatch[i].timer.Accu++;
			else if(stStgBatch[i].timer.Accu >=  stStgBatch[i].timer.Set)
				stStgBatch[i].timer.OTE = 1;
		}
	}
	return 0;
}

void CInspectThread::turnLight(int idx, int sw)
{
	//System param
	//if(sw){
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LIGHT_1 + idx]);
	//}else{
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, 0);
	//}

	//Recipe param
	//if (sw){
	//	if (idx == 0)
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE]);
	//	else
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE]);
	//}
	//else{
	//	G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, MODE_LIGHT_1 + idx, 0);
	//}


	//for (int i = 0; i < sizeof(g_IsLIGHT_NOT_USE) / 8;i++)
	//	{
	//		if (idx == g_IsLIGHT_NOT_USE[i])
	//			return;
	//	}
	//if (sw)
	//	{
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight
	//			(G_MainWnd->m_SerialInterface.m_IsLIGHT_CAM_PORT[idx],
	//			G_MainWnd->m_SerialInterface.m_IsLIGHT_CAM_CHANNEL[idx],
	//			G_MainWnd->m_DataHandling.m_RecipeParam.iParam[G_MainWnd->m_SerialInterface.m_IsLIGHT_CAM_PARAM[idx]]
	//			);
	//	}
	//else
	//	{
	//		G_MainWnd->m_SerialInterface.m_fnSendToComportLight
	//			(G_MainWnd->m_SerialInterface.m_IsLIGHT_CAM_PORT[idx],
	//			G_MainWnd->m_SerialInterface.m_IsLIGHT_CAM_CHANNEL[idx],
	//				0
	//			);
	//	}
	int aa = idx;
	int bb = sw;
//	G_MainWnd->m_SerialInterface.LIGHTONOFF2(aa, bb);
	G_MainWnd->m_SerialInterface.LIGHTONOFF(aa, bb);
	return;
}
//
int CInspectThread::summarizeResult(void)
{
	CString str;
	
	if (m_PartData[OUT_NGOUT].results[CAM_PCHI] == VI_OK && m_PartData[OUT_NGOUT].results[CAM_PCHI2] == VI_OK &&
		m_PartData[OUT_NGOUT].results[CAM_PCCA] == VI_OK && m_PartData[OUT_NGOUT].results[CAM_PCCA2] == VI_OK)
	{
		m_PartData[OUT_NGOUT].finalPass = 1;//PASS
	}else{
		m_PartData[OUT_NGOUT].finalPass = 0;
	}

	return m_PartData[OUT_NGOUT].finalPass;
}

int CInspectThread::setBatchStatus(int batch, int status)
{
	CString str;
	stStgBatch[batch].status = status;

	//STORE STEP IN FLASH DATA
	G_MainWnd->m_DataHandling.m_FlashData.data[STATUS_INPUT + batch] = status;
	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[STATUS_INPUT + batch]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[STATUS_INPUT + batch], str);

	return 0;
}

int CInspectThread::getBatchStatus(int batch)
{
	//int status = G_MainWnd->m_DataHandling.m_FlashData.data[STATUS_INPUT + batch];
	//stStgBatch[batch].status = status;
	//return status;
	return stStgBatch[batch].status;
}

int CInspectThread::restoreInspectionStatus(void)
{
	int i, step;
	for (i = INPUT; i <= OUT_CNVYR; i++)
	{
		m_fnSetBatchStep(i, G_MainWnd->m_DataHandling.m_FlashData.data[STEP_INPUT + i]);
		setBatchStatus(i, G_MainWnd->m_DataHandling.m_FlashData.data[STATUS_INPUT + i]);
	}

	//stStgBatch[IN_CNVYR].allow = G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_ALLOW];
	//stStgBatch[IN_CNVYR].request = G_MainWnd->m_DataHandling.m_FlashData.data[IN_CNVYR_REQUEST];
	//stStgBatch[OUT_CNVYR].allow = G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_ALLOW];
	//stStgBatch[OUT_CNVYR].request = G_MainWnd->m_DataHandling.m_FlashData.data[OUT_CNVYR_REQUEST];

	//Conveyor allow, request
	for (i = INPUT; i <= IN_BUFFER; i++)
	{
		step = m_fnGetBatchStep(i);
		if (step == STEP17)m_fnSetBatchStep(i, STEP16);
		if (step == STEP0 || step == STEP15 || step == STEP16 || step == STEP17)
			stStgBatch[IN_CNVYR].allow |= ST_IN_CNVYR_INTERLOCK[i]; // 0x0001 << i;
	}

	//Conveyor allow, request

	for (i = OUT_FLIPDROP; i <= OUT_NGOUT; i++)
	{
		step = m_fnGetBatchStep(i);
		if (step == STEP17)m_fnSetBatchStep(i, STEP16);
		if (step == STEP0 || step == STEP15 || step == STEP16 || step == STEP17)
			stStgBatch[OUT_CNVYR].allow |= ST_IN_CNVYR_INTERLOCK[i];  //0x0001 << i;
	}

	return 0;
}

int CInspectThread::errorRoutine(void)
{

	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN )
	{
		holdAutoRun();
		checkStatus = CHECK_SUSPEND;
	}
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO )
	{
		checkStatus = CHECK_SUSPEND;
	}
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	G_SystemModeData.unSystemError = SYSTEM_ERROR;
	G_MainWnd->m_ServerDlg.m_btnAuto.SetColorChange(WHITE, BLACK);
	G_MainWnd->m_ServerDlg.m_btnManual.SetColorChange(GREEN, BLACK);
	G_MainWnd->m_ServerDlg.m_btnStart.SetColorChange(WHITE, BLACK);
	G_MainWnd->m_ServerDlg.m_btnStop.SetColorChange(WHITE, BLACK);

	
	//TOWER LAMP ALARM

	return 0;
}

int CInspectThread::errorResetRoutine(void)
{
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, RGB(230,230,230));
	G_SystemModeData.unSystemError = SYSTEM_OK;
	G_MainWnd->m_PIODlg.buzzstop = 0;
	m_vi.m_nNoCoreCount = 0;
	checkStatus = 0;
	return 0;
}


int CInspectThread::showNGResult(void)
{

	return 0;
}



int CInspectThread::m_fnBeginThread(void)
{
	for (int i = 0; i < STAGE_THREAD_COUNT; i++)
	{
		isThreadRunning[i] = TRUE;
	}

	m_pThreads[STAGE_THREAD_INPUT] = AfxBeginThread(m_fnThreadStgInput, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL); // THREAD_PRIORITY_NORMAL
	m_pThreads[STAGE_THREAD_INPUT_BUFFER] = AfxBeginThread(m_fnThreadStgInputBuffer, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL);
	m_pThreads[STAGE_THREAD_FLIP_DROP] = AfxBeginThread(m_fnThreadStgFlipDrop, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL);
	m_pThreads[STAGE_THREAD_OUT_CENTERING] = AfxBeginThread(m_fnThreadStgOutCentering, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL);
	m_pThreads[STAGE_THREAD_CONVEYER] = AfxBeginThread(m_fnThreadStgConveyer, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL);
	m_pThreads[STAGE_THREAD_CAM_0] = AfxBeginThread(m_fnThreadStgCam0, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL); //20160315_KYS_VI0
	m_pThreads[STAGE_THREAD_CAM_1] = AfxBeginThread(m_fnThreadStgCam1, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL); //20160315_KYS_VI1
	m_pThreads[STAGE_THREAD_CAM_2] = AfxBeginThread(m_fnThreadStgCam2, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL); //20160315_KYS_VI2
	m_pThreads[STAGE_THREAD_CAM_3] = AfxBeginThread(m_fnThreadStgCam3, (LPVOID)this, THREAD_PRIORITY_NORMAL /*THREAD_PRIORITY_HIGHEST*/, 0, CREATE_SUSPENDED, NULL); //20160315_KYS_VI3

	return 0;
}


int CInspectThread::m_fnEndThread()
{
	for (int i = 0; i < STAGE_THREAD_COUNT; i++)
	{
		do{
			G_MainWnd->m_InspectThread.suspendCnt[i] = G_MainWnd->m_InspectThread.m_pThreads[i]->ResumeThread();
		} while (G_MainWnd->m_InspectThread.suspendCnt[i] > 0);

		isThreadRunning[i] = FALSE;
		::WaitForSingleObject(m_pThreads[i]->m_hThread, 5000); 
		m_pThreads[i] = NULL;
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////
// Batch Thread

UINT m_fnThreadStgInput(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *) lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_INPUT;

	while (it->isThreadRunning[nThreadIndex])
	{
		//INPUT
		if (it->checkStatus == CHECK_SUSPEND){
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(INPUT);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		if (it->m_fnGetBatchStep(INPUT) != STEP0){ ret = it->stg0Input(); }

		//Auto Run 
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			//STG0 LOAD AUTO START
			if (it->m_fnGetBatchStep(INPUT) == STEP0  && it->getBatchStatus(INPUT) == CLEAR  && //READY STATE
#ifndef 	NO_CORE_RUN
				//it->m_pPIO->readDI(IN_LOAD_DETECT, CARD_IO) == 0 &&
				it->m_pPIO->readDI(IN_LOAD_DETECT, CARD_IO) == 1 &&
#endif
				it->m_pPIO->readDI(IN_LOAD_STOP, CARD_IO) == 1 &&
				it->getBatchStatus(IN_CNVYR) != RUN
				)
			{
				if (!it->stStgBatch[INPUT].timer.On){
					it->stStgBatch[INPUT].timer.On = 1;
					it->stStgBatch[INPUT].timer.Set = T_WAIT;
				}
				else{
					if (it->stStgBatch[INPUT].timer.OTE == 1)//LOAD AUTO START
					{
						it->stStgBatch[INPUT].timer.clear();
						it->m_fnSetBatchStep(INPUT, STEP1);
					}
				}
			}
			else{
				if (it->m_fnGetBatchStep(INPUT) == STEP0  && it->stStgBatch[INPUT].timer.On){
					it->stStgBatch[INPUT].timer.clear();
				}
			}
		}

		//TIMER
		if (it->stStgBatch[INPUT].timer.On == 1){
			if (it->stStgBatch[INPUT].timer.Accu <  it->stStgBatch[INPUT].timer.Set)
				it->stStgBatch[INPUT].timer.Accu++;
			else if (it->stStgBatch[INPUT].timer.Accu >= it->stStgBatch[INPUT].timer.Set)
				it->stStgBatch[INPUT].timer.OTE = 1;
		}

		if (it->stStgBatch[INPUT].counter.On == 1){
			if (it->stStgBatch[INPUT].counter.Accu <  it->stStgBatch[INPUT].counter.Set)
				it->stStgBatch[INPUT].counter.Accu++;
			else if (it->stStgBatch[INPUT].counter.Accu >= it->stStgBatch[INPUT].counter.Set)
				it->stStgBatch[INPUT].counter.OTE = 1;
		}

		////STG1 IN_CENTERING
		if (it->m_fnGetBatchStep(IN_CENTERING) != STEP0){ ret = it->stg1Center(); }

		//Auto Run 
		//STG1 IN_CENTERING
		if (it->m_fnGetBatchStep(IN_CENTERING) == STEP0  &&  it->getBatchStatus(IN_CENTERING) == CLEAR  &&
#ifndef _VI_ALIGN
			it->m_PartData[IN_CENTERING].part == PART &&
			// getBatchStatus(IN_CENTERING) == CLEAR &&
#else
			(it->m_PartData[IN_CENTERING].part == PART ||
			 it->m_PartData[IN_VI1].part == PART||
			 it->m_PartData[IN_VI0].part == PART ||
			 it->m_PartDataBuffer[0].part == PART||
			 it->m_PartDataBuffer[3].part == PART)&&
#endif
			it->getBatchStatus(IN_CNVYR) != RUN
			)
		{
			if (!it->stStgBatch[IN_CENTERING].timer.On){
				it->stStgBatch[IN_CENTERING].timer.On = 1;
				it->stStgBatch[IN_CENTERING].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[IN_CENTERING].timer.OTE == 1)//LOAD AUTO START
				{
					it->stStgBatch[IN_CENTERING].timer.clear();
					it->m_fnSetBatchStep(IN_CENTERING, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(IN_CENTERING) == STEP0  && it->stStgBatch[IN_CENTERING].timer.On){
				it->stStgBatch[IN_CENTERING].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[IN_CENTERING].timer.On == 1){
			if (it->stStgBatch[IN_CENTERING].timer.Accu <  it->stStgBatch[IN_CENTERING].timer.Set)
				it->stStgBatch[IN_CENTERING].timer.Accu++;
			else if (it->stStgBatch[IN_CENTERING].timer.Accu >= it->stStgBatch[IN_CENTERING].timer.Set)
				it->stStgBatch[IN_CENTERING].timer.OTE = 1;
		}

		if (it->stStgBatch[IN_CENTERING].counter.On == 1){
			if (it->stStgBatch[IN_CENTERING].counter.Accu <  it->stStgBatch[IN_CENTERING].counter.Set)
				it->stStgBatch[IN_CENTERING].counter.Accu++;
			else if (it->stStgBatch[IN_CENTERING].counter.Accu >= it->stStgBatch[IN_CENTERING].counter.Set)
				it->stStgBatch[IN_CENTERING].counter.OTE = 1;
		}


//		//STG2 IN_VI1
//		if (it->m_fnGetBatchStep(IN_VI1) != STEP0){ ret = it->stg2VI1(); }
//
//#ifndef _VI_ALIGN
//		//Auto Run 
//		//STG2 IN_VI1
//		if (it->m_fnGetBatchStep(IN_VI1) == STEP0  &&   it->getBatchStatus(IN_VI1) == CLEAR  &&
//			it->m_PartData[IN_VI1].part == PART &&
//			// getBatchStatus(IN_VI1) == CLEAR &&
//			//(it->getBatchStatus(IN_CENTERING) == ALIGNED || it->getBatchStatus(IN_CENTERING) == FINISHED) &&
//			it->getBatchStatus(IN_CNVYR) != RUN
//			)
//		{
//			if (!it->stStgBatch[IN_VI1].timer.On){
//				it->stStgBatch[IN_VI1].timer.On = 1;
//				it->stStgBatch[IN_VI1].timer.Set = T_WAIT;
//			}
//			else{
//				if (it->stStgBatch[IN_VI1].timer.OTE == 1)
//				{
//					it->stStgBatch[IN_VI1].timer.clear();
//					it->m_fnSetBatchStep(IN_VI1, STEP1);
//				}
//			}
//		}
//		else{
//			if (it->m_fnGetBatchStep(IN_VI1) == STEP0  && it->stStgBatch[IN_VI1].timer.On){
//				it->stStgBatch[IN_VI1].timer.clear();
//			}
//		}
//#endif
//
//		//TIMER
//		if (it->stStgBatch[IN_VI1].timer.On == 1){
//			if (it->stStgBatch[IN_VI1].timer.Accu <  it->stStgBatch[IN_VI1].timer.Set)
//				it->stStgBatch[IN_VI1].timer.Accu++;
//			else if (it->stStgBatch[IN_VI1].timer.Accu >= it->stStgBatch[IN_VI1].timer.Set)
//				it->stStgBatch[IN_VI1].timer.OTE = 1;
//		}
//
//		if (it->stStgBatch[IN_VI1].counter.On == 1){
//			if (it->stStgBatch[IN_VI1].counter.Accu <  it->stStgBatch[IN_VI1].counter.Set)
//				it->stStgBatch[IN_VI1].counter.Accu++;
//			else if (it->stStgBatch[IN_VI1].counter.Accu >= it->stStgBatch[IN_VI1].counter.Set)
//				it->stStgBatch[IN_VI1].counter.OTE = 1;
//		}

		Sleep(5);

	}

	return ret;
}

//STG3 IN_BUFFER
UINT m_fnThreadStgInputBuffer(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_INPUT_BUFFER;

	while (it->isThreadRunning[nThreadIndex])
	{

		if (it->checkStatus == CHECK_SUSPEND){
			//G_AddLog(3, L"Inspect Thread Stg3 Suspended");
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(IN_BUFFER);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		if (it->m_fnGetBatchStep(IN_BUFFER) != STEP0){ ret = it->stg3Buffer(); }

		//Auto Run 
		//STG3 IN_BUFFER
		if (it->m_fnGetBatchStep(IN_BUFFER) == STEP0  &&  it->getBatchStatus(IN_BUFFER) == CLEAR  &&
			it->m_PartData[IN_BUFFER].part == PART &&
			// getBatchStatus(IN_BUFFER) == CLEAR &&
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			if (!it->stStgBatch[IN_BUFFER].timer.On){
				it->stStgBatch[IN_BUFFER].timer.On = 1;
				it->stStgBatch[IN_BUFFER].timer.Set = T_WAIT;
			}
			else{
			if (it->stStgBatch[IN_BUFFER].timer.OTE == 1)
				{
					it->stStgBatch[IN_BUFFER].timer.clear();
					it->m_fnSetBatchStep(IN_BUFFER, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(IN_BUFFER) == STEP0  && it->stStgBatch[IN_BUFFER].timer.On){
				it->stStgBatch[IN_BUFFER].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[IN_BUFFER].timer.On == 1){
			if (it->stStgBatch[IN_BUFFER].timer.Accu <  it->stStgBatch[IN_BUFFER].timer.Set)
				it->stStgBatch[IN_BUFFER].timer.Accu++;
			else if (it->stStgBatch[IN_BUFFER].timer.Accu >= it->stStgBatch[IN_BUFFER].timer.Set)
				it->stStgBatch[IN_BUFFER].timer.OTE = 1;
		}

		if (it->stStgBatch[IN_BUFFER].counter.On == 1){
			if (it->stStgBatch[IN_BUFFER].counter.Accu <  it->stStgBatch[IN_BUFFER].counter.Set)
				it->stStgBatch[IN_BUFFER].counter.Accu++;
			else if (it->stStgBatch[IN_BUFFER].counter.Accu >= it->stStgBatch[IN_BUFFER].counter.Set)
				it->stStgBatch[IN_BUFFER].counter.OTE = 1;
		}

		Sleep(5);
	}

	return ret;
}

//STG3 FLIPDROP
UINT m_fnThreadStgFlipDrop(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_FLIP_DROP;

	while (it->isThreadRunning[nThreadIndex])
	{

		if (it->checkStatus == CHECK_SUSPEND){
			//G_AddLog(3, L"Inspect Thread Stg3 Suspended");
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(OUT_FLIPDROP);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		// STG4 OUT_FLIPDROP
		if (it->m_fnGetBatchStep(OUT_FLIPDROP) != STEP0){ ret = it->stg4Flip(); }

		//Auto Run 
		//STG4 OUT_FLIPDROP
		if (it->m_fnGetBatchStep(OUT_FLIPDROP) == STEP0  &&
			it->m_PartData[OUT_FLIPDROP].part == PART &&
			// getBatchStatus(OUT_FLIPDROP) == CLEAR &&
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			if (!it->stStgBatch[OUT_FLIPDROP].timer.On){
				it->stStgBatch[OUT_FLIPDROP].timer.On = 1;
				it->stStgBatch[OUT_FLIPDROP].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[OUT_FLIPDROP].timer.OTE == 1)
				{
					it->stStgBatch[OUT_FLIPDROP].timer.clear();
					it->m_fnSetBatchStep(OUT_FLIPDROP, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_FLIPDROP) == STEP0  && it->stStgBatch[OUT_FLIPDROP].timer.On){
				it->stStgBatch[OUT_FLIPDROP].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[OUT_FLIPDROP].timer.On == 1){
			if (it->stStgBatch[OUT_FLIPDROP].timer.Accu <  it->stStgBatch[OUT_FLIPDROP].timer.Set)
				it->stStgBatch[OUT_FLIPDROP].timer.Accu++;
			else if (it->stStgBatch[OUT_FLIPDROP].timer.Accu >= it->stStgBatch[OUT_FLIPDROP].timer.Set)
				it->stStgBatch[OUT_FLIPDROP].timer.OTE = 1;
		}

		if (it->stStgBatch[OUT_FLIPDROP].counter.On == 1){
			if (it->stStgBatch[OUT_FLIPDROP].counter.Accu <  it->stStgBatch[OUT_FLIPDROP].counter.Set)
				it->stStgBatch[OUT_FLIPDROP].counter.Accu++;
			else if (it->stStgBatch[OUT_FLIPDROP].counter.Accu >= it->stStgBatch[OUT_FLIPDROP].counter.Set)
				it->stStgBatch[OUT_FLIPDROP].counter.OTE = 1;
		}

		//Stopper
		////STG5 OUT_STOPPER
		//if (it->m_fnGetBatchStep(OUT_STOPPER) != STEP0){ ret = it->stg5Stopper(); }

		////Auto Run 
		////STG5 OUT_STOPPER
		//if (it->m_fnGetBatchStep(OUT_STOPPER) == STEP0  &&   it->getBatchStatus(OUT_STOPPER) == CLEAR  &&
		//	it->m_PartData[OUT_STOPPER].part == PART &&
		//	// getBatchStatus(OUT_STOPPER) == CLEAR &&
		//	it->getBatchStatus(OUT_CNVYR) != RUN
		//	)
		//{
		//	if (!it->stStgBatch[OUT_STOPPER].timer.On){
		//		it->stStgBatch[OUT_STOPPER].timer.On = 1;
		//		it->stStgBatch[OUT_STOPPER].timer.Set = T_WAIT;
		//	}
		//	else{
		//		if (it->stStgBatch[OUT_STOPPER].timer.OTE == 1)
		//		{
		//			it->stStgBatch[OUT_STOPPER].timer.clear();
		//			it->m_fnSetBatchStep(OUT_STOPPER, STEP1);
		//		}
		//	}
		//}
		//else{
		//	if (it->m_fnGetBatchStep(OUT_STOPPER) == STEP0  && it->stStgBatch[OUT_STOPPER].timer.On){
		//		it->stStgBatch[OUT_STOPPER].timer.clear();
		//	}
		//}

		////TIMER
		//if (it->stStgBatch[OUT_STOPPER].timer.On == 1){
		//	if (it->stStgBatch[OUT_STOPPER].timer.Accu <  it->stStgBatch[OUT_STOPPER].timer.Set)
		//		it->stStgBatch[OUT_STOPPER].timer.Accu++;
		//	else if (it->stStgBatch[OUT_STOPPER].timer.Accu >= it->stStgBatch[OUT_STOPPER].timer.Set)
		//		it->stStgBatch[OUT_STOPPER].timer.OTE = 1;
		//}

		//if (it->stStgBatch[OUT_STOPPER].counter.On == 1){
		//	if (it->stStgBatch[OUT_STOPPER].counter.Accu <  it->stStgBatch[OUT_STOPPER].counter.Set)
		//		it->stStgBatch[OUT_STOPPER].counter.Accu++;
		//	else if (it->stStgBatch[OUT_STOPPER].counter.Accu >= it->stStgBatch[OUT_STOPPER].counter.Set)
		//		it->stStgBatch[OUT_STOPPER].counter.OTE = 1;
		//}

		Sleep(5);
	}

	return ret;
}

//STG6 OUT_CENTERING
UINT m_fnThreadStgOutCentering(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_OUT_CENTERING;

	//20160229_kys
	//WCHAR threadflag[1];
	//wsprintf(threadflag, L"%d", static_cast<int>(it->isThreadRunning[2]));
	//G_MainWnd->m_ServerDlg.m_staticThreadFlag.SetText(threadflag, RED, BLACK);
	///////
	while (it->isThreadRunning[nThreadIndex])
	{
		//20160229_kys
		//WCHAR batch_num[1];
		//wsprintf(batch_num, L"%d", (int)(G_MainWnd->m_InspectThread.m_fnGetBatchStep(OUT_CENTERING)));
		//G_MainWnd->m_ServerDlg.m_staticThreadFlag.SetText(batch_num, RED, BLACK);
////////////////////////////////////////		
		
		if (it->checkStatus == CHECK_SUSPEND){
			//G_AddLog(3, L"Inspect Thread Stg6 Suspended");
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(OUT_CENTERING);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}
		
		if (it->m_fnGetBatchStep(OUT_CENTERING) != STEP0){ ret = it->stg6Center(); }
		
		//Auto Run 
		//STG5 OUT_CENTERING
		if (it->m_fnGetBatchStep(OUT_CENTERING) == STEP0  &&   it->getBatchStatus(OUT_CENTERING) == CLEAR  &&
			// getBatchStatus(OUT_CENTERING) == CLEAR &&
#ifndef _VI_ALIGN
			it->m_PartData[OUT_CENTERING].part == PART &&
			// getBatchStatus(IN_CENTERING) == CLEAR &&
#else
			(it->m_PartData[OUT_CENTERING].part == PART 
			|| it->m_PartDataBuffer[13].part == PART
			//|| it->m_PartDataBuffer[14].part == PART
			//|| it->m_PartDataBuffer[15].part == PART
			//|| it->m_PartDataBuffer[16].part == PART
			//|| it->m_PartDataBuffer[17].part == PART
			|| it->m_PartData[OUT_VI2].part == PART
			|| it->m_PartData[OUT_VI3].part == PART
			)&&
#endif
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			
			if (!it->stStgBatch[OUT_CENTERING].timer.On){
				it->stStgBatch[OUT_CENTERING].timer.On = 1;
				it->stStgBatch[OUT_CENTERING].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[OUT_CENTERING].timer.OTE == 1)
				{
					it->stStgBatch[OUT_CENTERING].timer.clear();
					it->m_fnSetBatchStep(OUT_CENTERING, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_CENTERING) == STEP0  && it->stStgBatch[OUT_CENTERING].timer.On){
				it->stStgBatch[OUT_CENTERING].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[OUT_CENTERING].timer.On == 1){
			if (it->stStgBatch[OUT_CENTERING].timer.Accu <  it->stStgBatch[OUT_CENTERING].timer.Set)
				it->stStgBatch[OUT_CENTERING].timer.Accu++;
			else if (it->stStgBatch[OUT_CENTERING].timer.Accu >= it->stStgBatch[OUT_CENTERING].timer.Set)
				it->stStgBatch[OUT_CENTERING].timer.OTE = 1;
		}

		if (it->stStgBatch[OUT_CENTERING].counter.On == 1){
			if (it->stStgBatch[OUT_CENTERING].counter.Accu <  it->stStgBatch[OUT_CENTERING].counter.Set)
				it->stStgBatch[OUT_CENTERING].counter.Accu++;
			else if (it->stStgBatch[OUT_CENTERING].counter.Accu >= it->stStgBatch[OUT_CENTERING].counter.Set)
				it->stStgBatch[OUT_CENTERING].counter.OTE = 1;
		}

//		//STG7 OUT_VI2
//		if (it->m_fnGetBatchStep(OUT_VI2) != STEP0){ ret = it->stg7VI2(); }
//
//		//Auto Run 
//		//STG6 OUT_VI2
//		if (it->m_fnGetBatchStep(OUT_VI2) == STEP0 && it->getBatchStatus(OUT_VI2) == CLEAR  &&
//			it->m_PartData[OUT_VI2].part == PART &&
////#ifdef _VI_ALIGN
////			(it->getBatchStatus(OUT_CENTERING) == ALIGNED || it->getBatchStatus(OUT_CENTERING) == FINISHED) &&
////#endif
//			// getBatchStatus(OUT_VI2) == CLEAR &&
//			it->getBatchStatus(OUT_CNVYR) != RUN
//			)
//		{
//			if (!it->stStgBatch[OUT_VI2].timer.On){
//				it->stStgBatch[OUT_VI2].timer.On = 1;
//				it->stStgBatch[OUT_VI2].timer.Set = T_WAIT;
//			}
//			else{
//				if (it->stStgBatch[OUT_VI2].timer.OTE == 1)
//				{
//					it->stStgBatch[OUT_VI2].timer.clear();
//					it->m_fnSetBatchStep(OUT_VI2, STEP1);
//				}
//			}
//		}
//		else{
//			if (it->m_fnGetBatchStep(OUT_VI2) == STEP0  && it->stStgBatch[OUT_VI2].timer.On){
//				it->stStgBatch[OUT_VI2].timer.clear();
//			}
//		}
//
//		//TIMER
//		if (it->stStgBatch[OUT_VI2].timer.On == 1){
//			if (it->stStgBatch[OUT_VI2].timer.Accu <  it->stStgBatch[OUT_VI2].timer.Set)
//				it->stStgBatch[OUT_VI2].timer.Accu++;
//			else if (it->stStgBatch[OUT_VI2].timer.Accu >= it->stStgBatch[OUT_VI2].timer.Set)
//				it->stStgBatch[OUT_VI2].timer.OTE = 1;
//		}
//
//		if (it->stStgBatch[OUT_VI2].counter.On == 1){
//			if (it->stStgBatch[OUT_VI2].counter.Accu <  it->stStgBatch[OUT_VI2].counter.Set)
//				it->stStgBatch[OUT_VI2].counter.Accu++;
//			else if (it->stStgBatch[OUT_VI2].counter.Accu >= it->stStgBatch[OUT_VI2].counter.Set)
//				it->stStgBatch[OUT_VI2].counter.OTE = 1;
//		}
//
		//STG8 OUT_NGOUT
		if (it->m_fnGetBatchStep(OUT_NGOUT) != STEP0){ ret = it->stg8NGout(); }

		//Auto Run 
		//STG7 OUT_NGOUT
		if (it->m_fnGetBatchStep(OUT_NGOUT) == STEP0  &&  it->getBatchStatus(OUT_NGOUT) == CLEAR  &&
			it->m_PartData[OUT_NGOUT].part == PART &&
			// getBatchStatus(OUT_NGOUT) == CLEAR &&
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			if (!it->stStgBatch[OUT_NGOUT].timer.On){
				it->stStgBatch[OUT_NGOUT].timer.On = 1;
				it->stStgBatch[OUT_NGOUT].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[OUT_NGOUT].timer.OTE == 1)
				{
					it->stStgBatch[OUT_NGOUT].timer.clear();
					it->m_fnSetBatchStep(OUT_NGOUT, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_NGOUT) == STEP0  && it->stStgBatch[OUT_NGOUT].timer.On){
				it->stStgBatch[OUT_NGOUT].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[OUT_NGOUT].timer.On == 1){
			if (it->stStgBatch[OUT_NGOUT].timer.Accu <  it->stStgBatch[OUT_NGOUT].timer.Set)
				it->stStgBatch[OUT_NGOUT].timer.Accu++;
			else if (it->stStgBatch[OUT_NGOUT].timer.Accu >= it->stStgBatch[OUT_NGOUT].timer.Set)
				it->stStgBatch[OUT_NGOUT].timer.OTE = 1;
		}

		if (it->stStgBatch[OUT_NGOUT].counter.On == 1){
			if (it->stStgBatch[OUT_NGOUT].counter.Accu <  it->stStgBatch[OUT_NGOUT].counter.Set)
				it->stStgBatch[OUT_NGOUT].counter.Accu++;
			else if (it->stStgBatch[OUT_NGOUT].counter.Accu >= it->stStgBatch[OUT_NGOUT].counter.Set)
				it->stStgBatch[OUT_NGOUT].counter.OTE = 1;
		}

		Sleep(5);
	}

	return ret;
}

//STG9 IN_OUT_CNVYR
UINT m_fnThreadStgConveyer(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_CONVEYER;

	while (it->isThreadRunning[nThreadIndex])
	{

		if (it->checkStatus == CHECK_SUSPEND){
			//G_AddLog(3, L"Inspect Thread Suspended");
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(IN_CNVYR);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		if (it->m_fnGetBatchStep(IN_CNVYR) != STEP0){ ret = it->stg9InConveyorRun(); }

		//Auto Run 
		//IN_CNVYR BATCH AUTO START
		if (
			it->stStgBatch[IN_CNVYR].myturn == IN_CNVYR_TURN && 
			it->m_fnGetBatchStep(IN_CNVYR) == STEP0 &&
			it->getBatchStatus(OUT_CNVYR) != RUN &&
			it->m_PartData[OUT_FLIPDROP].part != PART &&
			it->stStgBatch[IN_CNVYR].holdShuttle == 0 &&
			(((it->m_PartData[INPUT].part == PART || it->m_PartData[IN_CENTERING].part == PART || it->m_PartData[IN_VI1].part == PART || it->m_PartData[IN_VI0].part == PART || it->m_PartData[IN_BUFFER].part == PART) &&
			(it->stStgBatch[IN_CNVYR].allow & CNVYR_IN_ALL) == CNVYR_IN_ALL && (it->stStgBatch[IN_CNVYR].request > 0)) ||
			// when part is in buffer stg only
			(it->m_PartData[INPUT].part != PART && it->m_PartData[IN_CENTERING].part != PART && it->m_PartData[IN_VI1].part != PART&& it->m_PartData[IN_VI0].part != PART && it->m_PartData[IN_BUFFER].part != PART)
			)
			)
		{
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
				if (!it->stStgBatch[IN_CNVYR].timer.On){
					it->stStgBatch[IN_CNVYR].timer.On = 1;
					it->stStgBatch[IN_CNVYR].timer.Set = T_WAIT;
				}
				else{
					if (it->stStgBatch[IN_CNVYR].timer.OTE == 1)
					{
						it->stStgBatch[IN_CNVYR].timer.clear();

						it->m_fnSetBatchStep(IN_CNVYR, STEP1); //Start
					}
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(IN_CNVYR) == STEP0 && it->stStgBatch[IN_CNVYR].timer.On){
				//it->stStgBatch[IN_CNVYR].status = CLEAR;
				it->stStgBatch[IN_CNVYR].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[IN_CNVYR].timer.On == 1){
			if (it->stStgBatch[IN_CNVYR].timer.Accu <  it->stStgBatch[IN_CNVYR].timer.Set)
				it->stStgBatch[IN_CNVYR].timer.Accu++;
			else if (it->stStgBatch[IN_CNVYR].timer.Accu >= it->stStgBatch[IN_CNVYR].timer.Set)
				it->stStgBatch[IN_CNVYR].timer.OTE = 1;
		}

		//2016.04.05 jec add  NOT USE Counter
		if (it->stStgBatch[IN_CNVYR].counter.On == 1){
			if (it->stStgBatch[IN_CNVYR].counter.Accu <  it->stStgBatch[IN_CNVYR].counter.Set)
				it->stStgBatch[IN_CNVYR].counter.Accu++;
			else if (it->stStgBatch[IN_CNVYR].counter.Accu >= it->stStgBatch[IN_CNVYR].counter.Set)
				it->stStgBatch[IN_CNVYR].counter.OTE = 1;
		}

		//STG10 OUT_CNVYR
		if (it->m_fnGetBatchStep(OUT_CNVYR) != STEP0){ ret = it->stg10OutConveyorRun(); }

		//Auto Run 
		//OUT_CNVYR BATCH AUTO START
		if (
			it->stStgBatch[IN_CNVYR].myturn != IN_CNVYR_TURN && // OUT_CNVYR_TURN
			it->m_fnGetBatchStep(OUT_CNVYR) == STEP0 &&
			it->getBatchStatus(IN_CNVYR) != RUN &&
			it->stStgBatch[OUT_CNVYR].holdShuttle == 0 &&
			(((it->m_PartData[OUT_FLIPDROP].part == PART || it->m_PartData[OUT_CENTERING].part == PART || it->m_PartData[OUT_VI2].part == PART || it->m_PartData[OUT_VI3].part == PART || it->m_PartData[OUT_NGOUT].part == PART) &&
			(it->stStgBatch[OUT_CNVYR].allow & CNVYR_OUT_ALL) == CNVYR_OUT_ALL && 	it->stStgBatch[OUT_CNVYR].request > 0) ||
			// when part is in buffer stg only
			(it->m_PartData[OUT_FLIPDROP].part != PART && it->m_PartData[OUT_CENTERING].part != PART && it->m_PartData[OUT_VI2].part != PART && it->m_PartData[OUT_VI3].part != PART &&  it->m_PartData[OUT_NGOUT].part != PART)
		))
		{
			if (!it->stStgBatch[OUT_CNVYR].timer.On){
				it->stStgBatch[OUT_CNVYR].timer.On = 1;
				//20160224_kys_outconv_start_time_delay
				//it->stStgBatch[OUT_CNVYR].timer.Set = T_WAIT*100; //AFTER OUT_FLIPDROP DONE
				//it->stStgBatch[OUT_CNVYR].timer.Set = T_WAIT; //AFTER OUT_FLIPDROP DONE
				it->stStgBatch[OUT_CNVYR].timer.Set = 50; //AFTER OUT_FLIPDROP DONE,20  // Out Conveyor rotate Delay,,,concern about filpper fault
			}
			else{
				if (it->stStgBatch[OUT_CNVYR].timer.OTE == 1)
				{
					it->stStgBatch[OUT_CNVYR].timer.clear();
					if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					it->m_fnSetBatchStep(OUT_CNVYR, STEP1); //Start
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_CNVYR) == STEP0 && it->stStgBatch[OUT_CNVYR].timer.On){
				//it->stStgBatch[OUT_CNVYR].status = CLEAR;
				it->stStgBatch[OUT_CNVYR].timer.clear();
			}
		}

		//TIMER
		if (it->stStgBatch[OUT_CNVYR].timer.On == 1){
			if (it->stStgBatch[OUT_CNVYR].timer.Accu <  it->stStgBatch[OUT_CNVYR].timer.Set)
				it->stStgBatch[OUT_CNVYR].timer.Accu++;
			else if (it->stStgBatch[OUT_CNVYR].timer.Accu >= it->stStgBatch[OUT_CNVYR].timer.Set)
				it->stStgBatch[OUT_CNVYR].timer.OTE = 1;
		}

		//2016.04.05 jec add  NOT USE Counter
		if (it->stStgBatch[OUT_CNVYR].counter.On == 1){
			if (it->stStgBatch[OUT_CNVYR].counter.Accu <  it->stStgBatch[OUT_CNVYR].counter.Set)
				it->stStgBatch[OUT_CNVYR].counter.Accu++;
			else if (it->stStgBatch[OUT_CNVYR].counter.Accu >= it->stStgBatch[OUT_CNVYR].counter.Set)
				it->stStgBatch[OUT_CNVYR].counter.OTE = 1;
		}
		Sleep(5);
	}

	return ret;
}

//20160315_KYS_ADD_VI0
UINT m_fnThreadStgCam0(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_CAM_0;

	while (it->isThreadRunning[nThreadIndex])
	{
		//VI0
		if (it->checkStatus == CHECK_SUSPEND){
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(IN_VI0);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}
//
		//STG11 IN_VI0
		if (it->m_fnGetBatchStep(IN_VI0) != STEP0){ ret = it->stg11VI0(); }

#ifndef _VI_ALIGN
		//Auto Run 
		//STG11 IN_VI0
		if (it->m_fnGetBatchStep(IN_VI0) == STEP0  &&   it->getBatchStatus(IN_VI0) == CLEAR  &&
			it->m_PartData[IN_VI0].part == PART &&
			// getBatchStatus(IN_VI1) == CLEAR &&
			//(it->getBatchStatus(IN_CENTERING) == ALIGNED || it->getBatchStatus(IN_CENTERING) == FINISHED) &&
			it->getBatchStatus(IN_CNVYR) != RUN
			)
		{
			//2016.04.05 jec edit
			//it->stStgBatch[IN_VI0].timer.clear();
			//it->m_fnSetBatchStep(IN_VI0, STEP1);
			if (!it->stStgBatch[IN_VI0].timer.On){
				it->stStgBatch[IN_VI0].timer.On = 1;
				it->stStgBatch[IN_VI0].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[IN_VI0].timer.OTE == 1)
				{
					it->stStgBatch[IN_VI0].timer.clear();
					it->m_fnSetBatchStep(IN_VI0, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(IN_VI0) == STEP0  && it->stStgBatch[IN_VI0].timer.On){
				it->stStgBatch[IN_VI0].timer.clear();
			}
		}
#endif

		//TIMER
		if (it->stStgBatch[IN_VI0].timer.On == 1){
			if (it->stStgBatch[IN_VI0].timer.Accu <  it->stStgBatch[IN_VI0].timer.Set)
				it->stStgBatch[IN_VI0].timer.Accu++;
			else if (it->stStgBatch[IN_VI0].timer.Accu >= it->stStgBatch[IN_VI0].timer.Set)
				it->stStgBatch[IN_VI0].timer.OTE = 1;
		}

		if (it->stStgBatch[IN_VI0].counter.On == 1){
			if (it->stStgBatch[IN_VI0].counter.Accu <  it->stStgBatch[IN_VI0].counter.Set)
				it->stStgBatch[IN_VI0].counter.Accu++;
			else if (it->stStgBatch[IN_VI0].counter.Accu >= it->stStgBatch[IN_VI0].counter.Set)
				it->stStgBatch[IN_VI0].counter.OTE = 1;
		}
		Sleep(1);
	}

	return ret;
}

//20160315_KYS_ADD_VI1
UINT m_fnThreadStgCam1(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_CAM_1;

	while (it->isThreadRunning[nThreadIndex])
	{
		//VI1
		if (it->checkStatus == CHECK_SUSPEND){
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(IN_VI1);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}
		//
		//STG2 IN_VI1
		if (it->m_fnGetBatchStep(IN_VI1) != STEP0){ ret = it->stg2VI1(); }

#ifndef _VI_ALIGN
		//Auto Run 
		//STG2 IN_VI1
		if (it->m_fnGetBatchStep(IN_VI1) == STEP0  &&   it->getBatchStatus(IN_VI1) == CLEAR  &&
			it->m_PartData[IN_VI1].part == PART &&
			// getBatchStatus(IN_VI1) == CLEAR &&
			//(it->getBatchStatus(IN_CENTERING) == ALIGNED || it->getBatchStatus(IN_CENTERING) == FINISHED) &&
			it->getBatchStatus(IN_CNVYR) != RUN
			)
		{
			//it->stStgBatch[IN_VI1].timer.clear();
			//it->m_fnSetBatchStep(IN_VI1, STEP1);
			if (!it->stStgBatch[IN_VI1].timer.On){
				it->stStgBatch[IN_VI1].timer.On = 1;
				it->stStgBatch[IN_VI1].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[IN_VI1].timer.OTE == 1)
				{
					it->stStgBatch[IN_VI1].timer.clear();
					it->m_fnSetBatchStep(IN_VI1, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(IN_VI1) == STEP0  && it->stStgBatch[IN_VI1].timer.On){
				it->stStgBatch[IN_VI1].timer.clear();
			}
		}
#endif

		//TIMER
		if (it->stStgBatch[IN_VI1].timer.On == 1){
			if (it->stStgBatch[IN_VI1].timer.Accu <  it->stStgBatch[IN_VI1].timer.Set)
				it->stStgBatch[IN_VI1].timer.Accu++;
			else if (it->stStgBatch[IN_VI1].timer.Accu >= it->stStgBatch[IN_VI1].timer.Set)
				it->stStgBatch[IN_VI1].timer.OTE = 1;
		}
		//2016.04.05 jec add  NOT USE Counter
		if (it->stStgBatch[IN_VI1].counter.On == 1){
			if (it->stStgBatch[IN_VI1].counter.Accu <  it->stStgBatch[IN_VI1].counter.Set)
				it->stStgBatch[IN_VI1].counter.Accu++;
			else if (it->stStgBatch[IN_VI1].counter.Accu >= it->stStgBatch[IN_VI1].counter.Set)
				it->stStgBatch[IN_VI1].counter.OTE = 1;
		}
		Sleep(1);
	}

	return ret;
}

//20160315_KYS_ADD_VI2
UINT m_fnThreadStgCam2(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_CAM_2;

	while (it->isThreadRunning[nThreadIndex])
	{
		//VI2
		if (it->checkStatus == CHECK_SUSPEND){
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(OUT_VI2);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}
		//
		//STG7 OUT_VI2
		if (it->m_fnGetBatchStep(OUT_VI2) != STEP0){ ret = it->stg7VI2(); }

#ifndef _VI_ALIGN
		//Auto Run 
		//STG7 OUT_VI2
		if (it->m_fnGetBatchStep(OUT_VI2) == STEP0  &&   it->getBatchStatus(OUT_VI2) == CLEAR  &&
			it->m_PartData[OUT_VI2].part == PART &&
			// getBatchStatus(IN_VI1) == CLEAR &&
			//(it->getBatchStatus(IN_CENTERING) == ALIGNED || it->getBatchStatus(IN_CENTERING) == FINISHED) &&
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			//2016.04.05 jec edit
			//it->stStgBatch[OUT_VI2].timer.clear();
			//it->m_fnSetBatchStep(OUT_VI2, STEP1);

			if (!it->stStgBatch[OUT_VI2].timer.On){
				it->stStgBatch[OUT_VI2].timer.On = 1;
				it->stStgBatch[OUT_VI2].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[OUT_VI2].timer.OTE == 1)
				{
					it->stStgBatch[OUT_VI2].timer.clear();
					it->m_fnSetBatchStep(OUT_VI2, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_VI2) == STEP0  && it->stStgBatch[OUT_VI2].timer.On){
				it->stStgBatch[OUT_VI2].timer.clear();
			}
		}
#endif

		//TIMER
		if (it->stStgBatch[OUT_VI2].timer.On == 1){
			if (it->stStgBatch[OUT_VI2].timer.Accu <  it->stStgBatch[OUT_VI2].timer.Set)
				it->stStgBatch[OUT_VI2].timer.Accu++;
			else if (it->stStgBatch[OUT_VI2].timer.Accu >= it->stStgBatch[OUT_VI2].timer.Set)
				it->stStgBatch[OUT_VI2].timer.OTE = 1;
		}

		if (it->stStgBatch[OUT_VI2].counter.On == 1){
			if (it->stStgBatch[OUT_VI2].counter.Accu <  it->stStgBatch[OUT_VI2].counter.Set)
				it->stStgBatch[OUT_VI2].counter.Accu++;
			else if (it->stStgBatch[OUT_VI2].counter.Accu >= it->stStgBatch[OUT_VI2].counter.Set)
				it->stStgBatch[OUT_VI2].counter.OTE = 1;
		}
		Sleep(1);
	}

	return ret;
}

//20160315_KYS_ADD_VI3
UINT m_fnThreadStgCam3(LPVOID lParam)
{
	CInspectThread *it = (CInspectThread *)lParam;

	int ret = 0L;
	int nThreadIndex = STAGE_THREAD_CAM_3;

	while (it->isThreadRunning[nThreadIndex])
	{
		//VI3
		if (it->checkStatus == CHECK_SUSPEND){
			if (it->isThreadRunning[nThreadIndex])
				it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}

		//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){ //TODO
		if (it->checkStatus == CHECK_NG){
			//manual mode
			if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			{
				it->holdBatch(OUT_VI3);
			}
			it->suspendCnt[nThreadIndex] = it->m_pThreads[nThreadIndex]->SuspendThread();
		}
		//
		//STG12 OUT_VI3
		if (it->m_fnGetBatchStep(OUT_VI3) != STEP0){ ret = it->stg12VI3(); }

#ifndef _VI_ALIGN
		//Auto Run 
		//STG12 OUT_VI3
		if (it->m_fnGetBatchStep(OUT_VI3) == STEP0  &&   it->getBatchStatus(OUT_VI3) == CLEAR  &&
			it->m_PartData[OUT_VI3].part == PART &&
			// getBatchStatus(IN_VI1) == CLEAR &&
			//(it->getBatchStatus(IN_CENTERING) == ALIGNED || it->getBatchStatus(IN_CENTERING) == FINISHED) &&
			it->getBatchStatus(OUT_CNVYR) != RUN
			)
		{
			//it->stStgBatch[OUT_VI3].timer.clear();
			//it->m_fnSetBatchStep(OUT_VI3, STEP1);
			if (!it->stStgBatch[OUT_VI3].timer.On){
				it->stStgBatch[OUT_VI3].timer.On = 1;
				it->stStgBatch[OUT_VI3].timer.Set = T_WAIT;
			}
			else{
				if (it->stStgBatch[OUT_VI3].timer.OTE == 1)
				{
					it->stStgBatch[OUT_VI3].timer.clear();
					it->m_fnSetBatchStep(OUT_VI3, STEP1);
				}
			}
		}
		else{
			if (it->m_fnGetBatchStep(OUT_VI3) == STEP0  && it->stStgBatch[OUT_VI3].timer.On){
				it->stStgBatch[OUT_VI3].timer.clear();
			}
		}
#endif

		//TIMER
		if (it->stStgBatch[OUT_VI3].timer.On == 1){
			if (it->stStgBatch[OUT_VI3].timer.Accu <  it->stStgBatch[OUT_VI3].timer.Set)
				it->stStgBatch[OUT_VI3].timer.Accu++;
			else if (it->stStgBatch[OUT_VI3].timer.Accu >= it->stStgBatch[OUT_VI3].timer.Set)
				it->stStgBatch[OUT_VI3].timer.OTE = 1;
		}

		//2016.04.05 jec add  NOT USE Counter
		if (it->stStgBatch[OUT_VI3].counter.On == 1){
			if (it->stStgBatch[OUT_VI3].counter.Accu <  it->stStgBatch[OUT_VI3].counter.Set)
				it->stStgBatch[OUT_VI3].counter.Accu++;
			else if (it->stStgBatch[OUT_VI3].counter.Accu >= it->stStgBatch[OUT_VI3].counter.Set)
				it->stStgBatch[OUT_VI3].counter.OTE = 1;
		}
		Sleep(1);
	}

	return ret;
}

int CInspectThread::m_fnClearParts()
{
	for (int batch = INPUT; batch <= OUT_CNVYR; batch++)
	{
		stStgBatch[batch].status = CLEAR;

			m_csCheck.Lock();
			m_fnSetBatchStep(batch, STEP0);
			m_fnSetPrevBatchStep(batch, STEP0);
			m_fnSetOldBatchStep(batch, STEP0);
			setBatchStatus(batch, CLEAR);
			if (batch < OUT_FLIPDROP)
				//|| batch == IN_VI0)   //kys_add
			{
				m_fnAllowConveyor(IN_CNVYR, TRUE, batch);
				m_fnRequestConveyor(IN_CNVYR, FALSE, batch);
				m_PartData[batch].part = CLEAR;
			}
			if (batch >= OUT_FLIPDROP && batch < IN_CNVYR)
				//|| batch == OUT_VI3)
			{
				m_fnAllowConveyor(OUT_CNVYR, TRUE, batch);
				m_fnRequestConveyor(OUT_CNVYR, FALSE, batch);
				m_PartData[batch].part = CLEAR;
			}
			//m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
			m_csCheck.Unlock();
			G_MainWnd->m_PIODlg.m_btnStepClear[batch].SetWindowText(L"0");

			//Inspection Part Data Clear
			m_PartData[batch].results[CAM_PCHI] = VI_CLEAR;
			m_PartData[batch].results[CAM_PCCA] = VI_CLEAR;
			m_PartData[batch].results[CAM_PCCA2] = VI_CLEAR;
			m_PartData[batch].results[CAM_PCHI2] = VI_CLEAR;

			m_PartDataVecDefects[batch][CAM_PCHI].clear();
			m_PartDataVecDefects[batch][CAM_PCCA].clear();
			m_PartDataVecDefects[batch][CAM_PCCA2].clear();
			m_PartDataVecDefects[batch][CAM_PCHI2].clear();

			//DataBuffer
			for (int i = 0; i < TOTAL_BUFFER_SIZE; i++){
				m_PartDataBuffer[i].part = CLEAR;

				m_PartDataBufferVecDefects[i][CAM_PCHI].clear();
				m_PartDataBufferVecDefects[i][CAM_PCCA].clear();
				m_PartDataBufferVecDefects[i][CAM_PCCA2].clear();
				m_PartDataBufferVecDefects[i][CAM_PCHI2].clear();
			}

			m_vi.clearVIResults();

	}//for (int batch = INPUT; batch <= OUT_BUFFER; batch++)

	return 0;
}

#ifdef DEBUG_PRINT_TIME
void CInspectThread::DebugPrint(const wchar_t* szDebugString, ...)
{
	wchar_t szBufferString[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szDebugString);
	vswprintf_s(szBufferString, szDebugString, (va_list)vargs);
	va_end(vargs);

	wchar_t szBuffer[2048] = L"[VI] ";
	wcscat(szBuffer, szBufferString);

	NIPL *pNIPL = NIPL::GetInstance();
	pNIPL->DebugPrint(szBuffer);
}
#endif