﻿#include "StdAfx.h"
#include "VisionInspection.h"
#include "nBizFerriteCoreInspection.h"
#include "afxdialogex.h"


IMPLEMENT_DYNAMIC(CVisionInspection, CDialogEx)

extern CMainWnd	*G_MainWnd;

struct ThreadCamData {
	CVisionInspection *m_pThis;
	int m_nThreadIndex;
	int m_nCamIndex;
};

UINT m_fnThreadCam(LPVOID lParam);

UINT m_fnLiveThread(LPVOID lParam);

CVisionInspection::CVisionInspection(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVisionInspection::IDD, pParent)
{

}

CVisionInspection::~CVisionInspection(void)
{

}

void CVisionInspection::DoDataExchange(CDataExchange* pDX)
{
	int id;
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC, m_pic);

	//for (id = 0; id < CAM_NO; id++)
	for (id = 0; id < 4; id++)
	{
		//DDX_Control(pDX, IDC_LIGHT + id, m_btnLight[id]);
		DDX_Control(pDX, IDC_BTN_GRAB + id, m_btnGrab[id]);
		DDX_Control(pDX, IDC_BTN_INSPECTION + id, m_btnInspection[id]);
		DDX_Control(pDX, IDC_BTN_GRAB_SAVE + id, m_btnGrabSave[id]);
	}
	for (id = CAM_NO; id < 4; id++)
	{
		//m_btnLight[id].EnableWindow(FALSE);
		m_btnGrab[id].EnableWindow(FALSE);
		m_btnInspection[id].EnableWindow(FALSE);
		m_btnGrabSave[id].EnableWindow(FALSE);
	}
	//m_btnGrab[CAM_PCCA2].EnableWindow(TRUE);
	//m_btnInspection[CAM_PCCA2].EnableWindow(TRUE);

	//20160317_KYS_ADD_LIGHT_BUTTON
	for (id = 0; id < 8; id++)
	{
		DDX_Control(pDX, IDC_LIGHT0 + id, m_btnLight[id]);
	}
		
	for (id = LIGHT_CAM1_1; id < sizeof(g_IsLIGHT_NOT_USE) / sizeof(g_IsLIGHT_NOT_USE[0]); id++)
	{
		m_btnLight[g_IsLIGHT_NOT_USE[id]].EnableWindow(FALSE);
	}
	//m_btnGrab[CAM_PCCA2].EnableWindow(TRUE);
	//m_btnInspection[CAM_PCCA2].EnableWindow(TRUE);
	//for (id = LIGHT_CAM1_1; id < LIGHT_ALL_COUNT; id++)
	//{
		//m_btnLight[id].EnableWindow(FALSE);
	//}


	DDX_Control(pDX, IDC_GRID_CAM_SETTING, m_GridCamSetting);
	DDX_Control(pDX, IDC_BTN_VI_GRID_ENABLE, m_btnVIGridEnable);
	DDX_Control(pDX, IDC_BTN_VI_GRID_DISABLE, m_btnVIGridDisable);
	DDX_Control(pDX, IDC_VI_APPLY, m_btnVIGridApply);

}

BEGIN_MESSAGE_MAP(CVisionInspection, CDialogEx)

	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(UM_VI, &CVisionInspection::OnUmVi)
	//ON_COMMAND_RANGE(IDC_LIGHT, IDC_LIGHT4, &CVisionInspection::OnBnClickedLight)
	ON_COMMAND_RANGE(IDC_LIGHT0, IDC_LIGHT7, &CVisionInspection::OnBnClickedLight)
	ON_COMMAND_RANGE(IDC_BTN_GRAB, IDC_BTN_GRAB4, &CVisionInspection::OnBnClickedBtnGrab)
	ON_COMMAND_RANGE(IDC_BTN_INSPECTION, IDC_BTN_INSPECTION4, &CVisionInspection::OnBnClickedBtnInspection)
	ON_COMMAND_RANGE(IDC_BTN_GRAB_SAVE, IDC_BTN_GRAB_SAVE4, &CVisionInspection::OnBnClickedBtnGrabSave)

	ON_BN_CLICKED(IDC_BTN_VI_GRID_ENABLE, &CVisionInspection::OnBnClickedBtnViGridEnable)
	ON_BN_CLICKED(IDC_BTN_VI_GRID_DISABLE, &CVisionInspection::OnBnClickedBtnViGridDisable)
	ON_BN_CLICKED(IDC_VI_APPLY, &CVisionInspection::OnBnClickedViApply)

END_MESSAGE_MAP()


BOOL CVisionInspection::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();

	OnBnClickedBtnViGridDisable();

	return TRUE;  // return TRUE unless you set the focus to a control
	// ¿¹¿Ü: OCX ¼Ó¼º ÆäÀÌÁö´Â FALSE¸¦ ¹ÝÈ¯ÇØ¾ß ÇÕ´Ï´Ù.
}


void CVisionInspection::m_fnInit(){

	int i;

	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);	

	statusCam[0] = CAM_CLOSE;
	statusCam[1] = CAM_CLOSE;
	statusCam[2] = CAM_CLOSE;
	statusCam[3] = CAM_CLOSE;

#ifdef _DEBUG
	//// retrieve the heartbeat node from the transport layer node map
	//GenApi::CIntegerPtr pHeartbeat = pCam->GetTLNodeMap()->GetNode("HeartbeatTimeout");
	//// set heartbeat to 600 seconds. (Note: Only GigE cameras have a "HeatbeatTimeout" node)
	//if (pHeartbeat != NULL ) pHearbeat->SetValue(600*1000);
#endif


//#ifndef _CAM_SKIP
//	if (pylonInitGrab(CAM_PCHI) == 1){
//		if(pylonSetParam(CAM_PCHI) == 0)
//			G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCHI].SetColorChange(GREEN, BLACK);
//
//		else{
//			G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCHI].SetColorChange(RGB(230, 230, 230), BLACK);
//		}
//
//	}
//	else{
//		G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCHI].SetColorChange(RGB(230, 230, 230), BLACK);
//	}
//	if (pylonInitGrab(CAM_PCCA) == 1){
//		if(pylonSetParam(CAM_PCCA) == 0)
//			G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCCA].SetColorChange(GREEN, BLACK);
//		else{
//			G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCCA].SetColorChange(RGB(230, 230, 230), BLACK);
//		}
//	}
//	else{
//		G_MainWnd->m_ServerDlg.m_btnCamStatus[CAM_PCCA].SetColorChange(RGB(230, 230, 230), BLACK);
//	}
//#endif

	for (i = 0; i < 4; i++)
	{
		pRawBuf[i] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);
		pImgBuf[i] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
		pOutBuf[i] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	}

	initGridCamSetting();
	fillGridCamSetting(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	renewCamSettingParam();

	//Camera Thread

//#ifndef _CAM_SKIP
//	pylonSetParam2();
//#endif


	m_LiveHandls[0].CamIndex = 0;
	m_LiveHandls[1].CamIndex = 1;
	m_LiveHandls[2].CamIndex = 2;
	m_LiveHandls[3].CamIndex = 3;

	//m_iplBuffGetImage = cvCreateImage(cvSize(LIVE_SIZE_X, LIVE_SIZE_Y), IPL_DEPTH_8U, 3);

	//for (int nStep = 0; nStep < MAX_CAM_COUNT; nStep++)
	//{
	//	m_ResultImage[nStep].Create(LIVE_SIZE_X, LIVE_SIZE_Y, ((IPL_DEPTH_8U & 255) * 3));
	//}

	//int MapSize = ((LIVE_SIZE_W*LIVE_SIZE_H) * 2 * 2) + sizeof(int) * 3 + (sizeof(double) * 2);
	int MapSize = sizeof(SMP_LIVE);
	hLIMapping = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, MapSize, CAM_LIVE_IMG_MAP_NAME);
	hMapRead = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, CAM_LIVE_IMG_MAP_NAME);

	if (hMapRead != NULL)
		m_pImgData = (SMP_LIVE*)MapViewOfFile(hMapRead, FILE_MAP_ALL_ACCESS, 0, 0, 0);

	m_LiveHandls[0].pImgData = m_pImgData;
	m_LiveHandls[1].pImgData = m_pImgData;
	m_LiveHandls[2].pImgData = m_pImgData;
	m_LiveHandls[3].pImgData = m_pImgData;

	m_LiveHandls[0].m_ViewImage = pRawBuf[0];
	m_LiveHandls[1].m_ViewImage = pRawBuf[1];
	m_LiveHandls[2].m_ViewImage = pRawBuf[2];
	m_LiveHandls[3].m_ViewImage = pRawBuf[3];


#ifdef VIRTUAL_RUN

	//read image files into image data buffer
	//IplImage *pRawBuf2[4];
	pRawBuf2[0] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	pRawBuf2[1] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	pRawBuf2[2] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	pRawBuf2[3] = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
	
	pRawBuf2[0] = cvLoadImage("D:\\nBizData\\Image\\CH270060\\0.jpg", CV_LOAD_IMAGE_COLOR);
	pRawBuf2[1] = cvLoadImage("D:\\nBizData\\Image\\CH270060\\1.jpg", CV_LOAD_IMAGE_COLOR);
	pRawBuf2[2] = cvLoadImage("D:\\nBizData\\Image\\CH270060\\2.jpg", CV_LOAD_IMAGE_COLOR);
	pRawBuf2[3] = cvLoadImage("D:\\nBizData\\Image\\CH270060\\3.jpg", CV_LOAD_IMAGE_COLOR);

#endif


	m_fnBeginCamThread();

	//VI Thread
	for (i = 0; i < MAX_VI_TEST; i++)
	{
		//2016.04.06 jec add
		m_LiveHandls[0].pImgData->LiveCheck[i] = 0;
		//queViData[i].clear();
		//queViResults[i].clear();
	}

	memset(flagThreadsVI, 0x00, 4*sizeof(int));
	m_fnBeginVIThread();
	m_IsCalibrationErr[0] = 0;
	m_IsCalibrationErr[1] = 0;
	m_IsCalibrationErr[2] = 0;
	m_IsCalibrationErr[3] = 0;

};

void CVisionInspection::m_fnDeInit(){
	
	m_fnEndCamThread();
	Sleep(100);
	m_fnEndVIThread();
	Sleep(100);
	for (int i = 0; i < 4; i++)
	{
		cvReleaseImage(&pRawBuf[i]);
		cvReleaseImage(&pImgBuf[i]);
		cvReleaseImage(&pOutBuf[i]);
	}

#ifdef 	VIRTUAL_RUN
	cvReleaseImage(&pRawBuf2[0]);
	cvReleaseImage(&pRawBuf2[1]);
	cvReleaseImage(&pRawBuf2[2]);
	cvReleaseImage(&pRawBuf2[3]);
#endif

};

void CVisionInspection::OnPaint()
{
	CPaintDC dc(this);
	drawImage();

}


void CVisionInspection::OnClose()
{
	// TODO: ¿©±â¿¡ ¸Þ½ÃÁö Ã³¸®±â ÄÚµå¸¦ Ãß°¡ ¹×/¶Ç´Â ±âº»°ªÀ» È£ÃâÇÕ´Ï´Ù.

	CDialogEx::OnClose();
}


HBRUSH CVisionInspection::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  ¿©±â¼­ DCÀÇ Æ¯¼ºÀ» º¯°æÇÕ´Ï´Ù.

	// TODO:  ±âº»°ªÀÌ Àû´çÇÏÁö ¾ÊÀ¸¸é ´Ù¸¥ ºê·¯½Ã¸¦ ¹ÝÈ¯ÇÕ´Ï´Ù.
	return hbr;
}


void CVisionInspection::OnBnClickedLight(UINT nID)
{
	int idx = nID - IDC_LIGHT0;
	if (swLight[idx] != ON){
		swLight[idx] = ON;
	}else{
		swLight[idx] = OFF;
	}
	G_MainWnd->m_InspectThread.turnLight(idx, swLight[idx]);
}

int CVisionInspection::pylonSetParam(int idx)
{
	int exitCode = 0;
	//try
	//{

		//// Set the image format and AOI
		//if (idx == CAM_PCHI){
		////if (1){
		//	pCam[idx]->PixelFormat.SetValue(PixelFormat_BayerBG8); //PixelFormat_BayerGR8
		//	pCam[idx]->OffsetX.SetValue(0);
		//	pCam[idx]->OffsetY.SetValue(0);
		//	pCam[idx]->Width.SetValue(LIVE_SIZE_W);
		//	pCam[idx]->Height.SetValue(LIVE_SIZE_H);

		//	//Set acquisition mode
		//	//pCam[idx]->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		//	///Light Source type setting
		//	pCam[idx]->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);
		//	////////////////
		//	pCam[idx]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
		//	pCam[idx]->BalanceRatioAbs.SetValue(2.984375);
		//	pCam[idx]->BalanceRatioRaw.SetValue(191);

		//	pCam[idx]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
		//	pCam[idx]->BalanceRatioAbs.SetValue(1.5);
		//	pCam[idx]->BalanceRatioRaw.SetValue(96);

		//	pCam[idx]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
		//	pCam[idx]->BalanceRatioAbs.SetValue(1.84375);
		//	pCam[idx]->BalanceRatioRaw.SetValue(118);

		//	////Set exposure settings
		//	pCam[idx]->ExposureMode.SetValue(ExposureMode_Timed);
		//	//pCam[idx]->ExposureTimeRaw.SetValue(7000);
		//	pCam[idx]->ExposureTimeRaw.SetValue(12000);

		//	//// Gain Setting
		//	pCam[idx]->GainRaw.SetValue(300);
		//	pCam[idx]->BlackLevelSelector.SetValue(BlackLevelSelector_All);
		//	pCam[idx]->BlackLevelRaw.SetValue(50);
		//	pCam[idx]->GammaSelector.SetValue(GammaSelector_User);
		//	pCam[idx]->Gamma.SetValue(1.0);
		//	pCam[idx]->GammaEnable.SetValue(true);

		//	////WhiteBalance Off
		//	pCam[idx]->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

		//	// Create an image buffer
		//	ImageSize = (size_t)(pCam[idx]->PayloadSize.GetValue());

		//	// The parameter MaxNumBuffer can be used to control the count of buffers
		//	// allocated for grabbing. The default value of this parameter is 10.
		//	//pCam[idx]->MaxNumBuffer = 5;

		//}
		//else {



		//}


	//}
	//catch (GenICam::GenericException &e)
	//{
	//	// Error handling.
	//wchar_t* pszTmp = ConvertMultybyteToUnicode(e.GetDescription());
	//G_AddLog(3, L"pylonSetParam Exception %s", pszTmp);
	//delete[] pszTmp;
	//	exitCode = -1;
	//}

	return exitCode;
}

//inline int CVisionInspection::pylonGrab(int idx)
//{
//	// The exit code of the sample application.
//	int exitCode = 0;
//	CString strSavePath;
//	int camIdx, imageIdx;
//	int cntGrab = 0;
//	int wait = 0;
//
//	if (idx == CAM_PCHI || idx == CAM_PCCA || idx == CAM_PCCA2 || idx == CAM_PCHI2){
//		camIdx = idx;
//		imageIdx = idx;
//	}
//	//else{
//
//	//	camIdx = CAM_PCCA;
//	//	imageIdx = CAM_PCCA2;
//	//}
//
//REGRAB:
//	try
//	{
//		m_LiveHandls[camIdx].flagCapture[camIdx] = TRUE;
//		while (wait++ < 20){
//
//			//20160229_kys_grab_test
//			//G_AddLog(3, L"pylonGrab_Count[Cam %d: Grab Count: %d] Exception ", idx, wait);
//			if (m_LiveHandls[camIdx].flagCapture[camIdx] == FALSE)
//			{
//				if (imageIdx == CAM_PCHI)
//				{
//#ifndef VIRTUAL_RUN
//					cvCvtColor(pRawBuf[camIdx], pImgBuf[CAM_PCHI], CV_BayerBG2RGB);
//#else
//					cvCopy(pRawBuf2[CAM_PCHI], pImgBuf[CAM_PCHI]);
//#endif
//					cvCopy(pImgBuf[CAM_PCHI], G_MainWnd->m_ServerDlg.pImgBuf[CAM_PCHI]);
//					//cvCopy(pImgBuf[CAM_PCHI], pImgBuf[CAM_PCHI2]);
//					//cvCopy(pImgBuf[CAM_PCHI2], G_MainWnd->m_ServerDlg.pImgBuf[CAM_PCHI2]);
//					//20160229_kys_grab_test
//					G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d/20]  ", idx, wait);
//					return 0;
//				}
//				else if (imageIdx == CAM_PCCA)
//				{
//#ifndef VIRTUAL_RUN
//					cvCvtColor(pRawBuf[camIdx], pImgBuf[CAM_PCCA], CV_BayerBG2RGB);
//#else
//					cvCopy(pRawBuf2[CAM_PCCA], pImgBuf[CAM_PCCA]);
//#endif
//					cvCopy(pImgBuf[CAM_PCCA], G_MainWnd->m_ServerDlg.pImgBuf[CAM_PCCA]);
//					//20160229_kys_grab_test
//					G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d/20]  ", idx, wait);
//
//					return 0;
//				}
//				else if (imageIdx == CAM_PCCA2)
//				{
//#ifndef VIRTUAL_RUN
//					cvCvtColor(pRawBuf[camIdx], pImgBuf[CAM_PCCA2], CV_BayerBG2RGB);
//#else
//					cvCopy(pRawBuf2[CAM_PCCA2], pImgBuf[CAM_PCCA2]);
//#endif
//					cvCopy(pImgBuf[CAM_PCCA2], G_MainWnd->m_ServerDlg.pImgBuf[CAM_PCCA2]);
//					//20160229_kys_grab_test
//					G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d/20]  ", idx, wait);
//					return 0;
//				}
////20160309_kys_add
//				else if (imageIdx == CAM_PCHI2)
//				{
//#ifndef VIRTUAL_RUN
//					cvCvtColor(pRawBuf[camIdx], pImgBuf[CAM_PCHI2], CV_BayerBG2RGB);
//#else
//					cvCopy(pRawBuf2[CAM_PCHI2], pImgBuf[CAM_PCHI2]);
//#endif
//					cvCopy(pImgBuf[CAM_PCHI2], G_MainWnd->m_ServerDlg.pImgBuf[CAM_PCHI2]);
//					//20160229_kys_grab_test
//					G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d/20]  ", idx, wait);
//					return 0;
//				}
//			}
//			else
//			{
////				G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d]", idx, wait);
//			}
//			//20160229_kys_grab_test
//			Sleep(10);
//			//Sleep(50);
//		}
//		//20160229_kys_grab_test
//		G_AddLog(3, L"pylonGrab_Count[Cam %d: Grab Count: %d] Exception ", idx, wait);
//
//		//
//		exitCode = 1;
//	}
//	catch (Exception &e)
//	{
//		if (cntGrab++ < 2)
//			goto REGRAB;
//		// Error handling.
//		G_AddLog(3, L"pylonInitGrab[Cam %d: Grab Count: %d] Exception ", idx);
//		exitCode = 1;
//
//	}
//
//	return exitCode;
//}

int CVisionInspection::pylonGrab(int idx)
{
	// The exit code of the sample application.
	int exitCode = 0;
	CString strSavePath;
	int camIdx, imageIdx;
	int cntGrab = 0;
	int wait = 0;

	if (idx == CAM_PCHI || idx == CAM_PCCA || idx == CAM_PCCA2 || idx == CAM_PCHI2){
		camIdx = idx;
		imageIdx = idx;
	}
	//else{

	//	camIdx = CAM_PCCA;
	//	imageIdx = CAM_PCCA2;
	//}

REGRAB:
	try
	{
		m_LiveHandls[camIdx].flagCapture[camIdx] = TRUE;
		while (wait++ < 100){

			//20160229_kys_grab_test
			//G_AddLog(3, L"pylonGrab_Count[Cam %d: Grab Count: %d] Exception ", idx, wait);
			if (m_LiveHandls[camIdx].flagCapture[camIdx] == FALSE)
			{
				if (imageIdx >= CAM_PCHI	&& imageIdx <= CAM_PCHI2) {
#ifndef VIRTUAL_RUN
					cvCvtColor(pRawBuf[camIdx], pImgBuf[imageIdx], CV_BayerBG2RGB);
#else
					cvCopy(pRawBuf2[CAM_PCHI], pImgBuf[imageIdx]);
#endif
					csDisplay[imageIdx].Lock();
					cvCopy(pImgBuf[imageIdx], G_MainWnd->m_ServerDlg.pImgBuf[imageIdx]);
					csDisplay[imageIdx].Unlock();
					G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d]", idx, wait);

					return 0;
				}
			}
			else
			{
				//G_AddLog(3, L"PylonGrab_DelayCount[Cam %d: Grab Count: %d]", idx, wait);
				Sleep(50);
				continue;
			}
			//20160229_kys_grab_test
			Sleep(50);
			//Sleep(30);
		}
		//20160229_kys_grab_test
		G_AddLog(3, L"pylonGrab_Count[Cam %d: Grab Count: %d] Exception ", idx, wait);
		//
		exitCode = 1;
	}
	catch (Exception &e)
	{
		if (cntGrab++ < 2)
			goto REGRAB;
		// Error handling.
		G_AddLog(3, L"pylonInitGrab[Cam %d: Grab Count: %d] Exception ", idx);
		exitCode = 1;

	}

	return exitCode;
}

void CVisionInspection::OnBnClickedBtnGrab(UINT nID)
{
	int idx = nID - IDC_BTN_GRAB;
	if (!(idx == CAM_PCHI || idx == CAM_PCCA || idx == CAM_PCCA2 || idx == CAM_PCHI2))return;
#ifdef	_CAM_SKIP
	AfxMessageBox(L"CAN SKIP DEFINED. CANNOT GRAB IMAGE", MB_ICONHAND);
	return;
#endif
	if (!pylonGrab(idx)){
		//G_MainWnd->m_ServerDlg.paintImageReady[idx] = 1;
		::SendMessage(this->m_hWnd, WM_PAINT, 0, idx);
	}else{//Error

	}
}

void CVisionInspection::OnBnClickedBtnInspection(UINT nID)
{
	int wait = 0;
	int vr1Result = 0;
	CString str;
	int idx = nID - IDC_BTN_INSPECTION;
#ifdef	_CAM_SKIP
	AfxMessageBox(L"CAN SKIP DEFINED. CANNOT GRAB IMAGE", MB_ICONHAND);
	return;
#endif
#ifdef	_VI_SKIP
	AfxMessageBox(L"VISION INSPECTION SKIP DEFINED. CANNOT INSPECT IMAGE", MB_ICONHAND);
	return;
#endif

	if (idx == CAM_PCHI)	{

		//clear queue
			::PostMessage(this->m_hWnd, UM_VI, (WPARAM)UW_VI_1, NULL); //STG1 VISION INSPECTION REQUEST

			//result loop
			while (wait++ < 10){
				if (!queViResults[CAM_PCHI].empty())
				{
					//
					csResult[CAM_PCHI].Lock();
					VI_RESULT &vr = queViResults[CAM_PCHI][0];
					str.Format(L"VI_RESULT[CAM_PCHI] : %d, %d", vr1Result, vr.result);

					vr.vecDefects->clear();
					delete vr.vecDefects;
					queViResults[CAM_PCHI].erase(queViResults[CAM_PCHI].begin());

					csResult[CAM_PCHI].Unlock();
					
					//vr.vecDefects.clear();

					wait = 0;
					AfxMessageBox(str.GetBuffer());
					str.ReleaseBuffer();
					break;
				}
				else{
					Sleep(500);
				}

			}//while (wait ++ < 10){

			//while (wait++ < 10){
			//	if (!queViResults[CAM_PCHI2].empty())
			//	{
			//		//
			//		VI_RESULT vr = queViResults[CAM_PCHI2].front();
			//		str.Format(L"VI_RESULT[CAM_PCHI2] : %d, %d", vr1Result, vr.result);
			//		//vr.vecDefects.clear();
			//		try{
			//			queViResults[CAM_PCHI2].pop();
			//		}
			//		catch (Exception& e){
			//			wchar_t* pszTmp = ConvertMultybyteToUnicode(e.what());
			//			G_AddLog(3, L"queViResults[CAM_PCHI2].pop(); Exception: %s", pszTmp);
			//			delete[] pszTmp;
			//		}
			//		wait = 0;
			//		AfxMessageBox(str.GetBuffer());
			//		str.ReleaseBuffer();
			//		break;
			//	}
			//	else{
			//		Sleep(500);
			//	}
			//}//while (wait ++ < 10){
	}
	else 	if (idx == CAM_PCCA){
		::PostMessage(this->m_hWnd, UM_VI, (WPARAM)UW_VI_2, NULL); //STG3 VISION INSPECTION REQUEST

		//result loop
		while (wait++ < 10){
			if (!queViResults[CAM_PCCA].empty())
			{
				//
				csResult[CAM_PCCA].Lock();
				VI_RESULT &vr = queViResults[CAM_PCCA][0];
				str.Format(L"VI_RESULT[CAM_PCCA] : %d, %d", vr1Result, vr.result);

				vr.vecDefects->clear();
				delete vr.vecDefects;
				queViResults[CAM_PCCA].erase(queViResults[CAM_PCCA].begin());
				csResult[CAM_PCCA].Unlock();
				//vr.vecDefects.clear();
				
				wait = 0;
				AfxMessageBox(str.GetBuffer());
				str.ReleaseBuffer();
				break;
			}
			else{
				Sleep(500);
			}
		}//while (wait ++ < 10){
	}
	else 	if (idx == CAM_PCCA2){
		::PostMessage(this->m_hWnd, UM_VI, (WPARAM)UW_VI_3, NULL); //STG3 VISION INSPECTION REQUEST
		//result loop
		while (wait++ < 10){
			if (!queViResults[CAM_PCCA2].empty())
			{
				csResult[CAM_PCCA2].Lock();
				VI_RESULT &vr = queViResults[CAM_PCCA2][0];
				str.Format(L"VI_RESULT[CAM_PCCA2] : %d, %d", vr1Result, vr.result);

				vr.vecDefects->clear();
				delete vr.vecDefects;
				queViResults[CAM_PCCA2].erase(queViResults[CAM_PCCA2].begin());
				csResult[CAM_PCCA2].Unlock();

				//vr.vecDefects.clear();

				wait = 0;
				AfxMessageBox(str.GetBuffer());
				str.ReleaseBuffer();
				break;
			}
			else{
				Sleep(500);
			}
		}//while (wait ++ < 10){
	}
//20160309_KYS	
	else 	if (idx == CAM_PCHI2){
		::PostMessage(this->m_hWnd, UM_VI, (WPARAM)UW_VI_4, NULL); //STG3 VISION INSPECTION REQUEST

		//result loop
		while (wait++ < 10){
			if (!queViResults[CAM_PCHI2].empty())
			{
				//
				csResult[CAM_PCHI2].Lock();
				VI_RESULT &vr = queViResults[CAM_PCHI2][0];
				str.Format(L"VI_RESULT[CAM_PCHI2] : %d, %d", vr1Result, vr.result);

				vr.vecDefects->clear();
				delete vr.vecDefects;
				queViResults[CAM_PCHI2].erase(queViResults[CAM_PCHI2].begin());
				csResult[CAM_PCHI2].Unlock();
				
				//vr.vecDefects.clear();

				wait = 0;
				AfxMessageBox(str.GetBuffer());
				str.ReleaseBuffer();
				break;
			}
			else{
				Sleep(500);
			}
		}//while (wait ++ < 10){
	}
//
}



void CVisionInspection::drawImage()
{
	CDC* pDC;
	CRect rect;
	pDC = m_pic.GetDC();
	m_pic.GetClientRect(&rect);

	CRect rect14, rect24, rect34, rect44;

	if (pImgBuf[CAM_PCHI] != NULL	/*&& G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCHI]*/)
	{
		rect14.top = rect.top;
		rect14.bottom = rect.top + static_cast<int>(rect.Height() / 2);
		rect14.left = rect.left;
		rect14.right = rect.left + static_cast<int>(rect.Width() / 2);
		DisplayIplImg(pImgBuf[CAM_PCHI], pDC, rect14,1);
	}
	if (pImgBuf[CAM_PCCA] != NULL	/*&& G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCCA]*/)
	{
		rect24.top = rect.top;
		rect24.bottom = rect.top + static_cast<int>(rect.Height() / 2);
		rect24.left = rect.left + static_cast<int>(rect.Width() / 2);
		rect24.right = rect.right;
		DisplayIplImg(pImgBuf[CAM_PCCA], pDC, rect24,2);
	}
	if (pImgBuf[CAM_PCCA2] != NULL	/*&& G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCCA2]*/)
	{
		rect34.top = rect.top + static_cast<int>(rect.Height() / 2);
		rect34.bottom = rect.bottom;
		rect34.left = rect.left;
		rect34.right = rect.left + static_cast<int>(rect.Width() / 2);
		DisplayIplImg(pImgBuf[CAM_PCCA2], pDC, rect34,3);
	}
	if (pImgBuf[CAM_PCHI2] != NULL	/*&& G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCHI2]*/)
	{
		rect44.top = rect.top + static_cast<int>(rect.Height() / 2);
		rect44.bottom = rect.bottom;
		rect44.left = rect.left + static_cast<int>(rect.Width() / 2);
		rect44.right = rect.right;
		DisplayIplImg(pImgBuf[CAM_PCHI2], pDC, rect44,4);
	}
	ReleaseDC(pDC);

}


void CVisionInspection::drawRect( )
{
	CDC* pDC;
	CRect rect;

	pDC = m_pic.GetDC();
	m_pic.GetClientRect(&rect);

	CPen pen;
	pen.CreatePen( PS_SOLID, 1, RGB(255,0,0) );
	CPen* oldPen = pDC->SelectObject( &pen );

	int x1, y1, x2, y2;

	x1 = (int)(points[0]*rect.Width()/LIVE_SIZE_W);
	y1 = (int)(points[1]*rect.Height()/LIVE_SIZE_H);
	x2 = (int)(points[2]*rect.Width()/LIVE_SIZE_W);
	y2 = (int)(points[3]*rect.Height()/LIVE_SIZE_H);
	//pDC->Rectangle(x1, y1, x2, y2);
	pDC->MoveTo(x1, y1);
	pDC->LineTo(x2, y1); pDC->LineTo(x2, y2);pDC->LineTo(x1, y2);pDC->LineTo(x1, y1);

	pDC->SelectObject( oldPen );

	ReleaseDC(pDC);

	return;
}

void CVisionInspection::drawRect(int coords[4])
{
	CDC* pDC;
	CRect rect;

	pDC = m_pic.GetDC();
	m_pic.GetClientRect(&rect);

	CPen pen;
	pen.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(&pen);

	int x1, y1, x2, y2;

	x1 = (int)(coords[0] * rect.Width() / LIVE_SIZE_W);
	y1 = (int)(coords[1] * rect.Height() / LIVE_SIZE_H);
	x2 = (int)(coords[2] * rect.Width() / LIVE_SIZE_W);
	y2 = (int)(coords[3] * rect.Height() / LIVE_SIZE_H);
	//pDC->Rectangle(x1, y1, x2, y2);
	pDC->MoveTo(x1, y1);
	pDC->LineTo(x2, y1); pDC->LineTo(x2, y2); pDC->LineTo(x1, y2); pDC->LineTo(x1, y1);

	pDC->SelectObject(oldPen);

	ReleaseDC(pDC);

	return;
}

afx_msg LRESULT CVisionInspection::OnUmVi(WPARAM wParam, LPARAM lParam)
{

	if (wParam == UW_VI_1)
	{
		SetUMMsgResponse(UW_VI_1, UM_VI_ONGOING);

#ifdef _CAM_SKIP
		if (1){
			SetUMMsgResponse(UW_VI_1, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
#else
		if (!pylonGrab(CAM_PCHI)){

//			m_bViProcess[0] = !m_bViProcess[0];

			SetUMMsgResponse(UW_VI_1, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT 
			//COPY IMAGE DATA
			//G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCHI2] = 1;
//			m_fnSaveImage(CAM_PCHI, VI_OK, (long)lParam);
			//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0); 

#endif
			retryVIreq[CAM_PCHI] = 0;
//			m_bViProcess[0] = !m_bViProcess[0];

			//vector
			VI_DATA vd0;
			vd0.id = (long)lParam;
			vd0.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			cvCopy(pImgBuf[CAM_PCHI], vd0.pImgBuf);

			G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCHI] = 1;
			m_fnSaveImage2(CAM_PCHI, VI_OK, (long)lParam, vd0.pImgBuf, vd0.szImageFileName);
			::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

			csData[CAM_PCHI].Lock();
			queViData[CAM_PCHI].push(vd0);
			csData[CAM_PCHI].Unlock();

//VI_PCHI:
//			if (suspendCntVI[CAM_PCHI] > 0)
//			{
//				do{
//					suspendCntVI[CAM_PCHI] = m_pThreadsVI[CAM_PCHI]->ResumeThread();
//				} while (suspendCntVI[CAM_PCHI] > 0);
//			}
//			else{
//				Sleep(100);
//				if (retryVIreq[CAM_PCHI]++ > 10)
//					goto VI_PCHI_ERROR;
//				else
//					goto VI_PCHI;
//			}

			//vector
			//VI_DATA vd3;
			//vd3.id = (long)lParam;
			//vd3.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			//cvCopy(pImgBuf[CAM_PCHI2], vd3.pImgBuf);
			//queViData[CAM_PCHI2].push(vd3);

//VI_PCHI2:
//			if (suspendCntVI[CAM_PCHI2] > 0)
//			{
//				do{
//					suspendCntVI[CAM_PCHI2] = m_pThreadsVI[CAM_PCHI2]->ResumeThread();
//				} while (suspendCntVI[CAM_PCHI2] > 0);
//			}
//			else{
//				Sleep(100);
//				if (retryVIreq[CAM_PCHI2]++ > 10)
//					goto VI_PCHI_ERROR;
//				else
//					goto VI_PCHI2;
//			}

		}
		else{//Error
//VI_PCHI_ERROR:
			SetUMMsgResponse(UW_VI_1, UM_VI_GRAB_ERROR);
		}

	}
	else if (wParam == UW_VI_2)
	{
		SetUMMsgResponse(UW_VI_2, UM_VI_ONGOING);
#ifdef _CAM_SKIP
		if (1){
			SetUMMsgResponse(UW_VI_2, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
#else
		if (!pylonGrab(CAM_PCCA)){
			
//			m_bViProcess[1] = !m_bViProcess[1];

			SetUMMsgResponse(UW_VI_2, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
			//COPY IMAGE DATA
#endif
			retryVIreq[CAM_PCCA] = 0;
			//vector
			VI_DATA vd1;
			vd1.id = (long)lParam;
			vd1.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			cvCopy(pImgBuf[CAM_PCCA], vd1.pImgBuf);

			G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCCA] = 1;
			m_fnSaveImage2(CAM_PCCA, VI_OK, (long)lParam, vd1.pImgBuf, vd1.szImageFileName);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

			csData[CAM_PCCA].Lock();
			queViData[CAM_PCCA].push(vd1);
			csData[CAM_PCCA].Unlock();

//VI_PCCA:
//			if (suspendCntVI[CAM_PCCA] > 0)
//			{
//				do{
//					suspendCntVI[CAM_PCCA] = m_pThreadsVI[CAM_PCCA]->ResumeThread();
//				} while (suspendCntVI[CAM_PCCA] > 0);
//			}
//			else{
//				Sleep(100);
//				if (retryVIreq[CAM_PCCA]++ > 10)
//					goto VI_PCCA_ERROR;
//				else
//					goto VI_PCCA;
//			}

		}
		else{//Error
//VI_PCCA_ERROR:
			SetUMMsgResponse(UW_VI_2, UM_VI_GRAB_ERROR);
		}

	}
	else if (wParam == UW_VI_3)
	{
		SetUMMsgResponse(UW_VI_3, UM_VI_ONGOING);
#ifdef _CAM_SKIP
		if (1){
			SetUMMsgResponse(UW_VI_3, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
#else
		if (!pylonGrab(CAM_PCCA2)){

//			m_bViProcess[2] = !m_bViProcess[2];

			SetUMMsgResponse(UW_VI_3, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
			//COPY IMAGE DATA
#endif
			retryVIreq[CAM_PCCA2] = 0;

			//vector
			VI_DATA vd2;
			vd2.id = (long)lParam;
			vd2.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			cvCopy(pImgBuf[CAM_PCCA2], vd2.pImgBuf);

			G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCCA2] = 1;
			m_fnSaveImage2(CAM_PCCA2, VI_OK, (long)lParam, vd2.pImgBuf, vd2.szImageFileName);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

			csData[CAM_PCCA2].Lock();
			queViData[CAM_PCCA2].push(vd2);
			csData[CAM_PCCA2].Unlock();

//VI_PCCA2:
//			if (suspendCntVI[CAM_PCCA2] > 0)
//			{
//				do{
//					suspendCntVI[CAM_PCCA2] = m_pThreadsVI[CAM_PCCA2]->ResumeThread();
//				} while (suspendCntVI[CAM_PCCA2] > 0);
//			}
//			else{
//				Sleep(100);
//				if (retryVIreq[CAM_PCCA2]++ > 10)
//					goto VI_PCCA2_ERROR;
//				else
//					goto VI_PCCA2;
//			}

		}
		else{//Error
//VI_PCCA2_ERROR:
			SetUMMsgResponse(UW_VI_3, UM_VI_GRAB_ERROR);
		}
	}
//20160307_KYS_ADD	
	else if (wParam == UW_VI_4)
	{
		SetUMMsgResponse(UW_VI_4, UM_VI_ONGOING);
#ifdef _CAM_SKIP
		if (1){
			SetUMMsgResponse(UW_VI_4, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT
#else
		if (!pylonGrab(CAM_PCHI2)){

//			m_bViProcess[3] = !m_bViProcess[3];

			SetUMMsgResponse(UW_VI_4, UM_VI_GRABBED); //INITIATE SHUTTLE MOVE NEXT 
			//COPY IMAGE DATA
#endif
			retryVIreq[CAM_PCHI2] = 0;

			//vector
			VI_DATA vd3;
			vd3.id = (long)lParam;
			vd3.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			cvCopy(pImgBuf[CAM_PCHI2], vd3.pImgBuf);

			G_MainWnd->m_ServerDlg.paintImageReady[CAM_PCHI2] = 1;
			m_fnSaveImage2(CAM_PCHI2, VI_OK, (long)lParam, vd3.pImgBuf, vd3.szImageFileName);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

			csData[CAM_PCHI2].Lock();
			queViData[CAM_PCHI2].push(vd3);
			csData[CAM_PCHI2].Unlock();

			//VI_PCHI:
			//			if (suspendCntVI[CAM_PCHI] > 0)
			//			{
			//				do{
			//					suspendCntVI[CAM_PCHI] = m_pThreadsVI[CAM_PCHI]->ResumeThread();
			//				} while (suspendCntVI[CAM_PCHI] > 0);
			//			}
			//			else{
			//				Sleep(100);
			//				if (retryVIreq[CAM_PCHI]++ > 10)
			//					goto VI_PCHI_ERROR;
			//				else
			//					goto VI_PCHI;
			//			}

			//vector
			//VI_DATA vd3;
			//vd3.id = (long)lParam;
			//vd3.pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 3);
			//cvCopy(pImgBuf[CAM_PCHI2], vd3.pImgBuf);
			//queViData[CAM_PCHI2].push(vd3);

			//VI_PCHI2:
			//			if (suspendCntVI[CAM_PCHI2] > 0)
			//			{
			//				do{
			//					suspendCntVI[CAM_PCHI2] = m_pThreadsVI[CAM_PCHI2]->ResumeThread();
			//				} while (suspendCntVI[CAM_PCHI2] > 0);
			//			}
			//			else{
			//				Sleep(100);
			//				if (retryVIreq[CAM_PCHI2]++ > 10)
			//					goto VI_PCHI_ERROR;
			//				else
			//					goto VI_PCHI2;
			//			}

		}
		else{//Error
//		VI_PCHI2_ERROR:
			SetUMMsgResponse(UW_VI_4, UM_VI_GRAB_ERROR);
		}
	}
//
	return 0;

}

void CVisionInspection::SetUMMsgResponse(int msg, int rsp)
{
	msgRsp.RSP[msg] = rsp;
}


int CVisionInspection::GetUMMsgResponse(int msg)
{
	int rsp;
	rsp = msgRsp.RSP[msg];
	return rsp;
}

int CVisionInspection::m_fnSaveImage(int cam, int NG, long id)
{
	CString strSavePath;
	CTime ctCurrentTime;

	//Save Image
	if (NG == VI_OK){
		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] == 2){ // 2: Save
			ctCurrentTime = CTime::GetCurrentTime();

			strSavePath.Format(L"%s\\%s\\%d_%s_%d.jpg",
				G_MainWnd->m_DataHandling.m_chImageSavePath,
				G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
				id,
				ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
				cam /*Cam No.*/
				);

			//cvSaveImage(ConvertUnicodeToMultybyte2(strSavePath.GetBuffer()), pImgBuf[cam]);
			//strSavePath.ReleaseBuffer();
			char *pMultibyte = ConvertUnicodeToMultybyte2(strSavePath.GetBuffer());
			cvSaveImage(pMultibyte, pImgBuf[cam]);
			strSavePath.ReleaseBuffer();
			delete[] pMultibyte;
		}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){
	}
	else{

		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] >= 1){ // 0: No Save
			ctCurrentTime = CTime::GetCurrentTime();

			strSavePath.Format(L"%s\\%s\\%d_%s_%d.jpg",
			G_MainWnd->m_DataHandling.m_chImageSaveNGPath,
				G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
				id,
				ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
				cam /*Cam No.*/
				);

			//cvSaveImage(ConvertUnicodeToMultybyte2(strSavePath.GetBuffer()), pImgBuf[cam]);
			//strSavePath.ReleaseBuffer();
			char *pMultibyte2 = ConvertUnicodeToMultybyte2(strSavePath.GetBuffer());
			cvSaveImage(pMultibyte2, pImgBuf[cam]);
			strSavePath.ReleaseBuffer();
			delete[] pMultibyte2;
		}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){
	}

	return 0;
}

int CVisionInspection::m_fnSaveImage2(int cam, int NG, long id, IplImage *pImgBuf, wchar_t *szImageFileName)
{
	CString strSavePath;
	CTime ctCurrentTime;

//	csData[cam].Lock();
	//Save Image
	if (NG == VI_OK){
		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] == 2){ // 2: Save
			ctCurrentTime = CTime::GetCurrentTime();

			strSavePath.Format(L"%s\\%s\\%d_%s_%d.jpg",
				G_MainWnd->m_DataHandling.m_chImageSavePath,
				G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
				id,
				ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
				cam /*Cam No.*/
				);

			if (szImageFileName != 0x00) {
				wcscpy(szImageFileName, LPCTSTR(strSavePath));
			}

			//cvSaveImage(ConvertUnicodeToMultybyte2(strSavePath.GetBuffer()), pImgBuf);
			//strSavePath.ReleaseBuffer();

			char *pMultibyte = ConvertUnicodeToMultybyte2(strSavePath.GetBuffer());
			cvSaveImage(pMultibyte, pImgBuf);

			strSavePath.ReleaseBuffer();
			delete[] pMultibyte;

		}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){
	}
	else{
		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] >= 1){ // 0: No Save
			ctCurrentTime = CTime::GetCurrentTime();

			strSavePath.Format(L"%s\\%s\\%d_%s_%d.jpg",
				G_MainWnd->m_DataHandling.m_chImageSaveNGPath,
				G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
				id,
				ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
				cam /*Cam No.*/
				);

			if (szImageFileName != 0x00) {
				wcscpy(szImageFileName, PCTSTR(strSavePath));
			}

			//cvSaveImage(ConvertUnicodeToMultybyte2(strSavePath.GetBuffer()), pImgBuf);
			//strSavePath.ReleaseBuffer();

			char *pMultibyte2 = ConvertUnicodeToMultybyte2(strSavePath.GetBuffer());
			cvSaveImage(pMultibyte2, pImgBuf);

			strSavePath.ReleaseBuffer();
			delete[] pMultibyte2;

		}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){
	}

//	csData[cam].Unlock();
	return 0;
}

bool CVisionInspection::LoadNIPJob()
{
	wstring strJobFile = VISION_FOLDER;
	strJobFile += L"VI_MagCore.nip";

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		return false;
	}

	return true;
}


bool CVisionInspection::DoVisionInspection(wstring strComp, int nCamId, bool &bFoundDefect, bool &bFoundCore, bool &bLoadImage, VI_DATA vd, VI_RESULT& vr)
{
//	IplImage *pImg = vd.pImgBuf;
//	IplImage *pImgServerDlg = G_MainWnd->m_ServerDlg.pImgBuf[nCamId];

	//make viResults
	vector<VI_DEFECT> &listDefect = *vr.vecDefects; //

	wstring strModel = G_MainWnd->m_RecipeSetDlg.part_Test.part_model;
	wchar_t szText[256];

	//if (!LoadNIPJob()) {
	//	wsprintf(szText, L"비전 검사를 수행하지 못하였습니다. 모델 데이터 파일 로드 실패 : %s", strModel.c_str());
	//	G_AddLog(3, szText);
	//	return false;
	//}

//	Mat dImg = cvarrToMat(pImg);

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	bLoadImage = pNIPO->LoadImage(vd.szImageFileName, dImg);
	if(!bLoadImage) {
		int nSurface = 0;
		if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) nSurface = NG_SURF_TOP;
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) nSurface = NG_SURF_BTM;
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) nSurface = NG_SURF_IN;
		else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) nSurface = NG_SURF_OUT;

		VI_DEFECT def;
		def.type = NG_TYP_NOT_LOAD_IMAGE;
		def.surface = nSurface;
		listDefect.push_back(def);

		// Calibration 실패시 NG 처리함. 장비가 멈추지 않도록 함 ==>  return true;
		m_IsCalibrationErr[nCamId] = NG_CAL_ERR_NO_IMAGE;

		G_AddLog(3, L"DoVisionInspection Error, Cannot Load Image File : %s", vd.szImageFileName);
		return true;
	}

	Mat dCalibrationImg;
	Mat dCharTemplateImg;
	Mat dOutImg;

	if (CHECK_EMPTY_IMAGE(dImg)) {
		G_AddLog(3, L"DoVisionInspection Exception: Invalid Image Buffer (%s)", strComp.c_str());
		return false;
	}

	wstring strVIData = VISION_FOLDER;
	strVIData += strModel + L".vid";

	wstring strTemplateImagePath = VISION_MODEL_SAMPLE_FOLDER;
	strTemplateImagePath += strModel + L"\\";
	strTemplateImagePath += L"CharTemplate.bmp";

	// Load Character Template Image
//	NIPO *pNIPO = NIPO::GetInstance();
	wstring strProcessName;
	NIPJobProcess *pProcess;

	wstring strImageFileName = L"MagCore_Orig_";
	strImageFileName += strComp;
	strImageFileName += L".jpg";
/*	try{
		pNIPO->SaveImage(strImageFileName.c_str(), dImg); // exception after merge 1229
	}
	catch (Exception &e){
		wchar_t* pszTmp = ConvertMultybyteToUnicode(e.what());
		G_AddLog(3, L"DoVisionInspection Line 937 Exception: %s", pszTmp);
		delete[] pszTmp;
	}
*/
	if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) {
		strProcessName = L"Load Template Image";
		pProcess = m_dJob.FindProcess(strProcessName);
		if (pProcess) {
			NIPLParam_LoadTemplateImage *pParam = (NIPLParam_LoadTemplateImage *)pNIPO->SetNIPLParam(pProcess);
			pParam->m_strTemplateImagePath = strTemplateImagePath;

			NIPLInput dInput;
			NIPLOutput dOutput;

			dInput.m_pParam = pParam;
			NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
			delete pParam;
			if (NIPL_SUCCESS(nErr)) {
				dCharTemplateImg = dOutput.m_dImg;
			}
		}
		else {
			wsprintf(szText, L"%s don't Inspect , Vision Template Image Load failed , nCamId: %d", strProcessName.c_str(), nCamId);
			G_AddLog(3, szText);
			return false;
		}
	}

	// Calibration
	NIPLCircle dMinCircle;
	NIPLCircle dMaxCircle;

	//G_AddLog(3, L"MagCore Calibration Start CAM[%d]", nCamId);

	bFoundCore = true;
	strProcessName = L"MagCore Calibration";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		m_IsCalibrationErr[nCamId] = 0;
		NIPLParam_MagCore_Calibration *pParam = (NIPLParam_MagCore_Calibration *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			wsprintf(szText, L"%s don't Inspect , Vision parameter set failed , nCamId: %d", strProcessName.c_str(), nCamId);
			G_AddLog(3, szText);
			return false;
		}

		pParam->m_strDataPath = strVIData;
		pParam->m_strComp = strComp;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		delete pParam;
		if (NIPL_SUCCESS(nErr)) {
			dCalibrationImg = dOutput.m_dImg;

			// convert out image to color image
			cvtColor(dCalibrationImg, dOutImg, CV_GRAY2BGR);

			if (dOutput.m_pResult != nullptr) {
				NIPLResult_FindCircle *pResult = (NIPLResult_FindCircle *)dOutput.m_pResult;

				dMinCircle = pResult->m_listCircle[0];
				dMaxCircle = pResult->m_listCircle[1];

				delete pResult;
			}
		}
		else {
			int nSurface = 0;
			if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) nSurface = NG_SURF_TOP;
			else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) nSurface = NG_SURF_BTM;
			else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) nSurface = NG_SURF_IN;
			else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) nSurface = NG_SURF_OUT;

			VI_DEFECT def;
			def.type = NG_TYP_CALIBRATION_FAIL;
			def.surface = nSurface;
			listDefect.push_back(def);

			wsprintf(szText, L"%s don't Inspect , Error code : %d, nCamId: %d", strProcessName.c_str(), nErr, nCamId);
			G_AddLog(3, szText);

			// Calibration 실패시 NG 처리함. 장비가 멈추지 않도록 함 ==>  return true;
			m_IsCalibrationErr[nCamId] = NG_CAL_ERR;

			if (nErr == NIPL_ERR_NO_OBJECT) {
				bFoundCore = false;

				wsprintf(szText, L"%s don't Inspect , No Core ( CheckCount [%d] ), nCamId: %d", strProcessName.c_str(), m_nNoCoreCount + 1, nCamId);
				G_AddLog(3, szText);
			}

			return true;
		}
	}
	else {
		wsprintf(szText, L"%s don't Inspect, nCamId: %d", strProcessName.c_str(), nCamId);
		G_AddLog(3, szText);
		return false;
	}


	//G_AddLog(3, L"MagCore Calibration END CAM[%d]", nCamId);
	//G_AddLog(3, L"MagCore Inspection Start CAM[%d]", nCamId);
	bFoundDefect = false;
	strProcessName = L"MagCore Inspection";
	pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam_MagCore *pParam = (NIPLParam_MagCore *)pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			wsprintf(szText, L"%s don't Inspect, Vision parameter set failed, nCamId: %d", strProcessName.c_str(), nCamId);
			G_AddLog(3, szText);
			return false;
		}

		pParam->m_strDataPath = strVIData;
		pParam->m_strComp = strComp;


		pParam->m_dCharTemplateImage = dCharTemplateImg;

		//// Set Circle Position
		NIPLParam_Circle2Rect &dParam_Circle2Rect = pParam->m_dParam_Circle2Rect;
		dParam_Circle2Rect.m_nCenterPosX = dMinCircle.m_ptCenter.x;
		dParam_Circle2Rect.m_nCenterPosY = dMinCircle.m_ptCenter.y;
		dParam_Circle2Rect.m_nMinRadius = dMinCircle.m_nRadius;
		dParam_Circle2Rect.m_nMaxRadius = dMaxCircle.m_nRadius;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dCalibrationImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		delete pParam;
		if (NIPL_SUCCESS(nErr)) {
			if (dOutput.m_pResult != nullptr) {
				bFoundDefect = true;

				int nSurface = 0;
				if (CHECK_STRING(strComp, STR_MAGCORE_COMP_UPPER)) nSurface = NG_SURF_TOP;
				else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_LOWER)) nSurface = NG_SURF_BTM;
				else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_INNER)) nSurface = NG_SURF_IN;
				else if (CHECK_STRING(strComp, STR_MAGCORE_COMP_OUTER)) nSurface = NG_SURF_OUT;

				NIPLResult_Defect_MagCore *pResult = (NIPLResult_Defect_MagCore *)dOutput.m_pResult;
				for (auto &dDefect : pResult->m_listDefect) {
					int nDefecType = 0;
					switch (dDefect.m_nType) {
					case NIPLDefect_MagCore::DEFECT_TYPE_SCRETCH: nDefecType = NG_TYP_SCRETCH; break;
					case NIPLDefect_MagCore::DEFECT_TYPE_BROKEN_CHARACTER: nDefecType = NG_TYP_BROKEN_CHARACTER; break;
					}

					VI_DEFECT def;
					def.type = nDefecType;
					def.surface = nSurface;
					def.coords[VI_LEFT] = dDefect.m_rcBoundingBox.x;
					def.coords[VI_TOP] = dDefect.m_rcBoundingBox.y;
					def.coords[VI_RIGHT] = dDefect.m_rcBoundingBox.x + dDefect.m_rcBoundingBox.width;
					def.coords[VI_BOTTOM] = dDefect.m_rcBoundingBox.y + dDefect.m_rcBoundingBox.height;
					def.size = dDefect.m_nSize;
					listDefect.push_back(def);

					rectangle(dOutImg, dDefect.m_rcBoundingBox, CV_RGB(255, 0, 0), 3);
				}

				delete pResult;

				// Save Defect Image
				/*
				strImageFileName = L"MagCore_Defect_";
				strImageFileName += strComp;
				strImageFileName += L".jpg";
				pNIPO->SaveImage(strImageFileName.c_str(), dOutImg);
				*/
			}
		}
		else {
			wsprintf(szText, L"%s don't Inspect,  Error code : %d, nCamId: %d", strProcessName.c_str(), nErr, nCamId);
			G_AddLog(3, szText);
			return false;
		}
	}
	else {
		wsprintf(szText, L"%s don't Inspect, nCamId: %d", strProcessName.c_str(), nCamId);
		G_AddLog(3, szText);
		return false;
	}

	//G_AddLog(3, L"MagCore Inspection END CAM[%d]", nCamId);

	IplImage dIplOutImg = IplImage(dOutImg);
	cvCopy(&dIplOutImg, vd.pImgBuf /*pImgBuf[nCamId]*/);
	//cvCopy(&dIplOutImg, pImgServerDlg);
	//G_MainWnd->m_ServerDlg.paintImageReady[nCamId] = 1;
	//::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

	return true;
}

//Camera Color Control: 1, 2
int CVisionInspection::initGridCamSetting()
{
	int nSize;
	// system param grid **********************
	try {

		nSize = sizeof(ST_RECIPE_ITEM_VI_DLG) / 8 + 1;

		m_GridCamSetting.SetRowCount(nSize);
		m_GridCamSetting.SetColumnCount(3);
		m_GridCamSetting.SetFixedRowCount(1);
		m_GridCamSetting.SetFixedColumnCount(2);
		m_GridCamSetting.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	for (int row = 0; row < m_GridCamSetting.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridCamSetting.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT | GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if (row == 0)
			{
				Item.nFormat = DT_VCENTER | GVIF_FORMAT;

				if (col == 0)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T(" NUM "));
				}
				else if (col == 1)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T("                   CAMERA SETTING                   "));
				}
				else if (col == 2)
				{
					Item.nFormat = DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_NOPREFIX;
					Item.strText.Format(_T("                 VALUE                 "));
				}
			}
			else
			{
				if (col == 0)
				{
					if (row != 0)
					{
						Item.nFormat = DT_VCENTER | GVIF_FORMAT | DT_CENTER;
						Item.strText.Format(L"%d", row);
					}
				}
				else if (col == 1)
				{
					Item.nFormat = DT_VCENTER | GVIF_FORMAT;
					//int nSize = sizeof(ST_RECIPE_ITEM_VI_DLG) / 8;
					if (row - 1 < nSize)
						Item.strText.Format(L"%s", ST_RECIPE_ITEM_VI_DLG[row - 1]);
				}
			}

			m_GridCamSetting.SetItem(&Item);
		}
	}

	m_GridCamSetting.AutoSize(GVS_BOTH);
	m_GridCamSetting.SetTextColor(RGB(0, 0, 105));
	m_GridCamSetting.SetEditable(FALSE);
	m_btnVIGridEnable.EnableWindow(TRUE);
	m_btnVIGridDisable.EnableWindow(FALSE);
	m_GridCamSetting.Refresh();

	return 0;
}

int CVisionInspection::fillGridCamSetting(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	BOOL bReturn = FALSE;
	CString strValueData;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if (strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength() - 4);
			if (strRecipeName == recipe)
			{
				G_MainWnd->m_RecipeSetDlg.m_iniRecipe.SetFileName(finder.GetFilePath());
				//load recipe data in GridModeRecipe
				strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_DATA, KEY_MODEL, L"");
				GetDlgItem(IDC_STATIC_VI_PNO)->SetWindowText(strValueData);
				//strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_DATA, KEY_TYPE, L"");
				//GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(strValueData);

				int nSize = sizeof(ST_RECIPE_ITEM_VI_DLG) / 8 + 1;

				for (int row = 1; row < nSize; row++)
				{
					strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, ST_RECIPE_ITEM_VI_DLG[row - 1], L""); //RECIPE
					if (strValueData == "")
						continue;
					m_GridCamSetting.SetItemText(row, 2, strValueData);
				}
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}
	m_GridCamSetting.Refresh();
	return 0;

	return 0;
}

int CVisionInspection::applyGridCamSetting()
{
	return 0;
}

void CVisionInspection::OnBnClickedBtnViGridEnable()
{
	m_GridCamSetting.SetEditable(TRUE);

	m_btnVIGridDisable.EnableWindow(TRUE);
	m_btnVIGridEnable.EnableWindow(FALSE);

	m_GridCamSetting.Refresh();
}


void CVisionInspection::OnBnClickedBtnViGridDisable()
{
	m_GridCamSetting.SetEditable(FALSE);

	m_btnVIGridDisable.EnableWindow(FALSE);
	m_btnVIGridEnable.EnableWindow(TRUE);

	m_GridCamSetting.Refresh();
}

// SAVE, RENEW PYRON SETTING, IMAGE GRAB, DISPLAY
void CVisionInspection::OnBnClickedViApply()
{
	// SAVE, RENEW m_RecipeParam
	CString modelSelected;
	if (G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"IT CAN BE DONE IN ENGINEER MODE", MB_ICONHAND);
		return;
	}
	else{
		if (!saveCamSettingRecipe(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel)){
			renewCamSettingParam();
			fillGridCamSetting(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		}else{
			//MessageBox here fail to save VI Cam setting

			return;
		}
	}

	//LIGHT INTENSITY 
	//CAM1_PCHI
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM1_PORT, LIGHT_CAM1_1_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM1_PORT, LIGHT_CAM1_2_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH2]);
	//CAM2_PCCA
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM2_PORT, LIGHT_CAM2_UPPER_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM2_PORT, LIGHT_CAM2_LOWER_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH2]);
	//CAM3_PCCA2
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM3_PORT, LIGHT_CAM3_1_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM3_PORT, LIGHT_CAM3_2_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH2]);
	//CAM4_PCHI2
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM4_PORT, LIGHT_CAM4_UPPER_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(LIGHT_CAM4_PORT, LIGHT_CAM4_LOWER_CH, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH2]);

	//LIGHT ON
	for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	{
		G_MainWnd->m_InspectThread.turnLight(i, ON);
	}


#ifndef _CAM_SKIP
	for (int i = 0; i < MAX_VI_TEST; i++)
	{
		//2016.04.06 jec add
		G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[i].pImgData->LiveCheck[i] = 0;
	}

	//RENEW PYRON SETTING, 
	if (pylonSetParam3() == -1){
		//Error
		AfxMessageBox(L"Camera Setting Timeout Failed. Basler_CamLive2 program should be running.", MB_ICONHAND);
		return;
	}

	for (int i = 0; i < MAX_VI_TEST; i++)
	{
		//2016.04.06 jec add
		G_MainWnd->m_InspectThread.m_vi.m_LiveHandls[i].pImgData->LiveCheck[i] = 4;
	}
	//IMAGE GRAB, DISPLAY

	if (!pylonGrab(CAM_PCHI)){
		::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	}
	else{//Error
		AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
		return;
	}
	if (!pylonGrab(CAM_PCCA)){
		::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	}
	else{//Error
		AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
		return;
	}
	if (!pylonGrab(CAM_PCCA2)){
		::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	}
	else{//Error
		AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
		return;
	}
	if (!pylonGrab(CAM_PCHI2)){
		::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	}
	else{//Error
		AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
		return;
	}
#endif

	//Part Clear
	G_MainWnd->m_InspectThread.m_fnClearParts();
	return;
}

void CVisionInspection::applyRecipeInProduction()
{
	// SAVE, RENEW m_RecipeParam
	CString modelSelected;

	renewCamSettingParam();
	fillGridCamSetting(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);


	//LIGHT INTENSITY 
	//CAM1_PCHI
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, CH_1, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, CH_2, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH2]);
	////CAM2_PCCA
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(1, CH_1, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(1, CH_2, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH2]);
	////CAM3_PCCA2
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, CH_1, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(0, CH_2, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH2]);
	////CAM4_PCHI2
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(2, CH_1, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH1]);
	//G_MainWnd->m_SerialInterface.m_fnSendToComportLight(2, CH_2, G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH2]);

	////LIGHT ON
	//for (int i = LIGHT_CAM1_1; i < LIGHT_ALL_COUNT; i++)
	//{
	//	G_MainWnd->m_InspectThread.turnLight(i, ON);
	//}
	////
#ifndef _CAM_SKIP
	//RENEW PYRON SETTING, 
	if (pylonSetParam3() == -1){
		//Error
		AfxMessageBox(L"Camera Setting Timeout Failed. Basler_CamLive2 program should be running.", MB_ICONHAND);
		return;
	}

	//IMAGE GRAB, DISPLAY

	//if (!pylonGrab(CAM_PCHI)){
	//	::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	//}
	//else{//Error
	//	AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
	//	return;
	//}
	//if (!pylonGrab(CAM_PCCA)){
	//	::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	//}
	//else{//Error
	//	AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
	//	return;
	//}
	//if (!pylonGrab(CAM_PCCA2)){
	//	::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	//}
	//else{//Error
	//	AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
	//	return;
	//}
	//if (!pylonGrab(CAM_PCHI2)){
	//	::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
	//}
	//else{//Error
	//	AfxMessageBox(L"Camera Image Grab Failed", MB_ICONHAND);
	//	return;
	//}
#endif

	//Part Clear
	G_MainWnd->m_InspectThread.m_fnClearParts();
	return;
}


int CVisionInspection::saveCamSettingRecipe(CString recipe)
{
	//find recipe
	CString strRecipe, strRecipeName, strRecipeFullPath;
	bool filefound = false;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if (strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength() - 4);

			if (strRecipeName == recipe)
			{
				strRecipeFullPath = (LPCTSTR)finder.GetFilePath();
				//m_iniRecipe.SetFileName(strRecipeFullPath);  // in Recipe Dlg
				filefound = true;
				break;
			}
		}//
	}

	//if not, just return. Recipe file can be made in Recipe Dlg
	if (!filefound)
	{
		//MessageBox here

		return -1;
	}

	//save recipe data from grid
	BOOL bReturn = FALSE;
	CString strValueData;

	GV_ITEM Item;
	Item.col = 2;

	for (int row = 1; row < m_GridCamSetting.GetRowCount(); row++)
	{
		strValueData = m_GridCamSetting.GetItemText(row, Item.col);
		if (strValueData == "")
			continue;
		bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(SECTION_RECIPE_CAM, ST_RECIPE_ITEM_VI_DLG[row - 1], strValueData);
	}

	return 0;
}

int CVisionInspection::renewCamSettingParam()
{
	BOOL bReturn = FALSE;
	CString strValueData;

	//CAM1
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_LIGHT_INTENSITY_CH1, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH1] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_LIGHT_INTENSITY_CH2, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE_CH2] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_RED, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_RED] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_GREEN, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_BLUE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_LIGHT_TIME, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_TIME] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAIN_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_GAIN_RAW] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BLACKLEVEL_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BLACKLEVEL_RAW] = _wtoi(strValueData);
	
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAMMA_ENABLE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_GAMMA_ENABLE] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAMMA, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM1_GAMMA] = _wtof(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_WHITE_BALANCE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_WHITE_BALANCE] = _wtoi(strValueData);


	//CAM2
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_LIGHT_INTENSITY_CH1, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH1] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_LIGHT_INTENSITY_CH2, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE_CH2] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_RED, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_RED] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_GREEN, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_BLUE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_LIGHT_TIME, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_TIME] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAIN_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_GAIN_RAW] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BLACKLEVEL_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BLACKLEVEL_RAW] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAMMA_ENABLE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_GAMMA_ENABLE] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAMMA, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM2_GAMMA] = _wtof(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_WHITE_BALANCE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_WHITE_BALANCE] = _wtoi(strValueData);

	//CAM3
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_LIGHT_INTENSITY_CH1, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH1] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_LIGHT_INTENSITY_CH2, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_SOURCE_CH2] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_BALANCERATIORAW_RED, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_BALANCERATIORAW_RED] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_BALANCERATIORAW_GREEN, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_BALANCERATIORAW_BLUE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_LIGHT_TIME, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_LIGHT_TIME] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_GAIN_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_GAIN_RAW] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_BLACKLEVEL_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_BLACKLEVEL_RAW] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_GAMMA_ENABLE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_GAMMA_ENABLE] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_GAMMA, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM3_GAMMA] = _wtof(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM3_WHITE_BALANCE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM3_WHITE_BALANCE] = _wtoi(strValueData);

	//CAM4
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_LIGHT_INTENSITY_CH1, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH1] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_LIGHT_INTENSITY_CH2, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_SOURCE_CH2] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_BALANCERATIORAW_RED, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_BALANCERATIORAW_RED] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_BALANCERATIORAW_GREEN, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_BALANCERATIORAW_BLUE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_LIGHT_TIME, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_LIGHT_TIME] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_GAIN_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_GAIN_RAW] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_BLACKLEVEL_RAW, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_BLACKLEVEL_RAW] = _wtoi(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_GAMMA_ENABLE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_GAMMA_ENABLE] = _wtoi(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_GAMMA, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM4_GAMMA] = _wtof(strValueData);
	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM4_WHITE_BALANCE, L"");
	G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM4_WHITE_BALANCE] = _wtoi(strValueData);

	return 0;
}

int CVisionInspection::pylonSetParam2()
{
	int exitCode = 0;
	//try
	//{
		//if (statusCam[CAM_PCHI] == CAM_OPEN){
		//	// Set the image format and AOI
		//	pCam[CAM_PCHI]->PixelFormat.SetValue(PixelFormat_BayerBG8); //PixelFormat_BayerGR8
		//	pCam[CAM_PCHI]->OffsetX.SetValue(0);
		//	pCam[CAM_PCHI]->OffsetY.SetValue(0);
		//	pCam[CAM_PCHI]->Width.SetValue(LIVE_SIZE_W);
		//	pCam[CAM_PCHI]->Height.SetValue(LIVE_SIZE_H);

		//	//Set acquisition mode
		//	//pCam[CAM_PCHI]->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		//	///Light Source type setting
		//	pCam[CAM_PCHI]->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);
		//	////////////////
		//	pCam[CAM_PCHI]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
		//	pCam[CAM_PCHI]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_RED]);

		//	pCam[CAM_PCHI]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
		//	pCam[CAM_PCHI]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_GREEN]);

		//	pCam[CAM_PCHI]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
		//	pCam[CAM_PCHI]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_BLUE]);

		//	////Set exposure settings
		//	pCam[CAM_PCHI]->ExposureMode.SetValue(ExposureMode_Timed);
		//	pCam[CAM_PCHI]->ExposureTimeRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_LIGHT_TIME]);

		//	//// Gain Setting
		//	pCam[CAM_PCHI]->GainRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_GAIN_RAW]);
		//	pCam[CAM_PCHI]->BlackLevelSelector.SetValue(BlackLevelSelector_All);
		//	pCam[CAM_PCHI]->BlackLevelRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_BLACKLEVEL_RAW]);
		//	pCam[CAM_PCHI]->GammaSelector.SetValue(GammaSelector_User);
		//	pCam[CAM_PCHI]->Gamma.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM1_GAMMA]);
		//	pCam[CAM_PCHI]->GammaEnable.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM1_GAMMA_ENABLE]);

		//	////WhiteBalance Off
		//	pCam[CAM_PCHI]->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

		//	// Create an image buffer
		//	ImageSize = (size_t)(pCam[CAM_PCHI]->PayloadSize.GetValue());

		//	// The parameter MaxNumBuffer can be used to control the count of buffers
		//	// allocated for grabbing. The default value of this parameter is 10.
		//	//pCam[CAM_PCHI]->MaxNumBuffer = 5;


		//}


		//	//Cam2
		//if (statusCam[CAM_PCCA] == CAM_OPEN){
		//	pCam[CAM_PCCA]->PixelFormat.SetValue(PixelFormat_BayerBG8); //PixelFormat_BayerGR8
		//	pCam[CAM_PCCA]->OffsetX.SetValue(0);
		//	pCam[CAM_PCCA]->OffsetY.SetValue(0);
		//	pCam[CAM_PCCA]->Width.SetValue(LIVE_SIZE_W);
		//	pCam[CAM_PCCA]->Height.SetValue(LIVE_SIZE_H);

		//	//Set acquisition mode
		//	//pCam[CAM_PCCA]->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		//	///Light Source type setting
		//	pCam[CAM_PCCA]->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);
		//	////////////////
		//	pCam[CAM_PCCA]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
		//	pCam[CAM_PCCA]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_RED]);

		//	pCam[CAM_PCCA]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
		//	pCam[CAM_PCCA]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_GREEN]);

		//	pCam[CAM_PCCA]->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
		//	pCam[CAM_PCCA]->BalanceRatioRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_BLUE]);

		//	////Set exposure settings
		//	pCam[CAM_PCCA]->ExposureMode.SetValue(ExposureMode_Timed);
		//	pCam[CAM_PCCA]->ExposureTimeRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_LIGHT_TIME]);

		//	//// Gain Setting
		//	pCam[CAM_PCCA]->GainRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_GAIN_RAW]);
		//	pCam[CAM_PCCA]->BlackLevelSelector.SetValue(BlackLevelSelector_All);
		//	pCam[CAM_PCCA]->BlackLevelRaw.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_BLACKLEVEL_RAW]);
		//	pCam[CAM_PCCA]->GammaSelector.SetValue(GammaSelector_User);
		//	pCam[CAM_PCCA]->Gamma.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.fParam[RP_CAM2_GAMMA]);
		//	pCam[CAM_PCCA]->GammaEnable.SetValue(G_MainWnd->m_DataHandling.m_RecipeParam.iParam[RP_CAM2_GAMMA_ENABLE]);

		//	////WhiteBalance Off
		//	pCam[CAM_PCCA]->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

		//	// Create an image buffer
		//	ImageSize = (size_t)(pCam[CAM_PCCA]->PayloadSize.GetValue());

		//	// The parameter MaxNumBuffer can be used to control the count of buffers
		//	// allocated for grabbing. The default value of this parameter is 10.
		//	//pCam[CAM_PCCA]->MaxNumBuffer = 5;
		//}
	//}
	//catch (GenICam::GenericException &e)
	//{
	//	// Error handling.
	// wchar_t* pszTmp = ConvertMultybyteToUnicode(e.GetDescription());
	//	G_AddLog(3, L"pylonSetParam Exception %s", pszTmp);
	// delete[] pszTmp;
	//	exitCode = -1;
	//}

	return exitCode;
}

int CVisionInspection::pylonSetParam3()
{
	int retry = 0; 
	wcscpy_s(m_LiveHandls[CAM_PCHI].pImgData->wcPNo, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	wcscpy_s(m_LiveHandls[CAM_PCCA].pImgData->wcPNo, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	wcscpy_s(m_LiveHandls[CAM_PCHI2].pImgData->wcPNo, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	wcscpy_s(m_LiveHandls[CAM_PCCA2].pImgData->wcPNo, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	m_LiveHandls[CAM_PCHI].pImgData->paramSet[CAM_PCHI] = TRUE;
	m_LiveHandls[CAM_PCCA].pImgData->paramSet[CAM_PCCA] = TRUE;
	m_LiveHandls[CAM_PCHI2].pImgData->paramSet[CAM_PCHI2] = TRUE;
	m_LiveHandls[CAM_PCCA2].pImgData->paramSet[CAM_PCCA2] = TRUE;
	Sleep(100);
	while (retry++ < 10)
	{
		if (m_LiveHandls[CAM_PCHI].pImgData->paramSet[CAM_PCHI] != FALSE && m_LiveHandls[CAM_PCCA].pImgData->paramSet[CAM_PCCA] != FALSE&&
			m_LiveHandls[CAM_PCHI2].pImgData->paramSet[CAM_PCHI2] != FALSE && m_LiveHandls[CAM_PCCA2].pImgData->paramSet[CAM_PCCA2] != FALSE
			)
			Sleep(100);
		else
			return 0;
	}

	if (retry >= 10)
		return -1;
	else
		return 0;

}

void CVisionInspection::OnBnClickedBtnGrabSave(UINT nID)
{
	int idx = nID - IDC_BTN_GRAB_SAVE;
	if (!(idx == CAM_PCHI || idx == CAM_PCCA || idx == CAM_PCCA2 || idx ==CAM_PCHI2))return;
#ifdef	_CAM_SKIP
	AfxMessageBox(L"CAN SKIP DEFINED. CANNOT GRAB IMAGE", MB_ICONHAND);
	return;
#endif
	if (!pylonGrab(idx)){
		::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
		//Save Image
		m_fnSaveImage(idx, VI_OK, 0);
	}
	else{//Error

	}
	
}

void CVisionInspection::clearVIResults()
{
	int i;
	for (i = CAM_PCHI; i <= CAM_PCHI2; i++)
	{
		queViResults[i].clear();
//		while (!queViResults[i].empty())
//			queViResults[i].pop();
	}
}




////////////////////////////////////////////////////////////////////////////////////////////
//VI threads

int CVisionInspection::m_fnBeginVIThread(void)
{
	for (int i = 0; i < CAM_THREAD_COUNT; i++)
	{
		isThreadRunningVI[i] = TRUE;
	}
	/*
	for (int i = 0; i <4; i++)
	{
		m_bViProcess[i] = TRUE;
	}
	*/

	for (int i = 0; i < CAM_THREAD_COUNT; i++) {
		ThreadCamData *pThreadCamData = new ThreadCamData();
		pThreadCamData->m_pThis = this;
		pThreadCamData->m_nThreadIndex = i;

		switch (i % 4) {
		case 0: pThreadCamData->m_nCamIndex = CAM_PCHI; break;
		case 1: pThreadCamData->m_nCamIndex = CAM_PCCA; break;
		case 2: pThreadCamData->m_nCamIndex = CAM_PCCA2; break;
		case 3: pThreadCamData->m_nCamIndex = CAM_PCHI2; break;
		}

		m_pThreadsVI[i] = AfxBeginThread(m_fnThreadCam, (LPVOID)pThreadCamData, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL); // THREAD_PRIORITY_NORMAL
	}


	/*
	m_pThreadsVI[0] = AfxBeginThread(m_fnThreadCam0, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL); // THREAD_PRIORITY_NORMAL
	m_pThreadsVI[1] = AfxBeginThread(m_fnThreadCam1, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL); // THREAD_PRIORITY_HIGHEST
	m_pThreadsVI[2] = AfxBeginThread(m_fnThreadCam2, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL);
	m_pThreadsVI[3] = AfxBeginThread(m_fnThreadCam3, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL);

	m_pThreadsVI[4] = AfxBeginThread(m_fnThreadCam01, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL); // THREAD_PRIORITY_NORMAL
	m_pThreadsVI[5] = AfxBeginThread(m_fnThreadCam11, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL); // THREAD_PRIORITY_HIGHEST
	m_pThreadsVI[6] = AfxBeginThread(m_fnThreadCam21, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL);
	m_pThreadsVI[7] = AfxBeginThread(m_fnThreadCam31, (LPVOID)this, THREAD_PRIORITY_HIGHEST, 0, NULL, NULL);
	*/

	//for (int i = 0; i < 4; i++)
	//{
	//	suspendCntVI[i] = 1;
	//}

	return 0;
}

int CVisionInspection::m_fnEndVIThread()
{
	int i;
	for (i = 0; i < CAM_THREAD_COUNT; i++)
	{
		//do{
		//	suspendCntVI[i] = m_pThreadsVI[i]->ResumeThread();
		//} while (suspendCntVI[i] > 0);

		isThreadRunningVI[i] = FALSE;
		::WaitForSingleObject(m_pThreadsVI[i]->m_hThread, 5000);
		m_pThreadsVI[i] = NULL;
	}

	return 0;
}

UINT m_fnThreadCam(LPVOID lParam)
{
	ThreadCamData *pThreadCamData = (ThreadCamData *)lParam;
	CVisionInspection *it = pThreadCamData->m_pThis;
	int nThreadIndex = pThreadCamData->m_nThreadIndex;
	int nCamIndex = pThreadCamData->m_nCamIndex;
	delete pThreadCamData;

	wstring strComp;
	switch (nCamIndex) {
	case CAM_PCHI: strComp = STR_MAGCORE_COMP_INNER; break;
	case CAM_PCCA: strComp = STR_MAGCORE_COMP_UPPER; break;
	case CAM_PCCA2: strComp = STR_MAGCORE_COMP_OUTER; break;
	case CAM_PCHI2: strComp = STR_MAGCORE_COMP_LOWER; break;
	}

	it->m_nNoCoreCount = 0;

	int ret = 0L;

	while (it->isThreadRunningVI[nThreadIndex])
	{
		//if (it->checkStatus == CHECK_SUSPEND){
		//	it->suspendCntVI[nCamIndex] = it->m_pThreadsVI[nCamIndex]->SuspendThread();
		//}
		bool bEmpty = it->queViData[nCamIndex].empty();
		if (!bEmpty) {
			VI_RESULT vr;
			vr.vecDefects = new vector < VI_DEFECT >;
			it->csData[nCamIndex].Lock();
			VI_DATA vd = it->queViData[nCamIndex].front();
			it->queViData[nCamIndex].pop();
			it->csData[nCamIndex].Unlock();

			//Vision Inspection
			// 상부
			bool bFoundDefect = false;
			bool bFoundCore = false;
			bool bLoadImage = false;
			bool bSuccess;

#ifndef _VI_SKIP
			try{
				//G_AddLog(3, L"DoVisionInspection Start  CAM1");
				bSuccess = it->DoVisionInspection(strComp, nCamIndex, bFoundDefect, bFoundCore, bLoadImage, vd, vr);
			}
			catch (Exception& e){
				wchar_t* pszTmp = ConvertMultybyteToUnicode(e.what());
				G_AddLog(3, L"DoVisionInspection Exception, CamIndex(%d) : %s", nCamIndex, pszTmp);
				delete[] pszTmp;
				bSuccess = true;
				bFoundDefect = true;
			}

			//G_AddLog(3,L"DoVisionInspection End  CAM 1");
#else
			bSuccess = true;
			bFoundDefect = true;
			for (int j = 0; j < 1; j++)
			{
				VI_DEFECT def;
				def.type = NG_TYP_4;
				def.surface = NG_SURF_TOP;
				def.coords[VI_LEFT] = 0;
				def.coords[VI_TOP] = 0;
				def.coords[VI_RIGHT] = 1;
				def.coords[VI_BOTTOM] = 1;
				def.size = 1;
				vr.vecDefects->push_back(def);
			}

#endif
			if (bSuccess) //VI Succeeded
			{
#ifndef _VI_SKIP
				it->csDisplay[nCamIndex].Lock();
				cvCopy(vd.pImgBuf, G_MainWnd->m_ServerDlg.pImgBuf[nCamIndex]);
				it->csDisplay[nCamIndex].Unlock();
				G_MainWnd->m_ServerDlg.paintImageReady[nCamIndex] = 1;
				::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);
#endif
				if (!bFoundDefect && (it->m_IsCalibrationErr[nCamIndex] == 0)) {//nCamIndex VI PASSED  //160225_kys_Cal_Error is NG judge.
					vr.result = VI_OK;
				}
				else {
					vr.result = VI_NG;
#ifndef _CAM_SKIP
					it->m_fnSaveImage2(nCamIndex, VI_NG, vd.id, vd.pImgBuf);
#endif
					//
#ifdef kys_0401				
					if (it->m_IsCalibrationErr[nCamIndex] != 0)
					{
						G_MainWnd->m_ServerDlg.paintImageReady[nCamIndex] = 1;
						::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);
					}
#endif			
					//					
				}

				if (nCamIndex == CAM_PCCA2) {
					if (!bFoundCore) {
						it->m_nNoCoreCount++;

						if (it->m_nNoCoreCount >= 3) {
							vr.result = VI_ERROR;

							G_AddLog(3, L"Error due to no core detect count : %d, ", it->m_nNoCoreCount);
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 100);
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 100);
							G_MainWnd->m_InspectThread.errorRoutine();
							
							it->m_nNoCoreCount = 0;
						}
					}
					else {
						it->m_nNoCoreCount = 0;
					}
				}
			}
			else{
				vr.result = VI_ERROR;
			}

			cvReleaseImage(&vd.pImgBuf);
			vr.id = vd.id;
			//#ifndef 	VI_MEM_TEST
			it->csResult[nCamIndex].Lock();
			it->queViResults[nCamIndex].push_back(vr);
			it->csResult[nCamIndex].Unlock();
			//#endif
			//try{
			//	it->queViData[nCamIndex].pop();
			//	it->viCS[nCamIndex].Unlock();
			//}
			//catch (Exception& e){
			//	wchar_t* pszTmp = ConvertMultybyteToUnicode(e.what());
			//	G_AddLog(3, L"it->queViData[nCamIndex].pop() Exception: %s", pszTmp);
			//	delete[] pszTmp;
			//}


		}//if (it->vecViData[CAM_PCHI].size > 0 ){
		else{
			//Sleep(1);
		}
		Sleep(1);
	}//while

	return ret;
}


////////////////////////////////////////////////////////////////////////////////////////////
//Cam threads

int CVisionInspection::m_fnBeginCamThread(void)
{
	m_LiveHandls[0].bLiveRun = TRUE;
	m_LiveHandls[1].bLiveRun = TRUE;
	m_LiveHandls[2].bLiveRun = TRUE;
	m_LiveHandls[3].bLiveRun = TRUE;
	//m_pLiveThread[0] = ::AfxBeginThread(m_fnLiveThread, &m_LiveHandls[0], THREAD_PRIORITY_HIGHEST);
	//m_pLiveThread[1] = ::AfxBeginThread(m_fnLiveThread, &m_LiveHandls[1], THREAD_PRIORITY_HIGHEST);
	m_pLiveThread[0] = AfxBeginThread(m_fnLiveThread, &m_LiveHandls[0], THREAD_PRIORITY_HIGHEST, 0, DETACHED_PROCESS, NULL);
	m_pLiveThread[1] = AfxBeginThread(m_fnLiveThread, &m_LiveHandls[1], THREAD_PRIORITY_HIGHEST, 0, DETACHED_PROCESS, NULL);
	m_pLiveThread[2] = AfxBeginThread(m_fnLiveThread, &m_LiveHandls[2], THREAD_PRIORITY_HIGHEST, 0, DETACHED_PROCESS, NULL);
	m_pLiveThread[3] = AfxBeginThread(m_fnLiveThread, &m_LiveHandls[3], THREAD_PRIORITY_HIGHEST, 0, DETACHED_PROCESS, NULL);

	return 0;
}


int CVisionInspection::m_fnEndCamThread()
{
	m_LiveHandls[0].bLiveRun = FALSE;
	m_LiveHandls[1].bLiveRun = FALSE;
	m_LiveHandls[2].bLiveRun = FALSE;
	m_LiveHandls[3].bLiveRun = FALSE;
	::WaitForSingleObject(m_pLiveThread[0]->m_hThread, 5000);
	m_pLiveThread[0] = NULL;
	::WaitForSingleObject(m_pLiveThread[1]->m_hThread, 5000);
	m_pLiveThread[1] = NULL;
	::WaitForSingleObject(m_pLiveThread[2]->m_hThread, 5000);
	m_pLiveThread[2] = NULL;
	::WaitForSingleObject(m_pLiveThread[3]->m_hThread, 5000);
	m_pLiveThread[3] = NULL;

	return 0;
}

UINT m_fnLiveThread(LPVOID lParam)
{
	int ret = 0L;

	DsLive *pData = (DsLive *)lParam;
	SMP_LIVE *pImgData = pData->pImgData;
	int ImageSize = LIVE_SIZE_W*LIVE_SIZE_H;

	IplImage * pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);

	while (pData->bLiveRun == TRUE && pImgData != NULL)
	{
#ifndef VIRTUAL_RUN
		if (pImgData->LiveCheck[pData->CamIndex] == 2)
		{
			pImgData->LiveCheck[pData->CamIndex] = 4;
			if (pData->flagCapture[pData->CamIndex] == TRUE)
			{
				memcpy(pData->m_ViewImage->imageData, pImgData->LiveImage[pData->CamIndex], ImageSize);
				pData->flagCapture[pData->CamIndex] = FALSE;
			}

		}
#else
		if (pData->flagCapture[pData->CamIndex] == TRUE)
		{
			//memcpy(pData->m_ViewImage->imageData, pImgData->LiveImage[pData->CamIndex], ImageSize);
			pData->flagCapture[pData->CamIndex] = FALSE;
		}
#endif
		else
		{
			//20160229_kys_LiveThread_Test
			//Sleep(10);
			Sleep(1);
			continue;
		}


		/////////////////////////////////////////////////////
		//20160229_kys_LiveThread_Test
		//Sleep(20);
		Sleep(3);
	}

	return ret;
}

void CVisionInspection::DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect, int CamIdx)
{
	if (pImgIpl == NULL) return;

	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biXPelsPerMeter = 100;
	bitmapInfo.bmiHeader.biYPelsPerMeter = 100;
	bitmapInfo.bmiHeader.biClrUsed = 0;
	bitmapInfo.bmiHeader.biClrImportant = 0;
	bitmapInfo.bmiHeader.biSizeImage = 0;
	bitmapInfo.bmiHeader.biWidth = pImgIpl->width;
	bitmapInfo.bmiHeader.biHeight = -pImgIpl->height;
	IplImage* tempImage;
	if (pImgIpl->nChannels == 3)
	{
		tempImage = (IplImage*)cvClone(pImgIpl);
		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
	}
	else if (pImgIpl->nChannels == 1)
	{
		tempImage = cvCreateImage(cvGetSize(pImgIpl), IPL_DEPTH_8U, 3);
		cvCvtColor(pImgIpl, tempImage, CV_GRAY2BGR);
		bitmapInfo.bmiHeader.biBitCount = tempImage->depth * tempImage->nChannels;
	}
	//20160319_KYS
	IplImage* LinedImage = cvCloneImage(tempImage);
	cvLine(LinedImage, cvPoint(LinedImage->width / 2, 0), cvPoint(LinedImage->width / 2, LinedImage->height), CV_RGB(255, 0, 0), 3, 8, 0);
	cvLine(LinedImage, cvPoint(0, LinedImage->height / 2), cvPoint(LinedImage->width, LinedImage->height / 2), CV_RGB(255, 0, 0), 3, 8, 0);
	//
	//
#ifdef kys_0401	
	char www[256];
	char ttt[256];
	//20160401_kys

	sprintf(www, "CAM%d", CamIdx);
	CvFont FontKYS;
	cvInitFont(&FontKYS, CV_FONT_HERSHEY_DUPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 3, 3, 0, 6);
	cvPutText(LinedImage, www, cvPoint(25, 100), &FontKYS, CV_RGB(255, 0, 0));
	int nCalErr = G_MainWnd->m_InspectThread.m_vi.m_IsCalibrationErr[CamIdx - 1];
	if (nCalErr != 0) {
		if (nCalErr == NG_CAL_ERR) strcpy(ttt, "CAL-ERR");
		else if (nCalErr == NG_CAL_ERR_NO_IMAGE) strcpy(ttt, "NO-IMAGE");
		cvInitFont(&FontKYS, CV_FONT_HERSHEY_TRIPLEX/*CV_FONT_HERSHEY_SIMPLEX*/, 1.5, 2.0, 0.0, 4);
		cvPutText(LinedImage, ttt, cvPoint(25, 200), &FontKYS, CV_RGB(0, 255, 0));
	}

	cvDrawRect(LinedImage, cvPoint(4, 4), cvPoint(LinedImage->width - 4, LinedImage->height - 4), CV_RGB(0, 255, 0), 3);
#endif
	//
	pDC->SetStretchBltMode(COLORONCOLOR);
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(),
		0, 0, LinedImage->width, LinedImage->height, LinedImage->imageData, &bitmapInfo,
		DIB_RGB_COLORS, SRCCOPY);
	cvReleaseImage(&LinedImage);
	cvReleaseImage(&tempImage);

	//::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 
	//	0, 0, tempImage->width, tempImage->height, tempImage->imageData, &bitmapInfo, 
	//	DIB_RGB_COLORS, SRCCOPY);
	//cvReleaseImage(&tempImage);
}

