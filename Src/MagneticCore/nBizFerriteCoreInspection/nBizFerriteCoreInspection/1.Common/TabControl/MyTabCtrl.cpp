// MyTabCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "..\..\MaskServer.h"
#include "..\..\MaskServerDlg.h"
#include "MyTabCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl

CMyTabCtrl::CMyTabCtrl(CWnd *pParent)
{
	m_pdlgBrush = NULL;
	m_pdlgBrush = ::CreateSolidBrush(RGB(152,156,192));
}

CMyTabCtrl::~CMyTabCtrl()
{
	DeleteObject(m_pdlgBrush);
}


BEGIN_MESSAGE_MAP(CMyTabCtrl, CTabCtrl)
	//{{AFX_MSG_MAP(CMyTabCtrl)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnSelchange)
	//}}AFX_MSG_MAP
//	ON_WM_PAINT()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl message handlers

int CMyTabCtrl::m_fnAddTab(CWnd* pWnd, LPTSTR lpszCaption)
{
	int iIndex = GetItemCount();
	CRect myRect;
	GetClientRect(&myRect);

	InsertItem(iIndex, lpszCaption, iIndex);

	pWnd->SetWindowPos(NULL, 10, 30, 0, 0, SWP_FRAMECHANGED | SWP_NOSIZE | SWP_NOZORDER);

	return 0;
}

int CMyTabCtrl::m_fnInit(CWnd *pParent)
{
	m_pParent = (CMaskServerDlg*)pParent;
	m_nCurTabIndex = 0;

	return 0;
}

void CMyTabCtrl::OnSelchange(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	m_nCurTabIndex = GetCurSel();
	SetCurSel(m_nCurTabIndex);

	switch(m_nCurTabIndex)	
	{
	case 0:
		{
 		m_pParent->m_pMapTabDlg->ShowWindow(SW_SHOW);
 		m_pParent->m_pMapImageDlg->ShowWindow(SW_HIDE);
		break;
		}
	case 1:
		{
		m_pParent->m_pMapTabDlg->ShowWindow(SW_HIDE);
		m_pParent->m_pMapImageDlg->ShowWindow(SW_SHOW);
		break;
		}
	case 2:
		{
		break;
		}
	case 3:
		{
		break;
		}
	default:
		{
		m_pParent->m_pMapTabDlg->ShowWindow(SW_SHOW);
		m_pParent->m_pMapImageDlg->ShowWindow(SW_HIDE);
		break;
		}
	}	

	*pResult = 0;
}

void CMyTabCtrl::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CTabCtrl::PostNcDestroy();
}