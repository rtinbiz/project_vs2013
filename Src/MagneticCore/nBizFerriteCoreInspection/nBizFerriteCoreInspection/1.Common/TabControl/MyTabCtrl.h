#if !defined(AFX_MYTABCTRL_H__AE24340C_873D_412C_B52B_12394BC82E2E__INCLUDED_)
#define AFX_MYTABCTRL_H__AE24340C_873D_412C_B52B_12394BC82E2E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyTabCtrl.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CMyTabCtrl window

class CMaskServerDlg;

class CMyTabCtrl : public CTabCtrl
{
// Construction
public:
	CMyTabCtrl(CWnd *pParent = NULL);	

	CnBizFerriteCoreInspectionDlg			*m_pParent;
	int						m_nCurTabIndex;
	HBRUSH					m_pdlgBrush;

	int		m_fnAddTab(CWnd* pWnd, LPTSTR lpszCaption);
	int		m_fnInit(CWnd *pParent);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyTabCtrl)
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyTabCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyTabCtrl)
	afx_msg void OnSelchange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
//	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
//	afx_msg void OnPaint();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYTABCTRL_H__AE24340C_873D_412C_B52B_12394BC82E2E__INCLUDED_)
