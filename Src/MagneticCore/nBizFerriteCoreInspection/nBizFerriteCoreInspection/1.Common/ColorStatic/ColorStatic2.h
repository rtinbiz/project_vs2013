// ColorStatic2.h: interface for the CColorStatic2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COLORSTATIC2_H__DC6BEAD9_4BC2_476F_B405_CF25CD8CDAF7__INCLUDED_)
#define AFX_COLORSTATIC2_H__DC6BEAD9_4BC2_476F_B405_CF25CD8CDAF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

////////////////////////////////////////////////
// Color Define.
#define SMART_RED			RGB(255,128,128)
#define SMART_BLUE			RGB(128,128,255)
#define SMART_GREEN			RGB(128,255,128)
#define SMART_YELLOW		RGB(255,255,128)
#define SMART_MAGENTA		RGB(255,128,255)
#define SMART_BRIGHT_BLUE	RGB(128,255,255)

#define DARK_GRAY			RGB(128,128,128)
#define BRIGHT_GRAY			RGB(200,200,200)
#define GRAY				RGB(84, 84, 84)

#define WHITE				RGB(255,255,255)
#define BLACK				RGB(0,0,0)
#define GREEN				RGB(0,255,0)
#define BLUE				RGB(0,0,255)
#define RED					RGB(255,0,0)
#define YELLOW				RGB(255,255,0)
#define MAGENTA				RGB(255,0,255)

#define DEEP_BLUE			RGB(0,0,125)
////////////////////////////////////////////////

class CColorStatic2 : public CStatic
{
public:
	CColorStatic2();
	virtual ~CColorStatic2();

	void	SetText(CString strText, COLORREF textref=BLACK, COLORREF batangref=BRIGHT_GRAY);
	void	SetBackColor(COLORREF ref=BRIGHT_GRAY);
	void	SetBackTextColor(COLORREF ref=BRIGHT_GRAY, COLORREF textref=BLACK);
	void	SetTextColor2(COLORREF ref=BLACK);
	void	SetFont2(int p_nHeight, int p_nWidth, int p_nWeight, CString strFace=_T(""));

	void	StartBlink(int nPeriod, COLORREF first, COLORREF next);
	void	StopBlink();
	int		IsBlink();

	COLORREF GetBackColor();
	COLORREF GetTextColor();
	CString  GetText();

protected:

	COLORREF		m_refText;
	COLORREF		m_refBack;
	CBrush			m_brush;
	CFont			m_myFont;
	int				m_nBlink;

	COLORREF		m_refBlinkColor1;
	COLORREF		m_refBlinkColor2;


	//{{AFX_MSG(CColorStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

#endif // !defined(AFX_COLORSTATIC2_H__DC6BEAD9_4BC2_476F_B405_CF25CD8CDAF7__INCLUDED_)
