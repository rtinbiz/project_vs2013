#include "stdafx.h"
#include "nBizFerriteCoreInspection.h"
#include "SystemSetupDlg.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(CSystemSetupDlg, CDialogEx)

extern CMainWnd	*G_MainWnd;

CSystemSetupDlg::CSystemSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSystemSetupDlg::IDD, pParent)
{

}

CSystemSetupDlg::~CSystemSetupDlg()
{
}

void CSystemSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRID_SYS_PARAM, m_ctrMaintGrid);
	DDX_Control(pDX, IDC_BTN_SYS_PARAM_READ_ONLY, m_btnSystemReadOnly);
	DDX_Control(pDX, IDC_BTN_SYS_PARAM_SET_EDIT, m_btnSystemSetEditable);
	DDX_Control(pDX, IDC_BTN_SYS_PARAM_SAVE, m_btnSystemParamSave);
}


BEGIN_MESSAGE_MAP(CSystemSetupDlg, CDialogEx)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_SYS_PARAM, OnGridDblClick)
	ON_NOTIFY(NM_CLICK, IDC_GRID_SYS_PARAM, OnGridClick)

	ON_BN_CLICKED(IDC_BTN_SYS_PARAM_READ_ONLY, &CSystemSetupDlg::OnBnClickedBtnSysParamReadOnly)
	ON_BN_CLICKED(IDC_BTN_SYS_PARAM_SET_EDIT, &CSystemSetupDlg::OnBnClickedBtnSysParamSetEdit)
	ON_BN_CLICKED(IDC_BTN_SYS_PARAM_SAVE, &CSystemSetupDlg::OnBnClickedBtnSysParamSave)
END_MESSAGE_MAP()


// CSystemSetupDlg 메시지 처리기입니다.
void CSystemSetupDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Double Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

void CSystemSetupDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

void CSystemSetupDlg::m_fnInit()
{	
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
}


BOOL CSystemSetupDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	//#ifdef PORT_VIRTURE
	CString str;
	// 
	for ( int i = 1; i <= 8 ; i++ )
	{
		str.Format(_T("COM%d"), i);

		m_strComPortArray.Add(str);
	}

	// #else
	// 	COMMCONFIG lpCC;
	// 	BOOL bExistFlag = FALSE;
	// 	unsigned long ccsize = sizeof(COMMCONFIG);
	// 	CString str;
	// 
	// 	for ( int i = 1; i < 20 ; i++ )
	// 	{		
	// 		str.Format(L"COM%d",i);	
	// 		bExistFlag = FALSE;
	// 		bExistFlag = GetDefaultCommConfig(str, &lpCC, &ccsize); // 중요 포인트 return값이 TRUE이면 COMPORT존재
	// 		if ( bExistFlag )
	// 		{
	// 			m_strComPortArray.Add(str);
	// 		}
	// 	}
	// #endif

	// system param grid **********************
	try {
		m_ctrMaintGrid.SetRowCount(SYS_PARAM_STRING + SYS_PARAM_FLOAT + SYS_PARAM_INT);
		m_ctrMaintGrid.SetColumnCount(3);
		m_ctrMaintGrid.SetFixedRowCount(1);		
		m_ctrMaintGrid.SetFixedColumnCount(2);
		m_ctrMaintGrid.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	for (int row = 0; row < m_ctrMaintGrid.GetRowCount(); row++)
	{
		for (int col = 0; col < m_ctrMaintGrid.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;

				if (col == 0)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" NUM "));					
				}
				else if( col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                SYSTEM PARAM                 "));
				}
				else if( col == 2)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                 VALUE                 "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"%d", row);					
					}
				}
				else if(col == 1)
				{
					Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
					int nSize = sizeof(ST_TABLE_SYSTEM_0_COL) / 8;
					if(row-1 < nSize)
						Item.strText.Format(L"%s", ST_TABLE_SYSTEM_0_COL[row-1]);
				}	
			}

			m_ctrMaintGrid.SetItem(&Item);
		}
	}

	m_ctrMaintGrid.AutoSize(GVS_BOTH);
	m_ctrMaintGrid.SetTextColor(RGB(0, 0, 105));
	m_ctrMaintGrid.SetEditable(FALSE);
	m_btnSystemSetEditable.EnableWindow(TRUE);
	m_btnSystemReadOnly.EnableWindow(FALSE);
	m_ctrMaintGrid.Refresh();	

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CSystemSetupDlg::m_fnSystemUILoad()
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;

	m_fnUpdateComport();

	BOOL	bUse= FALSE;
	CString strValueData; 	

	GV_ITEM Item;	
	Item.col = 2;		

	int nSize = sizeof(ST_TABLE_SYSTEM_0_COL) / 8 + 1;

	for (int row = 1; row < nSize; row++)											
	{ 		
		strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_TABLE_SYSTEM_0_COL[row-1]);
		if(strValueData == "")
			continue;
		m_ctrMaintGrid.SetItemText(row, Item.col, strValueData);	
	}
	m_ctrMaintGrid.Refresh();	
}


void CSystemSetupDlg::m_fnUpdateComport()
{
	CString strValue;
	int nIndex = 0;

	strValue = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_COMPORT_1);
	G_MainWnd->m_SerialInterface.m_nComportIndex[0] = (_wtoi(strValue) - 1);
	strValue = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_COMPORT_2);
	G_MainWnd->m_SerialInterface.m_nComportIndex[1] = (_wtoi(strValue) - 1);
	strValue = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_COMPORT_3);
	G_MainWnd->m_SerialInterface.m_nComportIndex[2] = (_wtoi(strValue) - 1);

}


void CSystemSetupDlg::OnBnClickedBtnSysParamReadOnly()
{
	m_ctrMaintGrid.SetEditable(FALSE);

	m_btnSystemSetEditable.EnableWindow(TRUE);
	m_btnSystemReadOnly.EnableWindow(FALSE);

	m_ctrMaintGrid.Refresh();	
}


void CSystemSetupDlg::OnBnClickedBtnSysParamSetEdit()
{
	m_ctrMaintGrid.SetEditable(TRUE);

	m_btnSystemSetEditable.EnableWindow(FALSE);
	m_btnSystemReadOnly.EnableWindow(TRUE);

	m_ctrMaintGrid.Refresh();	
}


void CSystemSetupDlg::OnBnClickedBtnSysParamSave()
{
	CString strValueData;
	GV_ITEM Item;
	Item.col = 2;
	for (int row = 1; row < m_ctrMaintGrid.GetRowCount(); row++)
	{
		strValueData = m_ctrMaintGrid.GetItemText(row, Item.col);
		if(strValueData == "")
			continue;
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_TABLE_SYSTEM_0_COL[row-1], strValueData);		
	}
	m_fnSystemUILoad();
	G_MainWnd->m_DataHandling.m_fnSystemParamLoad();
	G_MainWnd->m_ServerDlg.m_staticInsp.SetText(G_MainWnd->m_DataHandling.m_SystemParam.LINE_ID, BLACK, WHITE);

}
