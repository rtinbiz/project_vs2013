#pragma once
#include "afxdialogex.h"
#include "Resource.h"
#include "afxwin.h"
#include "stdafx.h"
#include "1.Common\grid64\gridctrl.h"
#include "1.Common/IniClass/BIniFile.h"
#include "PathDefine.h"
#include "pmiMApi.h"
#include "pmiMApiDefs.h"
#include "1.Common\colorstatic\colorstatic2.h"
#include "1.Common\roundbutton\roundbutton2.h"


#define CARD_NO					0	// LCR
#define CARD_IO					0

#define AXIS_IN_CNVYR		0
#define AXIS_OUT_CNVYR		1

//
#define NUM_AXIS		2	//LCR AXIS No
#define NUM_AXIS_GRID_COL			NUM_AXIS+1 //AXIS NO + TEST NO.
#define NUM_STEP			100

#define		SECTION_AXIS_DATA		_T("AXIS_DATA")
#define		KEY_IN_STEP					_T("NO_IN_STEP")

///////////////////////////////////////////////////////////////
//UM_AXIS

#define UM_AXIS						WM_USER + 200 

#define UW_PROBE_X				0
#define UW_PROBE_Y				1
#define UW_PROBE_Z				2
#define UW_PROBE_T				3
#define UW_PROBE_L				4
#define UW_SHORT_X				5
#define UW_SHORT_Y				6
#define UW_SHORT_Z				7
#define UW_SHORT_T				8
#define UW_SHORT_L				9
#define UW_SHUTTLE				10

#define UW_CHECK_POS			11
#define UW_STEP_MOVE			12

#define UW_READY_TO_HOME			13
#define UW_LCR_HOME_TO_READY		14
#define UW_LCR_READY_HOME_READY	15
#define UW_SHUTTLE_SYS_INIT			16

#define UM_AXIS_CLEAR			0
#define UM_AXIS_ONGOING		1
#define UM_AXIS_OK				2
#define UM_AXIS_NG				4

typedef struct _LCR_AXIS_MSG
{

	int BUSY; 
	int RSP[UW_LCR_HOME_TO_READY + 1];


	_LCR_AXIS_MSG(){	memset(this,0x00, sizeof(LCR_AXIS_MSG));}
}LCR_AXIS_MSG;
	//LCR_AXIS_MSG axisMsgRsp;

//PROBE
#define LIMIT_SHUTTLE_MIN		 -1000
#define LIMIT_SHUTTLE_MAX      400000 

#define LIMIT_OUT_CNVYR_MIN      -1000
#define LIMIT_OUT_CNVYR_MAX      150000   


// m_cbActionMode.AddString(_T("0 : Not Stop"));
// m_cbActionMode.AddString(_T("1 : Immediately Stop"));

#define SOFT_LIMIT_N_STOP				0x00 //인터럽트 신호 만 발생
#define SOFT_LIMIT_E_STOP				0x01 //즉시 정지
#define SOFT_LIMIT_S_STOP				0x02 //감속 후 정지


enum
{
	Axis_Transfer=0,
	Axis_OutCnvyr,

};

typedef struct _LCR_AXIS_POSITION
{
	wchar_t part_model[256];
	wchar_t part_type[256]; //
	int steps; //total actual step number

	double absPos[NUM_STEP][NUM_AXIS_GRID_COL];

	_LCR_AXIS_POSITION(){	memset(this,0x00, sizeof(LCR_AXIS_POSITION));}
}LCR_AXIS_POSITION;

class AxisMotion : 	public CDialogEx
{
	DECLARE_DYNAMIC(AxisMotion)

public:
	
	int axisCard; //
	double m_dAxisCmdPos[8];
	CCriticalSection m_csAxisPos;

	LCR_AXIS_POSITION axisData;
	LCR_AXIS_MSG axisMsgRsp;

	int m_nMotEvtCnt;
	void RefreshParameter();
	void SetHomeMoveSpeed(int iAxisNo);
	void SetPosMoveSpeed(int iAxisNo);
	void SetJogMoveSpeed(int iAxisNo);

	int m_nBoardType;
	void InitDlgItem();
	int m_iDoNum, m_iDoNum2;
	int m_iDiNum, m_iDiNum2;
	int m_iAxisNum, m_iAxisNum2;
	void PosAndVelDisplay();
	void CardStatusDisplay();
	BOOL ReturnMessage(int iRet);
	CBitmap m_BitMapLedG;
	CBitmap m_BitMapLedR;
	CBitmap m_BitMapLedW;

	CEdit	m_EdtVelocity[8];
	CEdit	m_EdtActPos[8];
	CEdit	m_EdtCmdPos[8];
	CStatic	m_BtmRstStatus;
	CStatic	m_BtmSonStatus;
	CStatic	m_BtmCclrStatus;
	CStatic	m_BtmNLmtStatus;
	CStatic	m_BtmPLmtStatus;
	CStatic	m_BtmAlmStatus;
	CStatic	m_BtmInpStatus;
	CStatic	m_BtmEmgStatus;
	CStatic	m_BtmEzStatus;
	CStatic	m_BtmOrgStatus;
	CComboBox	m_CbAxisSelect;
	CComboBox	m_CbHomeMode;
	CComboBox	m_CbSelectEncoderMode;
	CComboBox	m_CbSelectPulseMode;
	//CComboBox	m_CbSelectRange;
	CComboBox m_CbSpeedMode;

	AxisMotion(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~AxisMotion();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_AXIS2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()


public:
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	void m_fnDeInit(void);

	HICON m_hIcon;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	// 생성된 메시지 맵 함수
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnChkOrgLogic();
	afx_msg void OnChkEzLogic();
	afx_msg void OnChkEmgLogic();
	afx_msg void OnChkInpLogic();
	afx_msg void OnChkAlmLogic();
	afx_msg void OnChkLmtLogic();
	afx_msg void OnChkInpEnable();
	afx_msg void OnClose();
	afx_msg void OnSelchangeCbRange();
	afx_msg void OnChkEncoderDir();
	afx_msg void OnChkHomeDir();
	afx_msg void OnBtnEstop();
	afx_msg void OnBtnSstop();
	//afx_msg void OnBtnPjog();
	//afx_msg void OnBtnNjog();
	afx_msg void OnBtnIncmove();
	afx_msg void OnBtnAbsmove();
	afx_msg void OnBtnHomemove();
	afx_msg void OnSelchangeCbPulseMode();
	afx_msg void OnSelchangeCbEncoderMode();
	afx_msg void OnSelchangeCbHomeMode();
	afx_msg void OnSelchangeCbAxisSelect();
	afx_msg void OnBtnMultiIncmove();
	afx_msg void OnBtnMultiAbsmove();
	afx_msg void OnBtnClearPos();
	afx_msg void OnBtnMultiEstop();
	afx_msg void OnBtnMultiSstop();
	afx_msg void OnBtnOverrideSpeed();
	afx_msg void OnBtnOverrideInc();
	afx_msg void OnBtnOverrideAbs();
	afx_msg void OnBtnCclr();
	afx_msg void OnBtnSvonOnoff();
	afx_msg void OnBtnRstOnoff();
	afx_msg void OnCbnSelchangeCbSpeedMode();

	CBIniFile		m_iniLCRAxisData;
	int InStep, InAxis;

	CString m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default);
	int applyAxisData(CString recipe);
	int homeLCRAxis(void);
	int homeShuttle(void);
	CEdit m_EdtCmdPos8;
	CEdit m_EdtActPos8;
	CEdit m_EdtVelocity8;

	CString m_fnGetAxisName(int iAxis);
	BOOL m_fnSetPosMoveSpeed(int iAxisNo);
	BOOL m_fnAbsmove(int iAxisNo, double nPos);
	BOOL m_fnIncmove(int iAxisNo, double nPos);
	BOOL m_fnSetHomeMoveSpeed(int iAxisNo);
	BOOL m_fnHomemove(int iAxisNo);
	BOOL m_fnSoftLimitEnable(BOOL bUse, int iAxisNo, BOOL bStopMode = 0);
	int m_fnAxCheckDone(int iAxisNo, int * nDone, int nWait);
	int m_fnAxHomeCheckDone(int iAxisNo, int * nDone, int nWait);
	BOOL m_fnClearPos(int iAxisNo);
	int m_fnAxisHome(int iAxis);

	int initAxis(int iAxisNo);

	afx_msg void OnBnClickedBtnLcrAxisAlarmReset(UINT nID);
	afx_msg void OnBnClickedBtnShuttleAlarmReset();

	int m_fnDisplayShuttleStatus(void);

	CRoundButton2 m_btnShuttleEStop;

	void SetUMMsgResponse(int msg, int rsp);
	int GetUMMsgResponse(int msg);

	CEdit m_EdtCmdPosCvyor;
	CEdit m_EdtActPosCvyor;
	CEdit m_EdtVelocityCvyor;
	CRoundButton2 m_btnOutCnvyorIncSet;
	CColorStatic2 m_staticOutCnvyorInc;
	afx_msg void OnBnClickedBtnOutCnvyorSet();
	afx_msg void OnBnClickedBtnCnvyorMove();
	CRoundButton2 m_btnOutCnvyrIncMove;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton9();
	CRoundButton2 m_btnAxisSetEnable;
	CRoundButton2 m_btnAxisSetDisable;
	afx_msg void OnBnClickedAxisAlarmReset2();
	CRoundButton2 m_btnInCnvyorIncSet;
	CRoundButton2 m_btnInCnvyrIncMove;
	CRoundButton2 m_btnOutCnvyrReset;
	CRoundButton2 m_btnInCnvyrReset;
	CRoundButton2 m_btnOutCnvyrClearPos;
	CRoundButton2 m_btnInCnvyrClearPos;
	CRoundButton2 m_btnAxisSetEnable2;
	CRoundButton2 m_btnAxisSetDisable2;
	afx_msg void OnBnClickedAxisAlarmReset();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedBtnCnvyorMove2();
	afx_msg void OnBnClickedBtnCnvyorSet2();
	CColorStatic2 m_staticInCnvyorInc;
	CRoundButton2 m_btnAxisEMStop;
	afx_msg void OnBnClickedAxisClear2();
	afx_msg void OnBnClickedAxisClear();
	afx_msg void OnBnClickedAxisAlarmReset3();
};

