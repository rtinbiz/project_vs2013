del *.obj /s
del Buildlog.htm /s
del *.manifest /s
del *.manifest.res /s
del *.pch /s
del *.ipch /s
del *.pdb /s
del *.dep /s
del *.idb /s
del *.ncb /s
del *.suo /s /a:h
del *.sbr /s
del *.ilk /s
del *.exp /s
del *.bsc /s
del *.sdf /s
del *.tlog /s
pause
