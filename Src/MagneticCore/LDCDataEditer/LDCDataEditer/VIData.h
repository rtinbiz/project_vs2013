#pragma once

#include "Xml/xml3.h"
using namespace XML3;

#define STR_MODEL L"Model"
#define STR_GUIDELINE L"GuideLine"
#define STR_NORMALIZE L"Normalize"

#define STR_VIDATA_VERSION "1.0"

enum COMP_INDEX {
	COMP_ALL = 0,
	COMP_TERMINAL,
	COMP_TUBE,
	COMP_GUIDE,
	COMP_BOLT,
	COMP_PAPER,
	COMP_CLIP
};

struct VIData_Model {
	wstring m_strMachine;
	wstring m_strNo;

	VIData_Model() {
		clear();
	}

	void clear() {
		m_strMachine.clear();
		m_strNo.clear();
	}
};

struct VIData_Calibration {
	Point m_ptGuideLineLeftStart;
	Point m_ptGuideLineLeftEnd;
	Point m_ptGuideLineRightEnd;
	int m_nNormalizeWidth;

	VIData_Calibration() {
		clear();
	}

	void clear() {
		m_ptGuideLineLeftStart = Point(0, 0);
		m_ptGuideLineLeftEnd = Point(0, 0);
		m_ptGuideLineRightEnd = Point(0, 0);
		m_nNormalizeWidth = 0;
	}
};

struct VIData_ROI {
	int m_nCompIndex;

	int m_nCenterX;
	int m_nCenterY;
	int m_nRadius;

	wstring m_strColor;
	int m_nStartX;
	int m_nStartY;
	int m_nEndX;
	int m_nEndY;
	int m_nRefX;
	int m_nRefY;
	int m_nSetId;

	VIData_ROI() {
		m_nCompIndex = 0;

		m_nCenterX = 0;
		m_nCenterY = 0;
		m_nRadius = 0;

		m_strColor = L"";
		m_nStartX = 0;
		m_nStartY = 0;
		m_nEndX = 0;
		m_nEndY = 0;
		m_nRefX = 0;
		m_nRefY = 0;
		m_nSetId = 0;
	}

	Rect GetBoundingBox();
	Rect GetRefBoundingBox();
	CPoint GetBoundingBoxCenterPos();

	bool operator<(const VIData_ROI& dROI)
	{
		if (m_nCompIndex != dROI.m_nCompIndex) {
			return (m_nCompIndex < dROI.m_nCompIndex);
		}
		if (!CHECK_STRING(m_strColor, dROI.m_strColor)) {
			return (wstricmp(m_strColor, dROI.m_strColor) > 0);
		}
		if (m_nSetId != dROI.m_nSetId) {
			return (m_nSetId < dROI.m_nSetId);
		}

		switch (m_nCompIndex) {
		case COMP_TERMINAL:
			if (m_nCenterY != dROI.m_nCenterY) {
				return (m_nCenterY < dROI.m_nCenterY);
			}
			if (m_nCenterX != dROI.m_nCenterX) {
				return (m_nCenterX < dROI.m_nCenterX);
			}
			if (m_nRadius != dROI.m_nRadius) {
				return (m_nRadius < dROI.m_nRadius);
			}
			break;
		case COMP_TUBE :
			if (m_nStartY != dROI.m_nStartY) {
				return (m_nStartY < dROI.m_nStartY);
			}
			if (m_nEndY != dROI.m_nEndY) {
				return (m_nEndY < dROI.m_nEndY);
			}
			if (m_nStartX != dROI.m_nStartX) {
				return (m_nStartX < dROI.m_nStartX);
			}
			if (m_nEndX != dROI.m_nEndX) {
				return (m_nEndX < dROI.m_nEndX);
			}
			break;
		default:
			if (m_nStartY != dROI.m_nStartY) {
				return (m_nStartY < dROI.m_nStartY);
			}
			if (m_nEndY != dROI.m_nEndY) {
				return (m_nEndY < dROI.m_nEndY);
			}
			if (m_nStartX != dROI.m_nStartX) {
				return (m_nStartX < dROI.m_nStartX);
			}
			if (m_nEndX != dROI.m_nEndX) {
				return (m_nEndX < dROI.m_nEndX);
			}
			break;
		}

		return false;
	}
};


class VIData {
public :
	VIData_Model m_dModel;
	VIData_Calibration m_dCal;
	vector<VIData_ROI> m_listTerminal;
	vector<VIData_ROI> m_listTube;
	vector<VIData_ROI> m_listGuide;
	vector<VIData_ROI> m_listBolt;
	vector<VIData_ROI> m_listPaper;
	vector<VIData_ROI> m_listClip;

	VIData() {
	}

	virtual ~VIData() {
		Clear();
	}

	void Clear() {
		m_dModel.clear();
		m_listTerminal.clear();
		m_listTube.clear();
		m_listGuide.clear();
		m_listBolt.clear();
		m_listPaper.clear();
		m_listClip.clear();
	}

	bool LoadData(wstring strDataPath);
	bool SaveData(wstring strDataPath);
	bool LoadCalibrationData(wstring strDataPath);
	bool SaveCalibrationData(wstring strDataPath);

	VIData_ROI AddData(int nCompIndex, int nImgWidth, int nImgHeight);
	void DeleteData(VIData_ROI dROI);
	void VIData::SortData(int nCompIndex);

private :
	void _SetXMLData(wstring strElementName, vector<VIData_ROI> &listROI, XMLElement &dXMLParent);
	vector<VIData_ROI> *VIData::_GetDataList(int nCompIndex);
};