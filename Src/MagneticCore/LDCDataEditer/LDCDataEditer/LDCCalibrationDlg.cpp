// LDCCalibrationDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "LDCDataEditer.h"
#include "LDCCalibrationDlg.h"
#include "afxdialogex.h"


// CLDCCalibrationDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLDCCalibrationDlg, CDialogEx)

CLDCCalibrationDlg::CLDCCalibrationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLDCCalibrationDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CLDCCalibrationDlg::~CLDCCalibrationDlg()
{
}

void CLDCCalibrationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Text(pDX, IDC_IMAGE_INFO, m_strImageInfo);
	DDX_Text(pDX, IDC_IMAGE_POS, m_strImagePos);
	DDX_Text(pDX, IDC_GUIDELINE_LEFT_START_POS_X, m_nGuideLineLeftStartX);
	DDX_Text(pDX, IDC_GUIDELINE_LEFT_START_POS_Y, m_nGuideLineLeftStartY);
	DDX_Text(pDX, IDC_GUIDELINE_LEFT_END_POS_X, m_nGuideLineLeftEndX);
	DDX_Text(pDX, IDC_GUIDELINE_LEFT_END_POS_Y, m_nGuideLineLeftEndY);
	DDX_Text(pDX, IDC_GUIDELINE_RIGHT_END_POS_X, m_nGuideLineRightEndX);
	DDX_Text(pDX, IDC_GUIDELINE_RIGHT_END_POS_Y, m_nGuideLineRightEndY);
	DDX_Text(pDX, IDC_NORMALIZE_WIDTH, m_nNormalizeWidth);
}

BEGIN_MESSAGE_MAP(CLDCCalibrationDlg, CDialogEx)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_RELOAD_DATA, &CLDCCalibrationDlg::OnBnClickedReloadData)
	ON_BN_CLICKED(IDC_SAVE_DATA, &CLDCCalibrationDlg::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_DATA_APPLY, &CLDCCalibrationDlg::OnBnClickedDataApply)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CLDCCalibrationDlg::OnImagePosValue)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_OPEN_IMAGE, &CLDCCalibrationDlg::OnBnClickedOpenImage)
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CLDCCalibrationDlg::OnBnClickedLoadImage)
END_MESSAGE_MAP()


// CLDCCalibrationDlg 메시지 처리기입니다.


void CLDCCalibrationDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	GetWindowRect(&m_rcPos);

	CDialogEx::OnClose();
}

void CLDCCalibrationDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	GetWindowRect(&m_rcPos);

	CDialogEx::OnOK();
}

void CLDCCalibrationDlg::GetWindowPos(CRect &rc)
{
	rc = m_rcPos;
}

BOOL CLDCCalibrationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);

	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}

	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	LoadCalibrationData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLDCCalibrationDlg::LoadCalibrationData()
{
	m_strImagePath = VISION_CALIBRATION_SAMPLE_PATH;
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);

	LoadImage();
	LoadVIData();
}

void CLDCCalibrationDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NIPO *pNIPO = NIPO::GetInstance();

	GetDlgItem(IDC_IMAGE_PATH)->GetWindowText(m_strImagePath);

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImagePath), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImagePath));
		return;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImagePath));
	}

	m_dImg = dImg;

	ShowImage();
}

void CLDCCalibrationDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

void CLDCCalibrationDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
}

void CLDCCalibrationDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	m_strImageInfo = szText;
	UpdateData(FALSE);
}

void CLDCCalibrationDlg::LoadVIData()
{
	wstring strVIDataPath = VISION_CALIBRATION_DATA_PATH;

	if (!m_dVI.LoadCalibrationData(strVIDataPath)) {
		AddLogText(L"Fail to load Calibration Data : %s", strVIDataPath.c_str());
		return;
	}
	else {
		AddLogText(L"Success to load Calibration Data : %s", strVIDataPath.c_str());
	}

	m_nGuideLineLeftStartX = m_dVI.m_dCal.m_ptGuideLineLeftStart.x;
	m_nGuideLineLeftStartY = m_dVI.m_dCal.m_ptGuideLineLeftStart.y;
	m_nGuideLineLeftEndX = m_dVI.m_dCal.m_ptGuideLineLeftEnd.x;
	m_nGuideLineLeftEndY = m_dVI.m_dCal.m_ptGuideLineLeftEnd.y;
	m_nGuideLineRightEndX = m_dVI.m_dCal.m_ptGuideLineRightEnd.x;
	m_nGuideLineRightEndY = m_dVI.m_dCal.m_ptGuideLineRightEnd.y;
	m_nNormalizeWidth = m_dVI.m_dCal.m_nNormalizeWidth;

	UpdateData(FALSE);

	ShowDataBox();
}

void CLDCCalibrationDlg::SaveVIData()
{
	ApplyData();

	wstring strVIDataPath = VISION_CALIBRATION_DATA_PATH;

	if (!m_dVI.SaveCalibrationData(strVIDataPath)) {
		AddLogText(L"Fail to save Calibration Data : %s", strVIDataPath.c_str());
		return;
	}
	else {
		AddLogText(L"Success to save Calibration Data : %s", strVIDataPath.c_str());
	}
}

void CLDCCalibrationDlg::ShowDataBox()
{
	m_wndImg.ClearImageMark();

	int nRadius = 2;
	ImageMark dMark;
	dMark.m_bCircle = true;
	dMark.m_rcBoundingBox = Rect(m_nGuideLineLeftStartX - nRadius, m_nGuideLineLeftStartY - nRadius, 2 * nRadius + 1, 2 * nRadius + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.m_rcBoundingBox = Rect(m_nGuideLineLeftEndX - nRadius, m_nGuideLineLeftEndY - nRadius, 2 * nRadius + 1, 2 * nRadius + 1);
	m_wndImg.AddImageMark(dMark);

	dMark.m_rcBoundingBox = Rect(m_nGuideLineRightEndX - nRadius, m_nGuideLineRightEndY - nRadius, 2 * nRadius + 1, 2 * nRadius + 1);
	m_wndImg.AddImageMark(dMark);

	m_wndImg.ShowImageMark();
}

void CLDCCalibrationDlg::ApplyData()
{
	UpdateData();

	m_dVI.m_dCal.m_ptGuideLineLeftStart.x = m_nGuideLineLeftStartX;
	m_dVI.m_dCal.m_ptGuideLineLeftStart.y = m_nGuideLineLeftStartY;
	m_dVI.m_dCal.m_ptGuideLineLeftEnd.x = m_nGuideLineLeftEndX;
	m_dVI.m_dCal.m_ptGuideLineLeftEnd.y = m_nGuideLineLeftEndY;
	m_dVI.m_dCal.m_ptGuideLineRightEnd.x = m_nGuideLineRightEndX;
	m_dVI.m_dCal.m_ptGuideLineRightEnd.y = m_nGuideLineRightEndY;
	m_dVI.m_dCal.m_nNormalizeWidth = m_nNormalizeWidth;

	ShowDataBox();
}

void CLDCCalibrationDlg::OnBnClickedReloadData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadCalibrationData();
}


void CLDCCalibrationDlg::OnBnClickedSaveData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SaveVIData();
}

void CLDCCalibrationDlg::OnBnClickedDataApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyData();
}

LRESULT CLDCCalibrationDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));
	SetControlDataValue(CPoint(nPosX, nPosY));

	return 0;
}

void CLDCCalibrationDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	m_strImagePos = szText;
	UpdateData(FALSE);
}

void CLDCCalibrationDlg::SetControlDataValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	CWnd *pWnd = GetFocusControl();
	if (pWnd == nullptr) {
		return;
	}

	wchar_t szText[256];
	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_GUIDELINE_LEFT_START_POS_X:
	case IDC_GUIDELINE_LEFT_START_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_GUIDELINE_LEFT_START_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_GUIDELINE_LEFT_START_POS_Y)->SetWindowText(szText);
		break;
	case IDC_GUIDELINE_LEFT_END_POS_X:
	case IDC_GUIDELINE_LEFT_END_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_GUIDELINE_LEFT_END_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_GUIDELINE_LEFT_END_POS_Y)->SetWindowText(szText);
		break;
	case IDC_GUIDELINE_RIGHT_END_POS_X:
	case IDC_GUIDELINE_RIGHT_END_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_GUIDELINE_RIGHT_END_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_GUIDELINE_RIGHT_END_POS_Y)->SetWindowText(szText);
		break;
	}

	ApplyData();
}

CWnd *CLDCCalibrationDlg::GetFocusControl()
{
	CWnd *pWnd = GetFocus();
	if (pWnd == nullptr) {
		return nullptr;
	}

	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_GUIDELINE_LEFT_START_POS_X:
	case IDC_GUIDELINE_LEFT_START_POS_Y:
	case IDC_GUIDELINE_LEFT_END_POS_X:
	case IDC_GUIDELINE_LEFT_END_POS_Y:
	case IDC_GUIDELINE_RIGHT_END_POS_X:
	case IDC_GUIDELINE_RIGHT_END_POS_Y:
		return pWnd;
	}

	return nullptr;
}


void CLDCCalibrationDlg::OnBnClickedOpenImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"jpg", LPCTSTR(m_strImagePath), OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	m_strImagePath = iFileDlg.GetPathName();
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);

	LoadImage();
}


void CLDCCalibrationDlg::OnBnClickedLoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadImage();
}
