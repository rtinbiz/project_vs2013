#include "stdafx.h"
#include "VIData.h"

Rect VIData_ROI::GetBoundingBox()
{
	Rect rcBoundingBox;
	switch (m_nCompIndex) {
	case COMP_TERMINAL :
		rcBoundingBox.x = m_nCenterX - m_nRadius;
		rcBoundingBox.y = m_nCenterY - m_nRadius;
		rcBoundingBox.width = m_nRadius * 2 + 1;
		rcBoundingBox.height = m_nRadius * 2 + 1;
		break;
	default:
		rcBoundingBox.x = m_nStartX;
		rcBoundingBox.y = m_nStartY;
		rcBoundingBox.width = m_nEndX - m_nStartX;
		rcBoundingBox.height = m_nEndY - m_nStartY;
	}

	return rcBoundingBox;
}

Rect VIData_ROI::GetRefBoundingBox()
{
	Rect rcBoundingBox;
	int nMargin = 2;
	rcBoundingBox.x = m_nRefX - nMargin;
	rcBoundingBox.y = m_nRefY - nMargin;
	rcBoundingBox.width = nMargin * 2 + 1;
	rcBoundingBox.height = nMargin * 2 + 1;

	return rcBoundingBox;
}

CPoint VIData_ROI::GetBoundingBoxCenterPos()
{
	CPoint pt;

	switch (m_nCompIndex) {
	case COMP_TERMINAL:
		pt.x = m_nCenterX;
		pt.y = m_nCenterY;
		break;
	default:
		pt.x = cvRound((m_nStartX + m_nEndX) * 0.5f);
		pt.y = cvRound((m_nStartY + m_nEndY) * 0.5f);
	}

	return pt;
}


bool VIData::LoadData(wstring strDataPath)
{
	Clear();

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, STR_MODEL)) {
			wstring strMachine = str2wstr(pData->v("machine"));
			wstring strNo = str2wstr(pData->v("no"));

			m_dModel.m_strMachine = strMachine;
			m_dModel.m_strNo = strNo;
		}
		else if (CHECK_STRING(strElementName, STR_LDC_COMP_TERMINAL)) {
			int nCenterX = stoi(pData->v("centerx"));
			int nCenterY = stoi(pData->v("centery"));
			int nRadius = stoi(pData->v("radius"));

			VIData_ROI dROI;
			dROI.m_nCompIndex = COMP_TERMINAL;

			dROI.m_nCenterX = nCenterX;
			dROI.m_nCenterY = nCenterY;
			dROI.m_nRadius = nRadius;

			m_listTerminal.push_back(dROI);
		}
		else if (CHECK_STRING(strElementName, STR_LDC_COMP_TUBE)) {
			wstring strColor = str2wstr(pData->v("color"));
			int nStartX = stoi(pData->v("startx"));
			int nStartY = stoi(pData->v("starty"));
			int nEndX = stoi(pData->v("endx"));
			int nEndY = stoi(pData->v("endy"));
			int nSetId = 0;
			int nRefX = 0;
			try { nRefX = stoi(pData->v("refx")); }
			catch (...) {}
			int nRefY = 0;
			try { nRefY = stoi(pData->v("refy")); }
			catch (...) {}
			try { nSetId = stoi(pData->v("set")); }
			catch (...) {}

			VIData_ROI dROI;
			dROI.m_nCompIndex = COMP_TUBE;

			dROI.m_strColor = strColor;
			dROI.m_nStartX = nStartX;
			dROI.m_nStartY = nStartY;
			dROI.m_nEndX = nEndX;
			dROI.m_nEndY = nEndY;
			dROI.m_nRefX = nRefX;
			dROI.m_nRefY = nRefY;
			dROI.m_nSetId = nSetId;

			m_listTube.push_back(dROI);
		}
		else {
			int nStartX = stoi(pData->v("startx"));
			int nStartY = stoi(pData->v("starty"));
			int nEndX = stoi(pData->v("endx"));
			int nEndY = stoi(pData->v("endy"));
			int nRefX = 0;
			try { nRefX = stoi(pData->v("refx")); }
			catch (...) {}
			int nRefY = 0;
			try { nRefY = stoi(pData->v("refy")); }
			catch (...) {}
			int nSetId = 0;
			try { nSetId = stoi(pData->v("set")); }
			catch (...) {}

			VIData_ROI dROI;

			dROI.m_nStartX = nStartX;
			dROI.m_nStartY = nStartY;
			dROI.m_nEndX = nEndX;
			dROI.m_nEndY = nEndY;
			dROI.m_nRefX = nRefX;
			dROI.m_nRefY = nRefY;
			dROI.m_nSetId = nSetId;

			vector<VIData_ROI> *pList = 0x00;
			if (CHECK_STRING(strElementName, STR_LDC_COMP_GUIDE)) {
				pList = &m_listGuide;
				dROI.m_nCompIndex = COMP_GUIDE;
			}
			else if (CHECK_STRING(strElementName, STR_LDC_COMP_BOLT)) {
				pList = &m_listBolt;
				dROI.m_nCompIndex = COMP_BOLT;
			}
			else if (CHECK_STRING(strElementName, STR_LDC_COMP_CLIP)) {
				pList = &m_listClip;
				dROI.m_nCompIndex = COMP_CLIP;
			}
			else if (CHECK_STRING(strElementName, STR_LDC_COMP_PAPER)) {
				pList = &m_listPaper;
				dROI.m_nCompIndex = COMP_PAPER;
			}

			if (pList) {
				pList->push_back(dROI);
			}
		}
	}

	SortData(COMP_ALL);

	return true;
}

bool VIData::SaveData(wstring strDataPath)
{
	XML dXML;

	// Set Header
	XMLHeader dHeader;
	XMLVariable &version = dHeader.GetVersion();
	XMLVariable &encoding = dHeader.GetEncoding();
	XMLVariable &standalone = dHeader.GetStandalone();
	version.SetValue("1.0");
	encoding.SetValue("utf-8");
	standalone.SetValue("");
	dXML.SetHeader(dHeader);

	// Set Body
	XMLElement dRoot("VIData");
	dRoot.AddVariable("version", STR_VIDATA_VERSION);

	XMLElement dXMLModel(wstr2str(STR_MODEL).c_str());
	dXMLModel.AddVariable("machine", wstr2str(m_dModel.m_strMachine).c_str());
	dXMLModel.AddVariable("no", wstr2str(m_dModel.m_strNo).c_str());
	dRoot.AddElement(dXMLModel);

	_SetXMLData(STR_LDC_COMP_TERMINAL, m_listTerminal, dRoot);
	_SetXMLData(STR_LDC_COMP_TUBE, m_listTube, dRoot);
	_SetXMLData(STR_LDC_COMP_GUIDE, m_listGuide, dRoot);
	_SetXMLData(STR_LDC_COMP_BOLT, m_listBolt, dRoot);
	_SetXMLData(STR_LDC_COMP_PAPER, m_listPaper, dRoot);
	_SetXMLData(STR_LDC_COMP_CLIP, m_listClip, dRoot);

	dXML.SetRootElement(dRoot);

	auto nResult = dXML.Save(strDataPath.c_str());
	if (nResult != XML3::XML_ERROR::OK) {
		return false;
	}

	return true;
}

void VIData::_SetXMLData(wstring strElementName, vector<VIData_ROI> &listROI, XMLElement &dXMLParent)
{
	for (auto &dROI : listROI) {
		XMLElement dXMLData(wstr2str(strElementName).c_str());

		string strCenterX = to_string(dROI.m_nCenterX);
		string strCenterY = to_string(dROI.m_nCenterY);
		string strRadius = to_string(dROI.m_nRadius);
		string strColor = wstr2str(dROI.m_strColor);
		string strStartX = to_string(dROI.m_nStartX);
		string strStartY = to_string(dROI.m_nStartY);
		string strEndX = to_string(dROI.m_nEndX);
		string strEndY = to_string(dROI.m_nEndY);
		string strRefX = to_string(dROI.m_nRefX);
		string strRefY = to_string(dROI.m_nRefY);
		string strSetId = to_string(dROI.m_nSetId);
		bool bSetId = (dROI.m_nSetId > 0);

		if (CHECK_STRING(strElementName, STR_LDC_COMP_TERMINAL)) {
			dXMLData.AddVariable("centerx", strCenterX.c_str());
			dXMLData.AddVariable("centery", strCenterY.c_str());
			dXMLData.AddVariable("radius", strRadius.c_str());
		}
		else if (CHECK_STRING(strElementName, STR_LDC_COMP_TUBE)) {
			dXMLData.AddVariable("color", strColor.c_str());
			dXMLData.AddVariable("startx", strStartX.c_str());
			dXMLData.AddVariable("starty", strStartY.c_str());
			dXMLData.AddVariable("endx", strEndX.c_str());
			dXMLData.AddVariable("endy", strEndY.c_str());
			if (dROI.m_nRefX > 0 && dROI.m_nRefY > 0) {
				dXMLData.AddVariable("refx", strRefX.c_str());
				dXMLData.AddVariable("refy", strRefY.c_str());
			}
		}
		else {
			dXMLData.AddVariable("startx", strStartX.c_str());
			dXMLData.AddVariable("starty", strStartY.c_str());
			dXMLData.AddVariable("endx", strEndX.c_str());
			dXMLData.AddVariable("endy", strEndY.c_str());
			if (dROI.m_nRefX > 0 && dROI.m_nRefY > 0) {
				dXMLData.AddVariable("refx", strRefX.c_str());
				dXMLData.AddVariable("refy", strRefY.c_str());
			}
		}

		if (bSetId) {
			dXMLData.AddVariable("set", strSetId.c_str());
		}

		dXMLParent.AddElement(dXMLData);
	}
}

VIData_ROI VIData::AddData(int nCompIndex, int nImgWidth, int nImgHeight)
{
	VIData_ROI dROI;
	dROI.m_nCompIndex = nCompIndex;

	switch (nCompIndex) {
	case COMP_TERMINAL :
		dROI.m_nCenterX = nImgWidth / 2;
		dROI.m_nCenterY = nImgHeight / 2;
		dROI.m_nRadius = 25;
		break;
	case COMP_TUBE :
		dROI.m_strColor = L"black";
		// do not insert 'break;' here to go down to dafault:
	default :
		dROI.m_nStartX = nImgWidth / 2 - 15;
		dROI.m_nStartY = nImgHeight / 2 - 15;
		dROI.m_nEndX = nImgWidth / 2 + 15;
		dROI.m_nEndY = nImgHeight / 2 + 15;
		break;
	}

	vector<VIData_ROI> *pList = _GetDataList(nCompIndex);
	if (pList) {
		pList->push_back(dROI);
	}

	return dROI;
}

void VIData::DeleteData(VIData_ROI dTargetROI)
{
	vector<VIData_ROI> *pList = _GetDataList(dTargetROI.m_nCompIndex);
	if (pList == nullptr) {
		return;
	}

	auto it = find_if(pList->begin(), pList->end(), [dTargetROI](const VIData_ROI &dROI) -> bool
	{
		bool bFound = false;
		switch (dROI.m_nCompIndex) {
		case COMP_TERMINAL:
			if (dTargetROI.m_nCenterX == dROI.m_nCenterX
				&& dTargetROI.m_nCenterY == dROI.m_nCenterY
				&& dTargetROI.m_nRadius == dROI.m_nRadius) {
				bFound = true;
			}
			break;
		default:
			if (dTargetROI.m_nStartX == dROI.m_nStartX
				&& dTargetROI.m_nStartY == dROI.m_nStartY
				&& dTargetROI.m_nEndX == dROI.m_nEndX
				&& dTargetROI.m_nEndY == dROI.m_nEndY) {
				bFound = true;
			}
			break;
		}

		return bFound;
	});

	if (it != pList->end()) {
		pList->erase(it);
	}
}

vector<VIData_ROI> *VIData::_GetDataList(int nCompIndex)
{
	vector<VIData_ROI> *pList = nullptr;
	switch (nCompIndex) {
	case COMP_TERMINAL: pList = &m_listTerminal; break;
	case COMP_TUBE: pList = &m_listTube; break;
	case COMP_GUIDE: pList = &m_listGuide; break;
	case COMP_BOLT: pList = &m_listBolt; break;
	case COMP_PAPER: pList = &m_listPaper; break;
	case COMP_CLIP: pList = &m_listClip; break;
	}

	return pList;
}

void VIData::SortData(int nCompIndex)
{
	if (nCompIndex == COMP_ALL) {
		SortData(COMP_TERMINAL);
		SortData(COMP_TUBE);
		SortData(COMP_GUIDE);
		SortData(COMP_BOLT);
		SortData(COMP_PAPER);
		SortData(COMP_CLIP);
		return;
	}

	vector<VIData_ROI> *pList = _GetDataList(nCompIndex);
	if (pList == nullptr) {
		return;
	}

	sort(pList->begin(), pList->end());
}

bool VIData::LoadCalibrationData(wstring strDataPath)
{
	m_dCal.clear();

	XML dXML;
	auto nResult = dXML.Load(strDataPath.c_str());
	if (nResult != XML_PARSE::OK) {
		return false;
	}

	auto dRoot = dXML.GetRootElement();
	auto listData = dRoot.GetChildren();
	for (auto pData : listData) {
		wstring strElementName = str2wstr(pData->GetElementName());
		if (CHECK_STRING(strElementName, STR_GUIDELINE)) {
			wstring strPos = str2wstr(pData->v("pos"));

			if (CHECK_STRING(strPos, L"left")) {
				int nStartX = stoi(pData->v("startx"));
				int nStartY = stoi(pData->v("starty"));
				int nEndX = stoi(pData->v("endx"));
				int nEndY = stoi(pData->v("endy"));

				m_dCal.m_ptGuideLineLeftStart = Point(nStartX, nStartY);
				m_dCal.m_ptGuideLineLeftEnd = Point(nEndX, nEndY);
			}
			else {
				int nEndX = stoi(pData->v("endx"));
				int nEndY = stoi(pData->v("endy"));

				m_dCal.m_ptGuideLineRightEnd = Point(nEndX, nEndY);
			}
		}
		else if (CHECK_STRING(strElementName, STR_NORMALIZE)) {
			int nWidth = stoi(pData->v("width"));

			m_dCal.m_nNormalizeWidth = nWidth;
		}
	}

	return true;
}


bool VIData::SaveCalibrationData(wstring strDataPath)
{
	XML dXML;

	// Set Header
	XMLHeader dHeader;
	XMLVariable &version = dHeader.GetVersion();
	XMLVariable &encoding = dHeader.GetEncoding();
	XMLVariable &standalone = dHeader.GetStandalone();
	version.SetValue("1.0");
	encoding.SetValue("utf-8");
	standalone.SetValue("");
	dXML.SetHeader(dHeader);

	// Set Body
	XMLElement dRoot("VIData");
	dRoot.AddVariable("version", STR_VIDATA_VERSION);

	XMLElement dXMLData(wstr2str(STR_GUIDELINE).c_str());
	dXMLData.AddVariable("pos", "left");
	dXMLData.AddVariable("startx", to_string(m_dCal.m_ptGuideLineLeftStart.x).c_str());
	dXMLData.AddVariable("starty", to_string(m_dCal.m_ptGuideLineLeftStart.y).c_str());
	dXMLData.AddVariable("endx", to_string(m_dCal.m_ptGuideLineLeftEnd.x).c_str());
	dXMLData.AddVariable("endy", to_string(m_dCal.m_ptGuideLineLeftEnd.y).c_str());
	dRoot.AddElement(dXMLData);

	dXMLData.clear();
	dXMLData = XMLElement(wstr2str(STR_GUIDELINE).c_str());
	dXMLData.AddVariable("pos", "right");
	dXMLData.AddVariable("endx", to_string(m_dCal.m_ptGuideLineRightEnd.x).c_str());
	dXMLData.AddVariable("endy", to_string(m_dCal.m_ptGuideLineRightEnd.y).c_str());
	dRoot.AddElement(dXMLData);

	dXMLData.clear();
	dXMLData = XMLElement(wstr2str(STR_NORMALIZE).c_str());
	dXMLData.AddVariable("width", to_string(m_dCal.m_nNormalizeWidth).c_str());
	dRoot.AddElement(dXMLData);

	dXML.SetRootElement(dRoot);

	auto nResult = dXML.Save(strDataPath.c_str());
	if (nResult != XML3::XML_ERROR::OK) {
		return false;
	}

	return true;
}
