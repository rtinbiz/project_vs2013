
// LDCDataEditerDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "LDCCalibrationDlg.h"

#define VISION_FOLDER L"D:\\nBizSystem\\Vision\\"
#define VISION_MODEL_SAMPLE_FOLDER L"D:\\nBizSystem\\Vision\\ModelSample\\"

#define STR_PROCESS_LDC_CALIBRATION L"LDC Calibration"
#define STR_PROCESS_LDC_TERMINAL L"LDC Terminal"
#define STR_PROCESS_LDC_TUBE L"LDC Tube"
#define STR_PROCESS_LDC_GUIDE L"LDC Guide"
#define STR_PROCESS_LDC_BOLT L"LDC Bolt"
#define STR_PROCESS_LDC_PAPER L"LDC Paper"
#define STR_PROCESS_LDC_CLIP L"LDC Clip"


// CLDCDataEditerDlg dialog
class CLDCDataEditerDlg : public CDialogEx
{
// Construction
public:
	CLDCDataEditerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_LDCDATAEDITER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	NIPLImageCtl m_wndImg;
	Mat m_dImg;
	CString m_strModel;

	NIPJob m_dJob;
	VIData m_dVI;

	CString m_strImagePath;

public:
	void ShowImage();
	void LoadImage();
	void AddLogText(const wchar_t* szText, ...);
	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	LRESULT OnImagePosDblClick(WPARAM wParam, LPARAM lParam);
	void ShowImagePosValue(CPoint ptImagePos);
	void ShowImageInfo();
	void LoadModelList();
	void ChangeModel();
	bool LoadNIPJob();
	void DoImageCalibration();
	void DoVisionInspection(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFinalNG);
	bool DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bSkip, bool &bDefect);
	void LoadVIData();
	void SaveVIData();
	void ChangeCompData(bool bSelectAll);
	void ShowCompData(int nCompIndex);
	void SelectAllCompData(bool bSelectAll);
	int GetSingleSelectCompDataIndex();
	void UpdateCompData();
	void ShowCompDataBox();
	void SelectData(VIData_ROI dROI);
	void RefreshCompData(VIData_ROI dROI);
	void SelectDataByImagePos(CPoint ptImagePos);
	void SetControlDataValue(CPoint ptImagePos);
	CWnd *GetFocusControl();
	void ApplyCompData(bool bRefresh = true);
	vector<int> GetSelectCompDataIndex();
	void RefreshCompData(vector<int> listIndex);

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_ctlImageArea;
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnBnClickedOpenImage();
	afx_msg void OnBnClickedLoadImage();
	CListBox m_ctlLogList;
	CString m_strImagePos;
	CString m_strImageInfo;
	CListBox m_ctlModelList;
	afx_msg void OnSelchangeModelList();
	afx_msg void OnClickedCompRadio(UINT nID);
	CString m_strModelMachine;
	CString m_strModelNo;
	int m_nCompIndex;
	CListBox m_ctlCompDataList;
	afx_msg void OnBnClickedCompdataSelectAll();
	afx_msg void OnBnClickedCompdataSelectNone();
	afx_msg void OnSelchangeCompdataList();

	CEdit m_ctlDataCenterX;
	CEdit m_ctlDataCenterY;
	CEdit m_ctlDataRaidus;
	CEdit m_ctlDataColor;
	CEdit m_ctlDataStartX;
	CEdit m_ctlDataStartY;
	CEdit m_ctlDataEndX;
	CEdit m_ctlDataEndY;
	CEdit m_ctlDataRefX;
	CEdit m_ctlDataRefY;
	CEdit m_ctlDataSetId;
	int m_nDataCenterX;
	int m_nDataCenterY;
	int m_nDataRadius;
	CString m_strDataColor;
	int m_nDataStartX;
	int m_nDataStartY;
	int m_nDataEndX;
	int m_nDataEndY;
	int m_nDataRefX;
	int m_nDataRefY;
	int m_nDataSetId;
	afx_msg void OnBnClickedDataApply();
	CButton m_ctlDataApply;
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnBnClickedCompdataAdd();
	CButton m_ctlDataAdd;
	CButton m_ctlDataDelete;
	afx_msg void OnBnClickedCompdataDelete();
	afx_msg void OnBnClickedTestImage();
	afx_msg void OnBnClickedReloadData();
	afx_msg void OnBnClickedReloadModelData();
	afx_msg void OnBnClickedEditCalibrationData();
};
