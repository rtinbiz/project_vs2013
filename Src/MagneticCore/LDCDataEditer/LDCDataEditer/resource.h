//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// LDCDataEditer.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LDCDATAEDITER_DIALOG        102
#define IDD_CALIBRATION_DIALOG          103
#define IDR_MAINFRAME                   128
#define IDC_IMAGE_AREA                  1000
#define IDC_IMAGE_CTRL                  1001
#define IDC_OPEN_IMAGE                  1002
#define IDC_IMAG_PATH                   1003
#define IDC_IMAGE_PATH                  1003
#define IDC_LOAD_IMAGE                  1004
#define IDC_LOG_LIST                    1005
#define IDC_IMAGE_POS                   1006
#define IDC_IMAGE_INFO                  1007
#define IDC_MODEL_LIST                  1008
#define IDC_COMP_ALL                    1009
#define IDC_COMP_TERMINAL               1010
#define IDC_COMP_TUBE                   1011
#define IDC_COMP_GUIDE                  1012
#define IDC_COMP_BOLT                   1013
#define IDC_COMP_PAPER                  1014
#define IDC_COMP_CLIP                   1015
#define IDC_COMPDATA_LIST               1016
#define IDC_MODEL_MACHINE               1017
#define IDC_MODEL_NO                    1018
#define IDC_DATA_TYPE                   1019
#define IDC_DATA_COLOR                  1020
#define IDC_DATA_CENTER_POS_X           1021
#define IDC_DATA_CENTER_POS_Y           1022
#define IDC_DATA_RADIUS                 1023
#define IDC_COMPDATA_SELECT_ALL         1024
#define IDC_COMPDATA_SELECT_NONE        1025
#define IDC_DATA_EXT_LEFT               1026
#define IDC_DATA_EXT_TOP                1027
#define IDC_DATA_EXT_RIGHT              1028
#define IDC_DATA_EXT_BOTTOM             1029
#define IDC_DATA_START_POS_X            1030
#define IDC_DATA_START_POS_Y            1031
#define IDC_DATA_END_POS_X              1032
#define IDC_DATA_END_POS_Y              1033
#define IDC_DATA_EXT_TOP2               1034
#define IDC_DATA_SET_ID                 1034
#define IDC_DATA_APPLY                  1035
#define IDC_COMPDATA_ADD                1036
#define IDC_GUIDELINE_RIGHT_END_POS_X   1036
#define IDC_COMPDATA_DELETE             1037
#define IDC_GUIDELINE_RIGHT_END_POS_Y   1037
#define IDC_SAVE_DATA                   1038
#define IDC_TEST_IMAGE                  1039
#define IDC_DATA_APPLY2                 1040
#define IDC_DATA_REF_POS_X              1040
#define IDC_DATA_APPLY3                 1041
#define IDC_DATA_REF_POS_Y              1041
#define IDC_RELOAD_DATA                 1042
#define IDC_RELOAD_MODEL_DATA           1043
#define IDC_GUIDELINE_LEFT_START_POS_X  1044
#define IDC_GUIDELINE_LEFT_START_POS_Y  1045
#define IDC_GUIDELINE_LEFT_END_POS_X    1046
#define IDC_GUIDELINE_LEFT_END_POS_Y    1047
#define IDC_NORMALIZE_WIDTH             1048
#define IDC_EDIT_CALIBRATION_DATA       1049
#define IDC_BUTTON1                     1050

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
