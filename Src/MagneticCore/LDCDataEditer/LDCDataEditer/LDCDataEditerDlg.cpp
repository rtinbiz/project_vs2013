
// LDCDataEditerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LDCDataEditer.h"
#include "LDCDataEditerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
//	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLDCDataEditerDlg dialog



CLDCDataEditerDlg::CLDCDataEditerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLDCDataEditerDlg::IDD, pParent)
	, m_strImagePos(_T(""))
	, m_strImageInfo(_T(""))
	, m_strModelMachine(_T(""))
	, m_strModelNo(_T(""))
	, m_nCompIndex(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_strImagePath = "";

	m_nDataCenterX = 0;
	m_nDataCenterY = 0;
	m_nDataRadius = 0;
	m_strDataColor = "";
	m_nDataStartX = 0;
	m_nDataStartY = 0;
	m_nDataEndX = 0;
	m_nDataEndY = 0;
	m_nDataRefX = 0;
	m_nDataRefY = 0;
	m_nDataSetId = 0;
}

void CLDCDataEditerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Text(pDX, IDC_IMAGE_POS, m_strImagePos);
	DDX_Text(pDX, IDC_IMAGE_INFO, m_strImageInfo);
	DDX_Control(pDX, IDC_MODEL_LIST, m_ctlModelList);
	DDX_Text(pDX, IDC_MODEL_MACHINE, m_strModelMachine);
	DDX_Text(pDX, IDC_MODEL_NO, m_strModelNo);
	DDX_Radio(pDX, IDC_COMP_ALL, m_nCompIndex);
	DDX_Control(pDX, IDC_COMPDATA_LIST, m_ctlCompDataList);
	DDX_Control(pDX, IDC_DATA_CENTER_POS_X, m_ctlDataCenterX);
	DDX_Control(pDX, IDC_DATA_CENTER_POS_Y, m_ctlDataCenterY);
	DDX_Control(pDX, IDC_DATA_RADIUS, m_ctlDataRaidus);
	DDX_Control(pDX, IDC_DATA_COLOR, m_ctlDataColor);
	DDX_Control(pDX, IDC_DATA_START_POS_X, m_ctlDataStartX);
	DDX_Control(pDX, IDC_DATA_START_POS_Y, m_ctlDataStartY);
	DDX_Control(pDX, IDC_DATA_END_POS_X, m_ctlDataEndX);
	DDX_Control(pDX, IDC_DATA_END_POS_Y, m_ctlDataEndY);
	DDX_Control(pDX, IDC_DATA_REF_POS_X, m_ctlDataRefX);
	DDX_Control(pDX, IDC_DATA_REF_POS_Y, m_ctlDataRefY);
	DDX_Control(pDX, IDC_DATA_SET_ID, m_ctlDataSetId);
	DDX_Text(pDX, IDC_DATA_CENTER_POS_X, m_nDataCenterX);
	DDX_Text(pDX, IDC_DATA_CENTER_POS_Y, m_nDataCenterY);
	DDX_Text(pDX, IDC_DATA_RADIUS, m_nDataRadius);
	DDX_Text(pDX, IDC_DATA_COLOR, m_strDataColor);
	DDX_Text(pDX, IDC_DATA_START_POS_X, m_nDataStartX);
	DDX_Text(pDX, IDC_DATA_START_POS_Y, m_nDataStartY);
	DDX_Text(pDX, IDC_DATA_END_POS_X, m_nDataEndX);
	DDX_Text(pDX, IDC_DATA_END_POS_Y, m_nDataEndY);
	DDX_Text(pDX, IDC_DATA_REF_POS_X, m_nDataRefX);
	DDX_Text(pDX, IDC_DATA_REF_POS_Y, m_nDataRefY);
	DDX_Text(pDX, IDC_DATA_SET_ID, m_nDataSetId);
	DDX_Control(pDX, IDC_DATA_APPLY, m_ctlDataApply);
	DDX_Control(pDX, IDC_COMPDATA_ADD, m_ctlDataAdd);
	DDX_Control(pDX, IDC_COMPDATA_DELETE, m_ctlDataDelete);
}

BEGIN_MESSAGE_MAP(CLDCDataEditerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(IDC_OPEN_IMAGE, &CLDCDataEditerDlg::OnBnClickedOpenImage)
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CLDCDataEditerDlg::OnBnClickedLoadImage)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CLDCDataEditerDlg::OnImagePosValue)
	ON_MESSAGE(WM_NIPL_IMAGE_POS_DBLCLICK, &CLDCDataEditerDlg::OnImagePosDblClick)

	ON_LBN_SELCHANGE(IDC_MODEL_LIST, &CLDCDataEditerDlg::OnSelchangeModelList)
	ON_COMMAND_RANGE(IDC_COMP_ALL, IDC_COMP_CLIP, OnClickedCompRadio)
	ON_BN_CLICKED(IDC_COMPDATA_SELECT_ALL, &CLDCDataEditerDlg::OnBnClickedCompdataSelectAll)
	ON_BN_CLICKED(IDC_COMPDATA_SELECT_NONE, &CLDCDataEditerDlg::OnBnClickedCompdataSelectNone)
	ON_LBN_SELCHANGE(IDC_COMPDATA_LIST, &CLDCDataEditerDlg::OnSelchangeCompdataList)
	ON_BN_CLICKED(IDC_DATA_APPLY, &CLDCDataEditerDlg::OnBnClickedDataApply)
	ON_BN_CLICKED(IDC_SAVE_DATA, &CLDCDataEditerDlg::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_COMPDATA_ADD, &CLDCDataEditerDlg::OnBnClickedCompdataAdd)
	ON_BN_CLICKED(IDC_COMPDATA_DELETE, &CLDCDataEditerDlg::OnBnClickedCompdataDelete)
	ON_BN_CLICKED(IDC_TEST_IMAGE, &CLDCDataEditerDlg::OnBnClickedTestImage)
	ON_BN_CLICKED(IDC_RELOAD_DATA, &CLDCDataEditerDlg::OnBnClickedReloadData)
	ON_BN_CLICKED(IDC_RELOAD_MODEL_DATA, &CLDCDataEditerDlg::OnBnClickedReloadModelData)
	ON_BN_CLICKED(IDC_EDIT_CALIBRATION_DATA, &CLDCDataEditerDlg::OnBnClickedEditCalibrationData)
END_MESSAGE_MAP()


// CLDCDataEditerDlg message handlers

BOOL CLDCDataEditerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);

	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}

	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	LoadModelList();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLDCDataEditerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLDCDataEditerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLDCDataEditerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CLDCDataEditerDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialogEx::OnSetFocus(pOldWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	m_wndImg.SetFocus();
}

void CLDCDataEditerDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
}

void CLDCDataEditerDlg::OnBnClickedOpenImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";

	CFileDialog iFileDlg(TRUE, L"jpg", LPCTSTR(m_strImagePath), OFN_READONLY, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	m_strImagePath = iFileDlg.GetPathName();
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);

	LoadImage();
}

void CLDCDataEditerDlg::OnBnClickedLoadImage()
{
	LoadImage();
}

void CLDCDataEditerDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	NIPO *pNIPO = NIPO::GetInstance();

	GetDlgItem(IDC_IMAGE_PATH)->GetWindowText(m_strImagePath);

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImagePath), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImagePath));
		return;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImagePath));
	}

	m_dImg = dImg;

	DoImageCalibration();
	ShowImage();
}

void CLDCDataEditerDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

LRESULT CLDCDataEditerDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));
	SetControlDataValue(CPoint(nPosX, nPosY));

	return 0;
}

void CLDCDataEditerDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	m_strImagePos = szText;
	UpdateData(FALSE);
}

void CLDCDataEditerDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	m_strImageInfo = szText;
	UpdateData(FALSE);
}

void CLDCDataEditerDlg::LoadModelList()
{
	m_ctlModelList.ResetContent();

	WIN32_FIND_DATA dFindData;
	wstring strFormat(VISION_FOLDER);
	strFormat += L"\\*.vid";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	int nFileCount = 0;
	if (hFile != INVALID_HANDLE_VALUE) {
		bool bFirstFile = true;
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			wchar_t *pCut = wcsrchr(dFindData.cFileName, '.');
			if (pCut) {
				*pCut = 0X00;
			}

			if (CHECK_STRING(dFindData.cFileName, L"Calibration")) {
				continue;
			}

			int nIndex = m_ctlModelList.GetCount();
			nIndex = m_ctlModelList.InsertString(nIndex, dFindData.cFileName);
			m_ctlModelList.SetCurSel(nIndex);
		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);
	}

	int nCount = m_ctlModelList.GetCount();
	if (nCount > 0) {
		AddLogText(L"Success to load models, Count : %d", nCount);
		m_ctlModelList.SetCurSel(0);
		ChangeModel();
	}
	else {
		AddLogText(L"Fail to load models, Count : %d", nCount);
	}
}


void CLDCDataEditerDlg::OnSelchangeModelList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeModel();
}

void CLDCDataEditerDlg::ChangeModel()
{
	int nIndex = m_ctlModelList.GetCurSel();
	if (nIndex < 0) {
		AddLogText(L"Fail to change model. No model selected.");
		return;
	}

	m_ctlModelList.GetText(nIndex, m_strModel);

	m_strImagePath = VISION_MODEL_SAMPLE_FOLDER;
	m_strImagePath += m_strModel;
	m_strImagePath += ".jpg";
	GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);

	LoadImage();
	LoadVIData();
}

bool CLDCDataEditerDlg::LoadNIPJob()
{
	wstring strJobFile = VISION_FOLDER;
	strJobFile += L"VI_LDC.nip";

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {
		return false;
	}

	AddLogText(L"NIPJob Data File : %s", strJobFile.c_str());

	return true;
}


void CLDCDataEditerDlg::DoImageCalibration()
{
	if (!LoadNIPJob()) {
		AddLogText(L"Cannot Load NIPJob Data File");
		return;
	}

	Mat dCalibrationImg;
	bool bFinalNG = false;
	DoVisionInspection(STR_PROCESS_LDC_CALIBRATION, VISION_CALIBRATION_DATA_PATH, m_dImg, dCalibrationImg, bFinalNG);

	if (!bFinalNG) {
		m_dImg = dCalibrationImg;
	}
}

void CLDCDataEditerDlg::DoVisionInspection(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFinalNG) 
{
	bool bDefect = false;
	bool bSkip = false;
	bool bSuccess = DoVisionInspectionProcess(strProcessName, strVIData, dImg, dOutImg, bSkip, bDefect);

	if (!bSuccess || bDefect) {
		bFinalNG = true;
	}
}

bool CLDCDataEditerDlg::DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bSkip, bool &bDefect)
{
	AddLogText(L"Process : %s", strProcessName.c_str());

	bSkip = false;

	bDefect = false;
	NIPO *pNIPO = NIPO::GetInstance();
	NIPJobProcess *pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam *pParam = pNIPO->SetNIPLParam(pProcess);
		if (pParam == nullptr) {
			AddLogText(L"  ! [%s] Cannot Set Parameter.", strProcessName.c_str());
			return false;
		}

		if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CALIBRATION)) ((NIPLParam_LDC_Calibration *)pParam)->m_strDataPath = strVIData;
		else ((NIPLParam_LDC *)pParam)->m_strDataPath = strVIData;

		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);

		delete pParam;

		if (NIPL_SUCCESS(nErr)) {
			if (CHECK_STRING(strProcessName, STR_PROCESS_LDC_CALIBRATION)) {
				dOutImg = dOutput.m_dImg;
			}
			else {
				if (dOutput.m_pResult != nullptr) {
					bDefect = true;
					if (CHECK_EMPTY_IMAGE(dOutImg)) {
						dImg.copyTo(dOutImg);
					}

					NIPLResult_Defect_LDC *pResult = (NIPLResult_Defect_LDC *)dOutput.m_pResult;
					for (auto &dDefect : pResult->m_listDefect) {
						rectangle(dOutImg, dDefect.m_rcBoundingBox, CV_RGB(255, 0, 0), 3);
					}
				}
			}
		}
		else if (NIPL_PASS(nErr)) {
			bSkip = true;
			AddLogText(L"    => Skip.", strProcessName.c_str());
		}
		else {
			AddLogText(L"  ! [%s] Cannot Perform Inspection. Error Code : %d.", strProcessName.c_str(), nErr);
			return false;
		}
	}
	else {
		bSkip = true;
		AddLogText(L"    => Skip.", strProcessName.c_str());
	}

	return true;
}


void CLDCDataEditerDlg::OnClickedCompRadio(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeCompData(true);
}

void CLDCDataEditerDlg::LoadVIData()
{
	wstring strVIDataPath = VISION_FOLDER;
	strVIDataPath += LPCTSTR(m_strModel);
	strVIDataPath += L".vid";

	if (!m_dVI.LoadData(strVIDataPath)) {
		AddLogText(L"Fail to load VI Data : %s", strVIDataPath.c_str());
		return;
	}
	else {
		AddLogText(L"Success to load VI Data : %s", strVIDataPath.c_str());
	}

	m_strModelMachine = m_dVI.m_dModel.m_strMachine.c_str();
	m_strModelNo = m_dVI.m_dModel.m_strNo.c_str();

	m_nCompIndex = COMP_ALL;
	UpdateData(FALSE);

	ChangeCompData(true);
}

void CLDCDataEditerDlg::ChangeCompData(bool bSelectAll)
{
	UpdateData();

	m_ctlCompDataList.ResetContent();

	m_ctlDataAdd.EnableWindow(true);

	switch (m_nCompIndex) {
	case COMP_ALL: 
		ShowCompData(COMP_TERMINAL);
		ShowCompData(COMP_TUBE);
		ShowCompData(COMP_GUIDE);
		ShowCompData(COMP_BOLT);
		ShowCompData(COMP_PAPER);
		ShowCompData(COMP_CLIP);

		m_ctlDataAdd.EnableWindow(false);
		break;
	case COMP_TERMINAL:
		ShowCompData(COMP_TERMINAL);
		break;
	case COMP_TUBE:
		ShowCompData(COMP_TUBE);
		break;
	case COMP_GUIDE:
		ShowCompData(COMP_GUIDE);
		break;
	case COMP_BOLT:
		ShowCompData(COMP_BOLT);
		break;
	case COMP_PAPER:
		ShowCompData(COMP_PAPER);
		break;
	case COMP_CLIP:
		ShowCompData(COMP_CLIP);
		break;
	}

	SelectAllCompData(bSelectAll);
}

void CLDCDataEditerDlg::ShowCompData(int nCompIndex)
{
	switch (nCompIndex) {
	case COMP_TERMINAL:
	{
		vector<VIData_ROI> &list = m_dVI.m_listTerminal;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Terminal] Center(%d, %d) Radius(%d)",
				dData.m_nCenterX, dData.m_nCenterY, dData.m_nRadius);

			if (dData.m_nSetId > 0) {
				swprintf_s(szText, L"%s SetId(%d)", szText, dData.m_nSetId);
			}

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	case COMP_TUBE:
	{
		vector<VIData_ROI> &list = m_dVI.m_listTube;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Tube] Color(%s) Start(%d, %d) End(%d, %d) Ref(%d, %d)",
				dData.m_strColor.c_str(), dData.m_nStartX, dData.m_nStartY, 
				dData.m_nEndX, dData.m_nEndY, dData.m_nRefX, dData.m_nRefY);

			if (dData.m_nSetId > 0) {
				swprintf_s(szText, L"%s SetId(%d)", szText, dData.m_nSetId);
			}

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	case COMP_GUIDE:
	{
		vector<VIData_ROI> &list = m_dVI.m_listGuide;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Guide] Start(%d, %d) End(%d, %d)",
				dData.m_nStartX, dData.m_nStartY, dData.m_nEndX, dData.m_nEndY);

			if (dData.m_nSetId > 0) {
				swprintf_s(szText, L"%s SetId(%d)", szText, dData.m_nSetId);
			}

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	case COMP_BOLT :
	{
		vector<VIData_ROI> &list = m_dVI.m_listBolt;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Bolt] Start(%d, %d) End(%d, %d)", 
				dData.m_nStartX, dData.m_nStartY, dData.m_nEndX, dData.m_nEndY);

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	case COMP_PAPER:
	{
		vector<VIData_ROI> &list = m_dVI.m_listPaper;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Paper] Start(%d, %d) End(%d, %d) Ref(%d, %d)",
				dData.m_nStartX, dData.m_nStartY, dData.m_nEndX, dData.m_nEndY,
				dData.m_nRefX, dData.m_nRefY);

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	case COMP_CLIP:
	{
		vector<VIData_ROI> &list = m_dVI.m_listClip;
		for (auto &dData : list) {
			wchar_t szText[256];
			swprintf_s(szText, L"[Clip] Start(%d, %d) End(%d, %d)",
				dData.m_nStartX, dData.m_nStartY, dData.m_nEndX, dData.m_nEndY);

			int nIndex = m_ctlCompDataList.GetCount();
			nIndex = m_ctlCompDataList.InsertString(nIndex, szText);
			m_ctlCompDataList.SetItemData(nIndex, (DWORD_PTR)&dData);
		}
	}
	break;
	}
}

void CLDCDataEditerDlg::OnBnClickedCompdataSelectAll()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SelectAllCompData(true);
}


void CLDCDataEditerDlg::OnBnClickedCompdataSelectNone()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SelectAllCompData(false);
}

void CLDCDataEditerDlg::SelectAllCompData(bool bSelectAll)
{
	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < nCount; i++) {
		m_ctlCompDataList.SetSel(i, bSelectAll);
	}

	ShowCompDataBox();
	UpdateCompData();
}

int CLDCDataEditerDlg::GetSingleSelectCompDataIndex()
{
	int nIndex = -1;
	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < m_ctlCompDataList.GetCount(); i++) {
		if (m_ctlCompDataList.GetSel(i) > 0) {
			if (nIndex == -1) {
				nIndex = i;
			}
			else {
				// not single item selected
				return -1;
			}
		}
	}

	return nIndex;
}

vector<int> CLDCDataEditerDlg::GetSelectCompDataIndex()
{
	vector<int> listSelected;
	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < m_ctlCompDataList.GetCount(); i++) {
		if (m_ctlCompDataList.GetSel(i) > 0) {
			listSelected.push_back(i);
		}
	}

	return listSelected;
}


void CLDCDataEditerDlg::OnSelchangeCompdataList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShowCompDataBox();
	UpdateCompData();
}

void CLDCDataEditerDlg::UpdateCompData()
{
	m_ctlDataCenterX.EnableWindow(false);
	m_ctlDataCenterY.EnableWindow(false);
	m_ctlDataRaidus.EnableWindow(false);
	m_ctlDataColor.EnableWindow(false);
	m_ctlDataStartX.EnableWindow(false);
	m_ctlDataStartY.EnableWindow(false);
	m_ctlDataEndX.EnableWindow(false);
	m_ctlDataEndY.EnableWindow(false);
	m_ctlDataRefX.EnableWindow(false);
	m_ctlDataRefY.EnableWindow(false);
	m_ctlDataSetId.EnableWindow(false);

	m_ctlDataApply.EnableWindow(false);
	m_ctlDataDelete.EnableWindow(false);

	vector<int> listIndex = GetSelectCompDataIndex();
	if (listIndex.size() == 0) {
		return;
	}
	else if (listIndex.size() == 1) {
		int nIndex = listIndex[0];;

		VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(nIndex);
		m_nDataCenterX = pROI->m_nCenterX;
		m_nDataCenterY = pROI->m_nCenterY;
		m_nDataRadius = pROI->m_nRadius;
		m_strDataColor = pROI->m_strColor.c_str();
		m_nDataStartX = pROI->m_nStartX;
		m_nDataStartY = pROI->m_nStartY;
		m_nDataEndX = pROI->m_nEndX;
		m_nDataEndY = pROI->m_nEndY;
		m_nDataRefX = pROI->m_nRefX;
		m_nDataRefY = pROI->m_nRefY;
		m_nDataSetId = pROI->m_nSetId;

		switch (pROI->m_nCompIndex) {
		case COMP_TERMINAL:
			m_ctlDataCenterX.EnableWindow(true);
			m_ctlDataCenterY.EnableWindow(true);
			m_ctlDataRaidus.EnableWindow(true);
			break;
		case COMP_TUBE:
			m_ctlDataColor.EnableWindow(true);
			m_ctlDataStartX.EnableWindow(true);
			m_ctlDataStartY.EnableWindow(true);
			m_ctlDataEndX.EnableWindow(true);
			m_ctlDataEndY.EnableWindow(true);
			m_ctlDataRefX.EnableWindow(true);
			m_ctlDataRefY.EnableWindow(true);
			m_ctlDataSetId.EnableWindow(true);
			break;
		case COMP_PAPER:
			m_ctlDataStartX.EnableWindow(true);
			m_ctlDataStartY.EnableWindow(true);
			m_ctlDataEndX.EnableWindow(true);
			m_ctlDataEndY.EnableWindow(true);
			m_ctlDataRefX.EnableWindow(true);
			m_ctlDataRefY.EnableWindow(true);
			m_ctlDataSetId.EnableWindow(true);
			break;
		default:
			m_ctlDataStartX.EnableWindow(true);
			m_ctlDataStartY.EnableWindow(true);
			m_ctlDataEndX.EnableWindow(true);
			m_ctlDataEndY.EnableWindow(true);
			m_ctlDataSetId.EnableWindow(true);
			break;
		}

		m_wndImg.GotoImagePos(pROI->GetBoundingBoxCenterPos());
	}
	else {
		int nIndex = listIndex[0];;

		VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(nIndex);
		m_nDataCenterX = pROI->m_nCenterX;
		m_nDataCenterY = pROI->m_nCenterY;
		m_nDataRadius = pROI->m_nRadius;
		m_strDataColor = pROI->m_strColor.c_str();
		m_nDataStartX = pROI->m_nStartX;
		m_nDataStartY = pROI->m_nStartY;
		m_nDataEndX = pROI->m_nEndX;
		m_nDataEndY = pROI->m_nEndY;
		m_nDataRefX = pROI->m_nRefX;
		m_nDataRefY = pROI->m_nRefY;
		m_nDataSetId = pROI->m_nSetId;

		switch (pROI->m_nCompIndex) {
		case COMP_TERMINAL:
			m_ctlDataRaidus.EnableWindow(true);
			break;
		case COMP_TUBE:
		case COMP_PAPER:
			m_ctlDataRefX.EnableWindow(true);
			m_ctlDataRefY.EnableWindow(true);
			break;
		}

		m_wndImg.GotoImagePos(pROI->GetBoundingBoxCenterPos());
	}

	m_ctlDataApply.EnableWindow(true);
	m_ctlDataDelete.EnableWindow(true);

	UpdateData(FALSE);
}

void CLDCDataEditerDlg::ShowCompDataBox()
{
	m_wndImg.ClearImageMark();

	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < m_ctlCompDataList.GetCount(); i++) {
		if (m_ctlCompDataList.GetSel(i) > 0) {
			VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(i);

			ImageMark dMark;
			if (pROI->m_nCompIndex == COMP_TERMINAL) dMark.m_bCircle = true;
			dMark.m_rcBoundingBox = pROI->GetBoundingBox();
			m_wndImg.AddImageMark(dMark);

			if ((pROI->m_nCompIndex == COMP_TUBE || pROI->m_nCompIndex == COMP_PAPER) && pROI->m_nRefX > 0 && pROI->m_nRefY > 0) {
				dMark.Clear();
				dMark.m_bCircle = true;
				dMark.m_rcBoundingBox = pROI->GetRefBoundingBox();
				m_wndImg.AddImageMark(dMark);
			}
		}
	}

	m_wndImg.ShowImageMark();
}

void CLDCDataEditerDlg::OnBnClickedDataApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ApplyCompData();
}

void CLDCDataEditerDlg::ApplyCompData(bool bRefresh)
{
	vector<int> listIndex = GetSelectCompDataIndex();
	if (listIndex.size() == 0) {
		return;
	}
	else if (listIndex.size() == 1) {
		UpdateData();

		int nIndex = listIndex[0];

		VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(nIndex);
		pROI->m_nCenterX = m_nDataCenterX;
		pROI->m_nCenterY = m_nDataCenterY;
		pROI->m_nRadius = m_nDataRadius;
		pROI->m_strColor = m_strDataColor;
		pROI->m_nStartX = m_nDataStartX;
		pROI->m_nStartY = m_nDataStartY;
		pROI->m_nEndX = m_nDataEndX;
		pROI->m_nEndY = m_nDataEndY;
		pROI->m_nRefX = m_nDataRefX;
		pROI->m_nRefY = m_nDataRefY;
		pROI->m_nSetId = m_nDataSetId;

		if (bRefresh) {
			RefreshCompData(*pROI);
		}
		else {
			ShowCompDataBox();
		}
	}
	else {
		UpdateData();

		for (int nIndex : listIndex) {
			VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(nIndex);
			switch (pROI->m_nCompIndex) {
			case COMP_TERMINAL:
				pROI->m_nRadius = m_nDataRadius;
				break;
			case COMP_TUBE:
			case COMP_PAPER:
				pROI->m_nRefX = m_nDataRefX;
				pROI->m_nRefY = m_nDataRefY;
				break;
			}
		}

		if (bRefresh) {
			RefreshCompData(listIndex);
		}
		else {
			ShowCompDataBox();
		}
	}
}

void CLDCDataEditerDlg::RefreshCompData(VIData_ROI dROI)
{
	m_dVI.SortData(dROI.m_nCompIndex);

	m_ctlCompDataList.ResetContent();
	ChangeCompData(false);
	SelectData(dROI);

	ShowCompDataBox();
	UpdateCompData();
}

void CLDCDataEditerDlg::RefreshCompData(vector<int> listIndex)
{
	m_ctlCompDataList.ResetContent();
	ChangeCompData(false);

	for (int i : listIndex) {
		m_ctlCompDataList.SetSel(i, true);
	}

	ShowCompDataBox();
	UpdateCompData();
}


void CLDCDataEditerDlg::OnBnClickedSaveData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	SaveVIData();
}

void CLDCDataEditerDlg::SaveVIData()
{
	UpdateData();

	m_dVI.m_dModel.m_strMachine = m_strModelMachine;
	m_dVI.m_dModel.m_strNo = m_strModelNo;

	wstring strVIDataPath = VISION_FOLDER;
	strVIDataPath += LPCTSTR(m_strModel);
	strVIDataPath += L".vid";

	if (!m_dVI.SaveData(strVIDataPath)) {
		AddLogText(L"Fail to save VI Data : %s", strVIDataPath.c_str());
		return;
	}
	else {
		AddLogText(L"Success to save VI Data : %s", strVIDataPath.c_str());
	}
}

void CLDCDataEditerDlg::OnBnClickedCompdataAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
	if (m_nCompIndex == COMP_ALL) {
		return;
	}

	VIData_ROI dROI = m_dVI.AddData(m_nCompIndex, m_dImg.cols, m_dImg.rows);
	RefreshCompData(dROI);
}

void CLDCDataEditerDlg::SelectData(VIData_ROI dROI)
{
	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < nCount; i++) {
		VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(i);
		bool bFound = false;

		if (pROI->m_nCompIndex == dROI.m_nCompIndex) {
			switch (pROI->m_nCompIndex) {
			case COMP_TERMINAL :
				if (pROI->m_nCenterX == dROI.m_nCenterX
					&& pROI->m_nCenterY == dROI.m_nCenterY
					&& pROI->m_nRadius == dROI.m_nRadius) {
					bFound = true;
				}
				break;
			default:
				if (pROI->m_nStartX == dROI.m_nStartX
					&& pROI->m_nStartY == dROI.m_nStartY
					&& pROI->m_nEndX == dROI.m_nEndX
					&& pROI->m_nEndY == dROI.m_nEndY) {
					bFound = true;
				}
				break;
			}
		}

		m_ctlCompDataList.SetSel(i, bFound);
	}
}

void CLDCDataEditerDlg::OnBnClickedCompdataDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int nIndex = GetSingleSelectCompDataIndex();
	if (nIndex < 0) {
		return;
	}

	VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(nIndex);

	m_dVI.DeleteData(*pROI);

	ChangeCompData(false);
}


void CLDCDataEditerDlg::OnBnClickedTestImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!LoadNIPJob()) {
		AddLogText(L"Cannot Load NIPJob Data File");
		return;
	}

	UpdateData();

	LoadImage();
	SaveVIData();

	SelectAllCompData(false);

	wstring strVIDataPath = VISION_FOLDER;
	strVIDataPath += LPCTSTR(m_strModel);
	strVIDataPath += L".vid";

	bool bFinalNG = false;
	Mat dOutImg;


	switch (m_nCompIndex) {
	case COMP_ALL:
		DoVisionInspection(STR_PROCESS_LDC_TERMINAL, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		DoVisionInspection(STR_PROCESS_LDC_TUBE, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		DoVisionInspection(STR_PROCESS_LDC_GUIDE, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		DoVisionInspection(STR_PROCESS_LDC_BOLT, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		DoVisionInspection(STR_PROCESS_LDC_PAPER, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		DoVisionInspection(STR_PROCESS_LDC_CLIP, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_TERMINAL:
		DoVisionInspection(STR_PROCESS_LDC_TERMINAL, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_TUBE:
		DoVisionInspection(STR_PROCESS_LDC_TUBE, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_GUIDE:
		DoVisionInspection(STR_PROCESS_LDC_GUIDE, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_BOLT:
		DoVisionInspection(STR_PROCESS_LDC_BOLT, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_PAPER:
		DoVisionInspection(STR_PROCESS_LDC_PAPER, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	case COMP_CLIP:
		DoVisionInspection(STR_PROCESS_LDC_CLIP, strVIDataPath, m_dImg, dOutImg, bFinalNG);
		break;
	}

	if (bFinalNG) {
		AddLogText(L"Test Result : NG");
		m_dImg = dOutImg;
		ShowImage();
	}
	else {
		AddLogText(L"Test Result : OK");
	}
}

LRESULT CLDCDataEditerDlg::OnImagePosDblClick(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	SelectDataByImagePos(CPoint(nPosX, nPosY));

	return 0;
}

void CLDCDataEditerDlg::SelectDataByImagePos(CPoint pt)
{
	int nCount = m_ctlCompDataList.GetCount();
	for (int i = 0; i < nCount; i++) {
		VIData_ROI *pROI = (VIData_ROI *)m_ctlCompDataList.GetItemData(i);
		bool bFound = false;

		Rect rc = pROI->GetBoundingBox();
		if (pt.x >= rc.x && pt.x <= (rc.x + rc.width - 1)
			&& pt.y >= rc.y && pt.y <= (rc.y + rc.height - 1)) {
			bFound = true;
		}

		if (bFound) {
			SelectAllCompData(false);
			m_ctlCompDataList.SetSel(i);
			OnSelchangeCompdataList();
			break;
		}
	}
}

void CLDCDataEditerDlg::SetControlDataValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	CWnd *pWnd = GetFocusControl();
	if (pWnd == nullptr) {
		return;
	}

	wchar_t szText[256];
	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_DATA_CENTER_POS_X:
	case IDC_DATA_CENTER_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_CENTER_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_DATA_CENTER_POS_Y)->SetWindowText(szText);
		break;
	case IDC_DATA_RADIUS:
		swprintf_s(szText, L"%d", cvRound(sqrt(pow(ptImagePos.x - m_nDataCenterX, 2) + pow(ptImagePos.y - m_nDataCenterY, 2))));
		GetDlgItem(IDC_DATA_RADIUS)->SetWindowText(szText);
		break;
	case IDC_DATA_START_POS_X:
	case IDC_DATA_START_POS_Y:
		{
			int nWidth = m_nDataEndX - m_nDataStartX;
			int nHeight = m_nDataEndY - m_nDataStartY;
			swprintf_s(szText, L"%d", ptImagePos.x);
			GetDlgItem(IDC_DATA_START_POS_X)->SetWindowText(szText);
			swprintf_s(szText, L"%d", ptImagePos.y);
			GetDlgItem(IDC_DATA_START_POS_Y)->SetWindowText(szText);
			swprintf_s(szText, L"%d", ptImagePos.x + nWidth);
			GetDlgItem(IDC_DATA_END_POS_X)->SetWindowText(szText);
			swprintf_s(szText, L"%d", ptImagePos.y + nHeight);
			GetDlgItem(IDC_DATA_END_POS_Y)->SetWindowText(szText);
		}
		break;
	case IDC_DATA_END_POS_X:
	case IDC_DATA_END_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_END_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_DATA_END_POS_Y)->SetWindowText(szText);
		break;
	case IDC_DATA_REF_POS_X:
	case IDC_DATA_REF_POS_Y:
		swprintf_s(szText, L"%d", ptImagePos.x);
		GetDlgItem(IDC_DATA_REF_POS_X)->SetWindowText(szText);
		swprintf_s(szText, L"%d", ptImagePos.y);
		GetDlgItem(IDC_DATA_REF_POS_Y)->SetWindowText(szText);
		break;
	}

	ApplyCompData(false);
}

CWnd *CLDCDataEditerDlg::GetFocusControl()
{
	CWnd *pWnd = GetFocus();
	if (pWnd == nullptr) {
		return nullptr;
	}

	int nID = pWnd->GetDlgCtrlID();
	switch (nID) {
	case IDC_DATA_CENTER_POS_X:
	case IDC_DATA_CENTER_POS_Y:
	case IDC_DATA_RADIUS:
	case IDC_DATA_START_POS_X:
	case IDC_DATA_START_POS_Y:
	case IDC_DATA_END_POS_X:
	case IDC_DATA_END_POS_Y:
	case IDC_DATA_REF_POS_X:
	case IDC_DATA_REF_POS_Y:
		return pWnd;
	}

	return nullptr;
}

void CLDCDataEditerDlg::OnBnClickedReloadData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeModel();
}


void CLDCDataEditerDlg::OnBnClickedReloadModelData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadModelList();
}

void CLDCDataEditerDlg::OnBnClickedEditCalibrationData()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ShowWindow(SW_HIDE);

	CLDCCalibrationDlg iDlg;
	iDlg.DoModal();

	CRect rc;
	iDlg.GetWindowPos(rc);
	SetWindowPos(NULL, rc.left, rc.top, rc.Width(), rc.Height(), SWP_NOZORDER);

	ShowWindow(SW_SHOW);
}
