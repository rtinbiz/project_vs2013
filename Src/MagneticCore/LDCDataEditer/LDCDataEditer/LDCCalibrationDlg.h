#pragma once

#define	VISION_CALIBRATION_DATA_PATH L"D:\\nBizSystem\\Vision\\Calibration.vid"
#define	VISION_CALIBRATION_SAMPLE_PATH L"D:\\nBizSystem\\Vision\\ModelSample\\Calibration.jpg"

// CLDCCalibrationDlg 대화 상자입니다.

class CLDCCalibrationDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLDCCalibrationDlg)

private :
	CRect m_rcPos;
	CString m_strImagePath;
	Mat m_dImg;
	NIPLImageCtl m_wndImg;
	VIData m_dVI;

	HICON m_hIcon;

public:
	void GetWindowPos(CRect &rc);
	void LoadCalibrationData();
	void ShowImage();
	void LoadImage();
	void AddLogText(const wchar_t* szText, ...);
	void ShowImageInfo();
	void LoadVIData();
	void SaveVIData();
	void ShowDataBox();
	void ApplyData();
	void ShowImagePosValue(CPoint ptImagePos);
	void SetControlDataValue(CPoint ptImagePos);
	CWnd *GetFocusControl();

	CListBox m_ctlLogList;
	CStatic m_ctlImageArea;
	CString m_strImageInfo;
	CString m_strImagePos;

	int m_nGuideLineLeftStartX;
	int m_nGuideLineLeftStartY;
	int m_nGuideLineLeftEndX;
	int m_nGuideLineLeftEndY;
	int m_nGuideLineRightEndX;
	int m_nGuideLineRightEndY;
	int m_nNormalizeWidth;

public:
	CLDCCalibrationDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLDCCalibrationDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_CALIBRATION_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedReloadData();
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnBnClickedDataApply();
	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedOpenImage();
	afx_msg void OnBnClickedLoadImage();
};
