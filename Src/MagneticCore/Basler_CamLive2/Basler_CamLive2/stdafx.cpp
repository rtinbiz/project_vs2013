
// stdafx.cpp : source file that includes just the standard includes
// Basler_CamLive2.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"


char* ConvertUnicodeToMultybyte(CString strUnicode)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
	char* pMultibyte = new char[nLen];
	memset(pMultibyte, 0x00, (nLen)*sizeof(char));
	if (nLen > 510)nLen = 510;
	WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, pMultibyte, nLen, NULL, NULL);
	strcpy_s(chResult, pMultibyte);
	delete[] pMultibyte;
	return chResult;
}

wchar_t* ConvertMultybyteToUnicode(const char *chText)
{
	wchar_t* pszTmp = NULL;
	int iLen = ::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, 0);
	if (iLen > 510)iLen = 510;
	pszTmp = new wchar_t[iLen + 1];
	::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, iLen);
	wcscpy_s(wcResult, pszTmp);
	delete[] pszTmp;
	return wcResult;
}

