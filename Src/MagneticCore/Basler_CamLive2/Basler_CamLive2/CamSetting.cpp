#include "stdafx.h"
#include "CamSetting.h"


CamSetting::CamSetting()
{
	m_iniRecipe.SetFileName(RECIPE_FOLDER);

}


CamSetting::~CamSetting()
{
}



int CamSetting::renewCamSettingParam(CString recipe, int camIdx)
{
	BOOL bReturn = FALSE;
	CString strValueData;
	CString strRecipe, strRecipeName;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if (strRecipe.Right(4) == ".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength() - 4);
			if (strRecipeName == recipe)
			{
				m_iniRecipe.SetFileName(finder.GetFilePath());
				bReturn = true; //found
				break;
			}
		}
	}
	
	if (bReturn)
	{
		//CAM1
		if (camIdx == CAM_PCHI){
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_LIGHT_INTENSITY, L"");
			m_RecipeParam.iParam[RP_CAM1_LIGHT_SOURCE] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_RED, L"");
			m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_RED] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_GREEN, L"");
			m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BALANCERATIORAW_BLUE, L"");
			m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_LIGHT_TIME, L"");
			m_RecipeParam.iParam[RP_CAM1_LIGHT_TIME] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAIN_RAW, L"");
			m_RecipeParam.iParam[RP_CAM1_GAIN_RAW] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_BLACKLEVEL_RAW, L"");
			m_RecipeParam.iParam[RP_CAM1_BLACKLEVEL_RAW] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAMMA_ENABLE, L"");
			m_RecipeParam.iParam[RP_CAM1_GAMMA_ENABLE] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_GAMMA, L"");
			m_RecipeParam.fParam[RP_CAM1_GAMMA] = _wtof(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM1_WHITE_BALANCE, L"");
			m_RecipeParam.iParam[RP_CAM1_WHITE_BALANCE] = _wtoi(strValueData);
		}

		//CAM2
		if (camIdx == CAM_PCCA){
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_LIGHT_INTENSITY, L"");
			m_RecipeParam.iParam[RP_CAM2_LIGHT_SOURCE] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_RED, L"");
			m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_RED] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_GREEN, L"");
			m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_GREEN] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BALANCERATIORAW_BLUE, L"");
			m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_BLUE] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_LIGHT_TIME, L"");
			m_RecipeParam.iParam[RP_CAM2_LIGHT_TIME] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAIN_RAW, L"");
			m_RecipeParam.iParam[RP_CAM2_GAIN_RAW] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_BLACKLEVEL_RAW, L"");
			m_RecipeParam.iParam[RP_CAM2_BLACKLEVEL_RAW] = _wtoi(strValueData);

			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAMMA_ENABLE, L"");
			m_RecipeParam.iParam[RP_CAM2_GAMMA_ENABLE] = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_GAMMA, L"");
			m_RecipeParam.fParam[RP_CAM2_GAMMA] = _wtof(strValueData);
			strValueData = m_fnReadIniFile(SECTION_RECIPE_CAM, KEYNAME_CAM2_WHITE_BALANCE, L"");
			m_RecipeParam.iParam[RP_CAM2_WHITE_BALANCE] = _wtoi(strValueData);
		}

	}

	return 0;
}


CString CamSetting::m_fnReadIniFile(LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default)
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"";
	if (default != 0)
	{
		lpDefault = default;
	}

	bReturn = m_iniRecipe.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	if (bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";
		//G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}


int CamSetting::pylonSetParam2(Camera_t *pCamera, int camIdx)
{
	int exitCode = 0;
	try
	{
		if (camIdx == CAM_PCHI){
			//Set acquisition mode
			//pCamera->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
			///Light Source type setting
			pCamera->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);
			////////////////
			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_RED]);

			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_GREEN]);

			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_BALANCERATIORAW_BLUE]);

			////Set exposure settings
			pCamera->ExposureMode.SetValue(ExposureMode_Timed);
			pCamera->ExposureTimeRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_LIGHT_TIME]);

			//// Gain Setting
			pCamera->GainRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_GAIN_RAW]);
			pCamera->BlackLevelSelector.SetValue(BlackLevelSelector_All);
			pCamera->BlackLevelRaw.SetValue(m_RecipeParam.iParam[RP_CAM1_BLACKLEVEL_RAW]);
			pCamera->GammaSelector.SetValue(GammaSelector_User);
			pCamera->Gamma.SetValue(m_RecipeParam.fParam[RP_CAM1_GAMMA]);
			pCamera->GammaEnable.SetValue(m_RecipeParam.iParam[RP_CAM1_GAMMA_ENABLE]);

			////WhiteBalance Off
			pCamera->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

			// The parameter MaxNumBuffer can be used to control the count of buffers
			// allocated for grabbing. The default value of this parameter is 10.
			//pCamera->MaxNumBuffer = 5;
		}

		if (camIdx == CAM_PCCA){
			//Set acquisition mode
			//pCamera->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
			///Light Source type setting
			pCamera->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);
			////////////////
			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_RED]);

			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_GREEN]);

			pCamera->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
			pCamera->BalanceRatioRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_BALANCERATIORAW_BLUE]);

			////Set exposure settings
			pCamera->ExposureMode.SetValue(ExposureMode_Timed);
			pCamera->ExposureTimeRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_LIGHT_TIME]);

			//// Gain Setting
			pCamera->GainRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_GAIN_RAW]);
			pCamera->BlackLevelSelector.SetValue(BlackLevelSelector_All);
			pCamera->BlackLevelRaw.SetValue(m_RecipeParam.iParam[RP_CAM2_BLACKLEVEL_RAW]);
			pCamera->GammaSelector.SetValue(GammaSelector_User);
			pCamera->Gamma.SetValue(m_RecipeParam.fParam[RP_CAM2_GAMMA]);
			pCamera->GammaEnable.SetValue(m_RecipeParam.iParam[RP_CAM2_GAMMA_ENABLE]);

			////WhiteBalance Off
			pCamera->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

			// The parameter MaxNumBuffer can be used to control the count of buffers
			// allocated for grabbing. The default value of this parameter is 10.
			//pCamera->MaxNumBuffer = 5;
		}

	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		//G_AddLog(3, L"pylonSetParam Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = -1;
	}

	return exitCode;
}