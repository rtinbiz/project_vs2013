
// Basler_CamLive2Dlg.h : header file
//
#include "CamSetting.h"


#define LIVE_SIZE_W		1624
#define LIVE_SIZE_H		1234

#pragma once
#pragma pack(1)
typedef struct SMP_LIVE_TAG
{
	char LiveImage[2][LIVE_SIZE_W * LIVE_SIZE_H];
	int LiveGrabCamsRun;
	double AFValueCam[2];
	int LiveCheck[2];
	wchar_t wcPNo[64];
	BOOL paramSet[2];
}SMP_LIVE;
#pragma pack()

typedef struct LiveThreadData_TAG
{
	BOOL RunThread;

	int CamIndex;
	SMP_LIVE *pSMP;
	Camera_t *pCamera;
	CWinThread *pLiveThread;
	CamSetting *m_pCamSetting;

	LiveThreadData_TAG()
	{
		RunThread = FALSE;
		CamIndex = -1;
		pSMP = NULL;
		pCamera = NULL;
		pLiveThread = NULL;
		m_pCamSetting = NULL;
	}
}LData;


// CBasler_CamLive2Dlg dialog
class CBasler_CamLive2Dlg : public CDialogEx
{
// Construction
public:
	CBasler_CamLive2Dlg(CWnd* pParent = NULL);	// standard constructor
	~CBasler_CamLive2Dlg();

// Dialog Data
	enum { IDD = IDD_BASLER_CAMLIVE2_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	HANDLE hLIMapping;
	SMP_LIVE * lpMapping;
	ITransportLayer *pTl;
	DeviceInfoList_t devices;

	Camera_t *pCamera[2];
	LData LiveData[2];
	void ConnectLiveCam();

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnClose();

	CamSetting *m_pCamSetting;

};

UINT thfn_LiveThread(LPVOID pParam);