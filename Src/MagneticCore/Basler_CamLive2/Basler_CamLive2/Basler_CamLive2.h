
// Basler_CamLive2.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CBasler_CamLive2App:
// See Basler_CamLive2.cpp for the implementation of this class
//

class CBasler_CamLive2App : public CWinApp
{
public:
	CBasler_CamLive2App();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CBasler_CamLive2App theApp;