
// Basler_CamLive2Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "Basler_CamLive2.h"
#include "Basler_CamLive2Dlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void GetAFValue(BYTE *a, int sx, int sy, BYTE *out)
{
	// (In) a: The image (row major) containing elements of int. 
	// (In) sx: size of image along x-dimension. 
	// (In) sy: size of image along y-dimension. 
	// (Out) out: The output image with dimensions (sx-2, sy-2). 

	int i, j, ctr = 0;
	BYTE *p1, *p2, *p3;

	p1 = a; p2 = p1 + sx; p3 = p2 + sx;

	for (j = 1; j < sy - 1; ++j)
	{
		for (i = 1; i < sx - 1; ++i)
		{
			out[ctr++] = (abs((p1[0] + 2 * p1[1] + p1[2]) - (p3[0] + 2 * p3[1] + p3[2])) +
				abs((p1[2] + 2 * p2[2] + p3[2]) - (p1[0] + 2 * p2[0] + p3[0]))) / 6;
			++p1; ++p2; ++p3;
		}
		//	p1 += 2; p2 += 2; p3 +=2; 
	}
}
double m_fnGetFVByImage(BYTE * pRawData, IplImage *pDestImage)
{
	double returnFValue = 0.0;
	CvScalar sumBuff;
	cvZero(pDestImage);
	GetAFValue((BYTE*)pRawData, pDestImage->width, pDestImage->height, (BYTE*)pDestImage->imageData);
	sumBuff = cvSum(pDestImage);
	returnFValue = sumBuff.val[0];
	return returnFValue;
}

UINT thfn_LiveThread(LPVOID pParam)
{
	LData *pData = (LData *)pParam;
	Camera_t *pCam = pData->pCamera;
	//CvvImage aaa;
	try
	{
		// Open the camera
		pCam->Open();

		// Get the first stream grabber object of the selected camera
		Camera_t::StreamGrabber_t StreamGrabber(pCam->GetStreamGrabber(0));

		// Open the stream grabber
		StreamGrabber.Open();

		// Set the image format and AOI
		pCam->PixelFormat.SetValue(PixelFormat_BayerBG8);//PixelFormat_YUV422Packed);
		pCam->OffsetX.SetValue(0);
		pCam->OffsetY.SetValue(0);
		//pCam->Width.SetValue(pCam->Width.GetMax());
		//pCam->Height.SetValue(pCam->Height.GetMax());

		pCam->Width.SetValue(LIVE_SIZE_W);
		pCam->Height.SetValue(LIVE_SIZE_H);


		//pCam1 = cvCreateImage(cvSize((int)pCam->Width.GetMax(), (int)pCam->Height.GetMax()),IPL_DEPTH_8U , 1);
		//pCam2 = cvCreateImage(cvSize((int)Camera2.Width.GetMax(), (int)Camera2.Height.GetMax()),IPL_DEPTH_8U , 1);
		//Disable acquisition start trigger if available
		{
			GenApi::IEnumEntry* acquisitionStart = pCam->TriggerSelector.GetEntry(TriggerSelector_AcquisitionStart);
			if (acquisitionStart && GenApi::IsAvailable(acquisitionStart))
			{
				pCam->TriggerSelector.SetValue(TriggerSelector_AcquisitionStart);
				pCam->TriggerMode.SetValue(TriggerMode_Off);
			}
		}

		//Disable frame start trigger if available
		{
			GenApi::IEnumEntry* frameStart = pCam->TriggerSelector.GetEntry(TriggerSelector_FrameStart);
			if (frameStart && GenApi::IsAvailable(frameStart))
			{
				pCam->TriggerSelector.SetValue(TriggerSelector_FrameStart);
				pCam->TriggerMode.SetValue(TriggerMode_Off);
			}
		}

		//Set acquisition mode
		pCam->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		///Light Source type setting
		pCam->LightSourceSelector.SetValue(LightSourceSelector_Daylight6500K);	
		////////////////
		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
		//pCam->BalanceRatioAbs.SetValue(2.73438);
		pCam->BalanceRatioRaw.SetValue(191);

		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
		//pCam->BalanceRatioAbs.SetValue(1.28125);
		pCam->BalanceRatioRaw.SetValue(96);

		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
		//pCam->BalanceRatioAbs.SetValue(6.4513);
		//pCam->BalanceRatioAbs.SetValue(13.4513);
		pCam->BalanceRatioRaw.SetValue(118);

		//Set exposure settings
		pCam->ExposureMode.SetValue(ExposureMode_Timed);
		pCam->ExposureTimeRaw.SetValue(5000);

		// Gain Setting
		pCam->GainRaw.SetValue(300);
		pCam->BlackLevelSelector.SetValue(BlackLevelSelector_All);
		pCam->BlackLevelRaw.SetValue(50);
		pCam->GammaSelector.SetValue(GammaSelector_User);
		pCam->Gamma.SetValue(0.7);
		pCam->GammaEnable.SetValue(true);
		//pCam->GammaEnable.SetValue(false);

		// Create an image buffer
		const size_t ImageSize = (size_t)(pCam->PayloadSize.GetValue());

		// We won't use image buffers greater than ImageSize
		StreamGrabber.MaxBufferSize.SetValue(ImageSize);
		// We won't queue more than one image buffer at a time
		StreamGrabber.MaxNumBuffer.SetValue(1);

		// Allocate all resources for grabbing. Critical parameters like image
		// size now must not be changed until FinishGrab() is called.
		StreamGrabber.PrepareGrab();

		const StreamBufferHandle hBuffer = StreamGrabber.RegisterBuffer(pData->pSMP->LiveImage[pData->CamIndex], ImageSize);

		// Put the buffer into the grab queue for grabbing
		StreamGrabber.QueueBuffer(hBuffer, NULL);

		// Grab 10 times
		const uint32_t numGrabs = 100;

		IplImage *destimage = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H), IPL_DEPTH_8U, 1);

		DWORD GrabTime1 = 0;
		pData->pSMP->LiveCheck[pData->CamIndex] = 4;
		while (pData->RunThread == TRUE && pData->pSMP->LiveGrabCamsRun == 1)
		{
			//cout << "Cam1==========================================================" << endl;
			// Let the camera acquire one single image ( Acquisiton mode equals
			// SingleFrame! )
			if (pData->pSMP->LiveCheck[pData->CamIndex] == 4)
			{
				GrabTime1 = ::GetTickCount();
				pCam->AcquisitionStart.Execute();

				// Wait for the grabbed image with a timeout of 3 seconds
				if (StreamGrabber.GetWaitObject().Wait(3000))
				{
					// Get the grab result from the grabber's result queue
					GrabResult Result;
					StreamGrabber.RetrieveResult(Result);

					if (Result.Succeeded())
					{

						pData->pSMP->LiveCheck[pData->CamIndex] = 1;
						memcpy(pData->pSMP->LiveImage[pData->CamIndex], Result.Buffer(), ImageSize);

						pData->pSMP->AFValueCam[pData->CamIndex] = m_fnGetFVByImage((BYTE*)pData->pSMP->LiveImage[pData->CamIndex], destimage);
						// Reuse the buffer for grabbing the next image
						//if (n < numGrabs - 1)
						StreamGrabber.QueueBuffer(Result.Handle(), NULL);
						pData->pSMP->LiveCheck[pData->CamIndex] = 2;
					}
					else
					{
						break;
					}
				}
			}
			else if (pData->pSMP->paramSet[pData->CamIndex] == TRUE) //Param Set
			{
				pData->m_pCamSetting->renewCamSettingParam(pData->pSMP->wcPNo, pData->CamIndex);
				pData->m_pCamSetting->pylonSetParam2(pData->pCamera, pData->CamIndex);
				pData->pSMP->paramSet[pData->CamIndex] = FALSE;
			}else
				Sleep(10);
		}
		// Clean up

		// You must deregister the buffers before freeing the memory
		StreamGrabber.DeregisterBuffer(hBuffer);

		// Free all resources used for grabbing
		StreamGrabber.FinishGrab();

		// Close stream grabber
		StreamGrabber.Close();

		// Close camera
		pCam->Close();
	}
	catch (GenICam::GenericException &e)
	{
		CString strCatchMsg;
		// Error handling
		strCatchMsg.Format(L"%s", e.GetDescription());
		pData->pLiveThread = NULL;
		return 0;
	}
	pData->pLiveThread = NULL;

	return 0;
}
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CBasler_CamLive2Dlg dialog



CBasler_CamLive2Dlg::CBasler_CamLive2Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBasler_CamLive2Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}
CBasler_CamLive2Dlg::~CBasler_CamLive2Dlg()
{
	LiveData[0].RunThread = FALSE;
	while (LiveData[0].pLiveThread != NULL)
	{
		Sleep(10);
	}
	LiveData[1].RunThread = FALSE;
	while (LiveData[1].pLiveThread != NULL)
	{
		Sleep(10);
	}

	if (pCamera[0] != NULL)
		delete pCamera[0];

	if (pCamera[1] != NULL)
		delete pCamera[1];


	CloseHandle(hLIMapping);
}

void CBasler_CamLive2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBasler_CamLive2Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CBasler_CamLive2Dlg::OnBnClickedOk)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CBasler_CamLive2Dlg message handlers

BOOL CBasler_CamLive2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	pCamera[0] = NULL;
	pCamera[1] = NULL;
	hLIMapping = NULL;

	int MapSize = sizeof(SMP_LIVE);

	hLIMapping = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, MapSize, CAM_LIVE_IMG_MAP_NAME);
	lpMapping = (SMP_LIVE*)MapViewOfFile(hLIMapping, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	lpMapping->LiveGrabCamsRun = 1;

	m_pCamSetting = new CamSetting();

	// Create the camera object of the first available camera.
	// The camera object is used to set and get all available
	// camera features.
	ConnectLiveCam();
	::SetTimer(this->m_hWnd, UI_LIVE_CONNECT, 3000, 0);//InitDlg

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CBasler_CamLive2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBasler_CamLive2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBasler_CamLive2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


BOOL CBasler_CamLive2Dlg::PreTranslateMessage(MSG* pMsg)
{

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE) 
		return TRUE;

	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN) 
		return TRUE;
	return CDialogEx::PreTranslateMessage(pMsg);
	//return TRUE;
}


void CBasler_CamLive2Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == UI_LIVE_CONNECT)
	{
		ConnectLiveCam();
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CBasler_CamLive2Dlg::ConnectLiveCam()
{
	int tryCnt;
	if (LiveData[0].pLiveThread != NULL && LiveData[1].pLiveThread != NULL)
	{
		if (this->IsWindowVisible() == TRUE)
			//this->ShowWindow(SW_HIDE);
		return;
	}
	if (this->IsWindowVisible() == FALSE)
		this->ShowWindow(SW_SHOW);
	//////////////////////////////////////////////////////
	// Get the transport layer factory
	CTlFactory& TlFactory = CTlFactory::GetInstance();

	// Create the transport layer object needed to enumerate or
	// create a camera object of type Camera_t::DeviceClass()
	pTl = TlFactory.CreateTl(Camera_t::DeviceClass());

	// Exit the application if the specific transport layer is not available
	if (!pTl)
		return;
	// Get all attached cameras and exit the application if no camera is found
	if (0 == pTl->EnumerateDevices(devices))
		return;

	int CamCount = devices.size();
	if (CamCount<2)
	{
		LiveData[0].RunThread = FALSE;
		while (LiveData[0].pLiveThread != NULL)
			Sleep(10);

		LiveData[1].RunThread = FALSE;
		while (LiveData[1].pLiveThread != NULL)
			Sleep(10);

		if (pCamera[0] != NULL)
			delete pCamera[0];
		pCamera[0] = NULL;

		if (pCamera[1] != NULL)
			delete pCamera[1];

		pCamera[1] = NULL;
		return;
	}


	CString CamName;
	if (LiveData[0].pLiveThread == NULL)
	{
		//////////////////////////////////////////////////////
		if (pCamera[0] != NULL)
			delete pCamera[0];

		tryCnt = 0;
	RETRY2:
		try{
			pCamera[0] = new Camera_t(pTl->CreateDevice(devices[0]));
		}
		catch (GenICam::GenericException &e)
		{
			// Error handling.

			if (++tryCnt < 3){
				goto RETRY2;
			}
			else{
				return;
			}
		}

		CamName.Format(L"%s", ConvertMultybyteToUnicode(pCamera[0]->GetDeviceInfo().GetUserDefinedName().c_str()));
		if (CamName.CompareNoCase(L"PCHI") == 0)
			//if(CamName == "Cam1")
		{
			LiveData[0].CamIndex = 0;
		}
		else
		{
			LiveData[0].CamIndex = 1;
		}
		LiveData[0].pCamera = pCamera[0];
		LiveData[0].pSMP = lpMapping;
		LiveData[0].RunThread = TRUE;
		LiveData[0].m_pCamSetting = m_pCamSetting;
		LiveData[0].pLiveThread = ::AfxBeginThread(thfn_LiveThread, &LiveData[0], THREAD_PRIORITY_HIGHEST);
	}
	if (LiveData[1].pLiveThread == NULL)
	{
		if (pCamera[1] != NULL)
			delete pCamera[1];
		tryCnt = 0;
	RETRY3:
		try{
			pCamera[1] = new Camera_t(pTl->CreateDevice(devices[1]));
		}
		catch (GenICam::GenericException &e)
		{
			// Error handling.

			if (++tryCnt < 3){
				goto RETRY3;
			}
			else{
				return;
			}
		}

		CamName.Format(L"%s", ConvertMultybyteToUnicode(pCamera[1]->GetDeviceInfo().GetUserDefinedName().c_str())); //.GetModelName());
		if (CamName.CompareNoCase(L"PCCA") == 0)
		{
			LiveData[1].CamIndex = 1;
		}
		else
		{
			LiveData[1].CamIndex = 0;
		}
		LiveData[1].pCamera = pCamera[1];
		LiveData[1].pSMP = lpMapping;
		LiveData[1].RunThread = TRUE;
		LiveData[1].m_pCamSetting = m_pCamSetting;
		LiveData[1].pLiveThread = ::AfxBeginThread(thfn_LiveThread, &LiveData[1], THREAD_PRIORITY_HIGHEST);
	}
}

void CBasler_CamLive2Dlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}


void CBasler_CamLive2Dlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnClose();
}
