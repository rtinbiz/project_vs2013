
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently


#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars


// Include files to use the PYLON API
#include <pylon/PylonIncludes.h>
#include <conio.h>
using namespace Pylon;


// Settings to use Basler GigE cameras
#include <pylon/gige/BaslerGigECamera.h>
typedef Pylon::CBaslerGigECamera Camera_t;
using namespace Basler_GigECameraParams;
using namespace Basler_GigEStreamParams;

#include "opencv2/opencv.hpp"


#define CAM_LIVE_IMG_MAP_NAME		L"LIVE_IMG"
#define UI_LIVE_CONNECT				1150
#define UI_LIVE_CLOSE				1151


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


char chResult[512];
wchar_t wcResult[512];

char* ConvertUnicodeToMultybyte(CString strUnicode);
wchar_t* ConvertMultybyteToUnicode(const char *chText);

#define		RECIPE_FOLDER								_T("D:\\nBizSystem\\Recipe\\")
#define		RECIPE_FOLDER_PATH						_T("D:\\nBizSystem\\Recipe\\*.*")


#define CAM_NO				2

//
#define CAM_PCHI			0
#define CAM_PCCA			1
#define CAM_PCCA2			2
#define CAM_PCHI2			3

