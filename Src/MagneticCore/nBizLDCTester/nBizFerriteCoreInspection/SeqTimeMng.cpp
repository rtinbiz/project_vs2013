// SeqTimeMng.cpp: implementation of the CSeqTimeMng class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SeqTimeMng.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define CV_1NEXT_HANDLING_TIMEOUT	5
#define CV_CUR_HANDLING_TIMEOUT	20

CSeqTimeMng::CSeqTimeMng()
{
	m_nStartLog = 0;
	m_nStart1NextHandlingTimeout = 0;
	m_nStartCurHandlingTimeout = 0;
	m_nStartUserHandlingTimeout = 0;
	m_nStartDSensorTimeout = 0;
	m_nStartSSensorTimeout = 0;
}

CSeqTimeMng::~CSeqTimeMng()
{

}

int	CSeqTimeMng::m_fnShowInformDlg(int nSwType, char* szInform)
{
// 	CInformDlg dlg;
// 
// 	dlg.m_fnSetInform(nSwType, szInform);
// 
// 	return (int)dlg.DoModal();
	return 0;
}

BOOL CSeqTimeMng::m_fnIs1NextHandlignTimeout()
{
	int nTimeOut = m_fnGetCurrentTime() - m_nStart1NextHandlingTimeout;
	if( nTimeOut < 0 ) nTimeOut += 24 * 3600;
 
	if(CV_1NEXT_HANDLING_TIMEOUT <= nTimeOut)	return TRUE;
	else										return FALSE;
}

void CSeqTimeMng::m_fnReset1NextHandlingTimeout()
{
	m_nStart1NextHandlingTimeout = m_fnGetCurrentTime();
}

BOOL CSeqTimeMng::m_fnIsCurHandlignTimeout()
{
	int nTimeOut = m_fnGetCurrentTime() - m_nStartCurHandlingTimeout;
	if( nTimeOut < 0 ) nTimeOut += 24 * 3600;
 
	if(CV_CUR_HANDLING_TIMEOUT <= nTimeOut)	return TRUE;
	else										return FALSE;
}

void CSeqTimeMng::m_fnResetCurHandlingTimeout()
{
	m_nStartCurHandlingTimeout = m_fnGetCurrentTime();
}


BOOL CSeqTimeMng::m_fnIsUserHandlignTimeout(int nUserTimeOut, BOOL bLogWrite, int nLogTimeSec, int nLogLevel, char* pLogText)
{
	int nTimeOut = m_fnGetCurrentTime() - m_nStartUserHandlingTimeout;
	if( nTimeOut < 0 ) nTimeOut += 24 * 3600;	
	
	if(nTimeOut % nLogTimeSec == 0)
	{
		if(m_nLogOldTime != nTimeOut)
			G_AddLog(nLogLevel,L"Waiting Time %d/%dSec : %s", nTimeOut, nUserTimeOut, pLogText);

		m_nLogOldTime = nTimeOut;		
	}
	
	if(nUserTimeOut <= nTimeOut)	return TRUE;
	else										    return FALSE;	

}

void CSeqTimeMng::m_fnResetUserHandlingTimeout()
{
	m_nStartUserHandlingTimeout = m_fnGetCurrentTime();
}