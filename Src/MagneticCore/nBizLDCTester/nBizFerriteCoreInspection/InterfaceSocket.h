#pragma once

#include "Socket/ClientSocket.h"
#include "1.Common/IniClass/BIniFile.h"
#include "1.Common\ExclAPI\XLAutomation.h"
#include "1.Common\ExclAPI\XLEzAutomation.h"
//#include "InformDlg.h"
#include "SeqTimeMng.h"
#include <vector>
#include < tlhelp32.h>
using namespace std;


//Sequence Check
static char *G_ByWho[] = 
{
	"0",//"BY_HOST",
	"1",//"BY_OP",
	"2"//"BY_EQP_SELF"
};

#define CMD_STX		 0x02
#define CMD_ETX		 0x03

#define CMD_ALIVE	 0x01
#define CMD_PANELID	 0x10
#define CMD_ACK		 0x11
#define CMD_NAK		 0x12
#define CMD_START	 0x13
#define CMD_END		 0x14
#define CMD_STOP     0x15
#define CMD_OK		 0x16
#define CMD_NG		 0x16
#define CMD_ALARM	 0x17
#define CMD_SAVE	 0x18

#define PORT_SCANNER		1000	//SCANNER PORT

class CInterfaceSocket  : public CSeqTimeMng
{
	DECLARE_DYNAMIC(CInterfaceSocket)

public:
	CInterfaceSocket(void);
	~CInterfaceSocket(void);

protected:

	DECLARE_MESSAGE_MAP()

private:
	//CInformDlg m_cimInfodlg;
	int m_nSeqSetp;
public:	

	struct EqpSendData
	{
		BYTE	  nSTX;
		BYTE      nCMD;
		wchar_t      chData[68];
		BYTE      nETX;		

		EqpSendData()
		{
			memset(this, 0x00, sizeof(EqpSendData));
		}
	}m_SendData, m_ReceiveData;

	struct FromCimTimeData
	{
		wchar_t	  chReceiveTime[100];
		int       nID;	

		FromCimTimeData()
		{
			memset(this, 0x00, sizeof(FromCimTimeData));
		}
	}m_ReceiveTimeData;
	
	CRITICAL_SECTION  m_CriSend;

	CClientSocket m_ClientSocket[2];	//scanner 2
	wchar_t m_chServerIP[2][50];
	wchar_t m_chServerPortNum[2][50];
	int portScanner;
	//CString m_strReciveString;
	vector<char> m_vecReceiveData;

	CBIniFile iniSocket;
	BOOL m_bConnect[2];
	void m_fnInitSocket();
	void m_fnSendToEqp(BYTE nCmd, CString strData);
	void m_fnSendToEqpReply(BYTE nCmd, int nWhere);
	void m_fnGetSystemErrorStatus(int *pIndex);	
	void m_fnDataReset();	

	LRESULT OnClientReceive(WPARAM wParam, LPARAM lClient);
	LRESULT OnClientReConnect(WPARAM wParam, LPARAM lClient);
	LRESULT OnConnectOK(WPARAM wParam, LPARAM lClient);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	void m_fnGetReceiveDataTime(int nId);

	bool ProcessKill(CString strProcessName);
	bool GetProcessModule(DWORD dwPID,CString sProcessName);

private:
	void		m_fnCheck();
};
