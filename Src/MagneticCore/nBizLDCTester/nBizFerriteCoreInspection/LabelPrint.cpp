// LabelPrint.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "nBizLDCTester.h"
#include "LabelPrint.h"
#include "afxdialogex.h"

#pragma comment(lib,"sqlite\\sqlite3.lib")

// CLabelPrint 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLabelPrint, CDialogEx)

extern CMainWnd	*G_MainWnd;

CLabelPrint::CLabelPrint(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLabelPrint::IDD, pParent)
{

}

CLabelPrint::~CLabelPrint()
{
}

void CLabelPrint::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_LABEL_PNO, m_staticPNo);
	//DDX_Control(pDX, IDC_EDIT_LOTNO, m_editLotNo);
	DDX_Control(pDX, IDC_EDIT_PRINT_COUNT, m_editPrintCount);
	DDX_Control(pDX, IDC_GRID_LABEL_MODEL, m_GridLabelModel);
	DDX_Control(pDX, IDC_EDIT_LABEL_SERIAL, m_editSerialNo);
	DDX_Control(pDX, IDC_COMBO_LOT_MONTH, m_cmbLotMonth);
	DDX_Control(pDX, IDC_COMBO_LOT_DAY, m_cmbLotDay);
	DDX_Control(pDX, IDC_BTN_SERIALNO_SET2, m_btnLotNoSet);
	DDX_Control(pDX, IDC_BTN_SERIALNO_SET, m_btnSetSerialNo);
	DDX_Control(pDX, IDC_BTN_MANU_PRINT, m_btnManuPrint);
	DDX_Control(pDX, IDC_STATIC_LABEL_LOTNO, m_staticLotSerial);
	DDX_Control(pDX, IDC_STATIC_LOT_SET, m_staticLot);
	DDX_Control(pDX, IDC_BUTTON1, m_btnScanCountReset);
}


BEGIN_MESSAGE_MAP(CLabelPrint, CDialogEx)
	//ON_BN_CLICKED(IDC_BTN_LOTNO, &CLabelPrint::OnBnClickedBtnLotno)
	ON_BN_CLICKED(IDC_BTN_MANU_PRINT, &CLabelPrint::OnBnClickedBtnManuPrint)
	ON_NOTIFY(NM_CLICK, IDC_GRID_LABEL_MODEL, OnGridClick)
	ON_WM_TIMER()
	//ON_BN_CLICKED(IDC_CHECK_COMP_FILTER, &CLabelPrint::OnBnClickedCheck)
	ON_BN_CLICKED(IDC_BTN_SERIALNO_SET, &CLabelPrint::OnBnClickedBtnSerialnoSet)
	ON_BN_CLICKED(IDC_BTN_SERIALNO_SET2, &CLabelPrint::OnBnClickedBtnLotNoSet)
	ON_BN_CLICKED(IDC_BUTTON1, &CLabelPrint::OnBnClickedButton1)
END_MESSAGE_MAP()

void CLabelPrint::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//recipeSelected = m_GridLabelModel.GetCell(pItem->iRow, pItem->iColumn)->GetText();
	//GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(recipeSelected);
	////load recipe data
	//clearGridModelRecipe();
	//fillGridModelRecipe(recipeSelected);
}

void CLabelPrint::m_fnInit(void)
{
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	SetTimer(WM_TIMER_SERIAL, 1000, NULL);
	SetTimer(WM_TIMER_LABLE, 600000, NULL); //label lot no. delete
	memset(&m_partLabel,0x00, sizeof(PART_LABEL)); 

	sqliteInit();

	memset(m_scancheck, 0x0, sizeof(m_scancheck));
	memset(m_QRLENGTH, 0x0, sizeof(m_QRLENGTH));
	memset(m_LabelPrinterCheck, 0x0, sizeof(m_LabelPrinterCheck));
	m_nManual_Print_Count = 0;
	m_nManual_Print_Count2 = 0;
	m_nManual_Print_Countnow = 0;
}

BOOL CLabelPrint::OnInitDialog()
{
	CString str;
	int i;

	CDialogEx::OnInitDialog();
	CenterWindow();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	for (i = 1; i <= 12; i++)
	{
		str.Format(L"%d", i);
		m_cmbLotMonth.AddString(str);
	}
	for (i = 1; i <= 31; i++)
	{
		str.Format(L"%d", i);
		m_cmbLotDay.AddString(str);
	}

	customer = MOBIS;
	initGridModel();
	fillGridModel();
	setModel(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CLabelPrint::initGridModel(void)
{
	try {
		m_GridLabelModel.SetRowCount(30);
		m_GridLabelModel.SetColumnCount(2);
		m_GridLabelModel.SetFixedRowCount(1);		
		m_GridLabelModel.SetFixedColumnCount(1);
		m_GridLabelModel.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridLabelModel.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridLabelModel.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                       모 델 명                       "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"%d", row);					
					}
				}
			}
			m_GridLabelModel.SetItem(&Item);
		}
	}

	m_GridLabelModel.AutoSize(GVS_BOTH);
	m_GridLabelModel.SetTextColor(RGB(0, 0, 105));
	//m_GridLabelModel.ExpandToFit(true);
	//m_GridLabelModel.AutoSizeColumns();
	m_GridLabelModel.SetEditable(FALSE);
	m_GridLabelModel.Refresh();

	return 0;
}

int CLabelPrint::fillGridModel(void)
{
	int row = 1;

	CString strRecipe, strRecipeName;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".lbl") 
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			m_GridLabelModel.GetCell(row++, 1)->SetText(strRecipeName);
		}//
	}
	strRecipe.Empty();
	strRecipeName.Empty();
	m_GridLabelModel.Refresh();
	return 0;
}

int CLabelPrint::setModel(CString model)
{
	CString strRecipe, strRecipeName;
	BOOL bReturn = FALSE;
	CString strValueData;
	CString strFilePath;
	CFileFind finder;

	CStdioFile cstFile;

	CString strLine = _T("");
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".lbl") 
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == model)
			{
				strFilePath = (LPCTSTR)finder.GetFilePath();
				bReturn =true; //found
				break;
			}else{

			}
		}
	}

	if(bReturn)
	{
		nZPL = 0;
		try{
			cstFile.Open(strFilePath, CFile::modeRead|CFile::typeText, NULL);
			while(cstFile.ReadString(strLine))
			{
				strZPL[nZPL++]=strLine;
			}
			cstFile.Close();
		}
		catch(CFileException* pError)
		{
			TCHAR sz[1024];
			pError->GetErrorMessage(sz, 1024);
			CString errmsg = sz;
			//G_AddLog(3,L"%s", errmsg);
			G_AddLog(3, sz);
		}

		int index = _wtoi(strZPL[LINE_LABEL_INDEX]);
		if (index >= 20){
			G_AddLog(3, L"LINE_LABEL_INDEX >= 20 Error");
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"LINE_LABEL_INDEX >= 20 Error ", BLACK, RED);
			AfxMessageBox(L"LINE_LABEL_INDEX는 20보다 작아야 합니다. label file(.lbl)의 LINE_LABEL_INDEX를 수정해야 합니다. ");
			return -1;
		}

		m_staticPNo.SetText(strZPL[LINE_PARTNO], BLACK, WHITE);

		//if (G_MainWnd->m_RecipeSetDlg.part_Test.part_type == L"MOBIS"){
		if (!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type,L"MOBIS")){
			customer = MOBIS;
		}
		else if(!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type, L"LGE")){
			customer = LGE;
		}
		else if (!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type, L"MANDO")){
			customer = MANDO;
		}

		//Lot No. Set Automatically
		if (customer == MOBIS)
			setLotNo();
		else if(customer == LGE)
			setLotNoLGE();
		else if (customer == MANDO)
			setLotNoMANDO();

		index = _wtoi(strZPL[LINE_LABEL_INDEX]);
		
			if ((customer == MOBIS) || (customer == LGE))
				{
					strValueData.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + index]);
					while (strValueData.GetLength()< 4)strValueData.Insert(0, '0');
				}
			else
				{
				strValueData.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + index]);
				while (strValueData.GetLength()< 3)strValueData.Insert(0, '0');
				}
		//while(strValueData.GetLength()< 4)strValueData.Insert(0,'0');
		m_editSerialNo.SetWindowText(strValueData);
	}else{
		m_staticPNo.SetText(L"Label print file not found",BLACK, RED);
	}
	return bReturn;
}

void CLabelPrint::OnBnClickedBtnManuPrint()
{
	//
	CString str;
	//int i;//, cnt;
	int nbytes;
	

	GetDlgItem(IDC_EDIT_PRINT_COUNT)->GetWindowText(str);
	m_nManual_Print_Count = _wtoi(str);
	if (m_nManual_Print_Count == 0)m_nManual_Print_Count = 1;
////1111_kys	
		if (customer == MANDO)
			{
				m_nManual_Print_Count2 = ((float)m_nManual_Print_Count) / 2;
				m_nManual_Print_Count = (int)(m_nManual_Print_Count2+0.5);
			}
////
		for (m_nManual_Print_Countnow = 0; m_nManual_Print_Countnow < m_nManual_Print_Count; m_nManual_Print_Countnow++)
	{
		makeZPLString(customer, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		CString strMessage;
		BYTE byPort[512];
		CString strCR;
		CString strLF;
		memset(byPort, 0x00, 256);
		strCR.Format(L"%c",13);
		strLF.Format(L"%c",10);
		strMessage = ZPLString + strCR + strLF;		
		if(strMessage.GetLength() < 512){
			memset(byPort, 0x00, 512);
			memcpy(byPort, ConvertUnicodeToMultybyte(strMessage), strMessage.GetLength());
			nbytes = G_MainWnd->m_SerialInterface.m_CommThread[1].WriteComm(byPort, strMessage.GetLength());
			Sleep(10);
		}else{
			G_AddLog(3,L"strMessage.GetLength() < 512 fail  ");
		}
	}

	return;
}


int CLabelPrint::printLabel()
{
	int nbytes;
	int nLine;
	//test
	nLine = makeZPLString(customer, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	if (nLine == -1)return nLine;
	CString strMessage;
	BYTE byPort[512];
	CString strCR;
	CString strLF;
	memset(byPort, 0x00, 256);
	strCR.Format(L"%c",13);
	strLF.Format(L"%c",10);
	strMessage = ZPLString + strCR + strLF;
	//160118_KYS
	if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LABEL_SCAN_PRINT_SKIP] != 1)//NOT SKIP
	{
		if (strMessage.GetLength() < 512)
		{
			memset(byPort, 0x00, 512);
			memcpy(byPort, ConvertUnicodeToMultybyte(strMessage), strMessage.GetLength());
			nbytes = G_MainWnd->m_SerialInterface.m_CommThread[1].WriteComm(byPort, strMessage.GetLength());
		}
		else
		{
			G_AddLog(3, L"strMessage.GetLength() < 512 fail ");
		}
	}
	//
	++m_LabelPrinterCheck[LABELPRINTER_NEW];
	wchar_t step[5];
	swprintf_s(step, L"%d", m_partLabel.idxPrinterLabel);
	m_btnScanCountReset.SetWindowText(step);
	return nbytes;
}

int CLabelPrint::makeZPLString(int customer, CString model)
{
	int nLine = 0;

	if (!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type, L"MOBIS"))
		{
			nLine = makeZPLStringMOBIS(model);
		}
	else if (!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type, L"LGE"))
		{
		nLine = makeZPLStringLGE(model);
		}
	else if (!wcscmp(G_MainWnd->m_RecipeSetDlg.part_Test.part_type, L"MANDO"))
		{
		nLine = makeZPLStringMANDO(model);
	}
	
	return nLine;
}

////////////////////////////////////////////////////////////////////////////////////////////
//CUSTOMER MOBIS

int CLabelPrint::makeZPLStringMOBIS(CString model)
{
	CString str;
	CString strQR;
	int qrIndex;
	strQR.Empty();
	ZPLString.Empty();
	int nLine = 0;
	for(nLine = 0;nLine < nZPL; nLine++ )
	{
		switch(nLine){
		case  LINE_LABEL_INDEX:

			break;
		case  LINE_PARTNO:

			break;
		case  LINE_VENDORCODE:

			break;
		case  LINE_PNO_DATA:
			ZPLString+=strZPL[nLine];
			//ZPLString+=strZPL[LINE_PARTNO];
			//ZPLString+="^FS";
			break;
		case  LINE_LNO_DATA:
			ZPLString+=strZPL[nLine];
			if (setLotNo() == L"")return -1;
			ZPLString+=strLotNo;
			ZPLString+="^FS";
			break;
		case  LINE_CODE_DATA:
			ZPLString+=strZPL[nLine];
			//QR CODE
			qrIndex = makeQRCode();
			if(qrIndex == m_partLabel.idxPrinterLabel)
			{
				ZPLString +=m_partLabel.QRcode[m_partLabel.idxPrinterLabel];
				if(++m_partLabel.idxPrinterLabel >= QR_LABEL_CNT ) m_partLabel.idxPrinterLabel = 0;
			}else
			{//error
				return -1;

			}
			ZPLString+="^FS";
			break;
		default:
			ZPLString+=strZPL[nLine];
			break;
		}
	}

	return nLine;
}

CString CLabelPrint::getSerialNo(void){

	CString strSerial;
	int nSerial;
	int serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	nSerial = (G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] );
	if (nSerial > 9999)nSerial = 1;
	strSerial.Format(L"%d", nSerial);
	while (strSerial.GetLength()< 4)strSerial.Insert(0, '0');
	return strSerial;
}

int CLabelPrint::makeQRCode(void)
{
	CString str;
	CString strQR;
	int nstrQR;
	int index;
	int serialIndex;
	index = strZPL[LINE_PARTNO].Find('-');
	if(index != -1)
	{// found
		str = strZPL[LINE_PARTNO];
		int n = str.Delete(index,1);
	}

	//MOBIS_P/N 불일치
	if (G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel == L"JF_AE_DE_PHEV_OBC_TR")
	{
		if (str != MOBIS_4)
		{
			G_AddLog(3, L"해당 모델의 P/N를 확인하세요");
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"해당 모델의 P/N를 확인하세요", BLACK, RED);
			AfxMessageBox(L"해당 모델의 P/N를 확인하세요. label file(.lbl)의 P/N 정보가 불일치 합니다 ");
			return -4;
		}
	}
	if (G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel == L"JF_AE_DE_PHEV_OBC_PFC_IN")
	{
		if (str != MOBIS_5)
		{
			G_AddLog(3, L"해당 모델의 P/N를 확인하세요");
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"해당 모델의 P/N를 확인하세요", BLACK, RED);
			AfxMessageBox(L"해당 모델의 P/N를 확인하세요. label file(.lbl)의 P/N 정보가 불일치 합니다 ");
			return -5;
		}
	}
	strQR+=str;

	strQR+=",";
	strQR+=strZPL[LINE_VENDORCODE];
	strQR+=setLotNo();
	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	if (serialIndex >= 20){
		G_AddLog(3, L"LINE_LABEL_INDEX >= 20 Error");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"LINE_LABEL_INDEX >= 20 Error ", BLACK, RED);
		AfxMessageBox(L"LINE_LABEL_INDEX는 20보다 작아야 합니다. label file(.lbl)의 LINE_LABEL_INDEX를 수정해야 합니다. ");
		return -1;
	}
	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	while (str.GetLength()< 4)str.Insert(0, '0');
	updateLabelSerialEnd(rowNo, G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	if (++G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] > 9999)
		G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] = 1;

	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LABEL_SERIAL_0 + serialIndex], str);
	m_editSerialNo.SetWindowText(str);
	strQR+=str;
	nstrQR = strQR.GetLength();
	if (nstrQR == QR_LENGTH || ( serialIndex == 15  && nstrQR == QR_LENGTH2))
	{
		memset(m_partLabel.QRcode[m_partLabel.idxPrinterLabel],0x00,256);
		if(nstrQR < 256){
			memcpy(m_partLabel.QRcode[m_partLabel.idxPrinterLabel], strQR.GetBuffer(), nstrQR*sizeof(wchar_t)); //Unicode
			strQR.ReleaseBuffer();
			strQR.Empty();
			G_AddLog(3,L"PRINT LABEL Serial No.: %d ", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] - 1);
			return m_partLabel.idxPrinterLabel;
		}
		else
			G_AddLog(3,L"nstrQR < 256 fault");
	}

	strQR.Empty();
	return -1;
}

CString CLabelPrint::setLotNo(void)
{
	BOOL bReturn = FALSE;
	CString strValueData;
	//CString strLotNoCode;
	CTime ctime = CTime::GetCurrentTime();
	CString str;
	int serialIndex;

	if (strLotMobis == L"")
	{//Error
		m_staticLotSerial.SetText(L"", BLACK, WHITE);
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(L"");
		G_MainWnd->m_ServerDlg.m_staticLOT.SetText(L"", BLACK, RGB(255, 255, 110));
		m_staticLot.SetText(L"", BLACK, WHITE);
		G_AddLog(3, L"Lot No. Not Set");
		//AfxMessageBox(L"Lot No. Not Set.");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"Lot No. Not Set", BLACK, RED);
		return L"";
	}

	strLotNoCode = strLotMobis;

	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	while(str.GetLength()< 4)str.Insert(0,'0');

	strLotNo = strLotNoCode + str;

	m_staticLotSerial.SetText(strLotNo, BLACK, WHITE);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(strLotNo);
	G_MainWnd->m_ServerDlg.m_staticLOT.SetText(strLotNo, BLACK, RGB(255,255,110));

	return strLotNoCode;
	//Unicode to Multibyte
	//return ConvertUnicodeToMultybyte(strLotNoCode);
}

////////////////////////////////////////////////////////////////////////////////////////////
//CUSTOMER LGE

int CLabelPrint::makeZPLStringLGE(CString model)
{
	CString str;
	CString strQR;
	int qrIndex;
	strQR.Empty();
	ZPLString.Empty();
	int nLine = 0;
	for(nLine = 0;nLine < nZPL; nLine++ )
	{
		switch(nLine){
		case  LINE_LABEL_INDEX:
		case  LINE_PARTNO:
		case  LINE_VENDORCODE:
		case  LINE_LNO_PREFIX:
		case  LINE_LNO_SUFFIX:
		case  LINE_PNO_LBL:

			break;
		case  LINE_PNO_DATA:
			ZPLString+=strZPL[nLine];
			ZPLString+=strZPL[LINE_PARTNO];
			ZPLString+="^FS";
			break;
		case  LINE_LNO_DATA://QR CODE DATA
			ZPLString+=strZPL[LINE_LNO_DATA];
			//QR CODE
			qrIndex = makeQRCodeLGE();
			if (qrIndex == -1)return -1;
			ZPLString +=m_partLabel.QRcode[m_partLabel.idxPrinterLabel];
			ZPLString+="^FS";
			ZPLString+=strZPL[LINE_CODE_DATA];
			ZPLString +=m_partLabel.QRcode[m_partLabel.idxPrinterLabel];
			ZPLString+="^FS";
			if(++m_partLabel.idxPrinterLabel >= QR_LABEL_CNT ) m_partLabel.idxPrinterLabel = 0;
			break;
		case  LINE_CODE_DATA:

			break;
		default:
			ZPLString+=strZPL[nLine];
			break;
		}
	}

	return nLine;
}

int CLabelPrint::makeQRCodeLGE(void)
{
	CString str;
	CString strQR;
	int nstrQR;
	int serialIndex;

	strQR+=strZPL[LINE_VENDORCODE];
	str = setLotNoLGE();
	if (str == L"")return -1;
	strQR += str;
	strQR+="A";
	strQR+=strZPL[LINE_PARTNO].Mid(3,8);

	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	if (serialIndex >= 20){
		G_AddLog(3, L"LINE_LABEL_INDEX >= 20 Error");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"LINE_LABEL_INDEX >= 20 Error ", BLACK, RED);
		AfxMessageBox(L"LINE_LABEL_INDEX는 20보다 작아야 합니다. label file(.lbl)의 LINE_LABEL_INDEX를 수정해야 합니다. ");
		return -1;
	}
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	while(str.GetLength()< 4)str.Insert(0, '0');
	updateLabelSerialEnd(rowNo, G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
//1109_kys
	if (G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] > 9999)
		G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] = 1;
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LABEL_SERIAL_0 + serialIndex], str);
	m_editSerialNo.SetWindowText(str);
	strQR+=str;

	nstrQR = strQR.GetLength();
	if(nstrQR == QR_LENGTH_LGE)
	{
		memset(m_partLabel.QRcode[m_partLabel.idxPrinterLabel],0x00,256);
		if(nstrQR < 256){
			memcpy(m_partLabel.QRcode[m_partLabel.idxPrinterLabel], strQR.GetBuffer(), nstrQR*sizeof(wchar_t));
			strQR.ReleaseBuffer();
			strQR.Empty();
			G_AddLog(3, L"PRINT LABEL Serial No.: %d ", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
			++G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex];
			
			return m_partLabel.idxPrinterLabel;
		}
		else
			G_AddLog(3,L"if(nstrQR < 256) fault ");
	}
	strQR.Empty();
	return -1;
}

CString CLabelPrint::setLotNoLGE(void)
{
	BOOL bReturn = FALSE;
	CString strValueData;
	//CString strLotNoCode;
	CTime ctime = CTime::GetCurrentTime();
	CString str;
	int serialIndex;

	if (strLotLGE == L"")
	{//Error
		m_staticLotSerial.SetText(L"", BLACK, WHITE);
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(L"");
		G_MainWnd->m_ServerDlg.m_staticLOT.SetText(L"", BLACK, RGB(255, 255, 110));
		m_staticLot.SetText(L"", BLACK, WHITE);
		G_AddLog(3, L"Lot No. Not Set");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"Lot No. Not Set", BLACK, RED);
		return L"";
	}
	strLotNoCode = strLotLGE;
	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	while (str.GetLength()< 4)str.Insert(0, '0');
	strLotNo = strLotNoCode + str;
	m_staticLotSerial.SetText(strLotNo, BLACK, WHITE);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(strLotNo);
	G_MainWnd->m_ServerDlg.m_staticLOT.SetText(strLotNo, BLACK, RGB(255,255,110));

	return strLotNoCode;
}

//CUSTOMER MANDO

int CLabelPrint::makeZPLStringMANDO(CString model)
{
	CString str;
	CString strQR;
	int qrIndex;
	strQR.Empty();
	ZPLString.Empty();
	int nLine = 0;
	for (nLine = 0; nLine < nZPL; nLine++)
	{
		switch (nLine){
		case  LINE_LABEL_INDEX:		//0
		case  LINE_PARTNO:				//1
		case  LINE_VENDORCODE:		//2
		case  LINE_LNO_PREFIX:		//3
		case  LINE_LNO_SUFFIX:		//4
		//case  LINE_PNO_LBL:			//9

			break;
		case  LINE_PNO_DATA1:
		case  LINE_PNO_DATA:			//10
			ZPLString += strZPL[nLine];
			ZPLString += strZPL[LINE_PARTNO];
			ZPLString += "^FS";
			break;
		case  LINE_LNO_DATA1:
		case  LINE_LNO_DATA:			//12
			if (nLine == LINE_LNO_DATA)
				ZPLString += strZPL[LINE_LNO_DATA];
			else
				ZPLString += strZPL[LINE_LNO_DATA1];
				//QR CODE
			qrIndex = makeQRCodeMANDO();
			ZPLString += m_partLabel.QRcode[m_partLabel.idxPrinterLabel];
			ZPLString += "^FS";
			if (nLine == LINE_LNO_DATA)
				ZPLString += strZPL[LINE_CODE_DATA];
			else
				ZPLString += strZPL[LINE_CODE_DATA1];
			ZPLString += m_partLabel.QRcode[m_partLabel.idxPrinterLabel];
			ZPLString += "^FS";
			if (++m_partLabel.idxPrinterLabel >= QR_LABEL_CNT)
				m_partLabel.idxPrinterLabel = 0;
			break;
		
		case  LINE_CODE_DATA:		//13
			
			if ((G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)||(m_nManual_Print_Count2 == 0.5) ||
				((m_nManual_Print_Countnow == m_nManual_Print_Count - 1) && (m_nManual_Print_Count2<m_nManual_Print_Count)))
				//Auto Run 상태이거나 인쇄장수가 1장이거나 마지막 인쇄 index 번호가 홀수이면 한장만 인쇄한다.
				nLine = LINE_CODE_DATA1;
			else
				nLine = LINE_CODE_DATA;
			break;

		case  LINE_CODE_DATA1:
			break;
		default:
			ZPLString += strZPL[nLine];
			break;
		}
	}

	return nLine;
}


int CLabelPrint::makeQRCodeMANDO(void)
{
	CString str;
	CString strQR;
	int nstrQR;
	int serialIndex;

	strQR += setLotNoMANDO();
	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	if (G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] > 9999)
		G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] = 1;
	str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	while (str.GetLength()< 3)str.Insert(0, '0');
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LABEL_SERIAL_0 + serialIndex], str);
	m_editSerialNo.SetWindowText(str);
	strQR += str;

	nstrQR = strQR.GetLength();
	if (nstrQR == QR_LENGTH_MANDO)
	{
		memset(m_partLabel.QRcode[m_partLabel.idxPrinterLabel], 0x00, 256);
		if (nstrQR < 256){
			memcpy(m_partLabel.QRcode[m_partLabel.idxPrinterLabel], strQR.GetBuffer(), nstrQR*sizeof(wchar_t));
			strQR.ReleaseBuffer();
			strQR.Empty();
			G_AddLog(3, L"PRINT LABEL Serial No.: %d ", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
			++G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]; //1109_kys
			return m_partLabel.idxPrinterLabel;
		}
		else
			G_AddLog(3, L"if(nstrQR < 256) fault %s, %d ", __FILE__, __LINE__);
	}
	strQR.Empty();
	return -1;
}


CString CLabelPrint::setLotNoMANDO(void)
{
	/////////////////////////////////////////////
	BOOL bReturn = FALSE;
	CString strValueData;
	//CString strLotNoCode;
	CTime ctime = CTime::GetCurrentTime();
	CString str;
	int serialIndex;

	if (strLotMANDO == L"")
	{//Error
		m_staticLotSerial.SetText(L"", BLACK, WHITE);
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(L"");
		G_MainWnd->m_ServerDlg.m_staticLOT.SetText(L"", BLACK, RGB(255, 255, 110));
		m_staticLot.SetText(L"", BLACK, WHITE);
		G_AddLog(3, L"Lot No. Not Set");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"Lot No. Not Set", BLACK, RED);
		return L"";
	}
	strLotNoCode = strLotMANDO;
	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
	str.Format(L"%03d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
	//while (str.GetLength()< 4)str.Insert(0, '0');
	strLotNo = strLotNoCode + str;
	m_staticLotSerial.SetText(strLotNo, BLACK, WHITE);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(strLotNo);
	G_MainWnd->m_ServerDlg.m_staticLOT.SetText(strLotNo, BLACK, RGB(255, 255, 110));

	return strLotNoCode;
}



////////////////////////////////////////////////////////////////////////////////////////////

void CLabelPrint::OnTimer(UINT_PTR nIDEvent)
{
	CString strVal;
	int res;
	int hour;

	switch (nIDEvent)
	{
		case WM_TIMER_SERIAL: //date changes, clear serial no in flash data
			
			if (customer == MOBIS)
			{
				m_scancheck[SCAN_CHECK_OLD] = m_scancheck[SCAN_CHECK_NEW];
				m_QRLENGTH[QRLENGTH_OLD] = m_QRLENGTH[QRLENGTH_NEW];
				m_QRLENGTH[QRLENGTH_NEW] = (int)wcslen(m_partLabel.QRcodeScan);
				if (m_QRLENGTH[QRLENGTH_NEW] != m_QRLENGTH[QRLENGTH_OLD])
				
				{
					m_scancheck[SCAN_CHECK_NEW] = scannerCheck();

					//if ( m_scancheck[SCAN_CHECK_NEW]<0)
					if (m_scancheck[SCAN_CHECK_OLD] != m_scancheck[SCAN_CHECK_NEW])
					{
						if (m_scancheck[SCAN_CHECK_NEW] == -1)
						{
							G_MainWnd->m_PIODlg.writeDO(1, OUT_BUZZER, CARD_IO);
							G_AddLog(3, L"QR코드 자릿수가 다릅니다!!!");
							G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"QR코드 자릿수가 다릅니다!!!", BLACK, RED);
							AfxMessageBox(L"QR코드 자릿수가 다릅니다!!!(-1)");
						}

						if (m_scancheck[SCAN_CHECK_NEW] == -2)
						{
							G_MainWnd->m_PIODlg.writeDO(1, OUT_BUZZER, CARD_IO);
							G_AddLog(3, L"lbl 파일양식 '-' 확인하세요");
							G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"lbl 파일양식 '-' 확인하세요", BLACK, RED);
							AfxMessageBox(L"lbl 파일양식 '-' 확인하세요(-2)");
						}
						if (m_scancheck[SCAN_CHECK_NEW] == -3)
						{
							G_MainWnd->m_PIODlg.writeDO(1, OUT_BUZZER, CARD_IO);
							G_AddLog(3, L"이 모델의 Part No가 아닙니다");
							G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"이 모델의 Part No가 아닙니다", BLACK, RED);
							AfxMessageBox(L"이 모델의 Part No가 아닙니다(-3)");
						}
						if (m_scancheck[SCAN_CHECK_NEW] == -4)
						{
							G_MainWnd->m_PIODlg.writeDO(1, OUT_BUZZER, CARD_IO);
							G_AddLog(3, L"QR코드 자릿수나 P/N가 다릅니다");
							G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"QR코드 자릿수나 P/N가 다릅니다", BLACK, RED);
							AfxMessageBox(L"QR코드 자릿수나 P/N가 다릅니다(-4)");
						}
						if (m_scancheck[SCAN_CHECK_NEW] == 0)
						{
							//G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, WHITE);
							G_MainWnd->m_PIODlg.writeDO(0, OUT_BUZZER, CARD_IO);
							G_MainWnd->m_PIODlg.buzzstop = 0;
						}
					}
				}
			}
			else if(customer == LGE)
				scannerCheckLGE();
			else if (customer == MANDO)
				scannerCheckMANDO();
			break;

		case 	WM_TIMER_LABLE: //delete lot no. data before 1 month 
			CTime ctime = CTime::GetCurrentTime();
			hour = ctime.GetHour();
			CTimeSpan tspan(180, 0, 0, 0); //6 months
			CTime ctime2 = ctime - tspan;
			CString str2 = ctime2.Format("%Y-%m-%d %H:%M:%S");
			char * date2 = ConvertUnicodeToMultybyte(str2);

			//Delete old records
			//res = sqlite3_prepare_v2(db, "DELETE FROM LABLE WHERE DATE < datetime(?1);", -1, &stmtDeleteLable, NULL);
			res = sqlite3_bind_text(stmtDeleteLable, 1, date2, -1, SQLITE_STATIC);
			res = sqlite3_step(stmtDeleteLable);
			if (res == SQLITE_DONE){
				G_AddLog(3, L"Delete query done");
			}
			sqlite3_reset(stmtDeleteLable);

			//Insert a new row every one hour




			break;
	}
	CDialogEx::OnTimer(nIDEvent);

}

int CLabelPrint::scannerCheck(void)
{
	CString str, str1, str2,str3;
	int head;
	int qrLength=0;
	int i;
	int index;
	m_csLabel.Lock();
	qrLength = (int)wcslen(m_partLabel.QRcodeScan);

	//1.QR코드의 자릿수 확인
	if(qrLength == QR_LENGTH)
	{
		index = strZPL[LINE_PARTNO].Find('-');
		
		//2.'-' 확인
		if (index != -1) //found	
		{
			str2 = strZPL[LINE_PARTNO];
			int n = str2.Delete(index, 1);
			str = m_partLabel.QRcodeScan;
			int f= str.Find(str2);


			//3.MOBIS P/N 10자리 확인(equal이면,,,현재  선택된 모델의 lbl.ini 파일의 MOBIS P/N과 SCAN된 P/N중 앞 10자리 P/N만 찾는다.)  
			if (f >=0)										
			{	
				head = str.Find(strZPL[LINE_PARTNO].Left(5));
								
				if (head >= 0 && qrLength >= (head + QR_LENGTH))//4.SCAN해서 P/N 앞 5자리 찾고 자릿수가 MOBIS 양식(22자리 이상)을 만족한다.
				{		
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, WHITE);
					str1 = str.Mid(head, QR_LENGTH);
											for (i = 0; i< QR_LABEL_CNT; i++)
												{
													if (wcsncmp(str1.GetBuffer(), m_partLabel.QRcode[i], QR_LENGTH) == 0) //5.SCAN 데이터와 QRcode 저장 배열데이터를 비교한다.
													{
														G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(m_partLabel.QRcode[i], BLACK, RGB(230, 230, 230));
														memset(m_partLabel.QRcode[i], 0x00, 256 * sizeof(wchar_t));
														if (m_partLabel.idxPrinterLabel > 0)                                                //6.makeZPLStringMOBIS에서 한번이상 인쇄
														{
															m_partLabel.idxPrinterLabel--;
															break;
														}
														else{};//6.인쇄를 안했다. 그냥 나간다.
													}
													else{};                                    //5.
												}
											if (i<QR_LABEL_CNT){}
											else{G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(str1, BLACK, RED);}
											str1.ReleaseBuffer();
											memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(char));
											if (qrLength == (head + QR_LENGTH)){}
											else{	str1 = str.Mid(head + QR_LENGTH, (qrLength - head - QR_LENGTH));
													if (str1.GetLength() < 256)
														{memcpy(m_partLabel.QRcodeScan, str1.GetBuffer(), str1.GetLength()*sizeof(wchar_t));
														str1.ReleaseBuffer();
														}
													else
														G_AddLog(3, L"if(nstrQR < 256) fault");
												}
				}
				else{return -4;}//4.
				if (qrLength > 200)
					memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(wchar_t));
			}
			else//3.
				{	
				memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(wchar_t));
				return -3;
				}
		}
		else//2.
			{return -2;}	
	}
	else //1.
		{
		
			if ((qrLength == QR_LENGTH_MANDO) || (qrLength == QR_LENGTH_LGE))
				{memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(wchar_t));
					return -1;
				}
			else
					memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(wchar_t));
		}
	m_csLabel.Unlock();
	return 0;
}

int CLabelPrint::scannerCheckLGE(void)
{
	CString str, str1;
	int head;
	int qrLength;
	int i;

	m_csLabel.Lock();
	qrLength = (int)wcslen(m_partLabel.QRcodeScan);
	if(qrLength >= QR_LENGTH_LGE) 
	{
		str = m_partLabel.QRcodeScan;
		head = str.Find(strZPL[LINE_VENDORCODE].Left(2));
		if(head>= 0 && qrLength >= (head + QR_LENGTH_LGE)){
			str1 = str.Mid(head,QR_LENGTH_LGE);
			for(i = 0 ; i< QR_LABEL_CNT; i++){
				if(wcsncmp(str1.GetBuffer(),m_partLabel.QRcode[i],QR_LENGTH_LGE) == 0 )
				{//found
					G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(m_partLabel.QRcode[i], BLACK, RGB(230,230,230));
					memset(m_partLabel.QRcode[i], 0x00, 256 * sizeof(wchar_t));
					if(m_partLabel.idxPrinterLabel > 0)//
						m_partLabel.idxPrinterLabel--;
					break;
				}
			}
			if(i<QR_LABEL_CNT){

			}else{
				G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(str1, BLACK, RED);
			}
			str1.ReleaseBuffer();
			memset(m_partLabel.QRcodeScan, 0x00,256*sizeof(char));
			if(qrLength == (head + QR_LENGTH_LGE))
			{
			}else{
				str1 = str.Mid(head+QR_LENGTH,(qrLength -head - QR_LENGTH));
				if(str1.GetLength() < 256){
					memcpy(m_partLabel.QRcodeScan, str1.GetBuffer(), str1.GetLength()*sizeof(wchar_t));
					str1.ReleaseBuffer();
				}else{
					G_AddLog(3,L"if(str1.GetLength() < 256) fail  ");
				}
			}
		}

		if(qrLength > 200)//
			memset(m_partLabel.QRcodeScan, 0x00,256*sizeof(char));
	}
	m_csLabel.Unlock();

	return 0;
}

int CLabelPrint::scannerCheckMANDO(void)
{
	CString str, str1;
	int head;
	int qrLength;
	int i;

	m_csLabel.Lock();
	qrLength = (int)wcslen(m_partLabel.QRcodeScan);
	if (qrLength >= QR_LENGTH_MANDO)
	{
		str = m_partLabel.QRcodeScan;
		head = str.Find(strZPL[LINE_VENDORCODE].Left(4));
		if (head >= 0 && qrLength >= (head + QR_LENGTH_MANDO)){
			str1 = str.Mid(head, QR_LENGTH_MANDO);
			for (i = 0; i< QR_LABEL_CNT; i++){
				if (wcsncmp(str1.GetBuffer(), m_partLabel.QRcode[i], QR_LENGTH_MANDO) == 0)
				{//found
					G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(m_partLabel.QRcode[i], BLACK, RGB(230, 230, 230));
					memset(m_partLabel.QRcode[i], 0x00, 256 * sizeof(wchar_t));
					if (m_partLabel.idxPrinterLabel > 0)//
						m_partLabel.idxPrinterLabel--;
					break;
				}
			}
			if (i<QR_LABEL_CNT){

			}
			else{
				G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(str1, BLACK, RED);
			}
			str1.ReleaseBuffer();
			memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(char));
			if (qrLength == (head + QR_LENGTH_MANDO))
			{
			}
			else{
				str1 = str.Mid(head + QR_LENGTH, (qrLength - head - QR_LENGTH));
				if (str1.GetLength() < 256){
					memcpy(m_partLabel.QRcodeScan, str1.GetBuffer(), str1.GetLength()*sizeof(wchar_t));
					str1.ReleaseBuffer();
				}
				else{
					G_AddLog(3, L"if(str1.GetLength() < 256) fail  ");
				}
			}
		}

		if (qrLength > 200)//
			memset(m_partLabel.QRcodeScan, 0x00, 256 * sizeof(char));
	}
	m_csLabel.Unlock();

	return 0;
}

//void CLabelPrint::OnBnClickedCheck()
//{
		//int iIsCheck = 0;
		//CString model;
		//iIsCheck = IsDlgButtonChecked(IDC_CHECK_COMP_FILTER);
		//if(iIsCheck){//LGE-GM
		//	customer = LGE;
		//	model = G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel;
		//	model += "_LGE";
		//	setModel(model);
		//}else{//
		//	customer = MOBIS;
		//	setModel(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		//}
//}

//Set Serial No.
void CLabelPrint::OnBnClickedBtnSerialnoSet()
{
	int serialIndex;
	int serialNo;
	CString str;
	serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);

	GetDlgItem(IDC_EDIT_LABEL_SERIAL)->GetWindowText(str);
	serialNo = _wtoi(str);

	if (serialNo < 1 || serialNo > 9999){
		AfxMessageBox(L"serialNo Range:  1 ~ 9999", MB_ICONHAND);
		return;
	}

	//check if serial no. is duplicated
	int no = 0;
	int res;
	int nSerBegin = 0, nSerEnd = 0;
	//CString strLotNo;

	char PNO[128];
	char LOT[128];

	/*if (customer == MOBIS){
		strLotNo = strLotMobis;
	}
	else if (customer == LGE)
	{
		strLotNo = strLotLGE;
	}
	else if (customer == MANDO)
	{
		strLotNo = strLotMANDO;
	}*/

	

	//res = sqlite3_prepare_v2(db, "SELECT * FROM LABLE WHERE PNO = ?1 AND LOT = ?2;", -1, &stmtSelectLable, NULL);
	strcpy(PNO, ConvertUnicodeToMultybyte(strZPL[LINE_PARTNO]));
	strcpy(LOT, ConvertUnicodeToMultybyte(strLotNo));

	res = sqlite3_bind_text(stmtSelectLable, 1, PNO, -1, SQLITE_STATIC);
	res = sqlite3_bind_text(stmtSelectLable, 2, LOT, -1, SQLITE_STATIC);
	res = sqlite3_step(stmtSelectLable);
	if (res == SQLITE_ROW){
		no = sqlite3_column_int(stmtSelectLable, 0);
		nSerBegin = sqlite3_column_int(stmtSelectLable, 3);
		nSerEnd = sqlite3_column_int(stmtSelectLable, 4);
		if (nSerBegin <= serialNo && serialNo <= nSerEnd){
			AfxMessageBox(L"Serial No. used already.", MB_ICONHAND);

			return;
		}
	}

	G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] = serialNo;
	//1109_KYS
	//G_MainWnd->m_LabelPrint.setModel(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	//////////1111_kys
	if (customer == MOBIS)
		setLotNo();
	else if (customer == LGE)
		setLotNoLGE();
	else if (customer == MANDO)
		setLotNoMANDO();
	memset(m_LabelPrinterCheck, 0x0, sizeof(m_LabelPrinterCheck));
}


void CLabelPrint::OnBnClickedCheckLotNoSet()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

int CLabelPrint::sqliteInit()
{
	int res;
	res = sqlite3_open("D:\\nBizData\\CPK\\EXE\\OBC1.s3db", &db);

	//prepare prestatement
	res = sqlite3_prepare_v2(db, "INSERT INTO LABLE(PNO, LOT, SERIALBEGIN, SERIALEND, DATE) VALUES(?1, ?2, ?3, ?4, ?5);", -1, &stmtInsertLable, NULL);
	res = sqlite3_prepare_v2(db, "SELECT * FROM LABLE WHERE PNO = ?1 AND LOT = ?2;", -1, &stmtSelectLable, NULL);
	res = sqlite3_prepare_v2(db, "SELECT * FROM LABLE WHERE LOT = ?1 AND SERIALBEGIN <= ?2 AND SERIALEND >= ?3;", -1, &stmtSelectLable2, NULL);
	res = sqlite3_prepare_v2(db, "UPDATE LABLE SET SERIALEND = ?1 WHERE NO = ?2;", -1, &stmtUpdateLable, NULL);
	res = sqlite3_prepare_v2(db, "DELETE FROM LABLE WHERE DATE < datetime(?1);", -1, &stmtDeleteLable, NULL);

	return 0;
}


void CLabelPrint::OnBnClickedBtnLotNoSet()
{
	CTime ctime;
	int year, month, day;
	CString sYear;
	//1111_kys- member val use
	//CString strLotNo;

	wchar_t chYear;
	wchar_t chMonth;
	wchar_t chDay;

	GetDlgItem(IDC_EDIT_LOTNO_YEAR)->GetWindowText(sYear);
	if (sYear == L""){ year = ctime.GetYear(); }
	else { year = _wtoi(sYear); }

	month = m_cmbLotMonth.GetCurSel() + 1;
	day = m_cmbLotDay.GetCurSel() + 1;

	if (customer == MOBIS)
	{
		//Mobis
		if (day < 10)
			chDay = '0' + day;
		else{
			chDay = 'A' + day - 10;
		}
		//1226_KYS
		if (year < 2010)
			chYear = '0' + year;
		else{
			chYear = 'A' + (year-2010);
		}
		//
		
		
		//strLotMobis.Format(L"%X%X%c", (year - 2000) % 16, month, chDay);
		strLotMobis.Format(L"%c%X%c",chYear, month, chDay);
		strLotNo = strLotMobis;
	}
	else if (customer == LGE){
		//LGE
		year = year - 2010;
		if (year < 10)
			chYear = '0' + year;
		else{
			chYear = 'A' + year - 10;
		}
		switch (month){
		case 10: chMonth = 'O';	break;
		case 11: chMonth = 'N';	break;
		case 12: chMonth = 'D';	break;
		default: 	chMonth = '0' + month; break;
		}
		if (day < 10)
			chDay = '0' + day;
		else{
			chDay = 'A' + day - 10;
		}
		strLotLGE.Format(L"%c%c%c", chYear, chMonth, chDay);
		strLotNo = strLotLGE;
	}

	else if (customer == MANDO)
	{
		//MANDO
		strLotMANDO.Format(L"TQ72-%02d%02d%02d", year - 2000, month, day);
		strLotNo = strLotMANDO;
	}
	else {
		G_AddLog(3, L"Cumstor is neither 'MOBIS' nor 'LGE'. Lot No. was not set. ");
	}

	m_staticLot.SetText(strLotNo, BLACK, WHITE);
	G_AddLog(3, L"Lot No. Set: %s", strLotNo);

	//Lot No. 중복 검사
	int dbRowNo = 0;
	if (strLotNo != L""){
		dbRowNo = m_fnCheckLotNoDB(strLotNo);
	}
	else{
		AfxMessageBox(L"Lot No. Not Set");
	}
	//////////1111_kys
	if (customer == MOBIS)
		setLotNo();
	else if (customer == LGE)
		setLotNoLGE();
	else if (customer == MANDO)
		setLotNoMANDO();
}


int CLabelPrint::m_fnCheckLotNoDB(CString strLotNo)
{
	//Lot No. 중복 검사
	//Select
	int no = 0;
	int res;
	CString strBuff;
	int nSerBegin = 0, nSerEnd = 0;
	//char *PNO, *LOT, *DATE;

	char PNO[128];
	char LOT[128];
	char DATE[128];

	//res = sqlite3_prepare_v2(db, "SELECT * FROM LABLE WHERE PNO = ?1 AND LOT = ?2;", -1, &stmtSelectLable, NULL);
	strcpy(PNO, ConvertUnicodeToMultybyte(strZPL[LINE_PARTNO]));
	strcpy(LOT, ConvertUnicodeToMultybyte(strLotNo));

	//std::string strPNO(PNO);
	//std::string strLOT(LOT);

	res = sqlite3_bind_text(stmtSelectLable, 1, PNO, -1, SQLITE_STATIC);
	res = sqlite3_bind_text(stmtSelectLable, 2, LOT, -1, SQLITE_STATIC);
	res = sqlite3_step(stmtSelectLable);
	if (res == SQLITE_ROW){
		no = sqlite3_column_int(stmtSelectLable, 0);
		nSerBegin = sqlite3_column_int(stmtSelectLable, 3);
		nSerEnd = sqlite3_column_int(stmtSelectLable, 4);
	}

	sqlite3_reset(stmtSelectLable);

	if (res == SQLITE_DONE ){ //Insert new row
		//insert
		int serialBegin = 1;
		int serialEnd = 1;
		CTime ctime = CTime::GetCurrentTime();
		CString str = ctime.Format("%Y-%m-%d %H:%M:%S");
		strcpy(DATE, ConvertUnicodeToMultybyte(str));
		strcpy(PNO, ConvertUnicodeToMultybyte(strZPL[LINE_PARTNO]));
		strcpy(LOT, ConvertUnicodeToMultybyte(strLotNo));

		res = sqlite3_bind_text(stmtInsertLable, 1, PNO, -1, SQLITE_STATIC);
		res = sqlite3_bind_text(stmtInsertLable, 2, LOT, -1, SQLITE_STATIC);
		res = sqlite3_bind_int(stmtInsertLable, 3, serialBegin);
		res = sqlite3_bind_int(stmtInsertLable, 4, serialEnd);
		res = sqlite3_bind_text(stmtInsertLable, 5, DATE, -1, SQLITE_STATIC);

		res = sqlite3_step(stmtInsertLable);
		if (res == SQLITE_DONE){
			strBuff.Format(L"Lot No. %s Added", strLotNo);
			G_AddLog(3, L"Lot No. %s Added", strBuff);
			MessageBox(strBuff);

			strcpy(PNO, ConvertUnicodeToMultybyte(strZPL[LINE_PARTNO]));
			strcpy(LOT, ConvertUnicodeToMultybyte(strLotNo));

			//res = sqlite3_prepare_v2(db, "SELECT * FROM LABLE WHERE PNO = ?1 AND LOT = ?2;", -1, &stmtSelectLable, NULL);
			res = sqlite3_bind_text(stmtSelectLable, 1, PNO, -1, SQLITE_STATIC);
			res = sqlite3_bind_text(stmtSelectLable, 2, LOT, -1, SQLITE_STATIC);
			res = sqlite3_step(stmtSelectLable);
			if (res == SQLITE_ROW){
				no = sqlite3_column_int(stmtSelectLable, 0);
				nSerBegin = sqlite3_column_int(stmtSelectLable, 3);
				nSerEnd = sqlite3_column_int(stmtSelectLable, 4);
			}
			sqlite3_reset(stmtSelectLable);
		}
		sqlite3_reset(stmtInsertLable);
	}

	if (no > 0 && nSerEnd > 0){
		if (nSerEnd > 1)
			nSerEnd++;
		strBuff.Format(L"%d", nSerEnd);
		if ((customer == MOBIS) || (customer == LGE))
		{
			while (strBuff.GetLength()< 4)strBuff.Insert(0, '0');
		}
		else
		{
			while (strBuff.GetLength()< 3)strBuff.Insert(0, '0');
		}
		
		m_editSerialNo.SetWindowText(strBuff);
		int serialIndex;

		CString str = strLotNo + strBuff;

		m_staticLotSerial.SetText(str, BLACK, WHITE);
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT5)->SetWindowText(str);
		G_MainWnd->m_ServerDlg.m_staticLOT.SetText(str, BLACK, RGB(255, 255, 110));

		serialIndex = _wtoi(strZPL[LINE_LABEL_INDEX]);
		G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] = nSerEnd;
		G_AddLog(3, L" Serial No Set: %d", nSerBegin, nSerEnd - 1, nSerEnd);
		rowNo = no;
		G_AddLog(3, L" Row No. in LABLE table : %d", rowNo);
	}

	return rowNo;
}


int CLabelPrint::updateLabelSerialEnd(int rowNo, int serialEnd)
{
	int res;
	//update
	//res = sqlite3_prepare_v2(db, "UPDATE LABLE SET SERIALEND = ?1 WHERE NO = ?2;", -1, &stmtUpdateLable, NULL);
	res = sqlite3_bind_int(stmtUpdateLable, 1, serialEnd);
	res = sqlite3_bind_int(stmtUpdateLable, 2, rowNo);
	res = sqlite3_step(stmtUpdateLable);
	if (res == SQLITE_DONE){
		//G_AddLog(3, L"Update Label Serial No. in DB Done");
	}
	else{
		G_AddLog(3, L"Update Label Serial No. in DB Failed");
	}
	sqlite3_reset(stmtUpdateLable);
	return res;
}


void CLabelPrint::OnBnClickedButton1()
{
	m_partLabel.idxPrinterLabel = 0;
	wchar_t step[5];
	swprintf_s(step, L"%d", m_partLabel.idxPrinterLabel);
	m_btnScanCountReset.SetWindowText(step);
}
