#include "stdAfx.h"
#include "InterfaceSocket.h"
#include "nBizLDCTester.h"
#include "sys/stat.h"

IMPLEMENT_DYNAMIC(CInterfaceSocket, CWnd)

#define TIMER_RECONNECT		5000
#define TIMER_RECONNECT2		5001

extern CMainWnd* G_MainWnd;


CInterfaceSocket::CInterfaceSocket(void)
{
	// 클래스 생성 **
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	wndclass.style			= CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= AfxWndProc;
	wndclass.hInstance		= AfxGetInstanceHandle();
	wndclass.hIcon			= NULL;
	wndclass.hCursor		= AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName	= NULL;
	wndclass.lpszClassName	= L"CommSocket";

	AfxRegisterClass(&wndclass);

	CreateEx(NULL,L"CommSocket", NULL, NULL, CRect(0,0,0,0), NULL, NULL, NULL);	
	// 클래스 생성 ##	

	m_ClientSocket[0].InitSocket(this);
	m_ClientSocket[1].InitSocket(this);
	memset(m_chServerIP, 0x00, 50);
	memset(m_chServerPortNum, 0x00, 50);	

	//m_cimInfodlg.m_fnInit(this);
	
	InitializeCriticalSection(&m_CriSend);

	m_nSeqSetp = 0;	
}

CInterfaceSocket::~CInterfaceSocket(void)
{
	DeleteCriticalSection(&m_CriSend);	
	m_ClientSocket[0].Close();
	m_ClientSocket[1].Close();
	KillTimer(TIMER_RECONNECT);
	KillTimer(TIMER_RECONNECT2);
	m_vecReceiveData.clear();
}


BEGIN_MESSAGE_MAP(CInterfaceSocket, CWnd)
	ON_WM_TIMER()
	ON_MESSAGE(UM_CIM_RECEIVE, OnClientReceive)	// Client로 부터 수신되는 데이터
	ON_MESSAGE(UM_CIM_CLOSE, OnClientReConnect)	// Client로 부터 수신되는 데이터
	ON_MESSAGE(UM_CIM_CONNECT_OK, OnConnectOK)	// Client로 부터 수신되는 데이터

END_MESSAGE_MAP()


void CInterfaceSocket::m_fnInitSocket()
{
	CString strValueData;

	//strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SCANNER_IP1);	
	//strcpy_s(m_chServerIP[0], (LPCTSTR)strValueData);

	//strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SCANNER_IP2);	
	//strcpy_s(m_chServerIP[1], (LPCTSTR)strValueData);

	//BOOL  bRes1 = FALSE, bRes2 = FALSE;
	//bRes1 = m_ClientSocket[0].Create();
	//bRes2 = m_ClientSocket[1].Create();
	//if(bRes1&&bRes2)
	//{
	//	m_ClientSocket[0].id = 0;
	//	m_ClientSocket[1].id = 1;
	//}
	//else
	//{
	//	return;
	//}

	//SetTimer(TIMER_RECONNECT, 500, NULL);	
	//SetTimer(TIMER_RECONNECT2, 500, NULL);
	CSequenceManager::m_fnStartScenario(TRUE, FALSE);
}

void CInterfaceSocket::m_fnSendToEqpReply(BYTE nCmd, int nWhere)
{
	CString str;
	if(nCmd == CMD_ACK)
		str.Format(L"%x%s", nWhere,L"ACK");
	else if(nCmd == CMD_NAK)
		str.Format(L"%x%s", nWhere,L"NAK");

	m_fnSendToEqp(nCmd, str);
}

void CInterfaceSocket::m_fnSendToEqp(BYTE nCmd, CString strData)
{
	int nCnt = 0;

	m_SendData.nSTX = CMD_STX;
	m_SendData.nCMD = nCmd;
	memcpy(m_SendData.chData, strData.GetBuffer(), sizeof(m_SendData.chData));
	strData.ReleaseBuffer();//2014.08.19 jec

	m_SendData.nETX = CMD_ETX;

	EnterCriticalSection(&m_CriSend);
	//m_ClientSocket.Send((char*)&m_SendData, sizeof(EqpSendData));	
	LeaveCriticalSection(&m_CriSend);
}

void CInterfaceSocket::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == TIMER_RECONNECT)
	{
		m_ClientSocket[0].Connect(m_chServerIP[0], PORT_SCANNER);

		if(m_bConnect[0] == TRUE)
		{
			G_AddLog(3,L"Scanner 1 Connected");
			KillTimer(TIMER_RECONNECT);
		}
	}
	else if(nIDEvent == TIMER_RECONNECT2)
	{
		m_ClientSocket[1].Connect(m_chServerIP[1], PORT_SCANNER);

		if(m_bConnect[1] == TRUE)
		{
			G_AddLog(3,L"Scanner 2 Connected");
			KillTimer(TIMER_RECONNECT2);
		}
	}

	CWnd::OnTimer(nIDEvent);
}



LRESULT CInterfaceSocket::OnClientReceive(WPARAM wParam, LPARAM lParam)
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;

	if(G_SystemModeData.unSystemMode == SYSTEM_EXIT|| pMainWnd == NULL)
		return 0;

	int nReceiveSize = (int)wParam;	

	if(nReceiveSize <= 0 || nReceiveSize >= 4096)
	{
		G_AddLog(3,L"Return Receive Size over %d", nReceiveSize);

		return 0;
	}

	//memcpy(G_MainWnd->m_InspectThread.m_PartData[LOAD].QRcode,(void*)lParam, nReceiveSize);

	return 0;
	}


void CInterfaceSocket::m_fnDataReset()
{
	m_vecReceiveData.clear();
}

LRESULT CInterfaceSocket::OnClientReConnect(WPARAM wParam, LPARAM lParam)
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	int id = (int)lParam;
	//pMainWnd->m_ServerDlg.m_staConnectCim.SetBackTextColor(UI_OFF_BACK, UI_OFF_FONT);
	G_AddLog(3,L"Server 접속 종료");
	m_bConnect[id] = FALSE;

	BOOL  bRes = FALSE;
	
	m_ClientSocket[id].Close();
	bRes = m_ClientSocket[id].Create();

	if(bRes)
	{
		G_AddLog(3,L"Scanner socket &d recreated", id);
	}
	else
	{
		G_AddLog(3,L"Scanner socket &d recreate failed", id);
		return 0;
	}
	if(id==0){
		KillTimer(TIMER_RECONNECT);	
		SetTimer(TIMER_RECONNECT, 500, NULL);
	}else{
		KillTimer(TIMER_RECONNECT2);	
		SetTimer(TIMER_RECONNECT2, 500, NULL);
	}

	return 0;
}



LRESULT CInterfaceSocket::OnConnectOK(WPARAM wParam, LPARAM lParam)
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;

	int nErrorCode = (int)wParam;
	int id = (int)lParam;

	if(nErrorCode == 0)	
	{
		//pMainWnd->m_ServerDlg.m_staConnectCim.SetBackTextColor(UI_ON_BACK, UI_ON_FONT);
		m_bConnect[id] = TRUE;
	}
	else
	{
		//G_AddLog(3,L"%d EQP Connect Error", nErrorCode);

		//pMainWnd->m_ServerDlg.m_staConnectCim.SetBackTextColor(UI_OFF_BACK, UI_OFF_FONT);
		m_bConnect[id] = FALSE;
	}
	return 0;
}

void CInterfaceSocket::m_fnGetReceiveDataTime(int nWhere)
{
	CString strTempTime;
	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();
	strTempTime = ctCurrentTime.Format(L"%Y/%m/%d/%H:%M:%S");

	memcpy(m_ReceiveTimeData.chReceiveTime, strTempTime, strTempTime.GetLength());
	m_ReceiveTimeData.nID = nWhere;

	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TIME_UPDATE, (LPARAM)&m_ReceiveTimeData);

}

void CInterfaceSocket::m_fnCheck()  // 검사 Check Thread
{


}




bool CInterfaceSocket::ProcessKill(CString strProcessName)
{
	HANDLE         hProcessSnap = NULL; 
	BOOL           bRet      = FALSE; 
	PROCESSENTRY32 pe32      = {0}; 

	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

	if (hProcessSnap == (HANDLE)-1) 
		return false;

	pe32.dwSize = sizeof(PROCESSENTRY32); 

	//프로세스가 메모리상에 있으면 첫번째 프로세스를 얻는다

	if (Process32First(hProcessSnap, &pe32)) 
	{ 
		BOOL          bCurrent = FALSE; 
		MODULEENTRY32 me32       = {0}; 

		do 
		{ 
			bCurrent = GetProcessModule(pe32.th32ProcessID,strProcessName);

			if(bCurrent) 
			{ 
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID); 
				if(hProcess)
				{
					if(TerminateProcess(hProcess, 0))
					{
						unsigned long nCode; //프로세스 종료 상태 
						GetExitCodeProcess(hProcess, &nCode);
					}
					CloseHandle(hProcess); 
				}
			} 
		} 

		while (Process32Next(hProcessSnap, &pe32)); //다음 프로세스의 정보를 구하여 있으면 루프를 돈다.

	} 

	CloseHandle (hProcessSnap); 

	return true;
}

bool CInterfaceSocket::GetProcessModule(DWORD dwPID,CString sProcessName)

{ 
	HANDLE        hModuleSnap = NULL; 

	MODULEENTRY32 me32        = {0}; 

	hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwPID); 
	if (hModuleSnap == (HANDLE)-1) 
		return (FALSE); 

	me32.dwSize = sizeof(MODULEENTRY32); 

	//해당 프로세스의 모듈리스트를 루프로 돌려서 프로세스이름과 동일하면 

	//true를 리턴한다.

	if(Module32First(hModuleSnap, &me32)) 
	{ 
		do 
		{ 
			if(me32.szModule == sProcessName)
			{ 
				CloseHandle (hModuleSnap); 
				return true;
			} 
		} 

		while(Module32Next(hModuleSnap, &me32)); 
	} 
	CloseHandle (hModuleSnap); 
	return false;
} 