
// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// nBizLDCTester.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"
G_SystemModeStruct G_SystemModeData;


char* ConvertUnicodeToMultybyte(CString strUnicode)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
	char* pMultibyte = new char[nLen];
	memset(pMultibyte, 0x00, (nLen)*sizeof(char));
	if (nLen > 510)nLen = 510;
	WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, pMultibyte, nLen, NULL, NULL);
	strcpy(chResult, pMultibyte);
	delete[] pMultibyte;
	return chResult;
	return pMultibyte;
}


wchar_t* ConvertMultybyteToUnicode(const char *chText)
{
	wchar_t* pszTmp = NULL; 
	int iLen = ::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, 0);
	if (iLen > 510)iLen = 510;
	pszTmp = new wchar_t[iLen + 1];
	::MultiByteToWideChar(CP_ACP, 0, chText, -1, pszTmp, iLen);
	wcscpy(wcResult, pszTmp);
	delete[] pszTmp;
	return wcResult;
	return pszTmp;
}


void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect)
{
	BITMAPINFO bitmapInfo;
	bitmapInfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
	bitmapInfo.bmiHeader.biPlanes=1;
	bitmapInfo.bmiHeader.biCompression=BI_RGB;
	bitmapInfo.bmiHeader.biXPelsPerMeter=100;
	bitmapInfo.bmiHeader.biYPelsPerMeter=100;
	bitmapInfo.bmiHeader.biClrUsed=0;
	bitmapInfo.bmiHeader.biClrImportant=0;
	bitmapInfo.bmiHeader.biSizeImage=0;
	bitmapInfo.bmiHeader.biWidth=pImgIpl->width;
	bitmapInfo.bmiHeader.biHeight=-pImgIpl->height;
	IplImage* tempImage;
	if(pImgIpl->nChannels == 3)
	{
		tempImage = (IplImage*)cvClone(pImgIpl);
		bitmapInfo.bmiHeader.biBitCount=tempImage->depth * tempImage->nChannels;
	}
	else if(pImgIpl->nChannels == 1)
	{
		tempImage =  cvCreateImage(cvGetSize(pImgIpl), IPL_DEPTH_8U, 3);
		cvCvtColor(pImgIpl, tempImage, CV_GRAY2BGR);
		bitmapInfo.bmiHeader.biBitCount=tempImage->depth * tempImage->nChannels;
	}
	pDC->SetStretchBltMode(COLORONCOLOR);
	::StretchDIBits(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 
		0, 0, tempImage->width, tempImage->height, tempImage->imageData, &bitmapInfo, 
		DIB_RGB_COLORS, SRCCOPY);
	cvReleaseImage(&tempImage);
}
