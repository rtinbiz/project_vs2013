#include "stdafx.h"
#include "InspectThread.h"
#include "MainWnd.h"


extern CMainWnd	*G_MainWnd;

CInspectThread::CInspectThread(void)
{
	m_nSeqMode = 0;
	m_nScanCount = 1;
	checkStatus = 0;
	suspendCount = 0;
	suspendCount_Input = 0;
	fLCRLoadCorrect = 0;
	fDCRZeroAdjust = 0;
}

CInspectThread::~CInspectThread(void)
{
	
}


BEGIN_MESSAGE_MAP(CInspectThread, CWnd)
	ON_WM_TIMER()
	ON_WM_CREATE()
END_MESSAGE_MAP()

int CInspectThread::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//SetTimer(WM_TIMER_100m, 100, NULL);
	//SetTimer(WM_TIMER_1S, 1000, NULL);
	return 0;
}

int CInspectThread::m_fnInit()
{
	return CSequenceManager::m_fnStartScenario(TRUE, FALSE);
}

int CInspectThread::m_fnInit(CDataHandling* pDataHandling, 
	CSerialInterface		*pSerialInterface, 
	CRecipeSetDlg			*pRecipeSetDlg,
	CInstrument			*pInstrumentDlg,
	PIO						*pPIODlg
	//AxisMotion				*pAxisDlg
	)
{
	m_pDataHandling = pDataHandling;
	m_pSerialInterface = pSerialInterface;
	m_pRecipeSetDlg = pRecipeSetDlg ;	
	m_pInstrumentDlg = pInstrumentDlg ;	
	m_pPIO = pPIODlg ;	
	//m_pAxis = pAxisDlg ;	

	G_MainWnd->m_AxisDlg.m_fnInit();
	CSequenceManager::m_fnStartMotionThread(TRUE);
	m_vi.m_fnInit();

	memset(m_nCurStep, 0x00, sizeof(int)*NO_BATCH_SQC);
	memset(m_nPrevStep, 0x00, sizeof(int)*NO_BATCH_SQC);
	memset(m_nOldStep, 0x00, sizeof(int)*NO_BATCH_SQC);

	memset(&stLoadBatch,0x00, sizeof(LOAD_BATCH)); 
	memset(&stLCRBatch,0x00, sizeof(LCR_BATCH)); 
	memset(&stOSCBatch,0x00, sizeof(OSC_BATCH)); 
	memset(&stDCRBatch,0x00, sizeof(DCR_BATCH)); 
	memset(&stHIPOTBatch,0x00, sizeof(HIPOT_BATCH)); 
	memset(&stIRBatch,0x00, sizeof(IR_BATCH)); 
	memset(&stVIBatch,0x00, sizeof(VI_BATCH)); 
	memset(&stUNLOADBatch,0x00, sizeof(UNLOAD_BATCH)); 
	memset(&stShuttleBatch,0x00, sizeof(SHUTTLE_BATCH)); 
	memset(&stRETURNCVBatch,0x00, sizeof(RETURNCV_BATCH)); 
	memset(&m_PartData,0x00, NO_STAGE*sizeof(PART_DATA));

	return 0;
}

int CInspectThread::m_fnBeginInspectThread()
{
	return CSequenceManager::m_fnStartScenario(TRUE, FALSE);
}

void CInspectThread::m_fnDeInit()
{
			
	do{
		suspendCount = m_fnResumeStep();
	}while(suspendCount  > 1 );

	do{
		suspendCount_Input = m_fnResumeStep_Input();
	} while (suspendCount_Input  > 1);
	//}while (suspendCount  > 1);
	CSequenceManager::m_fnDeInit();
	G_AddLog(3, L"CSequenceManager::m_fnDeInit 종료");
	//G_MainWnd->m_AxisDlg.m_fnDeInit();
	//G_AddLog(3, L"G_MainWnd->m_AxisDlg.m_fnDeInit 종료");
}

void CInspectThread::m_fnCheck()
{
	int ret = 0;

	if(checkStatus == CHECK_SUSPEND){
		checkStatus = 0;
		G_AddLog(3,L"Inspect Thread to Manual Mode");
		suspendCount_Input = m_fnSuspendStep_Input();
	}

	//if(checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR){
	if(checkStatus == CHECK_NG){
		//manual mode
		if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN )
		{
			m_csCheck.Lock();
			holdAutoRun();
			m_csCheck.Unlock();
		}
		G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
		G_MainWnd->m_ServerDlg.m_btnAuto.SetColorChange(WHITE, BLACK);
		G_MainWnd->m_ServerDlg.m_btnManual.SetColorChange(GREEN, BLACK);
		G_MainWnd->m_ServerDlg.m_btnStart.SetColorChange(WHITE, BLACK);
		G_MainWnd->m_ServerDlg.m_btnStop.SetColorChange(WHITE, BLACK);
		G_AddLog(3,L"Part NG, turn into Manual Mode");

		//checkStatus = 0;
		suspendCount_Input = m_fnSuspendStep_Input();
	}

	m_csCheck.Lock();

	if(m_fnGetBatchStep(LCR) != STEP0){
		if(fLCRLoadCorrect  == 1)
			ret = stg2LCRLoadCorrection();
		else
			ret = stg2LCRTest(); 
	}
	if(m_fnGetBatchStep(DCR) != STEP0){ ret = stg4DCRTest();  /* do more with ret */}
	if(m_fnGetBatchStep(VI) != STEP0){ ret = stg6VITest();  /* do more with ret */}
	if(m_fnGetBatchStep(SHUTTLE) != STEP0){ ret = stg7ShuttleRun();  /* do more with ret */}
	if (m_fnGetBatchStep(UNLOAD) != STEP0){ ret = stg6Unload();  /* do more with ret */ }
#ifdef _FI	
	if(m_fnGetBatchStep(LOAD) != STEP0){ ret = stg1Load();  /* do more with ret */}
	if(m_fnGetBatchStep(OSC) != STEP0){ ret = stg3OSCTest();  /* do more with ret */}
	//if(m_fnGetBatchStep(IR) != STEP0){ ret = stg5IRTest();  /* do more with ret */}
	//if(m_fnGetBatchStep(HIPOT) != STEP0){ ret = stg5HIPOTTest();  /* do more with ret */}
	//if(m_fnGetBatchStep(UNLOAD) != STEP0){ ret = stg6Unload();  /* do more with ret */}
	if(m_fnGetBatchStep(RETURNCV) != STEP0){ ret = stg8ReturnConveyorRun();  /* do more with ret */}
#endif

	m_csCheck.Unlock();

	//Auto Run 
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
		m_csCheck.Lock();
		TimerAutoRun();
		m_csCheck.Unlock();
	}

	//TIMER
	batchTimer();

}


void CInspectThread::m_fnScanCountPlus()
{
	CString strScanCountText;
	strScanCountText.Format(L"%d", m_nScanCount);

	CMainWnd *pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	m_nScanCount++;

	//pMainWnd->m_ServerDlg.m_ctrlScanCount.SetText(strScanCountText, RED);
}


//////////////////////////////////////////////////////////////////////////////////////////
//BATCH STEP

void CInspectThread::m_fnClearBatchStep(int batch)
{
	m_nCurStep[batch]	= 0;
	m_nPrevStep	[batch]	= 0;	
}

void CInspectThread::m_fnSetBatchStep(int batch, int nStep)
{
	CString str;
	m_nPrevStep[batch] = m_nCurStep[batch];
	m_nCurStep[batch] = nStep;
	G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_BATCH_STEP + batch] = m_nCurStep[batch];
	wchar_t step[5];
	swprintf_s(step,L"%d",nStep);
	G_MainWnd->m_PIODlg.m_btnBatchClear[batch].SetWindowText(step);
	//STORE STEP IN FLASH DATA
	if(!(nStep == STEP22 || nStep == STEP19 || nStep == STEP20 )){
		str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_BATCH_STEP + batch]);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LOAD_BATCH_STEP + batch],str); 
	}

}

int	CInspectThread::m_fnGetBatchStep(int batch)
{
	return m_nCurStep[batch];
}

int	CInspectThread::m_fnGetPrevBatchStep(int batch)
{
	return m_nPrevStep[batch];
}

void	CInspectThread::m_fnSetPrevBatchStep(int batch, int nStep)
{

	m_nPrevStep[batch] = nStep;
}

void CInspectThread::m_fnSetOldBatchStep(int batch, int nOldStep)
{
	m_nOldStep[batch] = nOldStep;
}

int		CInspectThread::m_fnGetOldBatchStep(int batch)
{
	return m_nOldStep[batch];
}

BOOL CInspectThread::m_fnCheckBatchStep(int batch)
{
	if(m_nCurStep[batch] == m_nPrevStep[batch])
		return FALSE;
	else
	{
		m_nPrevStep[batch] = m_nCurStep[batch];		
		return TRUE;
	}
}

int	CInspectThread::m_fnRetryBatchStep(int batch)
{
	return m_nCurStep[batch];
}


void CInspectThread::m_fnSetInspectionStep(int batch, int nStep)
{
	CString str;

	switch(batch){
	case LCR:
		stLCRBatch.LCR_test_step = nStep; 
		G_MainWnd->m_DataHandling.m_FlashData.data[LCR_INSPECT_STEP] = stLCRBatch.LCR_test_step;
		str.Format(L"%d",stLCRBatch.LCR_test_step);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LCR_INSPECT_STEP],str); 		
		break;
	case OSC: 
		stOSCBatch.oscStep = nStep; 
		break;
	case DCR:
		stDCRBatch.dcrStep = nStep; 
		G_MainWnd->m_DataHandling.m_FlashData.data[DCR_INSPECT_STEP] = stDCRBatch.dcrStep;
		str.Format(L"%d",stDCRBatch.dcrStep);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[DCR_INSPECT_STEP],str); 	
		break;
	case HIPOT:
		stHIPOTBatch.hiPotStep = nStep; 
		G_MainWnd->m_DataHandling.m_FlashData.data[HIPOT_INSPECT_STEP] = stHIPOTBatch.hiPotStep;
		str.Format(L"%d",stHIPOTBatch.hiPotStep);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[HIPOT_INSPECT_STEP],str); 	
		break;
	case IR:
		stIRBatch.irStep = nStep; 
		G_MainWnd->m_DataHandling.m_FlashData.data[IR_INSPECT_STEP] = stIRBatch.irStep;
		str.Format(L"%d",stIRBatch.irStep);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[IR_INSPECT_STEP],str); 
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////
//LCR test sequence
int CInspectThread::stg2LCRTest(void)
{
	int testNo;
	int stepArr; //step index in lcr test array
	ViStatus ret;
	int iRet;
	int nDone;
	CString str;
	long param;
	int running, cnt;
	int iAxis;
	int nRunning;

	switch(m_fnGetBatchStep(LCR))
	{
		case STEP0: //READY
			if(m_fnGetPrevBatchStep(LCR) != STEP0)
			{
				m_fnSetBatchStep(LCR, STEP0);
			}
			break;

		case STEP1: //START
			//20160308_softdetection_add
			m_PartData[STG_LCR].SoftDetect = 1;
			//
			stShuttleBatch.allow &= ~SHUTTLE_LCR; //not allowed shuttle
			stLCRBatch.pAxisData = &m_AxisDlg.axisData;
			setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
			stLCRBatch.retry = 0;
			stLCRBatch.NG = 0;
			stLCRBatch.VISAFault = 0; 
			G_AddLog(3,L"LCR Batch Start");
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_LCR);
			if (m_pPIO->readDI(IN_LCR_SHORT_UP, CARD_IO) == 1)//UP
			m_fnSetBatchStep(LCR, STEP3);
			break;

		case STEP3: //
			if(stLCRBatch.LCR_Axis_step + 1 < G_MainWnd->m_AxisDlg.axisData.steps) // next step
			{
				m_fnSetBatchStep(LCR, STEP5);
			}
			else
			{
				//Home
				if(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME] > 0 &&
					stLCRBatch.testAccu >= G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME] )
				{
#ifdef _FI
					m_fnSetBatchStep(LCR, STEP40);
#else
					m_fnSetBatchStep(LCR, STEP14); 
					//m_fnSetBatchStep(LCR, STEP24);
#endif
				}else{
					setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
					m_fnSetBatchStep(LCR, STEP14); 
				}
			}
			break;


////////////////////////////////////////////////////////////////

		//AXIS MOVE forward
		case STEP5: //
			if(m_fnGetPrevBatchStep(LCR) != STEP5)
			{
				G_AddLog(3,L"STEP5 [stLCRBatch.LCR_Axis_step: %d]", stLCRBatch.LCR_Axis_step + 1);
				if(G_MainWnd->m_AxisDlg.checkLCRAxisPosition(stLCRBatch.LCR_Axis_step))
				{
					if(G_MainWnd->m_AxisDlg.stepMoveLCR(stLCRBatch.LCR_Axis_step + 1) == 1){
						m_fnSetBatchStep(LCR, STEP5);
					}
					else{
						//Error
						str.Format(L"LCR Axis Step Move [%d] Failed", stLCRBatch.LCR_Axis_step + 1);
						G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
						G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
						m_fnSetBatchStep(LCR, STEP19);
					}
				}else{
					//Error
					str.Format(L"LCR Axis step %d not in position", stLCRBatch.LCR_Axis_step);
					G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(LCR, STEP19);
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //20sec
				}else{
					//check LCR Axis running status
					nDone = G_MainWnd->m_AxisDlg.stepMoveCheckLCR();
					 if(nDone == emSTAND){//all stopped
						stLCRBatch.timer.clear();
						setLCRAxisStep(KEY_LCR_AXIS_STEP, ++stLCRBatch.LCR_Axis_step);
						//AFTER MOVE
						if(static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[stLCRBatch.LCR_Axis_step][NUM_LCR_AXIS]) == 0)
						{
							m_fnSetBatchStep(LCR, STEP3);
						}
						else
						{
							m_fnSetBatchStep(LCR, STEP6);
						}
					 }else{
						 if(stLCRBatch.timer.OTE == 1)
						 {
							 stLCRBatch.timer.clear();
							 //Error
							 //G_AddLog(3,L"%s, %d, LCR AXIS STEP RUN STOP TIME OUT ERROR ", __FILE__, __LINE__);
							 str.Format(L"LCR Axis Step Move %d  RUN TIME OUT ERROR", stLCRBatch.LCR_Axis_step);
							 G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
							 G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
							 m_fnSetBatchStep(SHUTTLE, STEP19);
						 }
					 }//if(nDone == emSTAND){//stopped
				}
			}//if(m_fnGetPrevBatchStep(LCR) != STEP5)
			break;

		case STEP6: 
			testNo = static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[stLCRBatch.LCR_Axis_step][NUM_LCR_AXIS]);
			stepArr = testNo -1;
			if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[stepArr].shortPin == 1){//SHORT PIN DOWN
				m_fnSetBatchStep(LCR, STEP11);
			}else{
				m_fnSetBatchStep(LCR, STEP7);
			}
			break;

		case STEP7: //
			testNo = static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[stLCRBatch.LCR_Axis_step][NUM_LCR_AXIS]);
			stepArr = testNo -1;
			if(testNo> 0)
			{
				G_AddLog(3,L"LCR MEASURE STEP [%d]", stepArr);
				ret = m_pInstrumentDlg->measureLcr(stepArr, m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr]); //array step no
				if(ret == VI_SUCCESS){
					//Result
					double l = atof(m_pInstrumentDlg->vi[VI_LCR].str_result);
					if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
						l += G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[stepArr].offset*(+1.0E-6);
						//G_AddLog(3,L"LCR Measure Offset applied ");
					}
					m_PartData[STG_LCR].l[stepArr] = l;

					if(m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr].upperLimit != 0)
					{
						if( m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr].lowerLimit*(+1.0E-6) <= l && l <= m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr].upperLimit*(+1.0E-6)){//uH
							m_PartData[STG_LCR].lPass[stepArr] = 1;
							G_AddLog(3,L"LCR Measure Pass ");
						}else{
							m_PartData[STG_LCR].lPass[stepArr] = 0;
							stLCRBatch.NG=1;
							//G_AddLog(3,L"LCR Measure Fail ");
							//m_fnSetBatchStep(LCR, STEP8); //step back to probe up
							//break;
						}
					}else{
						if( m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr].lowerLimit*(+1.0E-6) <= l ){//uH
							m_PartData[STG_LCR].lPass[stepArr] = 1;
							G_AddLog(3,L"LCR Measure Pass ");
						}else{
							m_PartData[STG_LCR].lPass[stepArr] = 0;
							stLCRBatch.NG=1;
							//G_AddLog(3,L"LCR Measure Fail ");
							//m_fnSetBatchStep(LCR, STEP8); //step back to probe up
							//break;
						}
					}

					//Main Dlg Grid Update
					param = STG_LCR * 0x100 + stepArr;
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);

				}else{ //test error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 11 ); // INDEX NO.
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 11 ); // INDEX NO.
					ViStatus ret = 0;		//1124_KYS_ADD
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[stepArr].shortPin == 1){//SHORT PIN DOWN
				m_fnSetBatchStep(LCR, STEP12);
			}else{
				m_fnSetBatchStep(LCR, STEP3);
			}
			break;

		case STEP8: //NG
			if(stLCRBatch.NG){
				stLCRBatch.NG = 0;
				if(stLCRBatch.retry++ < G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_RETEST_COUNT])
				{
					m_fnSetBatchStep(LCR, STEP9);
				}else
				{
					//NG final
					m_PartData[STG_LCR].NG++;
					m_fnSetBatchStep(LCR, STEP3);
				}
			}else{
				m_fnSetBatchStep(LCR, STEP3);
			}
			break;

		case STEP9: //AXIS STEP BACK, PROBE UP 
			if(m_fnGetPrevBatchStep(LCR) != STEP9)
			{
				G_AddLog(3,L"STEP9 [stLCRBatch.LCR_Axis_step: %d]", stLCRBatch.LCR_Axis_step - 1);
				if(G_MainWnd->m_AxisDlg.stepMoveLCR(stLCRBatch.LCR_Axis_step - 1)){
					m_fnSetBatchStep(LCR, STEP9);
				}
				else{
					//Error
					m_fnSetBatchStep(LCR, STEP19);break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 200; //20sec
				}else{
					//check LCR Axis running status
					nDone = G_MainWnd->m_AxisDlg.stepMoveCheckLCR();
					if(nDone == emSTAND){//all stopped
						stLCRBatch.timer.clear();
						setLCRAxisStep(KEY_LCR_AXIS_STEP, --stLCRBatch.LCR_Axis_step);
						//AFTER MOVE
						if(static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[stLCRBatch.LCR_Axis_step][8]) == 0)
						{
							m_fnSetBatchStep(LCR, STEP3);
						}
						else
						{
							m_fnSetBatchStep(LCR, STEP7);
						}
					}else{
						if(stLCRBatch.timer.OTE == 1)
						{
							stLCRBatch.timer.clear();
							//Error
							G_AddLog(3,L"LCR AXIS STEP RUN STOP TIME OUT ERROR ");
							m_fnSetBatchStep(SHUTTLE, STEP19);break;
						}
					}//if(nDone == emSTAND){//stopped
				}
			}//if(m_fnGetPrevBatchStep(LCR) != STEP9)
			break;

		case STEP11: //SHORT PIN DOWN
			if(m_fnGetPrevBatchStep(LCR) != STEP11)
			{
				iRet = m_pPIO->writeDO(1, OUT_LCR_SHORT_DOWN,CARD_IO);
				m_fnSetBatchStep(LCR, STEP11);
				//160205_KYS
				stLCRBatch.timer.clear();
			}
			else
			{	//wait till PIN DOWN
				if(m_pPIO->readDI(IN_LCR_SHORT_DOWN, CARD_IO)==1)//DOWN
				{
					//TIMER 0.5S
					if(!stLCRBatch.timer.On){
						stLCRBatch.timer.On = 1;
						stLCRBatch.timer.Set = 5;
					}else{
						if(stLCRBatch.timer.OTE == 1)
						{
							stLCRBatch.timer.clear();
							stLCRBatch.counter.clear();
							m_fnSetBatchStep(LCR, STEP7); //TEST
							G_AddLog(3,L"LCR SHORT PIN DOWN");
						}
					}
				}else{
					//160205_KYS_COUNTER->TIMER
					if (!stLCRBatch.timer.On){
						stLCRBatch.timer.On = 1;
						stLCRBatch.timer.Set = 500;// 5sec
					}else{
						if (stLCRBatch.timer.OTE == 1)
						{
							stLCRBatch.timer.clear();
							stLCRBatch.counter.clear();
							//timeout error
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 12 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 12 );
							m_fnSetBatchStep(LCR, STEP19);break;
						}
					}
				}//if(m_pPIO->readDI(IN_LCR_SHORT_DOWN, CARD_IO)==1)//DOWN
			}
			break;

		case STEP12: //SHORT PIN UP
			if(m_fnGetPrevBatchStep(LCR) != STEP12)
			{
				iRet = m_pPIO->writeDO(1, OUT_LCR_SHORT_UP,CARD_IO);
				m_fnSetBatchStep(LCR, STEP12);
			}
			else
			{	//wait till PIN UP
				if(m_pPIO->readDI(IN_LCR_SHORT_UP, CARD_IO)==1)//UP
				{
					//TIMER 0.5S
					if(!stLCRBatch.timer.On){
						stLCRBatch.timer.On = 1;
						stLCRBatch.timer.Set = 5;
					}else{
						if(stLCRBatch.timer.OTE == 1)
						{
							stLCRBatch.timer.clear();
							stLCRBatch.counter.clear();
							m_fnSetBatchStep(LCR, STEP3);
							G_AddLog(3,L"LCR SHORT PIN UP");
						}
					}
				}else{
					if(!stLCRBatch.counter.On){
						stLCRBatch.counter.On = 1;
						stLCRBatch.counter.Set = 200;// 20sec
					}else{
						if(stLCRBatch.counter.OTE == 1)
						{
							stLCRBatch.timer.clear();
							stLCRBatch.counter.clear();
							//timeout error
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 12 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 12 );
							m_fnSetBatchStep(LCR, STEP19); break;
						}
					}
				}//if(m_pPIO->readDI(IN_LCR_SHORT_UP, CARD_IO)==1)//UP
			}
			break;

		case STEP13: // AXIS HOME
#ifdef _FI 
			//// request shuttle safe
			//setBatchStatus(LCR, FINISHED);
			//stShuttleBatch.allow |= SHUTTLE_LCR; //allow shuttle
			//if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			//	stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[LCR]; //request shuttle
			m_fnSetBatchStep(LCR, STEP40);
#else		//PI hole shuttle 
			m_fnSetBatchStep(LCR, STEP30);
#endif
			break;


		case STEP14: //
			if(m_fnGetPrevBatchStep(LCR) != STEP14) 
			{
				setBatchStatus(LCR, FINISHED);
				stLCRBatch.timer.On = 1;
				stLCRBatch.timer.Set = 5; //1000ms 
				m_fnSetBatchStep(LCR, STEP14);
				stLCRBatch.testAccu++;
			}
			else
			{
				if(stLCRBatch.timer.OTE == 1)
				{
					stShuttleBatch.allow |= SHUTTLE_LCR; //allow shuttle
					stLCRBatch.timer.clear();
					if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
						m_fnSetBatchStep(LCR, STEP16);
					else
						m_fnSetBatchStep(LCR, STEP15);
				}
				//count up and error
			}
			break;
		case STEP15: //LCR MANUAL END
			stShuttleBatch.allow |= SHUTTLE_LCR; //allow shuttle
			m_fnSetBatchStep(LCR, STEP0); //
			G_AddLog(3,L"LCR Batch Cycle End ");
			break;
		case STEP16: //LCR AUTO END
			stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; //allow shuttle
			stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[LCR]; //request shuttle
			m_fnSetBatchStep(LCR, STEP17); //
			G_AddLog(3,L"LCR Batch Shuttle Request ");
			break;
		case STEP17: //LCR WAIT SHUTTLE END
			//wait until shuttle end
			if(stShuttleBatch.request == 0)
			{
				stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; //allow shuttle
				m_fnSetBatchStep(LCR, STEP0);
				G_AddLog(3,L"LCR Auto Run Batch Cycle End ");
			}
			break;

			////////////////////////////////////////////////////////////////////////
			// ERROR
		case STEP19:  //ERROR STATE
			if(m_fnGetPrevBatchStep(LCR) != STEP19)
			{
				m_fnSetOldBatchStep(LCR, m_fnGetPrevBatchStep(LCR));
				m_fnSetBatchStep(LCR, STEP19);
				G_MainWnd->m_PIODlg.m_btnErrorReset[LCR].SetColorChange(RED, BLACK);
				errorRoutine();
			}else
			{ // error process time count up

			}
			break;

		case STEP20:   //ERROR RESET
			//error reset
			stLCRBatch.timer.clear();
			//clear error queue and register
			G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_LCR);
			//m_fnSetBatchStep(LCR, m_fnGetOldBatchStep(LCR));
			m_fnSetBatchStep(LCR, STEP22);
			break;

		case STEP22:   //HOLD

			break;

	/////////////////////////////////////////////////////////////////////////////////////////
	// PI HOME
		case STEP24:   //
			if(m_fnGetPrevBatchStep(LCR) != STEP24)
			{
				if(G_MainWnd->m_AxisDlg.checkLCRAxisReadyPosition() != 1)
				{	//Not in ready position
					m_fnSetBatchStep(LCR, STEP19);
				}else{
					m_fnSetBatchStep(LCR, STEP24);
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 5; //0.5sec
				}else{
					if(stLCRBatch.timer.On && stLCRBatch.timer.OTE){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP25);
					}
				}
			}
			break;

		case STEP25:   //1. P_Z_AXIS_HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP25)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_Z);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP25);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					running = 0;
					iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(Axis_Probe_Z, &nDone, 2);
					running += nDone;
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_Z); if( m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_Z); if( m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP26);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP26: //2.P_X_Axis ABS 40000
			if(m_fnGetPrevBatchStep(LCR) != STEP26)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_X, 40000);
				if( iRet == TMC_RV_OK )	{
					m_fnSetBatchStep(LCR, STEP26); 
				}else{//Error
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					nRunning = G_MainWnd->m_AxisDlg.stepMoveCheckLCR(); //All Axis Stop
					if(nRunning == emSTAND){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP27); 
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;
			
		case STEP27: //3.P_Y_Axis ABS 40000
			if(m_fnGetPrevBatchStep(LCR) != STEP27)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Y, 40000);
				if( iRet == TMC_RV_OK )	{
					m_fnSetBatchStep(LCR, STEP27); 
				}else{//Error
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					//iRet = pmiAxCheckDone(CARD_NO, Axis_Probe_Y,  &nRunning);
					nRunning = G_MainWnd->m_AxisDlg.stepMoveCheckLCR();
					if(nRunning == emSTAND){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP28); 
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP28: //4.P_T_Axis HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP28)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_T);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP28);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP29);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;


		case STEP29: //5.P_L_Axis HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP29)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_L);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP29);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 3000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP30);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP30: //6.P_X_Axis HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP30)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_X);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP30);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP31);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;


		case STEP31: //7.P_Y_Axis HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP31)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_Y);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP31);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP32);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP32: //8.SHORT_Axis Together HOME
			if(m_fnGetPrevBatchStep(LCR) != STEP32)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_X);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_Y);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_Z);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_T);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_L);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP32);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 3000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;}
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;}
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP33);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP33: //9.SHORT_Axis Together Ready
			if(m_fnGetPrevBatchStep(LCR) != STEP33)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_X, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SX]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_Y, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SY]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_Z, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SZ]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_T, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_ST]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_L, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SL]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP33);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 3000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP34);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP34: //10.P_X_Axis ABS 30000
			if(m_fnGetPrevBatchStep(LCR) != STEP34)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_X, 30000);
				if( iRet == TMC_RV_OK )	{
					m_fnSetBatchStep(LCR, STEP34); 
				}else{//Error
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					nRunning = G_MainWnd->m_AxisDlg.stepMoveCheckLCR(); //All Axis Stop
					if(nRunning == emSTAND){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP35); 
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;
			
		case STEP35: //11.P_Y_Axis ABS 40000
			if(m_fnGetPrevBatchStep(LCR) != STEP35)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Y, 40000);
				if( iRet == TMC_RV_OK )	{
					m_fnSetBatchStep(LCR, STEP35); 
				}else{//Error
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					nRunning = G_MainWnd->m_AxisDlg.stepMoveCheckLCR();
					if(nRunning == emSTAND){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP36); 
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP36: //12.P_X_Axis ABS 40000
			if(m_fnGetPrevBatchStep(LCR) != STEP36)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_X, 40000);
				if( iRet == TMC_RV_OK )	{
					m_fnSetBatchStep(LCR, STEP36); 
				}else{//Error
					m_fnSetBatchStep(LCR, STEP19); break;
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					nRunning = G_MainWnd->m_AxisDlg.stepMoveCheckLCR(); //All Axis Stop
					if(nRunning == emSTAND){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP37); 
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP37: //13.P_T,L,Z_Axis READY
			if(m_fnGetPrevBatchStep(LCR) != STEP37)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Z, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_Z]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_T, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_T]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_L, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_L]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP37);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 3000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP38);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP38: //14.P_Y_Axis READY
			if(m_fnGetPrevBatchStep(LCR) != STEP38)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Y, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_Y]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP38);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP39);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP39: //15.P_X_Axis READY
			if(m_fnGetPrevBatchStep(LCR) != STEP39)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_X, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP39);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						stLCRBatch.testAccu = 0;
						G_AddLog(3,L"LCR AXIS AUTO HOME TO READY ¿Ï·á");
						m_fnSetBatchStep(LCR, STEP14);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

	/////////////////////////////////////////////////////////////////////////////////////////
	// FI HOME
		case STEP40: 
			if(m_fnGetPrevBatchStep(LCR) != STEP40)
			{
				if(G_MainWnd->m_AxisDlg.checkLCRAxisReadyPosition() != 1)
				{	//Not in ready position
					m_fnSetBatchStep(LCR, STEP19);
				}else{
					m_fnSetBatchStep(LCR, STEP40);
				}
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 5; //0.5sec
				}else{
					if(stLCRBatch.timer.On && stLCRBatch.timer.OTE){
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP41);
					}
				}
			}
			break;

		case STEP41: //Z axis
			if(m_fnGetPrevBatchStep(LCR) != STEP41)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_Z);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_Z);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP41);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_Z); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP42);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP42: //X,Y,L axis
			if(m_fnGetPrevBatchStep(LCR) != STEP42)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_X);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_Y);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_L);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_X);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_Y);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_L);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP42);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_X); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_Y); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_L); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP43);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP43: //T axis
			if(m_fnGetPrevBatchStep(LCR) != STEP43)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Probe_T);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnHomemove(Axis_Short_T);	if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP43);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 2000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxHomeCheckDone(iAxis, &nDone, 0);
						running += nDone;
					}
					if(running == 0)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Probe_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnClearPos(Axis_Short_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Probe_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						iRet = G_MainWnd->m_AxisDlg.m_fnSoftLimitEnable(TRUE, Axis_Short_T); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ){m_fnSetBatchStep(LCR, STEP19); break;} 
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP44);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP44: //T axis to Ready
			if(m_fnGetPrevBatchStep(LCR) != STEP44)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_T, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_T]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_T, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_ST]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP44);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP45);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP45: //X, Y, L axis to Ready
			if(m_fnGetPrevBatchStep(LCR) != STEP45)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_X, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Y, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_Y]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_L, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_L]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_X, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SX]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_Y, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SY]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_L, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SL]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP45);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP46);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP46: //Z axis to Ready
			if(m_fnGetPrevBatchStep(LCR) != STEP46)
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Probe_Z, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_Z]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove(Axis_Short_Z, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_SZ]); if( G_MainWnd->m_AxisDlg.ReturnMessage(iRet) == FALSE ) {m_fnSetBatchStep(LCR, STEP19); break;}
				m_fnSetBatchStep(LCR, STEP46);
			}
			else
			{
				if(!stLCRBatch.timer.On){
					stLCRBatch.timer.On = 1;
					stLCRBatch.timer.Set = 1000; //
				}else{
					running = 0;
					for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
					{
						iRet = G_MainWnd->m_AxisDlg.m_fnAxCheckDone(iAxis, &nDone, 0);//20sec 
						running += nDone;
					}
					if(running == 0)
					{
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP47);
					}else if(stLCRBatch.timer.On == 1 && stLCRBatch.timer.OTE == 1)
					{
						//Time out error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
						stLCRBatch.timer.clear();
						m_fnSetBatchStep(LCR, STEP19); break;
					}
				}
			}
			break;

		case STEP47:
				stLCRBatch.testAccu = 0;
				G_AddLog(3,L"LCR AXIS AUTO HOME TO READY ¿Ï·á");
				//m_fnSetBatchStep(LCR, STEP47);
				m_fnSetBatchStep(LCR, STEP14);
			break;

		//case STEPx: 
		//	if(m_fnGetPrevBatchStep(LCR) != STEPx)
		//	{

		//		m_fnSetBatchStep(LCR, STEPx);
		//	}
		//	else
		//	{
		//		if(!stLCRBatch.timer.On){
		//			stLCRBatch.timer.On = 1;
		//			stLCRBatch.timer.Set = 5; //0.5sec
		//		}else{


		//		m_fnSetBatchStep(LCR, STEPx+1);
		//		}
		//	}
		//	break;

/*
	STEP40, // FI AXIS HOME READY START
	STEP41, // LCR AXIS Z HOME
	STEP42, // LCR AXIS X,Y,L HOME
	STEP43, // LCR AXIS T HOME
	STEP44, // LCR AXIS T READY
	STEP45, // LCR AXIS X,Y,L READY
	STEP46, // LCR AXIS Z READY
	STEP47, // FI AXIS HOME READY END
	STEP48, //
	STEP49, //

	STEP30, // PI AXIS HOME READY START
	STEP31, // PROBE Y P1 MOVE, PROBE Z HOME, SHORT Z HOME
	STEP32, // PROBE X HOME, SHORT X, Y HOME
	STEP33, // PROBE T,L HOME, SHORT T,L HOME
	STEP34, // PROBE T,L,Z READY, SHORT T,L,Z READY
	STEP35, // PROBE X READY, SHORT X READY
	STEP36, // PROBE Y HOME, SHORT Y READY
	STEP37, // PROBE Y READY
	STEP38, // PI AXIS HOME READY END
	STEP39, //
*/


	}//switch(m_fnGetBatchStep(LCR))

		return 0;
}

////////////////////////////////////////////////////////////////////////////////////////
//LCR Load correction
int CInspectThread::stg2LCRLoadCorrection(void)
{
	int testNo;
	int stepArr; //step index in lcr test array
	ViStatus ret;
	CString str;
	int nDone;

	switch(m_fnGetBatchStep(LCR))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(LCR) != STEP0)
		{
			m_fnSetBatchStep(LCR, STEP0);
		}
		break;

	case STEP1: //START
		if(fLCRLoadCorrect != 1){
			G_AddLog(3,L"LCR Load Correction fLCRLoadCorrect Not Set");
			m_fnSetBatchStep(LCR, STEP19); break;
		}else{

		}
		stShuttleBatch.allow &= ~SHUTTLE_LCR; //not allowed shuttle
		setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_LCR);
		m_fnSetBatchStep(LCR, STEP3);
		break;


	case STEP3: //
		if(stLCRBatch.LCR_Axis_step + 1 < stLCRBatch.pAxisData->steps) // next step
		{
			m_fnSetBatchStep(LCR, STEP5);
		}
		else
		{
			setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
			m_fnSetBatchStep(LCR, STEP14);
		}
		break;

		//LCR TEST
		////////////////////////////////////////////////////////////////

		//AXIS MOVE forward
	case STEP5: //
		if (m_fnGetPrevBatchStep(LCR) != STEP5)
		{
			G_AddLog(3, L"STEP5 [stLCRBatch.LCR_Axis_step: %d]", stLCRBatch.LCR_Axis_step + 1);
			if (G_MainWnd->m_AxisDlg.checkLCRAxisPosition(stLCRBatch.LCR_Axis_step))
			{
				if (G_MainWnd->m_AxisDlg.stepMoveLCR(stLCRBatch.LCR_Axis_step + 1) == 1){
					m_fnSetBatchStep(LCR, STEP5);
				}
				else{
					//Error
					str.Format(L"LCR Axis Step Move [%d] Failed", stLCRBatch.LCR_Axis_step + 1);
					G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					m_fnSetBatchStep(LCR, STEP19);
				}
			}
			else{
				//Error
				str.Format(L"LCR Axis step %d not in position", stLCRBatch.LCR_Axis_step);
				G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				m_fnSetBatchStep(LCR, STEP19);
			}
		}
		else
		{
			if (!stLCRBatch.timer.On){
				stLCRBatch.timer.On = 1;
				stLCRBatch.timer.Set = 200; //20sec
			}
			else{
				//check LCR Axis running status
				nDone = G_MainWnd->m_AxisDlg.stepMoveCheckLCR();
				if (nDone == emSTAND){//all stopped
					stLCRBatch.timer.clear();
					setLCRAxisStep(KEY_LCR_AXIS_STEP, ++stLCRBatch.LCR_Axis_step);
					//AFTER MOVE
					if (static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[stLCRBatch.LCR_Axis_step][NUM_LCR_AXIS]) == 0)
					{
						m_fnSetBatchStep(LCR, STEP3);
					}
					else
					{
						m_fnSetBatchStep(LCR, STEP6);
					}
				}
				else{
					if (stLCRBatch.timer.OTE == 1)
					{
						stLCRBatch.timer.clear();
						//Error
						//G_AddLog(3,L"%s, %d, LCR AXIS STEP RUN STOP TIME OUT ERROR ", __FILE__, __LINE__);
						str.Format(L"LCR Axis Step Move %d  RUN TIME OUT ERROR", stLCRBatch.LCR_Axis_step);
						G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
						G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
						m_fnSetBatchStep(SHUTTLE, STEP19);
					}
				}//if(nDone == emSTAND){//stopped
			}
		}//if(m_fnGetPrevBatchStep(LCR) != STEP5)
		break;

	case STEP6: //
		if (stLCRBatch.ShortTest == 0){//not done
			m_fnSetBatchStep(LCR, STEP7);
			stLCRBatch.ShortTest = 1;
		}
		else
		{
			m_fnSetBatchStep(LCR, STEP3);
		}
		break;


		//LCR CORRECTION
	case STEP7: //
		testNo = static_cast<int>(stLCRBatch.pAxisData->absPos[stLCRBatch.LCR_Axis_step][8]);
		stepArr = testNo -1;
		if(testNo> 0)
		{
				//Correction
				ret = m_pInstrumentDlg->correctLcr(stepArr, m_pRecipeSetDlg->part_Test.lcrTestInfo[stepArr]); //array step no
				if (ret != VI_SUCCESS) {
					G_AddLog(3, L"LCR SHORT CORRECTION FAILED");
					m_fnSetBatchStep(LCR, STEP19); break;
				}
		}
		G_AddLog(3, L"LCR SHORT CORRECTION SUCCEEDED");
//1113_MERGE_ADD
		G_MainWnd->m_DataHandling.m_FlashData.data[LCR_CORRECTION] = 1;
		m_fnSetBatchStep(LCR, STEP3);
		break;



		//
	case STEP14: //
		if(m_fnGetPrevBatchStep(LCR) != STEP14) 
		{
			stLCRBatch.timer.On = 1;
			stLCRBatch.timer.Set = 10; //1000ms 
			m_fnSetBatchStep(LCR, STEP14);
		}
		else
		{
			if(stLCRBatch.timer.OTE == 1)
			{
				stShuttleBatch.allow |= SHUTTLE_LCR; //allow shuttle
				stLCRBatch.timer.clear();
				setBatchStatus(LCR, FINISHED);
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(LCR, STEP16);
				else
					m_fnSetBatchStep(LCR, STEP15);
			}
			//count up and error
		}
		break;
	case STEP15: //LCR MANUAL END
		fLCRLoadCorrect = 0;
		stLCRBatch.ShortTest = 0;//Clear
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; //allow shuttle
		m_fnSetBatchStep(LCR, STEP0); //
		break;
	case STEP16: //LCR AUTO END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; //allow shuttle
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[LCR]; //request shuttle
		m_fnSetBatchStep(LCR, STEP17); //
		break;
	case STEP17: //LCR WAIT SHUTTLE END
		//wait until shuttle end
		if(stShuttleBatch.request == 0)
		{
			fLCRLoadCorrect  = 0;
			stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; //allow shuttle
			m_fnSetBatchStep(LCR, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(LCR) != STEP19)
		{
			m_fnSetOldBatchStep(LCR, m_fnGetPrevBatchStep(LCR));
			m_fnSetBatchStep(LCR, STEP19);
			G_MainWnd->m_PIODlg.m_btnErrorReset[LCR].SetColorChange(RED, BLACK);
			errorRoutine();
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//error reset

		//clear error queue and register
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_LCR);
		m_fnSetBatchStep(LCR, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(LCR))

	return 0;
}

//SHUTTLE test sequence
int CInspectThread::stg7ShuttleRun(void)
{
	int iRet;
	int nDone;
	CString str;

	switch(m_fnGetBatchStep(SHUTTLE))
	{
		case STEP0: //READY
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP0)
			{
				m_fnSetBatchStep(SHUTTLE, STEP0);
				if(stShuttleBatch.status != CLEAR && m_pPIO->readDI(IN_SHUTTLE_DOWN, CARD_IO)==1
					&& m_pPIO->readDI(IN_SHUTTLE_RETURN, CARD_IO)==1){
					setBatchStatus(SHUTTLE,CLEAR);
				}else{

				}
			}
			break;

		case STEP1: //START
			setBatchStatus(SHUTTLE, LOCKED);
			//check before chuttle start
			G_AddLog(3,L"SHUTTLE STEP1");
			m_fnSetBatchStep(SHUTTLE, STEP2);
			break;

		case STEP2: //delay 1 sec
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP2) 
			{
				stShuttleBatch.timer.On = 1;
				stShuttleBatch.timer.Set = 2; //0.2SEC
				//stShuttleBatch.timer.Set = 5; //1000ms 
				m_fnSetBatchStep(SHUTTLE, STEP2);
			}
			else
			{
				if(stShuttleBatch.timer.OTE == 1)
				{
					stShuttleBatch.timer.clear();
					//safe to shuttle?
					m_fnSetBatchStep(SHUTTLE, STEP5); //GO TO UP
				}
			}
			break;

			//AI1 TEST
		case STEP3: 
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP3)
			{
				G_AddLog(3,L"SHUTTLE STEP3");
				if(!stShuttleBatch.timer.On){
					stShuttleBatch.timer.On = 1;
					stShuttleBatch.timer.Set = 5; //1000ms 
					m_fnSetBatchStep(SHUTTLE, STEP3);
				}
			}
			else
			{
				//SHUTTLE RESET
				if(stShuttleBatch.timer.OTE == 1)
				{
					stShuttleBatch.timer.clear();
					m_fnSetBatchStep(SHUTTLE, STEP10);
				}
			}
			break;
		case STEP4:
			break;

		case STEP5: //UP
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP5)
			{
				iRet = m_pPIO->writeDO(1, OUT_SHUTTLE_UP,CARD_IO);
				m_fnSetBatchStep(SHUTTLE, STEP5);
			}
			else
			{	//wait till lift up
				if(m_pPIO->readDI(OUT_SHUTTLE_UP, CARD_IO)==1)//UP
				{
					//TIMER 0.5S
					if(!stShuttleBatch.timer.On){
						stShuttleBatch.timer.On = 1;
						stShuttleBatch.timer.Set = 50;
					}else{
						if(stShuttleBatch.timer.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							stShuttleBatch.counter.clear();
							m_fnSetBatchStep(SHUTTLE, STEP6);
						}
					}
				}else{
					if(!stShuttleBatch.counter.On){
						stShuttleBatch.counter.On = 1;
						stShuttleBatch.counter.Set = 500;// 20sec
					}else{
						if(stShuttleBatch.counter.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							stShuttleBatch.counter.clear();
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 83 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 83 );
							m_fnSetBatchStep(SHUTTLE, STEP19); break;
						}
					}
				}//if(m_pPIO->readDI(IN_SHUTTLE_LIFT_UP, CARD_IO)==1)//UP
			}
			break;
		case STEP6: //SHUTTLE RUN FORWARD
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP6) 
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove( 8 + AXIS_SHUTTLE, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD]);
				if( iRet != TMC_RV_OK )
				{//Error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 80 );
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 80 );
					m_fnSetBatchStep(SHUTTLE, STEP19); break;
				}else{
					m_fnSetBatchStep(SHUTTLE, STEP6);
				}
			}else{
				if(!stShuttleBatch.timer.On){
					stShuttleBatch.timer.On = 1;
					//stShuttleBatch.timer.Set = 200;//2sec
					stShuttleBatch.timer.Set = 2000;//20sec_20160106_kys
				}else{
					pmiAxCheckDone(CARD_NO2, AXIS_SHUTTLE,  &nDone);
					if(nDone == emRUNNING){
						if(stShuttleBatch.timer.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							//Error
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 82 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 82 );
							m_fnSetBatchStep(SHUTTLE, STEP19); break;
						}
					}else if(nDone == emSTAND){//stopped
						//check IO
						//if(m_pPIO->readDI(IN_SHUTTLE_FORWARD,CARD_IO)==1){
							stShuttleBatch.timer.clear();
							m_fnSetBatchStep(SHUTTLE, STEP8);
						//}
					}
				}
			}//if(m_fnGetPrevBatchStep(SHUTTLE) != STEP5) 
			break;

		//AXIS MOVE
		case STEP7: 

			break;
		case STEP8: //DOWN
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP8)
			{
				iRet = m_pPIO->writeDO(1, OUT_SHUTTLE_DOWN,CARD_IO);
				m_fnSetBatchStep(SHUTTLE, STEP8);
			}
			else
			{	//wait till lift up
				if(m_pPIO->readDI(OUT_SHUTTLE_DOWN, CARD_IO)==1)//DOWN
				{
					//TIMER 0.5S
					if(!stShuttleBatch.timer.On){
						stShuttleBatch.timer.On = 1;
						stShuttleBatch.timer.Set = 50;
					}else{
						if(stShuttleBatch.timer.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							stShuttleBatch.counter.clear();
							m_fnSetBatchStep(SHUTTLE, STEP9);
						}
					}
				}else{
					if(!stShuttleBatch.counter.On){
						stShuttleBatch.counter.On = 1;
						stShuttleBatch.counter.Set = 500;// 20sec
					}else{
						if(stShuttleBatch.counter.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							stShuttleBatch.counter.clear();
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 83 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 83 );
							m_fnSetBatchStep(SHUTTLE, STEP19); break;
						}
					}
				}//if(m_pPIO->readDI(IN_SHUTTLE_LIFT_DOWN, CARD_IO)==1)//DOWN
			}
			break;
		case STEP9: //SHUTTLE RUN RETURN
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP9) 
			{
				iRet = G_MainWnd->m_AxisDlg.m_fnAbsmove( 8 + AXIS_SHUTTLE, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN]);
				if( iRet != TMC_RV_OK )
				{//Error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 81 );
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 81 );
					m_fnSetBatchStep(SHUTTLE, STEP19); break;
				}else{
					m_fnSetBatchStep(SHUTTLE, STEP9);
				}
			}else{
				if(!stShuttleBatch.timer.On){
					stShuttleBatch.timer.On = 1;
					//stShuttleBatch.timer.Set = 200;//2sec
					stShuttleBatch.timer.Set = 2000;//20sec_20160106_kys 
				}else{
					pmiAxCheckDone(CARD_NO2, AXIS_SHUTTLE,  &nDone);
					if(nDone == emRUNNING){
						if(stShuttleBatch.timer.OTE == 1)
						{
							stShuttleBatch.timer.clear();
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 82 );
							::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 82 );
							m_fnSetBatchStep(SHUTTLE, STEP19); break;
						}
					}else if(nDone == emSTAND){
						//if(m_pPIO->readDI(IN_SHUTTLE_RETURN,CARD_IO)==1){
							stShuttleBatch.timer.clear();
							m_fnSetBatchStep(SHUTTLE, STEP10);
						//}
					}
				}
			}//if(m_fnGetPrevBatchStep(SHUTTLE) != STEP9) 
			break;
		case STEP10: //SIGNAL
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP10)
			{
				if(!stShuttleBatch.timer.On){
					stShuttleBatch.timer.On = 1;
					stShuttleBatch.timer.Set = 5; //
					m_fnSetBatchStep(SHUTTLE, STEP10);
				}
			}
			else
			{
				//SHUTTLE RESET
				if(stShuttleBatch.timer.OTE == 1)
				{
					stShuttleBatch.timer.clear();
					m_fnShiftPartData();
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_SHIFT_UPDATE, NULL);
					stShuttleBatch.request = 0; 
					//G_MainWnd->m_ServerDlg.clearGridMainResult();
					m_fnSetBatchStep(SHUTTLE, STEP11); 
				}
			}
			break;
		case STEP11: //
			G_AddLog(3,L"SHUTTLE STEP11");
			setBatchStatus(LOAD, CLEAR);
			setBatchStatus(LCR, CLEAR);
			setBatchStatus(OSC, CLEAR);
			setBatchStatus(DCR, CLEAR);
			setBatchStatus(HIPOT, CLEAR);
			setBatchStatus(IR, CLEAR);
			setBatchStatus(VI, CLEAR);
			setBatchStatus(UNLOAD, CLEAR);
			setBatchStatus(SHUTTLE, CLEAR);

//1124_kys_ok_end_signal
#ifndef _FI
			int stage;
			stage= STG_VI_UNLOAD;
			if ((m_pPIO->readDI(IN_UNLOAD_JIG_DETECT, CARD_IO) == 1) &&
				(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass))
			{
				m_pPIO->writeDO(1, OUT_BUZZER, CARD_IO);
			}
//#endif
//
//20160308_kys_Detection_Check
			if ((m_PartData[STG_VI_UNLOAD].SoftDetect == 1) &&
				(m_pPIO->readDI(IN_UNLOAD_JIG_DETECT, CARD_IO) == 0))
			{
				str.Format(L"UNLOAD SENSOR NOT DETECTION");
				G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				m_fnSetBatchStep(SHUTTLE, STEP19);
				break;
			}
//
#endif
			m_fnSetBatchStep(SHUTTLE, STEP0); //GOTO READY STATUS
			break;

		////////////////////////////////////////////////////////////////////////
			// ERROR
		case STEP19:  //ERROR STATE
			if(m_fnGetPrevBatchStep(SHUTTLE) != STEP19)
			{
				m_fnSetOldBatchStep(SHUTTLE, m_fnGetPrevBatchStep(SHUTTLE));
				m_fnSetBatchStep(SHUTTLE, STEP19);
				errorRoutine();
				G_MainWnd->m_PIODlg.m_btnErrorReset[SHUTTLE].SetColorChange(RED, BLACK);
			}else
			{ // error process time count up

			}
			break;

		case STEP20:   //ERROR RESET
			//error reset
			stShuttleBatch.timer.clear();
			m_fnSetBatchStep(SHUTTLE, STEP22);
			break;

		case STEP22:   //HOLD

			break;

	}//switch(m_fnGetBatchStep(STN1_LOAD))

		return 0;
}


//Part Inspection Data
int CInspectThread::m_fnShiftPartData()
{
#ifdef _FI
	for(int i = NO_STAGE - 1; i >= 1; i--)
	{
		//m_PartData[i].CopyFrom(&m_PartData[i-1]);
		//m_PartData[i-1].Reset();
		memcpy((void *)&m_PartData[i],(void *)&m_PartData[i-1], sizeof(PART_DATA));
		memset((void *)&m_PartData[i-1],0x00,sizeof(PART_DATA)); 
	}
#else
		memcpy((void *)&m_PartData[STG_VI_UNLOAD],(void *)&m_PartData[STG_DCR], sizeof(PART_DATA));
		memset((void *)&m_PartData[STG_DCR],0x00,sizeof(PART_DATA)); 

		memcpy((void *)&m_PartData[STG_DCR],(void *)&m_PartData[STG_LCR], sizeof(PART_DATA));
		memset((void *)&m_PartData[STG_LCR],0x00,sizeof(PART_DATA)); 

#endif
	return 0;
}

int CInspectThread::m_fnShiftPartData(int batchTo, int batchFrom)
{
	memcpy((void *)&m_PartData[batchTo], (void *)&m_PartData[batchFrom], sizeof(PART_DATA));
	memset((void *)&m_PartData[batchFrom], 0x00, sizeof(PART_DATA));
	//m_PartData[batchTo].CopyFrom(&m_PartData[batchFrom]);
	//m_PartData[batchFrom].Reset();
	return 0;
}

int CInspectThread::m_fnClearPartData(int batch)
{
	memset((void *)&m_PartData[batch], 0x00, sizeof(PART_DATA));
	//m_PartData[batch].Reset();
	return 0;
}

int CInspectThread::m_fnClearPartDataAll(void)
{
	memset((void *)m_PartData, 0x00, sizeof(m_PartData)*NO_STAGE);
	return 0;
}

int CInspectThread::m_fnInitPartData(int batch, int model_no, int part_no)
{
	m_PartData[batch].Reset();
	m_PartData[batch].model_no = model_no;
	m_PartData[batch].part_no = part_no;
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////
//AUTO START

int CInspectThread::TimerAutoRun(void)//10ms
{
	//BCR LOAD AUTO START
#ifdef _FI

	if(m_fnGetBatchStep(LOAD) == STEP0  && //READY STATE
		m_pPIO->readDI(IN_LOAD_JIG_DETECT, CARD_IO) == 1 && m_pPIO->readDI(IN_LOAD_JIG_DETECT2, CARD_IO) == 1 &&   //1121_kys
		m_pPIO->readDI(IN_LOAD_AREA_SENSOR1,CARD_IO)== 0 &&
		m_pPIO->readDI(IN_LOAD_AREA_SENSOR2,CARD_IO)== 0  && stShuttleBatch.status != LOCKED )
	{
		if(!stLoadBatch.timer.On){
			stLoadBatch.timer.On = 1;
			stLoadBatch.timer.Set = 30; //3SEC
		}else{
			if(stLoadBatch.timer.OTE == 1)//BCR LOAD AUTO START
			{
				stLoadBatch.timer.clear();
				G_MainWnd->m_InspectThread.m_fnSetBatchStep(LOAD, STEP1);
			}
		}
	}else{
		if(m_fnGetBatchStep(LOAD) == STEP0  && stLoadBatch.timer.On){
			stLoadBatch.timer.clear();
		}
	}

#endif

	//LCR BATCH AUTO START
	if(m_fnGetBatchStep(LCR) == STEP0 && 
		m_pPIO->readDI(IN_LCR_JIG_DETECT,CARD_IO)==1 &&
#ifdef _FI
		stLCRBatch.status == CLEAR &&  stShuttleBatch.status != LOCKED &&
#endif
#ifndef _FI
		m_pPIO->readDI(IN_LOAD_AREA_SENSOR1,CARD_IO) == 0 && m_fnGetBatchStep(SHUTTLE) == STEP0 &&
#endif
		fLCRLoadCorrect  != 1)
	{
		if(!stLCRBatch.timer.On){
			stLCRBatch.timer.On = 1;
			stLCRBatch.timer.Set = 10; //1sec
		}else{
			if(stLCRBatch.timer.OTE == 1)
			{
				stLCRBatch.timer.clear();
				if(G_MainWnd->m_AxisDlg.checkLCRAxisReadyPosition())
					m_fnSetBatchStep(LCR, STEP1); //Start
				else{
					//ready postion error


				}
			}
		}
	}else{
		if(m_fnGetBatchStep(LCR) == STEP0 && stLCRBatch.timer.On){
			stLCRBatch.timer.clear();
		}
	}

	//DCR BATCH AUTO START
	if( m_fnGetBatchStep(DCR) == STEP0  && 
		m_pPIO->readDI(IN_DCR_JIG_DETECT,CARD_IO)==1 &&
		stDCRBatch.status == CLEAR && stShuttleBatch.status != LOCKED
		&& m_fnGetBatchStep(SHUTTLE) == STEP0 
		//&& m_PartData[STG_DCR].NG ==0 
#ifndef _FI	
		&& m_pPIO->readDI(IN_LOAD_AREA_SENSOR1,CARD_IO) == 0
#endif		
		)
	{
		if(!stDCRBatch.timer.On){
			stDCRBatch.timer.On = 1;
			stDCRBatch.timer.Set = 10;
		}else{
			if(stDCRBatch.timer.OTE == 1)
			{
				stDCRBatch.timer.clear();
				m_fnSetBatchStep(DCR, STEP1); //Start
			}
		}
	}else{
		if(m_fnGetBatchStep(DCR) == STEP0  && stDCRBatch.timer.On){
			stDCRBatch.timer.clear();
		}
	}


	//VI BATCH AUTO START
	if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
		if (m_fnGetBatchStep(VI) == STEP0 && 
			m_pPIO->readDI(IN_UNLOAD_JIG_DETECT, CARD_IO) == 1 &&
			stVIBatch.status == CLEAR && stShuttleBatch.status != LOCKED  && m_fnGetBatchStep(SHUTTLE) == STEP0
			//&& m_PartData[STG_VI_UNLOAD].NG == 0 

			&&m_fnGetBatchStep(UNLOAD) == STEP0 

			)
			{
			if (!stVIBatch.timer.On){
				stVIBatch.timer.On = 1;
				stVIBatch.timer.Set = 10;
			}
			else{
				if (stVIBatch.timer.OTE == 1)
				{
					stVIBatch.timer.clear();
					m_fnSetBatchStep(VI, STEP1); //Start
				}
	
				else{
					if (m_fnGetBatchStep(UNLOAD) == STEP0 && stShuttleBatch.status != LOCKED){

					}
					else{//reset
						stVIBatch.timer.clear();
					}
				}

			}
		}

		else{
			if (m_fnGetBatchStep(VI) == STEP0 &&  m_fnGetBatchStep(UNLOAD) == STEP0 && stVIBatch.timer.On){
				stVIBatch.timer.clear();
			}
		}

	}

	//UNLOAD_UNLOAD BATCH AUTO START
	if (m_fnGetBatchStep(UNLOAD) == STEP0 &&
		m_pPIO->readDI(IN_UNLOAD_JIG_DETECT, CARD_IO) == 1 &&
		//#ifndef _VI_SKIP
		((G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1 && (m_fnGetBatchStep(VI) == STEP0 && stVIBatch.status == FINISHED)) ||
		G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 0
#ifndef _FI		
		&& (0) //NOT RUN IN 공정검사 ( NO VISION INSPECTION )
#endif
		) &&
		//#endif
		stUNLOADBatch.status == CLEAR && stShuttleBatch.status != LOCKED 
//1117_kys
#ifndef _FI		
		&& (0) //NOT RUN IN 공정검사 ( NO VISION INSPECTION )
#endif		
		)
	{
		if (!stUNLOADBatch.timer.On){
			stUNLOADBatch.timer.On = 1;
			stUNLOADBatch.timer.Set = 10;
		}
		else{
			if (stUNLOADBatch.timer.OTE == 1)
			{
				stUNLOADBatch.timer.clear();
				m_fnSetBatchStep(UNLOAD, STEP1); //Start
			}
		}
	}
	else{
		if (m_fnGetBatchStep(UNLOAD) == STEP0 && stUNLOADBatch.timer.On){
			stUNLOADBatch.timer.clear();
		}
	}



#ifdef _FI
	//OSC BATCH AUTO START
	if( m_fnGetBatchStep(OSC) == STEP0  &&
		m_pPIO->readDI(IN_OSC_JIG_DETECT,CARD_IO)==1 &&
		stOSCBatch.status == CLEAR &&  stShuttleBatch.status != LOCKED 	&& m_fnGetBatchStep(SHUTTLE) == STEP0 
		//&& 	m_PartData[STG_OSC].NG ==0 
		)
	{
		if(!stOSCBatch.timer.On){
			stOSCBatch.timer.On = 1;
			stOSCBatch.timer.Set = 10;
		}else{
			if(stOSCBatch.timer.OTE == 1)
			{
				stOSCBatch.timer.clear();
				m_fnSetBatchStep(OSC, STEP1); //Start
			}
		}
	}else{
		if(m_fnGetBatchStep(OSC) == STEP0  && stOSCBatch.timer.On){
			stOSCBatch.timer.clear();
		}
	}

	//HIPOT BATCH AUTO START
	if( 	m_fnGetBatchStep(HIPOT) == STEP0 && m_fnGetBatchStep(IR) == STEP0  &&
		m_pPIO->readDI(IN_HIPOTIR_JIG_DETECT,CARD_IO)==1 &&
		stHIPOTBatch.status == CLEAR &&  stShuttleBatch.status != LOCKED && m_fnGetBatchStep(SHUTTLE) == STEP0 
		//&& m_PartData[STG_HIPOT_IR].NG == 0 
	)
	{
		if(!stHIPOTBatch.timer.On){
			stHIPOTBatch.timer.On = 1;
			stHIPOTBatch.timer.Set = 10;
		}else{
			if(stHIPOTBatch.timer.OTE == 1)
			{
				stHIPOTBatch.timer.clear();
				m_fnSetBatchStep(HIPOT, STEP1); //Start
			}
		}
	}else{
		if( 	m_fnGetBatchStep(HIPOT) == STEP0 && stHIPOTBatch.timer.On){
			stHIPOTBatch.timer.clear();
		}
	}

	////IR BATCH AUTO START
	//if(m_fnGetBatchStep(HIPOT) == STEP0 && m_fnGetBatchStep(IR) == STEP0  && 
	//	m_pPIO->readDI(IN_HIPOTIR_JIG_DETECT,CARD_IO)==1 &&
	//	stIRBatch.status == CLEAR && stShuttleBatch.status != LOCKED)
	//{
	//	if(!stIRBatch.timer.On){
	//		stIRBatch.timer.On = 1;
	//		stIRBatch.timer.Set = 10;
	//	}else{
	//		if(stIRBatch.timer.OTE == 1)
	//		{
	//			stIRBatch.timer.clear();
	//			m_fnSetBatchStep(IR, STEP1); //Start
	//		}
	//	}
	//}else{
	//	if( m_fnGetBatchStep(IR) == STEP0  && stIRBatch.timer.On)
	//		stIRBatch.timer.clear();
	//}




#endif

	//SHUTTLE BATCH AUTO START
	if(m_fnGetBatchStep(SHUTTLE) == STEP0 && 
		m_pPIO->readDI(IN_LCR_LIFT_DOWN,CARD_IO)==1 &&
		m_pPIO->readDI(IN_DCR_LIFT_DOWN, CARD_IO) == 1 &&
		m_pPIO->readDI(IN_LOAD_AREA_SENSOR1,CARD_IO)==0 &&  
		m_pPIO->readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)==0 && 
#ifdef _FI
		m_pPIO->readDI(IN_UNLOAD_CV_UP,CARD_IO)==1 && (stShuttleBatch.allow & SHUTTLE_ALL) == SHUTTLE_ALL  && (stShuttleBatch.request  & 0x3F ) > 0 && 
		m_pPIO->readDI(IN_OSC_LIFT_DOWN,CARD_IO)==1 && m_pPIO->readDI(IN_HIPOTIR_LIFT_DOWN,CARD_IO)==1 &&
		m_pPIO->readDI(IN_UNLOAD_CV_UP,CARD_IO)==1 &&
		m_pPIO->readDI(IN_LOAD_AREA_SENSOR2,CARD_IO)==0 &&
#else
		//LCR, DCR
		(stShuttleBatch.allow & 0x0A) == 0x0A  && (stShuttleBatch.request  & 0x0A ) > 0 && 
		G_MainWnd->m_AxisDlg.checkLCRAxisReadyPosition() == 1 &&
#endif
		 stShuttleBatch.holdShuttle == 0 
		)
	{
		if(!stShuttleBatch.timer.On){
			stShuttleBatch.timer.On = 1;
#ifdef _FI
			stShuttleBatch.timer.Set = 10; //1sec
			//stShuttleBatch.timer.Set = 400; //timedelay_20160106
#else
			stShuttleBatch.timer.Set = 50; //2sec
#endif
			stShuttleBatch.status = LOCKED;
		}else{
			if(stShuttleBatch.timer.OTE == 1)
			{
				stShuttleBatch.timer.clear();
				m_fnSetBatchStep(SHUTTLE, STEP1); //Start
			}
		}
	}else{
		if(m_fnGetBatchStep(SHUTTLE) == STEP0 && stShuttleBatch.timer.On){
			stShuttleBatch.status = CLEAR;
			stShuttleBatch.timer.clear();
		}
	}

	return 0;
}


int CInspectThread::stg1Load(void)
{	

	CString str;
	switch(m_fnGetBatchStep(LOAD))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(LOAD) != STEP0) 
		{
			m_fnSetBatchStep(LOAD, STEP0);
		}else
		{

		}
		break;

	case STEP1: //START
		stShuttleBatch.allow &= ~SHUTTLE_LOAD; 
		m_fnSetBatchStep(LOAD, STEP14); 
		break;

	case STEP14: //
		setBatchStatus(LOAD, FINISHED);
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LOAD]; //allow shuttle
		if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
			m_fnSetBatchStep(LOAD, STEP16);
		else
			m_fnSetBatchStep(LOAD, STEP15);

		break;

	case STEP15: //LOAD MANUAL END
		m_fnSetBatchStep(LOAD, STEP0); //
		break;
	case STEP16: //LOAD AUTO END
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 0 ); // INDEX NO.
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LOAD]; //allow shuttle
#ifdef _FI
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[LOAD]; //request shuttle
#endif
		m_fnSetBatchStep(LOAD, STEP17); //
		break;
	case STEP17: //LOAD WAIT SHUTTLE END
		//wait until shuttle end
		if(stShuttleBatch.request == 0)
		{
			m_fnSetBatchStep(LOAD, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(LOAD) != STEP19)
		{
			m_fnSetOldBatchStep(LOAD, m_fnGetPrevBatchStep(LOAD));
			m_fnSetBatchStep(LOAD, STEP19);
			G_MainWnd->m_PIODlg.m_btnErrorReset[LOAD].SetColorChange(RED, BLACK);
			errorRoutine();
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//errorResetRoutine();
		//error reset

		m_fnSetBatchStep(LOAD, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(LOAD))

	return 0;

}


int CInspectThread::stg4DCRTest(void)
{
	ViStatus ret;
	int iRet;
	CString str;
	long param;
	int iTest;

	switch (m_fnGetBatchStep(DCR))
	{
	case STEP0: //READY
		if (m_fnGetPrevBatchStep(DCR) != STEP0)
		{
			m_fnSetBatchStep(DCR, STEP0);
		}
		else
		{

		}
		break;

	case STEP1: //START
		//20160308_softdetection_add
		m_PartData[STG_DCR].SoftDetect = 1;
		//
		stShuttleBatch.allow &= ~SHUTTLE_DCR; //not allowed shuttle
		m_fnSetInspectionStep(DCR, 0);
		stDCRBatch.retry = 0;
		stLCRBatch.NG = 0;
		stLCRBatch.VISAFault = 0;
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_DCR);
		//160219_kys_UiDelete_add
#ifdef _FI
		if (m_fnGetBatchStep(OSC) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_OSC);
		}
#else
		if (m_fnGetBatchStep(LCR) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_LCR);
		}
#endif
		//
		iTest = 0;
		for (int i = 0; i < MAX_DCR_TEST; i++)
		{
			if (m_pRecipeSetDlg->part_Test.dcrTestInfo[i].bTest){
				iTest++;
			}
		}
		if (iTest > 0)
		{
			m_fnSetBatchStep(DCR, STEP2);
		}
		else{
#ifdef _FI
			m_fnSetBatchStep(DCR, STEP14);
#else
			m_fnSetBatchStep(DCR, STEP14);
#endif
		}
		break;
	case STEP2: //JIG UP
		if (m_fnGetPrevBatchStep(DCR) != STEP2)
		{
			iRet = m_pPIO->writeDO(1, OUT_DCR_LIFT_UP, CARD_IO);
			m_fnSetBatchStep(DCR, STEP2);
		}
		else
		{	//wait till lift up
			if (m_pPIO->readDI(IN_DCR_LIFT_UP, CARD_IO) == 1)//UP
			{
				//TIMER 0.5S
				if (!stDCRBatch.timer.On){
					stDCRBatch.timer.On = 1;
					stDCRBatch.timer.Set = 200;//stDCRBatch.timer.Set = 500;   //1021_KYS
				}
				else{
					if (stDCRBatch.timer.OTE == 1)
					{
						stDCRBatch.timer.clear();
						stDCRBatch.counter.clear();
						m_fnSetBatchStep(DCR, STEP3);
					}
				}
			}
			else{
				if (!stDCRBatch.counter.On){
					stDCRBatch.counter.On = 1;
					stDCRBatch.counter.Set = 200;// 20sec
				}
				else{
					if (stDCRBatch.counter.OTE == 1)
					{
						stDCRBatch.timer.clear();
						stDCRBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 30);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 30);
						m_fnSetBatchStep(DCR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_DCR_LIFT_UP, CARD_IO)==1)//UP
		}
		break;
	case STEP3: //
		if (stDCRBatch.dcrStep < MAX_DCR_TEST){
			if (m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].bTest){
				if (fDCRZeroAdjust)
					m_fnSetBatchStep(DCR, STEP8);
				else{

					if (!stDCRBatch.timer.On){
						stDCRBatch.timer.On = 1;
						stDCRBatch.timer.Set = 300;//stDCRBatch.timer.Set = 500;   //1021_KYS
					}
					else{
						if (stDCRBatch.timer.OTE == 1)
						{
							stDCRBatch.timer.clear();
							stDCRBatch.counter.clear();

							m_fnSetInspectionStep(DCR, stDCRBatch.dcrStep);
							m_fnSetBatchStep(DCR, STEP7);
						}
					}



				}
			}
			else{
				m_fnSetInspectionStep(DCR, ++stDCRBatch.dcrStep);
			}
		}
		else{
			//goto JIG DOWN
			//1113_MERGE_ADD			
			if (fDCRZeroAdjust){
				G_MainWnd->m_DataHandling.m_FlashData.data[DCR_ZERO_ADJUST] = 1;
			}

#ifdef _FI
			m_fnSetBatchStep(DCR, STEP10);
#else //공정검사
			if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
				m_fnSetBatchStep(DCR, STEP10);
			}else{
				m_fnSetBatchStep(DCR, STEP4);
			}
#endif //#ifdef _FI

		}
		break;

	case STEP4: //OK NG CHECK
		if (m_pPIO->readDI(IN_DCR_JIG_DETECT, CARD_IO) == 1)
		{
			if (m_fnGetPrevBatchStep(DCR) != STEP4)
			{
				//Tact time display
				CString strTact;
				CTime ctimeOut = CTime::GetCurrentTime();
				CTimeSpan ctimeTact = ctimeOut - ctUnloadTime;
				ctUnloadTime = ctimeOut;
				strTact = ctimeTact.Format(L"%H:%M:%S");
				G_MainWnd->m_ServerDlg.m_staticTimeMasterElapsec.SetText(strTact, WHITE, BLACK);
				m_fnSetBatchStep(DCR, STEP4);
				int fail = summarizeResult(); //OK/NG Display
				G_MainWnd->m_DataHandling.m_fnDayReportWrite(TRUE);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_STATUS_UPDATE, NULL);
				if (!fail){//Pass
					m_fnSetBatchStep(DCR, STEP10); //JIG DOWN
				}
				else{//Fail
					m_fnSetBatchStep(DCR, STEP5);
				}
			}
		}
		else{
			//jig out
			//goto step 11
			if (!stDCRBatch.timer.On){

				stDCRBatch.timer.On = 1;
				stDCRBatch.timer.Set = 20;
			}
			else{
				if (stDCRBatch.timer.OTE == 1)
				{
					stDCRBatch.timer.clear();
					m_fnSetBatchStep(DCR, STEP10);
				}
			}
		}//
		break;

	case STEP5: //NG JIG CLEAR
		if (m_pPIO->readDI(IN_DCR_JIG_DETECT, CARD_IO) != 0)
		{
			//wait until other batch test finished
			//#ifndef _VI_SKIP
			//			if(( stShuttleBatch.allow & 0x42 ) == 0x42){//LCR, VI TEST DONE
			//#else
			//			if(( stShuttleBatch.allow & 0x02 ) == 0x02){//LCR, VI TEST DONE
			//#endif
			if ((G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1 && ((stShuttleBatch.allow & 0x42) == 0x42)) ||
				(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 0 && ((stShuttleBatch.allow & 0x02) == 0x02))
				){

				//NG data display
				//showNGResult();
				//fail alarm,message
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 32);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 34);
				//Manual mode
				G_MainWnd->m_ServerDlg.m_btnNGOut.SetColorChange(YELLOW, BLACK);
				G_MainWnd->m_ServerDlg.m_btnNGClear.SetColorChange(YELLOW, BLACK);
				checkStatus = CHECK_NG;
				m_fnSetBatchStep(DCR, STEP9);
			}
		}
		else{ //no jig
			if (m_pPIO->readDI(IN_DCR_LIFT_DOWN, CARD_IO) == 1)
				m_fnSetBatchStep(DCR, STEP14);
			else
				m_fnSetBatchStep(DCR, STEP10);
		}
		break;

		//DCR TEST
	case STEP7: //
		ret = m_pInstrumentDlg->measureDcr(stDCRBatch.dcrStep, m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep]);
			if (ret == VI_SUCCESS){
			//Result
			double r = atof(m_pInstrumentDlg->vi[VI_DCR].str_result);
			if (G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
				r += G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[stDCRBatch.dcrStep].offset*(+1.0E-3);
				//G_AddLog(3,L"HIPOT MEASURE OFFSET APPLIED [%f] ", G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[stDCRBatch.dcrStep].offset);
			}
			m_PartData[STG_DCR].r[stDCRBatch.dcrStep] = r;

			if (m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].upperLimit > 0 && m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].lowerLimit > 0)
			{
				if (r <= m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].upperLimit*(+1.0E-3) &&
					m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].lowerLimit*(+1.0E-3) <= r){
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 1;
				}
				else{
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 0;
					stDCRBatch.NG++;
				}
			}
			else	if (m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].upperLimit > 0)
			{
				if (r <= m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].upperLimit*(+1.0E-3)){
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 1;
				}
				else{
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 0;
					stDCRBatch.NG++;
				}
			}
			else if (m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].lowerLimit > 0)
			{
				if (m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep].lowerLimit*(+1.0E-3) <= r){
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 1;
				}
				else{
					m_PartData[STG_DCR].rPass[stDCRBatch.dcrStep] = 0;
					stDCRBatch.NG++;
				}
			}

			param = STG_DCR * 0x100 + stDCRBatch.dcrStep;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);

			m_fnSetInspectionStep(DCR, ++stDCRBatch.dcrStep);
			m_fnSetBatchStep(DCR, STEP3);
		}
		else{ //test error
//1125_KYS_
#ifdef _FI
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 31);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 31);
#endif
//
			stDCRBatch.VISAFault++;
			m_fnSetBatchStep(DCR, STEP19); break;
		}
		break;

	case STEP8: //Zero Adjustment
		ret = m_pInstrumentDlg->zeroAdjustDcr(stDCRBatch.dcrStep, m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep]);
		if(ret == 0){//succeded
			m_fnSetInspectionStep(DCR, ++stDCRBatch.dcrStep);
			m_fnSetBatchStep(DCR, STEP3);
			G_AddLog(3,L"DCR Zero Adjustment [%d] succeded", stDCRBatch.dcrStep);
		}else{ //failed
			stDCRBatch.NG=1;
			//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 32 );
			//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 32 );
			G_AddLog(3,L"DCR Zero Adjustment [%d] failed", stDCRBatch.dcrStep);
			m_fnSetBatchStep(DCR, STEP10);
		}
		break;

	case STEP9: //공정검사 NG part out check
		if(m_fnGetPrevBatchStep(DCR) != STEP9)
		{
			if(m_pPIO->readDI(IN_DCR_JIG_DETECT, CARD_IO) == 0)
			{
				m_fnSetBatchStep(DCR, STEP9);
			}else{
				m_fnSetBatchStep(DCR, STEP5);
			}			
		}
		else
		{
			if(!stDCRBatch.timer.On){
				stDCRBatch.timer.On = 1;
				stDCRBatch.timer.Set = 10;//TIMER 1S
			}else{
				if(stDCRBatch.timer.OTE == 1)
				{
					stDCRBatch.timer.clear();
					//clear grid
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_SHIFT_UPDATE, NULL);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 33 );
					if(m_pPIO->readDI(IN_DCR_LIFT_DOWN, CARD_IO)==1)
						m_fnSetBatchStep(DCR, STEP15);
					else
						m_fnSetBatchStep(DCR, STEP10);
				}
			}
		}
		break;

	case STEP10: //LIFT DOWN
		if(m_fnGetPrevBatchStep(DCR) != STEP10)
		{
			Sleep(200);//1123_KYS
			iRet = m_pPIO->writeDO(1, OUT_DCR_LIFT_DOWN,CARD_IO);
			m_fnSetBatchStep(DCR, STEP10);
		}
		else
		{	//wait till lift DOWN
			if(m_pPIO->readDI(IN_DCR_LIFT_DOWN, CARD_IO)==1)//DOWN
			{
				if(!stDCRBatch.timer.On){
					stDCRBatch.timer.On = 1;
					stDCRBatch.timer.Set = 10;//TIMER 1S
				}else{
					if(stDCRBatch.timer.OTE == 1)
					{
						stDCRBatch.timer.clear();
						stDCRBatch.counter.clear();
						m_fnSetBatchStep(DCR, STEP12);
					}
				}
			}else{
				if(!stDCRBatch.counter.On){
					stDCRBatch.counter.On = 1;
					stDCRBatch.counter.Set = 200;// 20sec
				}else{
					if(stDCRBatch.counter.OTE == 1)
					{
						stDCRBatch.timer.clear();
						stDCRBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 30 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 30 );
						m_fnSetBatchStep(DCR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_DCR_LIFT_DOWN, CARD_IO)==1)//DOWN
		}
		break;

		//retry?
	case STEP12: //
		if(stDCRBatch.NG){
			stDCRBatch.NG = 0;
			if(stDCRBatch.retry++ < G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_RETEST_COUNT])
			{
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_DCR);
				m_fnSetInspectionStep(DCR, 0);//
				m_fnSetBatchStep(DCR, STEP2);
			}else
			{
				//NG final
				m_PartData[STG_DCR].NG++;
				m_fnSetBatchStep(DCR, STEP14);
			}
		}else{
			m_fnSetBatchStep(DCR, STEP14);
		}
		break;

		//
	case STEP14: //
		if(m_fnGetPrevBatchStep(DCR) != STEP14)
		{
			setBatchStatus(DCR, FINISHED);
			stDCRBatch.timer.On = 1; 
			stDCRBatch.timer.Set = 10; 
			m_fnSetBatchStep(DCR, STEP14);
		}
		else
		{
			if(stDCRBatch.timer.OTE == 1)
			{
				stDCRBatch.timer.clear();
				if(fDCRZeroAdjust )fDCRZeroAdjust = 0;
				stShuttleBatch.allow |= SHUTTLE_DCR; //allow shuttle
				m_fnSetInspectionStep(DCR, 0);
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(DCR, STEP16);
				else
					m_fnSetBatchStep(DCR, STEP15);
			}
		}
		break;

	case STEP15: //DCR MANUAL END
		stShuttleBatch.allow |= SHUTTLE_DCR; //allow shuttle
		m_fnSetBatchStep(DCR, STEP0); //
		break;
	case STEP16: //DCR AUTO END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[DCR]; //allow shuttle
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[DCR]; //request shuttle
		m_fnSetBatchStep(DCR, STEP17); //	
		break;
	case STEP17: //DCR WAIT SHUTTLE END
		//wait until shuttle end
		if(stShuttleBatch.request == 0)
		{
			stShuttleBatch.allow |= SHUTTLE_DCR; //allow shuttle
			m_fnSetBatchStep(DCR, STEP0);
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(DCR) != STEP19)
		{
			m_fnSetOldBatchStep(DCR, m_fnGetPrevBatchStep(DCR));
			m_fnSetBatchStep(DCR, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[DCR].SetColorChange(RED, BLACK);
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		stDCRBatch.timer.clear();
		//clear error queue and register
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_DCR);
		//1125_JHS_ADD
		ret = m_pInstrumentDlg->measureDcr(stDCRBatch.dcrStep, m_pRecipeSetDlg->part_Test.dcrTestInfo[stDCRBatch.dcrStep]);
		m_fnSetBatchStep(DCR, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(DCR))

	return 0;
}

int CInspectThread::stg6VITest(void)
{
	long param;
	CString str;
	int iTest;
	switch(m_fnGetBatchStep(VI))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(VI) != STEP0)
		{
			m_fnSetBatchStep(VI, STEP0);
		}else
		{
		}
		break;
	case STEP1: //START
		stShuttleBatch.allow &= ~SHUTTLE_VI; //not allowed shuttle
		G_MainWnd->m_ServerDlg.clearGridMainResult(STG_VI_UNLOAD);
		iTest = 0;
		for (int i = 0; i < MAX_VI_TEST; i++)
		{
			if (m_pRecipeSetDlg->part_Test.viTestInfo[i].bTest){
				iTest++;
			}
		}
		if (iTest > 0)
		{
			m_fnSetBatchStep(VI, STEP2);
		}
		else{
			m_fnSetBatchStep(VI, STEP14);
		}
		break;

	case STEP2: //LIGHT ON
		if(m_fnGetPrevBatchStep(VI) != STEP2) 
		{
			turnLight(ON);
			if(!stVIBatch.timer.On){
				stVIBatch.timer.On = 1;
				stVIBatch.timer.Set = 10;//
			}
			m_fnSetBatchStep(VI, STEP2);
		}
		else
		{	
			if(stVIBatch.timer.OTE == 1)
			{
				stVIBatch.timer.clear();
				m_fnSetBatchStep(VI, STEP5);
			}
		}
		break;
	case STEP3: //
		m_fnSetBatchStep(VI, STEP4);
		break;

		//
	case STEP4: //
		if(m_fnGetPrevBatchStep(VI) != STEP4) 
		{
			m_fnSetBatchStep(VI, STEP4);
		}
		else
		{
			if(0) //
				m_fnSetBatchStep(VI, STEP5);
		}
		break;

		//VI TEST START
	case STEP5: //
		if(m_fnGetPrevBatchStep(VI) != STEP5) 
		{
			if (G_MainWnd->m_InspectThread.m_vi.camConnected != CAM_CONNECTED)
			{
				AfxMessageBox(_T("Camera Disconnected."), MB_ICONHAND);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 62);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 62);
				m_fnSetBatchStep(VI, STEP19);
			}

			::PostMessage(m_vi.m_hWnd, UM_VI, (WPARAM)UW_VINSPECTION, NULL);
			if(!stVIBatch.timer.On){
				stVIBatch.timer.On = 1;
				stVIBatch.timer.Set = 200;//Time out
			}
			m_fnSetBatchStep(VI, STEP5);
		}
		else
		{
			if(m_vi.GetUMMsgResponse(UW_VINSPECTION) == UM_VI_DONE)	
			{//
				m_vi.SetUMMsgResponse(UW_VINSPECTION, UM_VI_CLEAR);
				stVIBatch.timer.clear();
				m_fnSetBatchStep(VI, STEP7);
			}else if(m_vi.GetUMMsgResponse(UW_VINSPECTION) == UM_VI_ERROR){//ERROR
				//Error
				stVIBatch.timer.clear();
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 61 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 61 );
				m_fnSetBatchStep(VI, STEP19);
				m_vi.SetUMMsgResponse(UW_VINSPECTION, UM_VI_CLEAR);
			}

			if(stVIBatch.timer.OTE == 1)//TIME OUT
			{
				stVIBatch.timer.clear();
				//Error
				//1117_kys
#ifndef _FI
				if ((G_MainWnd->m_InspectThread.stShuttleBatch.allow & 0x0A) == 0x0A){				//1121_KYS_LCR+DCR
#else
				if ((G_MainWnd->m_InspectThread.stShuttleBatch.allow & 0x0A) == 0x3E){				//1121_KYS_LCR+OSC+DCR+IR+HIPOT
#endif			

					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 60);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 60);
					m_fnSetBatchStep(VI, STEP19);
					m_vi.SetUMMsgResponse(UW_VINSPECTION, UM_VI_CLEAR);
//#ifndef _FI
				}
//#endif
			}
		}
		break;

		//VI TEST RESULT
	case STEP7: //
		//## VISION INSPECTION ##//

		if (m_vi.nPass){ //VI RESULT
			m_PartData[STG_VI_UNLOAD].viPass[0] = 1;
			G_AddLog(3,L"VISION INSPECTION PASS ");
		}else{
			m_PartData[STG_VI_UNLOAD].viPass[0] = 0;
			stVIBatch.NG=1;
			G_AddLog(3,L"VISION INSPECTION FAIL ");
		}
		param = STG_VI_UNLOAD * 0x100 + 0;
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
		m_fnSetBatchStep(VI, STEP10);
		break;

	case STEP10: //
		turnLight(OFF);
		m_fnSetBatchStep(VI, STEP14);
		break;

	case STEP14: //FINISH
		if(m_fnGetPrevBatchStep(VI) != STEP14)
		{
			setBatchStatus(VI, FINISHED);
			stVIBatch.timer.On = 1; //1000ms 
			stVIBatch.timer.Set = 10;
			m_fnSetBatchStep(VI, STEP14);
		}
		else
		{
			if(stVIBatch.timer.OTE == 1)
			{
				stVIBatch.timer.clear();
				stShuttleBatch.allow |= SHUTTLE_VI; //allow shuttle
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(VI, STEP16);
				else
					m_fnSetBatchStep(VI, STEP15);
			}
			//count up and error
		}
		break;

	case STEP15: //VI MANUAL END
		stShuttleBatch.allow |= SHUTTLE_VI; //allow shuttle
		m_fnSetBatchStep(VI, STEP0); //
		break;
	case STEP16: //VI AUTO END
		stShuttleBatch.allow |= SHUTTLE_VI; //allow shuttle
		m_fnSetBatchStep(UNLOAD, STEP1);
		m_fnSetBatchStep(VI, STEP0); //
		break;
	case STEP17: 

		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(VI) != STEP19)
		{
			m_fnSetOldBatchStep(VI, m_fnGetPrevBatchStep(VI));
			m_fnSetBatchStep(VI, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[VI].SetColorChange(RED, BLACK);
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		m_vi.SetUMMsgResponse(UW_VINSPECTION, UM_VI_CLEAR);
		//error reset
		stVIBatch.timer.clear();
		m_fnSetBatchStep(VI, STEP22);
		break;

	case STEP22:   //HOLD

		break;
	}//switch(m_fnGetBatchStep(VI))
	return 0;
}

#ifdef _FI

int CInspectThread::stg3OSCTest(void)
{
	ViStatus ret;
	int iRet;
	CString str;
	long param;


	switch(m_fnGetBatchStep(OSC))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(OSC) != STEP0)
		{
			m_fnSetBatchStep(OSC, STEP0);
		}else
		{

		}
		break;

	case STEP1: //START
		stOSCBatch.timer.clear();
		stShuttleBatch.allow &= ~SHUTTLE_OSC; //not allowed shuttle
		stOSCBatch.oscStep = 0;
		stOSCBatch.retry = 0;
		stOSCBatch.NG = 0;
		stOSCBatch.VISAFault = 0; 
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_OSC);
		//160219_kys_UiDelete_add
		if (m_fnGetBatchStep(LCR) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_LCR);
		}

		if(stOSCBatch.oscStep < MAX_OSC_TEST){
			if(m_pRecipeSetDlg->part_Test.oscTestInfo[stOSCBatch.oscStep].bTest){
				m_fnSetBatchStep(OSC, STEP2);
			}else{
				m_fnSetBatchStep(OSC, STEP14);
			}
		}
		G_AddLog(3,L"OSC Batch Cycle START ");
		break;
	case STEP2: //JIG UP
		if(m_fnGetPrevBatchStep(OSC) != STEP2)
		{
			stOSCBatch.oscStep = 0;//
			iRet = m_pPIO->writeDO(1, OUT_OSC_LIFT_UP,CARD_IO);
			m_fnSetBatchStep(OSC, STEP2);
		}
		else
		{	//wait till lift up
			if(m_pPIO->readDI(IN_OSC_LIFT_UP, CARD_IO)==1)//UP
			{
				//TIMER 0.5S
				if(!stOSCBatch.timer.On){
					stOSCBatch.timer.On = 1;
					stOSCBatch.timer.Set = 200; //1021_KYS
					//stOSCBatch.timer.Set = 200;
				}else{
					if(stOSCBatch.timer.OTE == 1)
					{
						stOSCBatch.timer.clear();
						stOSCBatch.counter.clear();
						m_fnSetBatchStep(OSC, STEP3);
						G_AddLog(3,L"OSC JIG UP ");
					}
				}
			}else{
				if(!stOSCBatch.counter.On){
					stOSCBatch.counter.On = 1;
					stOSCBatch.counter.Set = 1000;// 20sec
				}else{
					if(stOSCBatch.counter.OTE == 1)
					{
						stOSCBatch.timer.clear();
						stOSCBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 20 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 20 );
						m_fnSetBatchStep(OSC, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_OSC_LIFT_UP, CARD_IO)==1)//UP
		}
		break;
	case STEP3: //
		if(stOSCBatch.oscStep < MAX_OSC_TEST){
			if(m_pRecipeSetDlg->part_Test.oscTestInfo[stOSCBatch.oscStep].bTest){
				m_fnSetInspectionStep(OSC,stOSCBatch.oscStep);
				m_fnSetBatchStep(OSC, STEP7);
			}else{
				stOSCBatch.oscStep++;
			}
		}else{
			//goto JIG DOWN
			m_fnSetBatchStep(OSC, STEP10);
		}
		break;

		//OSC TEST
	case STEP7: //
		ret = m_pInstrumentDlg->measureOsc(stOSCBatch.oscStep, m_pRecipeSetDlg->part_Test.oscTestInfo[stOSCBatch.oscStep]);
		if(ret == VI_SUCCESS){
			//Result
			swprintf_s(m_PartData[STG_OSC].phase, L"%s", ConvertMultybyteToUnicode(m_pInstrumentDlg->vi[VI_OSC].str_result));
			CString str(ConvertMultybyteToUnicode(m_pInstrumentDlg->vi[VI_OSC].str_result));
			if(str.Left(2) == L"OK"){
				m_PartData[STG_OSC].phasePass[stOSCBatch.oscStep] = 1;
				param = STG_OSC * 0x100 + stOSCBatch.oscStep;
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
				stOSCBatch.NG = 0;
				stOSCBatch.oscStep++;
				m_fnSetBatchStep(OSC, STEP3);  
				G_AddLog(3,L"OSC MEASUREMENT PASS ");
			}else{
				m_PartData[STG_OSC].phasePass[stOSCBatch.oscStep] = 0;
				//result NG
				param = STG_OSC * 0x100 + stOSCBatch.oscStep;
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
				G_AddLog(3,L"OSC MEASUREMENT FAIL ");
				if(stOSCBatch.NG++ < 1)
				{
					m_fnSetBatchStep(OSC, STEP3); //one more
					break;
				}
			}
			m_fnSetBatchStep(OSC, STEP10);
			break;

		}else{ 
			//VI test error
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 21 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 21 );
			stOSCBatch.VISAFault++;
			m_fnSetBatchStep(OSC, STEP19); break;
		}
		break;

	case STEP10: //JIG DOWN
		
		if(m_fnGetPrevBatchStep(OSC) != STEP10)
		{
			Sleep(2000);//1123_KYS
			iRet = m_pPIO->writeDO(1, OUT_OSC_LIFT_DOWN,CARD_IO);
			m_fnSetBatchStep(OSC, STEP10);
		}
		else
		{	//wait till lift DOWN
			if(m_pPIO->readDI(IN_OSC_LIFT_DOWN, CARD_IO)==1)//DOWN
			{
				if(!stOSCBatch.timer.On){
					stOSCBatch.timer.On = 1;
					stOSCBatch.timer.Set = 100;//TIMER 1S
				}else{
					if(stOSCBatch.timer.OTE == 1)
					{
						stOSCBatch.timer.clear();
						stOSCBatch.counter.clear();
						m_fnSetBatchStep(OSC, STEP12);
						G_AddLog(3,L"OSC JIG DOWN ");
					}
				}
			}else{
				if(!stOSCBatch.counter.On){
					stOSCBatch.counter.On = 1;
					stOSCBatch.counter.Set = 200;// 20sec
				}else{
					if(stOSCBatch.counter.OTE == 1)
					{
						stOSCBatch.timer.clear();
						stOSCBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 20 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 20 );
						m_fnSetBatchStep(OSC, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_OSC_LIFT_DOWN, CARD_IO)==1)//DOWN
		}
		break;

		//retry?
	case STEP12: //
		if(stOSCBatch.NG){
			stOSCBatch.NG = 0;
			if(stOSCBatch.retry++ < G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_RETEST_COUNT])
			{
				m_fnSetBatchStep(OSC, STEP2);
			}else
			{
				//NG final
				m_PartData[STG_OSC].NG++;

				m_fnSetBatchStep(OSC, STEP14);
			}
		}else{
			m_fnSetBatchStep(OSC, STEP14);
		}
		break;

	case STEP14: //
		if(m_fnGetPrevBatchStep(OSC) != STEP14)
		{
			m_fnSetInspectionStep(OSC,0);
			setBatchStatus(OSC, FINISHED);
			stOSCBatch.timer.On = 1;
			stOSCBatch.timer.Set = 10;
			m_fnSetBatchStep(OSC, STEP14);
		}
		else
		{
			if(stOSCBatch.timer.OTE == 1)
			{
				stShuttleBatch.allow |= SHUTTLE_OSC; //allow shuttle
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(OSC, STEP16);
				else
					m_fnSetBatchStep(OSC, STEP15);
			}
		}
		break;

	case STEP15: //OSC MANUAL END
		stShuttleBatch.allow |= SHUTTLE_OSC; //allow shuttle
		m_fnSetBatchStep(OSC, STEP0); //
		break;
	case STEP16: //OSC AUTO END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[OSC]; //allow shuttle
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[OSC]; //request shuttle
		m_fnSetBatchStep(OSC, STEP17); //
		G_AddLog(3,L"OSC AUTO RUN SHUTTLE REQUEST ");
		break;
	case STEP17: //OSC WAIT SHUTTLE END
		//wait until shuttle request cleared
		if(stShuttleBatch.request == 0)
		{
			stShuttleBatch.allow |= SHUTTLE_OSC; //allow shuttle
			m_fnSetBatchStep(OSC, STEP0);
			G_AddLog(3,L"OSC AUTO RUN BATCH END ");
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(OSC) != STEP19)
		{
			m_fnSetOldBatchStep(OSC, m_fnGetPrevBatchStep(OSC));
			m_fnSetBatchStep(OSC, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[OSC].SetColorChange(RED, BLACK);
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		stOSCBatch.retry = 0;
		stOSCBatch.timer.clear();
		//clear error queue and register
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_OSC);
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_WG);
		m_fnSetBatchStep(OSC, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(OSC))

	return 0;
}


int CInspectThread::stg5HIPOTTest(void)
{
	ViStatus ret;
	int iRet;
	CString str;
	long param;
	int nWait = 0;
	int i, iTest;

	switch(m_fnGetBatchStep(HIPOT))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(HIPOT) != STEP0) 
		{
			m_fnSetBatchStep(HIPOT, STEP0);
		}
		break;

	case STEP1: //START
		stShuttleBatch.allow &= ~SHUTTLE_HIPOT; //not allowed shuttle
		m_fnSetInspectionStep(HIPOT, 0);
		stHIPOTBatch.retry = 0;
		stHIPOTBatch.NG = 0;
		stHIPOTBatch.VISAFault = 0; 
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_HIPOT_IR);
		//160219_kys_UiDelete_add
		if (m_fnGetBatchStep(DCR) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_DCR);
		}
		//
		iTest = 0;
		for(i = 0; i < MAX_HIPOT_TEST ; i++){
			if(m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep].bTest){
				iTest++;
			}
		}
		if(iTest > 0 )
		{
			if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_HIPOT_TEST_LATER] == 1){
				//160118_KYS
				//m_fnSetBatchStep(HIPOT, STEP13);
				m_fnSetBatchStep(HIPOT, STEP2);
			}
			else{
				m_fnSetBatchStep(HIPOT, STEP2);
			}
		}else{
			m_fnSetBatchStep(HIPOT, STEP14);
		}
		G_AddLog(3,L"HIPOT BATCH START ");
		break;

	case STEP13:
		bool hipotGoodToGo;
		//160119_KYS-->OSC, HIPOT 동시 측정시 OSC 측정에 영향을 줌.
		if(//(m_pPIO->readDI(IN_LCR_JIG_DETECT,CARD_IO) ==0 || (m_pPIO->readDI(IN_LCR_JIG_DETECT,CARD_IO)==1 && stLCRBatch.status == FINISHED)) &&
		   (m_pPIO->readDI(IN_OSC_JIG_DETECT,CARD_IO) ==0 || (m_pPIO->readDI(IN_OSC_JIG_DETECT,CARD_IO)==1 && stOSCBatch.status == FINISHED)) 
		   //&& (m_pPIO->readDI(IN_DCR_JIG_DETECT,CARD_IO) ==0 || (m_pPIO->readDI(IN_DCR_JIG_DETECT,CARD_IO)==1 && stDCRBatch.status == FINISHED))
		  )
		{
			hipotGoodToGo = true;
		}else{
			hipotGoodToGo = false;
		}

		if (hipotGoodToGo){
			//160118_KYS
			//m_fnSetBatchStep(HIPOT, STEP2);
			m_fnSetBatchStep(HIPOT, STEP3);
		}
		break;

	case STEP2: //JIG UP
		if(m_fnGetPrevBatchStep(HIPOT) != STEP2)
		{
			iRet = m_pPIO->writeDO(1, OUT_HIPOTIR_LIFT_UP,CARD_IO);
			m_fnSetBatchStep(HIPOT, STEP2);
		}
		else
		{	//wait till lift up
			if(m_pPIO->readDI(IN_HIPOTIR_LIFT_UP, CARD_IO)==1)//UP
			{
				//TIMER 0.5S
				if(!stHIPOTBatch.timer.On){
					stHIPOTBatch.timer.On = 1;
					stHIPOTBatch.timer.Set = 5;
				}else{
					if(stHIPOTBatch.timer.OTE == 1)
					{
						stHIPOTBatch.timer.clear();
						stHIPOTBatch.counter.clear();
						
						//160119_KYS
						if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_HIPOT_TEST_LATER] == 1){
							//160118_KYS
							m_fnSetBatchStep(HIPOT, STEP13);
							//m_fnSetBatchStep(HIPOT, STEP2);
						}
						else{
							m_fnSetBatchStep(HIPOT, STEP3);
						}
						G_AddLog(3,L"HIPOT JIG UP ");

					}
				}
			}else{
				if(!stHIPOTBatch.counter.On){
					stHIPOTBatch.counter.On = 1;
					stHIPOTBatch.counter.Set = 200;// 20sec
				}else{
					if(stHIPOTBatch.counter.OTE == 1)
					{
						stHIPOTBatch.timer.clear();
						stHIPOTBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 40 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 40 );
						m_fnSetBatchStep(HIPOT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_HIPOT_LIFT_UP, CARD_IO)==1)//UP
		}
		break;

	case STEP3: //
		if(stHIPOTBatch.hiPotStep < MAX_HIPOT_TEST){
			if(m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep].bTest){
				m_fnSetBatchStep(HIPOT, STEP4);
				m_fnSetInspectionStep(HIPOT, stHIPOTBatch.hiPotStep);
			}else{
				m_fnSetInspectionStep(HIPOT, ++stHIPOTBatch.hiPotStep);
			}
		}else{
			if(1){//continue ir test?
				m_fnSetBatchStep(HIPOT, STEP8);
			}else{
				//GO TO JIG DOWN
				m_fnSetBatchStep(HIPOT, STEP10);
			}
		}
		break;

		//MEASURE STEP SWITCHING
	case STEP4: //
		if(m_fnGetPrevBatchStep(HIPOT) != STEP4)
		{
			iRet = G_MainWnd->m_PIODlg.switchRelayHipot(ON, stHIPOTBatch.hiPotStep);
			if(iRet != TMC_RV_OK ){
				//Error Msg
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 42 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 42 );
				m_fnSetBatchStep(HIPOT, STEP19); break;
			}
			m_fnSetBatchStep(HIPOT, STEP4);
		}else{
			//TIMER 0.5S
			if(!stHIPOTBatch.timer.On){
				stHIPOTBatch.timer.On = 1;
				stHIPOTBatch.timer.Set = 2;
			}else{
				if(stHIPOTBatch.timer.OTE == 1)
				{
					stHIPOTBatch.timer.clear();
					stHIPOTBatch.counter.clear();
					m_fnSetBatchStep(HIPOT, STEP5);
					G_AddLog(3,L"HIPOT SWITCHING RELAY ");
				}
			}
		}
		break;

		//HIPOT TEST START
	case STEP5: //
		ret = m_pInstrumentDlg->measureHipotStart(stHIPOTBatch.hiPotStep, m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep]);
		if(ret != VI_SUCCESS){
			//Error 
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 41 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 41 );
			m_fnSetBatchStep(HIPOT, STEP19); break;
		}
		nWait = 0;
		m_fnSetBatchStep(HIPOT, STEP6);
		G_AddLog(3,L"HIPOT MEASURE START ");
		break;

		//HIPOT TEST STATUS
	case STEP6: //
		ret = m_pInstrumentDlg->measureHipotStatus(stHIPOTBatch.hiPotStep, m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep]);
		if(ret == 0){//ended "STOPPED"
			m_fnSetBatchStep(HIPOT, STEP7);
			G_AddLog(3,L"HIPOT MEASURE STOPPED ");
		}
		//timeout here
		if(nWait++ > 100){//Time Out Error
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 41 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 41 );
			m_fnSetBatchStep(HIPOT, STEP19); break;
		}
		break;

		//HIPOT TEST RESULT
	case STEP7: //
		ret = m_pInstrumentDlg->measureHipotResult(stHIPOTBatch.hiPotStep, m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep]);
		if(ret != VI_SUCCESS){
			//Error 
			iRet = G_MainWnd->m_PIODlg.switchRelayHipot(OFF, stHIPOTBatch.hiPotStep);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 41 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 41 );
			m_fnSetBatchStep(HIPOT, STEP19); break;
		}
		//RELAY OFF

		iRet = G_MainWnd->m_PIODlg.switchRelayHipot(OFF, stHIPOTBatch.hiPotStep);
		if(iRet != TMC_RV_OK ){
			//Error Msg
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 42 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 42 );
			m_fnSetBatchStep(HIPOT, STEP19); break;
		}

		if(ret == VI_SUCCESS){
			//Result
			double i = atof(m_pInstrumentDlg->vi[VI_HIP].str_result);
			if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
				i += G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[stHIPOTBatch.hiPotStep].offset*(+1.0E-3);
				G_AddLog(3,L"HIPOT MEASURE OFFSET APPLIED [%f] ", G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[stHIPOTBatch.hiPotStep].offset);
			}
			m_PartData[STG_HIPOT_IR].i[stHIPOTBatch.hiPotStep] = i;
			if( i <=  m_pRecipeSetDlg->part_Test.hipTestInfo[stHIPOTBatch.hiPotStep].upperLimit*(+1.0E-3)){ //mA
				m_PartData[STG_HIPOT_IR].iPass[stHIPOTBatch.hiPotStep] = 1;
				G_AddLog(3,L"HIPOT MEASURE PASS [%f] ", m_PartData[STG_HIPOT_IR].i[stHIPOTBatch.hiPotStep]);
			}else{
				m_PartData[STG_HIPOT_IR].iPass[stHIPOTBatch.hiPotStep] = 0;
				G_AddLog(3,L"HIPOT MEASURE FAIL [%f] ", m_PartData[STG_HIPOT_IR].i[stHIPOTBatch.hiPotStep]);
				stHIPOTBatch.NG=1;
			}

			param = STG_HIPOT_IR * 0x100 + stHIPOTBatch.hiPotStep;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);

			m_fnSetInspectionStep(HIPOT, ++stHIPOTBatch.hiPotStep);
			m_fnSetBatchStep(HIPOT, STEP3);
		}
		break;

	case STEP8: //connect IR test

		setBatchStatus(HIPOT, CONTINUE);
		setBatchStatus(IR, CONTINUE);
		setBatchStatus(HIPOT, FINISHED);
		stShuttleBatch.allow &= ~SHUTTLE_IR; //not allowed shuttle
		m_fnSetBatchStep(IR, STEP8);
		m_fnSetBatchStep(HIPOT, STEP15);
		m_fnSetInspectionStep(HIPOT, 0);
		// IR TEST SKIP
		//m_fnSetBatchStep(HIPOT, STEP12);
		G_AddLog(3,L"HIPOT MEASURE CONNECTING TO IR ");
		break;

	case STEP10: //JIG DOWN
		if(m_fnGetPrevBatchStep(HIPOT) != STEP10)
		{
			iRet = m_pPIO->writeDO(1, OUT_HIPOTIR_LIFT_DOWN,CARD_IO);
			m_fnSetBatchStep(HIPOT, STEP10);
		}
		else
		{	//wait till lift DOWN
			if(m_pPIO->readDI(OUT_HIPOTIR_LIFT_DOWN, CARD_IO)==1)//DOWN
			{
				if(!stHIPOTBatch.timer.On){
					stHIPOTBatch.timer.On = 1;
					stHIPOTBatch.timer.Set = 5;//TIMER 1S
				}else{
					if(stHIPOTBatch.timer.OTE == 1)
					{
						stHIPOTBatch.timer.clear();
						stHIPOTBatch.counter.clear();
						m_fnSetBatchStep(HIPOT, STEP12);
						G_AddLog(3,L"HIPOT JIG DOWN ");
					}
				}
			}else{
				if(!stHIPOTBatch.counter.On){
					stHIPOTBatch.counter.On = 1;
					stHIPOTBatch.counter.Set = 200;// 20sec
				}else{
					if(stHIPOTBatch.counter.OTE == 1)
					{
						stHIPOTBatch.timer.clear();
						stHIPOTBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 40 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 40 );
						m_fnSetBatchStep(HIPOT, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_HIPOT_LIFT_DOWN, CARD_IO)==1)//DOWN
		}
		break;

		//retry?
	case STEP12: //
		if(stHIPOTBatch.NG){
			stHIPOTBatch.NG = 0;
			if(stHIPOTBatch.retry++ < G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_RETEST_COUNT])
			{
				m_fnSetBatchStep(HIPOT, STEP3);
				G_AddLog(3,L"HIPOT RETRY ");
			}else
			{
				//NG final
				m_PartData[STG_HIPOT_IR].NG++;
				m_fnSetBatchStep(HIPOT, STEP14);
			}
		}else{
			m_fnSetBatchStep(HIPOT, STEP14);
		}
		break;

	case STEP14: //
		if(m_fnGetPrevBatchStep(HIPOT) != STEP14) 
		{
			setBatchStatus(HIPOT, FINISHED);
			stHIPOTBatch.timer.On = 1; 
			stHIPOTBatch.timer.Set = 10; 
			m_fnSetBatchStep(HIPOT, STEP14);
		}
		else
		{
			if(stHIPOTBatch.timer.OTE == 1)
			{
				stShuttleBatch.allow |= SHUTTLE_HIPOT; //allow shuttle
				stHIPOTBatch.timer.clear();
				m_fnSetInspectionStep(HIPOT, 0);
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
				{
					m_fnSetBatchStep(HIPOT, STEP16);
				}
				else{
					m_fnSetBatchStep(HIPOT, STEP15);
				}

			}
		}
		break;

	case STEP15: //HIPOT MANUAL END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; //allow shuttle
		m_fnSetBatchStep(HIPOT, STEP0); //
		G_AddLog(3,L"HIPOT BATCH END ");
		break;
	case STEP16: //HIPOT AUTO END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; //allow shuttle
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[HIPOT]; //request shuttle
		m_fnSetBatchStep(HIPOT, STEP17); //
		G_AddLog(3,L"HIPOT AUTO RUN SHUTTLE REQUEST ");
		break;
	case STEP17: //HIPOT WAIT SHUTTLE END
		//wait until shuttle end
		if(stShuttleBatch.request == 0)
		{
			stShuttleBatch.allow |= SHUTTLE_HIPOT; //allow shuttle
			m_fnSetBatchStep(HIPOT, STEP0);
			G_AddLog(3,L"HIPOT AUTO RUN BATCH END ");
		}
		break;


		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(HIPOT) != STEP19)
		{
			m_fnSetOldBatchStep(HIPOT, m_fnGetPrevBatchStep(HIPOT));
			m_fnSetBatchStep(HIPOT, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.switchRelayHipot(0, 0);
			iRet = G_MainWnd->m_PIODlg.switchRelayHipot(0, 0);	
			if(iRet != TMC_RV_OK ){
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 42 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 42 );
			}
			G_MainWnd->m_PIODlg.m_btnErrorReset[HIPOT].SetColorChange(RED, BLACK);
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		stHIPOTBatch.timer.clear();
		//clear error queue and register
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_HIP);
		m_fnSetBatchStep(HIPOT, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(HIPOT))

	return 0;
}


int CInspectThread::stg5IRTest(void)
{
	int iRet;
	CString str;
	long param;

	switch(m_fnGetBatchStep(IR))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(IR) != STEP0)
		{
			m_fnSetBatchStep(IR, STEP0);
		}else
		{

		}
		break;

	case STEP1: //START
		stShuttleBatch.allow &= ~SHUTTLE_IR; //not allowed shuttle
		stIRBatch.retry = 0;
		stIRBatch.NG = 0;
		stIRBatch.VISAFault = 0;
		m_fnSetInspectionStep(IR, 0);

		m_fnSetBatchStep(IR, STEP2);
		G_AddLog(3,L"IR BATCH START ");
		break;
	case STEP2: //JIG UP
		if(m_fnGetPrevBatchStep(IR) != STEP2)
		{
			iRet = m_pPIO->writeDO(1, OUT_HIPOTIR_LIFT_UP,CARD_IO);
			m_fnSetBatchStep(IR, STEP2);
		}
		else
		{	//wait till lift up
			if(m_pPIO->readDI(IN_HIPOTIR_LIFT_UP, CARD_IO)==1)//UP
			{
				//TIMER 0.5S
				if(!stIRBatch.timer.On){
					stIRBatch.timer.On = 1;
					stIRBatch.timer.Set = 5;
				}else{
					if(stIRBatch.timer.OTE == 1)
					{
						stIRBatch.timer.clear();
						stIRBatch.counter.clear();
						m_fnSetBatchStep(IR, STEP3);
						G_AddLog(3,L"IR JIG UP ");
					}
				}
			}else{
				if(!stIRBatch.counter.On){
					stIRBatch.counter.On = 1;
					stIRBatch.counter.Set = 200;// 20sec
				}else{
					if(stIRBatch.counter.OTE == 1)
					{
						stIRBatch.timer.clear();
						stIRBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50 );
						m_fnSetBatchStep(IR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_IR_LIFT_UP, CARD_IO)==1)//UP
		}
		break;
	case STEP3: //
		if(stIRBatch.irStep < MAX_IR_TEST){
			if(m_pRecipeSetDlg->part_Test.irTestInfo[stIRBatch.irStep].bTest){
				m_fnSetBatchStep(IR, STEP4);
				m_fnSetInspectionStep(IR, stIRBatch.irStep);
			}else{
				m_fnSetInspectionStep(IR, ++stIRBatch.irStep);
			}
		}else{
			//goto JIG DOWN
			m_fnSetBatchStep(IR, STEP10);
		}
		break;

		//MEASURE STEP SWITCHING
	case STEP4: //
		if(m_fnGetPrevBatchStep(IR) != STEP4)
		{
			iRet = G_MainWnd->m_PIODlg.switchRelayIR(ON, stIRBatch.irStep);
			if(iRet != TMC_RV_OK ){
				//Error Msg
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 52 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 52 );
				m_fnSetBatchStep(IR, STEP19); break;
			}
			m_fnSetBatchStep(IR, STEP4);
		}else{
			//TIMER 0.5S
			if(!stIRBatch.timer.On){
				stIRBatch.timer.On = 1;
				stIRBatch.timer.Set = 5;
			}else{
				if(stIRBatch.timer.OTE == 1)
				{
					stIRBatch.timer.clear();
					stIRBatch.counter.clear();
					m_fnSetBatchStep(IR, STEP5);
					G_AddLog(3,L"IR SWITCHING RELAY ");
				}
			}
		}
		break;

//UM_MSG

		//IR TEST START
	case STEP5: //
		if(m_fnGetPrevBatchStep(IR) != STEP5)
		{
			::SendMessage(G_MainWnd->m_InstrumentDlg.m_hWnd, UM_INSTRUMENT, (WPARAM)UW_IR_START, NULL);
			stIRBatch.timer.clear();
			stIRBatch.timer.On = 1;
			stIRBatch.timer.Set = 50;
			m_fnSetBatchStep(IR, STEP5);
		}
		else
		{
			if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_START) == UM_INSTRUMENT_OK)	
			{//
				G_MainWnd->m_InstrumentDlg.SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_CLEAR);
				stIRBatch.timer.clear();
				m_fnSetBatchStep(IR, STEP6);
				G_AddLog(3,L"IR MEASURE START ");
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_START) == UM_INSTRUMENT_CLEAR){//NOT RECEIVED, SEND ONE MORE 
				if(stIRBatch.timer.Accu == 10){
					::SendMessage(G_MainWnd->m_InstrumentDlg.m_hWnd, UM_INSTRUMENT, (WPARAM)UW_IR_START, NULL);
				}
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_START) == UM_INSTRUMENT_NG){//NG
				//Error
				str.Format(L"IR STEP %d MEASURE START FAILED", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}
			if(stIRBatch.timer.OTE)
			{//TIME UP ERROR
				str.Format(L"IR STEP5 TIME UP ERROR [INSTRUMENT STEP: %d]", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}

		}//if(m_fnGetPrevBatchStep(IR) != STEP5)
		break;

		//IR TEST STATUS
	case STEP6: //
		if(m_fnGetPrevBatchStep(IR) != STEP6)
		{

			stIRBatch.timer.clear();
			stIRBatch.timer.On = 1;
			//stIRBatch.timer.Set = 200;
			stIRBatch.timer.Set = 1000;
			m_fnSetBatchStep(IR, STEP6);
		}
		else
		{
			if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_STATUS) == UM_INSTRUMENT_OK)	
			{//
				G_MainWnd->m_InstrumentDlg.SetUMMsgResponse(UW_IR_STATUS, UM_INSTRUMENT_CLEAR);
				stIRBatch.timer.clear();
				m_fnSetBatchStep(IR, STEP7);
				G_AddLog(3,L"IR MEASURE FINISHED ");
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_STATUS) == UM_INSTRUMENT_ONGOING ||
				G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_STATUS) == UM_INSTRUMENT_CLEAR ){
				::SendMessage(G_MainWnd->m_InstrumentDlg.m_hWnd, UM_INSTRUMENT, (WPARAM)UW_IR_STATUS, NULL);
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_STATUS) == UM_INSTRUMENT_NG){//NG
				//Error
				str.Format(L"IR STEP %d MEASURE STATUS FAILED", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}
			if(stIRBatch.timer.OTE)
			{//TIME UP ERROR
				str.Format(L"IR STEP6 TIME UP ERROR [INSTRUMENT STEP: %d]", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}

		}//if(m_fnGetPrevBatchStep(IR) != STEP6)
		break;

		//IR TEST RESULT
	case STEP7: //
		if(m_fnGetPrevBatchStep(IR) != STEP7)
		{
			::SendMessage(G_MainWnd->m_InstrumentDlg.m_hWnd, UM_INSTRUMENT, (WPARAM)UW_IR_RESULT, NULL);
			stIRBatch.timer.clear();
			stIRBatch.timer.On = 1;
			stIRBatch.timer.Set = 200;
			m_fnSetBatchStep(IR, STEP7);
		}
		else
		{
			if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_RESULT) == UM_INSTRUMENT_OK)	
			{//
				G_MainWnd->m_InstrumentDlg.SetUMMsgResponse(UW_IR_RESULT, UM_INSTRUMENT_CLEAR);
				stIRBatch.timer.clear();
				iRet = G_MainWnd->m_PIODlg.switchRelayIR(OFF, stIRBatch.irStep);
				//Result
				double ir = atof(m_pInstrumentDlg->vi[VI_IR].str_result);
				if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
					ir += G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[stIRBatch.irStep].offset*(+1.0E+6);
					G_AddLog(3,L"IR MEASURE OFFSET APPLIED");
				}
				m_PartData[STG_HIPOT_IR].ir[stIRBatch.irStep] = ir;
				if( m_pRecipeSetDlg->part_Test.irTestInfo[stIRBatch.irStep].lowerLimit*(+1.0E+6) <= ir ){//MOhm
					m_PartData[STG_HIPOT_IR].irPass[stIRBatch.irStep] = 1;
					G_AddLog(3,L"IR MEASURE PASS ");
				}else{
					m_PartData[STG_HIPOT_IR].irPass[stIRBatch.irStep] = 0;
					stIRBatch.NG=1;
					G_AddLog(3,L"IR MEASURE FAIL  ");
				}
				param = STG_HIPOT_IR * 0x100 + stIRBatch.irStep + MAX_HIPOT_TEST;
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
				m_fnSetInspectionStep(IR, ++stIRBatch.irStep);
				m_fnSetBatchStep(IR, STEP3);
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_RESULT) == UM_INSTRUMENT_CLEAR){//NOT RECEIVED, SEND ONE MORE 
				if(stIRBatch.timer.Accu == 10){
					::SendMessage(G_MainWnd->m_InstrumentDlg.m_hWnd, UM_INSTRUMENT, (WPARAM)UW_IR_RESULT,NULL);
				}
			}else if(G_MainWnd->m_InstrumentDlg.GetUMMsgResponse(UW_IR_RESULT) == UM_INSTRUMENT_NG){//NG
				//Error
				iRet = G_MainWnd->m_PIODlg.switchRelayIR(OFF, stIRBatch.irStep);
				str.Format(L"IR STEP %d MEASURE RESULT FAILED", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}
			if(stIRBatch.timer.OTE)
			{//TIME UP ERROR
				iRet = G_MainWnd->m_PIODlg.switchRelayIR(OFF, stIRBatch.irStep);
				str.Format(L"IR STEP7 TIME UP ERROR [INSTRUMENT STEP: %d]", stIRBatch.irStep);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3,str.GetBuffer()); str.ReleaseBuffer();
				m_fnSetBatchStep(IR, STEP19); break;
			}
		}//if(m_fnGetPrevBatchStep(IR) != STEP7)
		break;

	case STEP8: //connect from HIPOT test
		if(stIRBatch.status == CONTINUE)//CONTINUE FROM IR
		{
			stShuttleBatch.allow &= ~SHUTTLE_IR; //not allowed shuttle
			m_fnSetInspectionStep(IR, 0);
			G_AddLog(3,L"IR BATCH CONNECTED FROM HIPOT ");
		}
		m_fnSetBatchStep(IR, STEP3);
		break;

	case STEP10: //LIFT DOWN
		if(m_fnGetPrevBatchStep(IR) != STEP10)
		{
			iRet = m_pPIO->writeDO(1, OUT_HIPOTIR_LIFT_DOWN,CARD_IO);
			m_fnSetBatchStep(IR, STEP10);
		}
		else
		{	//wait till lift DOWN
			if(m_pPIO->readDI(OUT_HIPOTIR_LIFT_DOWN, CARD_IO)==1)//DOWN
			{
				if(!stIRBatch.timer.On){
					stIRBatch.timer.On = 1;
					stIRBatch.timer.Set = 5;//TIMER 1S
				}else{
					if(stIRBatch.timer.OTE == 1)
					{
						stIRBatch.timer.clear();
						stIRBatch.counter.clear();
						m_fnSetBatchStep(IR, STEP12);
						G_AddLog(3,L"IR JIG DOWN ");
					}
				}
			}else{
				if(!stIRBatch.counter.On){
					stIRBatch.counter.On = 1;
					stIRBatch.counter.Set = 200;// 20sec
				}else{
					if(stIRBatch.counter.OTE == 1)
					{
						stIRBatch.timer.clear();
						stIRBatch.counter.clear();
						//timeout error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 50 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 50 );
						m_fnSetBatchStep(IR, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_IR_LIFT_DOWN, CARD_IO)==1)//DOWN
		}
		break;

		//retry?
	case STEP12: //
		if(stIRBatch.NG){
			stIRBatch.NG = 0;
			if(stIRBatch.retry++ < G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_RETEST_COUNT])
			{
				m_fnSetBatchStep(IR, STEP3); //m_fnSetBatchStep(IR, STEP3);
				G_AddLog(3,L"IR MEASURE RETRY ");
			}else
			{
				//NG final
				m_PartData[STG_HIPOT_IR].NG++;
				m_fnSetBatchStep(IR, STEP14);
			}
		}else{
			m_fnSetBatchStep(IR, STEP14);
		}
		break;


	case STEP14: //
		if(m_fnGetPrevBatchStep(IR) != STEP14)
		{
			setBatchStatus(IR, FINISHED);
			if(stHIPOTBatch.status == CONTINUE){
				m_fnSetBatchStep(HIPOT, STEP0); //
				stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; //allow shuttle
				stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[IR]; //allow shuttle
			}
			stIRBatch.timer.On = 1; //1000ms
			stIRBatch.timer.Set = 5;
			m_fnSetBatchStep(IR, STEP14);
		}
		else
		{
			if(stIRBatch.timer.OTE == 1)
			{
				stIRBatch.timer.clear();
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(IR, STEP16);
				else
					m_fnSetBatchStep(IR, STEP15);
			}
			//count up and error
		}
		break;

	case STEP15: //IR MANUAL END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; //allow shuttle
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[IR]; //allow shuttle
		m_fnSetBatchStep(IR, STEP0); //
		break;
	case STEP16: //IR AUTO END
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; //allow shuttle
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[IR]; //allow shuttle
		stShuttleBatch.request |= ST_SHUTTLE_INTERLOCK[IR]; //request shuttle
		m_fnSetBatchStep(IR, STEP17); //
		G_AddLog(3,L"IR AUTO RUN SHUTTLE REQUEST ");
		break;
	case STEP17: //IR WAIT SHUTTLE END
		//wait until shuttle end
		if(stShuttleBatch.request == 0)
		{
			m_fnSetBatchStep(HIPOT, STEP0); //
			m_fnSetBatchStep(IR, STEP0);
			G_AddLog(3,L"IR AUTO RUN BATCH END ");
		}
		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(IR) != STEP19)
		{
			m_fnSetOldBatchStep(IR, m_fnGetPrevBatchStep(IR));
			m_fnSetBatchStep(IR, STEP19);
			iRet = G_MainWnd->m_PIODlg.switchRelayIR(0, 0);
			if(iRet != TMC_RV_OK ){
				//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 52 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 52 );
			}
			G_MainWnd->m_PIODlg.m_btnErrorReset[IR].SetColorChange(RED, BLACK);
			errorRoutine();
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		stIRBatch.timer.clear();
		//clear error queue and register
		G_MainWnd->m_InstrumentDlg.faultResetInstrument(VI_IR);
		m_fnSetBatchStep(IR, STEP22);
		break;

	case STEP22:   //HOLD
		break;

	}//switch(m_fnGetBatchStep(IR))

	return 0;
}
#endif

int CInspectThread::stg6Unload(void)
{
	int iRet;
	CString str;
	double dbActPos;
	double dbRETPos;
	double AXIS_POS_TOLERANCE;

	switch(m_fnGetBatchStep(UNLOAD))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP0)
		{
			m_fnSetBatchStep(UNLOAD, STEP0);
		}else
		{

		}
		break;

	case STEP1: //START
		stShuttleBatch.allow &= ~SHUTTLE_UNLOAD; //not allowed shuttle
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_VI_UNLOAD);     //1223_KYS
#ifdef _FI
		//160219_kys_UiDelete_add
		if (m_fnGetBatchStep(HIPOT) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_HIPOT_IR);
		}
#else
		if (m_fnGetBatchStep(DCR) == STEP0)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_DCR);
		}
#endif		
		stRETURNCVBatch.run = LOCKED; //lock
		m_fnSetBatchStep(UNLOAD, STEP2);
		G_AddLog(3,L"UNLOAD BATCH START ");
		break;

	case STEP2: // JIG CHECK //delay 1 sec
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP2)
		{
			stUNLOADBatch.timer.On = 1;
			stUNLOADBatch.timer.Set = 2;
			m_fnSetBatchStep(UNLOAD, STEP2);
		}else{
			if(stUNLOADBatch.timer.OTE == 1)
			{
				stUNLOADBatch.timer.clear();
				if(m_pPIO->readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)==1)
				{
					if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
						if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LABEL_SCAN_PRINT_SKIP] == 1)//SKIP
						{
							m_fnSetBatchStep(UNLOAD, STEP3);
						}
						else{
							m_fnSetBatchStep(UNLOAD, STEP10);
						}
					}
					else{
						m_fnSetBatchStep(UNLOAD, STEP5);//JIG DOWN
					}

				}else{
					m_fnSetBatchStep(UNLOAD, STEP11);	//GO TO END, NO JIG TO UNLOAD
				}
			}
		}
		break;

	case STEP3: //OK NG CHECK
		if(m_pPIO->readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)!=0)
		{
			if(m_fnGetPrevBatchStep(UNLOAD) != STEP3)
			{
				//Tact time display
				CString strTact;
				CTime ctimeOut = CTime::GetCurrentTime();
				CTimeSpan ctimeTact = ctimeOut - ctUnloadTime ;
				ctUnloadTime = ctimeOut;
				strTact = ctimeTact.Format(L"%H:%M:%S");
				G_MainWnd->m_ServerDlg.m_staticTimeMasterElapsec.SetText(strTact, WHITE, BLACK);
				m_fnSetBatchStep(UNLOAD, STEP3);
				int fail = summarizeResult();
//#ifdef _DEBUG
//				fail = 0; //Pass
//#endif
				G_AddLog(3,L"UNLOAD PART MEASUREMENT STEP FAIL COUNT %d  ", fail);
				
				G_MainWnd->m_DataHandling.m_fnDayReportWrite(TRUE);
				G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_OLD] = G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_NEW];
				//OK/NG Display
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_STATUS_UPDATE, NULL);

				if(!fail){//Pass
#ifdef _FI
					m_fnSetBatchStep(UNLOAD, STEP8); //SHUTTLE POSITION CHECK
#else
					//1117_KYS
					m_fnSetBatchStep(UNLOAD, STEP14); // GO FINISH
					//m_fnSetBatchStep(UNLOAD, STEP11); // CHECK IF JIG UNLOAD BY OPERATOR
#endif
				}else{//Fail
					m_fnSetBatchStep(UNLOAD, STEP4); 
				}
			}

		}else{
			//jig out
			//goto step 11
			if(!stUNLOADBatch.timer.On){
				stUNLOADBatch.timer.On = 1;
				stUNLOADBatch.timer.Set = 5;
			}else{
				if(stUNLOADBatch.timer.OTE == 1)
				{
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP14);
				}
			}
		}//
		break;

	case STEP4: //NG JIG CLEAR

		if(m_pPIO->readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)!=0)
		{
			//wait until other batch test finished
			if(
#ifdef _FI
				(stShuttleBatch.allow & SHUTTLE_ALL) == 0x7F 
#else
				//(stShuttleBatch.allow & SHUTTLE_ALL) == 0x0A
				(stShuttleBatch.allow & 0x0A) == 0x0A    //1117_KYS
#endif
				//&& (m_pPIO->readDI(IN_HIPOTIR_JIG_DETECT,CARD_IO) ==0 || (m_pPIO->readDI(IN_HIPOTIR_JIG_DETECT,CARD_IO)==1 && stIRBatch.status == FINISHED))
			){
					//NG data display
					//showNGResult();
					//fail alarm,message
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 70);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 70);
					G_MainWnd->m_ServerDlg.m_btnNGOut.SetColorChange(YELLOW, BLACK);
					G_MainWnd->m_ServerDlg.m_btnNGClear.SetColorChange(YELLOW, BLACK);
					//Manual mode
					checkStatus = CHECK_NG;
					m_fnSetBatchStep(UNLOAD, STEP11);//NG CLEAR CHECK
			}
		}else{ //no jig
			m_fnSetBatchStep(UNLOAD, STEP14);
		}
		break;

		//LIFT DOWN
#ifdef _FI
	case STEP5: 
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP5)
		{
			iRet = m_pPIO->writeDO(1, OUT_UNLOAD_CV_DOWN,CARD_IO);
			m_fnSetBatchStep(UNLOAD, STEP5);
			stUNLOADBatch.timer.clear();
		}
		else
		{	//wait till lift 
			if(m_pPIO->readDI(IN_UNLOAD_CV_DOWN, CARD_IO)==1)
			{
				//TIMER 0.5S
				stRETURNCVBatch.jig = PART_ON_CV_UNLOAD;
				if(!stUNLOADBatch.timer.On){
					stUNLOADBatch.timer.On = 1;
					stUNLOADBatch.timer.Set = 5;
				}else{
					if(stUNLOADBatch.timer.OTE == 1)
					{
						stUNLOADBatch.timer.clear();
						stUNLOADBatch.counter.clear();
						m_fnSetBatchStep(UNLOAD, STEP6);
						G_AddLog(3,L"UNLOAD LIFT DOWN  ");
					}
				}
			}else{
				if(!stUNLOADBatch.counter.On){
					stUNLOADBatch.counter.On = 1;
					stUNLOADBatch.counter.Set = 200;// 20sec
				}else{
					if(stUNLOADBatch.counter.OTE == 1)
					{
						stUNLOADBatch.timer.clear();
						stUNLOADBatch.counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 71 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 71 );
						m_fnSetBatchStep(UNLOAD, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_UNLOAD_LIFT_UP, CARD_IO)==1)
		}
		break;

	case STEP6: // STEP2 CV RUN
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP6)
		{
			if( m_fnGetBatchStep(RETURNCV) == STEP0 && !m_pPIO->readDI(IN_CV_PART_RETURN,CARD_IO))
			{
				stUNLOADBatch.timer.clear();     //1223_KYS
				stRETURNCVBatch.run = CLEAR; //
				m_fnSetBatchStep(UNLOAD, STEP6);
				m_fnSetBatchStep(RETURNCV, STEP1); //CV START
				G_AddLog(3,L"UNLOAD CONVEYOR BATCH START  ");
				if(!stUNLOADBatch.timer.On){
					stUNLOADBatch.timer.On = 1;
					//stUNLOADBatch.timer.Set = 350; //JIG PASS TIME: 10SEC
					//stUNLOADBatch.timer.Set = 150; //JIG PASS TIME: 10SEC
					stUNLOADBatch.timer.Set = 100; //JIG PASS TIME: 10SEC
				}
			}else{
				//message to remove returned jig in loading zone on return CV


			}
		}
		else
		{
			G_AddLog(3, L"&&&&&&&&&&&&&&&&&& SENSOR/TIME DELAY &&&&&&&&&&&&&&&&&&&&&&&&");
			if(m_pPIO->readDI(IN_CV_PART_RETURN,CARD_IO))//
			{
				stUNLOADBatch.timer.clear();
				m_fnSetBatchStep(UNLOAD, STEP9);
				G_AddLog(3, L"$$$$$$$$$$$$$$ SENSOR CHECK $$$$$$$$$$$$$$$$$$$$");
			}
			//Lift up in 5 sec 
			if(stUNLOADBatch.timer.On && stUNLOADBatch.timer.OTE)
			{
				if(m_pPIO->readDO(OUT_UNLOAD_CV_RUN,CARD_IO)==1)
				{
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP9);
					G_AddLog(3, L"$$$$$$$$$$$$$$ TIME CHECK $$$$$$$$$$$$$$$$$$$$");
				}
			}
//1223_KYS
			//if ((m_pPIO->readDI(IN_CV_PART_RETURN, CARD_IO)) &&
			//	stUNLOADBatch.timer.On && stUNLOADBatch.timer.OTE)
			//{
			//		stUNLOADBatch.timer.clear();
			//		m_fnSetBatchStep(UNLOAD, STEP9);
			//		G_AddLog(3, L"$$$$$$$$$$$$$$ CV_UP_CONDITION!!!!!!! $$$$$$$$$$$$$$$$$$$$");
			//}

		}
		break;

	case STEP7: // STEP7 JIG CV LIFT UP
		
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP7&&m_pPIO->readDI(IN_UNLOAD_JIG_DETECT, CARD_IO)==0 
		  //&&m_pPIO->readDI(IN_CV_PART_RETURN, CARD_IO)==1
			)
		{
			stUNLOADBatch.timer.clear();    //1223_KYS
			G_AddLog(3, L"$$$$$$$$$$$$$$$$ UNLOAD CV UP$$$$$$$$$$$$$$$$$$$$$$");
			iRet = m_pPIO->writeDO(1, OUT_UNLOAD_CV_UP,CARD_IO);
			m_fnSetBatchStep(UNLOAD, STEP7);
		}
		else
		{	//wait till lift 
			if(m_pPIO->readDI(IN_UNLOAD_CV_UP, CARD_IO)==1)
			{
				G_AddLog(3, L"$$$$$$$$$$$$$$$$ -5 $$$$$$$$$$$$$$$$$$$$$$");
				//TIMER 0.5S
				//stRETURNCVBatch.jig = CLEAR;
				stRETURNCVBatch.jig = PART_ON_CV_UNLOAD;
				if(!stUNLOADBatch.timer.On){
					stUNLOADBatch.timer.On = 1;
					stUNLOADBatch.timer.Set = 5;
				}else{
					if(stUNLOADBatch.timer.OTE == 1)
					{
						stUNLOADBatch.timer.clear();
						stUNLOADBatch.counter.clear();
						m_fnSetBatchStep(UNLOAD, STEP14);
						G_AddLog(3,L"UNLOAD LIFT UP  ");
					}
				}
			}else{
				if(!stUNLOADBatch.counter.On){
					stUNLOADBatch.counter.On = 1;
					stUNLOADBatch.counter.Set = 2000;// 20sec
				}else{
					if(stUNLOADBatch.counter.OTE == 1)
					{
						stUNLOADBatch.timer.clear();
						stUNLOADBatch.counter.clear();
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 71 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 71 );
						m_fnSetBatchStep(UNLOAD, STEP19); break;
					}
				}
			}//if(m_pPIO->readDI(IN_UNLOAD_LIFT_UP, CARD_IO)==1)
		}

		break;

	case STEP8: // STEP8 SHUTTLE CHECK IN RET POSITION FOR LIFT DOWN
//1113_MERGE_ADD FROM
		if (m_fnGetPrevBatchStep(UNLOAD) != STEP8)
		{
			if (!stUNLOADBatch.timer.On){
				stUNLOADBatch.timer.On = 1;
				stUNLOADBatch.timer.Set = 1000; //10sec
			}
			m_fnSetBatchStep(UNLOAD, STEP8);
		}
		else
		{
//1113_MERGE_ADD TO
			if (m_fnGetBatchStep(SHUTTLE) == STEP0)
			{
				dbRETPos = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN];
				AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];
				iRet = pmiAxGetActPos(CARD_NO2, AXIS_SHUTTLE, &dbActPos);
				if (iRet != TMC_RV_OK)
				{//Error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 72);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 72);
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP19);
				}
				if (dbActPos >= dbRETPos - AXIS_POS_TOLERANCE &&
					dbActPos <= dbRETPos + AXIS_POS_TOLERANCE)
				{
					//OK TO UNLOAD LIFT DOWN
					m_fnSetBatchStep(UNLOAD, STEP5);
				}
			}
//1113_MERGE_ADD FROM
			if (stUNLOADBatch.timer.On == 1 && stUNLOADBatch.timer.OTE == 1)
			{//Timeout Error
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 73);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 73);
				stUNLOADBatch.timer.clear();
				m_fnSetBatchStep(UNLOAD, STEP19);
			}
//1113_MERGE_ADD TO			
		}
		break;

	case STEP9: // STEP9 SHUTTLE CHECK IN RET POSITION FOR LIFT UP
//1113_MERGE_ADD FROM
		if (m_fnGetPrevBatchStep(UNLOAD) != STEP9)
		{
			if (!stUNLOADBatch.timer.On){
				stUNLOADBatch.timer.On = 1;
				stUNLOADBatch.timer.Set = 1000; //10sec
			}
			m_fnSetBatchStep(UNLOAD, STEP9);
		}
		else
		{
//1113_MERGE_ADD TO
			if (m_fnGetBatchStep(SHUTTLE) == STEP0)
			{
				dbRETPos = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN];
				AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];
				iRet = pmiAxGetActPos(CARD_NO2, AXIS_SHUTTLE, &dbActPos);
				if (iRet != TMC_RV_OK)
				{//Error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 72);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 72);
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP19);
				}
				if (dbActPos >= dbRETPos - AXIS_POS_TOLERANCE &&
					dbActPos <= dbRETPos + AXIS_POS_TOLERANCE)
				{
					//OK TO UNLOAD LIFT UP
					m_fnSetBatchStep(UNLOAD, STEP7);
					G_AddLog(3, L"$$$$$$$$$$$$$$ -1 $$$$$$$$$$$$$$$$$$$$");
				}
			}
//1113_MERGE_ADD
			if (stUNLOADBatch.timer.On == 1 && stUNLOADBatch.timer.OTE == 1)
			{//Timeout Error
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 73);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 73);
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP19);
			}
		}
		break;

	case STEP10: // label scan check
		
		if (m_fnGetPrevBatchStep(UNLOAD) != STEP10)
		{
			if (G_MainWnd->m_LabelPrint.m_partLabel.idxPrinterLabel == 0) //scanned or first
			{
				m_fnSetBatchStep(UNLOAD, STEP3);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, WHITE);
			}
			else{
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"LABEL SCAN 되지 않아 UNLOAD HOLD", BLACK, YELLOW);
				m_fnSetPrevBatchStep(UNLOAD, STEP10);
				m_pPIO->writeDO(1, OUT_BUZZER, CARD_IO); //1105_BUZZER_ADD
			}
		}
		else{
			if (G_MainWnd->m_LabelPrint.m_partLabel.idxPrinterLabel == 0)//scanned
			{
				m_fnSetBatchStep(UNLOAD, STEP3);
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, WHITE);
				m_pPIO->writeDO(0, OUT_BUZZER, CARD_IO);
			}
		}
		break;

#endif
	case STEP11: // STEP11 NG clear check
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP11)
		{
			if(m_pPIO->readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)==0)
			{
				m_fnSetBatchStep(UNLOAD, STEP11);
			}else{
				//not clear
				m_fnSetBatchStep(UNLOAD, STEP4);
			}
		}
		else
		{
			if(!stUNLOADBatch.timer.On){
				stUNLOADBatch.timer.On = 1;
				stUNLOADBatch.timer.Set = 20;
			}else{
				if(stUNLOADBatch.timer.OTE == 1)
				{
					stUNLOADBatch.timer.clear();
					m_fnSetBatchStep(UNLOAD, STEP14);
				}
			}
		}
		break;

	case STEP14: //
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP14)
		{
			setBatchStatus(UNLOAD, FINISHED);
			stUNLOADBatch.timer.On = 1; 
			stUNLOADBatch.timer.Set = 5;
			m_fnSetBatchStep(UNLOAD, STEP14);
		}
		else
		{
			if(stUNLOADBatch.timer.OTE == 1)
			{
				stUNLOADBatch.timer.clear();
				stShuttleBatch.allow |= SHUTTLE_UNLOAD; //allow shuttle
				if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
					m_fnSetBatchStep(UNLOAD, STEP16);
				else
					m_fnSetBatchStep(UNLOAD, STEP15);
			}
			//count up and error
		}
		break;

	case STEP15: //UNLOAD MANUAL END
		stShuttleBatch.allow |= SHUTTLE_UNLOAD; //allow shuttle
		setBatchStatus(UNLOAD, CLEAR);
		m_fnSetBatchStep(UNLOAD, STEP0); //
		break;
	case STEP16: //UNLOAD AUTO END
		stShuttleBatch.allow |= SHUTTLE_VI; //allow shuttle
		stShuttleBatch.allow |= SHUTTLE_UNLOAD; //allow shuttle
		setBatchStatus(UNLOAD, CLEAR);
		m_fnSetBatchStep(UNLOAD, STEP0); //
		G_AddLog(3,L"UNLOAD AUTO RUN BATCH END  ");
		break;
	case STEP17: //

		break;

		////////////////////////////////////////////////////////////////////////
		// ERROR
	case STEP19:  //ERROR STATE
		if(m_fnGetPrevBatchStep(UNLOAD) != STEP19)
		{
			m_fnSetOldBatchStep(UNLOAD, m_fnGetPrevBatchStep(UNLOAD));
			m_fnSetBatchStep(UNLOAD, STEP19);
			errorRoutine();
			G_MainWnd->m_PIODlg.m_btnErrorReset[UNLOAD].SetColorChange(RED, BLACK);
		}else
		{ // error process time count up

		}
		break;

	case STEP20:   //ERROR RESET
		//error reset
		stUNLOADBatch.timer.clear();
		m_fnSetBatchStep(UNLOAD, STEP22);
		break;

	case STEP22:   //HOLD

		break;

	}//switch(m_fnGetBatchStep(UNLOAD))

	return 0;
}


#ifdef _FI
// START CONDITION: m_fnGetBatchStep(RETURNCV) == STEP0  && stRETURNCVBatch.run != LOCKED
int CInspectThread::stg8ReturnConveyorRun(void)
{
	int iRet;

	switch(m_fnGetBatchStep(RETURNCV))
	{
	case STEP0: //READY
		if(m_fnGetPrevBatchStep(RETURNCV) != STEP0)
		{
			m_fnSetBatchStep(RETURNCV, STEP0);
		}else
		{

		}
		break;

	case STEP1: //START
		if(!m_pPIO->readDI(IN_CV_PART_RETURN,CARD_IO) && m_pPIO->readDI(IN_UNLOAD_CV_DOWN ,CARD_IO))
		{
			//TIMER 0.5S
			if(!stRETURNCVBatch.timer.On){
				stRETURNCVBatch.timer.On = 1;
				stRETURNCVBatch.timer.Set = 5;
			}else{
				if(stRETURNCVBatch.timer.OTE == 1)
				{
					stRETURNCVBatch.timer.clear();
					m_fnSetBatchStep(RETURNCV, STEP3);
					//160219_KYS_UNLOAD_VI DELETE CASE, LATER USE...
					//if (m_fnGetBatchStep(HIPOT) == STEP0)
					//{
					//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UW_CLEAR, (LPARAM)STG_VI_UNLOAD);
					//}
					G_AddLog(3,L"CONVEYOR BATCH START  ");
				}
			}
		}else{


		}
		break;

	case STEP2: //


		break;

	case STEP3: //CV RUN
		iRet = m_pPIO->writeDO(1, OUT_UNLOAD_CV_RUN,CARD_IO);
		if(iRet == TMC_RV_OK ){
			m_fnSetBatchStep(RETURNCV, STEP4);
			G_AddLog(3,L"CONVEYOR RUN  ");
		}else{
			//error
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 90 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 90 );
			m_fnSetBatchStep(RETURNCV, STEP19); break;
		}
		break;

	case STEP4:  //
		if(m_fnGetPrevBatchStep(RETURNCV) != STEP4)
		{
			m_fnSetBatchStep(RETURNCV, STEP4);

		}else
		{
			if(!stRETURNCVBatch.timer.On){
				stRETURNCVBatch.timer.On = 1;
				stRETURNCVBatch.timer.Set = RETURNCV_RUNTIME_FOR_UNLOAD_LIFTUP; //Conveyor Run and step Unload next in 5sec 
			}else{
				if((m_pPIO->readDO(OUT_UNLOAD_CV_RUN,CARD_IO) && stRETURNCVBatch.timer.OTE) || m_pPIO->readDI(IN_CV_PART_RETURN, CARD_IO))
				{ 
					stRETURNCVBatch.timer.clear();
#ifdef _FI
					if (G_MainWnd->m_LabelPrint.printLabel() == -1){
						//Label Print Error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 91);
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 91);
						m_fnSetBatchStep(RETURNCV, STEP19); break;
					}
					else{
						G_AddLog(3, L"PRINT LABEL ");
					}
#endif
					//if (m_fnGetBatchStep(UNLOAD) == STEP6){
					//	stUNLOADBatch.timer.clear();
					//	m_fnSetBatchStep(UNLOAD, STEP9); //UNLOAD LIFT UP
						G_AddLog(3, L"$$$$$$$$$$$$$$ -4 $$$$$$$$$$$$$$$$$$$$");
					//}
					m_fnSetBatchStep(RETURNCV, STEP5);
				}
			}
		}
		break;

	case STEP5: //
		if(m_fnGetPrevBatchStep(RETURNCV) != STEP5)
		{
			m_fnSetBatchStep(RETURNCV, STEP5);
		}else
		{
			if(!stRETURNCVBatch.timer.On){
				stRETURNCVBatch.timer.On = 1;
				stRETURNCVBatch.timer.Set = 2000;  
			}else{
				//if(m_pPIO->readDI(IN_CV_PART_RETURN, CARD_IO)){ 
					//stRETURNCVBatch.timer.clear();
					//m_fnSetBatchStep(RETURNCV, STEP0);
					//}
					if(stRETURNCVBatch.timer.OTE||m_pPIO->readDI(IN_CV_PART_RETURN, CARD_IO)==1){
					stRETURNCVBatch.timer.clear();
					iRet = m_pPIO->writeDO(0, OUT_UNLOAD_CV_RUN,CARD_IO);
					if(iRet == TMC_RV_OK ){
						m_fnSetBatchStep(RETURNCV, STEP0);
						G_AddLog(3,L"RETURNCV AUTO RUN BATCH END ");
					}else{
						//error
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 90 );
						::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 90 );
						m_fnSetBatchStep(RETURNCV, STEP19); break;
					}
				}
			}
		}
		break;

	}//switch(m_fnGetBatchStep(RETURNCV))

	return 0;
}

#endif


int CInspectThread::EMStop(void)
{
	int  iRet;

	//Axis Stop
	for(int i = 0; i < 8; i++){
		//pmiAxCheckDone(CARD_NO, i, &nDone);
		//if(nDone == emRUNNING)
		//	iRet = pmiAxStop(CARD_NO, i);	
		 iRet = pmiAxEStop(CARD_NO, i);			//
	}
	for(int i = 0; i < 3; i++){
		//pmiAxCheckDone(CARD_NO2, i, &nDone);
		//if(nDone == emRUNNING)
		//	iRet = pmiAxStop(CARD_NO, i);	
		iRet = pmiAxEStop(CARD_NO2, i);			//
	}

	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN ){
		G_MainWnd->m_ServerDlg.OnBnClickedBtnStop();
		G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
	}else if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){
		G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
	}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

	}

	//
	CString str;
	str.Format(L"EM STOP");
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
	G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
	G_SystemModeData.unSystemError = SYSTEM_ERROR;

	return 0;
}


int CInspectThread::holdAutoRun(void)
{
	for(int i = 0; i < NO_BATCH_SQC ; i++)
	{
#ifndef _FI
		if(i == LCR || i == DCR || i == SHUTTLE) continue;
#endif
		holdBatch(i);
	}
	return 0;
}


int CInspectThread::resumeAutoRun(void)
{
	for(int i = 0; i < NO_BATCH_SQC ; i++)
	{
#ifndef _FI
		if (!(i == LCR || i == DCR || i == SHUTTLE || i == VI || i == UNLOAD)) continue;
		//if (i == LCR || i == DCR || i == SHUTTLE) continue;          //1125_KYS
#endif
		continueBatch(i);
	}
	return 0;
}

int CInspectThread::resetBatch(int batch)
{
	CString strBuff;
	//check if it ok to reset in manual mode


	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		m_fnSetBatchStep(batch, STEP0); 
		stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[batch]; //allow shuttle
		stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[batch]; //request shuttle
	}
	else
	{
		//can not reset in auto mode
		strBuff.Format(_T(" Reset in Manual Mode "));
		AfxMessageBox(strBuff);
	}
	return 0;
}

int CInspectThread::holdBatch(int batch)
{
	int iRet;
	if(m_fnGetBatchStep(batch) !=STEP0 && m_fnGetBatchStep(batch) !=STEP22 &&  m_fnGetBatchStep(batch) !=STEP19&&  m_fnGetBatchStep(batch) !=STEP20){
		m_fnSetBatchStep(batch, STEP22); //put in hold status
		m_fnSetOldBatchStep(batch, m_fnGetPrevBatchStep(batch));

		//timer reset
		switch(batch){

			case LCR: if(stLCRBatch.timer.On) stLCRBatch.timer.clear(); 	break;
			case DCR: if(stDCRBatch.timer.On) stDCRBatch.timer.clear();	break;
			case VI: if(stVIBatch.timer.On) stVIBatch.timer.clear();	break;

#ifdef _FI	
			case LOAD: if(stLoadBatch.timer.On) stLoadBatch.timer.clear(); break;
			case OSC: if(stOSCBatch.timer.On) stOSCBatch.timer.clear();	break;
			case HIPOT: 
				if(stHIPOTBatch.timer.On) stHIPOTBatch.timer.clear(); 
				iRet = G_MainWnd->m_PIODlg.switchRelayHipot(0, 0);	
				if(iRet != TMC_RV_OK ){
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 42 );
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 42 );
				}
				break;
			case IR: 
				if(stIRBatch.timer.On) stIRBatch.timer.clear(); 
	 			iRet = G_MainWnd->m_PIODlg.switchRelayIR(0, 0);
				if(iRet != TMC_RV_OK ){
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 52 );
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 52 );
				}
				break;
			case UNLOAD: if(stUNLOADBatch.timer.On) stUNLOADBatch.timer.clear();	break;
			case SHUTTLE: if(stShuttleBatch.timer.On) stShuttleBatch.timer.clear();	break;
#endif
		}
	}
	return 0;
}

int CInspectThread::continueBatch(int batch)
{
	if(m_fnGetBatchStep(batch) == STEP22){
		m_fnSetBatchStep(batch, m_fnGetOldBatchStep(batch)); //put it back in old step
		if(m_fnGetOldBatchStep(batch) == STEP17
#ifndef _FI			
			||m_fnGetOldBatchStep(DCR) == STEP7     //1124_KYS_ADD_NO_RETRY_CASE_ OLDSTEP STEP7!!_PI 
#endif			
			)
			m_fnSetBatchStep(batch, STEP16); // request shuttle
		if(batch == HIPOT || batch == IR)
		{
			if(m_fnGetOldBatchStep(batch) == STEP5 || m_fnGetOldBatchStep(batch) == STEP6 || m_fnGetOldBatchStep(batch) == STEP7 )
			{
				m_fnSetBatchStep(batch, STEP4); // Start
			}
		}

	}
	return 0;
}

int CInspectThread::batchTimer(void)//100ms
{	//BCD
	if(stLoadBatch.timer.On == 1 ){
		if( stLoadBatch.timer.Accu <  stLoadBatch.timer.Set) 
			stLoadBatch.timer.Accu++;
		else if(stLoadBatch.timer.Accu >=  stLoadBatch.timer.Set)
			stLoadBatch.timer.OTE = 1;
	}
	//LCR
	if(stLCRBatch.timer.On == 1 ){
		if( stLCRBatch.timer.Accu <  stLCRBatch.timer.Set) 
			stLCRBatch.timer.Accu++;
		else if(stLCRBatch.timer.Accu >=  stLCRBatch.timer.Set)
			stLCRBatch.timer.OTE = 1;
	}
	//OSC
	if(stOSCBatch.timer.On == 1 ){
		if( stOSCBatch.timer.Accu <  stOSCBatch.timer.Set) 
			stOSCBatch.timer.Accu++;
		else if(stOSCBatch.timer.Accu >=  stOSCBatch.timer.Set)
			stOSCBatch.timer.OTE = 1;
	}
	//DCR
	if(stDCRBatch.timer.On == 1 ){
		if( stDCRBatch.timer.Accu <  stDCRBatch.timer.Set) 
			stDCRBatch.timer.Accu++;
		else if(stDCRBatch.timer.Accu ==  stDCRBatch.timer.Set)
			stDCRBatch.timer.OTE = 1;
	}
	//HIPOT
	if(stHIPOTBatch.timer.On == 1 ){
		if( stHIPOTBatch.timer.Accu <  stHIPOTBatch.timer.Set) 
			stHIPOTBatch.timer.Accu++;
		else if(stHIPOTBatch.timer.Accu ==  stHIPOTBatch.timer.Set)
			stHIPOTBatch.timer.OTE = 1;
	}
	//IR
	if(stIRBatch.timer.On == 1 ){
		if( stIRBatch.timer.Accu <  stIRBatch.timer.Set) 
			stIRBatch.timer.Accu++;
		else if(stIRBatch.timer.Accu ==  stIRBatch.timer.Set)
			stIRBatch.timer.OTE = 1;
	}
	//VI
	if(stVIBatch.timer.On == 1 ){
		if( stVIBatch.timer.Accu <  stVIBatch.timer.Set) 
			stVIBatch.timer.Accu++;
		else if(stVIBatch.timer.Accu ==  stVIBatch.timer.Set)
			stVIBatch.timer.OTE = 1;
	}
	//UNLOAD
	if(stUNLOADBatch.timer.On == 1 ){
		if( stUNLOADBatch.timer.Accu <  stUNLOADBatch.timer.Set) 
			stUNLOADBatch.timer.Accu++;
		else if(stUNLOADBatch.timer.Accu ==  stUNLOADBatch.timer.Set)
			stUNLOADBatch.timer.OTE = 1;
	}
	//SHUTTLE
	if(stShuttleBatch.timer.On == 1 ){
		if( stShuttleBatch.timer.Accu <  stShuttleBatch.timer.Set) 
			stShuttleBatch.timer.Accu++;
		else if(stShuttleBatch.timer.Accu ==  stShuttleBatch.timer.Set)
			stShuttleBatch.timer.OTE = 1;
	}
	//RETURNCV
	if(stRETURNCVBatch.timer.On == 1 ){
		if( stRETURNCVBatch.timer.Accu <  stRETURNCVBatch.timer.Set) 
			stRETURNCVBatch.timer.Accu++;
		else if(stRETURNCVBatch.timer.Accu ==  stRETURNCVBatch.timer.Set)
			stRETURNCVBatch.timer.OTE = 1;
	}
	return 0;
}

//LCR AXIS STEP
int CInspectThread::setLCRAxisStep(LPCTSTR lpszKeyName, int step)
{
	CString str;
	stLCRBatch.LCR_Axis_step = step;
	//AxisGrid display
	for (int row = 1; row < G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetRowCount(); row++)
	{
		G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetCell(row, (NUM_AXIS_GRID_COL +1))->SetBackClr(RGB(220,220,220));
	}
	G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetCell(step+1, (NUM_AXIS_GRID_COL +1))->SetBackClr(YELLOW);

	str.Format(L"%d", step);
	G_MainWnd->m_DataHandling.m_FlashData.data[LCR_AXIS_STEP] = step;
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, lpszKeyName, str );
	G_MainWnd->m_AxisDlg.m_GridLCRAxis.Refresh();
	return 0;
}



void CInspectThread::turnLight(int sw)
{
	if(sw){
		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(MODE_LIGHT_1, G_MainWnd->m_DataHandling.m_SystemParam.nLightBrightness);
	}else{
		G_MainWnd->m_SerialInterface.m_fnSendToComportLight(MODE_LIGHT_1, 0);
	}
	return;
}

//
int CInspectThread::summarizeResult(void)
{
	int pass, batchFail, fail;
	CString strNG, str, strBatch;
	int i;
	int stage;


#ifdef _FI
	stage = STG_VI_UNLOAD;
#else
	if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
		stage = STG_VI_UNLOAD;
	}
	else{
		stage = STG_DCR;
	}
#endif

	pass = fail = batchFail = 0;

	//LCR
	strBatch.Empty();
	strBatch+="LCR: ";
	for(i=0; i< MAX_L_TEST; i++)
	{
		if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].bTest)
		{
			if(!m_PartData[stage].lPass[i])
			{
				batchFail++;
				str.Format(L"[%d]%10.4f uH ", i,m_PartData[stage].l[i]*(+1.0E+6));
				strBatch+=str;
			}
		}
	}//for(i=0; i< MAX_L_TEST; i++)
	if(batchFail){
		strNG+=strBatch; 
		fail+=batchFail;
	}
	
	//DCR
	batchFail = 0;
	strBatch.Empty();
	strBatch+="DCR: ";
	for(i=0; i< MAX_DCR_TEST; i++)
	{
		if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].bTest)
		{
			if(!m_PartData[stage].rPass[i])
			{
				batchFail++;
				str.Format(L"[%d]%10.4f mOhm", i,m_PartData[stage].r[i]*(+1.0E+3));
				strBatch+=str;
			}
		}
	}//for(i=0;
	if(batchFail){
		strNG+=strBatch;
		fail+=batchFail;
	}

//#ifndef _VI_SKIP
	if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
		//VI
		batchFail = 0;
		strBatch.Empty();
		strBatch += "VI: ";
		for (i = 0; i < MAX_VI_TEST; i++)
		{
			if (G_MainWnd->m_RecipeSetDlg.part_Test.viTestInfo[i].bTest)
			{
				if (!m_PartData[stage].viPass[i])
				{
					batchFail++;
					str.Format(L"%s ", i, m_PartData[stage].viRes);
					strBatch += str;
				}
			}
		}
		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1)
		{
			if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_PASS_MODE] == 0)
			{
				//AFTER VISION TEST END, THIS USE.1114_kys		
				if (batchFail){
					strNG += strBatch;
					fail += batchFail;
				}
			}
		}
	}
//#endif

#ifdef _FI

	//OSC
	batchFail = 0;
	strBatch.Empty();
	strBatch+="OSC: ";
	for(i=0; i< MAX_OSC_TEST; i++)
	{
		if(G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[i].bTest)
		{
			if(!m_PartData[stage].phasePass[i])
			{
				batchFail++;
				str.Format(L"%s ", i,m_PartData[stage].phase);
				strBatch+=str;
			}
		}
	}//for(i=0;
	if(batchFail){
		strNG+=strBatch;
		fail+=batchFail;
	}


	//HIPOT
	batchFail = 0;
	strBatch.Empty();
	strBatch+="HIPOT: ";
	for(i=0; i< MAX_HIPOT_TEST; i++)
	{
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].bTest)
		{
			if(!m_PartData[stage].iPass[i])
			{
				batchFail++;
				str.Format(L"[%d]%10.4f mA", i,m_PartData[stage].i[i]*(+1.0E+3));
				strBatch+=str;
			}
		}
	}//for(i=0;
	if(batchFail){
		strNG+=strBatch;
		fail+=batchFail;
	}

	//IR
	batchFail = 0;
	strBatch.Empty();
	strBatch+="IR: ";
	for(i=0; i< MAX_IR_TEST; i++)
	{
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].bTest)
		{
			if(!m_PartData[stage].irPass[i])
			{
				batchFail++;
				str.Format(L"[%d]%10.4f MOhm", i,m_PartData[stage].ir[i]*(+1.0E-6));
				strBatch+=str;
			}
		}
	}//for(i=0;
	if(batchFail){
		strNG+=strBatch;
		fail+=batchFail;
	}

#endif

	if(!fail){ 
		m_PartData[stage].finalPass = 1;//PASS
	}else{
		m_PartData[stage].finalPass = 0;
	}
	if(strNG.GetLength() < 1020)
		memcpy(m_PartData[stage].ngItems, strNG, strNG.GetLength()*sizeof(wchar_t));
	else
		memcpy(m_PartData[stage].ngItems, strNG, 1020 * sizeof(wchar_t));
	//Production data update
	G_MainWnd->m_DataHandling.updateProductionData(m_PartData[stage].finalPass);

	return fail;
}

int CInspectThread::setBatchStatus(int batch, int status)
{
	CString str;
	switch(batch){
	case LOAD:	stLoadBatch.status = status;  break;
	case LCR:			stLCRBatch.status = status; break;
	case OSC:			stOSCBatch.status = status; break;
	case DCR:			stDCRBatch.status = status; break;
	case HIPOT:		stHIPOTBatch.status = status; break;
	case IR:				stIRBatch.status = status; break;
	case VI:				stVIBatch.status = status; break;
	case UNLOAD:		stUNLOADBatch.status = status; break;
	case SHUTTLE:	stShuttleBatch.status = status; break;
	}
	//STORE STEP IN FLASH DATA
	G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_STATUS + batch] = status;
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_STATUS + batch]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LOAD_STATUS + batch],str); 

	return 0;
}

int CInspectThread::restoreInspectionStatus(void)
{
	int i, step;
	for(i = LOAD; i<= SHUTTLE; i++)
	{
		m_fnSetBatchStep(i, G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_BATCH_STEP + i]);
		setBatchStatus(i, G_MainWnd->m_DataHandling.m_FlashData.data[LOAD_STATUS + i]);
	}

	stLCRBatch.LCR_Axis_step = G_MainWnd->m_DataHandling.m_FlashData.data[LCR_AXIS_STEP];
	//stShuttleBatch.allow = G_MainWnd->m_DataHandling.m_FlashData.data[SHUTTLE_ALLOW];
	//stShuttleBatch.request = G_MainWnd->m_DataHandling.m_FlashData.data[SHUTTLE_REQUEST];

	stDCRBatch.dcrStep = G_MainWnd->m_DataHandling.m_FlashData.data[DCR_INSPECT_STEP];
	stHIPOTBatch.hiPotStep = G_MainWnd->m_DataHandling.m_FlashData.data[HIPOT_INSPECT_STEP];
	stIRBatch.irStep = G_MainWnd->m_DataHandling.m_FlashData.data[IR_INSPECT_STEP];

	//shuttle allow, request
	for(i = LOAD; i< SHUTTLE; i++)
	{
		step = m_fnGetBatchStep(i);
		if(step == STEP17)m_fnSetBatchStep(i, STEP16);
		if(step == STEP0 || step == STEP15 || step == STEP16 || step == STEP17 )
			stShuttleBatch.allow |=  0x0001 << i;

	}

	return 0;
}


int CInspectThread::errorRoutine(void)
{

	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN )
	{
		holdAutoRun();
		checkStatus = CHECK_SUSPEND;
	}
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO )
	{
		checkStatus = CHECK_SUSPEND;
	}
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	G_SystemModeData.unSystemError = SYSTEM_ERROR;
	G_MainWnd->m_ServerDlg.m_btnAuto.SetColorChange(WHITE, BLACK);
	G_MainWnd->m_ServerDlg.m_btnManual.SetColorChange(GREEN, BLACK);
	G_MainWnd->m_ServerDlg.m_btnStart.SetColorChange(WHITE, BLACK);
	G_MainWnd->m_ServerDlg.m_btnStop.SetColorChange(WHITE, BLACK);

	//TOWER LAMP ALARM
	G_MainWnd->m_InstrumentDlg.safeCloseInstrument();
	return 0;
}

int CInspectThread::errorResetRoutine(void)
{
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(L"", BLACK, RGB(230,230,230));
	G_SystemModeData.unSystemError = SYSTEM_OK;
	G_MainWnd->m_PIODlg.buzzstop = 0;
	return 0;
}

int CInspectThread::showNGResult(void)
{
	CString strValueData;
	int i, j;
	int stg;

#ifdef _FI
	stg = STG_VI_UNLOAD;
#else
	stg = STG_DCR;
#endif
	//LCR
	for(j=0; j<MAX_L_TEST; j++){
		if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
		{
			strValueData.Format(_T("%10.2f uH"), m_PartData[stg].l[j]*(+1.0E+6));
			G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(j+1, 4)->SetText(strValueData);
			if(m_PartData[stg].lPass[j])
			{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(j+1, 5)->SetText(L"OK");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(j+1,5)->SetBackClr(GREEN);
			}else{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(j+1, 5)->SetText(L"NG");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(j+1, 5)->SetBackClr(RED);
			}
		}
	}//for(j=0; j<MAX_L_TEST; j++){
#ifdef _FI
	//OSC
	for(j=0; j<MAX_OSC_TEST; j++){
		i = j + MAX_L_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[j].bTest)
		{
			strValueData.Format(_T("%s"), m_PartData[stg].phase);
			G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 4)->SetText(strValueData);
			if(m_PartData[stg].phasePass[j])
			{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"OK");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(GREEN);
			}else{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"NG");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(RED);
			}
		}
	}//for(j=0; j<MAX_OSC_TEST; j++){
#endif
	//DCR
	for(j=0; j<MAX_DCR_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
		{
			strValueData.Format(_T("%10.2f mOhm"), m_PartData[stg].r[j]*(+1.0E+3));
			G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 4)->SetText(strValueData);
			if(m_PartData[stg].rPass[j])
			{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"OK");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(GREEN);
			}else{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"NG");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(RED);
			}
		}
	}//for(j=0; j<MAX_DCR_TEST; j++){
#ifdef _FI
	//HIPOT
	for(j=0; j<MAX_HIPOT_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j].bTest)
		{
			strValueData.Format(_T("%10.2f mA"), m_PartData[stg].i[j]*(+1.0E+3));
			G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 4)->SetText(strValueData);
			if(m_PartData[stg].iPass[j])
			{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"OK");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(GREEN);
			}else{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"NG");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(RED);
			}
		}
	}//for(j=0; j<MAX_HIPOT_TEST; j++){

	//IR
	for(j=0; j<MAX_IR_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[j].bTest)
		{
			try{
				strValueData.Format(_T("%10.2f MOhm"), m_PartData[stg].ir[j]*(+1.0E-6));
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 4)->SetText(strValueData);
				if(m_PartData[stg].irPass[j])
				{
					G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"OK");
					G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(GREEN);
				}else{
					G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"NG");
					G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(RED);
				}
			}catch (CException* ex)
			{
				ex->ReportError();
				TCHAR szMsg[1024] = {0};
				ex->GetErrorMessage(szMsg, 1024);
				G_AddLog(3, szMsg);
				G_AddLog(3,L"IR result Format Exception");
			}
		}
	}//for(j=0; j<MAX_HIPOT_TEST; j++){
#endif

	//VI
	for(j=0; j<MAX_VI_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + MAX_IR_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.viTestInfo[j].bTest)
		{
			strValueData.Format(_T("%s"), m_PartData[stg].viRes[j]);
			G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 4)->SetText(strValueData);
			if(m_PartData[stg].viPass[j])
			{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"OK");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(GREEN);
			}else{
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetText(L"NG");
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, 5)->SetBackClr(RED);
			}
		}
	}//for(j=0; j<MAX_VI_TEST; j++){

	G_MainWnd->m_ServerDlg.m_GridMainResult.Refresh();
	return 0;
}

void CInspectThread::m_fnPosCheckMotion()
{
	
	G_MainWnd->m_AxisDlg.PosAndVel();
	G_MainWnd->m_AxisDlg.m_fnDisplayLCRAxisStatus();    //160114_kys
#ifdef _FI
	//if (G_SystemModeData.unSystemMode == SYSTEM_AUTO||
		//G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
	//{
		stg5HIPOTTest();
		stg5IRTest();
	//}
#endif
	
}