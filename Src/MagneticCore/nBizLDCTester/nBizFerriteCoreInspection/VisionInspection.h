#pragma once
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "1.Common\RoundButton\RoundButton2.h"
#include "afxwin.h"


// Include files to use the PYLON API.
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

#define LIVE_SIZE_W 1280
#define LIVE_SIZE_H 960

// Namespace for using pylon objects.
using namespace Pylon;

// Namespace for using cout.
using namespace std;
using namespace Basler_GigECameraParams;

#define UM_VI							WM_USER + 300
#define UW_GRAB						0
#define UW_VINSPECTION			1
#define UW_VINSPECTION_G		2

#define UM_VI_CLEAR			0
#define UM_VI_ONGOING		1
#define UM_VI_DONE			2
#define UM_VI_ERROR			4

#define CAM_DISCONNECTED		0
#define CAM_CONNECTED			1

typedef struct _VI_MSG
{

	int BUSY; 
	int RSP[10]; //MESSAGE No.

	_VI_MSG(){	memset(this,0x00, sizeof(VI_MSG));}
}VI_MSG;





class CVisionInspection :public CDialogEx
{
		DECLARE_DYNAMIC(CVisionInspection)

public:
	CVisionInspection(CWnd* pParent = NULL);
	~CVisionInspection(void);

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_VISION };
	virtual BOOL OnInitDialog();
	void m_fnInit();
	void m_fnDeInit();
	int swLight;

private :
	NIPJob m_dJob;

protected:

	virtual void DoDataExchange(CDataExchange* pDX);
	DECLARE_MESSAGE_MAP()

	Pylon::PylonAutoInitTerm autoInitTerm;
	CBaslerGigEInstantCamera *pCam;

	// This smart pointer will receive the grab result data.
	CGrabResultPtr ptrGrabResult;

	// Number of images to be grabbed.
	static const uint32_t c_countOfImagesToGrab = 5;

	void drawImage(void);
	void drawRect();

	//Temp
	int points[4]; //might be less than max 32 defects in a image , memory fault_

public:

	IplImage *pRawBuf;
	IplImage *pImgBuf;

	size_t ImageSize;

	int nPass;
	int camConnected;

	afx_msg void OnBnClickedLight();

	int pylonInitGrab(void);
	int pylonInitGrabOne(void);
	int pylonInitGrabSWTrigger(void);

	int pylonGrab(void);
	int pylonGrabOne(void);
	int pylonGrabSWTrigger(void);
	int pylonSetParam(void);
	CStatic m_pic;
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedBtnGrab();

	void SetUMMsgResponse(int msg, int rsp);
	int GetUMMsgResponse(int msg);

	void ChangeRecipeSettings(bool nInit = false);
	bool LoadVIData();

private :
	bool LoadNIPJob();
	bool DoVisionInspection();
	void DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFoundDefect);

protected:
	afx_msg LRESULT OnUmVi(WPARAM wParam, LPARAM lParam);
	VI_MSG msgRsp;
public:
	afx_msg void OnBnClickedBtnVisionInspection();
};

