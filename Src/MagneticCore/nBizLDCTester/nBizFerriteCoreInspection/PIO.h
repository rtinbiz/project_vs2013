#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "stdafx.h"
#include "AxisMotion.h"
#include "tmcDApiAed.h"
#include "tmcDApiDef.h"
#include "1.Common/Led/LedButton.h"
#include "1.Common\roundbutton\roundbutton2.h"


#define WM_TIMER_IO WM_USER + 100
#define WM_TIMER_500mS WM_TIMER_IO + 1
// CIOViewDlg 대화 상자입니다.
#define BG_COLOR WHITE
#define FG_COLOR GREEN

#define IO_NUM 64

#ifdef _FI
/////////////////////////////////////////////////////////////////////////////////////////////
//DI DEFINE

#define IN_EM_STOP					0

#define IN_LOAD_JIG_DETECT		1
#define IN_LCR_JIG_DETECT			2
#define IN_OSC_JIG_DETECT			3
#define IN_DCR_JIG_DETECT			4
#define IN_HIPOTIR_JIG_DETECT		5
#define IN_UNLOAD_JIG_DETECT		6

#define IN_SHUTTLE_FORWARD		7
#define IN_SHUTTLE_RETURN			8
#define IN_CV_PART_RETURN			9

#define IN_DOOR_CLOSE				10
#define IN_DOOR_LOCK					11

#define IN_LOAD_AREA_SENSOR1	12
#define IN_LOAD_AREA_SENSOR2	13
#define IN_AIR_SENSOR				14
//1121_kys
#define IN_LOAD_JIG_DETECT2		15

#define IN_LCR_LIFT_UP				16
#define IN_LCR_LIFT_DOWN			17
#define IN_OSC_LIFT_UP				18
#define IN_OSC_LIFT_DOWN			19
#define IN_DCR_LIFT_UP				20
#define IN_DCR_LIFT_DOWN			21
#define IN_HIPOTIR_LIFT_UP			22
#define IN_HIPOTIR_LIFT_DOWN		23

#define IN_SHUTTLE_UP				24
#define IN_SHUTTLE_DOWN			25

//UNLOAD
#define IN_UNLOAD_CV_UP			26
#define IN_UNLOAD_CV_DOWN		27
#define IN_LCR_SHORT_UP				29
#define IN_LCR_SHORT_DOWN		28

//HOME ENABLE SENSOR
#define IN_LCR_PROBE_HOME			30
#define IN_LCR_SHORT_HOME		31

const CString ST_FI_DI[] = {
_T("EM STOP"),
_T("LOAD_JIG_DETECT"),
_T("LCR_JIG_DETECT"),	
_T("OSC_JIG_DETECT"),
_T("DCR_JIG_DETECT"),
_T("HIPOTIR_JIG_DETECT"),
_T("UNLOAD_JIG_DETECT"),

_T("SHUTTLE_FORWARD"),
_T("SHUTTLE_RETURN"),
_T("CV_PART_RETURN"),

_T("DOOR_CLOSE"),
_T("DOOR_LOCK"),

_T("LOAD_AREA_SENSOR1"),
_T("LOAD_AREA_SENSOR2"),

_T("AIR_SENSOR"),
_T("LOAD_JIG_DETECT2"),

_T("LCR_LIFT_UP"),
_T("LCR_LIFT_DOWN"),
_T("OSC_LIFT_UP"),
_T("OSC_LIFT_DOWN"),
_T("DCR_LIFT_UP"),
_T("DCR_LIFT_DOWN"),
_T("HIPOTIR_LIFT_UP"),
_T("HIPOTIR_LIFT_DOWN"),

_T("SHUTTLE_UP"),
_T("SHUTTLE_DOWN"),

	//UNLOAD
_T("UNLOAD_CV_UP"),
_T("UNLOAD_CV_DOWN"),

_T("LCR_SHORT_DOWN"),
_T("LCR_SHORT_UP"),

	//LCR_HOME_ENABLE
_T("LCR_PROBE_HOME"),
_T("LCR_SHORT_HOME"),

};
/////////////////////////////////////////////////////////////////////////////////////////////
//DO DEFINE

#define OUT_TOWER_RED				0
#define OUT_TOWER_YELLOW		1
#define OUT_TOWER_GREEN			2
#define OUT_BUZZER					3
#define OUT_UNLOAD_CV_CCW		4
#define OUT_UNLOAD_CV_RUN		5
#define OUT_DOOR_OPEN				6
//#define OUT_DOOR_RIGHT_OPEN		7
//#define OUT_DOOR_OPEN				8

#define OUT_LCR_PROBE_Z_UNBREAK		14
#define OUT_LCR_SHORT_Z_UNBREAK		15

#define OUT_LCR_LIFT_UP				16
#define OUT_LCR_LIFT_DOWN		17
#define OUT_OSC_LIFT_UP			18
#define OUT_OSC_LIFT_DOWN		19
#define OUT_DCR_LIFT_UP				20
#define OUT_DCR_LIFT_DOWN		21
#define OUT_HIPOTIR_LIFT_UP		22
#define OUT_HIPOTIR_LIFT_DOWN	23

#define OUT_SHUTTLE_UP				24
#define OUT_SHUTTLE_DOWN		25

#define OUT_UNLOAD_CV_UP			26
#define OUT_UNLOAD_CV_DOWN		27
#define OUT_LCR_SHORT_UP			29
#define OUT_LCR_SHORT_DOWN		28

//HIPOT/IR KRY
#define OUT_KR1_BASE					32
#define OUT_KR2_						33
#define OUT_KR3_						34
#define OUT_KR4_						35
#define OUT_KR5_						36
#define OUT_KR6_IR_CHASSIS		37
#define OUT_KR7_						38
#define OUT_KR8_						39

#define OUT_KR9_HIPOT_POS			40
#define OUT_KR10_HIPOT_NEG		41
#define OUT_KR11_IR_POS				42
#define OUT_KR12_IR_NEG				43
#define OUT_KR13_HIPOT_INTLOCK	44

const CString ST_FI_DO[] = {

_T("TOWER_RED"),
_T("TOWER_YELLOW"),
_T("TOWER_GREEN"),
_T("BUZZER"),
_T("UNLOAD_CV_CCW"),
_T("UNLOAD_CV_RUN"),
_T("DOOR_OPEN"),
_T(""),

_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T("LCR_PROBE_Z_UNBREAK"),
_T("LCR_SHORT_Z_UNBREAK"),

_T("LCR_LIFT_UP"),
_T("LCR_LIFT_DOWN"),
_T("OSC_LIFT_UP"),
_T("OSC_LIFT_DOWN"),
_T("DCR_LIFT_UP"),
_T("DCR_LIFT_DOWN"),
_T("HIPOTIR_LIFT_UP"),
_T("HIPOTIR_LIFT_DOWN"),

_T("SHUTTLE_UP"),
_T("SHUTTLE_DOWN"),
_T("UNLOAD_CV_UP"),
_T("UNLOAD_CV_DOWN"),

_T("LCR_SHORT_DOWN"),
_T("LCR_SHORT_UP"),
_T(""),
_T(""),

	//HIPOT/IR KRY
_T("KR1_BASE"),
_T("KR2_"),
_T("KR3_"),
_T("KR4_"),
_T("KR5_"),
_T("KR6_IR_CHASSIS"),
_T("KR7_"),
_T("KR8_"),

_T("KR9_HIPOT_POS"),
_T("KR10_HIPOT_NEG"),
_T("KR11_IR_POS"),
_T("KR12_IR_NEG"),
_T("KR13_HIPOT_INTLOCK"),

};

#else //공정검사

/////////////////////////////////////////////////////////////////////////////////////////////
//DI DEFINE

#define IN_EM_STOP					0
#define IN_LCR_JIG_DETECT			1
#define IN_DCR_JIG_DETECT			2
#define IN_UNLOAD_JIG_DETECT		3
#define IN_SHUTTLE_FORWARD		4
#define IN_SHUTTLE_RETURN			5
#define IN_LOAD_AREA_SENSOR1	6
#define IN_AIR_SENSOR				7


#define IN_LCR_LIFT_UP				16
#define IN_LCR_LIFT_DOWN			17
#define IN_DCR_LIFT_UP				18
#define IN_DCR_LIFT_DOWN			19
#define IN_SHUTTLE_UP				20
#define IN_SHUTTLE_DOWN			21
#define IN_LCR_SHORT_UP				22
#define IN_LCR_SHORT_DOWN		23

//HOME ENABLE SENSOR
#define IN_LCR_PROBE_HOME			30
#define IN_LCR_SHORT_HOME		31


const CString ST_FI_DI[] = {
_T("EM STOP"),
_T("LCR_JIG_DETECT"),
_T("DCR_JIG_DETECT_T("),
_T("UNLOAD_JIG_DETECT"),
_T("SHUTTLE_FORWARD_T("),
_T("SHUTTLE_RETURN"),
_T("LOAD_AREA_SENSOR1"),
_T("AIR_SENSOR"),

_T("昌盛"), //unicode test
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),

_T("LCR_LIFT_UP_T("),
_T("LCR_LIFT_DOWN"),
_T("DCR_LIFT_UP"),
_T("DCR_LIFT_DOWN_T("),
_T("SHUTTLE_UP"),
_T("SHUTTLE_DOWN"),
_T("LCR_SHORT_UP"),
_T("LCR_SHORT_DOWN"),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
//LCR_HOME_ENABLE
_T("LCR_PROBE_HOME"),
_T("LCR_SHORT_HOME"),

};

/////////////////////////////////////////////////////////////////////////////////////////////
//DO DEFINE

#define OUT_TOWER_RED				0
#define OUT_TOWER_YELLOW		1
#define OUT_TOWER_GREEN			2
#define OUT_BUZZER					3

#define OUT_LCR_PROBE_Z_UNBREAK		14
#define OUT_LCR_SHORT_Z_UNBREAK		15
#define OUT_LCR_LIFT_UP				16
#define OUT_LCR_LIFT_DOWN		17
#define OUT_DCR_LIFT_UP				18
#define OUT_DCR_LIFT_DOWN		19
#define OUT_SHUTTLE_UP				20
#define OUT_SHUTTLE_DOWN		21
#define OUT_LCR_SHORT_UP			22
#define OUT_LCR_SHORT_DOWN		23

const CString ST_FI_DO[] = {

_T("OUT_TOWER_RED"),
_T("TOWER_YELLOW"),
_T("TOWER_GREEN"),
_T("BUZZER"),
_T(""),
_T(""),
_T(""),
_T(""),

_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T(""),
_T("LCR_PROBE_Z_UNBREAK"),
_T("LCR_SHORT_Z_UNBREAK"),

_T("LCR_LIFT_UP"),
_T("LCR_LIFT_DOWN"),
_T("DCR_LIFT_UP"),
_T("DCR_LIFT_DOWN"),
_T("SHUTTLE_UP"),
_T("SHUTTLE_DOWN_T("),
_T("LCR_SHORT_UP"),
_T("LCR_SHORT_DOWN"),
};


#endif



/////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _TIMER
{
	int	Accu;		//timer count
	int	On;		//timer input
	int	Set;		//timer settime
	int	OTE;		//timer output

	_TIMER(){memset(this,0x00, sizeof(TIMER));}
	void clear(){memset(this,0x00, sizeof(TIMER));}
} TIMER;

typedef struct _COUNTER
{
	int	Accu;		//counter 
	int	On;		//counter input
	int	Set;		//counter settime
	int	OTE;		//counter output

	_COUNTER(){memset(this,0x00, sizeof(COUNTER));}
	void clear(){memset(this,0x00, sizeof(COUNTER));}
} COUNTER;


class PIO :public CDialogEx
{
	DECLARE_DYNAMIC(PIO)

public:
	PIO(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~PIO();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_IO };

	CRITICAL_SECTION m_criDIO; //DIO read/write mutex

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	void m_fnDeInit(void);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();

	unsigned int		m_uiOutStatus;
	unsigned long		m_uiGetDin32Ch, m_uiGetDin32Ch2;
	unsigned long		m_uiGetDout32Ch, m_uiGetDout32Ch2;

	BOOL LCRLimitDisable;
	BOOL doorClose;

	//U16		m_wOutStatus;
	//U32		m_uiGetDin32Ch, m_uiGetDin32Ch2;
	//U32		m_uiGetDout32Ch, m_uiGetDout32Ch2;
	//3 cards
	unsigned int		m_Din32[3][2];
	unsigned int		m_Dout32[3][2];

	CLedButton m_DILED[IO_NUM];
	CLedButton m_DOLED[IO_NUM];

	int DI[IO_NUM];
	int DO[IO_NUM];

	int DI_prev[IO_NUM];
	int DO_prev[IO_NUM];

	TIMER	timer[IO_NUM]; //50ms
	COUNTER	counter[IO_NUM]; //
	int twlamp, buzzstop;

	afx_msg void OnBnClickedChkDO(UINT nID);
	int timerAutoRunDIO(void);

	int readDI(int address, int board);
	int readDI_prev(int address, int board);
	int readDO(int address, int board);
	int writeDO(int val, int address, int board);
	int timerCheckDIO(void);
	int home(void);

	afx_msg void OnBnClickedBtnStartBatch(UINT nID);
	afx_msg void OnBnClickedBtnStopBatch(UINT nID);
	afx_msg void OnBnClickedBtnStepBatch(UINT nID);
	afx_msg void OnBnClickedBtnResetBatch(UINT nID);
	afx_msg void OnBnClickedBtnHomeBatch(UINT nID);

	CRoundButton2 m_btnStart[9];
	CRoundButton2 m_btnStop[9];
	CRoundButton2 m_btnStep[9];
	CRoundButton2 m_btnErrorReset[9];
	CRoundButton2 m_btnHome[9];
	CRoundButton2 m_btnBatchClear[9];

	afx_msg void OnBnClickedBtnLcrLoadCorrect();
	afx_msg void OnBnClickedBtnDcrZeroAdjust();
	CRoundButton2 m_btnLcrRoadCorrect;
	CRoundButton2 m_btnDcrZeroAdjust;
	int initIOLEDLabel(void);

#ifdef _FI
	int switchRelayHipot(int on, int step);
	int switchRelayIR(int on, int step);
#endif
	afx_msg void OnBnClickedBtnOffset(UINT nID);
	afx_msg void OnBnClickedBtnClearBatch(UINT nID);

	CRoundButton2 m_btnLCROffset;
	CRoundButton2 m_btnDCROffset;
	CRoundButton2 m_btnHIPOTOffset;
	CRoundButton2 m_btnIROffset;


};

