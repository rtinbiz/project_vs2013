// ColorStatic2.cpp: implementation of the CColorStatic2 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ColorStatic2.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CColorStatic2, CStatic)
	//{{AFX_MSG_MAP(CColorStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

HBRUSH CColorStatic2::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	pDC->SetBkColor(m_refBack);
	pDC->SetTextColor(m_refText);

	return m_brush;
	
	//	// TODO: Return a non-NULL brush if the parent's handler should not be called
	//	return NULL;
}

CColorStatic2::CColorStatic2()
{
	m_refText = DARK_GRAY;
	m_refBack = BRIGHT_GRAY;

	m_refBlinkColor1 = DARK_GRAY;
	m_refBlinkColor2 = BRIGHT_GRAY;

	m_brush.CreateSolidBrush(m_refBack);
	m_nBlink = 0;
}

CColorStatic2::~CColorStatic2()
{
	if(this->m_hWnd != NULL)
	{
		if(m_nBlink) KillTimer(0);
	}
	m_brush.DeleteObject();
	if(m_myFont.m_hObject != NULL) m_myFont.DeleteObject();
}

void CColorStatic2::SetText(CString strText, COLORREF textref/*=BLACK*/, COLORREF batangref/*=BRIGHT_GRAY*/)
{
	CString strWindowText;

	GetWindowText(strWindowText);

	if(m_refText == textref && m_refBack == batangref && !strText.Compare(strWindowText)) return;

	m_refText = textref;
	m_refBack = batangref;

	m_brush.DeleteObject();
	m_brush.CreateSolidBrush(m_refBack);

	SetWindowText(strText);
}

void CColorStatic2::SetBackColor(COLORREF ref/*=BRIGHT_GRAY*/)
{
	if(m_refBack == ref) return;

	m_refBack = ref;

	m_brush.DeleteObject();
	m_brush.CreateSolidBrush(m_refBack);

	Invalidate();
}

void CColorStatic2::SetTextColor2(COLORREF ref/*=BLACK*/)
{
	if(m_refText == ref) return;

	m_refText = ref;

	Invalidate();
}

COLORREF CColorStatic2::GetBackColor()
{
	return m_refBack;
}

COLORREF CColorStatic2::GetTextColor()
{
	return m_refText;
}

CString CColorStatic2::GetText()
{
	CString strText;

	GetWindowText(strText);

	return strText;
}

void CColorStatic2::SetFont2(int p_nHeight, int p_nWidth,int p_nWeight, CString strFace/*=""*/)
{
	BOOL	bCheck;
	CFont	*cur_font;
	LOGFONT cur_logfont;
	
	cur_font = GetFont();	
	cur_font->GetLogFont(&cur_logfont);
	
	if(p_nHeight) 	cur_logfont.lfHeight	= (long)p_nHeight;
	if(p_nWidth)	cur_logfont.lfWidth		= (long)p_nWidth;
	if(p_nWeight)	cur_logfont.lfWeight	= (long)p_nWeight;

	if(!strFace.IsEmpty()) _tcscpy_s(cur_logfont.lfFaceName, strFace);

	if(m_myFont.m_hObject != NULL) m_myFont.DeleteObject();
	bCheck = m_myFont.CreateFontIndirect(&cur_logfont);

	SetFont(&m_myFont);

	Invalidate();
}

void CColorStatic2::SetBackTextColor(COLORREF ref/*=BRIGHT_GRAY*/, COLORREF textref/*=BLACK*/)
{
	SetBackColor(ref);
	SetTextColor2(textref);
}



void CColorStatic2::OnTimer(UINT_PTR nIDEvent) 
{
	if(GetBackColor() == m_refBlinkColor1)
		SetBackColor(m_refBlinkColor2);
	else
		SetBackColor(m_refBlinkColor1);
		
	CStatic::OnTimer(nIDEvent);
}

void CColorStatic2::StartBlink(int nPeriod, COLORREF first, COLORREF next)
{
	if(m_nBlink) return;

	m_refBlinkColor1 = first;
	m_refBlinkColor2 = next;

	m_nBlink = 1;
	SetTimer(0, nPeriod, NULL);
}


void CColorStatic2::StopBlink()
{
	m_nBlink = 0;
	KillTimer(0);
}

int CColorStatic2::IsBlink()
{
	return m_nBlink;
}

void CColorStatic2::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CStatic::OnLButtonDown(nFlags, point);
}
