// LogMng.h: interface for the CLogMng class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOGMNG_H__CA6292D0_6D51_4170_B5FB_FC311AC39425__INCLUDED_)
#define AFX_LOGMNG_H__CA6292D0_6D51_4170_B5FB_FC311AC39425__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "filemng.h"

class CLogMng
{
public:
	CLogMng();
	virtual ~CLogMng();

	int				m_fnSetAutoDeleteInfo(CString strFilePath, int nPeriod=30);
	void			m_fnSetInitInfo(wchar_t* szLogPath, wchar_t* szHeaderName, int nLogPeriod=2);
	BOOL			m_fnLogMngWriteLog(wchar_t* szLogText, BOOL fIsAddNewLine=TRUE);
	BOOL			m_fnLogMngWriteLog(wchar_t* szLogText, BOOL fIsDateFolder, BOOL fIsAddNewLine=TRUE);
	BOOL			m_fnMakeDirectory(CString strPathName);

// Attribute
protected:
	int				m_nLogPeriod;
	CString			m_strLogPath;
	CString			m_strHeaderName;
	CFileMng		m_fileMng;

	// Auto Delete..
	HANDLE			m_hThread;					// Thread Handle.
	int				m_nDeletePeriod;
	CString			m_strDeleteFilePath;
	BOOL			m_bFileAutoDelete;
	BOOL			m_bFileAutoDelete_Working;

// Operation
protected:
	CString			m_fnReturnLogTextHeader();
	CString			m_fnReturnLogFileName(BOOL bDataReturn=TRUE);
	CString			m_fnGetNowString();
	CString			m_fnGetDateString();

	// Auto Delete..
	int				m_fnStartAutoDelete(BOOL bStart);
	static void		m_fnCheckAutoDelte(LPVOID pWnd);

};

#endif // !defined(AFX_LOGMNG_H__CA6292D0_6D51_4170_B5FB_FC311AC39425__INCLUDED_)
