#pragma once

// CLabelPrint 대화 상자입니다.

#pragma once
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "afxwin.h"
#include "1.Common/IniClass/BIniFile.h"
#include "PathDefine.h"
#include "1.Common/LogMng/LogMng.h"
#include "1.Common\roundbutton\roundbutton2.h"
#include "1.Common\colorstatic\colorstatic2.h"
#include "sqlite\sqlite3.h"

#define LINE_MAX				20
//#define LINE_MAX				15
#define LINE_LABEL_INDEX		0
#define LINE_PARTNO			1
#define LINE_VENDORCODE	2
#define LINE_LNO_PREFIX		3
#define LINE_LNO_SUFFIX		4
#define LINE_HEADER0			5
#define LINE_HEADER1			6
#define LINE_HEADER2			7
#define LINE_HEADER3			8
#define LINE_PNO_LBL			9
#define LINE_PNO_DATA		10
#define LINE_LNO_LBL			11
#define LINE_LNO_DATA		12
#define LINE_CODE_DATA		13
//#define LINE_TAIL				
//1111_kys
#define LINE_PNO_LBL1			14
#define LINE_PNO_DATA1		15
#define LINE_LNO_LBL1			16
#define LINE_LNO_DATA1		17
#define LINE_CODE_DATA1	18

#define QR_LABEL_CNT	16

#define WM_TIMER_SERIAL WM_USER + 200
#define WM_TIMER_LABLE  WM_USER + 201

//CUSTOMER
#define MOBIS		0
#define LGE			1
#define MANDO			2
#define QR_LENGTH_MANDO			14
#define QR_LENGTH			22			//MOBIS
#define QR_LENGTH2		25	        //?
#define QR_LENGTH_LGE	18			//LG

//MOBIS ....	PFC Inductor(No.5), Transformr(No.4)
// aaaaaaaaaa,KXBJbbbcccc              a:P/N(10),  ','(1),   KXBJ(창성No고정4),      b:년월일(3),     c:serial No.(4)					 Sum:22

//LG ....			6kw OBC LLC(No.6),   6kw OBC PFC IN(No.7),    COMP(No.8)
// ADaaabccccccccdddd					 AD(창성No고정2),   a:년월일(3),    b:Line(1),     c:P/N(8),    d:serial No.(4)					  Sum:18

//MANDO....    PS_EV_OBC(No.9)
//  TQ72-aabbccddd						TQ72(P/N고정4),		'-'(1),			a:년(2),  b:월(2), c:일(2),	d:serial No.(3)						Sum:14


//CUSTOMER & P/N
#define MOBIS_4			_T("3D36469035")
#define MOBIS_5			_T("3D36470035")


enum
{
	SCAN_CHECK_OLD=0,
	SCAN_CHECK_NEW,
	SCAN_CHECK_MAX
};

enum
{
	QRLENGTH_OLD = 0,
	QRLENGTH_NEW,
	QRLENGTH_MAX
};

enum
{
	LABELPRINTER_OLD = 0,
	LABELPRINTER_NEW,
	LABELPRINTER_MAX
};




class CLabelPrint : public CDialogEx
{
	DECLARE_DYNAMIC(CLabelPrint)

public:
	CLabelPrint(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CLabelPrint();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_LABEL };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);

	DECLARE_MESSAGE_MAP()



	sqlite3 *db;
	sqlite3_stmt *stmtSelectLable;	sqlite3_stmt *stmtSelectLable2;
	sqlite3_stmt *stmtSelectLableCount;
	sqlite3_stmt *stmtInsertLable;
	sqlite3_stmt *stmtUpdateLable;
	sqlite3_stmt *stmtUpdateLable2; //LOWER SERIAL UPDATE
	sqlite3_stmt *stmtDeleteLable;

	int lableHour;
	int rowNo;

public:


	typedef struct PART_LABEL
	{
		wchar_t QRcode[QR_LABEL_CNT][512]; //part info
		wchar_t QRcodeScan[256]; 
		int idxPrinterLabel;
		PART_LABEL(){memset(this,0x00, sizeof(PART_LABEL)); }
	};

	CCriticalSection m_csLabel;
	PART_LABEL m_partLabel;

	CString ZPLString; //complete string sent to printer
	CString strZPL[LINE_MAX]; //string array
	int	nZPL; //total line count
	int nLotNo;
	CString strLotNo;
	CString strLotNo_spare;
	CString strLotNoCode;
	int serialNo;
	int countPrint;
	int customer;
	int m_scancheck[SCAN_CHECK_MAX];
	int m_QRLENGTH[QRLENGTH_MAX];
	int m_LabelPrinterCheck[LABELPRINTER_MAX];
	CString strLotMobis;
	CString strLotLGE;
	CString strLotMANDO;

	CColorStatic2 m_staticPNo;
	CEdit m_editPrintCount;
	CGridCtrl m_GridLabelModel;

	//afx_msg void OnBnClickedBtnLotno();
	afx_msg void OnBnClickedBtnManuPrint();

	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	int initGridModel(void);
	int fillGridModel(void);
	int setModel(CString model);

	int printLabel();
	int makeZPLString(int customer, CString model);
	//MAJOR
	int makeZPLStringMOBIS(CString model);
	int makeQRCode(void);
	CString setLotNo(void);
	//LGE
	int makeZPLStringLGE(CString model);
	int makeQRCodeLGE(void);
	CString setLotNoLGE(void);

	//MANDO
	int makeZPLStringMANDO(CString model);
	int makeQRCodeMANDO(void);
	CString setLotNoMANDO(void);

	int m_nManual_Print_Count;
	float m_nManual_Print_Count2;
	int m_nManual_Print_Countnow;

	afx_msg void OnTimer(UINT_PTR nIDEvent);


	int scannerCheck(void);
	int scannerCheckLGE(void);
	int scannerCheckMANDO(void);


	afx_msg void OnBnClickedBtnSerialnoSet();
	CEdit m_editSerialNo;
	afx_msg void OnBnClickedCheckLotNoSet();
	CString getSerialNo(void);
	int sqliteInit();
	CComboBox m_cmbLotMonth;
	CComboBox m_cmbLotDay;
	CRoundButton2 m_btnLotNoSet;
	afx_msg void OnBnClickedBtnLotNoSet();
	CRoundButton2 m_btnSetSerialNo;
	CRoundButton2 m_btnManuPrint;
	int m_fnCheckLotNoDB(CString strLotNo);
	CColorStatic2 m_staticLotSerial;
	CColorStatic2 m_staticLot;
	int updateLabelSerialEnd(int rowNo, int serialEnd);
	afx_msg void OnBnClickedButton1();
	CRoundButton2 m_btnScanCountReset;
};
