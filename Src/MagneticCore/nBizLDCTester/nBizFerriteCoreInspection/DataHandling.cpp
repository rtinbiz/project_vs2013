#include "stdafx.h"
#include "DataHandling.h"
#include "MainWnd.h"

extern CMainWnd* G_MainWnd;

CDataHandling::CDataHandling(void)
{	
	memset(m_chImageSavePath, 0x00, MAX_PATH);
	memset( m_chImageSaveNGPath, 0x00, MAX_PATH);
}

CDataHandling::~CDataHandling(void)
{
}

void CDataHandling::m_fnInit()
{
	CString str;
	m_fnMakeDirectory(SYSTEM_FOLDER);

	m_iniSystemParam.SetFileName(SYSTEM_PARAM_INI_PATH);
	m_iniRecipe.SetFileName(RECIPE_FOLDER);

 	m_LogMng.m_fnSetInitInfo(SYSTEM_LOG_FOLDER,L"ProcessLog");
	m_DayReportMng.m_fnSetInitInfo(RESULT_DATA_FOLDER,L"DayReportLog");
 	m_AlarmMng.m_fnSetInitInfo(SYSTEM_ALARM_LOG_FOLDER,L"AlarmLog");

	m_fnSystemParamLoad();
	loadFlashData();

	bAutoDeleteState = false;
	//////////////////////////////////////////////     
	m_fnMakeDirectoryNewDay();

	str.Format(L"%s\\%s", m_chImageSavePath, m_SystemParam.ProductionModel);
	G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

	str.Format(L"%s\\%s", m_chImageSaveNGPath, m_SystemParam.ProductionModel);
	G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

}

void CDataHandling::m_fnWriteIniFile(UINT nIndex, LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString) 
{
	BOOL bReturn = FALSE;

	
	if(nIndex == SYSTEM_PARAM)
	{
		bReturn = m_iniSystemParam.WriteProfileString(lpszSection, lpszKeyName, lpString);
	}
	else if(nIndex == RECIPE)
	{
		bReturn = m_iniRecipe.WriteProfileString(lpszSection, lpszKeyName, lpString);
	}
	

	if(bReturn == FALSE)
	{
		CString strMessage;
		
		if(nIndex == SYSTEM_PARAM)
			strMessage = "SYSTEM_PARAM";
		else if(nIndex == RECIPE)
			strMessage = "RECIPE";		

		G_AddLog(3,L"%s 파일 기록 Error : 섹션%s, 키네임%s, 스트링%s", strMessage, lpszSection, lpszKeyName, lpString);

	}	
}

CString CDataHandling::m_fnReadIniFile(UINT nIndex, LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR defalut) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"0";
	if(defalut != 0)
	{
		lpDefault = defalut;
	}


	if(nIndex == SYSTEM_PARAM)
	{
		bReturn = m_iniSystemParam.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	}
	else if(nIndex == RECIPE)
	{
		bReturn = m_iniRecipe.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	}

	if(bReturn == FALSE)
	{
		CString strMessage;
		if(nIndex == SYSTEM_PARAM)
			strMessage = "SYSTEM_PARAM";
		else if(nIndex == RECIPE)
			strMessage = "RECIPE";		
		G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}


void CDataHandling::m_fnCvPutImage(IplImage* psrcImage, IplImage* pdstImage, int nX, int nY)
{
	int nHeight, nWidth; 
	unsigned char chValue = 0;

	for(nHeight=0; nHeight<pdstImage->height; nHeight++)
	{
		for(nWidth=0; nWidth<pdstImage->widthStep * 3; nWidth = nWidth + pdstImage->nChannels * 3) // 상하 반전
		{
			chValue = pdstImage->imageData[nHeight * pdstImage->widthStep + nWidth/3];

			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 0] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 1] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
			psrcImage->imageData[(nHeight+nY) * psrcImage->widthStep + (nWidth+(nX*3)) + 2] = pdstImage->imageData[((pdstImage->height-1) - nHeight) * pdstImage->widthStep + nWidth/3];
		}
	}  	
}

void CDataHandling::m_fnSystemParamLoad()
{	
	CString strValueData; 	
	
 	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_PASSWORD);	
	_tcscpy_s(m_SystemParam.sPassword, (LPCTSTR)strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_LIGHT);	
	m_SystemParam.nLightBrightness = _wtoi(strValueData);
		
	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SELECTED_RECIPE);	
	_tcscpy_s(m_SystemParam.ProductionModel, (LPCTSTR)strValueData);

	//fParam
	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_OSC_TOLERANCE);	
	m_SystemParam.fParam[SP_OSC_TOLERANCE] = _wtof(strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_AXIS_TOLERANCE);	
	m_SystemParam.fParam[SP_AXIS_TOLERANCE] = _wtof(strValueData);

	strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_MEASURE_CORRECTION);	
	m_SystemParam.correction = _wtoi(strValueData);

	//iParam
	for (int i = SP_HIPOT_TEST_LATER; i <= SP_VISION_INSPECTION+1; i++)
	{ 		
		strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, ST_TABLE_SYSTEM_0_COL[ i + 5 ]);
		m_SystemParam.iParam[i] = _wtoi(strValueData);
	}

} 
// 
void CDataHandling::m_fnFileWriteRecipeID(char *chRecipeID)
{
	CString strValueData; //OLD Data를 저장하고 NOW Data를 Write한다
	if(chRecipeID != NULL)
	{
		//strValueData = m_fnReadIniFile(RECIPE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW);
		//m_fnWriteIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_OLD, strValueData);

		//m_fnWriteIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW, chRecipeID);
	}
}
// 
CString CDataHandling::m_fnFileReadRecipeID(BOOL bFullPath)
{
 	CString strValueData;
// 	strValueData = m_fnReadIniFile(RECIPE_RECENT_USE, SECTION_STICK_DATA, KEYNAME_RECIPE_ID_NOW);
// 	CString strFullPath;
// 	strFullPath.Format(_T("%s%s.ini", RECIPE_FOLDER, strValueData);
// 	if(bFullPath)
// 		return strFullPath;


	return strValueData;
}

BOOL CDataHandling::m_fnMakeDirectory(CString strPathName)
{
	CString		strTemp, strMakePath;
	int			nTemp, nPos, nLen;
	wchar_t		pBuff[MAX_PATH];
	BOOL		fTemp;

	strPathName.TrimLeft(' ');
	strPathName.TrimRight(' ');

	strPathName.TrimLeft('\\');
	strPathName.TrimRight('\\');

	nPos=m_fnInPos(strPathName,'\\');

	if(nPos<0)
	{
		GetCurrentDirectory(MAX_PATH,pBuff);
		strMakePath.Format(pBuff);
		strMakePath=strMakePath + _T("\\") + strPathName;
		fTemp=CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
		strMakePath.ReleaseBuffer();

		if(fTemp)
			return TRUE;
		else
		{
			nTemp=GetLastError();

			if(nTemp == ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
				return TRUE;
			else
				return FALSE;
		}
	}

	do
	{
		nLen=strPathName.GetLength();
		if(strMakePath.GetLength()==0)
			strMakePath=strPathName.Left(nPos);
		else
			strMakePath=strMakePath + _T("\\") + strPathName.Left(nPos);
		strPathName=strPathName.Right(nLen-nPos-1);

		fTemp = CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
		strMakePath.ReleaseBuffer();

		if(!fTemp)
		{
			nTemp = GetLastError();

			if(nTemp != ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
				return FALSE;
		}

		nPos = m_fnInPos(strPathName,'\\');
		if(nPos<0)
		{
			strMakePath=strMakePath + _T("\\") + strPathName;
			fTemp=CreateDirectory(strMakePath.GetBuffer(strMakePath.GetLength()),NULL);
			strMakePath.ReleaseBuffer();

			if(fTemp)
				return TRUE;
			else
			{
				nTemp = GetLastError();
				if(nTemp == ERROR_ALREADY_EXISTS && nTemp != ERROR_ACCESS_DENIED)
					return TRUE;
				else
					return FALSE;
			}
		}
	} while(nPos>=0);

	return TRUE;
}

int CDataHandling::m_fnInPos(CString strBase, char cChar, int nStartPos)
{
	int		nLen, i;

	if(nStartPos<0)
		return -1;

	nLen=strBase.GetLength();

	if(nStartPos>(nLen-1))
		return -1;

	for(i=nStartPos;i<nLen;i++)
	{
		if(strBase.GetAt(i)==cChar)
			return i;
	}

	return -1;
}

void CDataHandling::m_fnInspectDataReset()
{
	//BOOL bRes = FALSE;
	/////bRes = G_MainWnd->m_BaslerLiveView.m_fnThreadSuspend();	
	//while(1)
	//{
	//	Sleep(10);
	//	if(G_MainWnd->m_BaslerLiveView.m_nThreadStop == TRUE)   // Thread 멈춤 확인
	//	{				
	//		break;
	//	}
	//}
	//G_MainWnd->m_BaslerLiveView.m_fnThreadResume();
}


int CDataHandling::systemRestore(void)
{
	return 0;
}

void CDataHandling::m_fnDayReportWrite(bool bResult)
{
	CStdioFile file;
	CFileException ex;
	CString strFileName, strData,strHeader,strMiddle;
	CString strTempDay, strTempTime;
	CTime ctCurrentTime;
	CFileFind finder;
	int nDayCount = 0;

	int stage;


#ifdef _FI
	stage = STG_VI_UNLOAD;
#else
		if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1)
			{
				stage = STG_VI_UNLOAD;
			}
		else{
				stage = STG_DCR;
			}
	//stage = STG_DCR;            //1124_kys
	//stage = STG_VI_UNLOAD;
#endif


	
	
	/////////////////////////////////////////1110_kys
	CString str;
	int serialIndex = _wtoi(G_MainWnd->m_LabelPrint.strZPL[LINE_LABEL_INDEX]);
	if ((G_MainWnd->m_LabelPrint.customer == MOBIS) || (G_MainWnd->m_LabelPrint.customer == LGE))
		
		{
		str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
			while (str.GetLength()< 4)str.Insert(0, '0');
		}
	else
	{
		str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);
		//str.Format(L"%d", G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + _wtoi(G_MainWnd->m_LabelPrint.strZPL[LINE_LABEL_INDEX])]);
			while (str.GetLength()< 3)str.Insert(0, '0');
	}
	if (//(G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_OLD] == G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_NEW])
		G_MainWnd->m_InspectThread.m_PartData[stage].finalPass)
	{
		G_MainWnd->m_LabelPrint.strLotNo_spare = G_MainWnd->m_LabelPrint.strLotNoCode+str;
	}

	//if ((G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_OLD] == G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_NEW])
	//	&& G_MainWnd->m_InspectThread.m_PartData[stage].finalPass)

	//	{
	//		G_MainWnd->m_LabelPrint.strLotNo_spare = G_MainWnd->m_LabelPrint.strLotNo;
	//	}
	//else if ((G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_OLD] < G_MainWnd->m_LabelPrint.m_LabelPrinterCheck[LABELPRINTER_NEW])
	//	&& G_MainWnd->m_InspectThread.m_PartData[stage].finalPass)
	//		{
	//	G_MainWnd->m_LabelPrint.strLotNo_spare = G_MainWnd->m_LabelPrint.strLotNoCode + str;
	//		}
/////////////////////////////////////////////////////////////////////////////////////////////

	ctCurrentTime = CTime::GetCurrentTime();
	strTempDay = ctCurrentTime.Format(_T("%Y%m%d"));
	strTempTime = ctCurrentTime.Format(_T("%Y/%m/%d %H:%M:%S"));

	strFileName.Format(_T("%s%s_%s.csv"), RESULT_DATA_FOLDER, strTempDay,G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	CString strTest;
	BOOL bWorking = finder.FindFile(strFileName);
#ifdef _FI
//	int serialIndex = _wtoi(G_MainWnd->m_LabelPrint.strZPL[LINE_LABEL_INDEX]);
	strData.Format(_T(",%s,%s,%s,%d,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,\
%s,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%s,%s,%s\n"),
					  strTempTime.Right(8),//1122_kys
					  G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
					  G_MainWnd->m_InspectThread.m_PartData[stage].finalPass ? G_MainWnd->m_LabelPrint.strLotNo_spare : L"0",
		//G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] + 1,
		//G_MainWnd->m_InspectThread.m_PartData[stage].finalPass ? G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] + 1 : 0,
		G_MainWnd->m_InspectThread.m_PartData[stage].finalPass?G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] :0,
		//LCR
		G_MainWnd->m_InspectThread.m_PartData[stage].l[0]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[1]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[2]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[3]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[4]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[5]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[6]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[7]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[8]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[9]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[10]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[11]*(+1.0E+6),
		G_MainWnd->m_InspectThread.m_PartData[stage].l[12]*(+1.0E+6),

		//OSC
		G_MainWnd->m_InspectThread.m_PartData[stage].phase,

		//DCR
		G_MainWnd->m_InspectThread.m_PartData[stage].r[0]*(+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].r[1] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].r[2] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].r[3] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].r[4] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].r[5] * (+1.0E+3),

		//HIPOT
		G_MainWnd->m_InspectThread.m_PartData[stage].i[0]*(+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].i[1] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].i[2] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].i[3] * (+1.0E+3),
		G_MainWnd->m_InspectThread.m_PartData[stage].i[4] * (+1.0E+3),

		//IR
		G_MainWnd->m_InspectThread.m_PartData[stage].ir[0]*(+1.0E-6),
		G_MainWnd->m_InspectThread.m_PartData[stage].ir[1]*(+1.0E-6),
		G_MainWnd->m_InspectThread.m_PartData[stage].ir[2]*(+1.0E-6),
		G_MainWnd->m_InspectThread.m_PartData[stage].ir[3]*(+1.0E-6),
		G_MainWnd->m_InspectThread.m_PartData[stage].ir[4]*(+1.0E-6),
		//VI
		G_MainWnd->m_InspectThread.m_PartData[stage].viPass[0]?L"PASS":L"FAIL",

		//RESULT, NG ITEMS
		G_MainWnd->m_InspectThread.m_PartData[stage].finalPass?L"PASS":L"FAIL",
		G_MainWnd->m_InspectThread.m_PartData[stage].ngItems
		);
#else
	strData.Format(_T(",%s,%s,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,\
%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,%10.4f,\
%s,%s,%s\n"),
				   strTempTime.Right(8),//1122_kys
				   G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,

					//LCR
					G_MainWnd->m_InspectThread.m_PartData[stage].l[0]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[1]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[2]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[3]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[4]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[5]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[6]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[7]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[8]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[9]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[10]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[11]*(+1.0E+6),
					G_MainWnd->m_InspectThread.m_PartData[stage].l[12]*(+1.0E+6),


					//DCR
					G_MainWnd->m_InspectThread.m_PartData[stage].r[0]*(+1.0E+3),
					G_MainWnd->m_InspectThread.m_PartData[stage].r[1]*(+1.0E+3),
					G_MainWnd->m_InspectThread.m_PartData[stage].r[2]*(+1.0E+3),
					G_MainWnd->m_InspectThread.m_PartData[stage].r[3]*(+1.0E+3),
					G_MainWnd->m_InspectThread.m_PartData[stage].r[4]*(+1.0E+3),
					G_MainWnd->m_InspectThread.m_PartData[stage].r[5]*(+1.0E+3),

					//VI
					G_MainWnd->m_InspectThread.m_PartData[stage].viPass[0] ? L"PASS" : L"FAIL",

				   //RESULT, NG ITEMS
				   G_MainWnd->m_InspectThread.m_PartData[stage].finalPass?L"PASS":L"FAIL",
				   G_MainWnd->m_InspectThread.m_PartData[stage].ngItems
				   );
#endif
	//TCHAR sz[512];
	wchar_t sz[512];
	//160217_KYS_DATA_TITLE_ADD
	char DataTitle[512];
	if(!bWorking)
	{
		if(file.Open(strFileName, CFile::modeCreate | CFile::modeReadWrite, &ex)){
			G_MainWnd->m_ServerDlg.nDayReportCount = 0;
			//header 
#ifdef _FI
			strHeader.Format(L"MODEL:,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n", G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
//1113_MERGE_ADD
			file.WriteString(strHeader);
			file.WriteString(L",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
			file.WriteString(L",TIME,MODEL,LOT NO.,SERIAL NO.,LCR(uH),,,,,,,,,,,,,OSC,DCR(mOhm),,,,,,HIPOT(mA),,,,,IR(MOhm),,,,,VI,RESULT,NG ITEM\n");
			//file.WriteString(L",,,,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,1,2,3,4,5,6,1,2,3,4,5,1,2,3,4,5,1,,\n");
			//strMiddle.Format(_T(",,,,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,1,2,3,4,5,6,1,2,3,4,5,1,2,3,4,5,1,,\r\n"),
			strMiddle.Format(_T(",,,,,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,\
				%s,\
				%s,%s,%s,%s,%s,%s,\
				%s,%s,%s,%s,%s,\
				%s,%s,%s,%s,%s,\
				,,,\r\n"),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[0], ST_RECIPE_UNIT_TEST[15], L"")), //LCR 13
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[1], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[2], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[3], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[4], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[5], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[6], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[7], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[8], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[9], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[10], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[11], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[12], ST_RECIPE_UNIT_TEST[15], L"")),
				//(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[13], ST_RECIPE_UNIT_TEST[15], L"")), //LCR 14
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[14], ST_RECIPE_UNIT_TEST[15], L"")),	//OSC 1
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[15], ST_RECIPE_UNIT_TEST[15], L"")), //DCR 6
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[16], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[17], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[18], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[19], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[20], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[21], ST_RECIPE_UNIT_TEST[15], L"")), //HIPOT 5
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[22], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[23], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[24], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[25], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[26], ST_RECIPE_UNIT_TEST[15], L"")), //IR 5
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[27], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[28], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[29], ST_RECIPE_UNIT_TEST[15], L"")),
				(G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(ST_RECIPE_TEST_ITEM[30], ST_RECIPE_UNIT_TEST[15], L"")) //
				);
			memcpy(DataTitle, ConvertUnicodeToMultybyte(strMiddle), strMiddle.GetAllocLength() * 2);
			file.Write(DataTitle, strMiddle.GetAllocLength() * 2);
			
			/*strHeader.Format(L"MODEL:,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n", G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
			file.WriteString(strHeader);
			file.WriteString(L",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
			file.WriteString(L",TIME,MODEL,LOT NO.,SERIAL NO.,LCR(uH),,,,,,,,,,,,,OSC,DCR(mOhm),,,,,,HIPOT(mA),,,,,IR(MOhm),,,,,VI,RESULT,NG ITEM\n");
			file.WriteString(L",,,,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,1,2,3,4,5,6,1,2,3,4,5,1,2,3,4,5,1,,\n");*/
#else
			strHeader.Format(L"MODEL:,%s,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n", G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
			file.WriteString(strHeader);
			file.WriteString(L",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n");
			file.WriteString(L",TIME,MODEL,LCR(uH),,,,,,,,,,,,,DCR(mOhm),,,,,,VI,RESULT,NG ITEM\n");
			file.WriteString(L",,,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,1,,\n");
#endif
			//test result
			file.WriteString(strData);
			//char str[1024];
			//strcpy(str, ConvertUnicodeToMultybyte(strData));
			//LPCTSTR lp = strData;
			//file.WriteString(lp);
			G_MainWnd->m_ServerDlg.nDayReportCount++;
			file.SeekToEnd(); 
			file.Close(); 
		}else{
			ex.GetErrorMessage(sz, 256);
			//AddLog(3,_T( %s"),  sz);
		}

	}
	else
	{
		if(G_MainWnd->m_ServerDlg.nDayReportCount == 0)
		{
			//m_fnDayReportFileRead();
		}
		G_MainWnd->m_ServerDlg.nDayReportCount++;
		if(file.Open(strFileName, CFile::modeReadWrite, &ex)){
			file.SeekToEnd();
			file.WriteString(strData);
			file.Close(); 
		}else{
			ex.GetErrorMessage(sz, 256);
			G_AddLog(3,L" %s", sz);
		}
	}

}

void CDataHandling::m_fnDayReportFileRead()
{
	CStdioFile file;
	CFileException ex;

	CString strTempDay, strFileName, strLeftData;
	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();
	strTempDay = ctCurrentTime.Format(_T("%Y%m%d"));
	
	strFileName.Format(_T("D:\\nBizData2\\ResultData\\DayReport\\DayReportLog_%s.Csv"), strTempDay);
	//strFileName.Format(_T("D:\\%s.csv", strTempDay);
	
	CString strValue, strLine, strData, strInputValue;
	CStringArray strValueData;
	int nValue = 0;
	int nSumValue = 0;

	file.Open(strFileName, CFile::modeReadWrite, &ex); 
	
	GV_ITEM Item;	
	Item.nFormat =  DT_VCENTER;
	
	while(file.ReadString(strLine))
	{
		strData = strLine;
		bool a = true;
		while(a)
		{
			if(strData.Find(_T(",L")) != -1)
			{
				nValue = strData.Find(_T(",L"));
				strInputValue = strData.Left(nValue);
				strData = strData.Mid(nValue+1, strData.GetLength());
				strValueData.Add(strInputValue);
			}
			else
			{
				strValueData.Add(strData);
				a= false;
			}
				
		}
		G_MainWnd->m_ServerDlg.nDayReportCount++;
	
		GV_ITEM Item;	
		Item.nFormat =  DT_VCENTER;
	
		strValue = strValueData.GetAt(0).Right(8);
		Item.col = 1;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(2);
		Item.col = 2;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(4);
		Item.col = 3;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(6);
		Item.col = 4;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	
	
		strValue = strValueData.GetAt(8);
		Item.col = 5;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(10);
		Item.col = 6;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(12);
		Item.col = 7;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(14);
		Item.col = 8;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(16);
		Item.col = 9;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);	

		strValue = strValueData.GetAt(18);
		Item.col = 10;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);
	}

	file.Close(); 
	
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.AutoSize(GVS_BOTH);
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.Refresh();	
					
	int nSize = 0, nMax = 0, nMin = 0;
	int nGetCount =0;
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.GetScrollRange(SB_VERT, &nMin, &nMax);
	nSize = nMax / 5000;
	nGetCount = G_MainWnd->m_ServerDlg.nDayReportCount/30 +1;
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetScrollPos(SB_VERT, nSize*(G_MainWnd->m_ServerDlg.nDayReportCount) + 29 * nGetCount );
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.AutoSize(GVS_HEADER);
	G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetSelectedRange(G_MainWnd->m_ServerDlg.nDayReportCount, 1, G_MainWnd->m_ServerDlg.nDayReportCount, G_MainWnd->m_ServerDlg.m_ctrlGridResult.GetColumnCount() - 1);
	strValue.ReleaseBuffer();
	strValueData.RemoveAll();
}



int DeleteDirectory(const std::wstring &refcstrRootDirectory,
	bool              bDeleteSubdirectories = true)
{
	bool            bSubdirectory = false;       // Flag, indicating whether
	// subdirectories have been found
	HANDLE          hFile;                       // Handle to directory
	std::wstring     strFilePath;                 // Filepath
	std::wstring     strPattern;                  // Pattern
	WIN32_FIND_DATA FileInformation;             // File information

	strPattern = refcstrRootDirectory + _T("\\*.*");
	hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(FileInformation.cFileName[0] != '.')
			{
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + _T("\\") + FileInformation.cFileName;

				if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(bDeleteSubdirectories)
					{
						// Delete subdirectory
						int iRC = DeleteDirectory(strFilePath, bDeleteSubdirectories);
						if(iRC)
							return iRC;
					}
					else
						bSubdirectory = true;
				}
				else
				{
					// Set file attributes
					if(::SetFileAttributes(strFilePath.c_str(),
						FILE_ATTRIBUTE_NORMAL) == FALSE)
						return ::GetLastError();

					// Delete file
					if(::DeleteFile(strFilePath.c_str()) == FALSE)
						return ::GetLastError();
				}
			}
		} while(::FindNextFile(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if(dwError != ERROR_NO_MORE_FILES)
			return dwError;
		else
		{
			if(!bSubdirectory)
			{
				// Set directory attributes
				if(::SetFileAttributes(refcstrRootDirectory.c_str(),
					FILE_ATTRIBUTE_NORMAL) == FALSE)
					return ::GetLastError();

				// Delete directory
				if(::RemoveDirectory(refcstrRootDirectory.c_str()) == FALSE)
					return ::GetLastError();
			}
		}
	}

	return 0;
}


void CDataHandling::m_fnAutoDelete(CString strDeletePath, int nDay)
{
	CString strTempDay, strTempTime,  strTempTime2;
	CTime ctCurrentTime, ctDay;
	ctCurrentTime = CTime::GetCurrentTime();
	strTempTime = ctCurrentTime.Format(L"%Y%m%d%H%M%S");
	strTempDay = ctCurrentTime.Format(L"%Y%m%d");

	G_MainWnd->m_DataHandling.bAutoDeleteState = true;
	CString Command;
	CFileFind finderDay, finderTime;

	CString strFolderPathDay, strFolderPathTime;

	//strFolderPathDay.Format(L"%s\\*.*",strDeletePath);
	//strFolderPathTime.Format(L"%s%s\\*.*",strDeletePath, strTempDay);
	strFolderPathDay.Format(L"%s*.*", strDeletePath);
	strFolderPathTime.Format(L"%s%s\\*.*", strDeletePath, strTempDay);

	BOOL bWorking = finderDay.FindFile(strFolderPathDay);
	std::wstring s;
	int cnt = 0;
	while (bWorking)
	{
		bWorking = finderDay.FindNextFile();

		if (finderDay.IsDots())
			continue;

		if (finderDay.IsDirectory())
		{
			CString strPath1 = finderDay.GetFilePath();

			if(_wtoi(strTempDay) - _wtoi(strPath1.Right(8))  > nDay + 1)
			{
				s.clear() ;
				s.append((LPCTSTR)strPath1);
				DeleteDirectory(s);
				cnt++;
			}
			else if (_wtoi(strTempDay) - _wtoi(strPath1.Right(8)) >= nDay)
			{
				strPath1.Format(L"%s\\*.*", strPath1);
				BOOL bWorkingTime = finderTime.FindFile(strPath1);

				while (bWorkingTime)
				{
					bWorkingTime = finderTime.FindNextFile();

					if (finderTime.IsDots())
						continue;

					if (finderTime.IsDirectory())
					{
						CString strPath = finderTime.GetFilePath();
						CTime tmCreated;
						finderTime.GetCreationTime(tmCreated);
						strTempTime2 = tmCreated.Format("%Y%m%d%H%M%S");

						if(_wtoi(strTempTime.Right(8)) - _wtoi(strTempTime2.Right(8)) < 30000)
						{
							//Command.Format(_T("rmdir %s  /q /s", strPath);
							//system(Command); 
							s.clear() ;
							s.append((LPCTSTR)strPath);
							DeleteDirectory(s);
							cnt++;
							if(cnt > 10) break;
						}
					}
				}
				finderTime.Close();
			}
		}
	}
	finderDay.Close();
	//s = "";
	//s.shrink_to_fit();
	s.clear() ;
	G_MainWnd->m_DataHandling.bAutoDeleteState = false;
}


int CDataHandling::loadFlashData(void)
{
	BOOL bReturn = FALSE;
	CString strValueData;
	CString str;

	int i;

	for( i = 0; i< NO_FLASH_INTDATA; i++)
	{
			strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[i],L"0");
			m_FlashData.data[i] = _wtoi(strValueData);
	}
	for( i = 0; i< NO_FLASH_DBDATA; i++)
	{
		strValueData = m_fnReadIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[i],L"0");
		m_FlashData.dbdata[i] = _wtof(strValueData);
	}

	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

	return 0;
}

int CDataHandling::updateProductionData(int pass)
{
	//update
	m_FlashData.data[TEST_COUNT_ACCU]++;
	m_FlashData.data[COUNT_TOTAL]++;
	if(pass){ 
		m_FlashData.data[COUNT_OK]++;
	}else{
		m_FlashData.data[COUNT_NG]++;
	}
	m_FlashData.data[LOT_PERCENT] = static_cast<int>(100.0f*m_FlashData.data[COUNT_OK]/m_FlashData.data[LOT_TARGET]);

	BOOL bReturn = FALSE;
	CString str;

	str.Format(L"%d",m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	str.Format(L"%d",m_FlashData.data[COUNT_OK]);
	G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT6)->SetWindowText(str);

	// save
	int i;
	for( i = TEST_COUNT_ACCU; i<= COUNT_NG; i++)
	{
		str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
		m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[i],str); 
	}

	return 0;
}

//when it's a new day
void CDataHandling::m_fnMakeDirectoryNewDay()
{
	memset(m_chImageSavePath, 0x00, MAX_PATH);
	memset(m_chImageSaveNGPath, 0x00, MAX_PATH);

	CString strCreateDirectory, strCreateDay, CamDirectory, strNgCreateDay, strNgCreateDirectory, strAlignCreateDay, strAlignCreateDirectory;
	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();

	strCreateDay.Format(L"%s%s", DATA_IMAGE_FOLDER, ctCurrentTime.Format(L"%Y%m%d"));
	m_fnMakeDirectory(strCreateDay);
	strCreateDay.ReleaseBuffer();
	memcpy(m_chImageSavePath, strCreateDay, strCreateDay.GetLength()*sizeof(wchar_t));

	strNgCreateDay.Format(L"%s%s", DATA_IMAGE_NG_FOLDER, ctCurrentTime.Format(L"%Y%m%d"));
	m_fnMakeDirectory(strNgCreateDay);
	strNgCreateDay.ReleaseBuffer();
	memcpy(m_chImageSaveNGPath, strNgCreateDay, strNgCreateDay.GetLength()*sizeof(wchar_t));

}
