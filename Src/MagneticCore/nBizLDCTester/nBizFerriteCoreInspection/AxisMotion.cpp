#include "stdafx.h"
#include "AxisMotion.h"
#include "nBizLDCTester.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(AxisMotion, CDialogEx)

extern CMainWnd	*G_MainWnd;

#define	WM_CLOSE_DIALOG						(WM_USER + 3)

#define CMP_DIALOG							0
#define DIO_DIALOG								1
#define INTERPOLATION_DIALOG				2
#define INTERRUPT_DIALOG					3
#define MASTER_SLAVE_DIALOG				4

union {
	unsigned int mem;

	struct {
		USHORT EMG:1;
		USHORT ALM:1;
		USHORT P_LMT:1;
		USHORT N_LMT:1;
		USHORT ORG:1;
		USHORT DIR:1;
		USHORT H_OK:1;
		USHORT PCS:1;
		USHORT CRC:1;
		USHORT EZ:1;
		USHORT CLR:1;
		USHORT LAT:1;
		USHORT SD:1;
		USHORT INP:1;
		USHORT SON:1;
		USHORT RST:1;
		USHORT STA:1;
	}mem_bit;
}udtCardStatus, udtOldCardStatus, udtShuttleStatus, udtLCRAxisStatus[NUM_LCR_AXIS];

#pragma comment(lib,"C:\\Program Files\\Alpha Motion\\MotionComposer\\MApi(Library)\\Library\\pmiMApi.lib")

int exiAxisNo;


AxisMotion::AxisMotion(CWnd* pParent /*=NULL*/)
	: CDialogEx(AxisMotion::IDD, pParent)
{

}

AxisMotion::~AxisMotion(void)
{
	delete m_EdtCmdPos[8];
	delete m_EdtActPos[8];
	delete m_EdtVelocity[8];
	delete m_EdtCmdPos2[8];
	delete m_EdtActPos2[8];
	delete m_EdtVelocity2[8];
	memset(this,0x00, sizeof(LCR_AXIS_MSG));
	//delete ST_LCR_GRID_COL[12];
	//delete ST_LCR_GRID_IN_ROW[100];
	//memset(this,0x00, sizeof(LCR_AXIS_POSITION));
	//memset(this,0x00, sizeof(LCR_AXIS_POSITION));
	DestroyWindow();
}


void AxisMotion::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	for(int id=0; id<8; id++)
	{
		DDX_Control(pDX, IDC_EDT_CURR_VEL0 + id, m_EdtVelocity[id]);
		DDX_Control(pDX, IDC_EDT_CURR_ACT_POS0 + id, m_EdtActPos[id]);
		DDX_Control(pDX, IDC_EDT_CURR_CMD_POS0 + id, m_EdtCmdPos[id]);
	}
	for(int id=0; id<4; id++)
	{
		DDX_Control(pDX, IDC_EDT_CURR_VEL9 + id, m_EdtVelocity2[id]);
		DDX_Control(pDX, IDC_EDT_CURR_ACT_POS9 + id, m_EdtActPos2[id]);
		DDX_Control(pDX, IDC_EDT_CURR_CMD_POS9 + id, m_EdtCmdPos2[id]);
	}

	DDX_Control(pDX, IDC_BTM_RST_STATUS, m_BtmRstStatus);
	DDX_Control(pDX, IDC_BTM_SON_STATUS, m_BtmSonStatus);
	DDX_Control(pDX, IDC_BTM_CCLR_STATUS, m_BtmCclrStatus);
	DDX_Control(pDX, IDC_BTM_LLMT_STATUS, m_BtmNLmtStatus);
	DDX_Control(pDX, IDC_BTM_PLMT_STATUS, m_BtmPLmtStatus);
	DDX_Control(pDX, IDC_BTM_ALM_STATUS, m_BtmAlmStatus);
	DDX_Control(pDX, IDC_BTM_INP_STATUS, m_BtmInpStatus);
	DDX_Control(pDX, IDC_BTM_EMG_STATUS, m_BtmEmgStatus);
	DDX_Control(pDX, IDC_BTM_EZ_STATUS, m_BtmEzStatus);
	DDX_Control(pDX, IDC_BTM_ORG_STATUS, m_BtmOrgStatus);

	DDX_Control(pDX, IDC_CB_HOME_MODE, m_CbHomeMode);
	DDX_Control(pDX, IDC_CB_ENCODER_MODE, m_CbSelectEncoderMode);
	DDX_Control(pDX, IDC_CB_PULSE_MODE, m_CbSelectPulseMode);
	//DDX_Control(pDX, IDC_CB_RANGE, m_CbSelectRange);
	DDX_Control(pDX, IDC_CB_AXIS_SELECT, m_CbAxisSelect);
	DDX_Control(pDX, IDC_CB_SPEED_MODE, m_CbSpeedMode);
	DDX_Control(pDX, IDC_GRID_LCR_IN, m_GridLCRAxis);
	DDX_Control(pDX, IDC_EDT_CURR_CMD_POS8, m_EdtCmdPos8);
	DDX_Control(pDX, IDC_EDT_CURR_ACT_POS8, m_EdtActPos8);
	DDX_Control(pDX, IDC_EDT_CURR_VEL8, m_EdtVelocity8);
	DDX_Control(pDX, IDC_TXT_SHTL_FWD, m_staticShuttleForward);
	DDX_Control(pDX, IDC_TXT_SHTL_REV, m_staticShuttleReturn);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_FWD, m_btnShuttleFwd);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_RET, m_btnShuttleRev);
	for(int id=0; id< 10; id++)
	{
		DDX_Control(pDX, IDC_BTN_LCR_AXIS1 + id, m_btnLCRAxisGoToReady[id]);
		DDX_Control(pDX, IDC_BTN_LCR_AXIS11 + id, m_btnLCRAxisHome[id]);
		DDX_Control(pDX, IDC_BTN_LCR_AXIS21 + id, m_btnLCRAxisAlarmReset[id]);
		DDX_Control(pDX, IDC_BTN_LCR_AXIS31 + id, m_btnLCRAxisJOGP[id]);
		DDX_Control(pDX, IDC_BTN_LCR_AXIS41 + id, m_btnLCRAxisJOGN[id]);
	}
	DDX_Control(pDX, IDC_BTN_SHUTTLE_HOME2, m_btnShuttleAlarmReset);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_HOME, m_btnShuttleHome);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_FORWARD_POS, m_btnShuttleSetFWD);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_RETURN_POS, m_btnShuttleSetRET);
	DDX_Control(pDX, IDC_BUTTON6, m_btnLCRAxisEStop);
	DDX_Control(pDX, IDC_BUTTON7, m_btnShuttleEStop);
	DDX_Control(pDX, IDC_BTN_AXIS_READY_SET, m_btnLCRAxisReadySet);

	DDX_Control(pDX, IDC_BTN_AXIS_BUSY_CLEAR, m_btnMotorBusy);

	DDX_Control(pDX, IDC_BTN_IN_SET, m_btnLCRPosSet);
	DDX_Control(pDX, IDC_BTN_LCR_AXIS_SAVE, m_btnLCRPosSave);
	DDX_Control(pDX, IDC_BTN_AXIS_MSG_BUSY_CLEAR, m_btn_AutoAxisBusy);
	DDX_Control(pDX, IDC_BTN_LCR_HOME, m_btnLCRHomeAll);
	DDX_Control(pDX, IDC_BTN_LCR_READY, m_btnLCRReadyAll);
	DDX_Control(pDX, IDC_BTN_SHUTTLE_INIT2, m_btnShuttleInit);
//AXIS HOME ENABLE LAMP ADD _160111_KYS
	DDX_Control(pDX, IDC_BTN_SHORT_HOME, m_btnShortHomeEnable);
	DDX_Control(pDX, IDC_BTN_PROBE_HOME, m_btnProbeHomeEnable);
}


BEGIN_MESSAGE_MAP(AxisMotion, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_CHK_ORG_LOGIC, OnChkOrgLogic)
	ON_BN_CLICKED(IDC_CHK_EZ_LOGIC, OnChkEzLogic)
	ON_BN_CLICKED(IDC_CHK_EMG_LOGIC, OnChkEmgLogic)
	ON_BN_CLICKED(IDC_CHK_INP_LOGIC, OnChkInpLogic)
	ON_BN_CLICKED(IDC_CHK_ALM_LOGIC, OnChkAlmLogic)
	ON_BN_CLICKED(IDC_CHK_LMT_LOGIC, OnChkLmtLogic)
	ON_BN_CLICKED(IDC_CHK_INP_ENABLE, OnChkInpEnable)
	ON_BN_CLICKED(IDC_CHK_ENCODER_DIR, OnChkEncoderDir)
	ON_BN_CLICKED(IDC_CHK_HOME_DIR, OnChkHomeDir)
	ON_BN_CLICKED(IDC_BTN_ESTOP, OnBtnEstop)
	ON_BN_CLICKED(IDC_BTN_SSTOP, OnBtnSstop)
	//ON_BN_CLICKED(IDC_BTN_PJOG, OnBtnPjog)
	//ON_BN_CLICKED(IDC_BTN_NJOG, OnBtnNjog)
	ON_BN_CLICKED(IDC_BTN_INCMOVE, OnBtnIncmove)
	ON_BN_CLICKED(IDC_BTN_ABSMOVE, OnBtnAbsmove)
	ON_BN_CLICKED(IDC_BTN_HOMEMOVE, OnBtnHomemove)	
	ON_BN_CLICKED(IDC_BTN_MULTI_INCMOVE, OnBtnMultiIncmove)
	ON_BN_CLICKED(IDC_BTN_MULTI_ABSMOVE, OnBtnMultiAbsmove)
	ON_BN_CLICKED(IDC_BTN_CLEAR_POS, OnBtnClearPos)
	ON_BN_CLICKED(IDC_BTN_MULTI_ESTOP, OnBtnMultiEstop)
	ON_BN_CLICKED(IDC_BTN_MULTI_SSTOP, OnBtnMultiSstop)
	ON_BN_CLICKED(IDC_BTN_OVERRIDE_SPEED, OnBtnOverrideSpeed)
	ON_BN_CLICKED(IDC_BTN_OVERRIDE_ABS, OnBtnOverrideAbs)
	ON_BN_CLICKED(IDC_BTN_CCLR, OnBtnCclr)
	ON_BN_CLICKED(IDC_BTN_SVON_ONOFF, OnBtnSvonOnoff)
	ON_BN_CLICKED(IDC_BTN_RST_ONOFF, OnBtnRstOnoff)
	ON_CBN_SELCHANGE(IDC_CB_PULSE_MODE, OnSelchangeCbPulseMode)
	ON_CBN_SELCHANGE(IDC_CB_ENCODER_MODE, OnSelchangeCbEncoderMode)
	ON_CBN_SELCHANGE(IDC_CB_HOME_MODE, OnSelchangeCbHomeMode)
	ON_CBN_SELCHANGE(IDC_CB_AXIS_SELECT, OnSelchangeCbAxisSelect)	
	//ON_MESSAGE(WM_CLOSE_DIALOG, OnReleaseDlg)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_CB_SPEED_MODE, &AxisMotion::OnCbnSelchangeCbSpeedMode)
	//ON_BN_CLICKED(IDC_BTN_DIO_DLG, OnBtnDioDlg)
	//ON_BN_CLICKED(IDC_BTN_CMP_DLG, &AxisMotion::OnBnClickedBtnCmpDlg)
	//ON_BN_CLICKED(IDC_BTN_INTERPOLATION_DLG, &AxisMotion::OnBnClickedBtnInterpolationDlg)
	//ON_BN_CLICKED(IDC_BTN_INTERRUPT_DLG, &AxisMotion::OnBnClickedBtnInterruptDlg)
	//ON_BN_CLICKED(IDC_BTN_MASTER_SLAVE_DLG, &AxisMotion::OnBnClickedBtnMasterSlaveDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_LCR_IN, OnGridDblClick)
	ON_NOTIFY(NM_CLICK, IDC_GRID_LCR_IN, OnGridClick)
	ON_BN_CLICKED(IDC_BTN_IN_SET, &AxisMotion::OnBnClickedBtnInSet)
	ON_BN_CLICKED(IDC_BTN_LCR_AXIS_SAVE, &AxisMotion::OnBnClickedBtnLcrAxisSave)
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_FORWARD_POS, &AxisMotion::OnBnClickedBtnShuttleForwardPos)
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_RETURN_POS, &AxisMotion::OnBnClickedBtnShuttleReturnPos)
	ON_BN_CLICKED(IDC_BTN_AXIS_READY_SET, &AxisMotion::OnBnClickedBtnAxisReadySet)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_FWD, &AxisMotion::OnBnClickedBtnShuttleFwd)
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_RET, &AxisMotion::OnBnClickedBtnShuttleRet)
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_HOME, &AxisMotion::OnBnClickedBtnShuttleHome)
	ON_COMMAND_RANGE(IDC_BTN_LCR_AXIS1, IDC_BTN_LCR_AXIS10, &AxisMotion::OnBnClickedBtnLcrAxisGoToReadyPosition)
	ON_COMMAND_RANGE(IDC_BTN_LCR_AXIS11, IDC_BTN_LCR_AXIS20, &AxisMotion::OnBnClickedBtnLcrAxisGoToHome)
	ON_COMMAND_RANGE(IDC_BTN_LCR_AXIS21, IDC_BTN_LCR_AXIS30, &AxisMotion::OnBnClickedBtnLcrAxisAlarmReset)

	ON_BN_CLICKED(IDC_BTN_SHUTTLE_HOME2, &AxisMotion::OnBnClickedBtnShuttleAlarmReset)
	ON_BN_CLICKED(IDC_BUTTON6, &AxisMotion::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &AxisMotion::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BTN_AXIS_BUSY_CLEAR, &AxisMotion::OnBnClickedBtnAxisBusyClear)

	ON_BN_CLICKED(IDC_BTN_AXIS_MSG_BUSY_CLEAR, &AxisMotion::OnBnClickedBtnAxisMsgBusyClear)
	ON_BN_CLICKED(IDC_BTN_LCR_READY, &AxisMotion::OnBnClickedBtnLcrReady)
	ON_BN_CLICKED(IDC_BTN_LCR_HOME, &AxisMotion::OnBnClickedBtnLcrHome)
	ON_BN_CLICKED(IDC_BTN_SHUTTLE_INIT2, &AxisMotion::OnBnClickedBtnShuttleInit2)
END_MESSAGE_MAP()


void AxisMotion::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	int nResult;
	int Timeout = 0;
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	CString cell;
	CString str;

	//if((pItem->iRow-1) == 0)return;
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	m_btnMotorBusy.SetColorChange(RED, BLACK);
	if(pItem->iColumn == 12)
	{
		cell.Format(L"Step: %d Manual Move", pItem->iRow-1);
		GetDlgItem(IDC_LCR_IN_STATUS)->SetWindowText(cell);

		if(pItem->iRow-1 >= G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step -1 &&
			pItem->iRow-1 <= G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step +1 )
		{
			nResult = AfxMessageBox(L"수동으로 이동하시겠습니까?", MB_YESNO|MB_ICONQUESTION);
			switch( nResult ) {
			case IDYES:
				//move
				motorBusy = TRUE;
				m_btnMotorBusy.SetColorChange(RED, BLACK);
				G_AddLog(3,L"Manual Axis Step Move [%d]", pItem->iRow-1);
				nResult = stepMoveLCR(pItem->iRow-1);
				if(nResult){
					do{
						Sleep(100);
						nResult = stepMoveCheckLCR();
						//TIME OUT ERROR
						if(Timeout++ > 200){
							motorBusy = FALSE;
							m_btnMotorBusy.SetColorChange(WHITE, BLACK);
							str.Format(L"Manual Axis Step Move [%d] Timeout Error", pItem->iRow-1);
							G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
							G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
							//G_SystemModeData.unSystemError = SYSTEM_ERROR;							
							break;
						}
					}while(nResult != emSTAND);
					if(nResult == emSTAND){
						G_MainWnd->m_InspectThread.setLCRAxisStep(KEY_LCR_AXIS_STEP, pItem->iRow-1);
						motorBusy = FALSE;
						m_btnMotorBusy.SetColorChange(WHITE, BLACK);
						AfxMessageBox(L"STEP MOVE 완료", MB_ICONASTERISK);
					}
				}else{
				//Error
					motorBusy = FALSE;
					m_btnMotorBusy.SetColorChange(WHITE, BLACK);
					str.Format(L"Manual Axis Step Move [%d] Failed", pItem->iRow-1);
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
					G_SystemModeData.unSystemError = SYSTEM_ERROR;
					AfxMessageBox(str, MB_ICONHAND);
				}
				break;
			case IDNO:

				break;
			case IDCANCEL:

				break;
			};
		}else{
			AfxMessageBox(L"현재 스텝의 이전, 다음 스텝으로만 이동할 수 있습니다", MB_ICONHAND);
		}
	}//if(pItem->iColumn == 10)
	
}


void AxisMotion::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//find selected cell
	CString cell;
	if(pItem->iColumn == 9)
	{
		//cell.Format(L"Double Click Step: %d Manual Move", pItem->iRow-1);
		//GetDlgItem(IDC_LCR_IN_STATUS)->SetWindowText(cell);
	}else
	{
		InStep = pItem->iRow -1;
		InAxis = pItem->iColumn - 1;
		cell.Format(L"Step: %d, Axis: %d", InStep, InAxis);
		GetDlgItem(IDC_LCR_IN_STATUS)->SetWindowText(cell);
	}
}


BOOL AxisMotion::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//////////////////////////////////////////////////////////////////////////
	// Motion Control Board를 Load 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iConNum = 0;
	CString strMsg = _T("");	

	exiAxisNo = 0;

	//iRet = pmiSysLoad( TMC_FALSE, &iConNum );
	iRet = pmiSysLoad( TMC_TRUE, &iConNum );

	if(iRet >= 0)
	{
		// 파라미터 파일은 MotionComposer.ini 파일에 Parameter_File = 경로를 쫓아 간다.
		//iRet = pmiConParamLoad(NULL);
		char szCmeFilePath[MAX_PATH];
		sprintf_s(szCmeFilePath,"%s", TMC_FILE_NAME);
		if(pmiConParamLoad(szCmeFilePath) != TMC_RV_OK){
			AfxMessageBox(L"파라미터 파일이 존재하지 않습니다.");
			G_AddLog(3,L"pmiConParamLoad failed");
		}
	}else
	{		
		// Motion Conrol Board가 정상적으로 Load 되지 않았을때
		// 메세지 창에 에러 Load 에러 값을 표시 합니다.
		strMsg.Format(_T("Fail!!!\nError No. = %d"), iRet);
		AfxMessageBox(strMsg);
		//MessageBox(strMsg);

		return FALSE;
	}

	InitDlgItem();	// 다이알로그에 있는 각 아이템의 초기값을 표시합니다.

	SetTimer(100, 100, NULL);
	SetTimer(200, 100, NULL);
	//SetTimer(300, 4000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void AxisMotion::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


void AxisMotion::m_fnInit(void)
{
	int iRet = 0;
	CString strMsg;
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	ShowWindow(SW_HIDE);
	initGridLCRIN();
	GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	//m_staticShuttleForward.SetFont2(20, 8, 7);	
	strMsg.Format(L"%.f",G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD]);
	m_staticShuttleForward.SetText(strMsg, BLACK, RGB(230,230,230));
	//m_staticShuttleReturn.SetFont2(20, 8, 7);	
	strMsg.Format(L"%.f",G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN]);
	m_staticShuttleReturn.SetText(strMsg, BLACK, RGB(230,230,230));
	
	motorBusy = FALSE;
	m_btnMotorBusy.SetColorChange(WHITE, BLACK);
	//emg
	int iIsCheck;
	iRet = pmiGnSetEmgLevel( CARD_NO, emLOGIC_B );
	iRet = pmiGnGetEmgLevel( CARD_NO, &iIsCheck );
	iRet = pmiGnSetEmgLevel( CARD_NO2, emLOGIC_A );
	iRet = pmiGnGetEmgLevel( CARD_NO2, &iIsCheck );

	//LCR Axis ready position
	for(int i = 0; i < NUM_LCR_AXIS; i++)
	{
		strMsg.Format(L"%.f",G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + i]);
		GetDlgItem(IDC_EDT_AXIS0_READY + i)->SetWindowText(strMsg);

		iRet = initAxis(i);
		if( iRet == TMC_RV_OK) {
			G_AddLog(3,L"LCR Axis %d Motor on", i);
		}else{
			G_AddLog(3,L"LCR Axis %d Motor on failed", i);
			//Error
		}
	}

	//Shuttle
		iRet = initAxis(NUM_LCR_AXIS);
		if( iRet == TMC_RV_OK) {
			G_AddLog(3,L"Shuttle Servo on");
		}else{
			G_AddLog(3,L"Shuttle Servo on failed");
			//Error
		}

		//memset(axisData, 0x00, sizeof(LCR_AXIS_POSITION));
		//memset(axisMsgRsp, 0x00, sizeof(LCR_AXIS_MSG));
}

void AxisMotion::m_fnDeInit(void)
{
		int iRet = 0;
		G_AddLog(3, L"int iRet = 0");
	// TODO: Add your message handler code here and/or call default
	//////////////////////////////////////////////////////////////////////////
	// Load 된 Motion Control Board를 Unload 합니다.	                    //
	//////////////////////////////////////////////////////////////////////////

    KillTimer(200);
	G_AddLog(3, L"KillTimer(200)");
	KillTimer(100);
	G_AddLog(3, L"KillTimer(100)");
	Sleep(1000);
	iRet = pmiSysUnload();		//1024_kys
	if (iRet != 1){
		G_AddLog(3, L"pmiSysUnload Abnormal 종료");
	}else
		G_AddLog(3, L"pmiSysUnload Normal 종료");
}

void AxisMotion::OnClose() 
{
	CDialog::OnClose();
}

BOOL AxisMotion::ReturnMessage(int iRet)
{
	CString strBuff = _T("");

	if( iRet != TMC_RV_OK )
	{
		//strBuff.Format(_T("Error Return Code : %d"), iRet);
		//AfxMessageBox(strBuff);
		return FALSE;
	}

	return TRUE;
}

void AxisMotion::InitDlgItem()
{
	int iRet = 0;
	CString strMsg;
	CString strText, strVal, strVerH, strVerM, strVerL;
	int iDllVer = 0;
	int iModel = 0;
	int iDllVer2 = 0;
	int iModel2 = 0;

	m_nMotEvtCnt = 0;
	m_iAxisNum = 0;
	m_iDiNum = 0;
	m_iDoNum = 0;
	m_iAxisNum2 = 0;
	m_iDiNum2 = 0;
	m_iDoNum2 = 0;

	m_BitMapLedG.LoadBitmap(IDB_BTM_LED_GREEN);
	m_BitMapLedR.LoadBitmap(IDB_BTM_LED_RED);
	m_BitMapLedW.LoadBitmap(IDB_BTM_LED_WHITE);

	m_BtmOrgStatus.SetBitmap(m_BitMapLedW);
	m_BtmEzStatus.SetBitmap(m_BitMapLedW);
	m_BtmEmgStatus.SetBitmap(m_BitMapLedW);
	m_BtmInpStatus.SetBitmap(m_BitMapLedW);
	m_BtmAlmStatus.SetBitmap(m_BitMapLedW);
	m_BtmPLmtStatus.SetBitmap(m_BitMapLedW);
	m_BtmNLmtStatus.SetBitmap(m_BitMapLedW);
	m_BtmCclrStatus.SetBitmap(m_BitMapLedW);
	m_BtmSonStatus.SetBitmap(m_BitMapLedW);
	m_BtmRstStatus.SetBitmap(m_BitMapLedW);

	////////////////////////////////////////////////////////////////////////
	//CARD NO. 1

	iRet = pmiGnGetAxesNum( CARD_NO, &m_iAxisNum );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiGnGetDioNum( CARD_NO, &m_iDiNum, &m_iDoNum );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 모델명
	iRet = pmiConGetModel(CARD_NO, &iModel);
	if( ReturnMessage(iRet) == FALSE )
		return;
	switch (iModel)
	{
	case TMC_BA800P:

	case TMC_BA600P:

	case TMC_BA400P:

	case TMC_BA200P:

		strMsg.Format(_T("TMC-BA%d%02dP"), m_iAxisNum, m_iDiNum);
		SetDlgItemText(IDC_EDT_MODEL, strMsg);		

		break;

	case TMC_BB160P:

	case TMC_BB120P:

	case TMC_BB800P:

	case TMC_BB400P:

		strMsg.Format(_T("TMC-BB%d00P"), m_iAxisNum);
		SetDlgItemText(IDC_EDT_MODEL, strMsg);

		break;
	}
	
	// Dll Version//
	iRet = pmiConGetMApiVersion( CARD_NO, &iDllVer );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strText.Format(_T("%ld"), iDllVer);
	strVerH = strText.Left( 1 );
	strVerM = strText.Mid( 5, 1 );
	strVerL = strText.Right( 1 );
	strText = strVerH + '.' + strVerM + '.' + strVerL;
	SetDlgItemText(IDC_EDT_DLL_VER, strText);

	// 축 개수
	strMsg.Format(_T("%d"), m_iAxisNum);
	SetDlgItemText(IDC_EDT_AXIS_NUM, strMsg);

	// 범용 Digital Out 개수
	strMsg.Format(_T("%02d"), m_iDoNum);
	SetDlgItemText(IDC_EDT_DOUT_NUM, strMsg);

	// 범용 Digital In 개수
	strMsg.Format(_T("%02d"), m_iDiNum);
	SetDlgItemText(IDC_EDT_DIN_NUM, strMsg);

	/////////////////////////////////////////////////////////
	//CARD NO. 2

	iRet = pmiGnGetAxesNum( CARD_NO2, &m_iAxisNum2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiGnGetDioNum( CARD_NO2, &m_iDiNum2, &m_iDoNum2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 모델명
	iRet = pmiConGetModel(CARD_NO2, &iModel2);
	if( ReturnMessage(iRet) == FALSE )
		return;
	switch (iModel)
	{
	case TMC_BA800P:

	case TMC_BA600P:

	case TMC_BA400P:

	case TMC_BA200P:

		strMsg.Format(_T("TMC-BA%d%02dP"), m_iAxisNum2, m_iDiNum2);
		SetDlgItemText(IDC_EDT_MODEL2, strMsg);		

		break;

	case TMC_BB160P:

	case TMC_BB120P:

	case TMC_BB800P:

	case TMC_BB400P:

		strMsg.Format(_T("TMC-BB%d00P"), m_iAxisNum2);
		SetDlgItemText(IDC_EDT_MODEL2, strMsg);

		break;
	}

	// Dll Version//
	iRet = pmiConGetMApiVersion( CARD_NO2, &iDllVer2 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strText.Format(_T("%ld"), iDllVer);
	strVerH = strText.Left( 1 );
	strVerM = strText.Mid( 5, 1 );
	strVerL = strText.Right( 1 );
	strText = strVerH + '.' + strVerM + '.' + strVerL;
	SetDlgItemText(IDC_EDT_DLL_VER2, strText);

	// 축 개수
	strMsg.Format(_T("%d"), m_iAxisNum2);
	SetDlgItemText(IDC_EDT_AXIS_NUM2, strMsg);

	// 범용 Digital Out 개수
	strMsg.Format(_T("%02d"), m_iDoNum2);
	SetDlgItemText(IDC_EDT_DOUT_NUM2, strMsg);

	// 범용 Digital In 개수
	strMsg.Format(_T("%02d"), m_iDiNum2);
	SetDlgItemText(IDC_EDT_DIN_NUM2, strMsg); 
	/////////////////////////////////////////////////////////

	// int Axis;
	//for(Axis=0; Axis < m_iAxisNum; Axis++)
	//{
	//	strMsg.Format(_T("[카드 1] %d번 축"), Axis);
	//	m_CbAxisSelect.AddString(strMsg);
	//}
	//for(Axis=0; Axis < m_iAxisNum2; Axis++)
	//{
	//	if(Axis != AXIS_SHUTTLE){
	//		strMsg.Format(_T("[카드 2] %d번 축"), Axis);
	//		m_CbAxisSelect.AddString(strMsg);
	//	}else{
	//		strMsg.Format(_T("[카드 2] %d SHUTTLE"), Axis);
	//		m_CbAxisSelect.AddString(strMsg);
	//	}
	//}
	//strMsg.Format(_T("[카드 1] %d번 축"), Axis);

	m_CbAxisSelect.AddString(L"PROBE X"); //CARD NO:0
	m_CbAxisSelect.AddString(L"PROBE Y");
	m_CbAxisSelect.AddString(L"PROBE Z");
	m_CbAxisSelect.AddString(L"PROBE T");
	m_CbAxisSelect.AddString(L"PROBE L");

	m_CbAxisSelect.AddString(L"SHORT X");
	m_CbAxisSelect.AddString(L"SHORT Y");
	m_CbAxisSelect.AddString(L"SHORT Z");
	m_CbAxisSelect.AddString(L"SHORT T"); //CARD NO2:0
	m_CbAxisSelect.AddString(L"SHORT L"); //1

	m_CbAxisSelect.AddString(L"SHUTTLE"); //2


	m_CbAxisSelect.SetCurSel(0);

	m_CbSelectPulseMode.AddString(_T("0 : PLS NEG/DIR HI"));
	m_CbSelectPulseMode.AddString(_T("1 : PLS POS/DIR HI"));
	m_CbSelectPulseMode.AddString(_T("2 : PLS NEG/DIR LOW"));
	m_CbSelectPulseMode.AddString(_T("3 : PLS POS/DIR LOW"));
	m_CbSelectPulseMode.AddString(_T("4 : CW/CCW NEG"));
	m_CbSelectPulseMode.AddString(_T("5 : CW/CCW POS")); //
	m_CbSelectPulseMode.AddString(_T("6 : CW/CCW NEG INV"));
	m_CbSelectPulseMode.AddString(_T("7 : CW/CCW POS INV"));
	m_CbSelectPulseMode.AddString(_T("8 : Two Phase(Out Lead)"));
	m_CbSelectPulseMode.AddString(_T("9 : Two Phase(Dir Lead)"));

	m_CbSelectEncoderMode.AddString(_T("0 : X1(EA/EB)"));
	m_CbSelectEncoderMode.AddString(_T("1 : X2(EA/EB)"));
	m_CbSelectEncoderMode.AddString(_T("2 : X4(EA/EB)"));//
	m_CbSelectEncoderMode.AddString(_T("3 : Up/Down"));

	m_CbHomeMode.AddString(_T("0 : ORG ON -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("1 : ORG ON -> Stop -> Go back(Rev Spd) -> ORG OFF -> Go forward(Rev Spd) -> ORG ON -> Stop")); //
	m_CbHomeMode.AddString(_T("2 : ORG ON -> Slow down(Low Spd) -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("3 : ORG ON -> EZ signal -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("4 : ORG ON -> Stop -> Go back(Rev Spd) -> ORG OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("5 : ORG ON -> Stop -> Go back(High Spd) -> ORG OFF -> EZ signal -> Slow down -> Stop"));
	m_CbHomeMode.AddString(_T("6 : EL ON -> Stop -> Go back(Rev Spd) -> EL OFF -> Stop"));
	m_CbHomeMode.AddString(_T("7 : EL ON -> Stop -> Go back(Rev Spd) -> EL OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("8 : EL ON -> Stop -> Go back(High Spd) -> EL OFF -> Stop on EZ signal"));
	m_CbHomeMode.AddString(_T("9 : ORG ON -> Slow down -> Stop -> Go back -> Stop at beginning edge of ORG"));
	m_CbHomeMode.AddString(_T("10 : ORG ON -> EZ signal -> Slow down -> Stop -> Go back -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("11 : ORG ON -> Slow down -> Stop -> Go back (High Spd) -> ORG OFF -> EZ signal -> Slow down -> Stop -> Go forward(High Spd) -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("12 : EL ON -> Stop -> Go back (High Spd) -> EL OFF -> EZ signal -> Slow down -> Stop -> Go forward(High Spd) -> Stop at beginning edge of EZ"));
	m_CbHomeMode.AddString(_T("13 : EZ signal -> Slow down -> Stop"));

	m_CbSpeedMode.AddString(_T("0 : Constant"));
	m_CbSpeedMode.AddString(_T("1 : Trapezoidal"));
	m_CbSpeedMode.AddString(_T("2 : S-Curve"));

	SetDlgItemText(IDC_EDT_INIT_SPEED, _T("1000"));
	SetDlgItemText(IDC_EDT_DRV_SPEED, _T("10000"));
	SetDlgItemText(IDC_EDT_ACC_TIME, _T("100"));
	SetDlgItemText(IDC_EDT_DEC_TIME, _T("100"));

	SetDlgItemText(IDC_EDT_POSITION1, _T("1000"));
	SetDlgItemText(IDC_EDT_POSITION2, _T("2000"));

	SetDlgItemText(IDC_EDT_HOME_OFFSET, _T("0"));

	SetDlgItemText(IDC_EDT_OVERRIDE_SPEEDDATA, _T("50000"));
	SetDlgItemText(IDC_EDT_OVERRIDE_POSITIONDATA, _T("5000000"));

	udtCardStatus.mem = 0;
	udtOldCardStatus.mem = 0;

	RefreshParameter();
}


void AxisMotion::OnTimer(UINT_PTR nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if(nIDEvent == 100)
	{
		//m_csAxisPos.Lock();
		
		PosAndVel_UIUpDate();
		//m_csAxisPos.Unlock();
	}else if(nIDEvent == 200)
	{
		CardStatusDisplay();
		//160114_kys
		//m_fnDisplayLCRAxisStatus();
	}else 	if(nIDEvent == 300)
	{
		sysInitShuttle(); //One time Shuttle Sys Init with IO
		KillTimer(300);
	}
	CDialog::OnTimer(nIDEvent);	
}

void AxisMotion::CardStatusDisplay()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Card Satus를 읽어서 Display 합니다.                      //
	// 상태값이 변화가 있으면 변화된 값만 Display 합니다.                   //
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int axisCard;

	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
		if(iAxisNo > 4)return;
	}
	exiAxisNo = iAxisNo;

	iRet = pmiAxGetMechanical( axisCard, iAxisNo, &udtCardStatus.mem);
	if( ReturnMessage(iRet) == FALSE )
		return;

	if(udtCardStatus.mem_bit.ORG != udtOldCardStatus.mem_bit.ORG)	// 상태값이 변화를 확인합니다.
	{
		if(udtCardStatus.mem_bit.ORG == 1)
			m_BtmOrgStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmOrgStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.EZ != udtOldCardStatus.mem_bit.EZ)
	{
		if(udtCardStatus.mem_bit.EZ == 1)
			m_BtmEzStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmEzStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.EMG != udtOldCardStatus.mem_bit.EMG)
	{
		if(udtCardStatus.mem_bit.EMG == 1)
			m_BtmEmgStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmEmgStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.INP != udtOldCardStatus.mem_bit.INP)
	{
		if(udtCardStatus.mem_bit.INP == 1)
			m_BtmInpStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmInpStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.ALM != udtOldCardStatus.mem_bit.ALM)
	{
		if(udtCardStatus.mem_bit.ALM == 1)
			m_BtmAlmStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmAlmStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.P_LMT != udtOldCardStatus.mem_bit.P_LMT)
	{
		if(udtCardStatus.mem_bit.P_LMT == 1)
			m_BtmPLmtStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmPLmtStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.N_LMT != udtOldCardStatus.mem_bit.N_LMT)
	{
		if(udtCardStatus.mem_bit.N_LMT == 1)
			m_BtmNLmtStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmNLmtStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.CLR != udtOldCardStatus.mem_bit.CLR)
	{
		if(udtCardStatus.mem_bit.CLR == 1)
			m_BtmCclrStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmCclrStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.SON != udtOldCardStatus.mem_bit.SON)
	{
		if(udtCardStatus.mem_bit.SON == 1)
			m_BtmSonStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmSonStatus.SetBitmap(m_BitMapLedW);
	}
	if(udtCardStatus.mem_bit.RST != udtOldCardStatus.mem_bit.RST)
	{
		if(udtCardStatus.mem_bit.RST == 1)
			m_BtmRstStatus.SetBitmap(m_BitMapLedG);
		else
			m_BtmRstStatus.SetBitmap(m_BitMapLedW);
	}

	udtOldCardStatus.mem = udtCardStatus.mem;	// 이전 상태값을 업데이트 합니다.
}

void AxisMotion::PosAndVel()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Command, Encoder 위치 및 Command 속도를 Display 합니다.  //
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	double dbCmdPos = 0;
	double dbActPos = 0;
	double dbCmdVel = 0;
	int iAxisNo;

	//Card no. 1
	for(iAxisNo=0; iAxisNo < m_iAxisNum; iAxisNo++)
	{
		//CMD_POS_UPDATE
		iRet = pmiAxGetCmdPos( CARD_NO, iAxisNo, &dbCmdPos );
				if( ReturnMessage(iRet) == FALSE )
				return;
		m_dAxisCmdPos[iAxisNo] = dbCmdPos;

		//ACT_POS_UPDATE
		iRet = pmiAxGetActPos( CARD_NO, iAxisNo, &dbActPos );
				if( ReturnMessage(iRet) == FALSE )
				return;
		m_dAxisActPos[iAxisNo] = dbActPos;

		//Cmd_Velocity_UPDATE
		iRet = pmiAxGetCmdVel( CARD_NO, iAxisNo, &dbCmdVel );
				if( ReturnMessage(iRet) == FALSE )
				return;
		m_dAxisCmdVel[iAxisNo] = dbCmdVel;
	}	
			
	//Card No. 2
	for(iAxisNo=0; iAxisNo < m_iAxisNum2; iAxisNo++)
	{
		//CMD_POS_UPDATE
		iRet = pmiAxGetCmdPos( CARD_NO2, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
			return;
		
		m_dAxisCmdPos[8+iAxisNo] = dbCmdPos;

		//ACT_POS_UPDATE
		iRet = pmiAxGetActPos( CARD_NO2, iAxisNo, &dbActPos );
				if( ReturnMessage(iRet) == FALSE )
			return;
		m_dAxisActPos[8+iAxisNo] = dbActPos;

		//Cmd_Velocity_UPDATE
		iRet = pmiAxGetCmdVel( CARD_NO2, iAxisNo, &dbCmdVel );
				if( ReturnMessage(iRet) == FALSE )
			return;
		m_dAxisCmdVel[8+iAxisNo] = dbCmdVel;
	}
}
void AxisMotion::PosAndVel_UIUpDate()
{
	int iAxisNo;
	CString strVal_CmdPos;
	CString strVal_ActPos;
	CString strVal_CmdVel;
	int iRet = 0;

	for(iAxisNo=0; iAxisNo < USED_NUM_AXIS; iAxisNo++)
	{
		strVal_CmdPos.Format(_T("%.lf"),m_dAxisCmdPos[iAxisNo]);
		strVal_ActPos.Format(_T("%.lf"),m_dAxisActPos[iAxisNo]);
		strVal_CmdVel.Format(_T("%.lf"),m_dAxisCmdVel[iAxisNo]);

		if(iAxisNo < m_iAxisNum)
			{
				m_EdtCmdPos[iAxisNo].SetWindowText(strVal_CmdPos);
				m_EdtActPos[iAxisNo].SetWindowText(strVal_ActPos);
				m_EdtVelocity[iAxisNo].SetWindowText(strVal_CmdVel);
			}
				else
					{
						m_EdtCmdPos2[(iAxisNo)-(m_iAxisNum)].SetWindowText(strVal_CmdPos);
						m_EdtActPos2[(iAxisNo)-(m_iAxisNum)].SetWindowText(strVal_ActPos);
						m_EdtVelocity2[(iAxisNo)-(m_iAxisNum)].SetWindowText(strVal_CmdVel);
						if(iAxisNo==10)
							{
								m_EdtCmdPos8.SetWindowText(strVal_CmdPos);
								m_EdtActPos8.SetWindowText(strVal_ActPos);
								m_EdtVelocity8.SetWindowText(strVal_CmdVel);
							}
					}
	}
}

void AxisMotion::OnChkOrgLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ORG 논리를 변경 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;

	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ORG_LOGIC);
	iRet = pmiAxSetOrgLevel( axisCard, iAxisNo, iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetOrgLevel( axisCard, iAxisNo, &iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ORG_LOGIC, iIsCheck);
}

void AxisMotion::OnChkEzLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 EZ 논리를 변경 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_EZ_LOGIC);
	iRet = pmiAxSetEzLevel( axisCard, iAxisNo, iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEzLevel( axisCard, iAxisNo, &iIsCheck);
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EZ_LOGIC, iIsCheck);
}

void AxisMotion::OnChkEmgLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// EMG 논리를 변경 합니다.												//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;	
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_EMG_LOGIC);
	iRet = pmiGnSetEmgLevel( axisCard, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.	
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EMG_LOGIC, iIsCheck);

}

void AxisMotion::OnChkInpLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 INP 논리와 사용 유무를 변경 합니다.						//
	// Enable이 1로 설정되면 서보에서 Inpotion 신호가 발생해야 동작이		//
	// 완료된 것입니다.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck2 = IsDlgButtonChecked(IDC_CHK_INP_ENABLE);
	iIsCheck1 = IsDlgButtonChecked(IDC_CHK_INP_LOGIC);

	iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck2);
}

void AxisMotion::OnChkAlmLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ALM 논리와 사용 유무를 변경 합니다.						//	
	// Enable이 1로 설정되면 서보에서 알람 신호 발생 시 즉시 정지 합니다.	//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ALM_LOGIC);
	iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ALM_LOGIC, iIsCheck);	
}

void AxisMotion::OnChkLmtLogic() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 리미트 신호 감지 시 정지 방법을 설정 합니다.				//
	// 선택한 축의 +LMT, -LMT 논리를 변경 합니다.							//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_LMT_LOGIC);
	iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_LMT_LOGIC, iIsCheck);	
}

void AxisMotion::OnChkInpEnable() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 INP 사용 유무와 논리를 변경 합니다.						//
	// Enable이 1로 설정되면 서보에서 Inpotion 신호가 발생해야 동작이		//
	// 완료된 것입니다.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck2 = IsDlgButtonChecked(IDC_CHK_INP_ENABLE);
	iIsCheck1 = IsDlgButtonChecked(IDC_CHK_INP_LOGIC);

	iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 사용 유무와 논리가 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck2);
}

void AxisMotion::OnChkEncoderDir() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Encoder 값 방향을 설정 합니다							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_ENCODER_DIR);

	iRet = pmiAxSetEncDir( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEncDir( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	CheckDlgButton(IDC_CHK_ENCODER_DIR, iIsCheck);	
}

void AxisMotion::OnChkHomeDir() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Home Move 방향을 설정 합니다.							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iIsCheck = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iIsCheck = IsDlgButtonChecked(IDC_CHK_HOME_DIR);

	iRet = pmiAxHomeSetDir( axisCard, iAxisNo, iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxHomeGetDir( axisCard, iAxisNo, &iIsCheck );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_HOME_DIR, iIsCheck);	
}

void AxisMotion::OnSelchangeCbAxisSelect() 
{
	// TODO: Add your control notification handler code here
	RefreshParameter();

}

void AxisMotion::OnSelchangeCbPulseMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Pulse Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iMode = m_CbSelectPulseMode.GetCurSel();

	iRet = pmiAxSetPulseType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetPulseType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	m_CbSelectPulseMode.SetCurSel(iMode);	
}

void AxisMotion::OnSelchangeCbEncoderMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Encoder Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iMode = m_CbSelectEncoderMode.GetCurSel();

	iRet = pmiAxSetEncType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxGetEncType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectEncoderMode.SetCurSel(iMode);	
}

void AxisMotion::OnSelchangeCbHomeMode() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// Home Mode를 변경 합니다.												//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iMode = m_CbHomeMode.GetCurSel();

	iRet = pmiAxHomeSetType( axisCard, iAxisNo, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 정상적으로 적용되었는지 확인 합니다.
	iRet = pmiAxHomeGetType( axisCard, iAxisNo, &iMode );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbHomeMode.SetCurSel(iMode);	
}

void AxisMotion::SetPosMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Inc Move, Abs Move의 시작 속도, 운전속도, 가속도, 감속도를 설정		//
	// 합니다.																//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");	

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DEC_TIME, strGetValue);
	dbDecTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::SetJogMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Jog Move의 시작 속도, 운전속도, 가속도, 감속도를 설정				//
	// 합니다.																//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");	

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_JOG_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitJogVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetJogVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::SetJogMoveSpeed2(int iAxisNo)
{
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;

	dbStartSpeed = 100;
	dbAccTime = 100;
	iMode = 2;

	int axisCard;
	int iAxisNoInCard;
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}
	switch(iAxisNo){
	case UW_PROBE_X:  dbDriveSpeed = 5000; break;
	case  UW_PROBE_Y: dbDriveSpeed = 8000; break;
	case  UW_PROBE_Z: dbDriveSpeed = 20000; break;
	case  UW_PROBE_T: dbDriveSpeed = 800; break;
	case  UW_PROBE_L: dbDriveSpeed = 30000; break;
	case UW_SHORT_X: dbDriveSpeed = 5000; break;
	case  UW_SHORT_Y: dbDriveSpeed = 8000; break;
	case  UW_SHORT_Z: dbDriveSpeed = 20000; break;
	case  UW_SHORT_T: dbDriveSpeed = 800; break;
	case  UW_SHORT_L: dbDriveSpeed = 30000; break;
	}

	iRet = pmiAxSetInitJogVel( axisCard, iAxisNoInCard, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetJogVelProf( axisCard, iAxisNoInCard, iMode, dbDriveSpeed, dbAccTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::SetHomeMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Home Move의 시작 속도, 운전속도, 가속도, 감속도를 설정합니다. 		//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbRevSpeed = 0;
	double dbAccTime = 0;
	CString strGetValue = _T("");

	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_REV_SPEED, strGetValue);
	dbRevSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxHomeSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxHomeSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbRevSpeed, dbAccTime );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnEstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 즉시 정지 시킵니다.										//
	// 정지 명령어를 사용한 후 반드시 Done() 함수를 사용하여 정지 여부를	//
	// 확인 하십시요.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;

	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	iRet = pmiAxEStop( axisCard, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnSstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 감속 정지 시킵니다.										//
	// 정지 명령어를 사용한 후 반드시 Done() 함수를 사용하여 정지 여부를	//
	// 확인 하십시요.														//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	CString strGetValue = _T("");

	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	iRet = pmiAxStop( CARD_NO, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

//void AxisMotion::OnBtnPjog() 
//{
	////////////////////////////////////////////////////////////////////////////
	//// 선택한 축을 CCW 방향으로 Jog Move를 시작합니다.						//
	//// 멈추기 위해서는 Stop(Sudden_Stop, Decel_Stop) 명령어를 사용 하십시요.//
	////////////////////////////////////////////////////////////////////////////
	//int iRet = 0;
	//int iAxisNo = 0;

	//iAxisNo = m_CbAxisSelect.GetCurSel();
	////change card no for shuttle
	//if(iAxisNo < m_iAxisNum){//
	//	axisCard = CARD_NO;
	//}else{
	//	axisCard = CARD_NO2;
	//	iAxisNo = iAxisNo - m_iAxisNum;
	//}
	//SetJogMoveSpeed(iAxisNo);

	//iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_P );
	//if( ReturnMessage(iRet) == FALSE )
	//	return;
//}
//
//void AxisMotion::OnBtnNjog() 
//{
	////////////////////////////////////////////////////////////////////////////
	//// 선택한 축을 CW 방향으로 Jog Move를 시작합니다.						//
	//// 멈추기 위해서는 Stop(Sudden_Stop, Decel_Stop) 명령어를 사용 하십시요.//
	////////////////////////////////////////////////////////////////////////////
	//int iRet = 0;
	//int iAxisNo = 0;

	//iAxisNo = m_CbAxisSelect.GetCurSel();
	////change card no for shuttle
	//if(iAxisNo < m_iAxisNum){//
	//	axisCard = CARD_NO;
	//}else{
	//	axisCard = CARD_NO2;
	//	iAxisNo = iAxisNo - m_iAxisNum;
	//}
	//SetJogMoveSpeed(iAxisNo);

	//iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_N );
	//if( ReturnMessage(iRet) == FALSE )
	//	return;
//}

void AxisMotion::OnBtnIncmove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 입력한 위치값 만큼 이송합니다.(Inc Move)					//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbPosition = _tstof(strGetValue);

	SetPosMoveSpeed(iAxisNo);

	iRet = pmiAxPosMove( axisCard, iAxisNo, TMC_INC, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnAbsmove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 입력한 위치값까지 이송합니다.(abs Move)					//
	// 현재 위치가 입력한 위치값과 같으면 움직이지 않습니다.				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbPosition = _tstof(strGetValue);

	SetPosMoveSpeed(iAxisNo);

	iRet = pmiAxPosMove( axisCard, iAxisNo, TMC_ABS, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnHomemove() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 원점 복귀 동작을 수행 합니다.(Home Move)				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbOffset = 0;
	CString strGetValue = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_HOME_OFFSET, strGetValue);
	dbOffset = _tstof(strGetValue);

	SetHomeMoveSpeed(iAxisNo);

	// Home Move 완료 후 Offset 위치를 설정 합니다.
	iRet = pmiAxHomeSetShiftDist( axisCard, iAxisNo, dbOffset );
	if( ReturnMessage(iRet) == FALSE )
		return;

	// Home Move를 시작 합니다.
	iRet = pmiAxHomeMove( axisCard, iAxisNo );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::RefreshParameter()
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축에 설정되어 있는 Parameter 값들을 확인합니다.				//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int iIsCheck1 = 0;
	int iIsCheck2 = 0;
	double dbBuff1 = 0;
	double dbBuff2 = 0;
	double dbBuff3 = 0;
	CString strBuff = _T("");
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}


	// ORG 논리값을 확인 합니다.
	iRet = pmiAxGetOrgLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ORG_LOGIC, iIsCheck1);

	// EZ 논리값을 확인 합니다.
	iRet = pmiAxGetEzLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EZ_LOGIC, iIsCheck1);

	// EMG 논리값을 확인 합니다.
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_EMG_LOGIC, iIsCheck1);

	// INP 논리값과 사용 유무를 확인 합니다.
	iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck2 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_INP_ENABLE, iIsCheck1);
	CheckDlgButton(IDC_CHK_INP_LOGIC, iIsCheck2);

	// ALM 논리값과 사용 유무를 확인 합니다.
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ALM_LOGIC, iIsCheck1);	

	// Hardware Limit 논리값을 확인 합니다.
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_LMT_LOGIC, iIsCheck1);

	// Encoder 방향을 확인합니다.
	iRet = pmiAxGetEncDir( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_ENCODER_DIR, iIsCheck1);	

	// Home Move 방향을 확인합니다.
	iRet = pmiAxHomeGetDir( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	CheckDlgButton(IDC_CHK_HOME_DIR, iIsCheck1);	

	// Pulse Mode를 확인합니다.	
	iRet = pmiAxGetPulseType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectPulseMode.SetCurSel(iIsCheck1);

	// Encoder Mode를 확인합니다.
	iRet = pmiAxGetEncType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSelectEncoderMode.SetCurSel(iIsCheck1);	

	// Home Mode를 확인합니다.	
	iRet = pmiAxHomeGetType( axisCard, iAxisNo, &iIsCheck1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbHomeMode.SetCurSel(iIsCheck1);

	// Max Speed를 확인합니다.
	iRet = pmiAxGetMaxVel( axisCard, iAxisNo, &dbBuff1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.lf"), dbBuff1 );
	SetDlgItemText(IDC_EDT_MAX_SPEED, strBuff);

	// Init Speed를 확인합니다.
	iRet = pmiAxGetInitVel( axisCard, iAxisNo, &dbBuff1 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.lf"), dbBuff1 );
	SetDlgItemText(IDC_EDT_INIT_SPEED, strBuff);

	// Jog Speed를 확인합니다.
	iRet = pmiAxGetJogVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.lf"), dbBuff1 );
	SetDlgItemText(IDC_EDT_JOG_SPEED, strBuff);

	// Speed Mode를 확인합니다.
	iRet = pmiAxGetVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2, &dbBuff3 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	m_CbSpeedMode.SetCurSel(iIsCheck1);

	strBuff.Format( _T("%.lf"), dbBuff1 );
	SetDlgItemText(IDC_EDT_DRV_SPEED, strBuff);

	strBuff.Format( _T("%.lf"), dbBuff2 );
	SetDlgItemText(IDC_EDT_ACC_TIME, strBuff);

	strBuff.Format( _T("%.lf"), dbBuff3 );
	SetDlgItemText(IDC_EDT_DEC_TIME, strBuff);

	// Home Speed Mode를 확인합니다.
	iRet = pmiAxHomeGetVelProf( axisCard, iAxisNo, &iIsCheck1, &dbBuff1, &dbBuff2, &dbBuff3 );
	if( ReturnMessage(iRet) == FALSE )
		return;
	strBuff.Format( _T("%.lf"), dbBuff2 );
	SetDlgItemText(IDC_EDT_REV_SPEED, strBuff);
}

void AxisMotion::OnBtnMultiIncmove() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 다축 상대 위치값 이송을 동시에 시작하는 버튼입니다.					//
	// +방향으로 입력한 값 만큼 이송 합니다.								//
	// 0번 축과 1번축을 동시에 시작합니다.									//
	//////////////////////////////////////////////////////////////////////////

	return;

	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[2] = {0};
	double dbarPositionList[2] = {0};
	CString strGetValue = _T("");

	iAxisNum = 2;

	iarAxisList[0] = 0;	// 첫번째 축을 0번축으로 설정합니다.
	iarAxisList[1] = 1;	// 두번태 축을 1번축으로 설정합니다.

	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbarPositionList[0] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.
	GetDlgItemText(IDC_EDT_POSITION2, strGetValue);
	dbarPositionList[1] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.

	SetPosMoveSpeed(iarAxisList[0]);	// 첫번째 축의 이송 속도 프로파일를 설정합니다.
	SetPosMoveSpeed(iarAxisList[1]);	// 두번째 축의 이송 속도 프로파일을 설정합니다.

	// 동시에 이송시작합니다.
	iRet = pmiMxPosMove( CARD_NO, iAxisNum, TMC_INC, iarAxisList, dbarPositionList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiAbsmove() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 다축 절대 위치값 이송을 동시에 시작하는 버튼입니다.					//
	// +방향으로 입력한 값 까지 이송 합니다.								//
	// 현재 CommandPos값이 입혁한 값과 동일하면 더이상 움직이지 않습니다.	//
	// 이 버튼에서는 0번 축과 1번축을 동시에 시작합니다.					//
	//////////////////////////////////////////////////////////////////////////

	return;

	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[2] = {0};
	double dbarPositionList[2] = {0};
	CString strGetValue = _T("");

	iAxisNum = 2;

	iarAxisList[0] = 0;	// 첫번째 축을 0번축으로 설정합니다.
	iarAxisList[1] = 1;	// 두번태 축을 1번축으로 설정합니다.

	GetDlgItemText(IDC_EDT_POSITION1, strGetValue);
	dbarPositionList[0] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.
	GetDlgItemText(IDC_EDT_POSITION2, strGetValue);
	dbarPositionList[1] = _tstof(strGetValue);	// 첫번째 축에 적용할 이송 거리를 설정합니다.

	SetPosMoveSpeed(iarAxisList[0]);	// 첫번째 축의 이송 속도 프로파일를 설정합니다.
	SetPosMoveSpeed(iarAxisList[1]);	// 두번째 축의 이송 속도 프로파일을 설정합니다.

	// 동시에 이송시작합니다.
	iRet = pmiMxPosMove( CARD_NO, iAxisNum, TMC_ABS, iarAxisList, dbarPositionList );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiEstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축들을 동시에 감속 정지 시킵니다.								//
	// 정지 명령어를 사용한 후 반드시 Done() 또는 Multi_Done() 함수를		//
	// 사용하여 정지 여부를 확인 하십시요.									//
	// 이 버튼에서는 모든 축을 급정지 시킵니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[16] = {0};

	iAxisNum = m_iAxisNum;

	for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iarAxisList[AxisNo] = AxisNo;
	}

	iRet = pmiMxEStop( CARD_NO, iAxisNum, iarAxisList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnMultiSstop() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축들을 동시에 감속정지 시킵니다.								//
	// 정지 명령어를 사용한 후 반드시 Done() 또는 Multi_Done() 함수를		//
	// 사용하여 정지 여부를 확인 하십시요.									//
	// 이 버튼에서는 모든 축을 감속정지 시킵니다.							//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[16] = {0};

	iAxisNum = m_iAxisNum;

	for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iarAxisList[AxisNo] = AxisNo;
	}

	iRet = pmiMxStop( CARD_NO, iAxisNum, iarAxisList );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnClearPos() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Command 위치 값과 Actual(Encoder) 위치값을 설정합니다.	//
	// 이 버튼은 0으로 설정 합니다.											//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int axisCard;
	//0909_KYS
	iAxisNo = m_CbAxisSelect.GetCurSel();
			//change card no for shuttle
			if(iAxisNo < m_iAxisNum)
			{
				axisCard = CARD_NO;
			}
			else
			{
				axisCard = CARD_NO2;
				iAxisNo = iAxisNo - m_iAxisNum;
			}
#ifdef _DEBUG
		G_AddLog(3,L"OnBtnClearPos; called  %s, %d ", __FILE__, __LINE__);
#endif
     iRet = pmiAxSetCmdPos( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
		iRet = pmiAxSetActPos( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
		iRet = pmiAxSetPosError( axisCard, iAxisNo, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return;
}

void AxisMotion::OnBtnOverrideSpeed() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축이 이송 중일 때 운전 속도를 변경합니다.						//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbDriveSpeed = 0;	
	CString strValue = _T("");	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_OVERRIDE_SPEEDDATA, strValue);
	dbDriveSpeed = _tstof(strValue);

	iRet = pmiAxModifyVel( axisCard, iAxisNo, dbDriveSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnOverrideAbs() 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축이 Position Move 중일 때 이송 위치를 변경합니다.			//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	double dbPosition = 0;	
	CString strValue = _T("");	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_OVERRIDE_POSITIONDATA, strValue);
	dbPosition = _tstof(strValue);

	iRet = pmiAxModifyPos( axisCard, iAxisNo, dbPosition );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnCclr() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 편차 카운터를 초기화 시킵니다.							//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	// 선택한 축의 편차 카운터 클리어 신호의 출력 시간을 설정 합니다.
	iRet = pmiAxSetServoCrcTime( axisCard, iAxisNo, 6);
	if( ReturnMessage(iRet) == FALSE )
		return;

	// 선택한 축의 편차 카운터 클리어 신호를 출력 합니다.
	// 설정한 시간 동안 On된 후 Off 됩니다.
	iRet = pmiAxSetServoCrcOn( axisCard, iAxisNo );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnSvonOnoff() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo On/Off 신호를 On/Off 합니다.						//
	// 현재 상태가 On이면 Off, Off면 On 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iOnOff = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iRet = pmiAxGetServoOn( axisCard, iAxisNo, &iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iOnOff = (iOnOff == 0) ? 1:0;
	iRet = pmiAxSetServoOn( axisCard, iAxisNo, iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnBtnRstOnoff() 
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo Alarm Reset 신호를 On/Off 합니다.					//
	// 현재 상태가 On이면 Off, Off면 On 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iOnOff = 0;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	iRet = pmiAxGetServoReset( axisCard, iAxisNo, &iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
	iOnOff = (iOnOff == 0) ? 1:0;
	iRet = pmiAxSetServoReset( axisCard, iAxisNo, iOnOff );
	if( ReturnMessage(iRet) == FALSE )
		return;
}

void AxisMotion::OnCbnSelchangeCbSpeedMode()
{
	// TODO: Add your control notification handler code here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Speed Mode를 변경 합니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	int iAxisNo = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue;	
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}
	GetDlgItemText(IDC_EDT_INIT_SPEED, strGetValue);
	dbStartSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DRV_SPEED, strGetValue);
	dbDriveSpeed = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_ACC_TIME, strGetValue);
	dbAccTime = _tstof(strGetValue);

	GetDlgItemText(IDC_EDT_DEC_TIME, strGetValue);
	dbDecTime = _tstof(strGetValue);

	iMode = m_CbSpeedMode.GetCurSel();

	iRet = pmiAxSetInitVel( axisCard, iAxisNo, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return;

	iRet = pmiAxSetVelProf( axisCard, iAxisNo, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return;
}

//
//void AxisMotion::OnBtnDioDlg() 
//{
//	// TODO: Add your control notification handler code here
//	//////////////////////////////////////////////////////////////////////////
//	// Digital I/O를 컨트롤하는 창을 엽니다.								//	
//	//////////////////////////////////////////////////////////////////////////
//
//	if (m_pDInDOutDlg != NULL)
//	{ 
//		m_pDInDOutDlg->SetFocus();
//		return;
//	}
//
//	m_pDInDOutDlg = new CDInDOutDlg;
//
//	m_pDInDOutDlg->Create(IDD_DIO_DIALOG, this);
//	m_pDInDOutDlg->ShowWindow(SW_SHOW);
//}
//
//void AxisMotion::OnBnClickedBtnCmpDlg()
//{
//	// TODO: Add your control notification handler code here
//	//////////////////////////////////////////////////////////////////////////
//	// CMP를 컨트롤하는 창을 엽니다.								//	
//	//////////////////////////////////////////////////////////////////////////
//
//	if (m_pCmpDlg != NULL)
//	{ 
//		m_pCmpDlg->SetFocus();
//		return;
//	}
//
//	m_pCmpDlg = new CCmpDlg;
//
//	m_pCmpDlg->Create(IDD_CMP_DIALOG, this);
//	m_pCmpDlg->ShowWindow(SW_SHOW);
//}
//
//void AxisMotion::OnBnClickedBtnInterpolationDlg()
//{
//	// TODO: Add your control notification handler code here
//	//////////////////////////////////////////////////////////////////////////
//	// 보간을 컨트롤하는 창을 엽니다.								//	
//	//////////////////////////////////////////////////////////////////////////
//
//	if (m_pInterPolationDlg != NULL)
//	{ 
//		m_pInterPolationDlg->SetFocus();
//		return;
//	}
//
//	m_pInterPolationDlg = new CInterPolationDlg;
//
//	m_pInterPolationDlg->Create(IDD_INTERPOLATION_DIALOG, this);
//	m_pInterPolationDlg->ShowWindow(SW_SHOW);
//}
//
//void AxisMotion::OnBnClickedBtnInterruptDlg()
//{
//	// TODO: Add your control notification handler code here
//	//////////////////////////////////////////////////////////////////////////
//	// 인터럽트를 컨트롤하는 창을 엽니다.								//	
//	//////////////////////////////////////////////////////////////////////////
//
//	if (m_pInterruptDlg != NULL)
//	{ 
//		m_pInterruptDlg->SetFocus();
//		return;
//	}
//
//	m_pInterruptDlg = new CInterruptDlg;
//
//	m_pInterruptDlg->Create(IDD_INTERRUPT_DIALOG, this);
//	m_pInterruptDlg->ShowWindow(SW_SHOW);
//}
//
//void AxisMotion::OnBnClickedBtnMasterSlaveDlg()
//{
//	// TODO: Add your control notification handler code here
//	//////////////////////////////////////////////////////////////////////////
//	// 인터럽트를 컨트롤하는 창을 엽니다.								//	
//	//////////////////////////////////////////////////////////////////////////
//
//	if (m_pMasterSlaveDlg != NULL)
//	{ 
//		m_pMasterSlaveDlg->SetFocus();
//		return;
//	}
//
//	m_pMasterSlaveDlg = new CMasterSlaveDlg;
//
//	m_pMasterSlaveDlg->Create(IDD_MASTER_SLAVE_DIALOG, this);
//	m_pMasterSlaveDlg->ShowWindow(SW_SHOW);
//}
//
//LRESULT AxisMotion::OnReleaseDlg(WPARAM wParam, LPARAM lParam)
//{
//	switch(wParam)		
//	{
//	case DIO_DIALOG:
//
//		if (m_pDInDOutDlg != NULL)
//		{
//			delete m_pDInDOutDlg;
//			m_pDInDOutDlg = NULL;
//		}
//
//		break;
//
//	case CMP_DIALOG:
//
//		if (m_pCmpDlg != NULL)
//		{
//			delete m_pCmpDlg;
//			m_pCmpDlg = NULL;
//		}
//
//		break;
//
//	case INTERPOLATION_DIALOG:
//
//		if (m_pInterPolationDlg != NULL)
//		{
//			delete m_pInterPolationDlg;
//			m_pInterPolationDlg = NULL;
//		}
//
//		break;
//
//	case INTERRUPT_DIALOG:
//
//		if (m_pInterruptDlg != NULL)
//		{
//			delete m_pInterruptDlg;
//			m_pInterruptDlg = NULL;
//		}
//
//		break;
//
//	case MASTER_SLAVE_DIALOG:
//
//		if (m_pMasterSlaveDlg != NULL)
//		{
//			delete m_pMasterSlaveDlg;
//			m_pMasterSlaveDlg = NULL;
//		}
//
//		break;
//
//	default:
//		break;
//	}
//
//	return TRUE;
//}



int AxisMotion::initGridLCRIN(void)
{
	try {
		m_GridLCRAxis.SetRowCount(NUM_STEP + 1);
		m_GridLCRAxis.SetColumnCount(NUM_AXIS_GRID_COL + 2);
		m_GridLCRAxis.SetFixedRowCount(1);
		m_GridLCRAxis.SetFixedColumnCount(1);
		m_GridLCRAxis.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridLCRAxis.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridLCRAxis.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col != 0)
				{
					//Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
					Item.strText.Format(L"  %s  ", ST_LCR_GRID_COL[col -1]);
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						//Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
						Item.strText.Format(L"STEP %d", row -1);					
					}
				}
				else if(col == (NUM_AXIS_GRID_COL + 1))
				{
					Item.nFormat = GVIS_READONLY|DT_END_ELLIPSIS|DT_NOPREFIX;
					m_GridLCRAxis.GetCell(row, col)->SetBackClr(RGB(220,220,220));
					//Item.strText.Format(L"%d", row);	
				}
			}
			m_GridLCRAxis.SetItem(&Item);
		}
	}
	m_GridLCRAxis.AutoSize(GVS_BOTH);
	m_GridLCRAxis.SetTextColor(RGB(0, 0, 105));
	m_GridLCRAxis.AutoSizeColumns();
	//m_GridLCRAxis.SetEditable(FALSE);
	m_GridLCRAxis.SetEditable(TRUE);
	//m_GridLCRAxis.ExpandToFit(true);
	m_GridLCRAxis.Refresh();

	return 0;
}



//LCR IN Position Set
void AxisMotion::OnBnClickedBtnInSet()
{
	// 
	int iRet = 0;
	//int iAxisNum = 0;
	//double dbCmdPos = 0;
	double dbActPos = 0;
	//double dbVelocity = 0;
	CString strVal;
	int iAxisNo = 0;
	int iAxisNoCB = 0;
	int axisCard;
	//iAxisNoCB = GetDlgItemInt(IDC_CB_AXIS_SELECT);
	iAxisNoCB = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNoCB < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNoCB - m_iAxisNum;
	}

	if(axisCard == CARD_NO2 && iAxisNo == AXIS_SHUTTLE){
		AfxMessageBox(L"LCR Axis 포지션만 할 수 있습니다", MB_ICONHAND);
		return;
	}
	iRet = pmiAxGetCmdPos( axisCard, iAxisNo, &dbActPos );
	if( ReturnMessage(iRet) == FALSE )
		return;
	if(iAxisNoCB == InAxis)
	{
		strVal.Format(_T("%.lf"), dbActPos);
		if(InStep != 0)
			m_GridLCRAxis.GetCell(InStep+1, InAxis+1)->SetText(strVal);
	}
	else
	{
		//
	}
	m_GridLCRAxis.Refresh();
}



int AxisMotion::saveLCRAxisData(CString recipe)
{
	//find recipe
	CString strRecipe, strRecipeName, strRecipeFullPath;
	bool filefound = false;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".lax") //
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);

			if(strRecipeName == recipe)
			{
				strRecipeFullPath = (LPCTSTR)finder.GetFilePath();
				m_iniLCRAxisData.SetFileName(strRecipeFullPath);
				filefound = true;
				break;
			}
		}//

	}

	//if not make new recipe
	if(!filefound)
	{
		strRecipeFullPath = RECIPE_FOLDER  + recipe + L".lax";
		m_iniLCRAxisData.SetFileName(strRecipeFullPath);
	}

	//save recipe data from grid
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;

	// ....................
	GetDlgItem(IDC_EDIT_IN_STEP)->GetWindowText(strValueData);
	bReturn = m_iniLCRAxisData.WriteProfileString(SECTION_AXIS_DATA, KEY_IN_STEP, strValueData);
	
	for(j=0; j< NUM_STEP ; j++)
	{
		for(i=0; i< NUM_AXIS_GRID_COL ; i++)
		{
			{
				strValueData = m_GridLCRAxis.GetCell(j+1,i+1)->GetText();
				bReturn = m_iniLCRAxisData.WriteProfileString(ST_LCR_GRID_IN_ROW[j], ST_LCR_GRID_COL[i], strValueData);
			}

		}
	}

	fillGridLCRAxis(recipe);
	m_GridLCRAxis.Refresh();

	//
	if(recipe.Compare(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel) ==0)
	applyAxisData(recipe);

	return 0;
}

//save LCR Axis position data
void AxisMotion::OnBnClickedBtnLcrAxisSave()
{
	saveLCRAxisData(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	return;
}

//
int AxisMotion::fillGridLCRAxis(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".lax") //
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniLCRAxisData.SetFileName(finder.GetFilePath());
				//load recipe data in GridModeRecipe
				strValueData = m_fnReadIniFile( SECTION_AXIS_DATA, KEY_IN_STEP,L"");
				GetDlgItem(IDC_EDIT_IN_STEP)->SetWindowText(strValueData);
				for(j=0; j<NUM_STEP ; j++)
				{
					for(i=0; i< NUM_AXIS_GRID_COL ; i++)
					{
						strValueData = m_fnReadIniFile( ST_LCR_GRID_IN_ROW[j], ST_LCR_GRID_COL[i],L"");
						m_GridLCRAxis.GetCell(j+1, i+1)->SetText(strValueData);
					}
				}//for(j=0; j<NUM_STEP ; j++)
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}
	m_GridLCRAxis.Refresh();
	return 0;
}


CString AxisMotion::m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"";
	if(default != 0)
	{
		lpDefault = default;
	}

	bReturn = m_iniLCRAxisData.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";
		//G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}


int AxisMotion::applyAxisData(CString recipe)
{
	CString strRecipe, strRecipeName;
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;


	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".lax") //
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniLCRAxisData.SetFileName(finder.GetFilePath());
				//load recipe data in axisData struct
				strValueData = m_fnReadIniFile( SECTION_AXIS_DATA, KEY_IN_STEP,L"");
				axisData.steps = static_cast<int>(_wtof(strValueData));
				for(j=0; j<NUM_STEP ; j++)
				{
					for(i=0; i< NUM_AXIS_GRID_COL ; i++)
					{
						strValueData = m_fnReadIniFile( ST_LCR_GRID_IN_ROW[j], ST_LCR_GRID_COL[i],L"");
						axisData.absPos[j][i]= _wtof(strValueData);
					}
				}//for(j=0; j<NUM_STEP ; j++)
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}

	return 0;
}


int AxisMotion::homeLCRAxis(void)
{
	return 0;
}


int AxisMotion::homeShuttle(void)
{
	return 0;
}


void AxisMotion::OnBnClickedBtnShuttleForwardPos()
{
	// 
	int iRet = 0;
	//int iAxisNum = 0;
	//double dbCmdPos = 0;
	double dbActPos = 0;
	//double dbVelocity = 0;
	CString strVal;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	 if((axisCard == CARD_NO2) & (iAxisNo == AXIS_SHUTTLE))
	 {
		iRet = pmiAxGetActPos( CARD_NO2, AXIS_SHUTTLE, &dbActPos );
	            if( ReturnMessage(iRet) == FALSE )
		          { 
					  return;
					  //ERROR 처리
				   }
				else
				{
				      strVal.Format(_T("%.lf"), dbActPos);
	                  m_staticShuttleForward.SetText(strVal);
	                  //str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
	                  G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD] = (dbActPos);
	                  G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[SHUTTLE_FORWARD], strVal);
				}
     }

	
	else
	 {
		AfxMessageBox(L"Shuttle Axis 포지션만 할 수 있습니다", MB_ICONHAND);
		return;
	 }

}


void AxisMotion::OnBnClickedBtnShuttleReturnPos()
{
	// 
	int iRet = 0;
	//int iAxisNum = 0;
	//double dbCmdPos = 0;
	double dbActPos = 0;
	//double dbVelocity = 0;
	CString strVal;
	int iAxisNo = 0;
	int axisCard;
	iAxisNo = m_CbAxisSelect.GetCurSel();
	//change card no for shuttle
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
	}else{
		axisCard = CARD_NO2;
		iAxisNo = iAxisNo - m_iAxisNum;
	}

	
	
	
	if((axisCard == CARD_NO2) & (iAxisNo == AXIS_SHUTTLE))
	 {
		iRet = pmiAxGetActPos( CARD_NO2, AXIS_SHUTTLE, &dbActPos );
	            if( ReturnMessage(iRet) == FALSE )
		          { 
					  return;
					  //ERROR 처리
				   }
				else
				{
				      strVal.Format(_T("%.lf"), dbActPos);
	                  m_staticShuttleReturn.SetText(strVal);
	                  //str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[i]);
	                  G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN] = (dbActPos);
	                  G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[SHUTTLE_RETURN], strVal);
				}
     }

	
	else
	 {
		AfxMessageBox(L"Shuttle Axis 포지션만 할 수 있습니다", MB_ICONHAND);
		return;
	 }
	
}

//LCR Axis Step Move
int AxisMotion::stepMoveLCR(int step)
{
	int iRet;
	int axisCard;
	int iAxisNoInCard;
	double dbCmdPos = 0;

	// step < total step
	if( step >= axisData.steps){
		//Error

		return -1;
	}

	//
	//for(i = 0; i< 8; i ++){
	//	if(axisData.absPos[step][i] > 0){
	//		//move
	//		iRet = pmiAxPosMove( CARD_NO, i, TMC_ABS, axisData.absPos[step][i]);
	//		if( iRet != TMC_RV_OK )
	//		{//Error
	//			return iRet; 
	//		}
	//	}
	//}//for(i = 0; i< 8; i ++){

	//for(i = 8; i< NUM_LCR_AXIS; i ++){
	//	if(axisData.absPos[step][i] > 0){
	//		//move
	//		iRet = pmiAxPosMove( CARD_NO2, i-8, TMC_ABS, axisData.absPos[step][i]);
	//		if( iRet != TMC_RV_OK )
	//		{//Error
	//			return iRet; 
	//		}
	//	}
	//}//for(i = 0; i< 8; i ++){

	for(int iAxisNo = 0; iAxisNo< NUM_LCR_AXIS; iAxisNo ++){

			if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
				iAxisNoInCard = iAxisNo;
			}else{
				axisCard = CARD_NO2;
				iAxisNoInCard = iAxisNo - m_iAxisNum;
			}
		iRet = pmiAxGetCmdPos( axisCard, iAxisNoInCard, &dbCmdPos );
		if(axisData.absPos[step][iAxisNo] != dbCmdPos )
		{
			iRet = m_fnAbsmove(iAxisNo, axisData.absPos[step][iAxisNo]);
			if( iRet != TMC_RV_OK )
			{//Error

				return iRet; 
			}
		}
	}
	return iRet; //
}


int AxisMotion::stepMoveCheckLCR()
{
	int i, iRet;
	int nRunning;
	int nRunningSum;
	//
	nRunningSum = 0;

	for(i = 0; i< 8; i ++){
			iRet = pmiAxCheckDone(CARD_NO, i,  &nRunning);
			if(nRunning == 1)nRunningSum++;
	}
	for(i = 0; i< 2; i ++){
			iRet = pmiAxCheckDone(CARD_NO2, i,  &nRunning);
			if(nRunning == 1)nRunningSum++;
	}

	return nRunningSum; //0: all standing
}

//SET AXIS READY POSITION
void AxisMotion::OnBnClickedBtnAxisReadySet()
{
	int iRet = 0;
	double dbActPos = 0;
	CString strVal;
	int iAxisNo = 0;
	int iAxisNoCB = 0;

	//iAxisNoCB = GetDlgItemInt(IDC_CB_AXIS_SELECT);
	iAxisNoCB = m_CbAxisSelect.GetCurSel();
	if(iAxisNoCB < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNo = iAxisNoCB;
	}else{
		axisCard = CARD_NO2;
		iAxisNoCB = iAxisNoCB - m_iAxisNum;
	}

	if(axisCard == CARD_NO2 && iAxisNoCB == AXIS_SHUTTLE){
		AfxMessageBox(L"LCR Axis 포지션만 할 수 있습니다", MB_ICONHAND);
		return;
	}
	//iRet = pmiAxGetActPos( axisCard, iAxisNoCB, &dbActPos );
	iRet = pmiAxGetCmdPos( axisCard, iAxisNoCB, &dbActPos ); //STEP
	if( ReturnMessage(iRet) == FALSE )
		return;

	strVal.Format(_T("%5.1lf"), dbActPos);
	/*GetDlgItem(IDC_EDT_AXIS0_READY + iAxisNoCB)->SetWindowText(strVal);
	G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNoCB] =(dbActPos);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[AXIS_READY_X + iAxisNoCB], strVal);*/

	if(axisCard == CARD_NO)
	{
	GetDlgItem(IDC_EDT_AXIS0_READY + iAxisNo)->SetWindowText(strVal);
	G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNoCB] =(dbActPos);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[AXIS_READY_X + iAxisNoCB], strVal); 
	}
	else
	{
	GetDlgItem(IDC_EDT_AXIS0_READY9 + iAxisNoCB)->SetWindowText(strVal);
	G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_ST+iAxisNoCB] =(dbActPos);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL_DOUBLE[AXIS_READY_ST + iAxisNoCB], strVal);
	
	}
}


int AxisMotion::checkLCRAxisReadyPosition(void)
{
	int iRet = 0;
	int iAxisNum = 0;
	double dbActPos = 0;
	double dbCmdPos = 0;
	CString strVal;
	int iAxisNo;
	CString strMsg, str;
	double AXIS_POS_TOLERANCE;

	AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];

	//Card no. 1
	for(iAxisNo=0; iAxisNo < m_iAxisNum; iAxisNo++)
	{
		//iRet = pmiAxGetActPos( CARD_NO, iAxisNo, &dbActPos );
		iRet = pmiAxGetCmdPos( CARD_NO, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
		return 0;
		if(dbCmdPos >= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNo] - AXIS_POS_TOLERANCE &&
			dbCmdPos <= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNo] + AXIS_POS_TOLERANCE)
		//if(m_dAxisCmdPos[iAxisNo] >= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNo] - AXIS_POS_TOLERANCE &&
		//	m_dAxisCmdPos[iAxisNo] <= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNo] + AXIS_POS_TOLERANCE)
		{

		}else{
			str =m_fnGetAxisName(iAxisNo) + L"Not In Ready Position";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return 0;
		}
	}

	//Card No. 2
	for(iAxisNo=0; iAxisNo < 2; iAxisNo++)
	{
		//iRet = pmiAxGetActPos( CARD_NO2, iAxisNo, &dbActPos );

		iRet = pmiAxGetCmdPos( CARD_NO2, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
			return 0;
		if(dbCmdPos >= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X+8+ iAxisNo] - AXIS_POS_TOLERANCE &&
			dbCmdPos <= G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X+8+ iAxisNo] + AXIS_POS_TOLERANCE)
		//if(m_dAxisCmdPos[8+iAxisNo] >= G_MainWnd->m_DataHandling.m_FlashData.dbdata[8 + iAxisNo] - AXIS_POS_TOLERANCE &&
		//	m_dAxisCmdPos[8+iAxisNo] <= G_MainWnd->m_DataHandling.m_FlashData.dbdata[8 + iAxisNo] + AXIS_POS_TOLERANCE)
		{

		}else{
			str =m_fnGetAxisName(8 + iAxisNo) + L"Not In Ready Position";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return 0;
		}
	}
	return 1;
}

int AxisMotion::checkLCRAxisPosition(int step)
{
	int iRet = 0;
	int iAxisNum = 0;
	double dbActPos = 0;
	double dbCmdPos = 0;
	
	CString strVal;
	int iAxisNo;
	CString strMsg, str;

	//////////////////////////////////

	// step < total step
	if( step >= axisData.steps){
		//Error

		return -1;
	}
	//////////////////////////////////

	double AXIS_POS_TOLERANCE;

	AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];


 int iTimeOutCount=0;
	//Card no. 1
	for(iAxisNo=0; iAxisNo < m_iAxisNum; iAxisNo++)
	{
		while(1)
		{
			iRet = pmiAxGetActPos( CARD_NO, iAxisNo, &dbActPos );
			iRet = pmiAxGetCmdPos( CARD_NO, iAxisNo, &dbCmdPos );
			if( ReturnMessage(iRet) == FALSE )
				return 0;
			if( abs(/*m_dAxisCmdPos[iAxisNo]*/ dbCmdPos- axisData.absPos[step][iAxisNo]) < AXIS_POS_TOLERANCE)
			{
				break;
			}			
			else
			{
				//Timeout
				if(iTimeOutCount> 1000) //10sec
				{
					strMsg.Format(_T("Not In Step Position[%d]"), step);
					str += m_fnGetAxisName(iAxisNo) + strMsg;
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
					G_SystemModeData.unSystemError = SYSTEM_ERROR;
					return 0;
				}
				iTimeOutCount++;
			}
			Sleep(10);
		}
	}
	iTimeOutCount=0;
		//Card No. 2
	for(iAxisNo=0; iAxisNo < 2; iAxisNo++)
	{			
		while(1)
		{
			iRet = pmiAxGetActPos( CARD_NO2, iAxisNo, &dbActPos );
			iRet = pmiAxGetCmdPos( CARD_NO2, iAxisNo, &dbCmdPos );
			if( ReturnMessage(iRet) == FALSE )
				return 0;
			if( abs(/*m_dAxisCmdPos[iAxisNo + 8]*/dbCmdPos - axisData.absPos[step][iAxisNo + 8]) < AXIS_POS_TOLERANCE)
			{
				break;
			}			
			else
			{
				//Timeout
				if(iTimeOutCount> 1000) //10sec
				{
					strMsg.Format(_T("Not In Step Position[%d]"), step);
					str += m_fnGetAxisName(8 + iAxisNo) + strMsg;
					G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
					G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
					G_SystemModeData.unSystemError = SYSTEM_ERROR;
					return 0;
				}
				iTimeOutCount++;
			}
			Sleep(10);
		}
	}
	return 1;
}


int AxisMotion::checkLCRAxisHomePosition(void)
{
	int iRet = 0;
	int iAxisNum = 0;
	double dbActPos = 0;
	double dbCmdPos = 0;

	CString strVal;
	int iAxisNo;
	CString strMsg, str;

	double AXIS_POS_TOLERANCE;

	AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];

	//Card no. 1
	for(iAxisNo=0; iAxisNo < m_iAxisNum; iAxisNo++)
	{

		//iRet = pmiAxGetActPos( CARD_NO, iAxisNo, &dbActPos );
		iRet = pmiAxGetCmdPos( CARD_NO, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
			return 0;
		if(dbCmdPos >= 0 - AXIS_POS_TOLERANCE &&
			dbCmdPos <= 0 + AXIS_POS_TOLERANCE)
		//if(m_dAxisCmdPos[iAxisNo] >= 0 - AXIS_POS_TOLERANCE &&
		//	m_dAxisCmdPos[iAxisNo] <= 0 + AXIS_POS_TOLERANCE)
		{

		}else{
			strMsg.Format(_T("Not In Home Position"));
			str += m_fnGetAxisName(iAxisNo) + strMsg;
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return 0;
		}
	}

	//Card No. 2
	for(iAxisNo=0; iAxisNo < 2; iAxisNo++)
	{
		//iRet = pmiAxGetActPos( CARD_NO2, iAxisNo, &dbActPos );

		iRet = pmiAxGetCmdPos( CARD_NO2, iAxisNo, &dbCmdPos );
		if( ReturnMessage(iRet) == FALSE )
			return 0;
		if(dbCmdPos >= - AXIS_POS_TOLERANCE &&
			dbCmdPos <= + AXIS_POS_TOLERANCE)
		//if(m_dAxisCmdPos[iAxisNo + 8] >= 0 - AXIS_POS_TOLERANCE &&
		//	m_dAxisCmdPos[iAxisNo + 8] <= 0 + AXIS_POS_TOLERANCE)
		{

		}else{
			strMsg.Format(_T("Not In Home Position"));
			str += m_fnGetAxisName(8 + iAxisNo) + strMsg;
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return 0;
		}
	}
	return 1;
}


BOOL AxisMotion::PreTranslateMessage(MSG* pMsg)
{
	int i;
	int iRet = 0;
	int iAxisNo = 0;
	int iAxisNoInCard = 0;
	int axisCard;
	switch(pMsg->message)
	{
	case WM_LBUTTONDOWN:
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_PJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
			}else{
				axisCard = CARD_NO2;
				iAxisNo = iAxisNo - m_iAxisNum;
			}
			SetJogMoveSpeed(iAxisNo);
			//160119_KYS_TEACHING INTERLOCK
			if (!(G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) && G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO)))
			{
				AfxMessageBox(L"SHUTTLE DOWN, LCR LIFT DOWN 상태가 아닙니다.", MB_ICONHAND);
				//break;
				return TRUE;
			}
			//
				iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_P );
		}
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_NJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
			}else{
				axisCard = CARD_NO2;
				iAxisNo = iAxisNo - m_iAxisNum;
			}
			SetJogMoveSpeed(iAxisNo);
			//160119_KYS_TEACHING INTERLOCK
			if (!(G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) && G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO)))
			{
				AfxMessageBox(L"SHUTTLE DOWN, LCR LIFT DOWN 상태가 아닙니다.", MB_ICONHAND);
				//break;
				return TRUE;
			}
			//
			iRet = pmiAxJogMove( axisCard, iAxisNo, TMC_DIR_N );
		}
		for(i = IDC_BTN_LCR_AXIS31; i <= IDC_BTN_LCR_AXIS40; i++)
		{
			if(pMsg->hwnd == GetDlgItem(i)->m_hWnd)
			{
				iAxisNo = i - IDC_BTN_LCR_AXIS31;
				if(iAxisNo < m_iAxisNum){//
					axisCard = CARD_NO;
					iAxisNoInCard = iAxisNo;
				}else{
					axisCard = CARD_NO2;
					iAxisNoInCard = iAxisNo - m_iAxisNum;
				}
				SetJogMoveSpeed2(iAxisNo);
				//160119_KYS_TEACHING INTERLOCK
				if (!(G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) && G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO)))
				{
					AfxMessageBox(L"SHUTTLE DOWN, LCR LIFT DOWN 상태가 아닙니다.", MB_ICONHAND);
					//break;
					return TRUE;
				}
				//
				iRet = pmiAxJogMove( axisCard, iAxisNoInCard, TMC_DIR_P );
			}
		}

		for(i = IDC_BTN_LCR_AXIS41; i <= IDC_BTN_LCR_AXIS50; i++)
		{
			if(pMsg->hwnd == GetDlgItem(i)->m_hWnd)
			{
				iAxisNo = i - IDC_BTN_LCR_AXIS41;
				if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
				iAxisNoInCard = iAxisNo;
				}else{
					axisCard = CARD_NO2;
					iAxisNoInCard = iAxisNo - m_iAxisNum;
				}
				SetJogMoveSpeed2(iAxisNo);
				//160119_KYS_TEACHING INTERLOCK
				if (!(G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) && G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO)))
				{
					AfxMessageBox(L"SHUTTLE DOWN, LCR LIFT DOWN 상태가 아닙니다.", MB_ICONHAND);
					//break;
					return TRUE;
				}
				//
				iRet = pmiAxJogMove( axisCard, iAxisNoInCard, TMC_DIR_N );
			}
		}
		break;

	case WM_LBUTTONUP:
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_PJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
			}else{
				axisCard = CARD_NO2;
				iAxisNo = iAxisNo - m_iAxisNum;
			}
			iRet = pmiAxStop( axisCard, iAxisNo );
		}
		if(pMsg->hwnd == GetDlgItem(IDC_BTN_NJOG)->m_hWnd)
		{
			iAxisNo = m_CbAxisSelect.GetCurSel();
			if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
			}else{
				axisCard = CARD_NO2;
				iAxisNo = iAxisNo - m_iAxisNum;
			}
			iRet = pmiAxStop( axisCard, iAxisNo );
		}

		for(i = IDC_BTN_LCR_AXIS31; i <= IDC_BTN_LCR_AXIS40; i++)
		{
			if(pMsg->hwnd == GetDlgItem(i)->m_hWnd)
			{
				iAxisNo = i - IDC_BTN_LCR_AXIS31;
				if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
				}else{
					axisCard = CARD_NO2;
					iAxisNo = iAxisNo - m_iAxisNum;
				}
				iRet = pmiAxStop( axisCard, iAxisNo );
			}
		}

		for(i = IDC_BTN_LCR_AXIS41; i <= IDC_BTN_LCR_AXIS50; i++)
		{
			if(pMsg->hwnd == GetDlgItem(i)->m_hWnd)
			{
				iAxisNo = i - IDC_BTN_LCR_AXIS41;
				if(iAxisNo < m_iAxisNum){//
				axisCard = CARD_NO;
				}else{
					axisCard = CARD_NO2;
					iAxisNo = iAxisNo - m_iAxisNum;
				}
				iRet = pmiAxStop( axisCard, iAxisNo );
			}
		}
		break;

	default:
		break;
	}
			
//1007_KYS_ADD				
			if(pMsg->message==WM_KEYDOWN)
			{
				if(pMsg->wParam==VK_ESCAPE||pMsg->wParam==VK_RETURN)
				{
					return TRUE;
				}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////////////


BOOL AxisMotion::m_fnSetPosMoveSpeed(int iAxisNo)
{
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbAccTime = 0;
	double dbDecTime = 0;
	CString strGetValue = _T("");

	int axisCard;
	int iAxisNoInCard;

	iMode = 2; //S-curve
	dbStartSpeed = 1000; //1000;

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	switch(iAxisNo){
		case Axis_Probe_T:
		case Axis_Short_T:
				dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_VEL_T];	
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TACC_T];
				dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TDEC_T];
			break;

		case Axis_Probe_L:
		case Axis_Short_L:
				dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_VEL_L];	
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TACC_L];
				dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TDEC_L];
			break;

		case Axis_Probe_Z:
		case Axis_Short_Z:
				dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_VEL_Z];	
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TACC_Z];
				dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TDEC_Z];

			break;
		case Axis_Probe_X:
		case Axis_Short_X:
		case Axis_Probe_Y:
		case Axis_Short_Y:
				dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_VEL_XY];	
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TACC_XY];
				dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_AXIS_TDEC_XY];
			break;
		//SHUTTLE
		case 10:
				dbDriveSpeed = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_AXIS_VEL];	
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_AXIS_TACC];
				dbDecTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_AXIS_TDEC];
			break;

	}

	iRet = pmiAxSetInitVel( axisCard, iAxisNoInCard, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxSetVelProf( axisCard, iAxisNoInCard, iMode, dbDriveSpeed, dbAccTime, dbDecTime );	
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	return TRUE;
}


BOOL AxisMotion::m_fnAbsmove(int iAxisNo, double nPos) 
{

	int axisCard;
	int iAxisNoInCard;

	int iRet = 0;
	double dbPosition = 0;
	CString strGetValue = _T("");	
	CString str;

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		return FALSE;
	}

	iRet = m_fnSetPosMoveSpeed(iAxisNo);
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxisNo) + L" Set Speed Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	iRet = pmiAxPosMove( axisCard, iAxisNoInCard, TMC_ABS, nPos);
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxisNo) + L" Move Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}


BOOL AxisMotion::m_fnSetHomeMoveSpeed(int iAxisNo)
{
	//////////////////////////////////////////////////////////////////////////
	// Home Move의 시작 속도, 운전속도, 가속도, 감속도를 설정합니다. 		//	
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iMode = 0;
	double dbStartSpeed = 0;
	double dbDriveSpeed = 0;
	double dbRevSpeed = 0;
	double dbAccTime = 0;
	double dbOffset = 0;
	CString strGetValue = _T("");

	int axisCard;
	int iAxisNoInCard;

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}
	
	switch(iAxisNo){
		case Axis_Probe_X:
		case Axis_Short_X:
		case Axis_Probe_Y:
		case Axis_Short_Y:
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_DRIVE_SPEED_XY];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_REV_SPEED_XY];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_TACC_XY];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_HOMEMODE_XY];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_OFFSET_XY];
			break;
		case Axis_Probe_Z:
		case Axis_Short_Z:
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_DRIVE_SPEED_Z];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_REV_SPEED_Z];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_TACC_Z];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_HOMEMODE_Z];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_OFFSET_Z];
			break;

		case Axis_Probe_T:
		case Axis_Short_T:
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_DRIVE_SPEED_T];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_REV_SPEED_T];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_TACC_T];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_HOMEMODE_T];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_OFFSET_T];
			break;

		case Axis_Probe_L:
		case Axis_Short_L:
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_DRIVE_SPEED_L];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_REV_SPEED_L];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_TACC_L];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_HOMEMODE_L];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_HOME_OFFSET_L];
			break;

		case 10://Shuttle
				dbDriveSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_HOME_DRIVE_SPEED];
				dbRevSpeed    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_HOME_REV_SPEED];
				dbAccTime    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_HOME_TACC];
				iMode    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_HOME_HOMEMODE];
				dbOffset    = G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_SHUTTLE_HOME_OFFSET];
			break;
	}


	iRet = pmiAxHomeSetInitVel( axisCard, iAxisNoInCard, dbStartSpeed );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxHomeSetVelProf( axisCard, iAxisNoInCard, 2, dbDriveSpeed, dbRevSpeed, dbAccTime );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	
	//////////////////////////////////////////////////////////////////////////
	// 원점 복귀 완료시 카운터 클리어을 클리어 한다.
	//////////////////////////////////////////////////////////////////////////
	iRet = pmiAxHomeSetResetPos ( axisCard, iAxisNoInCard, 1 );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	iRet = pmiAxHomeSetType ( axisCard, iAxisNoInCard, iMode );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	// Home Move 완료 후 Offset 위치를 설정 합니다.
	iRet = pmiAxHomeSetShiftDist( axisCard, iAxisNoInCard, dbOffset );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	return TRUE;
}

BOOL AxisMotion::m_fnHomemove(int iAxisNo) 
{
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축을 원점 복귀 동작을 수행 합니다.(Home Move)		    	//
	//////////////////////////////////////////////////////////////////////////
	m_fnSoftLimitEnable(FALSE, iAxisNo);	

	int iRet = 0;	
	CString strGetValue = _T("");	
	int axisCard;
	int iAxisNoInCard;

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	iRet =m_fnSetHomeMoveSpeed(iAxisNo);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = m_fnSoftLimitEnable(FALSE, iAxisNo);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	// Home Move를 시작 합니다.
	iRet = pmiAxHomeMove( axisCard, iAxisNoInCard );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;


	return TRUE;
}

BOOL AxisMotion::m_fnSoftLimitEnable(BOOL bUse, int iAxisNo, BOOL bStopMode)
{
	int axisCard;
	int iAxisNoInCard;

	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	int iRet = 0;
	if(bUse == FALSE)
	{	
		iRet = pmiAxSetSoftLimitEnable( axisCard, iAxisNoInCard, bUse);
		if(iAxisNo < NUM_LCR_AXIS) //LCR
			G_MainWnd->m_PIODlg.LCRLimitDisable = 1;
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;

		return TRUE;
	}

	int nPosMin, nPosMax;

	switch(iAxisNo){
		case Axis_Probe_X:
			nPosMin = LIMIT_PROBE_X_MIN;
			nPosMax = LIMIT_PROBE_X_MAX;
			break;

		case Axis_Probe_Y:
			nPosMin = LIMIT_PROBE_Y_MIN;
			nPosMax = LIMIT_PROBE_Y_MAX;
			break;

		case Axis_Probe_Z:
			nPosMin = LIMIT_PROBE_Z_MIN;
			nPosMax = LIMIT_PROBE_Z_MAX;
			break;

		case Axis_Probe_T:
			nPosMin = LIMIT_PROBE_T_MIN;
			nPosMax = LIMIT_PROBE_T_MAX;
			break;
		case Axis_Probe_L:
			nPosMin = LIMIT_PROBE_L_MIN;
			nPosMax = LIMIT_PROBE_L_MAX;
			break;

		case Axis_Short_X:
			nPosMin = LIMIT_SHORT_X_MIN;
			nPosMax = LIMIT_SHORT_X_MAX;
			break;

		case Axis_Short_Y:
			nPosMin = LIMIT_SHORT_Y_MIN;
			nPosMax = LIMIT_SHORT_Y_MAX;
			break;

		case Axis_Short_Z:
			nPosMin = LIMIT_SHORT_Z_MIN;
			nPosMax = LIMIT_SHORT_Z_MAX;
			break;

		case Axis_Short_T:
			nPosMin = LIMIT_SHORT_T_MIN;
			nPosMax = LIMIT_SHORT_T_MAX;
			break;

		case Axis_Short_L:
			nPosMin = LIMIT_SHORT_L_MIN;
			nPosMax = LIMIT_SHORT_L_MAX;
			break;

		case 10://SHUTTLE
			nPosMin = LIMIT_SHUTTLE_MIN;
			nPosMax = LIMIT_SHUTTLE_MAX;
			break;
	}

	iRet = pmiAxSetSoftLimitPos( axisCard,  iAxisNoInCard, nPosMax, nPosMin);
	if(iAxisNo < NUM_LCR_AXIS) //LCR
		G_MainWnd->m_PIODlg.LCRLimitDisable = 0;
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxSetSoftLimitAction( axisCard, iAxisNoInCard, SOFT_LIMIT_E_STOP);

	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	//iRet = pmiAxSetSoftLimitEnable( axisCard, iAxisNoInCard, TRUE);
	iRet = pmiAxSetSoftLimitEnable(axisCard, iAxisNoInCard, FALSE);

	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	
	return TRUE;

}

int AxisMotion::initAxis(int iAxisNo)
{
	int iRet = 0;
	int axisCard;
	int iAxisNoInCard;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	int iIsCheck;

	// TODO: Add extra initialization here
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 ALM 논리와 사용 유무를 변경 합니다.
	// Enable이 1로 설정되면 서보에서 알람 신호 발생 시 즉시 정지 합니다.
	//////////////////////////////////////////////////////////////////////////

	if(axisCard == CARD_NO || (axisCard == CARD_NO2 && iAxisNoInCard < AXIS_SHUTTLE) )
	{
		//inp
		iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, emLOGIC_A );

		iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck );
		iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck );

		//alm
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );

		//lmt
		iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, emLOGIC_B );
		iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );

		iRet = pmiAxSetServoAlarmAction ( axisCard, iAxisNoInCard, emESTOP );
		if( iRet != TMC_RV_OK) {
			AfxMessageBox(L"알람 발생시 정지 방법 함수 동작이 이상합니다.");
			return iRet;
		}
	}
	else if(axisCard == CARD_NO2 && iAxisNoInCard == AXIS_SHUTTLE )
	{

		//inp
		iRet = pmiAxSetServoInpLevel( axisCard, iAxisNo, emLOGIC_A );
		iRet = pmiAxSetServoInpEnable( axisCard, iAxisNo, emLOGIC_B );

		iRet = pmiAxGetServoInpLevel( axisCard, iAxisNo, &iIsCheck );
		iRet = pmiAxGetServoInpEnable( axisCard, iAxisNo, &iIsCheck );

		//alm
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNo, emLOGIC_B );
		iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNo, &iIsCheck );

		//lmt
		iRet = pmiAxSetLimitLevel( axisCard, iAxisNo, emLOGIC_B );
		iRet = pmiAxGetLimitLevel( axisCard, iAxisNo, &iIsCheck );

		iRet = pmiAxSetServoAlarmAction ( axisCard, iAxisNoInCard, emESTOP );
		if( iRet != TMC_RV_OK) {
			AfxMessageBox(L"알람 발생시 정지 방법 함수 동작이 이상합니다.");
			return iRet;
		}
	}

	if( iRet != TMC_RV_OK) {
		//AfxMessageBox(_T("알람 레벨 함수 동작이 이상합니다.");
		return iRet;
	}
	//////////////////////////////////////////////////////////////////////////
	// 선택한 축의 Servo On/Off 신호를 On/Off 합니다
	// 현재 상태가 On이면 Off, Off면 On 합니다.
	//////////////////////////////////////////////////////////////////////////
	iRet = pmiAxSetServoOn( axisCard, iAxisNoInCard, emTRUE);
	if( iRet != TMC_RV_OK) {
		AfxMessageBox(L"서보 온 함수 동작이 이상합니다.");
		return iRet;
	}
	return iRet;
}

void AxisMotion::OnBnClickedBtnShuttleFwd()
{
	int iRet;
	int nDone;

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}

	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	if(IDCANCEL == AfxMessageBox(L"SHUTTLE FWD를 실행하시겠습니까?",MB_OKCANCEL|MB_ICONQUESTION))
		return;

	motorBusy = TRUE;
	m_btnMotorBusy.SetColorChange(RED, BLACK);
	iRet = m_fnAbsmove( 8 + AXIS_SHUTTLE, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD]);
	if(iRet == 1)
	{
		iRet = m_fnAxCheckDone( 8 + AXIS_SHUTTLE, &nDone, 100); //10sec
		if(iRet == 1){//OK
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE FWD 완료",MB_ICONASTERISK);
		}else{//NG
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE FWD 실패",MB_ICONASTERISK);
		}
	}else{
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE FWD 실패",MB_ICONASTERISK);
	}
}


void AxisMotion::OnBnClickedBtnShuttleRet()
{
	int iRet;
	int nDone;

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	if(IDCANCEL == AfxMessageBox(L"SHUTTLE RET를 실행하시겠습니까?",MB_OKCANCEL|MB_ICONQUESTION))
		return;

	motorBusy = TRUE;
	m_btnMotorBusy.SetColorChange(RED, BLACK);
	iRet = m_fnAbsmove( 8 + AXIS_SHUTTLE, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN]);
	if(iRet == 1)
	{
		iRet = m_fnAxCheckDone( 8 + AXIS_SHUTTLE, &nDone, 100); //10sec
		if(iRet == 1){//OK
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE RET 완료",MB_ICONASTERISK);
		}else{//NG
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE RET 실패",MB_ICONASTERISK);
		}
	}else{
			motorBusy = FALSE;
			m_btnMotorBusy.SetColorChange(WHITE, BLACK);
			AfxMessageBox(L"SHUTTLE RET 실패",MB_ICONASTERISK);
	}
}


int AxisMotion::m_fnAxCheckDone(int iAxisNo, int * nDone, int nWait)
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}
	do {
		Sleep(100);
		pmiAxCheckDone(axisCard, iAxisNoInCard, nDone /*&iRet*/);
		if(cnt++ >=  nWait) return 0;
		if(*nDone == emRUNNING) Sleep(100);
	}while(*nDone == emRUNNING);
	//*nDone = iRet;
	return 1;
}


int AxisMotion::m_fnAxHomeCheckDone(int iAxisNo, int * nDone, int nWait)
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	do {
		pmiAxHomeCheckDone(axisCard, iAxisNoInCard, nDone /*&iRet*/);
		if(cnt++ >=  nWait) return 0;
		if(*nDone == emRUNNING)Sleep(100);
	}while(*nDone == emRUNNING);
	//*nDone = iRet;

	return TRUE;
}


BOOL AxisMotion::m_fnClearPos(int iAxisNo) 
{
	//int iRet;
	int axisCard;
	int iAxisNoInCard;
	int cnt = 0;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	int iRet = 0;
	//for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iRet = pmiAxSetCmdPos( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
		iRet = pmiAxSetActPos( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
		iRet = pmiAxSetPosError( axisCard, iAxisNoInCard, 0 );
		if( ReturnMessage(iRet) == FALSE )
			return FALSE;
	}

	return TRUE;
}



void AxisMotion::OnBnClickedBtnShuttleHome()
{
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR){
		AfxMessageBox(L"관리자 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}
	if(IDCANCEL == AfxMessageBox(L"SHUTTLE HOME를 실행하시겠습니까?",MB_OKCANCEL|MB_ICONQUESTION))
		return;
	int iRet;
	motorBusy = TRUE;
	m_btnMotorBusy.SetColorChange(RED, BLACK);
	iRet = m_fnAxisHome(8 + AXIS_SHUTTLE);
	if(iRet == TRUE)
	{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Home 완료",MB_ICONASTERISK);
	}
	motorBusy = FALSE;
	m_btnMotorBusy.SetColorChange(WHITE, BLACK);
}


int AxisMotion::m_fnAxisHome(int iAxis)
{
	CString str;
	int iRet = 0;
	int nDone;
	int nWait = 300;//20sec

	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		return FALSE;
	}

	iRet = m_fnHomemove(iAxis); 
	if( ReturnMessage(iRet) == FALSE ){
			str =m_fnGetAxisName(iAxis) + L"Home Move Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
	if( ReturnMessage(iRet) == FALSE ){
			str = m_fnGetAxisName(iAxis) + L"Home Check Done Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	if(nDone == emSTAND){
#ifdef _DEBUG
		G_AddLog(3,L"m_fnClearPos(Axis No:%d); called", iAxis);
#endif
		iRet = m_fnClearPos(iAxis); 
		if( ReturnMessage(iRet) == FALSE ){
				str =m_fnGetAxisName(iAxis) + L"Home Clear Position Error";
				G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
				G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	iRet = m_fnSoftLimitEnable(TRUE, iAxis);
	if( ReturnMessage(iRet) == FALSE ){
		str =m_fnGetAxisName(iAxis) + L"Home Soft Limit Enable Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}

//JUST FOR SHUTTLE SYS INIT
int AxisMotion::m_fnAxisHome2(int iAxis)
{
	CString str;
	int iRet = 0;
	int nDone;
	int nWait = 300;//20sec

	iRet = m_fnHomemove(iAxis);
	if (ReturnMessage(iRet) == FALSE){
		str = m_fnGetAxisName(iAxis) + L"Home Move Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
	if (ReturnMessage(iRet) == FALSE){
		str = m_fnGetAxisName(iAxis) + L"Home Check Done Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	if (nDone == emSTAND){
#ifdef _DEBUG
		G_AddLog(3, L"m_fnClearPos(Axis No:%d); called", iAxis);
#endif
		iRet = m_fnClearPos(iAxis);
		if (ReturnMessage(iRet) == FALSE){
			str = m_fnGetAxisName(iAxis) + L"Home Clear Position Error";
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	iRet = m_fnSoftLimitEnable(TRUE, iAxis);
	if (ReturnMessage(iRet) == FALSE){
		str = m_fnGetAxisName(iAxis) + L"Home Soft Limit Enable Error";
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	return TRUE;
}

//10 Axis homing together
int AxisMotion::m_fnLCRAxisReadyToHome(void)
{
	int iRet = 0;
	int nDone;
	int nWait = 200;//20sec
	int iAxis;
	CString str;


	if(checkLCRAxisReadyPosition() != 1)
		return FALSE;

	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnHomemove(iAxis); 
		if( ReturnMessage(iRet) == FALSE ){
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 2 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 2 );
			return FALSE;
		}
	}

	int running, cnt;
	cnt = 0;
	do{
		running = 0;
		for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if( ReturnMessage(iRet) == FALSE ){
				if (cnt++ > 3){
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1);
					return FALSE;
				}
			}
			running += nDone; 
		}
	}while(running > 0);

	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
#ifdef _DEBUG
		G_AddLog(3, L"m_fnClearPos(Axis No.: %d); called ", iAxis);
#endif
		iRet = m_fnClearPos(iAxis); 
		if( ReturnMessage(iRet) == FALSE ){
			return FALSE;
		}
	}

	return TRUE;
}

//10 Axis homing together
int AxisMotion::m_fnLCRAxisHomeToReady(void)
{
	int iRet = 0;
	int nDone;
	int nWait = 200;//20sec
	int iAxis;
	CString str;

	// check if it is in home position 
	if(checkLCRAxisHomePosition() != 1)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 4 );
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 4 );
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}


	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAbsmove(iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]); 
		if( ReturnMessage(iRet) == FALSE ){
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5 );
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAxCheckDone(iAxis, &nDone, nWait);
		if( ReturnMessage(iRet) == FALSE ){
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6 );
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 7 );
	return TRUE;
}


//10 Axis homing together 공용
int AxisMotion::m_fnLCRAxisReadyToHome2(void)
{
	int iRet = 0;
	int nDone;
	int nWait = 200;//20sec
	int iAxis;
	CString str;
	int running, cnt;
	int nCount;

//160107_KYS_HOME가능 센서 추가
	if (!(G_MainWnd->m_PIODlg.readDI(IN_LCR_PROBE_HOME, CARD_IO) == 1 && G_MainWnd->m_PIODlg.readDI(IN_LCR_SHORT_HOME, CARD_IO) == 1)
		&& (checkLCRAxisReadyPosition() != 1))
		{
			AfxMessageBox(L"X,Y축 HOME 가능위치를 확인하세요!", MB_ICONINFORMATION);
			return FALSE;
		}

	if (m_fnLCRAxisStatusBitCheck() != 0)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 10);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 10);
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
///////////////////////////////////////20160111_HOME_TEST//////////////////////////////////////////////////
//
#ifdef _FI
//Z axis

	iRet = m_fnHomemove(Axis_Probe_Z);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_Z);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if( ReturnMessage(iRet) == FALSE ){
				return FALSE;
			}
			running += nDone; 
			if(cnt++ > 20) goto AXIS_TIMEUP;
		}
	}while(running > 0);

	iRet = m_fnClearPos(Axis_Probe_Z); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_Z); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_Z); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_Z); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;

	//T,L axis
	iRet = m_fnHomemove(Axis_Probe_T); 	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Probe_L);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_T);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_L);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	} while (running > 0);

	iRet = m_fnClearPos(Axis_Probe_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Probe_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

	//X,Y
	iRet = m_fnHomemove(Axis_Probe_X);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Probe_Y);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_X);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_Y);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	
	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if( ReturnMessage(iRet) == FALSE ){
				return FALSE;
			}
			running += nDone; 
			if(cnt++ > 20) goto AXIS_TIMEUP;
		}
	}while(running > 0);

	iRet = m_fnClearPos(Axis_Probe_X); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Probe_Y); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_X); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_Y); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_X); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_Y); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_X); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_Y); if( ReturnMessage(iRet) == FALSE ) goto AXIS_HOME_CLEAR;
	return TRUE;

AXIS_FALSE:
	if(iRet != TRUE)
	{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 2 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 2 );
			return FALSE;
	}

AXIS_TIMEUP:
	if(iRet != TRUE)
	{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1 );
			return FALSE;
	}

AXIS_HOME_CLEAR:
	if(iRet != TRUE)
	{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 8 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 8 );
			return FALSE;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//공정
#else
////////////////////////////////////////////////
//1. Probe Z axis
iRet = m_fnHomemove(Axis_Probe_Z);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
cnt = 0;
do{
	running = 0;
	Sleep(1000);
	for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
		if (ReturnMessage(iRet) == FALSE){
			return FALSE;
		}
		running += nDone;
		if (cnt++ > 20) goto AXIS_TIMEUP;
	}
}while (running > 0);

	iRet = m_fnClearPos(Axis_Probe_Z); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_Z); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

//2. Probe X+, Y- JOG MOVE TO LIMIT
	//X축 Jog Vel
	iRet = pmiAxSetInitJogVel( 0, 0, 100 );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	iRet = pmiAxSetJogVelProf( 0, 0, 1, 5000, 200 );	
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	//Y축 Jog Vel
	iRet = pmiAxSetInitJogVel( 0, 1, 100 );
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	iRet = pmiAxSetJogVelProf( 0, 1, 1, 5000, 200 );	
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	//X,Y JOG MOVE
	m_fnSoftLimitEnable(FALSE, 0);
	m_fnSoftLimitEnable(FALSE, 1);
	iRet = pmiAxJogMove (0,0, emDIR_P);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;

	iRet = pmiAxJogMove (0,1, emDIR_N);
	if( ReturnMessage(iRet) == FALSE )
		return FALSE;
	
	nCount = 0;
	do{
		Sleep(10);
		nCount++;
		G_AddLog(3, L"PROBE nCount: %d", nCount);
	}
	while((!udtLCRAxisStatus[0].mem_bit.P_LMT) || (!udtLCRAxisStatus[1].mem_bit.N_LMT)
	&&nCount<3000);
	G_AddLog(3, L"PROBE nCount: %d", nCount);
	if (nCount==3000)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 11);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 11);
		return FALSE;
	}
//3.Probe T,L HOME
	//T,L axis
	iRet = m_fnHomemove(Axis_Probe_T); 	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Probe_L);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	} while (running > 0);
	iRet = m_fnClearPos(Axis_Probe_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Probe_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

//4. Probe X,Y HOME
	iRet = m_fnHomemove(Axis_Probe_X); 	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Probe_Y);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	} while (running > 0);

	iRet = m_fnClearPos(Axis_Probe_X); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Probe_Y); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_X); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Probe_Y); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

//5. Short Z Axis HOME
	iRet = m_fnHomemove(Axis_Short_Z);	if( ReturnMessage(iRet) == FALSE ) goto AXIS_FALSE;
	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	}while (running > 0);

	iRet = m_fnClearPos(Axis_Short_Z); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_Z); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

//6. Short X-, Y- JOG MOVE TO LIMIT
	//X축 Jog Vel
	iRet = pmiAxSetInitJogVel( 0, 5, 100 );
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;
	iRet = pmiAxSetJogVelProf(0, 5, 1, 5000, 200);
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;
	//Y축 Jog Vel
	iRet = pmiAxSetInitJogVel(0, 6, 100);
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;
	iRet = pmiAxSetJogVelProf(0, 6, 1, 5000, 200);
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;
	m_fnSoftLimitEnable(FALSE, 5);
	m_fnSoftLimitEnable(FALSE, 6);
	//X,Y JOG MOVE
	iRet = pmiAxJogMove(0, 5, emDIR_N);
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;

	iRet = pmiAxJogMove(0, 6, emDIR_N);
	if (ReturnMessage(iRet) == FALSE)
		return FALSE;

	//WAIT TO LIMIT
	nCount = 0;
	do{
		Sleep(10);
		nCount++;
		G_AddLog(3, L"SHORT nCount: %d", nCount);
	} while ((!udtLCRAxisStatus[5].mem_bit.N_LMT) || (!udtLCRAxisStatus[6].mem_bit.N_LMT)
		&& nCount<3000);
	if (nCount == 3000)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 11);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 11);
		return FALSE;
	}
//7.Short T,L HOME
	//T,L axis
	iRet = m_fnHomemove(Axis_Short_T); 	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_L);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	} while (running > 0);

	iRet = m_fnClearPos(Axis_Short_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_T); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_L); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;

//8. Short X,Y HOME
	iRet = m_fnHomemove(Axis_Short_X); 	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;
	iRet = m_fnHomemove(Axis_Short_Y);	if (ReturnMessage(iRet) == FALSE) goto AXIS_FALSE;

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxHomeCheckDone(iAxis, &nDone, nWait);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20) goto AXIS_TIMEUP;
		}
	} while (running > 0);

	iRet = m_fnClearPos(Axis_Short_X); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnClearPos(Axis_Short_Y); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_X); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	iRet = m_fnSoftLimitEnable(TRUE, Axis_Short_Y); if (ReturnMessage(iRet) == FALSE) goto AXIS_HOME_CLEAR;
	return TRUE;

AXIS_FALSE:
	if (iRet != TRUE)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 2);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 2);
		return FALSE;
	}

AXIS_TIMEUP:
	if (iRet != TRUE)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 1);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 1);
		return FALSE;
	}

AXIS_HOME_CLEAR:
	if (iRet != TRUE)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 8);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 8);
		return FALSE;
	}
#endif
}

//10 Axis homing together 공용
int AxisMotion::m_fnLCRAxisHomeToReady2(void)
{
	int iRet = 0;
	int nDone;
	int nWait = 200;//20sec
	int iAxis;
	CString str;
	int running, cnt;

	// check if it is in home position 
	if(checkLCRAxisHomePosition() != 1)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 4 );
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 4 );
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}
	if (m_fnLCRAxisStatusBitCheck() != 0)
	{
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 10);
		::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 10);
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

#ifdef _FI
	///////////////////////////////////////////////////////////////////////////////////////////
	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAbsmove(iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]); 
		if( ReturnMessage(iRet) == FALSE ){
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5 );
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}

	for(iAxis = 0; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAxCheckDone(iAxis, &nDone, nWait);
		if( ReturnMessage(iRet) == FALSE ){
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6 );
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}


#else
	//short axis together ready
	for(iAxis = 5; iAxis < NUM_LCR_AXIS; iAxis++)
	{
		iRet = m_fnAbsmove(iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]); 
		if( ReturnMessage(iRet) == FALSE )
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5 );
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}
	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for(iAxis = 5; iAxis < NUM_LCR_AXIS; iAxis++)
		{
			iRet = m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 1000);
			if( ReturnMessage(iRet) == FALSE ){
				return FALSE;
			}
			running += nDone; 
			if(cnt++ > 20)
			{
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6 );
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6 );
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
				return FALSE;
			}
		}
	}while(running > 0);

	//P_X_40000
	iRet = m_fnAbsmove(Axis_Probe_X, 40000);
	if( ReturnMessage(iRet) == FALSE )
	{
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5 );
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5 );
		//G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);

		iRet = m_fnAxCheckDone(Axis_Probe_X, &nDone, 1000);
		if (ReturnMessage(iRet) == FALSE)
		{
			return FALSE;
		}
			running += nDone;
		if (cnt++ > 20)
			{
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6);
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
				return FALSE;
			}
		
	} while (running > 0);

	//P_Y_40000
	iRet = m_fnAbsmove(Axis_Probe_Y, 40000);
	if( ReturnMessage(iRet) == FALSE )
	{
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5 );
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5 );
		//G_SystemModeData.unSystemError = SYSTEM_ERROR;
		return FALSE;
	}

	cnt = 0;
	do{
		running = 0;
		Sleep(1000);

		iRet = m_fnAxCheckDone(Axis_Probe_Y, &nDone, 1000);
		if (ReturnMessage(iRet) == FALSE)
		{
			return FALSE;
		}
		running += nDone;
		if (cnt++ > 20)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6);
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}

	} while (running > 0);

	//Probe axis Z,T,L ready
	for(iAxis = 2; iAxis < NUM_LCR_AXIS-5; iAxis++)
	{
		iRet = m_fnAbsmove(iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]);
		if (ReturnMessage(iRet) == FALSE)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5);
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}
	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 2; iAxis < NUM_LCR_AXIS-5; iAxis++)
		{
			iRet = m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 1000);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20)
			{
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6);
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
				return FALSE;
			}
		}
	} while (running > 0);
	
	//Probe axis X,Y ready
	for(iAxis = 0; iAxis < NUM_LCR_AXIS-8; iAxis++)
	{
		iRet = m_fnAbsmove(iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]);
		if (ReturnMessage(iRet) == FALSE)
		{
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 5);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 5);
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			return FALSE;
		}
	}
	cnt = 0;
	do{
		running = 0;
		Sleep(1000);
		for (iAxis = 0; iAxis < NUM_LCR_AXIS - 8; iAxis++)
		{
			iRet = m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 1000);
			if (ReturnMessage(iRet) == FALSE){
				return FALSE;
			}
			running += nDone;
			if (cnt++ > 20)
			{
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 6);
				::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 6);
				G_SystemModeData.unSystemError = SYSTEM_ERROR;
				return FALSE;
			}
		}
	} while (running > 0);
#endif
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 7);
	return TRUE;
}




CString AxisMotion::m_fnGetAxisName(int iAxis)
{
	CString strLCRAxis;
	switch(iAxis){
	case 	Axis_Probe_X:	strLCRAxis.Format(L"LCR PROBE X ");	break;	
	case	Axis_Probe_Y:strLCRAxis.Format(L"LCR PROBE Y ");	break;	
	case	Axis_Probe_Z:strLCRAxis.Format(L"LCR PROBE Z ");	break;	
	case	Axis_Probe_T:strLCRAxis.Format(L"LCR PROBE T ");	break;	
	case	Axis_Probe_L:strLCRAxis.Format(L"LCR PROBE L ");	break;	
	case	Axis_Short_X:strLCRAxis.Format(L"LCR SHORT X ");	break;	
	case	Axis_Short_Y:strLCRAxis.Format(L"LCR SHORT Y ");	break;	
	case	Axis_Short_Z:strLCRAxis.Format(L"LCR SHORT Z ");	break;	
	case	Axis_Short_T:strLCRAxis.Format(L"LCR SHORT T ");	break;	
	case	Axis_Short_L:strLCRAxis.Format(L"LCR SHORT L ");	break;
	case	10:				strLCRAxis.Format(L" SHUTTLE ");	break; //Shuttle
	}
	return strLCRAxis;
}


void AxisMotion::OnBnClickedBtnLcrAxisGoToReadyPosition(UINT nID)
{
	int iRet;
	int nDone;
	int iAxis;
	CString str;

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR){
		AfxMessageBox(L"관리자 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	iAxis = nID - IDC_BTN_LCR_AXIS1;
	str = m_fnGetAxisName(iAxis) + L" 를 Ready Position으로 이동하시겠습니까?";

	if(IDCANCEL == AfxMessageBox(str,MB_OKCANCEL|MB_ICONQUESTION))
		return;

	motorBusy = TRUE;
	m_btnMotorBusy.SetColorChange(RED, BLACK);
	iRet = m_fnAbsmove(Axis_Probe_X + iAxis, G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxis]); 
	if(iRet){
			iRet = m_fnAxCheckDone(Axis_Probe_X + iAxis, &nDone, 200);//20sec 
	}else{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Ready Position 이동 실패",MB_ICONASTERISK);
	}
	if(iRet == TRUE)
	{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Ready Position 이동 완료",MB_ICONASTERISK);
	}else{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Ready Position 이동 실패",MB_ICONASTERISK);
	}

}


void AxisMotion::OnBnClickedBtnLcrAxisGoToHome(UINT nID)
{
	int iRet;
	int iAxis;
	CString str;

	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR){
		AfxMessageBox(L"관리자 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}

	iAxis = nID - IDC_BTN_LCR_AXIS11;
	str = m_fnGetAxisName(iAxis) + L" 를 Home으로 이동하시겠습니까?";

	if(IDCANCEL == AfxMessageBox(str,MB_OKCANCEL|MB_ICONQUESTION))
		return;
	motorBusy = TRUE;
	m_btnMotorBusy.SetColorChange(RED, BLACK);
	iRet = m_fnAxisHome(iAxis);
	if(iRet == TRUE)
	{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Home 완료",MB_ICONASTERISK);
	}else{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
		AfxMessageBox(L"Home 실패",MB_ICONASTERISK);
	}
}

void AxisMotion::OnBnClickedBtnLcrAxisAlarmReset(UINT nID)
{
	int iRet;
	int iAxisNo;
	int iIsCheck;
	int nCount;
	int iOnOff;

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}

	iAxisNo = nID - IDC_BTN_LCR_AXIS21;
	int axisCard;
	int iAxisNoInCard;
	if(iAxisNo < m_iAxisNum){//
		axisCard = CARD_NO;
		iAxisNoInCard = iAxisNo;
	}else{
		axisCard = CARD_NO2;
		iAxisNoInCard = iAxisNo - m_iAxisNum;
	}

	//emg
	iRet = pmiGnGetEmgLevel( axisCard, &iIsCheck );
	if(iIsCheck){
		iRet = pmiGnSetEmgLevel( axisCard, emLOGIC_B );
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel( axisCard, iAxisNoInCard, &iIsCheck );
	if(iIsCheck){
		iRet = pmiAxSetServoAlarmLevel( axisCard, iAxisNoInCard, emLOGIC_A );
	}

	//lmt
	iRet = pmiAxGetLimitLevel( axisCard, iAxisNoInCard, &iIsCheck );
	if(iIsCheck){
		//160118_KYS_LIMIT LOGIC 반대로 셋팅되는 현상
		//		iRet = pmiAxSetLimitLevel( axisCard, iAxisNoInCard, emLOGIC_A );    
		iRet = pmiAxSetLimitLevel(axisCard, iAxisNoInCard, emLOGIC_B);
	}

	//160118_KYS_ALARM RESET FUNC ADD  
	iRet = pmiAxSetServoReset(axisCard, iAxisNoInCard, emON);
	nCount = 0;
	do{
		iRet = pmiAxGetServoReset(axisCard, iAxisNoInCard, &iOnOff);
		Sleep(10);
		nCount++;
	} while ((iOnOff!=1) && (nCount<100) && (iRet==1));
	if (nCount == 100) //1sec
	{
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 11);
		//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 11);
		//return FALSE;
		AfxMessageBox(L"LCR SERVO RESET TIME OUT",MB_ICONHAND);
	}
	iRet = pmiAxSetServoReset(axisCard, iAxisNoInCard, emOFF);
}


void AxisMotion::OnBnClickedBtnShuttleAlarmReset()
{
	int iRet;
	int iIsCheck;

	//yaskawa shuttle

	//emg
	iRet = pmiGnGetEmgLevel( CARD_NO2, &iIsCheck );
	if(iIsCheck){
		iRet = pmiGnSetEmgLevel( CARD_NO2, emLOGIC_B );
	}

	//alm
	iRet = pmiAxGetServoAlarmLevel( CARD_NO2, AXIS_SHUTTLE, &iIsCheck );
	if(iIsCheck){
			iRet = pmiAxSetServoAlarmLevel( CARD_NO2, AXIS_SHUTTLE, emLOGIC_B );
	}

	//lmt
	iRet = pmiAxGetLimitLevel( CARD_NO2, AXIS_SHUTTLE, &iIsCheck );
	if(iIsCheck){
		iRet = pmiAxSetLimitLevel( CARD_NO2, AXIS_SHUTTLE, emLOGIC_B );
	}

}


void AxisMotion::OnBnClickedButton6()
{
		//////////////////////////////////////////////////////////////////////////
	// 선택한 축들을 동시에 감속 정지 시킵니다.								//
	// 정지 명령어를 사용한 후 반드시 Done() 또는 Multi_Done() 함수를		//
	// 사용하여 정지 여부를 확인 하십시요.									//
	// 이 버튼에서는 모든 축을 급정지 시킵니다.								//
	//////////////////////////////////////////////////////////////////////////
	int iRet = 0;
	int iAxisNum = 0;
	int iarAxisList[16] = {0};
	int iarAxisList2[16] = {0};

	iAxisNum = m_iAxisNum;
	for(int AxisNo=0; AxisNo<m_iAxisNum; AxisNo++)
	{
		iarAxisList[AxisNo] = AxisNo;
	}
	iRet = pmiMxEStop( CARD_NO, iAxisNum, iarAxisList );

	iAxisNum = 2;
	for(int AxisNo=0; AxisNo< iAxisNum ; AxisNo++)
	{
		iarAxisList2[AxisNo] = AxisNo;
	}
	iRet = pmiMxEStop( CARD_NO2, iAxisNum, iarAxisList2 );

	CString str;
	str.Format(L"LCR AXIS MOVE E STOP button pressed");
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
	G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
	G_SystemModeData.unSystemError = SYSTEM_ERROR;
	G_MainWnd->m_InspectThread.errorRoutine();
}


void AxisMotion::OnBnClickedButton7()
{
	int iRet = 0;
	iRet = pmiAxEStop(CARD_NO2, AXIS_SHUTTLE );

	CString str;
	str.Format(L"SHUTTLE MOVE E STOP button pressed");
	G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
	G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
	G_SystemModeData.unSystemError = SYSTEM_ERROR;
	G_MainWnd->m_InspectThread.errorRoutine();
}


int AxisMotion::m_fnDisplayShuttleStatus(void)
{
	int iRet = 0;

	iRet = pmiAxGetMechanical( CARD_NO2, AXIS_SHUTTLE, &udtShuttleStatus.mem);
	if( ReturnMessage(iRet) == FALSE )
	{
		m_btnShuttleAlarmReset.SetColorChange(WHITE, BLACK);
		m_btnShuttleFwd.SetColorChange(WHITE, BLACK);
		m_btnShuttleRev.SetColorChange(WHITE, BLACK);
		m_btnShuttleHome.SetColorChange(WHITE, BLACK);
	}	
	//Status
	if( udtShuttleStatus.mem_bit.SON && !udtCardStatus.mem_bit.EMG  && !udtShuttleStatus.mem_bit.ALM && 
		!udtShuttleStatus.mem_bit.P_LMT && !udtShuttleStatus.mem_bit.N_LMT)
	{//READY
		m_btnShuttleAlarmReset.SetColorChange(GREEN, BLACK);
	}else{
		m_btnShuttleAlarmReset.SetColorChange(RED, BLACK);
	}
	//Position
	double dbActPos=0;
	double AXIS_POS_TOLERANCE, shuttleFWD, shuttleRET=0;
	shuttleFWD = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD];
	shuttleRET = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN];
	AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];
	iRet = pmiAxGetActPos( CARD_NO2, AXIS_SHUTTLE, &dbActPos );
	if(dbActPos >= shuttleFWD - AXIS_POS_TOLERANCE && dbActPos <= shuttleFWD + AXIS_POS_TOLERANCE)
	{
		m_btnShuttleFwd.SetColorChange(GREEN, BLACK);
	}else{
		m_btnShuttleFwd.SetColorChange(WHITE, BLACK);
	}
	if(dbActPos >= shuttleRET - AXIS_POS_TOLERANCE && dbActPos <= shuttleRET + AXIS_POS_TOLERANCE)
	{
		m_btnShuttleRev.SetColorChange(GREEN, BLACK);
	}else{
		m_btnShuttleRev.SetColorChange(WHITE, BLACK);
	}
	if(dbActPos >=  - AXIS_POS_TOLERANCE && dbActPos <=  + AXIS_POS_TOLERANCE)
	{
		m_btnShuttleHome.SetColorChange(GREEN, BLACK);
	}else{
		m_btnShuttleHome.SetColorChange(WHITE, BLACK);
	}


	return 0;
}


int AxisMotion::m_fnDisplayLCRAxisStatus(void)
{
	int iRet = 0;
	int axisCard;
	int iAxisNoInCard;
	int iAxisNo;
	double dbActPos;
	double AXIS_POS_TOLERANCE, axisReady;
	double shuttleFWD, shuttleRET;

	//LCR
	for(iAxisNo = 0; iAxisNo < NUM_LCR_AXIS; iAxisNo++)
	{
		if(iAxisNo < m_iAxisNum){//
			axisCard = CARD_NO;
			iAxisNoInCard = iAxisNo;
		}else{
			axisCard = CARD_NO2;
			iAxisNoInCard = iAxisNo - m_iAxisNum;
		}

		iRet = pmiAxGetMechanical( axisCard, iAxisNoInCard, &udtLCRAxisStatus[iAxisNo].mem);
		if( ReturnMessage(iRet) == FALSE )
		{
			m_btnLCRAxisAlarmReset[iAxisNo].SetColorChange(WHITE, BLACK);
			m_btnLCRAxisGoToReady[iAxisNo].SetColorChange(WHITE, BLACK);
			m_btnLCRAxisHome[iAxisNo].SetColorChange(WHITE, BLACK);
			continue;
		}	
			//Status
		if( udtLCRAxisStatus[iAxisNo].mem_bit.SON && !udtCardStatus.mem_bit.EMG  && !udtLCRAxisStatus[iAxisNo].mem_bit.ALM && 
			!udtLCRAxisStatus[iAxisNo].mem_bit.P_LMT && !udtLCRAxisStatus[iAxisNo].mem_bit.N_LMT)
		{//READY
			m_btnLCRAxisAlarmReset[iAxisNo].SetColorChange(GREEN, BLACK);
		}else{
			m_btnLCRAxisAlarmReset[iAxisNo].SetColorChange(RED, BLACK);
		}
		//Position

		axisReady = G_MainWnd->m_DataHandling.m_FlashData.dbdata[AXIS_READY_X + iAxisNo];
		AXIS_POS_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_AXIS_TOLERANCE];
		//STEP MOTOR
		iRet = pmiAxGetCmdPos( axisCard, iAxisNoInCard, &dbActPos );
		if(dbActPos >= axisReady - AXIS_POS_TOLERANCE && dbActPos <= axisReady + AXIS_POS_TOLERANCE)
		{
			m_btnLCRAxisGoToReady[iAxisNo].SetColorChange(GREEN, BLACK);
		}else{
			m_btnLCRAxisGoToReady[iAxisNo].SetColorChange(WHITE, BLACK);
		}
		if(dbActPos >=  - AXIS_POS_TOLERANCE && dbActPos <=  + AXIS_POS_TOLERANCE)
		{
			m_btnLCRAxisHome[iAxisNo].SetColorChange(GREEN, BLACK);
		}else{
			m_btnLCRAxisHome[iAxisNo].SetColorChange(WHITE, BLACK);
		}
	}

//160107_KYS_HOME가능 센서 추가
	if (G_MainWnd->m_PIODlg.readDI(IN_LCR_PROBE_HOME, CARD_IO) == 1 && G_MainWnd->m_PIODlg.readDI(IN_LCR_SHORT_HOME, CARD_IO) == 1)
	{
		m_btnLCRHomeAll.SetColorChange(GREEN, BLACK);
	}
	else{
		m_btnLCRHomeAll.SetColorChange(WHITE, BLACK);
	}

	if (G_MainWnd->m_PIODlg.readDI(IN_LCR_PROBE_HOME, CARD_IO))
	{
		m_btnProbeHomeEnable.SetColorChange(GREEN, BLACK);
	}
	else{
		m_btnProbeHomeEnable.SetColorChange(RED, BLACK);
	}

	if (G_MainWnd->m_PIODlg.readDI(IN_LCR_SHORT_HOME, CARD_IO))
	{
		m_btnShortHomeEnable.SetColorChange(GREEN, BLACK);
	}
	else{
		m_btnShortHomeEnable.SetColorChange(RED, BLACK);
	}

//
	//SHUTTLE
		iRet = pmiAxGetMechanical( CARD_NO2, AXIS_SHUTTLE, &udtShuttleStatus.mem);
		if( ReturnMessage(iRet) == FALSE )
		{
			m_btnShuttleAlarmReset.SetColorChange(WHITE, BLACK);
			m_btnShuttleFwd.SetColorChange(WHITE, BLACK);
			m_btnShuttleRev.SetColorChange(WHITE, BLACK);
			m_btnShuttleHome.SetColorChange(WHITE, BLACK);
			return 0;
		}	
		//Status
		if( udtShuttleStatus.mem_bit.SON && !udtCardStatus.mem_bit.EMG  && !udtShuttleStatus.mem_bit.ALM && 
			!udtShuttleStatus.mem_bit.P_LMT && !udtShuttleStatus.mem_bit.N_LMT)
		{//READY
			m_btnShuttleAlarmReset.SetColorChange(GREEN, BLACK);
		}else{
			m_btnShuttleAlarmReset.SetColorChange(RED, BLACK);
		}
		//Position
		shuttleFWD = G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_FORWARD];
		shuttleRET =G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN];
		//SERVO MOTOR
		iRet = pmiAxGetActPos( CARD_NO2, AXIS_SHUTTLE, &dbActPos );
		if(dbActPos >= shuttleFWD - AXIS_POS_TOLERANCE && dbActPos <= shuttleFWD + AXIS_POS_TOLERANCE)
		{
			m_btnShuttleFwd.SetColorChange(GREEN, BLACK);
		}else{
			m_btnShuttleFwd.SetColorChange(WHITE, BLACK);
		}
		if(dbActPos >= shuttleRET - AXIS_POS_TOLERANCE && dbActPos <= shuttleRET + AXIS_POS_TOLERANCE)
		{
			m_btnShuttleRev.SetColorChange(GREEN, BLACK);
		}else{
			m_btnShuttleRev.SetColorChange(WHITE, BLACK);
		}
		if(dbActPos >=  - AXIS_POS_TOLERANCE && dbActPos <=  + AXIS_POS_TOLERANCE)
		{
			m_btnShuttleHome.SetColorChange(GREEN, BLACK);
		}else{
			m_btnShuttleHome.SetColorChange(WHITE, BLACK);
		}

	return 0;
}

//
int AxisMotion::sysInitLCRAxisPosCheck(void)
{
	//LCR
	//Manual mode
	if(G_MainWnd->m_DataHandling.m_FlashData.data[LCR_AXIS_STEP] == 0){ //Ready position
		if(!checkLCRAxisReadyPosition())//Error
		{
			AfxMessageBox(L"LCR 모터 축 위치가 맞지 않습니다. 홈으로 이동후 READY 포지션으로 이동시켜 포지션을 확인해주세요",MB_ICONHAND);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 0 );

		}
	}else {
		if(!checkLCRAxisPosition(G_MainWnd->m_DataHandling.m_FlashData.data[LCR_AXIS_STEP]))
		{
			AfxMessageBox(L"LCR 모터 축 위치가 맞지 않습니다. 동작시키지 마시고 LCR 검사를 리셋한 후, 각 모터축을 홈으로 이동후 READY 포지션으로 이동시켜 포지션을 확인해주세요",MB_ICONHAND);
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 0 );
		}
	}

	return 0;
}


void AxisMotion::OnBnClickedBtnAxisBusyClear()
{
		motorBusy = FALSE;
		m_btnMotorBusy.SetColorChange(WHITE, BLACK);
}



void AxisMotion::OnBnClickedBtnAxisMsgBusyClear()
{
	
}


void AxisMotion::OnBnClickedBtnLcrReady()
{
	int iRet;

#ifndef _FI
		//AfxMessageBox(L"최종검사기에서 실행해야 합니다",MB_ICONINFORMATION);
		//return;
#endif

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL)
	{
		AfxMessageBox(L"MANUAL 모드에서 실행해야 합니다",MB_ICONINFORMATION);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"관리자 모드로 리셋할 수 있습니다",MB_ICONINFORMATION);
		return;
	}
	if (G_MainWnd->m_PIODlg.readDI(IN_LCR_JIG_DETECT, CARD_IO) ||
		!G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO) || !G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO)
		)
	{
		AfxMessageBox(L"LCR JIG DETECT/LCR LIFT DOWN/SHUTTLE DOWN 센서를 확인하세요", MB_ICONINFORMATION);
		return;
	}
	if(IDCANCEL == AfxMessageBox(L"모든 포지션이 HOME POSITION에 있어야 합니다. LCR 10축 모두를 READY POSITION으로 이동하시겠습니까? ",MB_OKCANCEL|MB_ICONQUESTION))
		return;

	iRet = m_fnLCRAxisHomeToReady2();

	if(iRet)
		AfxMessageBox(L"LCR READY POSITION 이동 완료",MB_ICONINFORMATION);

}


void AxisMotion::OnBnClickedBtnLcrHome()
{
	int iRet;
	
#ifndef _FI
		//AfxMessageBox(L"최종검사기에서 실행해야 합니다",MB_ICONINFORMATION);
		//return;
#endif

	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL)
	{
		AfxMessageBox(L"MANUAL 모드에서 실행해야 합니다",MB_ICONINFORMATION);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"관리자 모드로 리셋할 수 있습니다",MB_ICONINFORMATION);
		return;
	}
	if (G_MainWnd->m_PIODlg.readDI(IN_LCR_JIG_DETECT, CARD_IO) ||
		!G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO) || !G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO)
		)
	{
		AfxMessageBox(L"LCR JIG DETECT/LCR LIFT DOWN/SHUTTLE DOWN 센서를 확인하세요", MB_ICONINFORMATION);
		return;
	}

	if (IDCANCEL == AfxMessageBox(L"모든 포지션이 READY POSITION 이거나 HOME 가능 센서위치여야 합니다. LCR 10축 모두를 Home으로 이동하시겠습니까? ", MB_OKCANCEL | MB_ICONQUESTION))
	{
		return;
	}
	else{
		//if (a&&b)
		//{
			iRet = m_fnLCRAxisReadyToHome2();
		//}
	
		if (iRet)
			AfxMessageBox(L"LCR HOME 완료", MB_ICONINFORMATION);

	}
}


int AxisMotion::sysInitShuttle(void)
{
	int iRet, nDone;
		//manual mode, shuttle step0
		if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
			AfxMessageBox(L"SHUTTLE SYS INIT HOME 이동: MANUAL 모드에서 실행해야 합니다",MB_ICONINFORMATION);
			return -1;
		}

		if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(SHUTTLE) != STEP0){
			AfxMessageBox(L"SHUTTLE SYS INIT HOME 이동: SHUTTLE STEP이 READY STEP (STEP0)이 아니어서 HOME으로 이동할 수 없습니다.",MB_ICONINFORMATION);
			return -1;
		}

		if(G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) != 1 
#ifdef _FI
			|| 	G_MainWnd->m_PIODlg.readDI(IN_UNLOAD_CV_UP, CARD_IO) != 1
#endif
			)
		{
			//
			AfxMessageBox(L"SHUTTLE HOME으로 이동할 수 없습니다 SHUTTLE DOWN, UNLOAD LIFT UP을 확인후 HOME으로 이동시켜 주세요",MB_ICONHAND);
			return -1;
		}

		//axisMsgRsp.BUSY = 1;
		G_MainWnd->m_InspectThread.setBatchStatus(SHUTTLE, LOCKED);
		iRet = m_fnAxisHome2(8 + AXIS_SHUTTLE);
		if(iRet){
			Sleep(1000);
			iRet = m_fnAbsmove( 8 + AXIS_SHUTTLE, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN]); 
			if(iRet == 1)
			{
				iRet = m_fnAxCheckDone( 8 + AXIS_SHUTTLE, &nDone, 150); //15sec
				if(iRet == 1){//OK
					SetUMMsgResponse(UW_READY_TO_HOME, UM_AXIS_OK);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 20 ); 
				}else{//NG
					SetUMMsgResponse(UW_READY_TO_HOME, UM_AXIS_NG);
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 3 ); 
					::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 3 ); 

				}
			}
		}
		else{
			AfxMessageBox(L"SHUTTLE HOME으로 이동할 수 없습니다 MANUAL로 HOME으로 이동 후 RET 위치로 이동시켜 주세요",MB_ICONHAND);
		}
		//axisMsgRsp.BUSY = 0;	
		G_MainWnd->m_InspectThread.setBatchStatus(SHUTTLE, CLEAR);

	return 0;
}



void AxisMotion::OnBnClickedBtnShuttleInit2()
{
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}

	if(motorBusy != FALSE){
		AfxMessageBox(L"Motor Busy",MB_ICONHAND);
		return;
	}

	if(IDCANCEL == AfxMessageBox(L"SHUTTLE HOME INIT을 실행하시겠습니까?",MB_OKCANCEL|MB_ICONQUESTION))
		return;

	int iRet = sysInitShuttle();
	if(iRet ==0)
		AfxMessageBox(L"SHUTTLE HOME INIT 완료",MB_ICONINFORMATION);
}

void AxisMotion::SetUMMsgResponse(int msg, int rsp)
{
	axisMsgRsp.RSP[msg] = rsp;
}

int AxisMotion::GetUMMsgResponse(int msg)
{
	int rsp;
	rsp = axisMsgRsp.RSP[msg];
	return rsp;
}

//m_fnLCRAxisStatusBitCheck_160112_kys
int AxisMotion::m_fnLCRAxisStatusBitCheck(void)
{
	int iAxisNo;
	for (iAxisNo = 0; iAxisNo < NUM_LCR_AXIS; iAxisNo++)
	{
		//SERVO ON
		if (!udtLCRAxisStatus[iAxisNo].mem_bit.SON){
			CString sMsg;
			sMsg.Format(L"%d번축의 SON 신호를 확인하세요", iAxisNo+1);
			AfxMessageBox(sMsg, MB_ICONHAND);
			return -1;
		}
		
		//EMG STOP
		if (udtLCRAxisStatus[iAxisNo].mem_bit.EMG){
			//CString sMsg;
			//sMsg.Format(L"%d번 축의 SON 신호를 확인하세요", iAxisNo+1);
			AfxMessageBox(L"EMERGENCY STOP 상태를 확인하세요", MB_ICONHAND);
			return -1;
		}
		
		//ALARM
		if (udtLCRAxisStatus[iAxisNo].mem_bit.ALM){
			CString sMsg;
			sMsg.Format(L"%d번축 ALARM 상태입니다", iAxisNo + 1);
			AfxMessageBox(sMsg, MB_ICONHAND);
			return -1;
		}
		
		//+LIMIT
		if (udtLCRAxisStatus[iAxisNo].mem_bit.P_LMT){
			CString sMsg;
			sMsg.Format(L"%d번축 +LIMIT 상태입니다", iAxisNo + 1);
			AfxMessageBox(sMsg, MB_ICONHAND);
			return -1;
		}
		
		//-LIMIT
		if (udtLCRAxisStatus[iAxisNo].mem_bit.N_LMT){
			CString sMsg;
			sMsg.Format(L"%d번축 -LIMIT 상태입니다", iAxisNo + 1);
			AfxMessageBox(sMsg, MB_ICONHAND);
			return -1;
		}
		
		//H_OK
		if ((!udtLCRAxisStatus[iAxisNo].mem_bit.H_OK)&&
			!(G_MainWnd->m_PIODlg.readDI(IN_LCR_PROBE_HOME, CARD_IO) == 1 && G_MainWnd->m_PIODlg.readDI(IN_LCR_SHORT_HOME, CARD_IO) == 1)){
			CString sMsg;
			sMsg.Format(L"%d번축 HOME_OK 상태가 아닙니다", iAxisNo + 1);
			AfxMessageBox(sMsg, MB_ICONHAND);
			return -1;
		}
	}
	return 0;
}