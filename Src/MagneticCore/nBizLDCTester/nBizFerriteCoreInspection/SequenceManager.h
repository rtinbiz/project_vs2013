#pragma once

#include <afxmt.h>
#include <afxwin.h>

#define SM_STEP_INIT				0
#define SM_STEP_START				1
#define SM_STEP_END					(-1)

#define SLEEP_COUNT				10



class CSequenceManager : public CWnd
{
public:
	CSequenceManager();
	virtual ~CSequenceManager();

	int				m_fnInit();
	int				m_fnDeInit();			

	int 			m_fnSuspendStep();
	int			    m_fnResumeStep();

	int 			m_fnSuspendStep_Input();
	int			    m_fnResumeStep_Input();

	void			m_fnInitStep(int nStepValue=SM_STEP_INIT);
	void			m_fnClearScenario();
	void			m_fnSetStep(int nStep);			
	void			m_fnSetOldStep(int nOldStep);	
	int				m_fnGetStep();
	int				m_fnGetOldStep();
	BOOL		m_fnCheckStep();
	int				m_fnRetryStep();
	int				m_fnPrevGetStep();

	HANDLE			m_hThread;
	CWinThread* m_pThread; 
	CWinThread* m_pThreadMotion; 
	int		suspendCount;
	

protected:

	int						m_fnStartScenario(BOOL bStart, BOOL nScenarioMode = TRUE);
	int                m_fnStartMotionThread(BOOL bStart);
	static UINT		m_fnCheckScenario(LPVOID lpThread);
	static UINT		m_fnCheckInput(LPVOID lpThread);
	static UINT     m_fnCheckMotion(LPVOID lpThread);
	virtual void	m_fnCheck() {}
	virtual void	m_fnPosCheckMotion() {}
	int				m_fnGetCurrentTime();
	int				m_fnGetCurrentTime2();
		
	
//	BOOL			m_bFlagThread;

	int				m_nCurStep;
	int				m_nOldStep;
	int				m_nPrevStep;

	volatile bool m_bFlagThread;    // 이벤트를 써도 됨
	volatile bool m_bFlagThreadMotion;    // 이벤트를 써도 됨
	CCriticalSection m_csExitThread;
};

