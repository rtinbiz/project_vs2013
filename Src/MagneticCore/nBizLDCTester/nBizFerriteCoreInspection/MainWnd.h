#pragma once

#include "nBizLDCTesterDlg.h"
#include "DataHandling.h"
#include "SerialInterface.h"
#include "InspectThread.h"
#include "SystemSetupDlg.h"
#include "RecipeSetDlg.h"
#include "Instrument.h"
#include "PIO.h"
#include "AxisMotion.h"
#include "LabelPrint.h"



class CMainWnd : public CWnd
{
	DECLARE_DYNAMIC(CMainWnd)

public:
	CMainWnd();
	virtual ~CMainWnd();			

	BOOL	m_fnInit();
	BOOL	m_fnDeInit();		

public:
	CDataHandling			m_DataHandling;

	CnBizLDCTesterDlg	m_ServerDlg;
	CInspectThread			m_InspectThread;
	CSerialInterface		m_SerialInterface;

	CSystemSetupDlg			m_SystemSetupDlg;
	CRecipeSetDlg			m_RecipeSetDlg;	
	CInstrument			m_InstrumentDlg;

	//CImageViewer			m_ImageViewDlg;
	//	CAlarmClearDlg			m_AlarmClearDlg;
	//	CLiveView				m_LiveView;
	PIO						m_PIODlg;
	AxisMotion				m_AxisDlg;
//1116_kys
//#ifdef _FI
	CLabelPrint				m_LabelPrint;
//#endif

	HBRUSH					m_DILEDalog_Brush;
	HBRUSH					m_Static_Brush;	
	HBRUSH					m_AlarmDlg_Brush;
	HBRUSH					m_InitDlg_Brush;	 
protected:

	DECLARE_MESSAGE_MAP()

protected:

	//{{AFX_MSG(CMainWnd)	
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG
	
public:

};


