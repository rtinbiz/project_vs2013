#include "StdAfx.h"
#include "VisionInspection.h"
#include "nBizLDCTester.h"
#include "afxdialogex.h"


IMPLEMENT_DYNAMIC(CVisionInspection, CDialogEx)

extern CMainWnd	*G_MainWnd;

CVisionInspection::CVisionInspection(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVisionInspection::IDD, pParent)
{
    pCam = NULL;  //1224_jec

}

CVisionInspection::~CVisionInspection(void)
{
	//1224_jec
	if (pRawBuf != NULL)
		cvReleaseImage(&pRawBuf);
	if (pImgBuf != NULL)
		cvReleaseImage(&pImgBuf);
	if (pCam != NULL)
		delete pCam;
	pCam = NULL;
}

void CVisionInspection::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PIC, m_pic);
}

BEGIN_MESSAGE_MAP(CVisionInspection, CDialogEx)

	ON_BN_CLICKED(IDC_LIGHT, &CVisionInspection::OnBnClickedLight)
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BTN_GRAB, &CVisionInspection::OnBnClickedBtnGrab)
	ON_MESSAGE(UM_VI, &CVisionInspection::OnUmVi)
	ON_BN_CLICKED(IDC_BTN_VISION_INSPECTION, &CVisionInspection::OnBnClickedBtnVisionInspection)
END_MESSAGE_MAP()


BOOL CVisionInspection::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();


	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CVisionInspection::m_fnInit(){

	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);	

#ifdef _DEBUG
	//// retrieve the heartbeat node from the transport layer node map
	//GenApi::CIntegerPtr pHeartbeat = pCam->GetTLNodeMap()->GetNode("HeartbeatTimeout");
	//// set heartbeat to 600 seconds. (Note: Only GigE cameras have a "HeatbeatTimeout" node)
	//if (pHeartbeat != NULL ) pHearbeat->SetValue(600*1000);
#endif

	camConnected = CAM_DISCONNECTED;

#ifndef _VI_SKIP
	//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
		if (!pylonInitGrab()){
			camConnected = CAM_CONNECTED;
			G_MainWnd->m_ServerDlg.m_statusCam.SetBackColor(GREEN);
			pylonSetParam();
		}
		else{
			camConnected = CAM_DISCONNECTED;
			G_MainWnd->m_ServerDlg.m_statusCam.SetBackColor(RGB(230, 230, 230));
		}
		//pylonInitGrabSWTrigger();
	//}
#endif

	pRawBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 1);
	pImgBuf = cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 3);

	if (!LoadNIPJob()) {
		AfxMessageBox(L"Cannot load NIPJob data");
	}
};

void CVisionInspection::m_fnDeInit(){



};

void CVisionInspection::OnPaint()
{
	CPaintDC dc(this); 
	drawImage();
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}


void CVisionInspection::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnClose();
}


HBRUSH CVisionInspection::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CVisionInspection::OnBnClickedLight()
{
	if(swLight != ON){
		swLight = ON;
	}else{
		swLight = OFF;
	}
	G_MainWnd->m_InspectThread.turnLight(swLight);
}


int CVisionInspection::pylonSetParam(void)
{
	int exitCode = 0;
	try
	{
		// Set the image format and AOI
		
		pCam->PixelFormat.SetValue(PixelFormat_BayerBG8); //PixelFormat_BayerGR8
		pCam->OffsetX.SetValue(0);
		pCam->OffsetY.SetValue(0);
		//pCam->Width.SetValue(pCam->Width.GetMax());
		//pCam->Height.SetValue(pCam->Height.GetMax());

		pCam->Width.SetValue(1280);
		pCam->Height.SetValue(960);
		//Set acquisition mode
		//pCam->AcquisitionMode.SetValue(AcquisitionMode_SingleFrame);
		pCam->LightSourceSelector.SetValue(LightSourceSelector_Off);	
		////////////////
		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Red);
		pCam->BalanceRatioAbs.SetValue(5.0);
		pCam->BalanceRatioRaw.SetValue(300);

		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Green);
		pCam->BalanceRatioAbs.SetValue(2.5);
		pCam->BalanceRatioRaw.SetValue(150);

		pCam->BalanceRatioSelector.SetValue(BalanceRatioSelector_Blue);
		pCam->BalanceRatioAbs.SetValue(5.0);
		pCam->BalanceRatioRaw.SetValue(300);

		////Set exposure settings
		pCam->ExposureMode.SetValue(ExposureMode_Timed);
		pCam->ExposureTimeRaw.SetValue(7000);

		//// Gain Setting
		pCam->GainRaw.SetValue(300);
		pCam->BlackLevelSelector.SetValue(BlackLevelSelector_All);
		pCam->BlackLevelRaw.SetValue(50);
		pCam->GammaSelector.SetValue(GammaSelector_User);
		pCam->Gamma.SetValue(1.0);
		pCam->GammaEnable.SetValue(true);
		
		////WhiteBalance Off
		pCam->BalanceWhiteAuto.SetValue(BalanceWhiteAuto_Off);

		// Create an image buffer
		ImageSize = (size_t)(pCam->PayloadSize.GetValue());

		// The parameter MaxNumBuffer can be used to control the count of buffers
		// allocated for grabbing. The default value of this parameter is 10.
		pCam->MaxNumBuffer = 5;
	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		G_AddLog(3, L"pylonSetParam Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = 1;
	}

	return 0;
}

int CVisionInspection::pylonInitGrab(void)
{
	int exitCode = 0;
	try
	{
		pCam = new CBaslerGigEInstantCamera(CTlFactory::GetInstance().CreateFirstDevice());
		pCam->Open();

	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		//G_AddLog(3, L"pylonInitGrab Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = 1;
	}

	return exitCode;
}

int CVisionInspection::pylonInitGrabOne(void)
{
	int exitCode = 0;
	try
	{
		pCam = new CBaslerGigEInstantCamera(CTlFactory::GetInstance().CreateFirstDevice());
		pCam->Open();
	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		G_AddLog(3, L"pylonInitGrabOne Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = 1;
	}
	return 0;
}

int CVisionInspection::pylonInitGrabSWTrigger(void)
{
	int exitCode = 0;
	try
	{
		// Create an instant camera object for the camera device found first.
		pCam = new CBaslerGigEInstantCamera(CTlFactory::GetInstance().CreateFirstDevice());

		// Register the standard configuration event handler for enabling software triggering.
		// The software trigger configuration handler replaces the default configuration
		// as all currently registered configuration handlers are removed by setting the registration mode to RegistrationMode_ReplaceAll.
		pCam->RegisterConfiguration( new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);

		// For demonstration purposes only, add sample configuration event handlers to print out information
		// about camera use and image grabbing.
		//pCam->RegisterConfiguration( new CConfigurationEventPrinter, RegistrationMode_Append, Cleanup_Delete);
		//pCam->RegisterImageEventHandler( new CImageEventPrinter, RegistrationMode_Append, Cleanup_Delete);

		// Print the model name of the camera.
		cout << "Using device " << pCam->GetDeviceInfo().GetModelName() << endl;

		// When setting the output queue size to 1 this strategy is equivalent to the GrabStrategy_LatestImageOnly grab strategy.
		pCam->OutputQueueSize = 1;

		// Open the pCam->
		pCam->Open();
	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		G_AddLog(3, L"pylonInitGrabSWTrigger Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = 1;
	}

	return 0;
}

int CVisionInspection::pylonGrab(void)
{
	// The exit code of the sample application.
	int retry = 0;
	int exitCode = 0;
	CString strSavePath;

REGRAB:
	try
	{
		pCam->StartGrabbing( c_countOfImagesToGrab);
		CGrabResultPtr ptrGrabResult;
		while ( pCam->IsGrabbing())
		{
			pCam->RetrieveResult( 500000, ptrGrabResult, TimeoutHandling_ThrowException);

			// Image grabbed successfully?
			if (ptrGrabResult->GrabSucceeded())
			{
				// Access the image data.
				const uint8_t *pImageBuffer = (uint8_t *) ptrGrabResult->GetBuffer();
				memcpy(pRawBuf->imageData, pImageBuffer, ptrGrabResult->GetImageSize());
				cvCvtColor(pRawBuf, pImgBuf, CV_BayerBG2RGB); 

			}
			else
			{
				if (retry++ <= 3){
					Sleep(10);
					goto REGRAB;
				}
				G_AddLog(3, L"pylonInitGrab Error %d %s", ptrGrabResult->GetErrorCode(), ptrGrabResult->GetErrorDescription().w_str());
			}
		}


	}
	catch (GenICam::GenericException &e)
	{
		if (retry++ <= 3){
			Sleep(10);
			goto REGRAB;
		}
		// Error handling.
		G_AddLog(3, L"pylonInitGrab Exception %s", ConvertMultybyteToUnicode(e.GetDescription()));
		exitCode = 1;
	}
	return exitCode;
}

int CVisionInspection::pylonGrabOne(void)
{
	return 0;
}

int CVisionInspection::pylonGrabSWTrigger(void)
{
	int exitCode = 0;
	try
	{
		// The GrabStrategy_LatestImageOnly strategy is used. The images are processed
		// in the order of their arrival but only the last received image
		// is kept in the output queue.
		// This strategy can be useful when the acquired images are only displayed on the screen.
		// If the processor has been busy for a while and images could not be displayed automatically
		// the latest image is displayed when processing time is available again.
		pCam->StartGrabbing( GrabStrategy_LatestImageOnly);

		// Execute the software trigger, wait actively until the camera accepts the next frame trigger or until the timeout occurs.
		if ( pCam->WaitForFrameTriggerReady( 200, TimeoutHandling_ThrowException))
		{
			pCam->ExecuteSoftwareTrigger();
		}

		// Wait for all images.
		WaitObject::Sleep(200);

		// Check whether the grab result is waiting.
		if ( pCam->GetGrabResultWaitObject().Wait( 0))
		{
			cout << endl << "A grab result waits in the output queue." << endl << endl;
		}

		// Only the last received image is waiting in the internal output queue
		// and is now retrieved.
		// The grabbing continues in the background, e.g. when using the hardware trigger mode.
		int nBuffersInQueue = 0;
		while( pCam->RetrieveResult( 0, ptrGrabResult, TimeoutHandling_Return))
		{
			cout << "Skipped " << ptrGrabResult->GetNumberOfSkippedImages() << " images." << endl;
			nBuffersInQueue++;
		}

		cout << "Retrieved " << nBuffersInQueue << " grab result from output queue." << endl << endl;
		if(nBuffersInQueue >= 1){
			const uint8_t *pImageBuffer = (uint8_t *) ptrGrabResult->GetBuffer();
            cout << "Gray value of first pixel: " << (uint32_t) pImageBuffer[0] << endl << endl;
			int imgSize = ptrGrabResult->GetImageSize();
			memcpy(pImgBuf->imageData, ptrGrabResult->GetBuffer(), ptrGrabResult->GetImageSize());
		}

		//pCam->StopGrabbing();

	}
	catch (GenICam::GenericException &e)
	{
		// Error handling.
		cerr << "An exception occurred." << endl
			<< e.GetDescription() << endl;
		exitCode = 1;
	}
	return 0;
}


void CVisionInspection::OnBnClickedBtnGrab()
{
	if (camConnected == CAM_CONNECTED)
	{
		if (!pylonGrab()){
			::SendMessage(this->m_hWnd, WM_PAINT, 0, 0);
		}
		else{//Error

		}
	}
	else{
		AfxMessageBox(_T("Camera Disconnected."), MB_ICONHAND);
		return;
	
	}

}


void CVisionInspection::drawImage()
{

	if(pImgBuf)
	{
		CDC* pDC;
		CRect rect;
		pDC = m_pic.GetDC();
		m_pic.GetClientRect(&rect);

		DisplayIplImg(pImgBuf, pDC, rect);
		ReleaseDC(pDC);
	}
}


void CVisionInspection::drawRect()
{
	CDC* pDC;
	CRect rect;

	pDC = m_pic.GetDC();
	m_pic.GetClientRect(&rect);

	CPen pen;
	pen.CreatePen( PS_SOLID, 1, RGB(255,0,0) );    // 빨간색 펜 생성
	CPen* oldPen = pDC->SelectObject( &pen );

	int x1, y1, x2, y2;

	x1 = (int)(points[0]*rect.Width()/LIVE_SIZE_W);
	y1 = (int)(points[1]*rect.Height()/LIVE_SIZE_H);
	x2 = (int)(points[2]*rect.Width()/LIVE_SIZE_W);
	y2 = (int)(points[3]*rect.Height()/LIVE_SIZE_H);
	//pDC->Rectangle(x1, y1, x2, y2);
	pDC->MoveTo(x1, y1);
	pDC->LineTo(x2, y1); pDC->LineTo(x2, y2);pDC->LineTo(x1, y2);pDC->LineTo(x1, y1);

	pDC->SelectObject( oldPen );

	ReleaseDC(pDC);

	return;
}


afx_msg LRESULT CVisionInspection::OnUmVi(WPARAM wParam, LPARAM lParam)
{
	CString strSavePath;
	CTime ctCurrentTime;

	if(wParam == UW_GRAB)
	{
		if(!pylonGrabSWTrigger()){
			//COPY IMAGE DATA
			memcpy(G_MainWnd->m_ServerDlg.pImgBuf[0], pImgBuf, ptrGrabResult->GetImageSize());
			::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT,0, 0);
		}else{//Error

		}
	}
	else if(wParam == UW_VINSPECTION)
	{
		SetUMMsgResponse(UW_VINSPECTION, UM_VI_ONGOING);
		if(!pylonGrab()){
			//COPY IMAGE DATA
			G_MainWnd->m_ServerDlg.paintImageReady[0]=1; 
			cvCopy(pImgBuf, G_MainWnd->m_ServerDlg.pImgBuf[0]);
			::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT,0, 0);

			//Save Image
			if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] == 2){ // 2:Save All
				ctCurrentTime = CTime::GetCurrentTime();
#ifdef _FI
				strSavePath.Format(L"%s\\%s\\%s_%s.jpg",
					G_MainWnd->m_DataHandling.m_chImageSavePath,
					G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
					ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
					//0 /*getData.nCamNum*/,
					G_MainWnd->m_LabelPrint.getSerialNo() /*Serial No.*/
					);
#else
				strSavePath.Format(L"%s\\%s\\%s.jpg",
					G_MainWnd->m_DataHandling.m_chImageSavePath,
					G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
					ctCurrentTime.Format(L"%Y%m%d%H%M%S")
					//,0 /*getData.nCamNum*/
					);
#endif
				cvSaveImage(ConvertUnicodeToMultybyte(strSavePath.GetBuffer()), pImgBuf);
				strSavePath.ReleaseBuffer();
			}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] == 2){

			//## Vision Inspection ##//
			bool bFoundDefect = DoVisionInspection();
			nPass = bFoundDefect ? 0 : 1;
			if (bFoundDefect) {

				::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);
				//Save NG Image
				if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){ // 0: No Save
					ctCurrentTime = CTime::GetCurrentTime();
#ifdef _FI
					strSavePath.Format(L"%s\\%s\\%s_%s.jpg",
						G_MainWnd->m_DataHandling.m_chImageSaveNGPath,
						G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
						ctCurrentTime.Format(L"%Y%m%d%H%M%S"),
						//0 /*getData.nCamNum*/,
						G_MainWnd->m_LabelPrint.getSerialNo() /*Serial No.*/
						);
#else
					strSavePath.Format(L"%s\\%s\\%s.jpg",
						G_MainWnd->m_DataHandling.m_chImageSaveNGPath,
						G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel,
						ctCurrentTime.Format(L"%Y%m%d%H%M%S")
						//,0 /*getData.nCamNum*/
						);
#endif
					cvSaveImage(ConvertUnicodeToMultybyte(strSavePath.GetBuffer()), pImgBuf);
					strSavePath.ReleaseBuffer();
				}//if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_IMAGE_SAVE_SKIP] > 0){

			}

			SetUMMsgResponse(UW_VINSPECTION, UM_VI_DONE);

		}else{//Error
			//1117_kys
#ifndef _FI
			if ((G_MainWnd->m_InspectThread.stShuttleBatch.allow & 0x0A) == 0x0A)
#endif			
				SetUMMsgResponse(UW_VINSPECTION, UM_VI_ERROR);
		}

	}
	else if (wParam == UW_VINSPECTION_G)
	{
		SetUMMsgResponse(UW_VINSPECTION_G, UM_VI_ONGOING);
		if (!pylonGrab()){
			//COPY IMAGE DATA
			G_MainWnd->m_ServerDlg.paintImageReady[0] = 1;
			cvCopy(pImgBuf, G_MainWnd->m_ServerDlg.pImgBuf[0]);

			::SendMessage(G_MainWnd->m_ServerDlg.m_hWnd, WM_PAINT, 0, 0);

			//## Vision Inspection ##//



			long param = STG_VI_UNLOAD * 0x100 + 0;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
			SetUMMsgResponse(UW_VINSPECTION_G, UM_VI_CLEAR);

		}
		else{//Error
#ifndef _FI
			//1117_kys
			if ((G_MainWnd->m_InspectThread.stShuttleBatch.allow & 0x0A) == 0x0A)
#endif
			SetUMMsgResponse(UW_VINSPECTION_G, UM_VI_ERROR);
		}

	}
	return 0;

}


void CVisionInspection::SetUMMsgResponse(int msg, int rsp)
{
	msgRsp.RSP[msg] = rsp;
}


int CVisionInspection::GetUMMsgResponse(int msg)
{
	int rsp;
	rsp = msgRsp.RSP[msg];
	return rsp;
}

bool CVisionInspection::LoadNIPJob()
{
	wstring strJobFile = VISION_FOLDER;
	strJobFile += L"VI_LDC.nip";

	m_dJob.Clear();

	NIPO *pNIPO = NIPO::GetInstance();
	if (!pNIPO->LoadJob(strJobFile, m_dJob)) {

		return false;
	}

	return true;
}

bool CVisionInspection::DoVisionInspection()
{
	LoadNIPJob();

	bool bFoundDefect = false;
	Mat dImg = cvarrToMat(pImgBuf);

	wstring strModel = G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel;
	wstring strVIData = VISION_FOLDER;
	strVIData += strModel + L".vid";

	Mat dCalibrationImg;
	DoVisionInspectionProcess(L"LDC Calibration", VISION_CALIBRATION_DATA_PATH, dImg, dCalibrationImg, bFoundDefect);

	Mat dOutImg;
	DoVisionInspectionProcess(L"LDC Terminal", strVIData, dCalibrationImg, dOutImg, bFoundDefect);
	DoVisionInspectionProcess(L"LDC Tube", strVIData, dCalibrationImg, dOutImg, bFoundDefect);
	DoVisionInspectionProcess(L"LDC Guide", strVIData, dCalibrationImg, dOutImg, bFoundDefect);
	DoVisionInspectionProcess(L"LDC Paper", strVIData, dCalibrationImg, dOutImg, bFoundDefect);
	DoVisionInspectionProcess(L"LDC Clip", strVIData, dCalibrationImg, dOutImg, bFoundDefect);
	DoVisionInspectionProcess(L"LDC Bolt", strVIData, dCalibrationImg, dOutImg, bFoundDefect);

	NIPO *pNIPO = NIPO::GetInstance();
	pNIPO->SaveImage(L"LDC_Orig.bmp", dImg);
	pNIPO->SaveImage(L"LDC_Calibration.bmp", dCalibrationImg);

	if (bFoundDefect) {
		pNIPO->SaveImage(L"LDC_Defect.bmp", dOutImg);

		IplImage dIplOutImg = IplImage(dOutImg);
		cvCopy(&dIplOutImg, G_MainWnd->m_ServerDlg.pImgBuf[0]);
		cvCopy(&dIplOutImg, pImgBuf);
	}

	return bFoundDefect;
}

void CVisionInspection::DoVisionInspectionProcess(wstring strProcessName, wstring strVIData, Mat dImg, Mat &dOutImg, bool &bFoundDefect)
{
	NIPO *pNIPO = NIPO::GetInstance();
	NIPJobProcess *pProcess = m_dJob.FindProcess(strProcessName);
	if (pProcess) {
		NIPLParam *pParam = pNIPO->SetNIPLParam(pProcess);
		if (CHECK_STRING(strProcessName, L"LDC Calibration")) ((NIPLParam_LDC_Calibration *)pParam)->m_strDataPath = strVIData;
		else ((NIPLParam_LDC *)pParam)->m_strDataPath = strVIData;
			
		NIPLInput dInput;
		NIPLOutput dOutput;

		dInput.m_dImg = dImg;
		dInput.m_pParam = pParam;
		NIPL_ERR nErr = pNIPO->DoNIPLProcess(strProcessName, &dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			if (CHECK_STRING(strProcessName, L"LDC Calibration")) {
				dOutImg = dOutput.m_dImg;
			}
			else {
				if (dOutput.m_pResult != nullptr) {
					if (!bFoundDefect) {
						bFoundDefect = true;
						dImg.copyTo(dOutImg);
					}

					NIPLResult_Defect_LDC *pResult = (NIPLResult_Defect_LDC *)dOutput.m_pResult;
					for (auto &dDefect : pResult->m_listDefect) {
						rectangle(dOutImg, dDefect.m_rcBoundingBox, CV_RGB(255, 0, 0), 3);
					}
				}
			}
		}
		else if (!NIPL_PASS(nErr)) {
			wchar_t szText[256];
			wsprintf(szText, L"%s 비전 검사를 수행하지 못하였습니다. 에러 코드 : %d", strProcessName.c_str(), nErr);
			G_AddLog(3, szText);
			//AfxMessageBox(szText);
		}

		delete pParam;
	}
}

void CVisionInspection::OnBnClickedBtnVisionInspection()
{
	// TODO: Add your control notification handler code here
	wstring strModel = G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel;
	wstring strImagePath = L"D:\\nBizSystem\\Vision\\ModelSample\\";
	strImagePath += strModel + L".jpg";

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (!pNIPO->LoadImage(strImagePath, dImg)) {
		AfxMessageBox(L"Cannot load test image");
		return;
	}

	IplImage dIplImg = IplImage(dImg);
	cvCopy(&dIplImg, pImgBuf);
	DoVisionInspection();
	
}
