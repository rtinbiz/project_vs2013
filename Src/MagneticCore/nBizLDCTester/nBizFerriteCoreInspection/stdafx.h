
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once
#pragma warning (disable : 4005)
#pragma warning (disable:4819)
#pragma warning (disable:4996)

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"
#include "SequenceManager.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.

#include <afxcview.h>
#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include <afxsock.h>            // MFC 소켓 확장

/*
#include "opencv/cv.h"
#include "opencv/cxcore.h"
#include "opencv/highgui.h"
*/

#include "NIPO.h"
#pragma comment (lib, "NIPO.lib")


#ifdef _UNICODE
#define tstring std::wstring
#else
#define tstring std::wstring
#endif


extern void G_AddDayReportLog(char* chPanelID, ...);
extern void G_AddLog(int nLogLevel, TCHAR* chLog, ...);
extern void G_AlarmListAdd(int nAlarmID, char* pAlarmData, int nAlarmCD = 2);

#define ON 1
#define OFF 0

enum
{
	SYSTEM_AUTO = 1000,
	SYSTEM_MANUAL = 1001,
	SYSTEM_AUTO_RUN,

	SYSTEM_BLUE,
	SYSTEM_INIT_END,

	SYSTEM_ERROR,
	SYSTEM_OK,
	SYSTEM_EXIT,
};


struct G_SystemModeStruct {	
	unsigned short unSystemMode;
	unsigned short unSystemInit;
	unsigned short unSystemError;
	unsigned short unLensIndex;
	unsigned short unLensIndexCMD;	
	unsigned short unUIMode;

	G_SystemModeStruct()
	{
		memset(this, 0x00, sizeof(G_SystemModeStruct));
	}
};

extern G_SystemModeStruct G_SystemModeData;

#define DIALOG_ALARM_COLOR			RGB(235,0,0)
#define DIALOG_BG_COLOR				RGB(60, 60, 60)		//RGB(235,235,235)
#define STATIC_BG_COLOR				RGB(50, 50, 50)		// RGB(225,225,225)
#define STATIC_TX_COLOR				RGB(180,180,180)	//RGB(30,30,145)
#define DIALOG_INIT_COLOR			RGB(153, 255,255)	//RGB(153, 153, 153)
#define ENABLE_UI_COLOR				RGB(80, 80, 150)
#define DISABLE_UI_COLOR			RGB(180, 180, 180)
#define ENABLE_UI_FONT				WHITE
#define DISABLE_UI_FONT				RGB(30,30,145)

#define QRCODE_LENGTH_MIN		10

#define _FI		//Final Inspection
//#define AI1		//TEST WITHOUT IO
//#define LCR_HOME  // LCR HOME AFTER MEASUREMENT FINISHED FOR STEP MOTOR POSITION
//#define _VI_SKIP
#define DEV_MODE

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#ifdef _FI
#include "vld.h"
#endif

char chResult[1024];
wchar_t wcResult[1024];

char* ConvertUnicodeToMultybyte(CString strUnicode);
wchar_t* ConvertMultybyteToUnicode(const char *chText);

void DisplayIplImg(IplImage* pImgIpl, CDC* pDC, CRect rect);

#pragma comment (lib, "NIPLGUI.lib")
#include "NIPLGUI.h"

