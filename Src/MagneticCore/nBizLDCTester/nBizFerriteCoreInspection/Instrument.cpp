#include "stdafx.h"
#include "Instrument.h"
#include "nBizLDCTester.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(CInstrument, CDialogEx)

extern CMainWnd	*G_MainWnd;

CInstrument::CInstrument(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInstrument::IDD, pParent)
{

}

CInstrument::~CInstrument()
{
}

void CInstrument::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_INSTRU_VISA, m_GridInstruVisa);
	DDX_Control(pDX, IDC_GRID_LCR_CORRECTION, m_GridLCRCorret);
	DDX_Control(pDX, IDC_BUTTON2, m_btnResetVILVR);
	DDX_Control(pDX, IDC_BUTTON4, m_btnResetVIOSC);
	DDX_Control(pDX, IDC_BUTTON8, m_btnResetVIDCR);
	DDX_Control(pDX, IDC_BUTTON9, m_btnResetVIHIPOT);
	DDX_Control(pDX, IDC_BUTTON10, m_btnResetVIIR);
	DDX_Control(pDX, IDC_BUTTON11, m_btnResetVIWG);
}

BEGIN_MESSAGE_MAP(CInstrument, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CInstrument::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BTN_LCR_REF_SAVE, &CInstrument::OnBnClickedBtnLcrRefSave)
	ON_BN_CLICKED(IDC_LCR_ADJUST, &CInstrument::OnBnClickedLcrAdjust)
	ON_COMMAND_RANGE(IDC_BUTTON2, IDC_BUTTON11, &CInstrument::OnBnClickedVIReset)
	ON_MESSAGE(UM_INSTRUMENT, &CInstrument::OnUmInstrument)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CInstrument::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	initGridInstrument();
	readGridInstrumentData();
	initGridLCRCorrect();
	readGridLCRCorrectRef();
	return TRUE;  // return TRUE unless you set the focus to a control
}


void CInstrument::m_fnInit()
{	
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	for(int i = VI_LCR; i <= VI_WG; i++){

#ifndef _FI
		if(i != VI_LCR && i != VI_DCR)
		{
			continue;
		}
#endif

		if(i == VI_IR) //TOS7200: Serial SP_COMPORT_4
		{
			if(G_MainWnd->m_SerialInterface.m_CommThread[COM_TOS7200].m_bConnected)
				G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
		}
		else{
			if(open(i)== VI_SUCCESS) 
				G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
		}
	}

	//Timer
	//SetTimer(WM_TIMER_INSTRUMENT, 1000, NULL);	//1 SEC
}

void CInstrument::m_fnDeInit()
{	
	for(int i = VI_LCR; i <= VI_WG; i++){
#ifndef _FI
		if(i != VI_LCR && i != VI_DCR ) continue;
#endif
		if(i != VI_IR) //TOS7200: Serial SP_COMPORT_4
		if(vi[i].connected == 1) 
			visaClose(i);
	}
}

void CInstrument::checkInstrumentComm()
{	
	CString str;
	int nRet;

	for(int i = VI_LCR; i <= VI_WG; i++){
#ifndef _FI
	if(!(i == VI_LCR || i == VI_DCR)) continue;
#endif
		if(i !=VI_IR)
		{
			if(initialize(i)!= VI_SUCCESS){
			if(open(i)== VI_SUCCESS) 
				G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
			}else{
			G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(RGB(230,230,230));
		}
		}else{
			str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*IDN?");
			nRet = str.Find( L"TOS");
			if(nRet > 0){
				G_AddLog(3,L"%s %s", ST_INSTRUMENT[i], str.GetBuffer()); str.ReleaseBuffer();
				G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
			}else{
				vi[i].connected = 0;
				G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(RGB(230,230,230));
			}
		}
	}
}

//E4980 Comm Test


////////////////////////////////////////////////////////////////////////////////////////////////////
// 계측기 통신 

//ViSession vi

//////////////////////////////////////////////////////////////////////////////////////////////////////
// 1.E4980

ViStatus retErr;

int CInstrument::initGridInstrument(void)
{
	try {
		m_GridInstruVisa.SetRowCount(8);
		m_GridInstruVisa.SetColumnCount(3);
		m_GridInstruVisa.SetFixedRowCount(1);		
		m_GridInstruVisa.SetFixedColumnCount(1);
		m_GridInstruVisa.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridInstruVisa.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridInstruVisa.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("    I T E M    "));					
				}
				else if (col == 2)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                           V A L U E                             "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"%d", row);					
					}
				}
				if(col == 1)
				{
					if(row >= 1 && row <= NUM_INSTRUMENTS)
					{
						Item.strText.Format(L"%s", ST_INSTRUMENT[row-1]);					
					}
				}
			}
			m_GridInstruVisa.SetItem(&Item);
		}
	}

	m_GridInstruVisa.AutoSize(GVS_BOTH);
	m_GridInstruVisa.SetTextColor(RGB(0, 0, 105));
	m_GridInstruVisa.AutoSizeColumns();
	m_GridInstruVisa.ExpandToFit(true);
	//m_GridInstruVisa.SetEditable(FALSE);
	m_GridInstruVisa.Refresh();

		//m_GridInstruVisa.GetCell(1, 4)->SetBackClr(RGB(0xc0, 0xc0, 0xa0));

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//

/* open the instrument.
* --------------------------------------------------------------- */
ViStatus CInstrument::open(int viIdx) //
{
		/* Open the default resource manager session. */
		vi[viIdx].err = viOpenDefaultRM(&vi[viIdx].defaultRM);
		if (vi[viIdx].err != VI_SUCCESS) { vi[viIdx].connected = 0; return error_handler(viIdx); }
		/* Open the session using the oscilloscope's VISA address. */
		//G_AddLog(3,L"vi[%d] %s",viIdx, vi[viIdx].resource);
		vi[viIdx].err = viOpen(vi[viIdx].defaultRM, vi[viIdx].str_resource, VI_NULL, VI_NULL, &vi[viIdx].vi);
		if (vi[viIdx].err != VI_SUCCESS) { vi[viIdx].connected = 0; return error_handler(viIdx); }
		vi[viIdx].connected = 1;
		G_AddLog(3,L"%s Connected", ST_INSTRUMENT[viIdx]);
	//if(viIdx == VI_IR) //TOS7200: Serial SP_COMPORT_4
	//{
	//	ret = viSetAttribute(vi[viIdx].vi, VI_ATTR_ASRL_PARITY,  VI_ASRL_PAR_NONE );
	//	ret = viSetAttribute(vi[viIdx].vi, VI_ATTR_ASRL_DATA_BITS, 8 );
	//	ret = viSetAttribute(vi[viIdx].vi, VI_ATTR_ASRL_STOP_BITS, VI_ASRL_STOP_TWO );
	//	ret = viSetAttribute(vi[viIdx].vi, VI_ATTR_ASRL_FLOW_CNTRL, VI_ASRL_FLOW_XON_XOFF );
	//}

	return VI_SUCCESS;
}

/* --------------------------------------------------------------- */
ViStatus CInstrument::initialize (int viIdx)
{
	ViStatus ret= 0;
	/* Clear the interface. */
	//vi[viIdx].err = viClear(vi[viIdx].vi);
	//if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	/* Get and display the device's *IDN? string. */
	vi[viIdx].err = do_query_string(vi[viIdx].vi,"*IDN?"); // fail when time out error
	if (vi[viIdx].err != VI_SUCCESS) { 
		vi[viIdx].connected = 0;
		return error_handler(viIdx); }
	G_AddLog(3,L"vi[%d]: %s\n", viIdx, vi[viIdx].str_result);
	/* Clear status */
	vi[viIdx].err = do_command(vi[viIdx].vi,"*CLS");
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	//
	//do_command(vi,"*RST"); //makes factory default
	return vi[viIdx].err;
}

/* close the instrument.
* --------------------------------------------------------------- */
ViStatus CInstrument::visaClose(int viIdx) //
{
	/* Close the vi session and the resource manager session. */
	viClose(vi[viIdx].vi);
	viClose(vi[viIdx].defaultRM);
	return 0;
}

/* Send a command to the instrument.
* --------------------------------------------------------------- */
ViStatus CInstrument::do_command(int viIdx, char *command)
{
	char message[80];
	strcpy_s(message, command);
	strcat_s(message,"\n");
	vi[viIdx].err = viPrintf(vi[viIdx].vi, message);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	//check_instrument_errors(vi[viIdx].vi);
	return VI_SUCCESS;
}
/* Command with IEEE definite-length block.
/* --------------------------------------------------------------- */

/* --------------------------------------------------------------- */
ViStatus CInstrument::do_query_string(int viIdx, char *query)
{
	char message[80];
	strcpy_s(message, query);
	strcat_s(message,"\n");
	vi[viIdx].err = viPrintf(vi[viIdx].vi, message);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	Sleep(100);
	memset(vi[viIdx].str_result, 0x00, 256 * sizeof(char));
	vi[viIdx].err = viScanf(vi[viIdx].vi,"%t", vi[viIdx].str_result);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	//return check_instrument_errors(vi[viIdx].vi);
	return vi[viIdx].err;
}
/* Query for a number result.
/* --------------------------------------------------------------- */
ViStatus CInstrument::do_query_number(int viIdx, char *query)
{
	char message[80];
	strcpy_s(message, query);
	strcat_s(message,"\n");
	vi[viIdx].err = viPrintf(vi[viIdx].vi, message);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	vi[viIdx].err = viScanf(vi[viIdx].vi,"%lf", &vi[viIdx].num_result);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	//return check_instrument_errors(vi[viIdx].vi);
	return vi[viIdx].err;
}
/* Query for numbers result.
/* --------------------------------------------------------------- */
ViStatus CInstrument::do_query_numbers(int viIdx, char *query)
{
	char message[80];
	strcpy_s(message, query);
	strcat_s(message,"\n");
	vi[viIdx].err = viPrintf(vi[viIdx].vi, message);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	vi[viIdx].err = viScanf(vi[viIdx].vi,"%,10lf\n", vi[viIdx].dbl_results);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	return check_instrument_errors(vi[viIdx].vi);
}
/* Query for an IEEE definite-length block result.
/* --------------------------------------------------------------- */
int CInstrument::do_query_ieeeblock(int viIdx, char *query)
{
	char message[80];
	int data_length;
	strcpy_s(message, query);
	strcat_s(message,"\n");
	ViStatus err;
	vi[viIdx].err = viPrintf(vi[viIdx].vi, message);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	data_length = IEEEBLOCK_SPACE;
	vi[viIdx].err = viScanf(vi[viIdx].vi,"%#b\n", &data_length, vi[viIdx].ieeeblock_data);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	if (data_length == IEEEBLOCK_SPACE )
	{
		printf("IEEE block buffer full: ");
		printf("May not have received all data.\n");
	}
	if(err=check_instrument_errors(vi[viIdx].vi) != VI_SUCCESS)
	{
		return err;
	}
	return(data_length);
}
/* Check for instrument errors.
/* --------------------------------------------------------------- */
ViStatus CInstrument::check_instrument_errors(int viIdx)
{
	char str_err_val[256] = {0};
	char str_out[800] = "";
	vi[viIdx].err = viQueryf(vi[viIdx].vi,":SYSTem:ERRor?\n","%t", str_err_val);
	if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	while(strncmp(str_err_val, "+0,No error", 3) != 0 )
	{
		strcat_s(str_out,",");
		strcat_s(str_out, str_err_val);
		vi[viIdx].err = viQueryf(vi[viIdx].vi, ":SYSTem:ERRor?\n","%t", str_err_val);
		if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
	}
	if (strcmp(str_out,"") != 0)
	{
		printf("INST Error%s\n", str_out);
		G_AddLog(3, L"OSC INST Error%s\n", str_out);
		vi[viIdx].err = viFlush(vi[viIdx].vi, VI_READ_BUF);
		if (vi[viIdx].err != VI_SUCCESS) { return error_handler(viIdx); }
		vi[viIdx].err = viFlush(vi[viIdx].vi, VI_WRITE_BUF);
		if (vi[viIdx].err != VI_SUCCESS){return error_handler(viIdx);}
	}
	return VI_SUCCESS;
}
/* Handle VISA errors.
/* --------------------------------------------------------------- */
ViStatus CInstrument::error_handler(int viIdx)
{
	wchar_t werr_msg[1024] = {0};
	viStatusDesc(vi[viIdx].vi, vi[viIdx].err, vi[viIdx].err_msg);

	if(!strncmp(vi[viIdx].err_msg,"VI_ERROR_RSRC_NFOUND",20))vi[viIdx].connected = 0;
	if(!strncmp(vi[viIdx].err_msg,"VI_ERROR_RSRC_BUSY",18))vi[viIdx].connected = 0;
	if(!strncmp(vi[viIdx].err_msg,"VI_ERROR_TMO",12))vi[viIdx].connected = 0;
	if(vi[viIdx].connected == 0)
		G_MainWnd->m_ServerDlg.m_statusInstrument[viIdx].SetBackColor(RGB(230,230,230));
	MultiByteToWideChar(CP_ACP,0, vi[viIdx].err_msg, strlen(vi[viIdx].err_msg), (LPWSTR)werr_msg,1024);
	G_AddLog(3,L"vi[%d] %s: %s\n", viIdx, ST_INSTRUMENT[viIdx], werr_msg);
	if (vi[viIdx].err < VI_SUCCESS)
	{

		//do_command(VI_DCR, "*IDN?");   //1220_kys
		//do_command(VI_DCR, "*IDN?");
		return vi[viIdx].err;
	}
	return vi[viIdx].err;
}

int CInstrument::readGridInstrumentData(void)
{
	BOOL	bUse= FALSE;
	CString strValueData; 	
	GV_ITEM Item;	
	Item.col = 2;

	int nSize = sizeof(ST_TABLE_INSTRUMENT_RESOURCE) / 8 + 1;

	for (int row = 1; row < nSize; row++)											
	{ 		
		strValueData = G_MainWnd->m_DataHandling.m_fnReadIniFile(SYSTEM_PARAM, SECTION_INSTRUMENT_RESOURCE, ST_TABLE_INSTRUMENT_RESOURCE[row-1]);
		m_GridInstruVisa.SetItemText(row, Item.col, strValueData);


		memcpy(vi[row-1].str_resource, ConvertUnicodeToMultybyte(strValueData), strValueData.GetLength());

		//memcpy(vi[row-1].str_resource, strValueData.GetBuffer(strValueData.GetLength()), strValueData.GetLength());
		strValueData.ReleaseBuffer();
		//G_AddLog(3,L"vi[%d].resource: %s", row-1, vi[row-1].str_resource);
	}
	return 0;
}


void CInstrument::OnBnClickedButton1()
{
	CString strValueData;
	GV_ITEM Item;
	Item.col = 2;
	for (int row = 1; row < m_GridInstruVisa.GetRowCount(); row++)
	{
		strValueData = m_GridInstruVisa.GetItemText(row, Item.col);
		if(strValueData == "")
			break;
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_INSTRUMENT_RESOURCE, ST_TABLE_INSTRUMENT_RESOURCE[row-1], strValueData);		
	}
	readGridInstrumentData();
}


ViStatus CInstrument::measureLcr(int step, LCR_TEST_INFO lcrTestInfo )
{
	ViStatus ret= 0;
	int nCLS = 0;

	//SETTING : query and set 
	char strbuff[50];

	//sprintf_s(strbuff,"*RST");
	//ret = do_command(VI_LCR, strbuff);
	//if(ret != VI_SUCCESS) { return ret; }

	// LsRs ?
	do{
		ret = do_command(VI_LCR,"*CLS");
		if(nCLS++ > 3){return ret;}
	}while(ret != VI_SUCCESS);

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"FUNC:IMP LSRS");
	ret = do_command(VI_LCR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }

	if(lcrTestInfo.kHz > 0 && 1)
	{
		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,"FREQ %f", lcrTestInfo.kHz*1000);
		ret = do_command(VI_LCR, strbuff); 
		if(ret != VI_SUCCESS) { return ret; }
	}
	if(lcrTestInfo.Vac > 0 && 1)
	{
		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,"VOLT %f", lcrTestInfo.Vac);
		ret = do_command(VI_LCR, strbuff);
		if(ret != VI_SUCCESS) { return ret; }
	}

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"AMPL:ALC ON");
	ret = do_command(VI_LCR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }

	//if(lcrTestInfo.Vdc > 0 && 1)
	//{
	//	memset(strbuff, 0x00, 50*sizeof(char));
	//	sprintf_s(strbuff,"VOLT %f", lcrTestInfo.Vdc);
	//	ret = do_command(VI_LCR, strbuff);
	//	if(ret != VI_SUCCESS) { return ret; }
	//}

	//DC Amp bias *******
	if(lcrTestInfo.Ibias > 0 && 1)
	{
		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,":APERture LONG");
		ret = do_command(VI_LCR, strbuff);
		if(ret != VI_SUCCESS) { return ret; }

		//memset(strbuff, 0x00, 50*sizeof(char));
		//sprintf_s(strbuff,"BIAS:CURRent MAX %f", lcrTestInfo.Ibias * 1.2f);
		//ret = do_command(VI_LCR, strbuff);
		//if(ret != VI_SUCCESS) { return ret; }

		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,"BIAS:CURRent %f", lcrTestInfo.Ibias);
		ret = do_command(VI_LCR, strbuff);
		if(ret != VI_SUCCESS) { return ret; }

		sprintf_s(strbuff,"INIT:CONT ON;:TRIG:SOUR BUS");
		ret = do_command(VI_LCR, strbuff);
		if(ret != VI_SUCCESS) { return ret; }

		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,"BIAS:STATe ON");
		ret = do_command(VI_LCR, strbuff);
		if(ret != VI_SUCCESS) { goto TURNOFFDCBIAS; }
		Sleep(1000);
		ret = do_query_string(VI_LCR,"*TRG");
		if(ret != VI_SUCCESS) { goto TURNOFFDCBIAS; }

	}else{
		//TEST
		ret = do_command(VI_LCR,"INIT:IMM");
		if(ret != VI_SUCCESS) { goto TURNOFFDCBIAS; }
		ret = do_command(VI_LCR,"TRIG:IMM");
		if(ret != VI_SUCCESS) { goto TURNOFFDCBIAS; }
		ret = do_query_string(VI_LCR,"FETCh?");
	}

	if(ret != VI_SUCCESS) {
TURNOFFDCBIAS:
		sprintf_s(strbuff,"BIAS:STATe OFF");
		//ret = do_command(VI_LCR, strbuff);
		do_command(VI_LCR, strbuff);
		return ret; 
	}
	//Result
	double l = atof(vi[VI_LCR].str_result);

	sprintf_s(strbuff,"BIAS:STATe OFF");
	ret = do_command(VI_LCR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }

	return ret;
}

ViStatus CInstrument::correctLcr(int step, LCR_TEST_INFO lcrTestInfo )
{
	ViStatus ret = 0;
	int nCLS = 0;

	//SETTING : query and set 
	char strbuff[50];

	//sprintf_s(strbuff,"*RST");
	//ret = do_command(VI_LCR, strbuff);
	//if(ret != VI_SUCCESS) { return ret; }

	// LsRs ?
	do{
		ret = do_command(VI_LCR, "*CLS");
		if (nCLS++ > 3){ return ret; }
	} while (ret != VI_SUCCESS);

	memset(strbuff, 0x00, 50 * sizeof(char));
	sprintf_s(strbuff, "FUNC:IMP LSRS");
	ret = do_command(VI_LCR, strbuff);
	if (ret != VI_SUCCESS) { return ret; }

	if (lcrTestInfo.kHz > 0 && 1)
	{
		memset(strbuff, 0x00, 50 * sizeof(char));
		sprintf_s(strbuff, "FREQ %f", lcrTestInfo.kHz * 1000);
		ret = do_command(VI_LCR, strbuff);
		if (ret != VI_SUCCESS) { return ret; }
	}
	if (lcrTestInfo.Vac > 0 && 1)
	{
		memset(strbuff, 0x00, 50 * sizeof(char));
		sprintf_s(strbuff, "VOLT %f", lcrTestInfo.Vac);
		ret = do_command(VI_LCR, strbuff);
		if (ret != VI_SUCCESS) { return ret; }
	}

	memset(strbuff, 0x00, 50 * sizeof(char));
	sprintf_s(strbuff, "AMPL:ALC ON");
	ret = do_command(VI_LCR, strbuff);
	if (ret != VI_SUCCESS) { return ret; }

	//LOAD CORRECTION
	//ret += do_command(VI_LCR,":CORRection:LOAD:TYPE LSRS");
	//ret += do_command(VI_LCR,":CORRection:METHod SINGle");
	//memset(strbuff, 0x00, 50*sizeof(char));
	//sprintf_s(strbuff,":CORRection:SPOT[%d]:FREQuency %d",step+1, lcrTestInfo.kHz*1000); //Hz 20 to 2M
	//ret += do_command(VI_LCR, strbuff);
	//memset(strbuff, 0x00, 50*sizeof(char));
	//sprintf_s(strbuff,":CORRection:SPOT[%d]:LOAD:STANdard %g, %g",step+1, G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refPrimary, G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refSecondary); //Hz 20 to 2M
	//ret += do_command(VI_LCR, strbuff);
	//memset(strbuff, 0x00, 50*sizeof(char));
	//sprintf_s(strbuff,":CORRection:SPOT[%d]:LOAD:EXECute",step+1);
	//ret += do_command(VI_LCR, strbuff);

	//SHORT CORRECTION
	ret = do_command(VI_LCR, ":CORRection:LOAD:TYPE LSRS");		if (ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_LCR, ":CORRection:METHod SINGle");		if (ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_LCR, ":CORRection:SHORt:EXECute");		if (ret != VI_SUCCESS) { return ret; }
	//error status check
	ret = do_query_string(VI_LCR, ":SYSTem:ERRor?");		if (ret != VI_SUCCESS) { return ret; }
	//
	//if(vi[VI_LCR].str_result == "0" or something
	// IF NO ERROR //###############################################//

	if (ret == VI_SUCCESS){
		ret = do_command(VI_LCR, ":CORRection:SHORt:STATe ON");	
		if (ret != VI_SUCCESS) { return ret; }
	}

	return ret;
}

ViStatus CInstrument::enbleCorrectionPoint(int spot, int state)
{
	ViStatus ret= 0;
	char strbuff[50];
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,":CORRection:SPOT[&d]:STATe &d",spot,state); 
	ret = do_command(VI_LCR, strbuff);
	return ret;
}

ViStatus CInstrument::measureOsc(int step, OSC_TEST_INFO oscTestInfo )
{
	ViStatus ret= 0;
	char strbuff[256];
	double Vacpp;
	double tolerance = 0.05; //5%
	int turnRatio = 1;
	int nCLS = 0;
	double OSC_PROBE_TOLERANCE;

	Vacpp = oscTestInfo.Vac;

	//OSC DISPLAY SETTING
	do{
		ret = do_command(VI_OSC,"*CLS");
		ret = do_command(VI_OSC,"*CLS");
		if(nCLS++ > 10){return ret;}
	}while(ret != VI_SUCCESS);
	Sleep(10);
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,":CHANnel1:SCALe %G", (Vacpp/2.0f)*OSC_PROBE_ATTENUATION);	
	ret = do_command(VI_OSC, strbuff);
	Sleep(10);
	sprintf_s(strbuff,":CHANnel1:OFFSet %G", Vacpp*OSC_PROBE_ATTENUATION);	
	ret = do_command(VI_OSC, strbuff);
	Sleep(10);
	sprintf_s(strbuff,":CHANnel2:SCALe %G", (Vacpp/(2.0f*oscTestInfo.turnRatio))*OSC_PROBE_ATTENUATION);	
	ret = do_command(VI_OSC, strbuff);
	Sleep(10);
	sprintf_s(strbuff,":CHANnel2:OFFSet %G", -(Vacpp/oscTestInfo.turnRatio)*OSC_PROBE_ATTENUATION);
	ret = do_command(VI_OSC, strbuff);
	Sleep(10);
	sprintf_s(strbuff,":TIMebase:SCALe %G", (1.0f/(oscTestInfo.kHz*1000.0f)));	
	ret = do_command(VI_OSC, strbuff);
	Sleep(10);
	//Waveform
	memset(strbuff, 0x00, 50*sizeof(char));
	nCLS = 0;
	do{
		ret = do_command(VI_WG,"*CLS");
		if(nCLS++ > 10){return ret;}
	}while(ret != VI_SUCCESS);
	Sleep(10);
	ret = do_command(VI_WG,"VOLT:UNIT VPP");
	Sleep(10);
	ret = do_command(VI_WG,"FUNC SIN");
	Sleep(10);
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"VOLTage %G", Vacpp);	
	ret = do_command(VI_WG, strbuff);
	//memset(strbuff, 0x00, 50*sizeof(char));
	Sleep(10);
	sprintf_s(strbuff,"FREQuency %G", oscTestInfo.kHz*1000);	
	ret = do_command(VI_WG, strbuff);
	Sleep(50);//1123_KYS_Modify
	if(ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_WG,"PHAS 0");
	Sleep(50);//1123_KYS_Modify
	if(ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_WG,"OUTP ON");
	if(ret != VI_SUCCESS) { return ret; }

	Sleep(50);
	//OSC TEST
	ret = do_query_string(VI_OSC,":MEASure:PHASe? CHANnel1, CHANnel2");
	Sleep(50);//1123_KYS_Add
	if(ret != VI_SUCCESS) { return ret; }
	double phase = atof(vi[VI_OSC].str_result);
	G_MainWnd->m_InspectThread.m_PartData[STG_OSC].oscPhase = phase;
	ret = do_query_string(VI_OSC,":MEASure:FREQuency? CHANnel2");
	Sleep(50);//1123_KYS_Add
	if(ret != VI_SUCCESS) { return ret; }
	double freq = atof(vi[VI_OSC].str_result); 
	G_MainWnd->m_InspectThread.m_PartData[STG_OSC].oscHz = freq;
	ret = do_query_string(VI_OSC,":MEASure:VPP? CHANnel2");
	Sleep(300);//1123_KYS_Add  50--->300
	if(ret != VI_SUCCESS) { return ret; }
	double vpp = atof(vi[VI_OSC].str_result);
	vpp /= OSC_PROBE_ATTENUATION;
	G_MainWnd->m_InspectThread.m_PartData[STG_OSC].oscV2 = vpp;
	Sleep(10);
	ret = do_command(VI_WG,"OUTP OFF");
	if(ret != VI_SUCCESS) { return ret; }
	//judge
	int judge;
	memset(strbuff, 0x00, 256*sizeof(char));
	sprintf_s(strbuff,"ph:%G f:%G V2:%G", phase, freq/1000.0f, vpp);

	OSC_PROBE_TOLERANCE = G_MainWnd->m_DataHandling.m_SystemParam.fParam[SP_OSC_TOLERANCE]*0.01f;

	if(360.0f*(-OSC_PROBE_TOLERANCE) <= phase && phase <= 360.0f*(OSC_PROBE_TOLERANCE) && 
		(oscTestInfo.kHz*1000*(1.0f-OSC_PROBE_TOLERANCE)) <= freq && freq <= (oscTestInfo.kHz*1000*(1.0f+OSC_PROBE_TOLERANCE)) && 
		(Vacpp*(1.0f-OSC_PROBE_TOLERANCE)* oscTestInfo.turnRatio ) <= vpp && (Vacpp*(1.0f+OSC_PROBE_TOLERANCE)* oscTestInfo.turnRatio ) >= vpp )
		//oscTestInfo.Vac*0.95f/* *n */<= vpp && oscTestInfo.Vac*1.05f /* *n */ <= 5.0f )//±권선비
	{
		sprintf_s(vi[VI_OSC].str_result,"OK %s", strbuff); judge=1;
		//sprintf_s(vi[VI_OSC].str_result,"OK"); judge=1;
	}else{
		sprintf_s(vi[VI_OSC].str_result,"NG %s", strbuff); judge=0;
		//sprintf_s(vi[VI_OSC].str_result,"NG"); judge=0;
	}

	return ret;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// 3.HIOKI RM3545 
//DC700V / 최소 5mΩ
ViStatus CInstrument::measureDcr(int step, DCR_TEST_INFO dcrTestInfo )
{
	
	//
	ViStatus ret;
	char strbuff[50];
	int nCLS = 0;
	//Set Measurement Conditions
	//do_command(":RES:RANG 1E+0");  //1ohm range
	//do_command(":SAMPle:RATE FAST");
	//do_command(":TRIG:SOUR EXT");
	//do_command(":CALC:LIM:MODE ABS");
	//do_command(":CALC:LIM:BEEP IN,0,0");
	//do_command(":CALC:LIM:BEEP HI,1,0");
	//do_command(":CALC:LIM:BEEP LO,1,0");
	//do_command(":CALC:LIM:UPP 1E+0");
	//do_command(":CALC:LIM:LOW 0.5E+0");
	//do_command(":CALC:LIM:STAT ON");

	//terminal multiplexer measurement
	do{
		ret = do_command(VI_DCR,"*CLS");
		if(nCLS++ > 10){return ret;}
	}while(ret != VI_SUCCESS);
	Sleep(5);

	// <= 1 Ohm
	ret = do_command(VI_DCR, "RES:RANG 1000.0000E-3"); // 
	if (ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_DCR, ":CALC:LIM:STAT ON"); 
	if (ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_DCR, ":CALC:LIM:MODE ABS"); 
	if (ret != VI_SUCCESS) { return ret; }
	ret = do_command(VI_DCR, ":CALC:LIM:UPP 1.0");
	if (ret != VI_SUCCESS) { return ret; }

	//1007_KYS_ADD_TEMP_CORRECT
	//ret= do_command(VI_DCR,":CALC:TCOR:PAR 23,3930");//23
	//if(ret!=VI_SUCCESS){return ret;}
	//ret= do_command(VI_DCR,":CALC:TCOR:STAT ON");
	//if(ret!=VI_SUCCESS){return ret;}

	ret= do_command(VI_DCR,"SCAN:MODE OFF");
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_command(VI_DCR,":TRIG:SOUR IMM"); //Select an internal triggering
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_command(VI_DCR,":INIT:CONT OFF");
	if(ret != VI_SUCCESS) { return ret; }

	//CHANNEL
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff, "CH %d", dcrTestInfo.ch1term1);
	ret = do_command(VI_DCR, strbuff);
	if (ret != VI_SUCCESS) { return ret; }
	sprintf_s(strbuff,"CH:STAT ON,%d", dcrTestInfo.ch1term1);
	ret= do_command(VI_DCR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	//Sleep(10);
	//for (int k = 0; k < 20; k++)
	//{
		ret = do_query_string(VI_DCR, ":READ?");
	//}
	//if (ret != VI_SUCCESS) { return ret; }
	//Sleep(1000);
	sprintf_s(strbuff, "CH:STAT OFF,%d", dcrTestInfo.ch1term1);
	ret = do_command(VI_DCR, strbuff);
	if (ret != VI_SUCCESS) { return ret; }

	/*if(ret != VI_SUCCESS) 
	{
		return ret; 
	}*/
	return ret;
}

int CInstrument::zeroAdjustDcr(int step, DCR_TEST_INFO dcrTestInfo )
{
	ViStatus ret= 0;
	char strbuff[50];

	//CHANNEL
	sprintf_s(strbuff,"CH %d", dcrTestInfo.ch1term1);
	ret= do_command(VI_DCR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_command(VI_DCR,":ADJust:ENABle ON");
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_query_string(VI_DCR,":ADJust?");
	if(ret != VI_SUCCESS) { return ret; }
	//Result
	int response = atoi(vi[VI_DCR].str_result); //0: succeded, 1: failed 
	return response;
}

ViStatus CInstrument::measureHipotStart(int step, HIPOT_TEST_INFO hipotTestInfo )
{
	ViStatus ret= 0;
	char strbuff[50];
	int stepNum;
	CString status;
	int i;
	int nCLS = 0;
	//Set Measurement Conditions
	do{
		//ret = do_command(VI_HIP,"*CLS");
		ret = do_command(VI_HIP,"*CLS");
		if(nCLS++ > 10){return ret;}
	}while(ret != VI_SUCCESS);

	ret= do_command(VI_HIP,":SOURce:SAFEty:STOP");  if(ret != VI_SUCCESS) { return ret; }
	ret= do_query_string(VI_HIP,":SOURce:SAFEty:SNUMBer?");	if(ret != VI_SUCCESS) { return ret; }
	stepNum =  atoi(vi[VI_HIP].str_result);
	for(i = 0; i < stepNum; i++)	{
		memset(strbuff, 0x00, 50*sizeof(char));
		sprintf_s(strbuff,":SOURce:SAFEty:STEP%d:DELete", i+1);
		ret= do_command(VI_HIP, strbuff);	if(ret != VI_SUCCESS) { return ret; }
	}

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,":SOURce:SAFEty:STEP1:AC:LEVel %d", static_cast<int>(hipotTestInfo.Vac)); //OVER 100V
	ret= do_command(VI_HIP, strbuff);	if(ret != VI_SUCCESS) { return ret; }
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,":SOURce:SAFEty:STEP1:AC:LIMit:HIGH %f", hipotTestInfo.upperLimit*(+1.0E-3)); //leakage current high limit (A) :+5.000000E-04 (DEFAULT)
	ret= do_command(VI_HIP, strbuff);	if(ret != VI_SUCCESS) { return ret; }
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,":SOURce:SAFEty:STEP1:AC:TIME:TEST %d", static_cast<int>(hipotTestInfo.sec)); //+3.000000E+00
	ret= do_command(VI_HIP, strbuff);	if(ret != VI_SUCCESS) { return ret; }

	ret= do_query_string(VI_HIP,":SOURce:SAFEty:SNUMBer?");	if(ret != VI_SUCCESS) { return ret; }
	stepNum =  atoi(vi[VI_HIP].str_result);

	//measurement
	if(stepNum == 1)
		ret= do_command(VI_HIP,":SOURce:SAFEty:STARt");
	else
		return stepNum;
	if(ret != VI_SUCCESS) { return ret; }
	return ret;
}

ViStatus CInstrument::measureHipotStatus(int step, HIPOT_TEST_INFO hipotTestInfo )
{	
	ViStatus ret= 0;
	CString status;

		ret = do_command(VI_HIP,"*CLS");
		ret= do_query_string(VI_HIP,":SOURce:SAFEty:STATUS?");
		if(ret != VI_SUCCESS) { return ret; }
		status =  vi[VI_HIP].str_result; //TEST, STOPPED
		int nCmp = strncmp(vi[VI_HIP].str_result,"STOPPED",7); 
		if(!nCmp){
			ret = 0;
		}else{
			ret = 1;
		}

	return ret;
}


ViStatus CInstrument::measureHipotResult(int step, HIPOT_TEST_INFO hipotTestInfo )
{
	ViStatus ret= 0;

	ret = do_command(VI_HIP,"*CLS");
	ret= do_command(VI_HIP,":SOURce:SAFEty:STOP");
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_query_string(VI_HIP,":SOURce:SAFEty:RESult:ALL:MMET?");
	if(ret != VI_SUCCESS) { return ret; }

	ret= do_command(VI_HIP,":SOURce:SAFEty:STOP");
	if(ret != VI_SUCCESS) { return ret; }
	return ret;
}



ViStatus CInstrument::measureIrStart(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	char strbuff[50];
	int nCLS = 0;
	float Lower_MOhm = (+5.0E+6);            //형식적인 Lower MOhm,,상하한 판단은 recipe로 한다!!!

	//Set Measurement Conditions
	do{
		ret = do_command(VI_IR,"*CLS");
		if(nCLS++ > 10){return ret;}
	}while(ret != VI_SUCCESS);

	if(ret != VI_SUCCESS) { return ret; }
	ret= do_command(VI_IR,  "AUTORANGE ON");  //ret= 
	if(ret != VI_SUCCESS) { return ret; }
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"LOWER %f,ON",Lower_MOhm ); //MOhm
	ret= do_command(VI_IR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	//memset(strbuff, 0x00, 50*sizeof(char));
	//sprintf_s(strbuff,"HIGHER %f,ON", irTestInfo.upperLimit*(+1.0E+6)); //%f  %g
	//ret= do_command(VI_IR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"TIMER %f,ON", irTestInfo.sec); //%f  %g LARGER THAN WAITTIME
	ret= do_command(VI_IR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"TESTV %d", irTestInfo.Vdc); //integer 10 to 1020
	ret= do_command(VI_IR, strbuff);
	if(ret != VI_SUCCESS) { return ret; }
	ret= do_command(VI_IR,"WAITTIME 0.5");			//real 0.3 to 10.0
	if(ret != VI_SUCCESS) { return ret; }
	//measurement
	ret = do_command(VI_IR,"START");				//START
	if(ret != VI_SUCCESS) { return ret; }
	return ret;
}

ViStatus CInstrument::measureIrStatus(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	ViUInt32 retCnt;
	//ViChar rcv[256];

	ret = do_command(VI_IR,"*CLS");
	//VOLTAGE, RESISTANCE, REMAINING TIME
	ret= do_query_string(VI_IR,"MON?"); //monitored voltage, resistance, time
	if(ret != VI_SUCCESS) { return ret; }

	//check if test finished
	ret= do_query_string(VI_IR,"DSR?"); //12: TESTING, 16: END, 1: DONE
	if(ret != VI_SUCCESS) { return ret; }
	retCnt = atoi(vi[VI_IR].str_result);
	return retCnt; // 0x12: testing
}


ViStatus CInstrument::measureIrResult(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	//char strbuff[50];
	double ir;
	CString str;

	ret = do_command(VI_IR,"*CLS");
	ret = do_query_string(VI_IR,"RDATA?");
	ir =  atof(vi[VI_IR].str_result);
	//vi[VI_IR].str_result display
	int rowGrid = step+1+MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST;
	str.Format(L"%f",ir);
	G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(str);
	if( irTestInfo.lowerLimit*(+1.0E+6) <= ir && ir <=  irTestInfo.upperLimit*(+1.0E+6)){//MOhm
		//G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"OK");
		G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(GREEN);
	}else{
		//G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"NG");
		G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(RED);
	}
	G_MainWnd->m_ServerDlg.m_GridMainResult.Refresh();
	ret = do_command(VI_IR,"STOP");
	return ret;

}


ViStatus CInstrument::measureIrStartCom(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	char strbuff[50];
	CString str;
	int nRet;
	int errCnt= 0;
	float Lower_MOhm = (+5.0E+6);

	//Set Measurement Conditions
	do{
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*CLS");
	nRet = str.Find(L"OK");
	if(errCnt++ > 10){ return nRet; }
	}while(nRet < 0);

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"AUTORANGE ON");
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	//memset(strbuff, 0x00, 50 * sizeof(char));
	//sprintf_s(strbuff, "UPPER 5E+009, OFF"); //MOhm //sprintf_s(strbuff, "UPPER 5000E6, OFF"); //MOhm
	//str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200, strbuff);
	//nRet = str.Find(L"OK");
	//if (nRet < 0){ return nRet; }

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff, "LOWER %G,ON",Lower_MOhm);
	//sprintf_s(strbuff, "LOWER %G,ON", irTestInfo.lowerLimit*(+1.0E+6)); //MOhm
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200, strbuff);
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"TIMER %G,ON", irTestInfo.sec); //%f  %g LARGER THAN WAITTIME
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200, strbuff);
	nRet = str.Find(L"OK");

	memset(strbuff, 0x00, 50*sizeof(char));
	sprintf_s(strbuff,"TESTV %d", static_cast<int>(irTestInfo.Vdc)); //integer 10 to 1020
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200, strbuff);
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"WAITTIME 0.5");
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"DSE #H31");
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	//measurement
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"START"); //START
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	return VI_SUCCESS;
}

ViStatus CInstrument::measureIrStatusCom(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	ViUInt32 retCnt;
	CString str;
	int nRet;

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*CLS");
	nRet = str.Find(L"OK");
	if(nRet < 0){ return nRet; }

	//VOLTAGE, RESISTANCE, REMAINING TIME
	//str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"MON?");
	//if(nRet < 0){ return nRet; }

	//check if test finished  //12: TESTING, 16: END, 1: DONE
	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"DSR?");
	retCnt = _wtoi(str);
	return retCnt; // 12: testing

}


ViStatus CInstrument::measureIrResultCom(int step, IR_TEST_INFO irTestInfo )
{
	ViStatus ret= 0;
	CString str;
	double ir;

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*CLS");

	//str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"DSR?");
	//status = _wtoi(str);
	//if((status & 0x30) == 0x20)//PASS
	//{
	//	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"RDATA?");
	//	ir = _wtof(str);
	//	str.Format(L"%5.2G",ir);
	//	memcpy(vi[VI_IR].str_result, str.GetBuffer(), str.GetLength());
	//	str.ReleaseBuffer();
	//}else if((status & 0x30) == 0x10)//FAIL
	//{
	//	str.Format(L"%5.2G",0);
	//	memcpy(vi[VI_IR].str_result, str.GetBuffer(), str.GetLength());
	//	str.ReleaseBuffer();
	//}

		str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"RDATA?");
		ir = _wtof(str);
		str.Format(L"%5.2f",ir);
		memcpy(vi[VI_IR].str_result, ConvertUnicodeToMultybyte(str.GetBuffer()), str.GetLength());
		str.ReleaseBuffer();

	str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"STOP");
	return ret;
}

void CInstrument::OnBnClickedBtnLcrRefSave()
{
	saveLCRCorrectRef(G_MainWnd->m_RecipeSetDlg.part_Test.part_model);
}


void CInstrument::OnBnClickedLcrAdjust()
{
	// CHECK LCR CORRECTION READY STATUS: PART IN LCR JIG, JIG UP
	//if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL  && G_MainWnd->m_InspectThread.m_fnGetBatchStep(LCR) == STEP0 )
	//	G_MainWnd->m_InspectThread.fLCRLoadCorrect = 1;
	//else
	//	AfxMessageBox(_T("Manual Mode, LCR step 0에서 설정할 수 있습니다", MB_ICONHAND);
}


int CInstrument::initGridLCRCorrect(void)
{
	try {
		m_GridLCRCorret.SetRowCount(3);
		m_GridLCRCorret.SetColumnCount(3);
		m_GridLCRCorret.SetFixedRowCount(1);		
		m_GridLCRCorret.SetFixedColumnCount(1);
		m_GridLCRCorret.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridLCRCorret.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridLCRCorret.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("   PARAMETER   "));					
				}
				else if (col == 2)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("        REFERENCE VALUE         "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if(row != 0)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"%d", row);					
					}
				}
				if(col == 1)
				{
					if(row == 1)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("Primary Parameter"));					
					}
					if(row == 2)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("Secondary Parameter"));					
					}
				}
			}
			m_GridLCRCorret.SetItem(&Item);
		}
	}

	m_GridLCRCorret.AutoSize(GVS_BOTH);
	m_GridLCRCorret.SetTextColor(RGB(0, 0, 105));
	m_GridLCRCorret.AutoSizeColumns();
	m_GridLCRCorret.ExpandToFit(true);
	//m_GridLCRCorret.SetEditable(FALSE);
	m_GridLCRCorret.Refresh();

	//m_GridLCRCorret.GetCell(1, 4)->SetBackClr(RGB(0xc0, 0xc0, 0xa0));

	return 0;
}


int CInstrument::readGridLCRCorrectRef(void)
{
	CString strValueData;
	double	fVal;
	
	GetDlgItem(IDC_LCR_CORR_MODEL)->SetWindowText(G_MainWnd->m_RecipeSetDlg.part_Test.part_model);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_LCR, KEY_LCR_REF_1ST_PARAM,L"");
	fVal = _wtof(strValueData);
	G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refPrimary = fVal;
	m_GridLCRCorret.GetCell(1,2)->SetText(strValueData);

	strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile(SECTION_LCR, KEY_LCR_REF_2ND_PARAM,L"");
	fVal = _wtof(strValueData);
	G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refSecondary = fVal;
	m_GridLCRCorret.GetCell(2,2)->SetText(strValueData);

	return 0;
}


int CInstrument::saveLCRCorrectRef(CString model)
{
	//find recipe
	CString strRecipe, strRecipeName, strRecipeFullPath;
	bool filefound = false;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") 
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);

			if(strRecipeName == model)
			{
				strRecipeFullPath = (LPCTSTR)finder.GetFilePath();
				filefound = true;
				break;
			}
		}//
	}

	//if not return -1
	if(!filefound)
	{
		return -1;
	}

	//save model data from grid

	BOOL bReturn = FALSE;
	CString strValueData;

	double fVal;

	strValueData = m_GridLCRCorret.GetCell(1,2)->GetText();
	bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(SECTION_LCR, KEY_LCR_REF_1ST_PARAM, strValueData);
	fVal = _wtof(strValueData);
	G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refPrimary = fVal;

	strValueData = m_GridLCRCorret.GetCell(2,2)->GetText();
	bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(SECTION_LCR, KEY_LCR_REF_2ND_PARAM, strValueData);
	fVal = _wtof(strValueData);
	G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[0].refSecondary = fVal;

	return 0;
}


int CInstrument::faultResetInstrument(int viIdx)
{
	ViStatus ret= 0;
	CString str;
	int nRet;

	if(viIdx != VI_IR)
	{
		ret= do_command(viIdx,"*CLS");
		ret= do_command(viIdx,"*CLS");
		ret= do_command(viIdx,"*CLS");
		if(ret != VI_SUCCESS) { return ret; }
	}else{
		str = G_MainWnd->m_SerialInterface.do_query_string_com(viIdx,"*CLS");
		str = G_MainWnd->m_SerialInterface.do_query_string_com(viIdx,"*CLS");
		str = G_MainWnd->m_SerialInterface.do_query_string_com(viIdx,"*CLS");
		nRet = str.Find(L"OK");
		if(nRet < 0){ return nRet; }
	}

	switch(viIdx){
	case VI_LCR:

		break;
	case VI_OSC:
		//check_instrument_errors(VI_OSC);
		break;
	case VI_DCR:


		break;
	case VI_HIP:

		break;
	case VI_IR:

		break;
	case VI_WG:

		break;
	}

	return 0;
}


int CInstrument::safeCloseInstrument(void)
{
	int ret;
	char strbuff[50];

	sprintf_s(strbuff,"BIAS:STATe OFF");
	ret = do_command(VI_LCR, strbuff);

#ifdef _FI
	ret = do_command(VI_WG,"OUTP OFF");
	//ret= do_command(VI_HIP,":SOURce:SAFEty:STOP");
	//G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"STOP");
#endif
	return 0;
}


void CInstrument::OnBnClickedVIReset(UINT nID)
{
	int nVI;
	ViStatus ret= 0;
	char strbuff[50];
	CString str;


	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(L"매뉴얼 모드에서 실행해야 합니다",MB_ICONHAND);
		return;
	}

	nVI = nID - IDC_BUTTON2;

#ifndef _FI
	if(nVI != VI_LCR && nVI != VI_DCR)
	{
		return;
	}
#endif

	if(nVI != VI_IR)
	{
		sprintf_s(strbuff,"*RST");
		ret = do_command(nVI, strbuff);
	}else{
		str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*CLS");
	}

}




afx_msg LRESULT CInstrument::OnUmInstrument(WPARAM wParam, LPARAM lParam)
{
	int iRet;
	int IRstep;

	if(instMsgRsp.BUSY == 1)return 0;

	switch(wParam){

	case UW_IR_START:
		instMsgRsp.BUSY = 1;
		SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_ONGOING);
		IRstep = G_MainWnd->m_InspectThread.stIRBatch.irStep;
		iRet = measureIrStartCom(IRstep, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[IRstep]);
		if(iRet < VI_SUCCESS)
		{ //Error
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 51 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 51 );
			SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_NG);
		}else{
			SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_OK);
		}
		instMsgRsp.BUSY = 0;
		break; 

	case UW_IR_STATUS:
		instMsgRsp.BUSY = 1;
		SetUMMsgResponse(UW_IR_STATUS, UM_INSTRUMENT_ONGOING);
		IRstep = G_MainWnd->m_InspectThread.stIRBatch.irStep;
		iRet = measureIrStatusCom(IRstep, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[IRstep]);
		if(iRet != 1){
			if(iRet == 12 || iRet == 16){//testing
				SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_TESTING);
			}
			//else{
			//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 51 );
			//	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 51 );
			//	SetUMMsgResponse(UW_IR_START, UM_INSTRUMENT_NG);
			//}
		}else{
			SetUMMsgResponse(UW_IR_STATUS, UM_INSTRUMENT_OK);
		}
		instMsgRsp.BUSY = 0;
		break; 

	case UW_IR_RESULT:
		instMsgRsp.BUSY = 1;
		SetUMMsgResponse(UW_IR_RESULT, UM_INSTRUMENT_ONGOING);
		IRstep = G_MainWnd->m_InspectThread.stIRBatch.irStep;
		iRet = measureIrResultCom(IRstep, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[IRstep]);
		if(iRet != VI_SUCCESS)
		{ //Error
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 51 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 51 );
			SetUMMsgResponse(UW_IR_RESULT, UM_INSTRUMENT_NG);
		}else{
			SetUMMsgResponse(UW_IR_RESULT, UM_INSTRUMENT_OK);
		}
		instMsgRsp.BUSY = 0;
		break; 

	}
	return 0;
}


void CInstrument::SetUMMsgResponse(int msg, int rsp)
{
	instMsgRsp.RSP[msg] = rsp;
}


int CInstrument::GetUMMsgResponse(int msg)
{
	int rsp;
	rsp = instMsgRsp.RSP[msg];
	return rsp;
}


//instrument connection check  IDN?
void CInstrument::OnTimer(UINT_PTR nIDEvent)
{
	int step;
	CString str;
	int nRet;

	switch(nIDEvent){
	case WM_TIMER_INSTRUMENT: 

		for(int i = VI_LCR; i <= VI_WG; i++)
		{
			switch(i){
			case VI_LCR:	step = G_MainWnd->m_InspectThread.m_fnGetBatchStep(LCR); break;
			case VI_OSC:
			case VI_WG:	step = G_MainWnd->m_InspectThread.m_fnGetBatchStep(OSC); break;
			case VI_DCR:	step = G_MainWnd->m_InspectThread.m_fnGetBatchStep(DCR); break;
			case VI_HIP:	step = G_MainWnd->m_InspectThread.m_fnGetBatchStep(HIPOT); break;
			case VI_IR:		step = G_MainWnd->m_InspectThread.m_fnGetBatchStep(IR); break;
			}


			if(!(step == STEP0 || step == STEP22 ))continue;
#ifndef _FI
			if(!(i == VI_LCR || i == VI_DCR)) continue; 
#endif
			if(i !=VI_IR)
			{
				if(initialize(i)!= VI_SUCCESS){
					if(open(i)== VI_SUCCESS) 
						G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
				}else{
					G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(RGB(230,230,230));
				}
			}else{
				str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*IDN?");
				nRet = str.Find(L"TOS");
				if(nRet > 0){
					G_AddLog(3,L"%s %s", ST_INSTRUMENT[i], str.GetBuffer()); str.ReleaseBuffer();
					G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(GREEN);
				}else{
					vi[i].connected = 0;
					G_MainWnd->m_ServerDlg.m_statusInstrument[i].SetBackColor(RGB(230,230,230));
				}
			}

		}
		break;

	}
	
	CDialogEx::OnTimer(nIDEvent);
}
