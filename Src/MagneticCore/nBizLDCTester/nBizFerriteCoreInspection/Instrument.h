#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "visa.h"
#include "1.Common\grid64\gridctrl.h"
#include "RecipeSetDlg.h"

#define NUM_INSTRUMENTS 6

#define VI_LCR	0		//E4980
#define VI_OSC	1		//DSOX2012A
#define VI_DCR	2		//RM3545
#define VI_HIP		3		//Chroma19052
#define VI_IR		4		//TOS7200
#define VI_WG		5		//DG1022

#define VISA_RESOURCE_USB_E4980 "USB0::0x0957::0x0909::MY46412865::0::INSTR" //
#define VISA_RESOURCE_DSOX2012A "USB0::0x0957::0x17A6::US50210029::0::INSTR"
#define VISA_RESOURCE_RM3545 "ASRL1"
#define VISA_RESOURCE_19052 "ASRL1"
#define VISA_RESOURCE_TOS7200 "ASRL1"

#define OSC_PROBE_ATTENUATION	1.0f

#define IEEEBLOCK_SPACE 5000000

//setting file name: *.vis
#define  INSTRUMENT_0						_T("E4980A")
#define  INSTRUMENT_1						_T("DSOX2012A")
#define  INSTRUMENT_2						_T("RM3545")
#define  INSTRUMENT_3						_T("19052")
#define  INSTRUMENT_4						_T("TOS7200")
#define  INSTRUMENT_5						_T("DG1022")

const CString ST_INSTRUMENT[] = 
{
	INSTRUMENT_0,
	INSTRUMENT_1,
	INSTRUMENT_2,
	INSTRUMENT_3,
	INSTRUMENT_4,
	INSTRUMENT_5,
};

//LCR CORRECTION Reference value: Ls Rs
#define		KEY_LCR_REF_1ST_PARAM						_T("LCR CORRECT 1st PARAM")
#define		KEY_LCR_REF_2ND_PARAM						_T("LCR CORRECT 2nd PARAM")

const CString ST_LCR_CORRECT_REF[] = 
{
	KEY_LCR_REF_1ST_PARAM,
	KEY_LCR_REF_2ND_PARAM,
};

#define WM_TIMER_INSTRUMENT WM_USER + 300

////////////////////////////////////////////////////////////
// UM_INSTRUMENT MESSAGE
#define UM_INSTRUMENT						WM_USER + 300 

#define UW_IR_START						0
#define UW_IR_STATUS					1
#define UW_IR_RESULT						2

#define UM_INSTRUMENT_CLEAR			0
#define UM_INSTRUMENT_ONGOING		1
#define UM_INSTRUMENT_OK				2
#define UM_INSTRUMENT_NG				4
#define UM_INSTRUMENT_TESTING		5

typedef struct _LCR_INSTRUMENT_MSG
{

	int BUSY; 
	int RSP[UW_IR_RESULT + 1];

	_LCR_INSTRUMENT_MSG(){	memset(this,0x00, sizeof(LCR_INSTRUMENT_MSG));}
}LCR_INSTRUMENT_MSG;
//LCR_INSTRUMENT_MSG MsgRsp;

class CInstrument : public CDialogEx
{
	DECLARE_DYNAMIC(CInstrument)

public:
	CInstrument(CWnd* pParent = NULL);   // 표준 생성자입니다.
	~CInstrument(void);

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_INSTRUMENT};

	//VISA Instrument
	typedef struct VISA
	{
		/* visa variables */
		ViSession		defaultRM, vi;									/* Device session ID. */
		ViStatus			err;												/* VISA function return value. */
		//ViRsrc			resource;										/* visa open resource */
		char				str_resource[256];//
		char				str_result[256];// = {0};					/* Result from do_query_string(). */
		double			num_result;										/* Result from do_query_number(). */
		unsigned char	ieeeblock_data[IEEEBLOCK_SPACE];		/* Result from do_query_ieeeblock(). */
		double			dbl_results[10];								/* Result from do_query_numbers(). */
		char				err_msg[1024];
		int					connected;										//-1: response error, 1: connected
		VISA(){	memset(this,0x00, sizeof(VISA));}
	};

	VISA vi[NUM_INSTRUMENTS];

	LCR_INSTRUMENT_MSG instMsgRsp;

///////////////////////////////////////////////////////////////////////////////////////////////////////
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	afx_msg LRESULT OnUmInstrument(WPARAM wParam, LPARAM lParam);

public:
	wchar_t vibuf[1024]; //scanf buffer

	//VISA
	ViStatus open(int viIdx);
	ViStatus initialize(int viIdx); /* Initialize to known state. */
	ViStatus do_command(int viIdx, char *command); /* Send command. */
	int do_command_ieeeblock(int viIdx, char *command, int num_bytes); /* Command w/IEEE block. */
	ViStatus do_query_string(int viIdx, char *query); /* Query for string. */
	ViStatus do_query_number(int viIdx, char *query); /* Query for number. */
	ViStatus do_query_numbers(int viIdx, char *query); /* Query for numbers. */
	int do_query_ieeeblock(int viIdx, char *query); /* Query for IEEE block. */
	ViStatus check_instrument_errors(int viIdx); /* Check for inst errors. */
	ViStatus error_handler(int viIdx); /* VISA error handler. */
	ViStatus visaClose(int viIdx);

	virtual BOOL OnInitDialog();
	void m_fnInit();
	void m_fnDeInit();
	void checkInstrumentComm();

	int LCR_test(void);
	
	CGridCtrl m_GridInstruVisa;
	int initGridInstrument(void);
	int readGridInstrumentData(void);
	afx_msg void OnBnClickedButton1();

	ViStatus measureLcr(int step, LCR_TEST_INFO lcrTestInfo );
	ViStatus correctLcr(int step, LCR_TEST_INFO lcrTestInfo );
	ViStatus enbleCorrectionPoint(int spot, int state);
	ViStatus measureOsc(int step, OSC_TEST_INFO oscTestInfo);
	ViStatus measureDcr(int step, DCR_TEST_INFO dcrTestInfo );
	int zeroAdjustDcr(int step, DCR_TEST_INFO dcrTestInfo );
	ViStatus measureHipotStart(int step, HIPOT_TEST_INFO hipotTestInfo );
	ViStatus measureHipotStatus(int step, HIPOT_TEST_INFO hipotTestInfo );
	ViStatus measureHipotResult(int step, HIPOT_TEST_INFO hipotTestInfo );
	ViStatus measureIrStart(int step, IR_TEST_INFO irTestInfo ); 
	ViStatus measureIrStatus(int step, IR_TEST_INFO irTestInfo );
	ViStatus measureIrResult(int step, IR_TEST_INFO irTestInfo );
	ViStatus measureIrStartCom(int step, IR_TEST_INFO irTestInfo ); 
	ViStatus measureIrStatusCom(int step, IR_TEST_INFO irTestInfo );
	ViStatus measureIrResultCom(int step, IR_TEST_INFO irTestInfo );
	CGridCtrl m_GridLCRCorret;
	afx_msg void OnBnClickedBtnLcrRefSave();
	afx_msg void OnBnClickedLcrAdjust();
	int initGridLCRCorrect(void);
	int readGridLCRCorrectRef(void);
	int saveLCRCorrectRef(CString model);
	int faultResetInstrument(int viIdx);
	int safeCloseInstrument(void);
	afx_msg void OnBnClickedVIReset(UINT nID);
	void SetUMMsgResponse(int msg, int rsp);
	int GetUMMsgResponse(int msg);

	CRoundButton2 m_btnResetVILVR;
	CRoundButton2 m_btnResetVIOSC;
	CRoundButton2 m_btnResetVIDCR;
	CRoundButton2 m_btnResetVIHIPOT;
	CRoundButton2 m_btnResetVIIR;
	CRoundButton2 m_btnResetVIWG;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

