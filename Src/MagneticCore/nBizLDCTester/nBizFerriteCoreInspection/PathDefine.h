#pragma once

#define      SYSTEM_ROOT_FOLDER			_T("D:\\nBizSystem\\")
#define		SYSTEM_PARAM_PATH			_T("D:\\nBizSystem\\SystemSetting.ini")
#define		RECIPE_PATH						_T("D:\\nBizSystem\\Recipe.ini")


#define      DATA_ROOT_FOLDER				_T("D:\\nBizData\\")
#define      DATA_NG_FOLDER				_T("D:\\nBizData\\NG\\")
#define      RESULT_DATA_FOLDER			_T("D:\\nBizData\\ResultData\\")
#define      IMAGE_SAVE_FOLDER			_T("D:\\nBizData\\Image\\")


#define OK_Exel			("OK")
#define NG_Exel			("NG")
#define NONE_Exel		("NONE")
#define	GCHP_Exel		("GCHP")
#define	TSIO_Exel		("TSIO")
#define	TSIM_Exel		("TSIM")
#define	PCUP_Exel		("PCUP")

/////////////////////////////////////////////////////////////////////////////////////////////////////
#define		SYSTEM_FOLDER								_T("D:\\nBizSystem\\System\\")
#define		SYSTEM_FOLDER_FILES						_T("D:\\nBizSystem\\System\\*.*")
#define		SYSTEM_PARAM_INI_PATH					_T("D:\\nBizSystem\\System\\System.ini")

#define      SYSTEM_LOG_FOLDER						_T("D:\\nBizSystem\\Process_Log\\")
#define      SYSTEM_ALARM_LOG_FOLDER				_T("D:\\nBizSystem\\Alarm_Log\\")

#define		VISION_FOLDER							_T("D:\\nBizSystem\\Vision\\")
#define		VISION_CALIBRATION_DATA_PATH						_T("D:\\nBizSystem\\Vision\\Calibration.vid")

#define		RECIPE_FOLDER								_T("D:\\nBizSystem\\Recipe\\")
#define		RECIPE_FOLDER_BLANK						_T("D:\\nBizSystem\\Recipe\\Recipe.ini")
#define		RECIPE_FOLDER_PATH						_T("D:\\nBizSystem\\Recipe\\*.*")
#define		PASSWORD_PATH								_T("D:\\nBizSystem\\System\\")
#define		TMC_FILE_NAME								_T("D:\\nBizSystem\\System\\Default.prm")

/////////////////////////////////////////////////////////////////////////////////////////////////////

#define      DATA_ROOT_FOLDER							_T("D:\\nBizData\\")
#define      DATA_ROOT_NG_FOLDER					_T("D:\\nBizData\\NG\\")
#define      RESULT_DATA_FOLDER						_T("D:\\nBizData\\ResultData\\")

///////////////////////////////////////////////////////////////////////////////////////////////////

#define      DATA_IMAGE_FOLDER						_T("D:\\nBizData\\Image\\")
#define      DATA_IMAGE_NG_FOLDER					_T("D:\\nBizData\\NG_Image\\")


/////////////////////////////////////////////////////////////////////////////////////////////////////////
//##############################################################//
//

///////////////////////////////////////////////////////////////////////////////////
// AXIS ERROR LOG

					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_ERROR_UPDATE, 0 );
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_AXIS_LOG_UPDATE, 0 );

const CString ST_AXIS_ERROR[] = {
	//  AXIS					0
_T("LCR AXIS POSITION CHECK ERROR"),
_T("LCR READY TO HOME CHECK DONE TIME OUT ERROR"),
_T("LCR READY TO HOME MOVE ERROR"),
_T("SYS INIT SHUTTLE READY ERROR"),
_T("AXIS HOME TO READY MOVE ERROR: NOT IN HOME POSITION"),
_T("AXIS HOME TO READY ABS MOVE ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("LCR AXIS HOME CLEAR ERROR"),
_T("ERROR_0"),

	//  AXIS					10
_T("LCR AXIS STATUS BIT ERROR"),
_T("LCR HOME JOG MOVE TIME OUT"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),

	//  AXIS					20
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),

	//  AXIS					30
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
};


const CString ST_AXIS_LOG[] = {
//  AXIS					0
_T("SYS INIT LCR AXIS POSITION CHECK ERROR"),
_T("LCR READY TO HOME CHECK DONE TIME OUT ERROR"),
_T("LCR READY TO HOME MOVE ERROR"),
_T("SYS INIT SHUTTLE READY ERROR"),
_T("AXIS HOME TO READY MOVE ERROR: NOT IN HOME POSITION"),
_T("AXIS HOME TO READY ABS MOVE ERROR"),
_T("AXIS HOME TO READY ABS MOVE CHECK DONE ERROR"),
_T("LCR AXIS HOME TO READY MOVE SUCCEDED"),
_T("LCR AXIS HOME CLEAR ERROR"),
_T("LOG_0"),

	//  AXIS					10
_T("LCR AXIS STATUS BIT ERROR"),
_T("LCR HOME JOG MOVE TIME OUT"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),

	//  AXIS					20
_T("SYS INIT SHUTTLE READY 완료"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),

	//  AXIS					30
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
};

/////////////////////////////////////////////////////////////////////////////
// Inspection Error & Log message
//
//  LOAD					00 ~ 09
//  LCR					10
//  OSC					20
//  DCR					30
//  HIPOT				40
//  IR						50
//  VI						60
//  UNLOAD				70
//  SHUTTLE			80
//  RETURNCV			90
//
//

					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 0 );
					//::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 0 );


const CString ST_INSPECT_ERROR[] = {
//  LOAD					00
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  LCR					10
_T("LCR JIG UP/DOWN ERROR"),
_T("LCR 계측기 통신 에러"),
_T("LCR SHORT PIN UP/DOWN ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  OSC					20
_T("OSC JIG UP/DOWN ERROR"),
_T("OSC 계측기 통신 에러"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  DCR					30
_T("DCR JIG UP/DOWN ERROR"),
_T("DCR 계측기 통신 에러"),
_T("검사 결과 NG입니다. Unload 지그를 MANUAL 모드에서 꺼내세요"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  HIPOT				40
_T("HIPOT JIG UP/DOWN ERROR"),
_T("HIPOT 계측기 통신 에러"),
_T("HIPOT RELAY SWITCHING ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  IR						50
_T("IR JIG UP/DOWN ERROR"),
_T("IR 계측기 통신 에러"),
_T("IR RELAY SWITCHING ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  VI						60
_T("VI TIME OUT ERROR"),
_T("VISION INSPECTION ERROR"),
_T("Camera Connection Error"),
_T("Camera Serial No Unmatched"),
_T("Camera InitGrab Error"),
_T("Camera Grab Error"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  UNLOAD				70
_T("검사 결과 NG입니다. Unload 지그를 MANUAL 모드에서 꺼내세요"),
_T("ERROR_0"),
_T("SHUTTLE POSITION CHECK FAIL"),
_T("UNLOAD UP/DOWN TIME OUT ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  SHUTTLE			80
_T("SHUTTLE FWD MOVE ERROR"),
_T("SHUTTLE RET MOVE ERROR"),
_T("SHUTTLE MOVE TIMEOUT ERROR"),
_T("SHUTTLE JIG UP/DOWN ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//  RETURNCV			90
_T("CONVEYOR RUN ERROR"),
_T("LABLE PRINT ERROR"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
//							100
_T("안전커버 열림"),
_T("LIGHT CURTAIN INTERRUPTED"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),

_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
_T("ERROR_0"),
};


const CString ST_INSPECT_LOG[] = {
//  LOAD					00
_T("NEW PART IN LOAD "),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  LCR					10
_T("LCR JIG UP/DOWN ERROR"),
_T("LCR 계측기 통신 에러"),
_T("LCR SHORT PIN UP/DOWN ERROR"),
_T("LCR JIG UP"),
_T("LCR JIG DOWN"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  OSC					20
_T("OSC JIG UP/DOWN ERROR"),
_T("OSC 계측기 통신 에러"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  DCR					30
_T("DCR JIG UP/DOWN ERROR"),
_T("DCR 계측기 통신 에러"),
_T("DCR ZERO ADJUSTMENT FAIL"),
_T("DCR 공정검사 FAIL JIG OUT"),
_T("DCR 공정검사 FAIL "),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  HIPOT				40
_T("HIPOT JIG UP/DOWN ERROR"),
_T("HIPOT 계측기 통신 에러"),
_T("HIPOT RELAY SWITCHING ERROR"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  IR						50
_T("IR JIG UP/DOWN ERROR"),
_T("IR 계측기 통신 에러"),
_T("IR RELAY SWITCHING ERROR"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  VI						60
_T("VI TIME OUT ERROR"),
_T("VISION INSPECTION ERROR"),
_T("Camera Connection Error"),
_T("Camera Serial No Unmatched"),
_T("Camera InitGrab Error"),
_T("Camera Grab Error"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  UNLOAD				70
_T("검사 결과 NG"),
_T("IR JIG UP/DOWN ERROR"),
_T("SHUTTLE POSITION CHECK FAIL"),
_T("UNLOAD UP/DOWN TIME OUT ERROR"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  SHUTTLE			80
_T("SHUTTLE FWD MOVE ERROR"),
_T("SHUTTLE RET MOVE ERROR"),
_T("SHUTTLE MOVE TIMEOUT ERROR"),
_T("SHUTTLE JIG UP/DOWN ERROR"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//  RETURNCV			90
_T("CONVEYOR RUN ERROR"),
_T("LABLE PRINT ERROR"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
//							100
_T("안전커버 열림"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),

_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
_T("LOG_0"),
};



