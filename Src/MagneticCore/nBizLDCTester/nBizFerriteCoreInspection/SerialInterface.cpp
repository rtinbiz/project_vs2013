#include "stdafx.h"
#include "SerialInterface.h"
#include "CommThread.h"
#include "MainWnd.h"


HWND g_hCommWnd;

IMPLEMENT_DYNAMIC(CSerialInterface, CWnd)

extern CMainWnd	*G_MainWnd;

CSerialInterface::CSerialInterface()
{
	// 클래스 생성 **
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	wndclass.style			= CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= AfxWndProc;
	wndclass.hInstance		= AfxGetInstanceHandle();
	wndclass.hIcon			= NULL;
	wndclass.hCursor		= AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName	= NULL;
	wndclass.lpszClassName	= L"Serial232";

	AfxRegisterClass(&wndclass);

	CreateEx(NULL,L"Serial232", NULL, NULL, CRect(0,0,0,0), NULL, NULL, NULL);	

	g_hCommWnd = this->m_hWnd;			
	
 	//m_CommThread.m_pSerialInterface = this;
	// 클래스 생성 ##		

	InitializeCriticalSection(&csComm);

}	

CSerialInterface::~CSerialInterface(void)
{
	memset(m_nComportIndex, 0x00, sizeof(int) * COMM_PORT_COUNT);
	
	DeleteCriticalSection(&csComm);		
}

BEGIN_MESSAGE_MAP(CSerialInterface, CWnd)	
	ON_MESSAGE(WM_COMM_READ,	OnCommRead)
	ON_WM_TIMER()
END_MESSAGE_MAP()


void CSerialInterface::m_fnInit()
{	
	BOOL bRes = TRUE;

	G_MainWnd->m_SystemSetupDlg.m_fnUpdateComport(); 
	//m_nComportIndex[0] = 0;

	for(int i= 0; i< COMM_PORT_COUNT; i++)
	{
		bRes = true;
		if(!m_CommThread[i].m_bConnected)
		{
			if(m_CommThread[i].m_fnInitialize(m_nComportIndex[i]) == FALSE)  //
			{
				G_AddLog(3,L"Comm Port %d 연결 실패", m_nComportIndex[i]+1); 
				bRes = FALSE;
			}
			else
			{
				G_AddLog(3,L"Comm Port %d 연결", m_nComportIndex[i]+1); 
				//Vision Light
				if(i == 0)
				{
					KillTimer(TIMER_LIGHT_READ);
					SetTimer(TIMER_LIGHT_READ, 500, NULL);
				}
			}
			if(bRes == FALSE)
			{
				//G_AlarmListAdd(ALARM_3001, D_3001);
			}
		}
	}
}


int CSerialInterface::m_fnDeInit(void)
{

	for(int i= 0; i< COMM_PORT_COUNT; i++)
	{
		if(m_CommThread[i].m_bConnected)
			m_CommThread[i].ClosePort();
		m_CommThread[i].m_bConnected = false;
	}
	KillTimer(TIMER_LIGHT_READ);
	return 0;
}



//long CSerialInterface::OnCommRead(WPARAM wParam, LPARAM lParam)
LRESULT CSerialInterface::OnCommRead(WPARAM wParam, LPARAM lParam)
{
	int i, j;
	if(G_SystemModeData.unSystemMode == SYSTEM_EXIT)
	{
		return 0;
	}

	int nPortID = 0;
	nPortID = (int)wParam;

	for(i = 0; i< COMM_PORT_COUNT; i++)
	{
		if(m_nComportIndex[i] == nPortID) //m_CommThread
		{
			BYTE aByte; //데이터를 저장할 변수		
			CString strReadByte;
			CString strCRLF;
			strCRLF.Format(L"%c%c",13, 10);

			int iSize =  m_CommThread[i].m_QueueRead.GetSize(); //포트로 들어온 데이터 갯수

			if(iSize != 0) // Size가 0이면 무시한다.
			{

				for(j  = 0 ; j < iSize; j++)//들어온 갯수 만큼 데이터를 읽어 와 화면에 보여줌
				{				
					m_CommThread[i].m_QueueRead.GetByte(&aByte);//큐에서 데이터 한개를 읽어옴
					strReadByte.Format(L"%c", aByte);
					m_CommThread[i].m_strReciveString += strReadByte;
				}

				//process commport data for each thread
				processCommData(i);

			}
		}
	}




	return 0;
}

void CSerialInterface::OnTimer(UINT_PTR nIDEvent)
{	
	if(nIDEvent == TIMER_LIGHT_READ)
	{
		//m_fnSendToComportLight(MODE_READ);
	}

	CWnd::OnTimer(nIDEvent);
}


BOOL CSerialInterface::m_fnSendToComportLight(int nMode, int nValue)
{
	int nSent;
	CString strMessage;
	BYTE byPort[256];
	CString strCR;
	CString strLF;
	Sleep(100);

	memset(byPort, 0x00, 256);

	strCR.Format(L"%c",13);
	strLF.Format(L"%c",10);
	if(nMode == MODE_READ)
	{
		strMessage.Format(L"nLPD20 0 0 ");
	}
	else if(nMode == MODE_LIGHT_1)
	{		
		strMessage.Format(L"nLPD20 1 %d ", nValue);
	}
	else if(nMode == MODE_LIGHT_2)
	{
		strMessage.Format(L"nLPD20 2 %d ", nValue);
	}

	strMessage = strMessage + strCR + strLF;		
	//memcpy(byPort, strMessage, strMessage.GetLength());
	memcpy(byPort, ConvertUnicodeToMultybyte(strMessage), strMessage.GetLength()); //Unicode
	EnterCriticalSection(&csComm);
	nSent = m_CommThread[0].WriteComm(byPort, strMessage.GetLength());
	LeaveCriticalSection(&csComm);

	return TRUE;
}

/* --------------------------------------------------------------- */
CString CSerialInterface::do_query_string_com(int portIdx, char *query)
{
	int nWait;
	int waitms = 100;
	char message[80];
	BYTE byPort[256];
	CString str;
	int iSize;
	BYTE aByte; //데이터를 저장할 변수		
	CString strReadByte;

	strcpy_s(message, query);
	strcat_s(message,"\r\n");
	int length = strlen(message);
	memcpy(byPort, message, length);
	length = m_CommThread[portIdx].WriteComm(byPort, length);
	if(length <= 0)
		return L"0";
	nWait = 0;

	do{
		Sleep(waitms);
		iSize =  m_CommThread[portIdx].m_QueueRead.GetSize(); //포트로 들어온 데이터 갯수
		//if(iSize >= 4)break;//worked, polling though
		if(iSize >= 2)break;//test
	}while(nWait++ < 50);

	if(iSize != 0) // Size가 0이면 무시한다.
	{
		Sleep(50);
		iSize =  m_CommThread[portIdx].m_QueueRead.GetSize(); //포트로 들어온 데이터 갯수
		for(int j  = 0 ; j < iSize; j++)
		{				
			m_CommThread[portIdx].m_QueueRead.GetByte(&aByte);//큐에서 데이터 한개를 읽어옴
			strReadByte.Format(_T("%c"), aByte);
			m_CommThread[portIdx].m_strReciveString += strReadByte;
		}
		//process commport data for each thread
		processCommData(portIdx);
	}

	//m_csVisaIR.Lock();
	if(strVisaIR.GetLength() > 0)
	{
		str = strVisaIR;
		strVisaIR.Empty();
		return str;
	}
	//m_csVisaIR.Unlock();

	return L"0";
}

int CSerialInterface::do_query_write_com(int portIdx, char *query)
{
	char message[80];
	BYTE byPort[256];
	CString str;
	strcpy_s(message, query);
	strcat_s(message,"\r\n");
	int length = strlen(message);
	memcpy(byPort, message, length);
	length = m_CommThread[portIdx].WriteComm(byPort, length);
	return length;
}

CString CSerialInterface::do_query_read_com(int portIdx)
{
	int waitms = 100;
	int nWait = 0;
	CString str;

	BYTE aByte; //데이터를 저장할 변수		
	CString strReadByte;
	CString strCRLF;
	strCRLF.Format(L"%c%c",13, 10);

	if(portIdx == COM_TOS7200)
	{

	int iSize =  m_CommThread[portIdx].m_QueueRead.GetSize(); //포트로 들어온 데이터 갯수

	if(iSize != 0) // Size가 0이면 무시한다.
	{
		for(int j  = 0 ; j < iSize; j++)//들어온 갯수 만큼 데이터를 읽어 와 화면에 보여줌
		{				
			m_CommThread[portIdx].m_QueueRead.GetByte(&aByte);//큐에서 데이터 한개를 읽어옴
			strReadByte.Format(L"%c", aByte);
			m_CommThread[portIdx].m_strReciveString += strReadByte;
		}

		//process commport data for each thread
		processCommData(portIdx);

	}

	//do{
		//	Sleep(waitms);
			//m_csVisaIR.Lock();
			if(strVisaIR.GetLength() > 0)
			{
				str = strVisaIR;
				strVisaIR.Empty();
				return str;
			}
		//	m_csVisaIR.Unlock();
		//}while(nWait++ < 10);
	}

	return L"0";
}


int CSerialInterface::processCommData(int port)
{
	switch(port)
	{
		//Comm1 //Light control
		case 0:
			processComm0();
			break;
		//Comm2
		case 1:
			processComm1();
			break;
		//Comm3
		case 2:
			processComm2(); //IR
			break;
		//Comm4
		case 3:
			processComm3();
			break;
		//Comm5
		case 4:
			processComm4();
			break;
		case 5:
			processComm5();
			break;
		case 6:
			processComm6();
			break;
		case 7:
			processComm7();
			break;
		default:

			break;
	}



	return 0;
}

int CSerialInterface::processComm0(void)
{

	CString strCRLF;
	strCRLF.Format(L"%c%c",13, 10);

	if(m_CommThread[0].m_strReciveString.GetLength() > 2)
	{
		if(m_CommThread[0].m_strReciveString.Right(2) == strCRLF)
		{
			m_CommThread[0].m_strReciveString.TrimRight();
			m_CommThread[0].m_strReciveString = m_CommThread[0].m_strReciveString.Mid(m_CommThread[0].m_strReciveString.Find(L" ") +1);						

			G_MainWnd->m_DataHandling.m_LightValueData.nLight1   = _wtoi(m_CommThread[0].m_strReciveString.Left(m_CommThread[0].m_strReciveString.Find(L" ")));
			G_MainWnd->m_DataHandling.m_LightValueData.nLight2 = _wtoi(m_CommThread[0].m_strReciveString.Mid(m_CommThread[0].m_strReciveString.Find(L" ")));

			m_CommThread[0].m_strReciveString = L"";
			m_CommThread[0].m_QueueRead.Clear();
		}
	}

	return 0;
}

int CSerialInterface::processComm1()
{
	if(m_CommThread[1].m_strReciveString.GetLength() > 2)
	{
		//memcpy(G_MainWnd->m_InspectThread.m_PartData[STG_LOAD].QRcode, m_CommThread[1].m_strReciveString, m_CommThread[1].m_strReciveString.GetLength());
		//G_MainWnd->m_ServerDlg.m_staticBarCode.SetText(m_CommThread[1].m_strReciveString);
		m_CommThread[1].m_strReciveString = _T("");
		m_CommThread[1].m_QueueRead.Clear();
	}	
	return 0;
}
int CSerialInterface::processComm2(){
	//m_csVisaIR.Lock();
	strVisaIR = m_CommThread[COM_TOS7200].m_strReciveString;
	m_CommThread[COM_TOS7200].m_strReciveString = _T("");
	m_CommThread[COM_TOS7200].m_QueueRead.Clear();
	//m_csVisaIR.Unlock();
	return 0;
}
int CSerialInterface::processComm3(){

	return 0;
}
int CSerialInterface::processComm4(){

	return 0;
}
int CSerialInterface::processComm5(){

	return 0;
}
int CSerialInterface::processComm6(){

	return 0;
}
int CSerialInterface::processComm7(){

	return 0;
}
