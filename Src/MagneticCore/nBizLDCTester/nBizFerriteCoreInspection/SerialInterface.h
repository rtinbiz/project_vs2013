﻿#pragma once

#include "CommThread.h"
#include "stdAfx.h"

#define MODE_READ							0
#define MODE_INIT							1
#define MODE_LIGHT_1						2
#define MODE_LIGHT_2						3


#define LABEL_PRINTER						1
#define COM_TOS7200						2

#ifdef _FI
#define COMM_PORT_COUNT			3		//Light Control, label printer
#else
#define COMM_PORT_COUNT			1		
#endif

enum
{
	TIMER_LIGHT_READ = WM_USER+1,
};

// CameraView On, Off
class CSerialInterface : public CWnd
{
	DECLARE_DYNAMIC(CSerialInterface)

public:
	CSerialInterface();
	~CSerialInterface(void);	

	int m_nComportIndex[COMM_PORT_COUNT];
	CCommThread m_CommThread[COMM_PORT_COUNT];

	CCriticalSection m_csVisaIR;
	CString strVisaIR;

	BOOL m_fnSendToComportLight(int nMode, int nValue = 0);
	CString do_query_string_com(int portIdx, char *query);
	int do_query_write_com(int portIdx, char *query);
	CString do_query_read_com(int portIdx);

public:		
	LRESULT OnCommRead(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

public:
	void m_fnInit();

	CRITICAL_SECTION csComm;


public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	int processCommData(int port);
	int m_fnDeInit(void);
	int processComm0(void);
	int processComm1(void);
	int processComm2(void);
	int processComm3(void);
	int processComm4(void);
	int processComm5(void);
	int processComm6(void);
	int processComm7(void);

};

//nLPD USE
//"nLPD20 1 %d " : CH1, 0~255
//"nLPD21 2 %d " : CH2, 0~100

