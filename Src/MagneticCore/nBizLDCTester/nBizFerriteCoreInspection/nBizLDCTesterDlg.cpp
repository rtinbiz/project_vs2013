
// nBizLDCTesterDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "nBizLDCTester.h"
#include "nBizLDCTesterDlg.h"
#include "afxdialogex.h"
#include "PassWordDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CnBizLDCTesterDlg 대화 상자
extern CMainWnd* G_MainWnd;

CnBizLDCTesterDlg::CnBizLDCTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CnBizLDCTesterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	memset(paintImageReady, 0x00, sizeof(int)*4);
}

void CnBizLDCTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG_VIEW, m_listLogView);
	DDX_Control(pDX, IDC_PIC, m_pcMainView);
	DDX_Control(pDX, IDC_GRID_RESULT_LIST, m_ctrlGridResult);
	DDX_Control(pDX, IDC_BTN_AUTO, m_btnAuto);
	DDX_Control(pDX, IDC_BTN_MANU, m_btnManual);
	DDX_Control(pDX, IDC_GRID_MAIN_RESULT, m_GridMainResult);
	DDX_Control(pDX, IDC_OPERATOR_MODE, m_OperatorMode);
	DDX_Control(pDX, IDC_STATIC_1, m_static1);
	DDX_Control(pDX, IDC_STATIC_2, m_static2);
	DDX_Control(pDX, IDC_STATIC_3, m_static3);
	DDX_Control(pDX, IDC_STATIC_4, m_static4);
	DDX_Control(pDX, IDC_STATIC_5, m_static5);
	DDX_Control(pDX, IDC_STATIC_6, m_static6);
	DDX_Control(pDX, IDC_STATIC_MODEL, m_staticModel);
	DDX_Control(pDX, IDC_STATIC_TYPE, m_staticType);
	DDX_Control(pDX, IDC_STATIC_ACCU, m_staticAccu);
	DDX_Control(pDX, IDC_STATIC_LOT, m_staticLOT);
	DDX_Control(pDX, IDC_STATIC_TARGET, m_staticTarget);
	DDX_Control(pDX, IDC_STATIC_BARCODE, m_staticBarCode);
	DDX_Control(pDX, IDC_STATIC_TARGET2, m_staticTargetPercent);
	DDX_Control(pDX, IDC_STATIC_7, m_static7);
	DDX_Control(pDX, IDC_STATIC_8, m_static8);
	DDX_Control(pDX, IDC_STATIC_9, m_staticTotal);
	DDX_Control(pDX, IDC_STATIC_10, m_staticOKNG);
	DDX_Control(pDX, IDC_BTN_MODE, m_btnMode);
	DDX_Control(pDX, IDC_BTN_MAIN_EXIT, m_btnNGClear);
	DDX_Control(pDX, IDC_BTN_STOP, m_btnStop);

	DDX_Control(pDX, IDC_STATIC_11, m_static11);
	DDX_Control(pDX, IDC_STATIC_12, m_static12);
	DDX_Control(pDX, IDC_TIME_AUTO_START, m_staticTimeMasterStart);
	DDX_Control(pDX, IDC_TIME_ELLAPSED, m_staticTimeMasterElapsec);
	DDX_Control(pDX, IDC_BTN_START, m_btnStart);
	DDX_Control(pDX, IDC_BTN_HOME, m_btnHome);
	DDX_Control(pDX, IDC_ERROR_MSG, m_ErrorMsg);
	DDX_Control(pDX, IDC_STATIC_13, m_staticOK);
	DDX_Control(pDX, IDC_STATIC_14, m_staticNG);

	for (int id = 0; id < 6; id++)
	{
		DDX_Control(pDX, IDC_STATUS_LCR + id, m_statusInstrument[id]);
	}

	DDX_Control(pDX, IDC_BTN_HOLD_SHUTTLE, m_btnHoldShuttle);
	DDX_Control(pDX, IDC_BTN_BUZZSTOP, m_btnBuzzStop);
	DDX_Control(pDX, IDC_MASTERTEST_OK, m_masterTest_OK);
	DDX_Control(pDX, IDC_MASTERTEST_NG, m_masterTest_NG);
	DDX_Control(pDX, IDC_BTN_MASTER_OK2, m_masterTest_OK_Start);
	DDX_Control(pDX, IDC_BTN_MASTER_NG2, m_masterTest_NG_Start);
	DDX_Control(pDX, IDC_BTN_NG_OUT, m_btnNGOut);
	DDX_Control(pDX, IDC_STATIC_15, m_staticName);
	DDX_Control(pDX, IDC_STATUS_CAM, m_statusCam);
}

BEGIN_MESSAGE_MAP(CnBizLDCTesterDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_MESSAGE(UM_UI_UPDATE, &CnBizLDCTesterDlg::OnUiUpdateCmd)
	ON_BN_CLICKED(IDC_BTN_AUTO, &CnBizLDCTesterDlg::OnBnClickedBtnAuto)
	ON_BN_CLICKED(IDC_BTN_MANU, &CnBizLDCTesterDlg::OnBnClickedBtnManu)
	ON_BN_CLICKED(IDC_BTN_MAIN_EXIT, &CnBizLDCTesterDlg::OnBnClickedBtnMainExit)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_MAIN_RESULT, OnGridDblClick)
	ON_NOTIFY(NM_CLICK, IDC_GRID_MAIN_RESULT, OnGridClick)
	ON_COMMAND_RANGE(IDC_STATUS_LCR, IDC_STATUS_WG, &CnBizLDCTesterDlg::OnStnClickedStatusInstrument)
	ON_BN_CLICKED(IDC_BTN_MODE, &CnBizLDCTesterDlg::OnBnClickedBtnMode)
	ON_COMMAND(ID_MENU_SYSTEM, &CnBizLDCTesterDlg::OnMenuSystem)
	ON_COMMAND(ID_MENU_MODEL, &CnBizLDCTesterDlg::OnMenuModel)
	ON_COMMAND(ID_MENU_INSTRUMENT, &CnBizLDCTesterDlg::OnMenuInstrument)
	ON_COMMAND(ID_MENU_AXIS, &CnBizLDCTesterDlg::OnMenuAxis)
	ON_COMMAND(ID_MENU_IO, &CnBizLDCTesterDlg::OnMenuIo)
	ON_COMMAND(ID_HELP_ABOUT, &CnBizLDCTesterDlg::OnHelpAbout)
	ON_COMMAND(ID_FILE_EXIT, &CnBizLDCTesterDlg::OnFileExit)
	ON_BN_CLICKED(IDC_BTN_START, &CnBizLDCTesterDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CnBizLDCTesterDlg::OnBnClickedBtnStop)
	ON_BN_CLICKED(IDC_BTN_HOME, &CnBizLDCTesterDlg::OnBnClickedBtnErrorReset)

	ON_BN_CLICKED(IDC_BTN_HOLD_SHUTTLE, &CnBizLDCTesterDlg::OnBnClickedBtnHoldShuttle)
	ON_BN_CLICKED(IDC_BTN_BUZZSTOP, &CnBizLDCTesterDlg::OnBnClickedBtnBuzzstop)
	ON_BN_CLICKED(IDC_BTN_MASTER_OK2, &CnBizLDCTesterDlg::OnBnClickedBtnMasterOk2)
	ON_BN_CLICKED(IDC_BTN_MASTER_NG2, &CnBizLDCTesterDlg::OnBnClickedBtnMasterNg2)
	ON_COMMAND(ID_MENU_LABELPRINT, &CnBizLDCTesterDlg::OnMenuLabelprint)
	ON_WM_INPUT()
	ON_BN_CLICKED(IDC_BTN_NG_OUT, &CnBizLDCTesterDlg::OnBnClickedBtnNgOut)
	ON_COMMAND(ID_MENU_VISIONINSPECTION, &CnBizLDCTesterDlg::OnMenuVisioninspection)
	ON_STN_CLICKED(IDC_STATUS_CAM, &CnBizLDCTesterDlg::OnStnClickedStatusCam)
END_MESSAGE_MAP()


// CnBizLDCTesterDlg 메시지 처리기

BOOL CnBizLDCTesterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	G_MainWnd->m_DataHandling.nDayReportNum = 1;

	SetTimer(TIMER_FILE_UPDATE, 1000, NULL);

	resultGridInit();
	initGridMainResult();

	m_OperatorMode.SetFont2(25, 10, 7);
	m_static1.SetFont2(20, 8, 7);	m_static1.SetText(L"모델명", BLACK, RGB(180,190,225));	
	m_static2.SetFont2(20, 8, 7);	m_static2.SetText(L"TYPE", BLACK, RGB(180,190,225));	
	m_static3.SetFont2(20, 8, 7);	m_static3.SetText(L"누적시험횟수", BLACK, RGB(180,190,225));	
	m_static4.SetFont2(20, 8, 7);	m_static4.SetText(L"LOT명", BLACK, RGB(180,190,225));	
	m_static5.SetFont2(20, 8, 7);	m_static5.SetText(L"LOT목표수량", BLACK, RGB(180,190,225));	
	m_static6.SetFont2(20, 8, 7);	m_static6.SetText(L"바코드", BLACK, RGB(180,190,225));	
	m_static7.SetFont2(20, 8, 7);	m_static7.SetText(L"Total", BLACK, RGB(180,190,225));	
	m_static8.SetFont2(20, 8, 7);	m_static8.SetText(L"OK/NG", BLACK, RGB(180,190,225));	
	m_static11.SetFont2(20, 8, 7);	m_static11.SetText(L"자동시험 시작시각", BLACK, RGB(180,190,225));	
	m_static12.SetFont2(20, 8, 7);	m_static12.SetText(L"단품시험 경과시간", BLACK, RGB(180,190,225));	
	m_staticOK.SetFont2(50, 25, 20);	m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
	m_staticNG.SetFont2(50, 25, 20);	m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));
	m_staticName.SetFont2(30, 12, 10);	//m_staticName.SetText(L"", BLACK, RGB(180, 190, 225));

	m_staticModel.SetFont2(20, 6, 7); m_staticModel.SetText(L"", BLACK, RGB(230,230,230));
	m_staticType.SetFont2(20, 6, 7);	m_staticType.SetText(L"", BLACK, RGB(230,230,230));
	m_staticAccu.SetFont2(20, 8, 7);	m_staticAccu.SetText(L"", BLACK, RGB(230,230,230));
	m_staticLOT.SetFont2(20, 8, 7);		m_staticLOT.SetText(L"", BLACK, RGB(255,255,110));
	m_staticTarget.SetFont2(20, 8, 7);	m_staticTarget.SetText(L"", BLACK, RGB(230,230,230));
	m_staticBarCode.SetFont2(20, 8, 7);	m_staticBarCode.SetText(L"", BLACK, RGB(230,230,230));
	m_staticTargetPercent.SetFont2(20, 8, 7);	m_staticTargetPercent.SetText(L"", BLACK, RGB(230,0,0));
	m_staticTotal.SetFont2(20, 8, 7); m_staticTotal.SetText(L"", BLACK, RGB(230,230,230));
	m_staticOKNG.SetFont2(20, 8, 7);	m_staticOKNG.SetText(L"", BLACK, RGB(230,230,230));
	m_ErrorMsg.SetFont2(25, 12, 10);	m_ErrorMsg.SetText(L"", BLACK, RGB(230,230,230));

	m_staticTimeMasterStart.SetFont2(20, 8, 7);	m_staticTimeMasterStart.SetText(L"", WHITE, BLACK);
	m_staticTimeMasterElapsec.SetFont2(20, 8, 7);	m_staticTimeMasterElapsec.SetText(L"", WHITE, BLACK);

	m_masterTest_OK.SetLedStatesNumber(LIGHT_STATE_SENTINEL);	
	m_masterTest_OK.SetIcon(GRAY_LIGHT, IDI_ICON_NONE, 24, 24);
	m_masterTest_OK.SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 24, 24);
	m_masterTest_OK.SetIcon(RED_LIGHT, IDI_ICON_RED, 24, 24);
	m_masterTest_NG.SetLedStatesNumber(LIGHT_STATE_SENTINEL);	
	m_masterTest_NG.SetIcon(GRAY_LIGHT, IDI_ICON_NONE, 24, 24);
	m_masterTest_NG.SetIcon(RED_LIGHT, IDI_ICON_RED, 24, 24);
	m_masterTest_NG.SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 24, 24);

	m_opMode = MODE_OPERATOR;
	m_OperatorMode.SetText(L"작업자 모드", BLACK, RGB(255,240,255));	
#ifdef _FI
	m_btnNGOut.SetWindowText(L"DOOR OPEN"); 
#else
	m_btnNGOut.SetWindowText(L"DCR DOWN"); 
#endif
	
	pImgBuf[0] =cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 3);
	//pImgBuf[1] =cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 3);
	//pImgBuf[2] =cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 3);
	//pImgBuf[3] =cvCreateImage(cvSize(LIVE_SIZE_W, LIVE_SIZE_H),IPL_DEPTH_8U , 3);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.

}

void CnBizLDCTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CnBizLDCTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if(paintImageReady[0])
		{
			CDC* pDC;
			CRect rect;
			pDC = m_pcMainView.GetDC();
			m_pcMainView.GetClientRect(&rect);
			DisplayIplImg(pImgBuf[0], pDC, rect);
			ReleaseDC(pDC);
		}
		CDialogEx::OnPaint();
	}
}


BOOL CnBizLDCTesterDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	switch(pMsg->message)
	{
	case WM_KEYDOWN:
		switch(pMsg->wParam)
		{ 
		case VK_RETURN:
			return TRUE;
			break;
		case VK_ESCAPE:			
			return TRUE;
			break;
		case VK_F7:
			break;
		}
	case WM_SYSKEYDOWN:
		switch(pMsg->wParam)
		{ 
		case VK_F4:			
			return TRUE;
			break;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CnBizLDCTesterDlg::DisplayImage(IplImage* srcimg, int item)
{
	CWnd* pWnd = GetDlgItem(item);
	CDC* pDCc = pWnd->GetDC();
	CRect rect;
	pWnd->GetClientRect(&rect);
	IplImage* img = cvCreateImage(cvSize(rect.Width(), rect.Height()), srcimg->depth, srcimg->nChannels);
	cvResize(srcimg, img);
	uchar buffer[sizeof(BITMAPINFOHEADER)*1024];
	BITMAPINFO* bmi = (BITMAPINFO*)buffer;
	int bmp_w = 0;
	int bmp_h = 0;
	int bpp = 0;
	bmp_w = img->width;
	bmp_h = img->height;
	bpp = (img->depth & 255)* img->nChannels;
	FillBitmapInfo(bmi, bmp_w, bmp_h, bpp, img->origin);
	int from_x = 0;
	int from_y = 0;
	int sw = 0;
	int sh = 0;
	from_x = MIN(0, bmp_w-1);
	from_y = MIN(0, bmp_h-1);
	sw = MAX(MIN(bmp_w - from_x, rect.Width()), 0);
	sh = MAX(MIN(bmp_h - from_y, rect.Height()), 0);
	SetDIBitsToDevice(pDCc->m_hDC, rect.left, rect.top, sw, sh, from_x, from_y, 
		0, sh, img->imageData + from_y * img->widthStep, bmi, 0);
	cvReleaseImage(&img);
	pWnd->ReleaseDC(pDCc);
}

void CnBizLDCTesterDlg::FillBitmapInfo(BITMAPINFO* bmi, int width, int height, int bpp, int origin)
{
	_ASSERT(bmi && width >= 0 && height >=0 && (bpp == 8 || bpp ==24 || bpp ==32));
	BITMAPINFOHEADER* bmih = &(bmi->bmiHeader);
	memset(bmih, 0, sizeof(*bmih));
	bmih->biSize = sizeof(BITMAPINFOHEADER);
	bmih->biWidth = width;
	bmih->biHeight = origin ? abs(height) : -abs(height);
	bmih->biPlanes = 1;
	bmih->biBitCount = (unsigned short)bpp;
	bmih->biCompression = BI_RGB;
	if(bpp == 8){
		RGBQUAD* palette = bmi->bmiColors;
		int i;
		for(i=0;i<256;i++){
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CnBizLDCTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CnBizLDCTesterDlg::m_fnInit()
{
	CDialog::Create(IDD, this);
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	m_QRScan_count = 0;
}


void CnBizLDCTesterDlg::m_fnDeInit()
{

	//DestroyWindow();
	CDialog::OnClose();
}


void CnBizLDCTesterDlg::m_fnAddLog(CString strLog)
{
	CString strCurrentTime;

	CTime ctCurrentTime;
	ctCurrentTime = CTime::GetCurrentTime();
	strCurrentTime = ctCurrentTime.Format(_T("%d일,%H시%M분%S초 : "));

	strLog = strCurrentTime + strLog;

	m_fnUpdateListofLog(strLog.GetBuffer(0));
	strLog.ReleaseBuffer();
}

void CnBizLDCTesterDlg::m_fnUpdateListofLog(TCHAR* szLog)
{
	//	int listcount = m_ctrlListView.GetCount();
	if(m_listLogView.GetCount() == 100)
		m_listLogView.DeleteString(99);

	m_listLogView.InsertString(0, szLog);
	m_listLogView.SetCurSel(0);
}


void CnBizLDCTesterDlg::OnClose()
{
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	OnBnClickedBtnManu();
	Sleep(100);
	G_SystemModeData.unSystemMode = SYSTEM_EXIT;
	G_MainWnd->m_DataHandling.m_fnInspectDataReset();
	G_AddLog(3, L"G_MainWnd->m_DataHandling.m_fnInspectDataReset 종료");
	G_MainWnd->m_InstrumentDlg.safeCloseInstrument();
	G_AddLog(3, L"G_MainWnd->m_InstrumentDlg.safeCloseInstrument 종료");
	::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CLOSE, 0, 0);
}


HBRUSH CnBizLDCTesterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CnBizLDCTesterDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case TIMER_FILE_UPDATE:
		CString strVal;
		CTime ctime = CTime::GetCurrentTime();
		int day = ctime.GetDay();
		if (day != G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_DAY])
		{
			//day update
			G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_DAY] = day;
			strVal.Format(L"%d", day);
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LABEL_SERIAL_DAY], strVal);
			//clear serial no.
			for (int i = 0; i < LABEL_SERIAL_MAX; i++)
			{
				G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LABEL_SERIAL_0 + i], L"0");
				G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + i] = 0;
			}

			//Maker image data folder
			G_MainWnd->m_DataHandling.m_fnMakeDirectoryNewDay();
		}

		//Auto Delete Image file
		//G_MainWnd->m_DataHandling.m_fnAutoDelete(DATA_IMAGE_FOLDER, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_AUTO_DELETE]);
		//G_MainWnd->m_DataHandling.m_fnAutoDelete(DATA_IMAGE_NG_FOLDER, G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_AUTO_DELETE]);

		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}


afx_msg LRESULT CnBizLDCTesterDlg::OnUiUpdateCmd(WPARAM wParam, LPARAM lParam)
{
	if(wParam == UW_CLEAR)
	{
		G_MainWnd->m_ServerDlg.clearGridMainResult((int)lParam);
	}	
	else if(wParam == UM_TIME_UPDATE)
	{
		//CInterfaceSocket::FromCimTimeData timeData;
		//memcpy((void*)&timeData, (void*)lParam, sizeof(CInterfaceSocket::FromCimTimeData));

		//int nID = timeData.nID;
		//GetDlgItem(nID)->SetWindowText(timeData.chReceiveTime);

		//m_staPanelID.SetText(G_MainWnd->m_DataHandling.strPanelID, WHITE, BLACK);		
		//m_staEQPID.SetText(G_MainWnd->m_DataHandling.strDeviceID.Left(14), WHITE, BLACK);		

	}
	else if(wParam == UM_INIT_UPDATE)
	{
		////////////////////////////////////
		//Production count
		CString strValueData;
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
		m_staticAccu.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
		m_staticTarget.SetText(strValueData, BLACK, RGB(230,230,230));

		strValueData.Format(_T("%4.1f %%"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT]*1.0f);
		m_staticTargetPercent.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
		m_staticTotal.SetText(strValueData, BLACK, RGB(230,230,230));
		strValueData.Format(_T("%d/%d"), G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK],G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG]);
		m_staticOKNG.SetText(strValueData, BLACK, RGB(230,230,230));


	}
	else if(wParam == UM_STATUS_UPDATE)
	{
		testResultUIUpdate();
		masterResultUIUpdate();
	}
	else if(wParam == UM_TEST_UPDATE)
	{
		int stg, step;
		step = (int)(lParam & 0x00FF);
		stg  = (int)((lParam>>8) & 0x00FF);

		testStepUIUpdate(stg, step);
	}
	else if(wParam == UM_INSPECT_ERROR_UPDATE)
	{
		m_ErrorMsg.SetText(ST_INSPECT_ERROR[(	int)lParam], BLACK, RED);
	}
	else if(wParam == UM_INSPECT_LOG_UPDATE)
	{
		wchar_t log[1024];
		wcscpy_s(log, ST_INSPECT_LOG[(int)lParam]);
		G_AddLog(3, log);
	}
	else if(wParam == UM_AXIS_ERROR_UPDATE)
	{
		m_ErrorMsg.SetText(ST_AXIS_ERROR[(	int)lParam], BLACK, RED);
	}
	else if(wParam == UM_AXIS_LOG_UPDATE)
	{
		wchar_t log[1024];
		wcscpy_s(log, ST_AXIS_LOG[(int)lParam]);
		G_AddLog(3, log);
	}
	else if(wParam == UM_SHIFT_UPDATE)//DATA SHIFT
	{
#ifdef _FI
		m_fnUpdateShiftResult();
#else
		m_fnUpdateShiftResultPI();
#endif
	}

	return TRUE;
}


void CnBizLDCTesterDlg::m_fnTimeCheck(int nTimeIndex)
{
	CTime createImageTime = 0;
	CString  strCreateTime_M, strCreateTime_S;

	createImageTime = CTime::GetCurrentTime();
	strCreateTime_M = createImageTime.Format(L"%M");
	strCreateTime_S = createImageTime.Format(L"%S");

	int nReciveTime = (_wtoi(strCreateTime_M) * 60 )+ _wtoi(strCreateTime_S);

	if (nTimeIndex == TIME_CHECK_EQP_ALIVE)
	{
		m_nReciveAliveTime = nReciveTime;
	}
	else if (nTimeIndex == TIME_CHECK_NOW)					 // Now Check Timer
	{
		m_nNowTime = nReciveTime;
	}
}




int CnBizLDCTesterDlg::resultGridInit(void)
{
	
	try {
		nDayReportCount = 0;
		m_ctrlGridResult.SetRowCount(5000);
		m_ctrlGridResult.SetColumnCount(7);
		m_ctrlGridResult.SetFixedRowCount(1);		
		m_ctrlGridResult.SetFixedColumnCount(1);
		m_ctrlGridResult.SetListMode(TRUE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
//		return ;
	}

	for (int row = 0; row < m_ctrlGridResult.GetRowCount(); row++)
	{
		for (int col = 0; col < m_ctrlGridResult.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{							
				if (col == 0)
				{					
					Item.strText.Format(_T(" NUM "));					
				}
				else if (col == 1)
				{					
					Item.strText.Format(_T("                 Time                 "));					
				}
				else if( col == 2)
				{
					Item.strText.Format(_T("            Model ID            "));					
				}
				else if( col == 3)
				{
					Item.strText.Format(_T("           Device ID           "));
				}
				else if( col == 4)
				{
					Item.strText.Format(_T("      Result      "));					
				}
				else if( col == 5)
				{
					Item.strText.Format(_T("                                                NG data                                              "));					
				}
				else if( col == 6)
				{
						Item.strText.Format(_T("  NG Count  "));					
				}
			}

			if(col == 0)
			{
				if(row != 0)
				{
					Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
					Item.strText.Format(_T("%d"), row);					
				}
			}
		/*	else if(col == 1)
			{
				Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
				int nSize = sizeof(ST_TABLE_RECIPE_0_COL) / 4;
				if(row-1 < nSize && row > 0)
					Item.strText.Format(_T("%s"), ST_TABLE_RECIPE_0_COL[row-1]);
			}*/
			m_ctrlGridResult.SetItem(&Item);
		}
	}

	m_ctrlGridResult.AutoSize(GVS_BOTH);
	//m_ctrlGridResult.SetTextColor(RGB(0, 0, 105));
	m_ctrlGridResult.AutoSizeColumns();
	//m_ctrlGridResult.ExpandToFit(true);
	m_ctrlGridResult.SetEditable(FALSE);
	m_ctrlGridResult.Refresh();

	return 0;
}

void CnBizLDCTesterDlg::OnBnClickedBtnAuto()
{
	CString msg;
	if(G_MainWnd->m_InspectThread.checkStatus == CHECK_NG){
		AfxMessageBox(_T("관리자 모드로 Unload 지그를 꺼낸 후 NG를 클리어해 주세요"),MB_ICONHAND);
		return;
	}

	if(G_SystemModeData.unSystemError != SYSTEM_OK  ){
		AfxMessageBox(_T("SYSTEM ERROR STATE"),MB_ICONHAND);
		return;
	}

	if(!G_MainWnd->m_PIODlg.readDI(IN_EM_STOP, CARD_IO) ){
	//if(G_MainWnd->m_PIODlg.readDI(IN_EM_STOP, CARD_IO) ){
		AfxMessageBox(_T("EM STOP STATE"),MB_ICONHAND);
		return;
	}
#ifdef _FI
	if (G_MainWnd->m_LabelPrint.customer == MOBIS)
		if (G_MainWnd->m_LabelPrint.strLotMobis == L"")
		{
			AfxMessageBox(_T("Lot No. Not Set"), MB_ICONHAND);
			return;
		}

	if (G_MainWnd->m_LabelPrint.customer == LGE)
		if (G_MainWnd->m_LabelPrint.strLotLGE == L"")
		{
			AfxMessageBox(_T("Lot No. Not Set"), MB_ICONHAND);
			return;
		}

	if (G_MainWnd->m_LabelPrint.customer == MANDO)
	if (G_MainWnd->m_LabelPrint.strLotMANDO == L"")
		{
			AfxMessageBox(_T("Lot No. Not Set"), MB_ICONHAND);
			return;
		}
#endif
//#ifndef _DEBUG
//	//instrument comm check
//	G_MainWnd->m_InstrumentDlg.checkInstrumentComm();
//	for(int i = VI_LCR; i<= VI_WG; i ++)
//	{
//#ifndef _FI
//		if(i != VI_LCR && i != VI_DCR) continue; 
//#endif
//		if(!G_MainWnd->m_InstrumentDlg.vi[i].connected){
//			msg.Format(_T("%s 연결이 안되어있습니다"), ST_INSTRUMENT[i]);
//			AfxMessageBox(msg, MB_ICONHAND);
//			return;
//		}
//	}
//#endif

	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL )
	{
		do{
			G_MainWnd->m_InspectThread.suspendCount_Input = G_MainWnd->m_InspectThread.m_fnResumeStep_Input();
		}while(G_MainWnd->m_InspectThread.suspendCount_Input > 0);
	}
	//m_btnAuto.EnableWindow(FALSE);
	//m_btnManual.EnableWindow(TRUE);
	G_SystemModeData.unSystemMode = SYSTEM_AUTO;
	m_btnAuto.SetColorChange(GREEN, BLACK);
	m_btnManual.SetColorChange(WHITE, BLACK);
	G_AddLog(3,L"Auto Mode");
	//160217_TEST
	//G_MainWnd->m_DataHandling.m_fnDayReportWrite(TRUE);
}


void CnBizLDCTesterDlg::OnBnClickedBtnManu()
{
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN )
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdAutoRun();
		G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO )
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}
	//m_btnAuto.EnableWindow(TRUE);
	//m_btnManual.EnableWindow(FALSE);
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	m_btnAuto.SetColorChange(WHITE, BLACK);
	m_btnManual.SetColorChange(GREEN, BLACK);
	m_btnStart.SetColorChange(WHITE, BLACK);
	m_btnStop.SetColorChange(WHITE, BLACK);

	// OK/NG CLEAR
	m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
	m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));

	G_AddLog(3,L"Manual Mode");
}


void CnBizLDCTesterDlg::OnBnClickedBtnStart()
{
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){

		if(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_LCR_LOAD_CORRECTION]==1)
		{
			if(G_MainWnd->m_DataHandling.m_FlashData.data[LCR_CORRECTION	] != 1){
				//if(G_MainWnd->m_InspectThread.fLCRLoadCorrect != 1){
				AfxMessageBox(_T("LCR Load Correction을 실시해 주세요"),MB_ICONHAND);
				return;
				//}
			}
		}

		if(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_DCR_ZERO_ADJUST]==1)
		{
			if(G_MainWnd->m_DataHandling.m_FlashData.data[DCR_ZERO_ADJUST	] != 1){
				//if(G_MainWnd->m_InspectThread.fLCRLoadCorrect != 1){
				AfxMessageBox(_T("DCR Zero Adjust를 실시해 주세요"),MB_ICONHAND);
				return;
				//}
			}
		}

		if(G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_MASTER_TEST]==1)
		{
			if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] != 1){
				if(nMasterOK != 1){
					AfxMessageBox(_T("양품 마스터 시험을 실시해 주세요"),MB_ICONHAND);
					return;
				}
			}
			//else if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] != 1 ){ //임시
			//	if(nMasterNG != 1){
			//		AfxMessageBox(_T("불량 마스터 시험을 실시해 주세요"),MB_ICONHAND);
			//		return;
			//	}
			//}
		}

		G_SystemModeData.unSystemMode = SYSTEM_AUTO_RUN;
		m_btnStart.SetColorChange(GREEN, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.resumeAutoRun();
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
		G_AddLog(3,L"Auto Start");

		//Time display
		CString strTimeAutoRun;
		ctimeAutoRun = CTime::GetCurrentTime();
		strTimeAutoRun = ctimeAutoRun.Format(L"%Y/%m/%d %H:%M:%S");
		m_staticTimeMasterStart.SetText(strTimeAutoRun, WHITE, BLACK);

	}
}


void CnBizLDCTesterDlg::OnBnClickedBtnStop()
{
	int iRet, nDone;
	G_MainWnd->m_InspectThread.m_csCheck.Lock();
	G_MainWnd->m_InspectThread.holdAutoRun();
	G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
		G_SystemModeData.unSystemMode = SYSTEM_AUTO;
		m_btnStart.SetColorChange(WHITE, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_AddLog(3,L"AUTO STOP");
	}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){
		m_btnStart.SetColorChange(WHITE, BLACK);
		m_btnStop.SetColorChange(WHITE, BLACK);
		G_AddLog(3,L"MANUAL STOP");
	}

	G_MainWnd->m_InstrumentDlg.safeCloseInstrument();

	//Axis Stop
	//LCR Axis stop
	for(int i = 0; i < 8; i++){
		pmiAxCheckDone(CARD_NO, i, &nDone);
		if(nDone == emRUNNING)
			iRet = pmiAxStop(CARD_NO, i);
	}
	//SHUTTLE Axis stop
	for(int i = 0; i < 4; i++){
		pmiAxCheckDone(CARD_NO2, i, &nDone);
		if(nDone == emRUNNING)
			iRet = pmiAxStop(CARD_NO2, i);
	}
}

//NG CLEAR
void CnBizLDCTesterDlg::OnBnClickedBtnMainExit()
{
	//CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	//G_SystemModeData.unSystemMode = SYSTEM_EXIT;
	//G_MainWnd->m_DataHandling.m_fnInspectDataReset();
	//pMainWnd->SendMessage(WM_CLOSE, 0, 0);

	if(G_MainWnd->m_InspectThread.checkStatus == CHECK_NG)
	{
		if(m_opMode == MODE_OPERATOR)
		{
			AfxMessageBox(_T("관리자 모드로 리셋할 수 있습니다"),MB_ICONHAND);
			return;
		}else{
			G_MainWnd->m_InspectThread.checkStatus = 0;
			//clear alarm
			m_staticNG.SetText(_T("NG"), RGB(210,210,210), RGB(230,230,230));
			m_ErrorMsg.SetText(_T(""), BLACK, RGB(230,230,230));
			//clear NG result
			clearNGResult();
		}
	}//if(G_MainWnd->m_InspectThread.checkStatus == CHECK_NG)

#ifdef _FI
	if(G_MainWnd->m_InspectThread.m_fnGetOldBatchStep(UNLOAD) == STEP4)
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(UNLOAD, STEP11);
#else
	if(G_MainWnd->m_InspectThread.m_fnGetOldBatchStep(DCR) == STEP5)
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(DCR, STEP9);
#endif
	G_MainWnd->m_ServerDlg.m_btnNGClear.SetColorChange(WHITE, BLACK);
	G_MainWnd->m_PIODlg.buzzstop = 0;
}

void CnBizLDCTesterDlg::clearNGResult()
{
	int i, j;
	//LCR
	for(j=0; j<MAX_L_TEST; j++){
		i=j+1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_L_TEST; j++){

	//OSC
	for(j=0; j<MAX_OSC_TEST; j++){
		i = j + MAX_L_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_OSC_TEST; j++){

	//DCR
	for(j=0; j<MAX_DCR_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_DCR_TEST; j++){

	//HIPOT
	for(j=0; j<MAX_HIPOT_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_HIPOT_TEST; j++){

	//IR
	for(j=0; j<MAX_IR_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_HIPOT_TEST; j++){

	//VI
	for(j=0; j<MAX_VI_TEST; j++){
		i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + MAX_IR_TEST + 1;
		if(G_MainWnd->m_RecipeSetDlg.part_Test.viTestInfo[j].bTest)
		{
			m_GridMainResult.GetCell(i, 8)->SetText(L"");
			m_GridMainResult.GetCell(i, 8)->SetBackClr(WHITE);
		}
	}//for(j=0; j<MAX_VI_TEST; j++){
	m_GridMainResult.Refresh();
	return ;
}


int CnBizLDCTesterDlg::initGridMainResult(void)
{
	//Grid init

	//cell bg color
	//	m_Grid.GetDefaultCell(FALSE, FALSE)->SetBackClr(RGB(0xFF, 0xFF, 0xE0));
	// m_Grid.SetCellType(1,1, RUNTIME_CLASS(CGridCellCheck));
	//m_GridMainResult.GetDefaultCell(row, col)->SetBackClr(RGB(0x50, 0x50, 0x50));

	// system param grid **********************
	try {
		m_GridMainResult.SetRowCount(MAX_TOTAL_TEST+1);
		m_GridMainResult.SetColumnCount(10);
		m_GridMainResult.SetFixedRowCount(1);		
		m_GridMainResult.SetFixedColumnCount(1);
		m_GridMainResult.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridMainResult.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridMainResult.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;

				if (col == 0)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 검사 항목 "));					
				}
				else if( col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 시험 단자 "));
				}
				else if( col == 2)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("       시험 조건       "));					
				}
				else if (col == 3)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 양품 기준 "));					
				}
				else if( col == 4)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" LCR "));
				}
				else if( col == 5)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" OSC "));					
				}
				else if( col == 6)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" DCR "));
				}
				else if( col == 7)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" HIPOT/IR "));					
				}
				else if( col == 8)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" VISION "));					
				}
				else if( col == 9)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("수동 시험"));					
				}

			}
			else
			{
				if(col == 0)
				{
					if(row == 1)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" 인덕턴스 "));					
					}
					else if(row == 2)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("     (0A) "));					
					}
					else if(row == 8)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("     (전류인가) "));					
					}
					else if(row == 12)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("     (Leakage) "));					
					}
					else if(row == 15)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" Oscilloscope "));					
					}
					else if(row == 16)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" DCR "));					
					}
					else if(row == 22)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" 내전압 "));					
					}
					else if(row == 27)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" 절연저항 "));					
					}
					else if(row == 32)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" 비전검사 "));					
					}
				}
				else if(col == 9)
				{
					m_GridMainResult.GetCell(row, col)->SetBackClr(RGB(220,220,220));
					//Item.crBkClr = (RGB(0x50, 0x50, 0x50));
					//Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
				}	
			}

			m_GridMainResult.SetItem(&Item);
		}
	}

	//m_GridMainResult.AutoSize(GVS_BOTH);
	m_GridMainResult.SetTextColor(RGB(0, 0, 105));
	m_GridMainResult.ExpandToFit(true);
	m_GridMainResult.SetEditable(FALSE);
	m_GridMainResult.Refresh();

	return 0;
}

int CnBizLDCTesterDlg::clearGridMainResult(void)
{
	clearGridMainResult(STG_LCR);
	clearGridMainResult(STG_OSC);
	clearGridMainResult(STG_DCR);
	clearGridMainResult(STG_HIPOT_IR);
	clearGridMainResult(STG_VI_UNLOAD);
	return 0;
}

int CnBizLDCTesterDlg::clearGridMainResult(int stg)
{
	int rowB, rowE, col;
	switch(stg)
	{
		case STG_LCR:
			rowB = 0;
			rowE = MAX_L_TEST;
			col = 4;
			break;
		case STG_OSC:
			rowB = MAX_L_TEST;
			rowE = MAX_L_TEST + MAX_OSC_TEST;
			col = 5;
			break;
		case STG_DCR:
			rowB = MAX_L_TEST + MAX_OSC_TEST;
			rowE = MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST;
			col = 6;
			break;
		case STG_HIPOT_IR:
			rowB = MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST;
			rowE = MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST;
			col = 7;
			break;
		case STG_VI_UNLOAD:
			rowB = MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST;
			rowE = MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST + 1;
			col = 8;

			// OK/NG CLEAR
			m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
			m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));

			break;
	}
	for (int row = rowB + 1; row < rowE + 1; row++)
	{
		//for (int col = 4; col <= 5; col++)//측정값, 시험결과
		//{
			m_GridMainResult.GetCell(row, col)->SetText(L"");
			if(m_GridMainResult.GetCell(row, col)->GetBackClr() == GREEN || m_GridMainResult.GetCell(row, col)->GetBackClr() == RED)
				m_GridMainResult.GetCell(row, col)->SetBackClr(WHITE);
		//}
	}
	m_GridMainResult.Refresh();
	return 0;
}

void CnBizLDCTesterDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}

void CnBizLDCTesterDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	int i, j;
	ViStatus ret;
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	int NGRetry = 0;
	long param;

	if(pItem->iColumn != 9)
	return;



	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(_T("매뉴얼 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	//
	if(pItem->iColumn == 9)
	{
		j = pItem->iRow -1;
		//unit measure
		if(j >=0 && j < MAX_L_TEST) //LCR test
		{

			if(G_MainWnd->m_InstrumentDlg.vi[VI_LCR].connected != 1){
				AfxMessageBox(_T("계측기 통신 연결 안되었습니다"),MB_ICONHAND);
				return;
			}
			//if(G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_UP, CARD_IO)!=1){
			//	AfxMessageBox(_T("지그가 검사 위치에 있지 않습니다."),MB_ICONHAND);
			//	return;m_AxisDlg.axisData
			//}
			if(static_cast<int>(G_MainWnd->m_AxisDlg.axisData.absPos[G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step][10]) != j+1){
				AfxMessageBox(_T("계측기 프로브 이동 위치가 맞지 않습니다."),MB_ICONHAND);
				return;
			}
			if(!G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest){
				AfxMessageBox(_T("검사 항목으로 설정되어있지않습니다."),MB_ICONHAND);
				return;
			}
			i = j;
			ret = G_MainWnd->m_InstrumentDlg.measureLcr(i, G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i]);
			if(ret == VI_SUCCESS){
				//Result
				double l = atof( G_MainWnd->m_InstrumentDlg.vi[VI_LCR].str_result);
				G_MainWnd->m_InspectThread.m_PartData[STG_LCR].l[i] = l;
				if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
					l += G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].offset*(+1.0E-6);
					G_AddLog(3,L"LCR Measure Offset applied [%f]", G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].offset);
				}
				if( G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].lowerLimit*(+1.0E-6) <= l && l <=  G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].upperLimit*(+1.0E-6)){
					G_MainWnd->m_InspectThread.m_PartData[STG_LCR].lPass[i] = 1;
				}else{
					G_MainWnd->m_InspectThread.m_PartData[STG_LCR].lPass[i] = 0;
				}
			}else{ //test error

			}

			param = STG_LCR * 0x100 + i;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
		}
#ifdef _FI
		if(j == (MAX_L_TEST + MAX_OSC_TEST - 1)) //OSC test
		{
			if(G_MainWnd->m_InstrumentDlg.vi[VI_OSC].connected != 1){
				AfxMessageBox(_T("계측기 통신 연결 안되었습니다"),MB_ICONHAND);
				return;
			}
			if(G_MainWnd->m_PIODlg.readDI(IN_OSC_LIFT_UP, CARD_IO)!=1){
				AfxMessageBox(_T("지그가 검사 위치에 있지 않습니다."),MB_ICONHAND);
				return;
			}
			if(!G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[j-MAX_L_TEST].bTest){
				AfxMessageBox(_T("검사 항목으로 설정되어있지않습니다."),MB_ICONHAND);
				return;
			}
			i = j-MAX_L_TEST;
RETRY:
			ret = G_MainWnd->m_InstrumentDlg.measureOsc(i, G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[i]);
			if(ret == VI_SUCCESS){
				//Result
				swprintf_s(G_MainWnd->m_InspectThread.m_PartData[STG_OSC].phase,L"%s", G_MainWnd->m_InstrumentDlg.vi[VI_OSC].str_result);
				CString str( G_MainWnd->m_InstrumentDlg.vi[VI_OSC].str_result);
				if(str.Left(2) == "OK"){
					G_MainWnd->m_InspectThread.m_PartData[STG_OSC].phasePass[i] = 1;
				}else{
					if(NGRetry++ < 1)
						goto RETRY;
					G_MainWnd->m_InspectThread.m_PartData[STG_OSC].phasePass[i] = 0;
				}
			}else{ //test error

			}

			param = STG_OSC * 0x100 + i;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);

		}
#endif
		if(j >= (MAX_L_TEST + MAX_OSC_TEST) && j < (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST)) //DCR test
		{
			if(G_MainWnd->m_InstrumentDlg.vi[VI_DCR].connected != 1){
				AfxMessageBox(_T("계측기 통신 연결 안되었습니다"),MB_ICONHAND);
				return;
			}
			if(G_MainWnd->m_PIODlg.readDI(IN_DCR_LIFT_UP, CARD_IO)!=1){
				AfxMessageBox(_T("지그가 검사 위치에 있지 않습니다."),MB_ICONHAND);
				return;
			}
			if(!G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j-MAX_L_TEST-MAX_OSC_TEST].bTest){
				AfxMessageBox(_T("검사 항목으로 설정되어있지않습니다."),MB_ICONHAND);
				return;
			}
			i = j-MAX_L_TEST-MAX_OSC_TEST;
			ret = G_MainWnd->m_InstrumentDlg.measureDcr(i, G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i]);
			if(ret == VI_SUCCESS){
				//Result
				double r = atof(G_MainWnd->m_InstrumentDlg.vi[VI_DCR].str_result);
				G_MainWnd->m_InspectThread.m_PartData[STG_DCR].r[i] = r;
				if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
					r += G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].offset*(+1.0E-3);
				}
				if( r <=  G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].upperLimit*(+1.0E-3)){
					G_MainWnd->m_InspectThread.m_PartData[STG_DCR].rPass[i] = 1;
				}else{
					G_MainWnd->m_InspectThread.m_PartData[STG_DCR].rPass[i] = 0;
				}
			}else{ //test error

			}

			param = STG_DCR * 0x100 + i;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
		}
#ifdef _FI
		if(j >= (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST) && j < (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST)) //HIPOT test
		{

			if(G_MainWnd->m_InstrumentDlg.vi[VI_HIP].connected != 1){
				AfxMessageBox(_T("계측기 통신 연결 안되었습니다"),MB_ICONHAND);
				return;
			}
			if(G_MainWnd->m_PIODlg.readDI(IN_HIPOTIR_LIFT_UP, CARD_IO)!=1){
				AfxMessageBox(_T("지그가 검사 위치에 있지 않습니다."),MB_ICONHAND);
				return;
			}
			if(!G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j-MAX_L_TEST-MAX_OSC_TEST - MAX_DCR_TEST].bTest){
				AfxMessageBox(_T("검사 항목으로 설정되어있지않습니다."),MB_ICONHAND);
				return;
			}
			ret = 1; //STOPPED
			int nWait = 0;
			i = j-MAX_L_TEST-MAX_OSC_TEST-MAX_DCR_TEST;
					ret = G_MainWnd->m_InstrumentDlg.measureHipotStart(i, G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i]);
					while(ret != 0){
						Sleep(1000);
						ret = G_MainWnd->m_InstrumentDlg.measureHipotStatus(i, G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i]);
						//timeout here
						if(nWait++ > 10){//Time Out Error
							AfxMessageBox(_T("내전압 계측기 통신 TIME OUT 에러"),MB_ICONHAND);
							return;
						}
					}
					ret = G_MainWnd->m_InstrumentDlg.measureHipotResult(i, G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i]);
					if(ret == VI_SUCCESS){
						//Result
						double ia = atof(G_MainWnd->m_InstrumentDlg.vi[VI_HIP].str_result);
						if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
							ia += G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].offset*(+1.0E-3);
							G_AddLog(3,L"HIPOT MEASURE OFFSET APPLIED [%f] ", G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].offset);
						}
						G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].i[i] = ia;
						if( G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].lowerLimit*(+1.0E-3) <= i && i <=  G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].upperLimit*(+1.0E-3)){
							G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].iPass[i] = 1;
						}else{
							G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].iPass[i] = 0;
						}
					}else{ //test error

					}

			param = STG_HIPOT_IR * 0x100 + i;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
		}
		if(j >= (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST) && j < (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST+MAX_IR_TEST)) //IR test
		{
			if(G_MainWnd->m_InstrumentDlg.vi[VI_IR].connected != 1){
				AfxMessageBox(_T("계측기 통신 연결 안되었습니다"),MB_ICONHAND);
				return;
			}

			if(G_MainWnd->m_PIODlg.readDI(IN_HIPOTIR_LIFT_UP, CARD_IO)!=1){
				AfxMessageBox(_T("지그가 검사 위치에 있지 않습니다."),MB_ICONHAND);
				return;
			}
			if(!G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j-MAX_L_TEST-MAX_OSC_TEST - MAX_DCR_TEST - MAX_HIPOT_TEST].bTest){
				AfxMessageBox(_T("검사 항목으로 설정되어있지않습니다."),MB_ICONHAND);
				return;
			}
			i = j-MAX_L_TEST-MAX_OSC_TEST-MAX_DCR_TEST-MAX_HIPOT_TEST;
			ret = G_MainWnd->m_InstrumentDlg.measureIrStartCom(i, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i]);
			if(ret != VI_SUCCESS){
				//Error
				AfxMessageBox(_T("IR 계측기 통신 에러"),MB_ICONHAND);
				return;
			}
			while(ret != 1){//12: TESTING, 16: END, 1: DONE
				ret = G_MainWnd->m_InstrumentDlg.measureIrStatusCom(i, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i]);
				//timeout 
				if(ret < 0){
					//Error
					AfxMessageBox(_T("IR 계측기 통신 에러"),MB_ICONHAND);
					return;
				}
			}

			ret = G_MainWnd->m_InstrumentDlg.measureIrResultCom(i, G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i]);
			if(ret == VI_SUCCESS){
				//Result
				double irohm = atof(G_MainWnd->m_InstrumentDlg.vi[VI_IR].str_result);
				if(G_MainWnd->m_DataHandling.m_SystemParam.correction == 1){
					irohm += G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].offset*(+1.0E+6);
					G_AddLog(3,L"IR MEASURE OFFSET APPLIED [%F] ", G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].offset);
				}
				G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].ir[i] = irohm;

				if( G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].lowerLimit*(+1.0E+6) <= irohm ){
					G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].irPass[i] = 1;
				}else{
					G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].irPass[i] = 0;
				}

			}else{ //test error

			}

			param = STG_HIPOT_IR * 0x100 + i + MAX_HIPOT_TEST;
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_TEST_UPDATE, (LPARAM)param);
		}
		if (j >= (MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST) + 1) //VI test
		{
			::PostMessage(G_MainWnd->m_InspectThread.m_vi.m_hWnd, UM_VI, (WPARAM)UW_VINSPECTION_G, NULL);

		}


#endif
	}//if(pItem->iColumn == 6)
}

//connect LCR
void CnBizLDCTesterDlg::OnStnClickedStatusInstrument(UINT nID)
{
	BOOL bRes = TRUE;
	CString str;
	ViStatus ret;
	int nRet;
	int viIdx = nID - IDC_STATUS_LCR;
	if(viIdx != VI_IR)
	{
		if(G_MainWnd->m_InstrumentDlg.vi[viIdx].connected != 1)
		{
			//ret = G_MainWnd->m_InstrumentDlg.visaClose(viIdx);
			ret = G_MainWnd->m_InstrumentDlg.open(viIdx);
			if(ret ==VI_SUCCESS) 
			{
				m_statusInstrument[viIdx].SetBackColor(RGB(0,0xff, 0));
			}
		}else{
			G_MainWnd->m_InstrumentDlg.vi[viIdx].err = G_MainWnd->m_InstrumentDlg.do_query_string(viIdx,"*IDN?"); 
			G_MainWnd->m_InstrumentDlg.vi[viIdx].err = G_MainWnd->m_InstrumentDlg.do_query_string(viIdx,"*IDN?"); 
			if (G_MainWnd->m_InstrumentDlg.vi[viIdx].err != VI_SUCCESS) { 
				G_MainWnd->m_InstrumentDlg.vi[viIdx].connected = 0;
				//ret = G_MainWnd->m_InstrumentDlg.visaClose(viIdx);
				G_AddLog(3,L"vi[%d]: %s\n", viIdx,L"Disconnected");
				 }
			G_AddLog(3, L"vi[%d]: %s\n", viIdx, ConvertMultybyteToUnicode(G_MainWnd->m_InstrumentDlg.vi[viIdx].str_result));
		}
	}else
	{
		//Serial port[3]
		if(!G_MainWnd->m_SerialInterface.m_CommThread[COM_TOS7200].m_bConnected)
		{
			if(G_MainWnd->m_SerialInterface.m_CommThread[COM_TOS7200].m_fnInitialize(G_MainWnd->m_SerialInterface.m_nComportIndex[COM_TOS7200]) == FALSE)  //
			{
				G_MainWnd->m_SerialInterface.m_CommThread[COM_TOS7200].m_bConnected = 0;
				G_AddLog(3,L"Comm Port %d 연결 실패", G_MainWnd->m_SerialInterface.m_nComportIndex[COM_TOS7200]+1); 
				bRes = FALSE;
			}else
			{
				m_statusInstrument[viIdx].SetBackColor(RGB(0,0xff, 0));
			}
		}else{
				str = G_MainWnd->m_SerialInterface.do_query_string_com(COM_TOS7200,"*IDN?");
				nRet = str.Find( L"TOS");
				if(nRet > 0){
					G_AddLog(3,L"vi[%d]: %s %s", viIdx, ST_INSTRUMENT[viIdx], str.GetBuffer()); str.ReleaseBuffer();
					m_statusInstrument[viIdx].SetBackColor(GREEN);
				}else{
					G_MainWnd->m_InstrumentDlg.vi[viIdx].connected = 0;
					m_statusInstrument[viIdx].SetBackColor(RGB(230,230,230));
				}
		}
	}
}



void CnBizLDCTesterDlg::OnBnClickedBtnMode()
{
	CPassWordDlg PassWordDlg;
	if(m_opMode == MODE_OPERATOR)
	{
#ifndef DEV_MODE
			if(PassWordDlg.DoModal() == TRUE) 
			{	
#endif
				m_opMode = MODE_SUPERVISOR;
				m_OperatorMode.SetText(L"관리자 모드", BLACK, RGB(225,255,255));
				return;
#ifndef DEV_MODE
			}
#endif
	}
	else
	{
		m_opMode = MODE_OPERATOR;	
		m_OperatorMode.SetText(L"작업자 모드", BLACK, RGB(255,240,255));
	}
	return;
}


void CnBizLDCTesterDlg::OnMenuSystem()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("관리자 모드로 열 수 있습니다"),MB_ICONHAND);
	}else{
		G_MainWnd->m_SystemSetupDlg.m_fnSystemUILoad();
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_SystemSetupDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_SystemSetupDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
	}
}


void CnBizLDCTesterDlg::OnMenuModel()
{
	//load recipe data
	if(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel != L"")
	{
		G_MainWnd->m_RecipeSetDlg.recipeSelected = G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel;
		G_MainWnd->m_RecipeSetDlg.GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_RecipeSetDlg.clearGridModelRecipe();
		G_MainWnd->m_RecipeSetDlg.fillGridModelRecipe(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	}

	if(m_opMode == MODE_OPERATOR)
	{
		//AfxMessageBox(_T("관리자 모드로 열 수 있습니다"),MB_ICONHAND);
		G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.SetEditable(FALSE);
	}else{
		G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.SetEditable(TRUE);
	}
	//1224_KYS
	G_MainWnd->m_RecipeSetDlg.ModelImageLoad(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_RecipeSetDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_RecipeSetDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
}


void CnBizLDCTesterDlg::OnMenuInstrument()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("관리자 모드로 열 수 있습니다"),MB_ICONHAND);
	}else{
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_InstrumentDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_InstrumentDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
		
	}
}


void CnBizLDCTesterDlg::OnMenuAxis()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("관리자 모드로 열 수 있습니다"),MB_ICONHAND);
	}
	//160118_KYS_AXIS DLG OPEN CONDITION ADD...TEACHING INTERLOCK
	else if (!G_MainWnd->m_PIODlg.readDI(IN_SHUTTLE_DOWN, CARD_IO) || !G_MainWnd->m_PIODlg.readDI(IN_LCR_LIFT_DOWN, CARD_IO))
	{
		AfxMessageBox(_T("SHUTTLE DOWN, LCR_LIFT_DOWN 상태에서 열 수 있습니다"), MB_ICONHAND);
	}
	else{
		G_MainWnd->m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		G_MainWnd->m_AxisDlg.fillGridLCRAxis(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
#ifdef _DEBUG
		::SetWindowPos(G_MainWnd->m_AxisDlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
		::SetWindowPos(G_MainWnd->m_AxisDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif

	}
}


void CnBizLDCTesterDlg::OnMenuIo()
{
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
}


void CnBizLDCTesterDlg::OnMenuVisioninspection()
{
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("관리자 모드로 열 수 있습니다"),MB_ICONHAND);
	}else{
#ifdef DEV_MODE
		::SetWindowPos(G_MainWnd->m_InspectThread.m_vi, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
		::SetWindowPos(G_MainWnd->m_InspectThread.m_vi, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif

	}
}


void CnBizLDCTesterDlg::OnHelpAbout()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}


void CnBizLDCTesterDlg::OnFileExit()
{
	//Program Exit
	CMainWnd* pMainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;
	OnBnClickedBtnManu();
	Sleep(100);
	G_SystemModeData.unSystemMode = SYSTEM_EXIT;
	G_MainWnd->m_InstrumentDlg.safeCloseInstrument();
	G_MainWnd->m_DataHandling.m_fnInspectDataReset();
	::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CLOSE, 0, 0);
}

//Error Reset
void CnBizLDCTesterDlg::OnBnClickedBtnErrorReset()
{
	CString strBuff;
	G_MainWnd->m_InspectThread.errorResetRoutine();
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){
#ifdef DEV_MODE
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOP, 50, 50, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_PIODlg, HWND_TOPMOST, 50, 50, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
		G_MainWnd->m_PIODlg.ShowWindow(SW_SHOW);
	}else{
		//can not home in auto mode
		strBuff.Format(_T(" Home in Manual Mode "));
		AfxMessageBox(strBuff);
	}
}


int CnBizLDCTesterDlg::OnSystemInit(void)
{
	//restore inspection status
	G_MainWnd->m_InspectThread.restoreInspectionStatus();
	G_MainWnd->m_InspectThread.m_csCheck.Lock();
	G_MainWnd->m_InspectThread.holdAutoRun();
	G_MainWnd->m_InspectThread.checkStatus = CHECK_SUSPEND;
	G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	//AxisGrid display
	for (int row = 1; row < G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetRowCount(); row++)
	{
		G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetCell(row, (NUM_AXIS_GRID_COL +1))->SetBackClr(RGB(220,220,220));
	}
	if(G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step >=0 &&
		G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step < G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetRowCount())
	G_MainWnd->m_AxisDlg.m_GridLCRAxis.GetCell(G_MainWnd->m_InspectThread.stLCRBatch.LCR_Axis_step+1, (NUM_AXIS_GRID_COL +1))->SetBackClr(YELLOW);
	//model
	G_MainWnd->m_RecipeSetDlg.applyRecipeInProduction(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	//apply recipe in LCR Axis Recipe
	G_MainWnd->m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	G_MainWnd->m_AxisDlg.fillGridLCRAxis(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	//
	//Instrument LCR Correction reference reading
	G_MainWnd->m_InstrumentDlg.GetDlgItem(IDC_LCR_CORR_MODEL)->SetWindowText(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	G_MainWnd->m_InstrumentDlg.readGridLCRCorrectRef();

	//Main Dlg
	m_staticModel.SetText(G_MainWnd->m_RecipeSetDlg.part_Test.part_model);
	m_staticType.SetText(G_MainWnd->m_RecipeSetDlg.part_Test.part_type);
	m_opMode = MODE_OPERATOR;
	m_OperatorMode.SetText(L"작업자 모드", BLACK, RGB(255,240,255));
	if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] == 1){
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GREEN_LIGHT);
	}else{
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GRAY_LIGHT);
	}
	if(G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] == 1){
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GREEN_LIGHT);
	}else{
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GRAY_LIGHT);
	}
	//load inspect data on main dlg grid
	fillGridMain(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	//Label Printer
#ifdef _FI
	G_MainWnd->m_LabelPrint.setModel(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
	StartEventCapture();
#endif

	//Axis Shuttle Sys Init
	G_MainWnd->m_AxisDlg.SetTimer(300, 2000, NULL);

	//manual mode
	G_SystemModeData.unSystemMode = SYSTEM_MANUAL;
	G_SystemModeData.unSystemError = SYSTEM_OK;
	m_btnAuto.SetColorChange(WHITE, BLACK);
	m_btnManual.SetColorChange(GREEN, BLACK);
	m_btnStart.SetColorChange(WHITE, BLACK);
	m_btnStop.SetColorChange(WHITE, BLACK);
	G_AddLog(3,L"Manual Mode");
#ifdef _FI
	m_staticName.SetText(L"최종 검사", BLACK, RGB(180, 190, 225));
#else
	m_staticName.SetText(L"공정 검사", BLACK, RGB(180, 190, 225));
#endif
	return 0;
}


int CnBizLDCTesterDlg::fillGridMain(CString recipe)
{

	int row = 1;

	CString strRecipe, strRecipeName;
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData, str;



	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") 
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				for(j=0; j<MAX_TOTAL_TEST - 1 ; j++)
				{
					for(i=0; i<8 ; i++)
					{	//CLEAR
						m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetBackClr(WHITE);
						m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(L"");

						strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4],L"");
						if(strValueData == "1")
						{
							//terminal
							if(i == 0){
								//strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"") + L"),L" +
								//	G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1],L"") + L"),L" +
								//	G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2],L"") + L"),L" +
								//	G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3],L"");
								strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[15],L"");
								m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(strValueData);
							}
							//test setting
							if(i ==1){
								if(j!=MAX_L_TEST){
									strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"") + L"kHz,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"") + L"Vac,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"") + L"Vdc,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"") + L"s,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"") + L"A" ;
								}else{//Oscilloscope
									strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"") + L"kHz,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"") + L"Vac,L" +
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"") + L"Vdc,L" +
										//G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"") + L"s,L" +
										//G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"") + L"A" + 
										G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[10],L"") + L"[N]" ;
								}
								m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(strValueData);
							}
							//pass criteria
							if(i ==2){
								strValueData = G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11],L"") + L"~" +
									G_MainWnd->m_RecipeSetDlg.m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12],L""); 
								//unit
								if( j< MAX_L_TEST) strValueData +="uH";
								else if( j< MAX_L_TEST + MAX_OSC_TEST) ;
								else if( j< MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST) strValueData +="mOhm";
								else if( j< MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST) strValueData +="mA";
								else if( j< MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST) strValueData +="MOhm";

								m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(strValueData);
							}

						}else{
							m_GridMainResult.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetBackClr(RGB(230,230,230));
						}
					}
				}//for(j=0; j<26 ; j++)
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}
	m_GridMainResult.Refresh();
	return 0;

}


//hold shuttle to stop all inspect batch finished
void CnBizLDCTesterDlg::OnBnClickedBtnHoldShuttle()
{
	if(G_MainWnd->m_InspectThread.stShuttleBatch.holdShuttle == 0)
	{
		G_MainWnd->m_InspectThread.stShuttleBatch.holdShuttle = 1;
		m_btnHoldShuttle.SetColorChange(GREEN, BLACK);

	}else{
		G_MainWnd->m_InspectThread.stShuttleBatch.holdShuttle = 0;
		m_btnHoldShuttle.SetColorChange(WHITE, BLACK);
	}
	
}


void CnBizLDCTesterDlg::OnBnClickedBtnBuzzstop()
{
	G_MainWnd->m_PIODlg.buzzstop = 1;
}


void CnBizLDCTesterDlg::OnBnClickedBtnMasterOk2()
{
	if(nMasterOK != 1){
		nMasterOK = 1;
		m_masterTest_OK_Start.SetColorChange(GREEN, BLACK);
	}
	else{
		nMasterOK = 0;
		m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
	}
}

void CnBizLDCTesterDlg::OnBnClickedBtnMasterNg2()
{
		if(nMasterNG != 1){
			nMasterNG = 1;
			m_masterTest_NG_Start.SetColorChange(GREEN, BLACK);
		}
		else{
			nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
		}
}


void CnBizLDCTesterDlg::OnMenuLabelprint()
{
#ifdef _FI
#ifdef _DEBUG
	::SetWindowPos(G_MainWnd->m_LabelPrint, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#else
	::SetWindowPos(G_MainWnd->m_LabelPrint, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
#endif
	G_MainWnd->m_LabelPrint.ShowWindow(SW_SHOW);
#endif
}


void CnBizLDCTesterDlg::StartEventCapture()
{
	RAWINPUTDEVICE rawInputDev[1]; 
	ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*1);
	// 키보드 RAWINPUTDEVICE 구조체 설정	
	rawInputDev[0].usUsagePage = 0x01;
	rawInputDev[0].usUsage = 0x06;
	rawInputDev[0].dwFlags = RIDEV_INPUTSINK;
	rawInputDev[0].hwndTarget = this->GetSafeHwnd();
	if( FALSE == RegisterRawInputDevices(rawInputDev, 1, sizeof(RAWINPUTDEVICE)) )
	{
		CString str;
		str.Format(_T("RegisterRawInputDevices Error %d"), GetLastError());
		G_AddLog(3,str.GetBuffer());
		str.ReleaseBuffer();
	}
}

void CnBizLDCTesterDlg::EndEventCapture()
{
	RAWINPUTDEVICE rawInputDev[1]; 
	ZeroMemory(rawInputDev, sizeof(RAWINPUTDEVICE)*1);
	// 키보드 RAWINPUTDEVICE 구조체 설정	
	rawInputDev[0].usUsagePage = 0x01;
	rawInputDev[0].usUsage = 0x06;
	rawInputDev[0].dwFlags = RIDEV_REMOVE; //<--
	rawInputDev[0].hwndTarget = this->GetSafeHwnd();

	if( FALSE == RegisterRawInputDevices(rawInputDev, 1, sizeof(RAWINPUTDEVICE)) )
	{
		CString str;
		str.Format(_T("RegisterRawInputDevices Error %d"), GetLastError());
		G_AddLog(3,str.GetBuffer());
		str.ReleaseBuffer();
	}
}

void CnBizLDCTesterDlg::OnRawInput(UINT nInputcode, HRAWINPUT hRawInput)
{
#ifdef _FI
	UINT   dwSize = 40;
	static BYTE lpb[40];
	//wchar_t c=0;
	char c = 0;
	GetRawInputData((HRAWINPUT)hRawInput, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));
	RAWINPUT* raw = (RAWINPUT*)lpb;
	if (raw->header.dwType == RIM_TYPEKEYBOARD && !raw->data.keyboard.Flags && raw->data.keyboard.VKey >= 0x20)
	{
		switch(raw->data.keyboard.VKey)
		{
		case 0xBC:
			c = ',';
			break;
		case 0xBD:
			c = '-';
			break;
		case 0xBE:
			c = '.';
			break;
		case 0xBF:
			c = '/';
			break;
		default:
			c = (char)(raw->data.keyboard.VKey);// & 0xFF);
			break;
		}
		int i = 0;
		int kys = 0;
		for (i = 0; i < 256; i++)
		{
			if (G_MainWnd->m_LabelPrint.m_partLabel.QRcodeScan[i] == 0)
			{
				kys = kys + 1;
				if (kys == 256)
					m_QRScan_count = 0;
			}
			else
				break;
		
		}
		
		G_MainWnd->m_LabelPrint.m_csLabel.Lock();
		//for(int i = 0; i< 256; i++)
		//{
			//if(G_MainWnd->m_LabelPrint.m_partLabel.QRcodeScan[i] == 0)
			//{
		G_MainWnd->m_LabelPrint.m_partLabel.QRcodeScan[m_QRScan_count] = c;
		++m_QRScan_count;
		if (m_QRScan_count == 256)
			m_QRScan_count = 0;
		//break;
			//}
		//}
		G_MainWnd->m_LabelPrint.m_csLabel.Unlock();
		
	} 
	CDialogEx::OnRawInput(nInputcode, hRawInput);
#endif
}


void CnBizLDCTesterDlg::testResultUIUpdate(void)
{
	int stage;
	CTime ctCurrentTime;
	wchar_t ngStr[100];
#ifdef _FI
	stage = STG_VI_UNLOAD;
#else
	//1124_kys
	if (G_MainWnd->m_DataHandling.m_SystemParam.iParam[SP_VISION_INSPECTION] == 1){
		stage = STG_VI_UNLOAD;
	}
	else{
		stage = STG_DCR;
	}
	//stage = STG_DCR;
	//stage = STG_VI_UNLOAD;
#endif

	ctCurrentTime = CTime::GetCurrentTime();

	////////////////////////////////////
	//OK NG display
	if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){ //PASS
		m_staticOK.SetText(L"OK", BLACK, GREEN);
		m_staticNG.SetText(L"NG", RGB(210,210,210), RGB(230,230,230));
	}else{
		m_staticOK.SetText(L"OK", RGB(210,210,210), RGB(230,230,230));
		m_staticNG.SetText(L"NG", BLACK, RED);
	}
	////////////////////////////////////
	//Production count
	CString strValueData;
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	m_staticAccu.SetText(strValueData, BLACK, RGB(230,230,230));
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	m_staticTarget.SetText(strValueData, BLACK, RGB(230,230,230));

	strValueData.Format(_T("%4.1f %%"),G_MainWnd->m_DataHandling.m_FlashData.data[LOT_PERCENT]*1.0f);
	m_staticTargetPercent.SetText(strValueData, BLACK, RGB(230,230,230));
	strValueData.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL]);
	m_staticTotal.SetText(strValueData, BLACK, RGB(230,230,230));
	strValueData.Format(_T("%d/%d"), G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK],G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG]);
	m_staticOKNG.SetText(strValueData, BLACK, RGB(230,230,230));


	////////////////////////////////////
	//result log data grid
	int serialIndex;
	CString strValue;
	GV_ITEM Item;	
	Item.nFormat =  DT_VCENTER;

	strValue = ctCurrentTime.Format(L"%H:%M:%S");
	Item.col = 1;
	m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, strValue);	

	Item.col = 2;
	m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);	

#ifdef _FI
	if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){ //PASS
		serialIndex = _wtoi(G_MainWnd->m_LabelPrint.strZPL[LINE_LABEL_INDEX]);
		strValue.Format(_T("%d"), G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex]);     //1122_kys
		//		strValue.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[LABEL_SERIAL_0 + serialIndex] + 1);
		Item.col = 3;
		G_MainWnd->m_ServerDlg.m_ctrlGridResult.SetItemText(G_MainWnd->m_ServerDlg.nDayReportCount, Item.col, strValue);
	}
#endif

	if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass)
	{
		strValue = "OK";
		Item.col = 4;
		m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, strValue);
	}else{
		strValue = "NG";
		Item.col = 4;
		m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, strValue);	

		strValue.Format(_T("%d"),G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG]);
		Item.col = 6;
		m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, strValue);	
	}

	memset(ngStr, 0x00, sizeof(wchar_t)* 100);
	if(wcslen(G_MainWnd->m_InspectThread.m_PartData[stage].ngItems) < 98)
		memcpy(ngStr, G_MainWnd->m_InspectThread.m_PartData[stage].ngItems, wcslen(G_MainWnd->m_InspectThread.m_PartData[stage].ngItems)*sizeof(wchar_t));
	else
		memcpy(ngStr, G_MainWnd->m_InspectThread.m_PartData[stage].ngItems, 95 * sizeof(wchar_t));
	//strValue = G_MainWnd->m_InspectThread.m_PartData[stage].ngItems;
	Item.col = 5;
	m_ctrlGridResult.SetItemText(nDayReportCount, Item.col, ngStr);	

	int nSize = 0, nMax = 0, nMin = 0;
	int nGetCount = 0;
	m_ctrlGridResult.GetScrollRange(SB_VERT, &nMin, &nMax);
	nSize = nMax / 5000;
	nGetCount = nDayReportCount/30 - 3; //+1;
	m_ctrlGridResult.SetScrollPos(SB_VERT, nSize*(nDayReportCount) + 28 * nGetCount ); // 29 * nGetCount );
	m_ctrlGridResult.AutoSize(GVS_HEADER);
	m_ctrlGridResult.SetSelectedRange(nDayReportCount, 1, nDayReportCount, m_ctrlGridResult.GetColumnCount() -1);
	m_ctrlGridResult.Refresh();	

	{
		//m_fnAutoDelete("E:\\nBizData\\"), 15);
	}
	strValue.ReleaseBuffer();

}


void CnBizLDCTesterDlg::masterResultUIUpdate(void)
{
	int stage;

#ifdef _FI
	stage = STG_VI_UNLOAD;
#else
	//1117_kys
	//stage = STG_DCR;
	stage = STG_VI_UNLOAD;
#endif

	if(nMasterOK == 1){
		if(G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){//OK
			m_masterTest_OK.SetLedState(GREEN_LIGHT);
			nMasterOK = 0;
			m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 1;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"1"); 
			//AfxMessageBox(_T("양품 마스터 시험 결과 OK입니다"),MB_ICONASTERISK); 
			m_ErrorMsg.SetText(_T("양품 마스터 시험 결과 OK입니다"), BLACK, GREEN); //RGB(230,230,230)
		}else{//NG
			m_masterTest_OK.SetLedState(RED_LIGHT);
			//nMasterOK = 0;
			m_masterTest_OK_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 0;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"0"); 
			//AfxMessageBox(_T("양품 마스터 시험 결과 NG입니다"),MB_ICONHAND);
			m_ErrorMsg.SetText(_T("양품 마스터 시험 결과 NG입니다"), BLACK, RED);
		}
	}
	if(nMasterNG == 1){
		if(!G_MainWnd->m_InspectThread.m_PartData[stage].finalPass){//NG
			m_masterTest_NG.SetLedState(GREEN_LIGHT);
			nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 1;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"1"); 
			//AfxMessageBox(_T("불량 마스터 시험 결과 OK입니다"),MB_ICONASTERISK); 
			m_ErrorMsg.SetText(_T("양품 마스터 시험 결과 OK입니다"), BLACK, RED);
		}else{//OK
			m_masterTest_NG.SetLedState(RED_LIGHT);
			//nMasterNG = 0;
			m_masterTest_NG_Start.SetColorChange(WHITE, BLACK);
			G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 0;
			G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"0"); 
			//AfxMessageBox(_T("불량 마스터 시험 결과 NG입니다"),MB_ICONHAND);
			m_ErrorMsg.SetText(_T("양품 마스터 시험 결과 NG입니다"), BLACK, GREEN);
		}
	}


}


void CnBizLDCTesterDlg::testStepUIUpdate(int stg, int step)
{
	int rowGrid;
	CString str;
	//vi[VI_LCR].str_result display
	switch(stg){
	case STG_LCR:

		rowGrid = step+1;
		str.Format(_T("%.2f uH"), G_MainWnd->m_InspectThread.m_PartData[STG_LCR].l[step]*(+1.0E+6));
		//m_GridMainResult.GetCell(rowGrid, COL_MEASURED)->SetText(G_MainWnd->m_InstrumentDlg.vi[VI_LCR].str_result);
		m_GridMainResult.GetCell(rowGrid, COL_LCR)->SetText(str);
		if( G_MainWnd->m_InspectThread.m_PartData[STG_LCR].lPass[step] ){
			//m_GridMainResult.GetCell(rowGrid, COL_LCR)->SetText(L"OK");
			m_GridMainResult.GetCell(rowGrid, COL_LCR)->SetBackClr(GREEN);
		}else{
			//m_GridMainResult.GetCell(rowGrid, COL_LCR)->SetText(L"NG");
			m_GridMainResult.GetCell(rowGrid, COL_LCR)->SetBackClr(RED);
		}
		break;

	case STG_OSC:
		rowGrid = step +MAX_L_TEST + 1 ;
		m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetText(ConvertMultybyteToUnicode(G_MainWnd->m_InstrumentDlg.vi[VI_OSC].str_result));
		//str.Format(L"%s", G_MainWnd->m_InspectThread.m_PartData[STG_OSC].phase);
		//m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetText(str);
		if(G_MainWnd->m_InspectThread.m_PartData[STG_OSC].phasePass[step] ){
			//m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetText(L"OK");
			m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetBackClr(GREEN);
		}else{
			//m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetText(L"NG");
			m_GridMainResult.GetCell(rowGrid, COL_OSC)->SetBackClr(RED);
		}
		break;

	case STG_DCR:
		rowGrid = step+MAX_L_TEST+MAX_OSC_TEST+1;
		str.Format(_T("%.2f mohm"), G_MainWnd->m_InspectThread.m_PartData[STG_DCR].r[step]*(+1.0E+3));
		m_GridMainResult.GetCell(rowGrid, COL_DCR)->SetText(str);
		if( G_MainWnd->m_InspectThread.m_PartData[STG_DCR].rPass[step] ){
			//m_GridMainResult.GetCell(rowGrid, COL_DCR)->SetText(L"OK");
			m_GridMainResult.GetCell(rowGrid, COL_DCR)->SetBackClr(GREEN);
		}else{
			//m_GridMainResult.GetCell(rowGrid, COL_DCR)->SetText(L"NG");
			m_GridMainResult.GetCell(rowGrid, COL_DCR)->SetBackClr(RED);
		}

		break;
	case STG_HIPOT_IR: // 5: hipot, 5: ir

		if(step < MAX_HIPOT_TEST)//Hipot
		{
			rowGrid = step+MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+1;
			str.Format(_T("%.2f mA"), G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].i[step]*(+1.0E+3));
			m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(str);
			if( G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].iPass[step]){
				//m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"OK");
				m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(GREEN);
			}else{
				//m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"NG");
				m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(RED);
			}

		}else{//IR
			step = step - MAX_HIPOT_TEST;
			rowGrid = step+MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST+1;
			str.Format(_T("%.2f Mohm"), G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].ir[step]*(+1.0E-6));
			m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(str);
			if( G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].irPass[step] ){
				//m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"OK");
				m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(GREEN);
			}else{
				//m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetText(L"NG");
				m_GridMainResult.GetCell(rowGrid, COL_HIPOTIR)->SetBackClr(RED);
			}
		}
		break;

	case STG_VI_UNLOAD:
		rowGrid = step + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST + 1;
		if (G_MainWnd->m_InspectThread.m_PartData[STG_VI_UNLOAD].viPass[step]){
			m_GridMainResult.GetCell(rowGrid, COL_VISION)->SetText(L"OK");
			m_GridMainResult.GetCell(rowGrid, COL_VISION)->SetBackClr(GREEN);
		}
		else{
			m_GridMainResult.GetCell(rowGrid, COL_VISION)->SetText(L"NG");
			m_GridMainResult.GetCell(rowGrid, COL_VISION)->SetBackClr(RED);
		}
		break;

	}

	G_MainWnd->m_ServerDlg.m_GridMainResult.Refresh();

}



void CnBizLDCTesterDlg::OnBnClickedBtnNgOut()
{
	if(G_MainWnd->m_InspectThread.checkStatus != CHECK_NG)
	{
		AfxMessageBox(_T("NG PART이 아닙니다"),MB_ICONHAND);
		return;
	}
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(_T("매뉴얼 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	if(m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(_T("관리자 모드로 할 수 있습니다"),MB_ICONHAND);
		return;
	}

	int iRet;

#ifdef _FI
	iRet = G_MainWnd->m_PIODlg.writeDO(1, OUT_DOOR_OPEN,CARD_IO);
#else
	iRet = G_MainWnd->m_PIODlg.writeDO(1, OUT_DCR_LIFT_DOWN,CARD_IO);
#endif
	G_MainWnd->m_ServerDlg.m_btnNGOut.SetColorChange(WHITE, BLACK);
}


int CnBizLDCTesterDlg::m_fnUpdateShiftResult(void) //FI
{
	CString strValueData;
	int i, j, k;
	int stg;

	//LCR
	for(k = COL_OSC; k <= COL_VISION; k++){
		stg = k - 3;
		//jig check
		if(G_MainWnd->m_PIODlg.readDI(stg + 1,CARD_IO)==1 ) //IN_OSC_JIG_DETECT	3
		{
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					strValueData.Format(_T("%10.2f uH"), G_MainWnd->m_InspectThread.m_PartData[stg].l[j]*(+1.0E+6));
					m_GridMainResult.GetCell(j+1, k)->SetText(strValueData);
					if(G_MainWnd->m_InspectThread.m_PartData[stg].lPass[j])
					{
						m_GridMainResult.GetCell(j+1,k)->SetBackClr(GREEN);
					}else{
						m_GridMainResult.GetCell(j+1, k)->SetBackClr(RED);
					}
				}
			}//for(j=0; j<MAX_L_TEST; j++){
		}else{//clear
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					m_GridMainResult.GetCell(j+1, k)->SetText(L"");
					m_GridMainResult.GetCell(j+1,k)->SetBackClr(WHITE);
				}
			}//for(j=0; j<MAX_L_TEST; j++){

		}
	}//for(i = COL_OSC; i <= COL_VISION; i++)

	//OSC
	for(k = COL_DCR; k <= COL_VISION; k++){
		stg = k - 3;
		if(G_MainWnd->m_PIODlg.readDI(stg + 1,CARD_IO)==1 ) //IN_OSC_JIG_DETECT	3
		{
			for(j=0; j<MAX_OSC_TEST; j++){
				i = j + MAX_L_TEST + 1;
				if(G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[j].bTest)
				{
					//strValueData.Format(_T("%s"), m_PartData[stg].phase);
					//m_GridMainResult.GetCell(i, k)->SetText(strValueData);
					if(G_MainWnd->m_InspectThread.m_PartData[stg].phasePass[j])
					{
						m_GridMainResult.GetCell(i, k)->SetText(L"OK");
						m_GridMainResult.GetCell(i, k)->SetBackClr(GREEN);
					}else{
						m_GridMainResult.GetCell(i, k)->SetText(L"NG");
						m_GridMainResult.GetCell(i, k)->SetBackClr(RED);
					}
				}
			}//for(j=0; j<MAX_OSC_TEST; j++){
		}else{
			for(j=0; j<MAX_OSC_TEST; j++){
				i = j + MAX_L_TEST + 1;
				if(G_MainWnd->m_RecipeSetDlg.part_Test.oscTestInfo[j].bTest)
				{
					m_GridMainResult.GetCell(i, k)->SetText(L"");
					m_GridMainResult.GetCell(i, k)->SetBackClr(WHITE);
				}
			}//for(j=0; j<MAX_OSC_TEST; j++){
		}
	}//for(k = COL_DCR; k <= COL_VISION; k++){

	//DCR
	for(k = COL_HIPOTIR; k <= COL_VISION; k++){
		stg = k - 3;
		if(G_MainWnd->m_PIODlg.readDI(stg + 1,CARD_IO)==1 ) //IN_OSC_JIG_DETECT	3
		{
			for(j=0; j<MAX_DCR_TEST; j++){
				i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
				if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
				{
					strValueData.Format(_T("%10.2f mOhm"), G_MainWnd->m_InspectThread.m_PartData[stg].r[j]*(+1.0E+3));
					m_GridMainResult.GetCell(i, k)->SetText(strValueData);
					if(G_MainWnd->m_InspectThread.m_PartData[stg].rPass[j])
					{
						m_GridMainResult.GetCell(i, k)->SetBackClr(GREEN);
					}else{
						m_GridMainResult.GetCell(i, k)->SetBackClr(RED);
					}
				}
			}//for(j=0; j<MAX_DCR_TEST; j++){
		}else{
			for(j=0; j<MAX_DCR_TEST; j++){
				i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
				if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
				{
					m_GridMainResult.GetCell(i, k)->SetText(L"");
					m_GridMainResult.GetCell(i, k)->SetBackClr(WHITE);
				}
			}//for(j=0; j<MAX_DCR_TEST; j++){
		}
	}//for(k = COL_HIPOTIR; k <= COL_VISION; k++){

	//HIPOT
	k = COL_VISION;
	stg = k - 3;
	if(G_MainWnd->m_PIODlg.readDI(stg + 1,CARD_IO)==1 ) //IN_OSC_JIG_DETECT	3
	{
		//HIPOT
		for(j=0; j<MAX_HIPOT_TEST; j++){
			i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + 1;
			if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j].bTest)
			{
				strValueData.Format(_T("%10.2f mA"), G_MainWnd->m_InspectThread.m_PartData[stg].i[j]*(+1.0E+3));
				m_GridMainResult.GetCell(i, k)->SetText(strValueData);
				if(G_MainWnd->m_InspectThread.m_PartData[stg].iPass[j])
				{
					m_GridMainResult.GetCell(i, k)->SetBackClr(GREEN);
				}else{
					m_GridMainResult.GetCell(i, k)->SetBackClr(RED);
				}
			}
		}//for(j=0; j<MAX_HIPOT_TEST; j++){
		//IR
		for(j=0; j<MAX_IR_TEST; j++){
			i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + 1;
			if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[j].bTest)
			{
				strValueData.Format(_T("%10.2f MOhm"), G_MainWnd->m_InspectThread.m_PartData[stg].ir[j]*(+1.0E-6));
				G_MainWnd->m_ServerDlg.m_GridMainResult.GetCell(i, k)->SetText(strValueData);
				if(G_MainWnd->m_InspectThread.m_PartData[stg].irPass[j])
				{
					m_GridMainResult.GetCell(i, k)->SetBackClr(GREEN);
				}else{
					m_GridMainResult.GetCell(i, k)->SetBackClr(RED);
				}
			}
		}//for(j=0; j<MAX_IR_TEST; j++){

	}else{
		//HIPOT
		for(j=0; j<MAX_HIPOT_TEST; j++){
			i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + 1;
			if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[j].bTest)
			{
				m_GridMainResult.GetCell(i, k)->SetText(L"");
				m_GridMainResult.GetCell(i, k)->SetBackClr(WHITE);
			}
		}//for(j=0; j<MAX_HIPOT_TEST; j++){
		//IR
		for(j=0; j<MAX_IR_TEST; j++){
			i = j + MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST +MAX_HIPOT_TEST + 1;
			if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[j].bTest)
			{
				m_GridMainResult.GetCell(i, k)->SetText(L"");
				m_GridMainResult.GetCell(i, k)->SetBackClr(WHITE);
			}
		}//for(j=0; j<MAX_IR_TEST; j++){
	}
	m_GridMainResult.Refresh();
	return 0;
}

//공정검사
int CnBizLDCTesterDlg::m_fnUpdateShiftResultPI(void)
{
	CString strValueData;
	int i, j, k;
	int stg;

	//LCR
	 //DCR STG
		k = COL_DCR;
		stg = k - 3;
		//jig check
		if(G_MainWnd->m_PIODlg.readDI(IN_DCR_JIG_DETECT,CARD_IO)==1 ) //
		{
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					strValueData.Format(_T("%10.2f uH"), G_MainWnd->m_InspectThread.m_PartData[stg].l[j]*(+1.0E+6));
					m_GridMainResult.GetCell(j+1, k)->SetText(strValueData);
					if(G_MainWnd->m_InspectThread.m_PartData[stg].lPass[j])
					{
						m_GridMainResult.GetCell(j+1,k)->SetBackClr(GREEN);
					}else{
						m_GridMainResult.GetCell(j+1, k)->SetBackClr(RED);
					}
				}
			}//for(j=0; j<MAX_L_TEST; j++){
		}else{//clear
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					m_GridMainResult.GetCell(j+1, k)->SetText(L"");
					m_GridMainResult.GetCell(j+1,k)->SetBackClr(WHITE);
				}
			}//for(j=0; j<MAX_L_TEST; j++){
		}

		//VISION STG
		k = COL_VISION;
		stg = k - 3;
		//jig check
		if(G_MainWnd->m_PIODlg.readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)==1 ) //
		{
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					strValueData.Format(_T("%10.2f uH"), G_MainWnd->m_InspectThread.m_PartData[stg].l[j]*(+1.0E+6));
					m_GridMainResult.GetCell(j+1, k)->SetText(strValueData);
					if(G_MainWnd->m_InspectThread.m_PartData[stg].lPass[j])
					{
						m_GridMainResult.GetCell(j+1,k)->SetBackClr(GREEN);
					}else{
						m_GridMainResult.GetCell(j+1, k)->SetBackClr(RED);
					}
				}
			}//for(j=0; j<MAX_L_TEST; j++){
		}else{//clear
			for(j=0; j<MAX_L_TEST; j++){
				if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[j].bTest)
				{
					m_GridMainResult.GetCell(j+1, k)->SetText(L"");
					m_GridMainResult.GetCell(j+1,k)->SetBackClr(WHITE);
				}
			}//for(j=0; j<MAX_L_TEST; j++){
		}

	//DCR
		//VISION STG
			k = COL_VISION;
			stg = k - 3;
			if(G_MainWnd->m_PIODlg.readDI(IN_UNLOAD_JIG_DETECT,CARD_IO)==1 ) 
			{
				for(j=0; j<MAX_DCR_TEST; j++){
					i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
					if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
					{
						strValueData.Format(_T("%10.2f mOhm"), G_MainWnd->m_InspectThread.m_PartData[stg].r[j]*(+1.0E+3));
						m_GridMainResult.GetCell(i, k)->SetText(strValueData);
						if(G_MainWnd->m_InspectThread.m_PartData[stg].rPass[j])
						{
							m_GridMainResult.GetCell(i, k)->SetBackClr(GREEN);
						}else{
							m_GridMainResult.GetCell(i, k)->SetBackClr(RED);
						}
					}
				}//for(j=0; j<MAX_DCR_TEST; j++){
			}else{
				for(j=0; j<MAX_DCR_TEST; j++){
					i = j + MAX_L_TEST + MAX_OSC_TEST + 1;
					if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[j].bTest)
					{
						m_GridMainResult.GetCell(i, k)->SetText(L"");
						m_GridMainResult.GetCell(i, k)->SetBackClr(WHITE);
					}
				}//for(j=0; j<MAX_DCR_TEST; j++){
			}

	m_GridMainResult.Refresh();
	return 0;
}



void CnBizLDCTesterDlg::OnStnClickedStatusCam()
{
#ifndef _VI_SKIP
	if (G_MainWnd->m_InspectThread.m_vi.camConnected == CAM_DISCONNECTED){
		if (!G_MainWnd->m_InspectThread.m_vi.pylonInitGrab()){
			m_statusCam.SetBackColor(GREEN);
			G_MainWnd->m_InspectThread.m_vi.pylonSetParam();
		}
		else{
			m_statusCam.SetBackColor(RGB(230, 230, 230));
		}
	}

#endif


}
