#include "stdafx.h"
#include "PIO.h"
#include "nBizLDCTester.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(PIO, CDialogEx)

extern CMainWnd	*G_MainWnd;


//PC 시스템에 장착된 총 보드 개수를 의미합니다.
#define MAX_BOARD_CNT 1

#pragma comment(lib,"C:\\Program Files\\Alpha Motion\\DigitalPro\\MApi(Library)\\Library\\tmcDApiAed_x64.lib")


PIO::PIO(CWnd* pParent /*=NULL*/)
	: CDialogEx(PIO::IDD, pParent)
{
	InitializeCriticalSection(&m_criDIO);	
	memset(DI_prev, 0x00, sizeof(int)*IO_NUM);
	memset(DI, 0x00, sizeof(int)*IO_NUM);
	memset(DO, 0x00, sizeof(int)*IO_NUM);
	buzzstop = 0;
}

PIO::~PIO(void)
{
	LeaveCriticalSection(&m_criDIO);
	DeleteCriticalSection(&m_criDIO);
}


void PIO::DoDataExchange(CDataExchange* pDX)
{
	int id;
	CDialogEx::DoDataExchange(pDX);
	for(id=0; id< 64; id++)
	{
		DDX_Control(pDX, IDC_CHK_DI_0 + id, m_DILED[id]);
		DDX_Control(pDX, IDC_CHK_DO_0 + id, m_DOLED[id]);
	}	
	for(id=0; id< 9; id++)
	{
		DDX_Control(pDX, IDC_BTN_START_BATCH0+ id, m_btnStart[id]);
		DDX_Control(pDX, IDC_BTN_STOP_BATCH0+ id, m_btnStop[id]);
		DDX_Control(pDX, IDC_BTN_STEP_BATCH0+ id, m_btnStep[id]);
		DDX_Control(pDX, IDC_BTN_RESET_BATCH36+ id, m_btnErrorReset[id]);
		DDX_Control(pDX, IDC_BTN_HOME_BATCH0+ id, m_btnHome[id]);
		//DDX_Control(pDX, IDC_BTN_RESET_BATCH27+ id, m_btnBatchClear[id]);
	}
	DDX_Control(pDX, IDC_BTN_LCR_LOAD_CORRECT, m_btnLcrRoadCorrect);
	DDX_Control(pDX, IDC_BTN_DCR_ZERO_ADJUST, m_btnDcrZeroAdjust);

	DDX_Control(pDX, IDC_BTN_HOME_BATCH10, m_btnLCROffset);
	DDX_Control(pDX, IDC_BTN_HOME_BATCH12, m_btnDCROffset);
	DDX_Control(pDX, IDC_BTN_HOME_BATCH13, m_btnHIPOTOffset);
	DDX_Control(pDX, IDC_BTN_HOME_BATCH14, m_btnIROffset);

	//DDX_Control(pDX, IDC_BTN_RESET_BATCH0, m_btnErrorReset[0]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH1, m_btnErrorReset[1]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH2, m_btnErrorReset[2]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH3, m_btnErrorReset[3]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH4, m_btnErrorReset[4]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH5, m_btnErrorReset[5]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH6, m_btnErrorReset[6]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH7, m_btnErrorReset[7]);
	//DDX_Control(pDX, IDC_BTN_RESET_BATCH8, m_btnErrorReset[8]);

	DDX_Control(pDX, IDC_BTN_RESET_BATCH27, m_btnBatchClear[0]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH28, m_btnBatchClear[1]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH29, m_btnBatchClear[2]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH30, m_btnBatchClear[3]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH31, m_btnBatchClear[4]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH32, m_btnBatchClear[5]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH33, m_btnBatchClear[6]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH34, m_btnBatchClear[7]);
	DDX_Control(pDX, IDC_BTN_RESET_BATCH35, m_btnBatchClear[8]);

}


BEGIN_MESSAGE_MAP(PIO, CDialogEx)

	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_COMMAND_RANGE(IDC_CHK_DO_0, IDC_CHK_DO_63, &PIO::OnBnClickedChkDO)
	ON_COMMAND_RANGE(IDC_BTN_START_BATCH0, IDC_BTN_START_BATCH8, &PIO::OnBnClickedBtnStartBatch)
	ON_COMMAND_RANGE(IDC_BTN_STOP_BATCH0, IDC_BTN_STOP_BATCH8, &PIO::OnBnClickedBtnStopBatch)
	ON_COMMAND_RANGE(IDC_BTN_STEP_BATCH0, IDC_BTN_STEP_BATCH8, &PIO::OnBnClickedBtnStepBatch)
	ON_COMMAND_RANGE(IDC_BTN_RESET_BATCH36, IDC_BTN_RESET_BATCH44, &PIO::OnBnClickedBtnResetBatch)
	ON_COMMAND_RANGE(IDC_BTN_HOME_BATCH0, IDC_BTN_HOME_BATCH8, &PIO::OnBnClickedBtnHomeBatch)
	ON_COMMAND_RANGE(IDC_BTN_HOME_BATCH10, IDC_BTN_HOME_BATCH14, &PIO::OnBnClickedBtnOffset)
	ON_COMMAND_RANGE(IDC_BTN_RESET_BATCH27, IDC_BTN_RESET_BATCH35, &PIO::OnBnClickedBtnClearBatch) 
	ON_BN_CLICKED(IDC_BTN_LCR_LOAD_CORRECT, &PIO::OnBnClickedBtnLcrLoadCorrect)
	ON_BN_CLICKED(IDC_BTN_DCR_ZERO_ADJUST, &PIO::OnBnClickedBtnDcrZeroAdjust)

END_MESSAGE_MAP()

BOOL PIO::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();

	// Motion Control Board를 Load 합니다.									//
	//////////////////////////////////////////////////////////////////////////
	int nRet = 0;
	int nConNum = 0;

	CString strMsg = _T("");

	//TMC-AxxxxP 시리즈용 함수.
	nRet = AIO_LoadDevice();
	
	//TMC-BxxxxP 시리즈용 함수.
	//nRet = AIO_pmiSysLoad( TMC_FALSE, &nConNum );
	//nRet = AIO_pmiSysLoad( TMC_TRUE, &nConNum );

	//TMC-AxxxxP 시리즈용으로 사용하고자 할 경우.
	//if( nRet >= 0 )

	////TMC-BxxxxP 시리즈용으로 사용하고자 할 경우.
	////if( nConNum >= 1 )
	//{
	//	//MessageBox(_T("Success!!!"));
	//}else
	//{		
	//	// Motion Conrol Board가 정상적으로 Load 되지 않았을때
	//	// 메세지 창에 에러 Load 에러 값을 표시 합니다.
	//	strMsg.Format(_T("Fail!!!\nError No. = %d"), nRet);
	//	MessageBox(strMsg);

	//	return FALSE;
	//}

	unsigned long dwModel, dwComm, dwDiNum, dwDoNum = 0;

	//TMC-AxxxxP 시리즈용 함수.
	AIO_BoardInfo( CARD_IO, &dwModel, &dwComm, &dwDiNum, &dwDoNum );
	G_AddLog(3,L"IO Card info: dwModel: %d, dwComm: %d, dwDiNum: %d, dwDoNum:%d", dwModel, dwComm, dwDiNum, dwDoNum);

	// if( MAX_BOARD_CNT != nConNum )


	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	for(int i = 0; i < 64; i++)
	{
		m_DILED[i].SetLedStatesNumber(LIGHT_STATE_SENTINEL);
		m_DILED[i].SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 16, 16);
		m_DILED[i].SetIcon(GRAY_LIGHT, IDI_ICON_TRANSPARENT, 16, 16);

		m_DOLED[i].SetLedStatesNumber(LIGHT_STATE_SENTINEL);
		m_DOLED[i].SetIcon(GREEN_LIGHT, IDI_ICON_GREEN, 16, 16);
		m_DOLED[i].SetIcon(GRAY_LIGHT, IDI_ICON_TRANSPARENT, 16, 16);
	}

	initIOLEDLabel();

	//disable buttons
#ifndef _FI
	for(int i = 0; i < NO_BATCH_SQC - 1; i++)
	{
		if (i == LCR || i == DCR || i == SHUTTLE || i == VI || i == UNLOAD)continue;    //1117_kys_unload_add
		m_btnStart[i].EnableWindow(FALSE);
		m_btnStop[i].EnableWindow(FALSE);
		m_btnStep[i].EnableWindow(FALSE);
		m_btnErrorReset[i].EnableWindow(FALSE);
		m_btnHome[i].EnableWindow(FALSE);
		m_btnBatchClear[i].EnableWindow(FALSE);
	}
#endif	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int PIO::initIOLEDLabel(void)
{
	int i;
	//clear
	for( i = 0; i < 64; i++)
	{
		m_DILED[i].SetWindowText(L"");
		m_DOLED[i].SetWindowText(L"");
	}

#ifdef _FI
	for (i = 0; i <= IN_LCR_SHORT_HOME; i++)
	{
		m_DILED[i].SetWindowText(ST_FI_DI[i]);
	}
	for( i = 0; i <= OUT_KR13_HIPOT_INTLOCK; i++)
	{
		m_DOLED[i].SetWindowText(ST_FI_DO[i]);
	}
#else //공정검사
	
	for( i = 0; i <= IN_LCR_SHORT_HOME; i++)
		{
			m_DILED[i].SetWindowText(ST_FI_DI[i]);
		}
		for( i = 0; i <= OUT_LCR_SHORT_DOWN; i++)
		{
			m_DOLED[i].SetWindowText(ST_FI_DO[i]);
		}
#endif

	return 0;
}

void PIO::m_fnInit(void)
{
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);	
	SetTimer(WM_TIMER_IO, 50, NULL);	//50ms
	SetTimer(WM_TIMER_500mS, 500, NULL);	//0.5s
	twlamp = 0;
	LCRLimitDisable = 0;
	writeDO(1,OUT_LCR_PROBE_Z_UNBREAK,CARD_IO);
	writeDO(1,OUT_LCR_SHORT_Z_UNBREAK,CARD_IO);
	doorClose = 0;

}

void PIO::m_fnDeInit(void)
{
	KillTimer(WM_TIMER_IO);	//50ms
	KillTimer(WM_TIMER_500mS);	//0.5s
	Sleep(100);

	// Load 된 Motion Control Board를 Unload 합니다.
	//TMC-AxxxxP 시리즈용 함수.
	//AIO_UnloadDevice();
	//AIO_pmiSysUnload();
	writeDO(0,OUT_LCR_PROBE_Z_UNBREAK,CARD_IO);
	writeDO(0,OUT_LCR_SHORT_Z_UNBREAK,CARD_IO);
#ifdef _FI
	writeDO(0, OUT_KR13_HIPOT_INTLOCK, CARD_IO);
#endif
	AIO_UnloadDevice();    //160114_위치변경
}

void PIO::OnTimer(UINT_PTR nIDEvent)
{

	int i;
	unsigned int m = 0x00000001;
	unsigned int m2;

	switch (nIDEvent)
	{
		case WM_TIMER_IO: //50ms

			//TMC-AxxxxP 시리즈용 함수.
			AIO_GetDIDWord(CARD_IO, 0, &m_uiGetDin32Ch);
			AIO_GetDIDWord(CARD_IO, 1, &m_uiGetDin32Ch2);
			AIO_GetDODWord(CARD_IO, 0, &m_uiGetDout32Ch);
			AIO_GetDODWord(CARD_IO, 1, &m_uiGetDout32Ch2);

			//ret = pmiDiGetData(CARD_IO, 0, &m_uiGetDin32Ch); //TMC_RV_OK
			//ret = pmiDiGetData(CARD_IO, 1, &m_uiGetDin32Ch2);
			//ret = pmiDoGetData(CARD_IO, 0, &m_uiGetDout32Ch);
			//ret = pmiDoGetData(CARD_IO, 1, &m_uiGetDout32Ch2);

			//LED status display
			EnterCriticalSection(&m_criDIO);
			for(i = 0; i < 32; i++)
			{
				m2 = m<<i;
				if( (m_uiGetDin32Ch & m2) == m2 ){DI[i]=1; m_DILED[i].SetLedState(GREEN_LIGHT);}else	{DI[i]=0; m_DILED[i].SetLedState(GRAY_LIGHT);}
				if( (m_uiGetDin32Ch2 & m2) == m2 ){DI[32+i]=1; m_DILED[32+i].SetLedState(GREEN_LIGHT);}else{DI[32+i]=0; m_DILED[32+i].SetLedState(GRAY_LIGHT);}
				if( (m_uiGetDout32Ch & m2) == m2 ){DO[i]=1;	m_DOLED[i].SetLedState(GREEN_LIGHT);}else{DO[i]=0; m_DOLED[i].SetLedState(GRAY_LIGHT);}
				if( (m_uiGetDout32Ch2 & m2) == m2 ){DI[32+i]=1; m_DOLED[32+i].SetLedState(GREEN_LIGHT);}else	{DI[32+i]=0; m_DOLED[32+i].SetLedState(GRAY_LIGHT);}
			}
			LeaveCriticalSection(&m_criDIO);

			//timer 
			for(i = 0; i < IO_NUM; i++)
			{
				if(timer[i].On == 1 ){
					if( timer[i].Accu <  timer[i].Set) 
						timer[i].Accu++;
					else if(timer[i].Accu ==  timer[i].Set)
						timer[i].OTE = 1;
				}
			}

			//EM STOP : Negative Edge
			if(!LCRLimitDisable)
			if(readDI_prev(IN_EM_STOP, CARD_IO) && !readDI(IN_EM_STOP, CARD_IO)){
			//if(!readDI_prev(IN_EM_STOP, CARD_IO) && readDI(IN_EM_STOP, CARD_IO)){
				G_MainWnd->m_InspectThread.EMStop();
			}
#ifdef _FI
			//DOOR LOCK
			if(readDO(OUT_DOOR_OPEN, CARD_IO) && readDI_prev(IN_DOOR_CLOSE, CARD_IO) && !readDI(IN_DOOR_CLOSE, CARD_IO) )
			{
				doorClose = 1;
			}
#endif
			//Edge detect
			memcpy(DI_prev, DI, sizeof(int)*IO_NUM);

			break;

		case WM_TIMER_500mS:
			//Sequence
			if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN)
				timerAutoRunDIO();

			//general DIO 
			timerCheckDIO();


			break;
		default:

			break;


	}//switch (nIDEvent)

	CDialogEx::OnTimer(nIDEvent);
}


void PIO::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}

void PIO::OnBnClickedChkDO(UINT nID)
{
	if(G_SystemModeData.unSystemMode != SYSTEM_MANUAL){
		AfxMessageBox(_T("매뉴얼 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR){
		AfxMessageBox(_T("관리자 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	//unsigned int m_uiOutStatus;
	U16 m_wOutStatus;
	int nDO = nID - IDC_CHK_DO_0;
	int iRet = 0;
	//iRet = pmiDoGetBit(CARD_IO, nDO, &m_uiOutStatus);
	AIO_GetDOBit(CARD_IO, nDO, &m_wOutStatus);
	//if(iRet == TMC_RV_OK)
	//{
		//if( m_uiOutStatus == FALSE )
		if( m_wOutStatus == 0 )
		{
			//iRet = pmiDoSetBit(CARD_IO, nDO, TMC_BIT_ON);
			AIO_PutDOBit(CARD_IO, nDO, CMD_ON);
		}else{
			//iRet = pmiDoSetBit(CARD_IO, nDO, TMC_BIT_OFF);
			AIO_PutDOBit(CARD_IO, nDO, CMD_OFF);
		}
	//}
}

//50ms Time Interval
int PIO::timerAutoRunDIO(void)
{


	///////////////////////////////////////////////////////////////////////////////////////////
	//

	return 0;
}

int PIO::readDI(int address, int board)
{
	int val;
	EnterCriticalSection(&m_criDIO);
	val = DI[address];
	DI_prev[address] = val;
	LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::readDI_prev(int address, int board)
{
	int val;
	EnterCriticalSection(&m_criDIO);
	val = DI_prev[address];
	LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::readDO(int address, int board)
{
	int val;
	EnterCriticalSection(&m_criDIO);
	val = DO[address];
	LeaveCriticalSection(&m_criDIO);
	return val;
}

int PIO::writeDO(int val, int address, int board)
{	
	U16 iRet;
	//unsigned int iVal;
	CString str;
	//EnterCriticalSection(&m_criDIO);
	//iRet = pmiDoSetBit(board, address, val); //TMC_BIT_ON
	//TMC-AxxxxP 시리즈용 함수.
	AIO_PutDOBit(CARD_IO, address, val);
	//LeaveCriticalSection(&m_criDIO);
	Sleep(5);
	AIO_GetDOBit(CARD_IO, address, &iRet);
	//iRet = pmiDoGetBit(board, address, &iVal); //TMC_BIT_ON
#ifndef _DEBUG
	if(val != (int)(iRet & 0xFFFF))
	{
		str.Format(L"IO Write Error: Address:%d, Value:%d", address, val);
		G_AddLog(3, str.GetBuffer()); 
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str , BLACK, RED);
		str.ReleaseBuffer();
		return 0;
	}
#endif
	return TMC_RV_OK;
}


int PIO::timerCheckDIO(void)
{
	int iRet;

	/////////////////////////////////////////////////////////////////////////////////////////
	// Tower Lamp
	if(G_MainWnd->m_InspectThread.checkStatus == CHECK_NG || G_SystemModeData.unSystemError == SYSTEM_ERROR || !readDI(IN_EM_STOP, CARD_IO) ){
	//if(G_SystemModeData.unSystemError == SYSTEM_ERROR || readDI(IN_EM_STOP, CARD_IO) ){
		if(++twlamp >= 2){
			twlamp = 0;
			if(!readDO(OUT_TOWER_RED, CARD_IO)){
				writeDO(1,OUT_TOWER_RED, CARD_IO);
//#ifndef _DEBUG
				if(!buzzstop) writeDO(1,OUT_BUZZER, CARD_IO);
//#endif
			}
			else{
				writeDO(0,OUT_TOWER_RED, CARD_IO);
				writeDO(0,OUT_BUZZER, CARD_IO);
			}
		}
	}else{
		if(readDO(OUT_TOWER_RED, CARD_IO))
			writeDO(0,OUT_TOWER_RED, CARD_IO);
		if(readDO(OUT_BUZZER, CARD_IO))
			writeDO(0,OUT_BUZZER, CARD_IO);

		if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN ){
			if(!readDO(OUT_TOWER_GREEN, CARD_IO))
				writeDO(1,OUT_TOWER_GREEN, CARD_IO);
		}else if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){
			if(++twlamp >= 2){
				twlamp = 0;
				if(!readDO(OUT_TOWER_GREEN, CARD_IO))
					writeDO(1,OUT_TOWER_GREEN, CARD_IO);
				else
					writeDO(0,OUT_TOWER_GREEN, CARD_IO);
			}
			if(readDO(OUT_TOWER_YELLOW, CARD_IO))
				writeDO(0,OUT_TOWER_YELLOW, CARD_IO);
		}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){
			if(!readDO(OUT_TOWER_YELLOW, CARD_IO))
				writeDO(1,OUT_TOWER_YELLOW, CARD_IO);
			if(readDO(OUT_TOWER_GREEN, CARD_IO))
				writeDO(0,OUT_TOWER_GREEN, CARD_IO);
		}
	}

	if(readDO(IN_LCR_LIFT_UP, CARD_IO) == 1)
	{
		writeDO(0,IN_LCR_LIFT_UP, CARD_IO);
		writeDO(1,OUT_LCR_LIFT_DOWN, CARD_IO);
	}

#ifdef _FI
	/////////////////////////////////////////////////////////////////////////////////////////
	//DOOR OPEN : Negative Edge 
	if(readDI_prev(IN_DOOR_CLOSE, CARD_IO) && !readDI(IN_DOOR_CLOSE, CARD_IO))
	{
		if(G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN ){
				G_MainWnd->m_ServerDlg.OnBnClickedBtnStop();
				G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}else if(G_SystemModeData.unSystemMode == SYSTEM_AUTO){
			G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}else if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

		}
		//
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
		AfxMessageBox(_T("안전커버가 열렸습니다"),MB_ICONHAND);
	}

#endif

#ifndef _FI //PRODUCTION INSPECTION
	// LIGHT CURTAIN 
	if(!readDI_prev(IN_LOAD_AREA_SENSOR1, CARD_IO) && readDI(IN_LOAD_AREA_SENSOR1, CARD_IO))
	{
		//LCR AXIS STEP CHECK
		if( G_MainWnd->m_InspectThread.m_fnGetBatchStep(LCR)  == STEP5 || G_MainWnd->m_InspectThread.m_fnGetBatchStep(LCR)  == STEP6 )
		{
			//LCR AXIS STOP
			G_MainWnd->m_InspectThread.EMStop();
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_ERROR_UPDATE, 101 );
			::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INSPECT_LOG_UPDATE, 101 );
		}
	}

	// FI, PI 장비 DEFINE 확인
	if(readDI(26, CARD_IO) || readDI(27, CARD_IO)) //FI: IN_UNLOAD_CV_UP:26, IN_UNLOAD_CV_DOWN:27 
	{
		G_MainWnd->m_InspectThread.EMStop();
		CString str;
		str.Format(L"장비 _FI DEFINE ERROR");
		G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
		G_AddLog(3,L"공정검사기에서는 IN_UNLOAD_CV_UP, IN_UNLOAD_CV_DOWN 둘 모두 OFF이어야 함"); 
		G_SystemModeData.unSystemError = SYSTEM_ERROR;
	}

#endif

	/////////////////////////////////////////////////////////////////////////////////////////
	//turn off DO when DI on 

	//LCR JIG LIFT DOWN
	if(readDO(OUT_LCR_LIFT_DOWN, CARD_IO) && readDI(IN_LCR_LIFT_DOWN, CARD_IO))
	{
		if(!timer[IN_LCR_LIFT_DOWN].On){
			timer[IN_LCR_LIFT_DOWN].On = 1; 
			timer[IN_LCR_LIFT_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_LCR_LIFT_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_LCR_LIFT_DOWN, CARD_IO);
				//
				timer[IN_LCR_LIFT_DOWN].clear();
			}
		}
	}//if(readDO(OUT_LCR_LIFT_DOWN, CARD_IO) &&

	//LCR JIG LIFT UP
	if(readDO(OUT_LCR_LIFT_UP, CARD_IO) && readDI(IN_LCR_LIFT_UP, CARD_IO))
	{
		if(!timer[IN_LCR_LIFT_UP].On){
			timer[IN_LCR_LIFT_UP].On = 1; 
			timer[IN_LCR_LIFT_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_LCR_LIFT_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_LCR_LIFT_UP, CARD_IO);
				//
				timer[IN_LCR_LIFT_UP].clear();
			}
		}
	}//if(readDO(OUT_LCR_LIFT_UP,CARD_IO) &&

	//LCR SHORT DOWN
	if(readDO(OUT_LCR_SHORT_DOWN, CARD_IO) && readDI(IN_LCR_SHORT_DOWN, CARD_IO))
	{
		if(!timer[IN_LCR_SHORT_DOWN].On){
			timer[IN_LCR_SHORT_DOWN].On = 1; 
			timer[IN_LCR_SHORT_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_LCR_SHORT_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_LCR_SHORT_DOWN, CARD_IO);
				timer[IN_LCR_SHORT_DOWN].clear();
			}
		}
	}//if(readDO(OUT_LCR_SHORT_DOWN, CARD_IO) &&
	
	//LCR SHORT UP
	if(readDO(OUT_LCR_SHORT_UP, CARD_IO) && readDI(IN_LCR_SHORT_UP, CARD_IO))
	{
		if(!timer[IN_LCR_SHORT_UP].On){
			timer[IN_LCR_SHORT_UP].On = 1; 
			timer[IN_LCR_SHORT_UP].Set = 10; //1s
		}else{
			if(timer[IN_LCR_SHORT_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_LCR_SHORT_UP, CARD_IO);
				timer[IN_LCR_SHORT_UP].clear();
			}
		}
	}//if(readDO(OUT_LCR_SHORT_UP, CARD_IO) &&

	//DCR JIG LIFT DOWN
	if(readDO(OUT_DCR_LIFT_DOWN, CARD_IO) && readDI(IN_DCR_LIFT_DOWN, CARD_IO))
	{
		if(!timer[IN_DCR_LIFT_DOWN].On){
			timer[IN_DCR_LIFT_DOWN].On = 1; 
			timer[IN_DCR_LIFT_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_DCR_LIFT_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_DCR_LIFT_DOWN, CARD_IO);
				//
				timer[IN_DCR_LIFT_DOWN].clear();
			}
		}
	}//if(readDO(OUT_DCR_LIFT_DOWN, CARD_IO) &&

	//DCR JIG LIFT UP
	if(readDO(OUT_DCR_LIFT_UP, CARD_IO) && readDI(IN_DCR_LIFT_UP,CARD_IO))
	{
		if(!timer[IN_DCR_LIFT_UP].On){
			timer[IN_DCR_LIFT_UP].On = 1; 
			timer[IN_DCR_LIFT_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_DCR_LIFT_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_DCR_LIFT_UP, CARD_IO);
				//
				timer[IN_DCR_LIFT_UP].clear();
			}
		}
	}//if(readDO(OUT_DCR_LIFT_UP, CARD_IO) &&


	//SHUTTLE DOWN
	if(readDO(OUT_SHUTTLE_DOWN, CARD_IO) && readDI(IN_SHUTTLE_DOWN, CARD_IO))
	{
		if(!timer[IN_SHUTTLE_DOWN].On){
			timer[IN_SHUTTLE_DOWN].On = 1; 
			timer[IN_SHUTTLE_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_SHUTTLE_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_SHUTTLE_DOWN, CARD_IO);
				//
				timer[IN_SHUTTLE_DOWN].clear();
			}
		}
	}//if(readDO(OUT_SHUTTLE_DOWN, CARD_IO) &&

	//SHUTTLE UP
	if(readDO(OUT_SHUTTLE_UP, CARD_IO) && readDI(IN_SHUTTLE_UP,CARD_IO))
	{
		if(!timer[IN_SHUTTLE_UP].On){
			timer[IN_SHUTTLE_UP].On = 1; 
			timer[IN_SHUTTLE_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_SHUTTLE_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_SHUTTLE_UP, CARD_IO);
				//
				timer[IN_SHUTTLE_UP].clear();
			}
		}
	}//if(readDO(OUT_SHUTTLE_UP, CARD_IO) &&

	if (!readDI(IN_AIR_SENSOR, CARD_IO))
	{
		if (G_SystemModeData.unSystemMode == SYSTEM_AUTO_RUN){
			G_MainWnd->m_ServerDlg.OnBnClickedBtnStop();
			G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_AUTO){
			G_MainWnd->m_ServerDlg.OnBnClickedBtnManu();
		}
		else if (G_SystemModeData.unSystemMode == SYSTEM_MANUAL){

		}
		if (G_SystemModeData.unSystemError != SYSTEM_ERROR)
		{
			G_SystemModeData.unSystemError = SYSTEM_ERROR;
			//1224_KYS
			CString str;
			str.Format(L"AIR DROP");
			G_MainWnd->m_ServerDlg.m_ErrorMsg.SetText(str, BLACK, RED);
			G_AddLog(3, str.GetBuffer()); str.ReleaseBuffer();
		}
	}

#ifdef _FI
	//CONVEYOR
	if (	readDO(OUT_UNLOAD_CV_RUN, CARD_IO) && readDI(IN_CV_PART_RETURN, CARD_IO))
	{
		//if(!timer[IN_CV_PART_RETURN].On){
		//	timer[IN_CV_PART_RETURN].On = 1; 
		//	timer[IN_CV_PART_RETURN].Set = 2; //
		//}else{
		//	if(timer[IN_CV_PART_RETURN].OTE == 1)
		//	{
				iRet = writeDO(0, OUT_UNLOAD_CV_RUN, CARD_IO);
		//		timer[IN_CV_PART_RETURN].clear();
		//	}
		//}
	}//if(readDO(OUT_SHUTTLE_UP, CARD_IO) &&


	//OSC JIG LIFT DOWN
	if(readDO(OUT_OSC_LIFT_DOWN, CARD_IO) && readDI(IN_OSC_LIFT_DOWN, CARD_IO))
	{
		if(!timer[IN_OSC_LIFT_DOWN].On){
			timer[IN_OSC_LIFT_DOWN].On = 1; 
			timer[IN_OSC_LIFT_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_OSC_LIFT_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_OSC_LIFT_DOWN, CARD_IO);
				//
				timer[IN_OSC_LIFT_DOWN].clear();
			}
		}
	}//if(readDO(OUT_OSC_LIFT_DOWN, CARD_IO) &&

	//OSC JIG LIFT UP
	if(readDO(OUT_OSC_LIFT_UP, CARD_IO) && readDI(IN_OSC_LIFT_UP, CARD_IO))
	{
		if(!timer[IN_OSC_LIFT_UP].On){
			timer[IN_OSC_LIFT_UP].On = 1; 
			timer[IN_OSC_LIFT_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_OSC_LIFT_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_OSC_LIFT_UP, CARD_IO);
				//
				timer[IN_OSC_LIFT_UP].clear();
			}
		}
	}//if(readDO(OUT_OSC_LIFT_UP, CARD_IO) &&


	//HIPOTIR JIG LIFT DOWN
	if(readDO(OUT_HIPOTIR_LIFT_DOWN, CARD_IO) && readDI(IN_HIPOTIR_LIFT_DOWN,CARD_IO))
	{
		if(!timer[IN_HIPOTIR_LIFT_DOWN].On){
			timer[IN_HIPOTIR_LIFT_DOWN].On = 1; 
			timer[IN_HIPOTIR_LIFT_DOWN].Set = 10; //1s
		}else{
			if(timer[IN_HIPOTIR_LIFT_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_HIPOTIR_LIFT_DOWN, CARD_IO);
				//
				timer[IN_HIPOTIR_LIFT_DOWN].clear();
			}
		}
	}//if(readDO(OUT_HIPOTIR_LIFT_DOWN, CARD_IO) &&

	//HIPOTIR JIG LIFT UP
	if(readDO(OUT_HIPOTIR_LIFT_UP, CARD_IO) && readDI(IN_HIPOTIR_LIFT_UP, CARD_IO))
	{
		if(!timer[IN_HIPOTIR_LIFT_UP].On){
			timer[IN_HIPOTIR_LIFT_UP].On = 1; 
			timer[IN_HIPOTIR_LIFT_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_HIPOTIR_LIFT_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_HIPOTIR_LIFT_UP, CARD_IO);
				//
				timer[IN_HIPOTIR_LIFT_UP].clear();
			}
		}
	}//if(readDO(OUT_HIPOTIR_LIFT_UP, CARD_IO) &&

	//UNLOAD JIG CV LIFT DOWN
	if(readDO(OUT_UNLOAD_CV_DOWN, CARD_IO) && readDI(IN_UNLOAD_CV_DOWN, CARD_IO))
	{
		if(!timer[IN_UNLOAD_CV_DOWN].On){
			timer[IN_UNLOAD_CV_DOWN].On = 1; 
			timer[IN_UNLOAD_CV_DOWN].Set = 10; //0.5s
		}else{
			if(timer[IN_UNLOAD_CV_DOWN].OTE == 1)
			{
				iRet = writeDO(0, OUT_UNLOAD_CV_DOWN, CARD_IO);
				//
				timer[IN_UNLOAD_CV_DOWN].clear();
			}
		}
	}//if(readDO(OUT_UNLOAD_CV_DOWN, CARD_IO) &&

	//UNLOAD JIG CV LIFT UP
	if(readDO(OUT_UNLOAD_CV_UP, CARD_IO) && readDI(IN_UNLOAD_CV_UP, CARD_IO))
	{
		if(!timer[IN_UNLOAD_CV_UP].On){
			timer[IN_UNLOAD_CV_UP].On = 1; 
			timer[IN_UNLOAD_CV_UP].Set = 10; //0.5s
		}else{
			if(timer[IN_UNLOAD_CV_UP].OTE == 1)
			{
				iRet = writeDO(0, OUT_UNLOAD_CV_UP, CARD_IO);
				//
				timer[IN_UNLOAD_CV_UP].clear();
			}
		}
	}//if(readDO(OUT_UNLOAD_CV_UP, CARD_IO) &&

	//RETURN CV RUN
	if (	readDO(OUT_UNLOAD_CV_RUN, CARD_IO) && readDI(IN_CV_PART_RETURN, CARD_IO))
	{
		if(!timer[IN_CV_PART_RETURN].On){
			timer[IN_CV_PART_RETURN].On = 1; 
			timer[IN_CV_PART_RETURN].Set = 10; //0.5s
		}else{
			if(timer[IN_CV_PART_RETURN].OTE == 1)
			{
				iRet = writeDO(0, OUT_UNLOAD_CV_RUN, CARD_IO);
				//
				timer[IN_CV_PART_RETURN].clear();
			}
		}
	}//if(readDO(OUT_UNLOAD_CV_RUN, CARD_IO) &&

	if(readDO(OUT_DOOR_OPEN, CARD_IO) && doorClose == 1 )
	{
		if(!timer[IN_DOOR_CLOSE].On){
			timer[IN_DOOR_CLOSE].On = 1; 
			timer[IN_DOOR_CLOSE].Set = 2; 
		}else{
			if(timer[IN_DOOR_CLOSE].OTE == 1)
			{
				doorClose = 0;
				timer[IN_DOOR_CLOSE].clear();
				iRet = writeDO(0, OUT_DOOR_OPEN, CARD_IO);
			}
		}
	}//if(readDO(OUT_DOOR_OPEN, CARD_IO) && readDI(IN_DOOR_CLOSE, CARD_IO))


#endif

	return 0;
}


int PIO::home(void)
{
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{


	}
	return 0;
}

void PIO::OnBnClickedBtnStartBatch(UINT nID)
{
	int batch = nID - IDC_BTN_START_BATCH0;
#ifndef _FI
	if(batch != LCR && batch != DCR && batch != SHUTTLE && batch != VI )
	{
		return;
	}
#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0)
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		else if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22)
			G_MainWnd->m_InspectThread.continueBatch(batch);
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}else{
		AfxMessageBox(_T("AUTO (STOP) 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}

void PIO::OnBnClickedBtnStopBatch(UINT nID)
{
	int iRet, i;
	int batch = nID - IDC_BTN_STOP_BATCH0;
#ifndef _FI
	if(batch != LCR && batch != DCR)
	{
		return;
	}
#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_AUTO)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.holdBatch(batch);
		if(batch == LCR){//LCR Axis stop
			for(i = 0; i < 8; i++){
				iRet = pmiAxStop(CARD_NO, i);
			}
		}
		if(batch == SHUTTLE){//SHUTTLE Axis stop
			iRet = pmiAxStop(CARD_NO2, AXIS_SHUTTLE);
			if(iRet != TMC_RV_OK){
				//Error

			}
		}
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}else{
		AfxMessageBox(_T("AUTO (STOP) 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}

void PIO::OnBnClickedBtnStepBatch(UINT nID)
{
	int batch = nID - IDC_BTN_STEP_BATCH0;
#ifndef _FI
	if(batch != LCR && batch != DCR)
	{
		return;
	}
#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP0){
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP1);
		}else if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP22){
			G_MainWnd->m_InspectThread.continueBatch(batch);
		}else{
			switch(batch){
			case LCR: G_MainWnd->m_InspectThread.stg2LCRTest(); 	break;
			case DCR: G_MainWnd->m_InspectThread.stg4DCRTest();	break;
			case VI: G_MainWnd->m_InspectThread.stg6VITest();	break;
			case SHUTTLE: G_MainWnd->m_InspectThread.stg7ShuttleRun();	break;
#ifdef _FI
			case LOAD: G_MainWnd->m_InspectThread.stg1Load(); break;
			case OSC: G_MainWnd->m_InspectThread.stg3OSCTest();	break;
			//case HIPOT: G_MainWnd->m_InspectThread.stg5HIPOTTest();	break;
			//case IR: G_MainWnd->m_InspectThread.stg5IRTest();	break;
			case UNLOAD: G_MainWnd->m_InspectThread.stg6Unload();	break;
#endif
			}
		}
		G_MainWnd->m_InspectThread.batchTimer();
	}else{
		AfxMessageBox(_T("MANUAL 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}


void PIO::OnBnClickedBtnResetBatch(UINT nID)
{
	int batch = nID - IDC_BTN_RESET_BATCH36;
#ifndef _FI
	if(batch != LCR && batch != DCR)
	{
		//return;
	}
#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(batch) == STEP19){
			m_btnErrorReset[batch].SetColorChange(WHITE, BLACK);
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP20); //put it in error reset step
		}
		//m_btnErrorReset[batch].SetColorChange(RGB(200, 153, 204),RGB(0,0,0)); 
		m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}else{
		AfxMessageBox(_T("MANUAL 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}

void PIO::OnBnClickedBtnHomeBatch(UINT nID)
{
	int iRet;
	ViStatus ret;
	char strbuff[256];
	int batch = nID - IDC_BTN_HOME_BATCH0;
#ifndef _FI
	if(batch != LCR && batch != DCR)
	{
		return;
	}
#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		//HOME ROUTINE
		switch(batch){
		case LOAD:  break;

		case LCR:
			//##############################################//
			//LCR Axis Ready Position 
			// should be done manually in AxisDlg
			//##############################################//
			//
			if(readDI(IN_LCR_LIFT_UP, CARD_IO)){
				iRet = writeDO(1, OUT_LCR_LIFT_DOWN, CARD_IO);
				if(iRet != TMC_RV_OK ){
					//Error

					return;
				}else{
					do{
						Sleep(1000);
					}while(!readDI(IN_LCR_LIFT_DOWN, CARD_IO));
					Sleep(1000);
				}
			}
			//reset LCR Correction
			G_MainWnd->m_InspectThread.fLCRLoadCorrect = 0;
			//reset status
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(LCR, STEP0);
			G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[LCR]; 
			G_MainWnd->m_InspectThread.stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[LCR]; 
			G_MainWnd->m_InspectThread.setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
			G_MainWnd->m_ServerDlg.clearGridMainResult(STG_LCR);
			//instrument
			sprintf_s(strbuff,"BIAS:STATe OFF");
			ret = G_MainWnd->m_InstrumentDlg.do_command(VI_LCR, strbuff);
			G_MainWnd->m_InstrumentDlg.initialize (VI_LCR);
			break;

		case DCR: 
			if(readDI(IN_DCR_LIFT_UP, CARD_IO)){
				iRet = writeDO(1, OUT_DCR_LIFT_DOWN, CARD_IO);
				if(iRet != TMC_RV_OK ){
					//Error

					return;
				}else{
					do{
						Sleep(1000);
					}while(!readDI(IN_DCR_LIFT_DOWN, CARD_IO));
					Sleep(1000);
				}
			}
			//reset status
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(DCR, STEP0);
			G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[DCR]; 
			G_MainWnd->m_InspectThread.stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[DCR]; 
			G_MainWnd->m_InspectThread.m_fnSetInspectionStep(DCR, 0);
			G_MainWnd->m_ServerDlg.clearGridMainResult(STG_DCR);
			//instrument
			sprintf_s(strbuff,":SENSe:CHReset");
			ret= G_MainWnd->m_InstrumentDlg.do_command(VI_DCR, strbuff);
			G_MainWnd->m_InstrumentDlg.initialize (VI_DCR);
			break;

#ifdef _FI
		case OSC: 
			if(readDI(IN_OSC_LIFT_UP, CARD_IO)){
				iRet = writeDO(1, OUT_OSC_LIFT_DOWN, CARD_IO);
				if(iRet != TMC_RV_OK ){
					//Error

					return;
				}else{
					do{
						Sleep(1000);
					}while(!readDI(IN_OSC_LIFT_DOWN, CARD_IO));
					Sleep(1000);
				}
			}
			//reset status
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(OSC, STEP0);
			G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[OSC]; 
			G_MainWnd->m_InspectThread.stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[OSC]; 
			G_MainWnd->m_InspectThread.m_fnSetInspectionStep(OSC, 0);
			G_MainWnd->m_ServerDlg.clearGridMainResult(STG_OSC);
			//instrument
			ret = G_MainWnd->m_InstrumentDlg.do_command(VI_WG,"OUTP OFF");
			G_MainWnd->m_InstrumentDlg.initialize (VI_LCR);
			G_MainWnd->m_InstrumentDlg.initialize (VI_WG);
			break;

		case HIPOT: 
		case IR: 	
			if(readDI(IN_HIPOTIR_LIFT_UP, CARD_IO)){
				iRet = writeDO(1, OUT_HIPOTIR_LIFT_DOWN, CARD_IO);
				if(iRet != TMC_RV_OK ){
					//Error

					return;
				}else{
					do{
						Sleep(1000);
					}while(!readDI(IN_HIPOTIR_LIFT_DOWN, CARD_IO));
					Sleep(1000);
				}
			}
			//reset status
			G_MainWnd->m_InspectThread.m_fnSetBatchStep(HIPOT, STEP0);
			G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[HIPOT]; 
			G_MainWnd->m_InspectThread.stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[HIPOT]; 
			G_MainWnd->m_InspectThread.m_fnSetInspectionStep(HIPOT, 0);

			G_MainWnd->m_InspectThread.m_fnSetBatchStep(IR, STEP0);
			G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[IR]; 
			G_MainWnd->m_InspectThread.stShuttleBatch.request &= ~ST_SHUTTLE_INTERLOCK[IR]; 
			G_MainWnd->m_InspectThread.m_fnSetInspectionStep(IR, 0);
			G_MainWnd->m_ServerDlg.clearGridMainResult(STG_HIPOT_IR);
			//instrument
			ret= G_MainWnd->m_InstrumentDlg.do_command(VI_HIP,":SOURce:SAFEty:STOP");
			ret = G_MainWnd->m_InstrumentDlg.do_command(VI_IR,"STOP");
			G_MainWnd->m_InstrumentDlg.initialize (VI_HIP);
			G_MainWnd->m_InstrumentDlg.initialize (VI_IR);
			break;
		case VI: 	

			break;
		case UNLOAD: 	

			break;
#endif

		case SHUTTLE: 
			//G_MainWnd->m_InspectThread.m_fnSetBatchStep(SHUTTLE, STEP0);
			//G_MainWnd->m_InspectThread.setBatchStatus(SHUTTLE, CLEAR);

			//if(readDI(IN_SHUTTLE_UP, CARD_IO)){
			//	iRet = writeDO(1, OUT_SHUTTLE_DOWN, CARD_IO);
			//	if(iRet != TMC_RV_OK ){
			//		//Error

			//		return;
			//	}else{
			//		do{
			//			Sleep(1000);
			//		}while(!readDI(IN_SHUTTLE_DOWN, CARD_IO));
			//		Sleep(1000);
			//	}
			//}
			//iRet = pmiAxHomeMove( CARD_NO2, AXIS_SHUTTLE );
			//if( iRet == FALSE )
			//	return;
			//else{
			//	do{
			//		pmiCsCheckDone(CARD_NO2, AXIS_SHUTTLE, &iRet);
			//	}while(iRet == emRUNNING);
			//	Sleep(1000);
			//}
			//return to RET position :  in AxisDlg manually
			//iRet = pmiAxPosMove( CARD_NO2, AXIS_SHUTTLE, TMC_ABS, G_MainWnd->m_DataHandling.m_FlashData.dbdata[SHUTTLE_RETURN] );
			//if( iRet != TMC_RV_OK )
			//{//Error

			//	return;
			//}
			//pmiAxCheckDone(CARD_NO2, AXIS_SHUTTLE,  &iRet);
			// if(iRet == emSTAND){
			//	if(readDI(IN_SHUTTLE_RETURN,CARD_IO)==1){
			//		//done
			//	}
			//}
			break;
		}
	}else{
		AfxMessageBox(_T("MANUAL 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}

#ifdef _FI
int PIO::switchRelayHipot(int on, int step)
{
	int i;
	int address;
	int iRet = TMC_RV_OK;

	//turn off all first
	for(i = OUT_KR1_BASE; i <= OUT_KR12_IR_NEG; i++)
	{
		iRet = writeDO(0, i, CARD_IO);
		if(iRet != TMC_RV_OK ){
			//Error
			return iRet;
		}
	}


	if(on){

		//turn on hipot interlock relay
		iRet = writeDO(1, OUT_KR13_HIPOT_INTLOCK, CARD_IO);
		if(iRet != TMC_RV_OK ){
			//Error
			return iRet;
		}

		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch1term1 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch1term1 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch1term2 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch1term2 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch2term1 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch2term1 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch2term2 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[step].ch2term2 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
	}else{

		//turn off hipot interlock relay
		iRet = writeDO(0, OUT_KR13_HIPOT_INTLOCK, CARD_IO);
		if(iRet != TMC_RV_OK ){
			//Error
			return iRet;
		}

	}


		return iRet;
}

int PIO::switchRelayIR(int on, int step)
{
	int i;
	int address;
	int iRet;

	//turn off first
	for(i = OUT_KR1_BASE; i <= OUT_KR12_IR_NEG; i++)
	{
		iRet = writeDO(0, i, CARD_IO);
		if(iRet != TMC_RV_OK ){
			//Error
			return iRet;
		}
	}

	if(on){

		//turn off HIPOT interlock relay
		iRet = writeDO(0, OUT_KR13_HIPOT_INTLOCK, CARD_IO);
		if(iRet != TMC_RV_OK ){
			//Error
			return iRet;
		}

		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch1term1 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch1term1 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch1term2 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch1term2 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch2term1 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch2term1 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
		if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch2term2 > 0){
			address = OUT_KR1_BASE + G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[step].ch2term2 - 1;
			iRet = writeDO(1, address, CARD_IO);
			if(iRet != TMC_RV_OK ){
				//Error
				return iRet;
			}
		}
	}

	return iRet;
}
#endif

//LCR LOAD CORRECTION BATCH AUTO RUN
void PIO::OnBnClickedBtnLcrLoadCorrect()
{
	//AUTO MODE
	if(G_SystemModeData.unSystemMode != SYSTEM_AUTO){
		AfxMessageBox(_T("AUTO 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(LCR) != STEP0){
		AfxMessageBox(_T("LCR STEP0에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}

	G_MainWnd->m_InspectThread.fLCRLoadCorrect  = 1;
	if(G_MainWnd->m_AxisDlg.checkLCRAxisReadyPosition())
		G_MainWnd->m_InspectThread.m_fnSetBatchStep(LCR, STEP1);
}

//DCR ZERO OHM ADJUST BATCH AUTO RUN
void PIO::OnBnClickedBtnDcrZeroAdjust()
{
	//AUTO MODE
	if(G_SystemModeData.unSystemMode != SYSTEM_AUTO){
		AfxMessageBox(_T("AUTO 모드에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}	
	if(G_MainWnd->m_InspectThread.m_fnGetBatchStep(DCR) != STEP0){
		AfxMessageBox(_T("DCR STEP0에서 실행해야 합니다"),MB_ICONHAND);
		return;
	}
	G_MainWnd->m_InspectThread.fDCRZeroAdjust = 1;
	G_MainWnd->m_InspectThread.m_fnSetBatchStep(DCR, STEP1);
}




void PIO::OnBnClickedBtnOffset(UINT nID)
{
	int i, j;
	int bReturn;
	CString strValueData;

	int batch = nID - IDC_BTN_HOME_BATCH10 + 1;
#ifndef _FI
	if(batch != LCR && batch != DCR)
	{
		return;
	}
#endif
	switch(batch){
	case LCR:
		for(i =0; i< MAX_L_TEST; i++)
		{
			if(G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].bTest == 1)
			{
				j = i;
				G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].offset = G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].correctValue - G_MainWnd->m_InspectThread.m_PartData[STG_LCR].l[i]; 
				strValueData.Format(L"%f",G_MainWnd->m_RecipeSetDlg.part_Test.lcrTestInfo[i].offset);
				bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], KEY_OFFSET, strValueData);
				//G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[COL_OFFSET])->SetText(strValueData);
			}
		}
		break;
	case DCR:
		for(i =0; i< MAX_DCR_TEST; i++)
		{
			if(G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].bTest == 1)
			{
				j = MAX_L_TEST+MAX_OSC_TEST + i;
				G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].offset = G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].correctValue - G_MainWnd->m_InspectThread.m_PartData[STG_DCR].r[i]; 
				strValueData.Format(L"%f",G_MainWnd->m_RecipeSetDlg.part_Test.dcrTestInfo[i].offset);
				bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], KEY_OFFSET, strValueData);
				//G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[COL_OFFSET])->SetText(strValueData);
			}
		}
		break;
	case HIPOT:
		for(i =0; i< MAX_HIPOT_TEST; i++)
		{
			if(G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].bTest == 1)
			{
				j = MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+ i;
				G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].offset = G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].correctValue - G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].i[i]; 
				strValueData.Format(L"%f",G_MainWnd->m_RecipeSetDlg.part_Test.hipTestInfo[i].offset);
				bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], KEY_OFFSET, strValueData);
				//G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[COL_OFFSET])->SetText(strValueData);
			}
		}
		break;
	case IR:
		for(i =0; i< MAX_IR_TEST; i++)
		{
			if(G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].bTest == 1)
			{
				j = MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST+i;
				G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].offset = G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].correctValue - G_MainWnd->m_InspectThread.m_PartData[STG_HIPOT_IR].ir[i]; 
				strValueData.Format(L"%f",G_MainWnd->m_RecipeSetDlg.part_Test.irTestInfo[i].offset);
				bReturn = G_MainWnd->m_RecipeSetDlg.m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], KEY_OFFSET, strValueData);
				//G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[COL_OFFSET])->SetText(strValueData);
			}
		}
		break;
	}

	//G_MainWnd->m_RecipeSetDlg.m_GridModelRecipe.Refresh();
	AfxMessageBox(_T("Offset Set"),MB_ICONINFORMATION);
}


void PIO::OnBnClickedBtnClearBatch(UINT nID)
{
		//int batch = nID - IDC_BTN_RESET_BATCH27;

	if(IDCANCEL == AfxMessageBox(_T("BATCH CLEAR를 하면 초기 상태로 CLEAR됩니다 BATCH CLEAR를 하시겠습니까?"),MB_OKCANCEL|MB_ICONQUESTION))
		return;

	int batch=0;
		switch(nID){

			case IDC_BTN_RESET_BATCH28:{ batch = 1; G_MainWnd->m_InspectThread.stLCRBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH30:{ batch = 3; G_MainWnd->m_InspectThread.stDCRBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH35:{ batch = 8; G_MainWnd->m_InspectThread.stShuttleBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH33:{ batch = 6; G_MainWnd->m_InspectThread.stVIBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH34:{ 
										   batch = 7; 
										   G_MainWnd->m_InspectThread.stUNLOADBatch.status = CLEAR;
										   G_MainWnd->m_LabelPrint.m_partLabel.idxPrinterLabel = 0;
			} 
				break;
#ifdef _FI
			case IDC_BTN_RESET_BATCH27:{ batch = 0; G_MainWnd->m_InspectThread.stLoadBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH29:{ batch = 2; G_MainWnd->m_InspectThread.stOSCBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH31:{ batch = 4; G_MainWnd->m_InspectThread.stHIPOTBatch.status = CLEAR;} break;
			case IDC_BTN_RESET_BATCH32:{ batch = 5; G_MainWnd->m_InspectThread.stIRBatch.status = CLEAR;} break;
			//case IDC_BTN_RESET_BATCH33:{ batch = 6; G_MainWnd->m_InspectThread.stVIBatch.status = CLEAR;} break;
			//case IDC_BTN_RESET_BATCH34:{ 
															//batch = 7; 
															//G_MainWnd->m_InspectThread.stUNLOADBatch.status = CLEAR;
															//G_MainWnd->m_LabelPrint.m_partLabel.idxPrinterLabel = 0;
									   //} 
									   //break;
#endif

		}

//#ifndef _FI
//	if(batch != LCR && batch != DCR)
//	{
//		
//		batch = 6; 
//		G_MainWnd->m_InspectThread.stVIBatch.status = CLEAR;
//		return;
//	}
//#endif
	if(G_SystemModeData.unSystemMode == SYSTEM_MANUAL)
	{
		G_MainWnd->m_InspectThread.m_csCheck.Lock();
		G_MainWnd->m_InspectThread.m_fnSetBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetPrevBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.m_fnSetOldBatchStep(batch, STEP0);
		G_MainWnd->m_InspectThread.setBatchStatus(batch, CLEAR);
		G_MainWnd->m_InspectThread.stShuttleBatch.allow |= ST_SHUTTLE_INTERLOCK[batch]; //allow shuttle
		if(batch == LCR) G_MainWnd->m_InspectThread.setLCRAxisStep(KEY_LCR_AXIS_STEP, 0);
		//m_btnErrorReset[batch].SetColorChange(RGB(255,255,255),RGB(0,0,0)); 
		G_MainWnd->m_InspectThread.m_csCheck.Unlock();
	}else{
		AfxMessageBox(_T("MANUAL 모드에서 실행해야 합니다"),MB_ICONINFORMATION);
	}
}
