// MainWnd.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "nBizLDCTester.h"
#include "MainWnd.h"

// CMainWnd

IMPLEMENT_DYNAMIC(CMainWnd, CWnd)

CMainWnd	*G_MainWnd;

CMainWnd::CMainWnd()
{
	// 클래스 생성 **
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));

	wndclass.style			= CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= AfxWndProc;
	wndclass.hInstance		= AfxGetInstanceHandle();
	wndclass.hIcon			= NULL;
	wndclass.hCursor		= AfxGetApp()->LoadStandardCursor(IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wndclass.lpszMenuName	= NULL;
	wndclass.lpszClassName	= L"MainWnd";

	AfxRegisterClass(&wndclass);

	CreateEx(NULL,L"MainWnd", NULL, NULL, CRect(0,0,0,0), NULL, NULL, NULL);	
	// 클래스 생성 ##

	
}

CMainWnd::~CMainWnd()
{

}


BEGIN_MESSAGE_MAP(CMainWnd, CWnd)
	ON_WM_TIMER()
	ON_WM_CLOSE()	
END_MESSAGE_MAP()
// CMainWnd 메시지 처리기입니다.

BOOL CMainWnd::m_fnInit()
{
	G_MainWnd = (CMainWnd*)AfxGetApp()->m_pMainWnd;	

	m_ServerDlg.m_fnInit();
	m_DataHandling.m_fnInit();
	m_InspectThread.m_fnInit(&m_DataHandling, &m_SerialInterface, &m_RecipeSetDlg, &m_InstrumentDlg, &m_PIODlg);
	m_SystemSetupDlg.m_fnInit();
	m_SerialInterface.m_fnInit();
	m_RecipeSetDlg.m_fnInit();
	m_InstrumentDlg.m_fnInit();
	m_PIODlg.m_fnInit();

#ifdef _FI
	m_LabelPrint.m_fnInit();
#endif

	m_ServerDlg.OnSystemInit();
	m_InspectThread.m_fnBeginInspectThread();

	G_MainWnd->m_AxisDlg.sysInitLCRAxisPosCheck();

	return TRUE;
}



BOOL CMainWnd::m_fnDeInit()
{	
	G_AddLog(3, L"사용자 프로그램 종료");
	
	m_InspectThread.m_fnDeInit();
	G_AddLog(3, L"m_InspectThread.m_fnDeInit 종료");
	m_ServerDlg.m_fnDeInit();
	G_AddLog(3, L"m_ServerDlg.m_fnDeInit 종료");
	m_InstrumentDlg.m_fnDeInit();
	G_AddLog(3, L"m_InstrumentDlg.m_fnDeInit 종료");
	m_PIODlg.m_fnDeInit();
	G_AddLog(3, L"m_PIODlg.m_fnDeInit 종료");
	m_SerialInterface.m_fnDeInit();
	G_AddLog(3, L"m_SerialInterface.m_fnDeInit 종료");
	//1227_kys
	G_MainWnd->m_AxisDlg.m_fnDeInit();
	G_AddLog(3, L"G_MainWnd->m_AxisDlg.m_fnDeInit 종료");
	return TRUE;	
}

void CMainWnd::OnClose()
{
	m_fnDeInit();		
	__super::OnClose();
}

void CMainWnd::OnTimer(UINT_PTR nIDEvent)
{
	__super::OnTimer(nIDEvent);
}

void G_AddLog(int nLogLevel, TCHAR* chLog, ...)
{

	TCHAR wbuf[1024];

	memset(wbuf, 0x00, 1024);

	//va_list marker;
	va_list marker;
	va_start( marker, chLog );
	_vstprintf_s(wbuf, 1024, chLog, marker);

	//size_t convertedChars = 0;
	//size_t origsize = _tcslen(chLog) + 1;
	//mbstowcs_s(&convertedChars, wbuf, origsize, buf, _TRUNCATE);

	switch (nLogLevel)
	{
	case 1: //List Log
		G_MainWnd->m_ServerDlg.m_fnAddLog(wbuf);
		break;
	case 2: //File Log
		G_MainWnd->m_DataHandling.m_LogMng.m_fnLogMngWriteLog(wbuf, FALSE, TRUE);
		break;

	case 3: //List + File LogZ
		G_MainWnd->m_ServerDlg.m_fnAddLog(wbuf);
		G_MainWnd->m_DataHandling.m_LogMng.m_fnLogMngWriteLog(wbuf, FALSE, TRUE);
		break;
	default:
		AfxMessageBox(_T("Log Level Error"), MB_TOPMOST);
		break;
	}
}

void G_AddDayReportLog(wchar_t* chPanelID, ...)
{
	wchar_t buf[512];
	memset(buf, 0x00, 512*sizeof(wchar_t));

	va_list marker;
	va_start( marker, chPanelID );
	vswprintf_s(buf, 512, chPanelID, marker);

	G_MainWnd->m_DataHandling.m_DayReportMng.m_fnLogMngWriteLog(buf, FALSE, TRUE);
}

