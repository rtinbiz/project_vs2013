#pragma once

#include "stdafx.h"
#include "SeqTimeMng.h"
#include "DataHandling.h"
#include "SerialInterface.h"
#include "nBizLDCTesterDlg.h"
#include "RecipeSetDlg.h"
#include "Instrument.h"
#include "PIO.h"
#include "AxisMotion.h"
#include "VisionInspection.h"

#define WM_TIMER_100m	WM_USER + 210
#define WM_TIMER_1S		WM_TIMER_100m + 1

#define CHECK_HOLD		1
#define CHECK_SUSPEND	2
#define CHECK_NG			3

#define MODE_MANUAL		999
#define MODE_AUTO			1000

#define NO_STAGE			6

#define STG_LOAD			0
#define STG_LCR					1
#define STG_OSC					2
#define STG_DCR					3
#define STG_HIPOT_IR			4
#define STG_VI_UNLOAD		5

#define NO_BATCH_SQC	10

#define LOAD			0
#define LCR						1
#define OSC					2
#define DCR					3
#define HIPOT					4
#define IR						5
#define VI						6
#define UNLOAD				7
#define SHUTTLE				8
#define RETURNCV			9

#define STN1_VI						1
#define STN1_LOAD					5
#define STN1_UNLOAD				9

#define SHUTTLE_LOAD				0x0001
#define SHUTTLE_LCR				0x0002
#define SHUTTLE_OSC				0x0004
#define SHUTTLE_DCR				0x0008
#define SHUTTLE_IR					0x0010
#define SHUTTLE_HIPOT			0x0020
#define SHUTTLE_VI					0x0040
#define SHUTTLE_UNLOAD			0x0080
#define SHUTTLE_SHUTTLE		0x0100
#define SHUTTLE_RETURNCV		0x0200
#define SHUTTLE_ALL				0xFF //except shuttle , RETURN C/V
////////////////////////////////////////////////////////////////////////////////////////////////

enum
{
	STEP0, //READY (WAIT TO BE TRIGGERED)
	STEP1, // START (CHECK IF ALRIGHT TO START)
	STEP2, // STEP 1
	STEP3, // STEP 2
	STEP4, //
	STEP5, // STEP AXIS MOVE
	STEP6, // TEST
	STEP7,
	STEP8,
	STEP9,
	STEP10,
	STEP11,
	STEP12,
	STEP13, // 
	STEP14, // 

	STEP15, // BATCH END AND FINISH
	STEP16, // AUTO END , 
	STEP17, // SHUTTLE REQUEST AND WAIT
	STEP18,
	STEP19,  //ERROR STATE
	STEP20,   //ERROR RESET

	STEP21,
	STEP22,	 //HOLD

	STEP23, //
	STEP24, //
	STEP25, //
	STEP26, //
	STEP27, //
	STEP28, //
	STEP29, // 

	STEP30, // FI AXIS HOME READY START
	STEP31, // LCR AXIS Z HOME
	STEP32, // LCR AXIS X,Y,L HOME
	STEP33, // LCR AXIS T HOME
	STEP34, // LCR AXIS T READY
	STEP35, // LCR AXIS X,Y,L READY
	STEP36, // LCR AXIS Z READY
	STEP37, // FI AXIS HOME READY END
	STEP38, //
	STEP39, //

	STEP40, // PI AXIS HOME READY START
	STEP41, // PROBE Y P1 MOVE, PROBE Z HOME, SHORT Z HOME
	STEP42, // PROBE X HOME, SHORT X, Y HOME
	STEP43, // PROBE T,L HOME, SHORT T,L HOME
	STEP44, // PROBE T,L,Z READY, SHORT T,L,Z READY
	STEP45, // PROBE X READY, SHORT X READY
	STEP46, // PROBE Y HOME, SHORT Y READY
	STEP47, // PROBE Y READY
	STEP48, // PI AXIS HOME READY END
	STEP49, //
};

const int ST_SHUTTLE_INTERLOCK[] = 
{
	 SHUTTLE_LOAD,
	 SHUTTLE_LCR	,
	 SHUTTLE_OSC,
	 SHUTTLE_DCR	,
	 SHUTTLE_IR,
	 SHUTTLE_HIPOT,
	 SHUTTLE_VI,
	 SHUTTLE_UNLOAD
	 //SHUTTLE_ALL
};

//Batch Status 
#define CLEAR				0
#define FINISHED			1
#define CONTINUE		2
#define PART				4	//PART 있음 
#define LOCKED			8	//shuttle interlock
#define PART_ON_CV_UNLOAD		0x10 //CV Unload 존에 part 있음
#define PART_ON_CV_LOAD			0x20

//#define RETURNCV_RUNTIME_FOR_UNLOAD_LIFTUP	350 //3.5sec
#define RETURNCV_RUNTIME_FOR_UNLOAD_LIFTUP	150 //3.5sec


class CInspectThread : public CSeqTimeMng
{
public:
	CInspectThread(void);
	~CInspectThread(void);

	//Batch Sequence Struct
	//BARCODE LOAD
	typedef struct LOAD_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;

		LOAD_BATCH(){	memset(this,0x00, sizeof(LOAD_BATCH));}
	};

	LOAD_BATCH stLoadBatch;

	typedef struct LCR_BATCH
	{
		int status;
		int LCR_test_step; //
		int LCR_Axis_step; //axis position step
		int retry;
		int NG;
		int VISAFault;
		int testAccu;
		int ShortTest;

		LCR_AXIS_POSITION *pAxisData;

		TIMER timer;
		COUNTER counter;

		LCR_BATCH(){	memset(this,0x00, sizeof(LCR_BATCH));}
	};

	LCR_BATCH stLCRBatch;

	typedef struct OSC_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int retry;
		int NG;
		int VISAFault;
		int oscStep;

		OSC_BATCH(){memset(this,0x00, sizeof(OSC_BATCH));}
	};

	OSC_BATCH stOSCBatch;

	typedef struct DCR_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int retry;
		int NG;
		int VISAFault;
		int dcrStep;

		DCR_BATCH(){memset(this,0x00, sizeof(DCR_BATCH));}
	};

	DCR_BATCH stDCRBatch;

	typedef struct HIPOT_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int retry;
		int NG;
		int VISAFault;
		int hiPotStep;

		HIPOT_BATCH(){memset(this,0x00, sizeof(HIPOT_BATCH));}
	};

	HIPOT_BATCH stHIPOTBatch;

	typedef struct IR_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int retry;
		int NG;
		int VISAFault;
		int irStep;

		IR_BATCH(){memset(this,0x00, sizeof(IR_BATCH));}
	};

	IR_BATCH stIRBatch;

	typedef struct VI_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int retry;
		int NG;
		int VISAFault;
		VI_BATCH(){memset(this,0x00, sizeof(VI_BATCH));}
	};

	VI_BATCH stVIBatch;

	typedef struct UNLOAD_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;

		UNLOAD_BATCH(){memset(this,0x00, sizeof(UNLOAD_BATCH));}
	};

	UNLOAD_BATCH stUNLOADBatch;


	typedef struct SHUTTLE_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;

		int		allow;
		int		request;
		int		interlockShuttle;  //if set, other batch shouldn't move
		int		holdShuttle;	//

		int		startDelay; //1sec

		SHUTTLE_BATCH(){	memset(this,0x00, sizeof(SHUTTLE_BATCH));}
	};

	SHUTTLE_BATCH stShuttleBatch;
	
	typedef struct RETURNCV_BATCH
	{
		int status;
		TIMER timer;
		COUNTER counter;
		int	jig;	// is there a jig on C/V in unloading side?
		int run; //request from UNLOAD to run CV 0: CLEAR, LOCKED

		RETURNCV_BATCH(){memset(this,0x00, sizeof(RETURNCV_BATCH));}
	};

	RETURNCV_BATCH stRETURNCVBatch;

	/////////////////////////////////////////////////////////////////////////////////////////
	//PART INSPECTION DATA
	typedef struct PART_DATA
	{
		int model_no;
		int part_no;

		wchar_t part_model[256];
		int part; //1: part, 0: no part

		//measured value
		double l[MAX_L_TEST]; //Inductance
		wchar_t phase[256]; //Oscilloscope
		double oscPhase;
		double oscHz;
		double oscV2;

		double r[MAX_DCR_TEST]; //Resistance
		double i[MAX_HIPOT_TEST]; //HiPot Leakage Current
		double ir[MAX_IR_TEST]; //Insulation Resistance
		wchar_t viRes[MAX_VI_TEST][256]; //

		// pass:1, fail:0
		int lPass[MAX_L_TEST]; //Inductance
		int phasePass[MAX_OSC_TEST]; //Oscilloscope
		int rPass[MAX_DCR_TEST]; //Resistance
		int iPass[MAX_HIPOT_TEST]; //HiPot Leakage Current
		int irPass[MAX_IR_TEST]; //Insulation Resistance
		int viPass[MAX_VI_TEST]; //Insulation Resistance

		int NG; // a step measure fails, count up
		int VISAFault[NO_STAGE]; // instrument Fault
		int finalPass; //1: Pass
		wchar_t ngItems[1024];
		//20160308_KYS_SoftDetectionSignal
		int SoftDetect;

		PART_DATA(){	memset(this,0x00, sizeof(PART_DATA));}
		void Reset(){memset(this,0x00, sizeof(PART_DATA));}
		void CopyFrom(PART_DATA *from){memcpy(this, (void *)from, sizeof(PART_DATA));}
	}; 

	PART_DATA m_PartData[NO_STAGE];

	AxisMotion				m_AxisDlg;
	CVisionInspection		m_vi;

	int m_fnInit();
	int m_fnInit(CDataHandling				* pDataHandling, 
					CSerialInterface			*pSerialInterface, 
					CRecipeSetDlg				*pRecipeSetDlg,
					CInstrument				*pInstrumentDlg,
					PIO							*pPIODlg
					//AxisMotion					*pAxisDlg
					);
	int m_fnBeginInspectThread();
	void m_fnDeInit();

	int			m_nSeqMode;	// loading or unloading
	int			m_nScanCount;
	int			m_nuTotalScanCount;
	
	//batch step
	void			m_fnClearBatchStep(int batch);
	void			m_fnSetBatchStep(int nbatch, int nStep);
	int				m_fnGetBatchStep(int batch);
	int				m_fnGetPrevBatchStep(int batch);
	void			m_fnSetPrevBatchStep(int batch, int nStep);
	void			m_fnSetOldBatchStep(int batch, int nOldStep);
	int				m_fnGetOldBatchStep(int batch);
	BOOL			m_fnCheckBatchStep(int batch);
	int				m_fnRetryBatchStep(int batch);

	//transfer part data between batches

	
protected:

	DECLARE_MESSAGE_MAP()

	CDataHandling			*m_pDataHandling;
	CSerialInterface		*m_pSerialInterface;
	CRecipeSetDlg			*m_pRecipeSetDlg;	
	CInstrument			*m_pInstrumentDlg;
	PIO						*m_pPIO;
	AxisMotion				*m_pAxis;

	int			m_nAutoMoveCount;
	int			m_nAutoInFocusCount;
	int			m_AFRetryCount;		//2013_01_14
	
	void		m_fnCheck();
	void		m_fnPosCheckMotion();

	void		m_fnCheckUnLoading();
	void		m_fnScanCountPlus();

	int				m_nCurStep[NO_BATCH_SQC];
	int				m_nPrevStep[NO_BATCH_SQC];
	int				m_nOldStep[NO_BATCH_SQC];

public:
	int		checkStatus; //true: suspend
	int		suspendCount; //thread 
	int		suspendCount_Input; //thread 


	CCriticalSection m_csCheck;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);


	int m_fnShiftPartData();
	int m_fnShiftPartData(int batchTo, int batchFrom);
	int m_fnClearPartData(int batch);
	int m_fnClearPartDataAll(void);
	int m_fnInitPartData(int batch, int model_no, int part_no);
	int TimerAutoRun(void);
	int stg1Load(void);
	int stg2LCRTest(void);
	int stg2LCRLoadCorrection(void);
	int stg4DCRTest(void);
	int stg7ShuttleRun(void);
	int stg6VITest(void);
	int stg6Unload(void);
	


	int EMStop(void);
	int holdAutoRun(void);
	int resumeAutoRun(void);
	int holdBatch(int batch);
	int resetBatch(int batch);
	int continueBatch(int batch);
	int batchTimer(void);
	int setLCRAxisStep(LPCTSTR lpszKeyName, int step);
	void turnLight(int sw);
	int summarizeResult(void);
	int setBatchStatus(int batch, int status);
	int restoreInspectionStatus(void);
	int errorRoutine(void);
	int errorResetRoutine(void);
	void m_fnSetInspectionStep(int batch, int nStep);

	CTime ctUnloadTime;
	int fLCRLoadCorrect;
	int fDCRZeroAdjust;

	int showNGResult(void);

#ifdef _FI
	int stg3OSCTest(void);
	int stg5HIPOTTest(void);
	int stg5IRTest(void);
	//int stg6Unload(void);
	int stg8ReturnConveyorRun(void);
#endif

};





