#pragma once
//#include "afxdialogex.h"
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "1.Common\RoundButton\RoundButton2.h"
#include "afxwin.h"


class CSystemSetupDlg :public CDialogEx
{
	DECLARE_DYNAMIC(CSystemSetupDlg)

public:
	CSystemSetupDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	~CSystemSetupDlg(void);


	// 대화 상자 데이터입니다.
	enum { IDD = IDD_SYSTEM_SETTING };

protected:

	CStringArray m_strComPortArray;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	void m_fnInit();
	virtual BOOL OnInitDialog();
	void m_fnSystemUILoad();
	void m_fnUpdateComport();

	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);

	CGridCtrl m_ctrMaintGrid;
	afx_msg void OnBnClickedBtnSysParamReadOnly();
	CRoundButton2 m_btnSystemReadOnly;
	CRoundButton2 m_btnSystemSetEditable;
	afx_msg void OnBnClickedBtnSysParamSetEdit();
	afx_msg void OnBnClickedBtnSysParamSave();
	CRoundButton2 m_btnSystemParamSave;
};

