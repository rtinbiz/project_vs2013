#if !defined(AFX_CLIENTSOCKET_H__749C32F2_4738_46E0_9396_FE9BB1F79606__INCLUDED_)
#define AFX_CLIENTSOCKET_H__749C32F2_4738_46E0_9396_FE9BB1F79606__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClientSocket.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CClientSocket command target
#include <afxsock.h>
#define	UM_CIM_CLOSE					WM_USER+2	// Client 소켓 해제
#define	UM_CIM_RECEIVE					WM_USER+3	// Client로 부터 수신되는 데이터
#define UM_CIM_CONNECT_OK				WM_USER+4

class CClientSocket : public CAsyncSocket
{
// Attributes
public:
	int	id; //ip[3]
	BOOL InitSocket(CWnd* pWnd);	
	
// Operations
public:
	CClientSocket();
	virtual ~CClientSocket();
	
protected:
	
// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClientSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClientSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	CWnd*		m_wndParent;		// Parent 윈도우
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTSOCKET_H__749C32F2_4738_46E0_9396_FE9BB1F79606__INCLUDED_)
