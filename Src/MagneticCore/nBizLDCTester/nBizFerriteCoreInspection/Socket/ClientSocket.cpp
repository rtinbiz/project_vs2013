// ClientSocket.cpp : implementation file
//

#include "stdafx.h"
#include "ClientSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClientSocket

CClientSocket::CClientSocket()
{	
	m_wndParent = NULL;
}

CClientSocket::~CClientSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CClientSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CClientSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CClientSocket member functions


BOOL CClientSocket::InitSocket(CWnd* pWnd)
{
	BOOL bInit;
	CString MsgErr;

	m_wndParent = pWnd;

	bInit = AfxSocketInit();
	if(!bInit)
	{
		MsgErr.Format(_T("%d 소켓 초기화 에러") , GetLastError());
		G_AddLog(3,L"%s", MsgErr);
		return FALSE;
	}

	return TRUE;
}

//서버에서 보낸 데이터를 화면에 보인다...
void CClientSocket::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class	
	wchar_t receiveBuf[4096];
	memset(receiveBuf, 0x00, 4096);

	int nRead = Receive(receiveBuf, sizeof(receiveBuf));	

	if(nRead <= 0 || nRead >= 4096)
	{
		G_AddLog(3,L"Return Send Size %d", nRead);

		return;
	}
	

	if(m_wndParent != NULL)
		m_wndParent->SendMessage(UM_CIM_RECEIVE, WPARAM(nRead), LPARAM(receiveBuf));
	
	CAsyncSocket::OnReceive(nErrorCode);
}

void CClientSocket::OnConnect(int nErrorCode) 
{	

	if(m_wndParent != NULL)
		m_wndParent->SendMessage(UM_CIM_CONNECT_OK, WPARAM(nErrorCode), LPARAM(id));

	CAsyncSocket::OnConnect(nErrorCode);
}

void CClientSocket::OnClose(int nErrorCode) 
{	
	if(m_wndParent != NULL)
		m_wndParent->SendMessage(UM_CIM_CLOSE, NULL,  LPARAM(id));

	CAsyncSocket::OnClose(nErrorCode);
}





