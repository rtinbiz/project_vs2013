#include "stdafx.h"
#include "RecipeSetDlg.h"
#include "nBizLDCTester.h"
#include "afxdialogex.h"

IMPLEMENT_DYNAMIC(CRecipeSetDlg, CDialogEx)

extern CMainWnd	*G_MainWnd;

CRecipeSetDlg::CRecipeSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRecipeSetDlg::IDD, pParent)
{
}

CRecipeSetDlg::~CRecipeSetDlg(void)
{
}


void CRecipeSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID_MODEL, m_GridModel);
	DDX_Control(pDX, IDC_GRID_MODEL_RECIPE, m_GridModelRecipe);
	DDX_Control(pDX, IDC_BTN_RECIPE_DELETE, m_btnRecipeDelete);
	DDX_Control(pDX, IDC_BTN_RECIPE_NEW, m_btnRecipeNew);
	DDX_Control(pDX, IDC_BTN_RECIPE_SAVE, m_btnRecipeSave);
	DDX_Control(pDX, IDC_BTN_APPLY, m_btnRecipeApply);
	DDX_Control(pDX, IDC_BUTTON2, m_btnResetAccu);
	DDX_Control(pDX, IDC_BUTTON3, m_btnResetProductionCount);
	DDX_Control(pDX, IDC_BUTTON5, m_btnSetLotTarget);
	DDX_Control(pDX, IDC_PRODUCT_PIC, m_ctlImageArea);
}


BEGIN_MESSAGE_MAP(CRecipeSetDlg, CDialogEx)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_MODEL, OnGridDblClick)
	ON_NOTIFY(NM_CLICK, IDC_GRID_MODEL, OnGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_GRID_MODEL_RECIPE, OnGridDblClick2)
	ON_NOTIFY(NM_CLICK, IDC_GRID_MODEL_RECIPE, OnGridClick2)
	ON_BN_CLICKED(IDC_BTN_APPLY, &CRecipeSetDlg::OnBnClickedBtnApply)
	ON_BN_CLICKED(IDC_BTN_RECIPE_DELETE, &CRecipeSetDlg::OnBnClickedBtnRecipeDelete)
	ON_BN_CLICKED(IDC_BTN_RECIPE_NEW, &CRecipeSetDlg::OnBnClickedBtnRecipeNew)
	ON_BN_CLICKED(IDC_BTN_RECIPE_SAVE, &CRecipeSetDlg::OnBnClickedBtnRecipeSave)
	ON_BN_CLICKED(IDC_BUTTON2, &CRecipeSetDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CRecipeSetDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &CRecipeSetDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


void CRecipeSetDlg::OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
}

void CRecipeSetDlg::OnGridClick(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//find selected production recipe
	recipeSelected = m_GridModel.GetCell(pItem->iRow, pItem->iColumn)->GetText();
	GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(recipeSelected);
	//load recipe data
	clearGridModelRecipe();
	fillGridModelRecipe(recipeSelected);
	//1224_KYS
	ModelImageLoad(recipeSelected);

}

void CRecipeSetDlg::OnGridDblClick2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Double Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

void CRecipeSetDlg::OnGridClick2(NMHDR *pNotifyStruct, LRESULT* /*pResult*/)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*) pNotifyStruct;
	//Trace(_T("Clicked on row %d, col %d\n"), pItem->iRow, pItem->iColumn);
}

BOOL CRecipeSetDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	CenterWindow();
	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initGridModel();
	initGridModelRecipe();
	fillGridModel();
	applyRecipeInProduction(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

	CString str;
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK]);
	GetDlgItem(IDC_EDIT6)->SetWindowText(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	GetDlgItem(IDC_EDIT4)->SetWindowText(str);

	//1224_kys_ImageArea
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);

	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		//AddLogText(L"Failed to create image view");
		AfxMessageBox(L"Failed to create image view", MB_ICONHAND);
		return FALSE;
	}
	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);
	//
	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CRecipeSetDlg::m_fnInit(void)
{
	CDialog::Create(IDD, AfxGetApp()->m_pMainWnd);		
	
	//m_fnRecipeLoad();

}


int CRecipeSetDlg::initGridModel(void)
{
	try {
		m_GridModel.SetRowCount(30);
		m_GridModel.SetColumnCount(2);
		m_GridModel.SetFixedRowCount(1);		
		m_GridModel.SetFixedColumnCount(1);
		m_GridModel.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridModel.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridModel.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;
				if (col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("                          모 델 명                             "));					
				}
			}
			else
			{
				if(col == 0)
				{
					if (row != 0)
					{
						Item.nFormat = DT_VCENTER | GVIF_FORMAT | DT_CENTER;
						//Number_matching
						if (row<5)
							Item.strText.Format(L"%d", row);
						else if (row>6)
							Item.strText.Format(L"%d", row - 2);
						else if (row == 5)
							Item.strText.Format((L"%d_%d"),row-1,row-4);
						else if (row == 6)
							Item.strText.Format((L"%d_%d"), row-2, row-4);
					}
				}
			}
			m_GridModel.SetItem(&Item);
		}
	}

	m_GridModel.AutoSize(GVS_BOTH);
	m_GridModel.SetTextColor(RGB(0, 0, 105));
	//m_GridModel.ExpandToFit(true);
	//m_GridModel.AutoSizeColumns();
	m_GridModel.SetEditable(FALSE);
	m_GridModel.Refresh();

	return 0;
}


int CRecipeSetDlg::initGridModelRecipe(void)
{
	//Grid init

	// system param grid **********************
	try {
		m_GridModelRecipe.SetRowCount(MAX_TOTAL_TEST + 1);
		m_GridModelRecipe.SetColumnCount(NO_RECIPE_ITEM + 2);
		m_GridModelRecipe.SetFixedRowCount(1);		
		m_GridModelRecipe.SetFixedColumnCount(1);
		m_GridModelRecipe.SetListMode(FALSE);
	}
	catch (CMemoryException* e)
	{
		e->ReportError();
		e->Delete();
		return 0;
	}

	//
	for (int row = 0; row < m_GridModelRecipe.GetRowCount(); row++)
	{
		for (int col = 0; col < m_GridModelRecipe.GetColumnCount(); col++)
		{
			GV_ITEM Item;
			Item.mask = GVIF_TEXT|GVIF_FORMAT;
			Item.col = col;
			Item.row = row;

			if(row == 0)
			{			
				Item.nFormat = DT_VCENTER|GVIF_FORMAT;

				if (col == 0)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("  검사 항목  "));	

				}
				else if( col == 1)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("Term 1"));
				}
				else if( col == 2)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("Term 2"));
				}
				else if( col == 3)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("Term 3"));
				}
				else if( col == 4)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("Term 4"));
				}
				else if( col == 5)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 시험 유무 "));					
				}
				else if (col == 6)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("시험조건[kHz]"));					
				}
				else if (col == 7)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" [Vac] "));					
				}
				else if (col == 8)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" [Vdc] "));					
				}
				else if (col == 9)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" [sec] "));					
				}
				else if (col == 10)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" [A] "));					
				}
				else if (col == 11)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" TurnRatio "));					
				}
				else if( col == 12)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 하한 "));
				}
				else if( col == 13)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 상한 "));					
				}
				else if( col == 14)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T("CORRECT VALUE"));					
				}
				else if( col == 15)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" OFFSET "));					
				}
				else if( col == 16)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" 검사항목 "));					
				}
				else if( col == 17)
				{
					Item.nFormat = DT_CENTER|DT_VCENTER|DT_SINGLELINE|DT_END_ELLIPSIS|DT_NOPREFIX;
					Item.strText.Format(_T(" Unit "));					
				}

			}//if(row == 0)
			else
			{
				if(col == 0)
				{
					if(row == 1)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("인덕턴스     [1]"));					
					}
					else if(row == 2)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("   (0A)        [2]"));					
					}
					else if(row == 3)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [3]"));					
					}
					else if(row == 4)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [4]"));					
					}
					else if(row == 5)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [5]"));					
					}
					else if(row == 6)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [6]"));					
					}
					else if(row == 7)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [7]"));					
					}
					else if(row == 8)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" (전류인가)  [8]"));					
					}
					else if(row == 9)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [9]"));					
					}
					else if(row == 10)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [10]"));					
					}
					else if(row == 11)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [11]"));					
					}
					else if(row == 12)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" (Leakage)   [12]"));					
					}
					else if(row == 13)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [13]"));					
					}
					else if(row == 14)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                 [14]"));					
					}
					else if(row == 15)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("Oscilloscope[1]"));					
					}
					else if(row == 16)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" DCR         [1]"));					
					}
					else if(row == 17)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [2]"));					
					}
					else if(row == 18)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [3]"));					
					}
					else if(row == 19)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [4]"));					
					}
					else if(row == 20)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [5]"));					
					}
					else if(row == 21)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [6]"));					
					}
					else if(row == 22)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T(" 내전압      [1]"));					
					}
					else if(row == 23)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [2]"));					
					}
					else if(row == 24)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [3]"));					
					}
					else if(row == 25)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [4]"));					
					}
					else if(row == 26)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [5]"));					
					}					
					else if(row == 27)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("절연저항    [1]"));					
					}
					else if(row == 28)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [2]"));					
					}
					else if(row == 29)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [3]"));					
					}
					else if(row == 30)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [4]"));					
					}
					else if(row == 31)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
						Item.strText.Format(_T("                [5]"));					
					}
					else if (row == 32)
					{
						Item.nFormat = DT_VCENTER | GVIF_FORMAT;
						Item.strText.Format(_T(" 비전검사        "));
					}
				}
				if(col == 5)
				{
					m_GridModelRecipe.SetCellType(row, col, RUNTIME_CLASS(CGridCellCheck));
					Item.crBkClr = (RGB(0x50, 0x50, 0x50));
					Item.nFormat =  DT_VCENTER|GVIF_FORMAT;
					Item.strText.Format(_T(" 시험 "));
				}
				if(col == 12)
				{
					if( row >= 22 && row <= 26)
					{
						m_GridModelRecipe.GetCell(row,col)->SetBackClr(RGB(230,230,230));
					}
				}
				if(col == 13)
				{
					if( row >= 27 && row <= 31)
					{
						m_GridModelRecipe.GetCell(row,col)->SetBackClr(RGB(230,230,230));
					}
				}
				if(col == NO_RECIPE_ITEM + 1)
				{
					if( row >= 1 && row <= 14)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"[uH]");					
					}
					if( row >= 16 && row <= 21)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"[mOhm]");					
					}
					if( row >= 22 && row <= 26)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"[mA]");					
					}
					if( row >= 27 && row <= 31)
					{
						Item.nFormat =  DT_VCENTER|GVIF_FORMAT|DT_CENTER;
						Item.strText.Format(L"[MOhm]");					
					}
					m_GridModelRecipe.GetCell(row,col)->SetBackClr(RGB(230,230,230));
				}
			}//if(row == 0)
			m_GridModelRecipe.SetItem(&Item);
		}
	}

	//m_GridModelRecipe.AutoSize(GVS_BOTH);
	m_GridModelRecipe.SetTextColor(RGB(0, 0, 105));
	m_GridModelRecipe.AutoSizeColumns();
	m_GridModelRecipe.ExpandToFit(true);

	//m_GridModelRecipe.SetEditable(FALSE);
	//m_GridModelRecipe.Refresh();


	return 0;
}


int CRecipeSetDlg::fillGridModel(void)
{
	int row = 1;
	

	CString strRecipe, strRecipeName;

	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		int IsPrefixedModelName = 0;
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			for (int i = 0; i < 11; i++)
			{
				if (strRecipeName == PrefixedModelName[i])
				{
					m_GridModel.GetCell(i + 1, 1)->SetText(strRecipeName);
					IsPrefixedModelName++;
				}

			}
				if (IsPrefixedModelName==0)
						
						{ 
							m_GridModel.GetCell(row + 11, 1)->SetText(strRecipeName);
							row++;
						}
				
			////MODEL-1
			//if (strRecipeName == L"AE_EV_OBC_TR")
			//{
			//	m_GridModel.GetCell(1, 1)->SetText(strRecipeName);
			//}
			////MODEL-2
			//else if (strRecipeName == L"AE_EV_OBC_PFC_IN")
			//{
			//	m_GridModel.GetCell(2, 1)->SetText(strRecipeName);
			//}
			////MODEL-3
			//else if (strRecipeName == L"AE_EV_OBC_IN")
			//{
			//	m_GridModel.GetCell(3, 1)->SetText(strRecipeName);
			//}
			////MODEL-4
			//else if (strRecipeName == L"JF_AE_DE_PHEV_OBC_TR")
			//{
			//	m_GridModel.GetCell(4, 1)->SetText(strRecipeName);
			//}
			////MODEL-5
			//else if (strRecipeName == L"JF_AE_DE_PHEV_OBC_PFC_IN")
			//{
			//	m_GridModel.GetCell(7, 1)->SetText(strRecipeName);
			//}
			////MODEL-6
			//else if (strRecipeName == L"6.6kW_OBC_LLC")
			//{
			//	m_GridModel.GetCell(8, 1)->SetText(strRecipeName);
			//}
			////MODEL-7
			//else if (strRecipeName == L"6.6kW_OBC_PFC_IN")
			//{
			//	m_GridModel.GetCell(9, 1)->SetText(strRecipeName);
			//}
			////MODEL-8
			//else if (strRecipeName == L"COMP_FILTER")
			//{
			//	m_GridModel.GetCell(10, 1)->SetText(strRecipeName);
			//}
			////MODEL-9
			//else if (strRecipeName == L"PS_EV_OBC")
			//{
			//	m_GridModel.GetCell(11, 1)->SetText(strRecipeName);
			//}
			////MODEL-4-1
			//else if (strRecipeName == L"JF_AE_DE_PHEV_OBC_TR_1")
			//{
			//	m_GridModel.GetCell(5, 1)->SetText(strRecipeName);
			//}
			////MODEL-4-2
			//else if (strRecipeName == L"PFC_INDUCTOR")
			//{
			//	m_GridModel.GetCell(6, 1)->SetText(strRecipeName);
			//}
			//else
			//{
			//	m_GridModel.GetCell(row+11, 1)->SetText(strRecipeName);
			//	row++;
			//}
			//m_GridModel.GetCell(row++, 1)->SetText(strRecipeName);
			
		}//
	}
	strRecipe.Empty();
	strRecipeName.Empty();
	m_GridModel.Refresh();
	return 0;
}


void CRecipeSetDlg::OnBnClickedBtnApply()
{
	if(recipeSelected != L"")
	{
		//selected recipe 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_SYSTEM_PARAM, KEYNAME_SELECTED_RECIPE, recipeSelected);	
		//G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel = recipe;
		wcscpy_s(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel, (LPCTSTR)recipeSelected);
		// set the recipe test data 

		//manual mode
		applyRecipeInProduction(recipeSelected);
		//close dlg
		//apply recipe in LCR Axis Recipe
		G_MainWnd->m_AxisDlg.GetDlgItem(IDC_STATIC_LCR_AXIS_RECIPE)->SetWindowText(recipeSelected);
		G_MainWnd->m_AxisDlg.fillGridLCRAxis(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		//
		//Instrument LCR Correction
		G_MainWnd->m_InstrumentDlg.GetDlgItem(IDC_LCR_CORR_MODEL)->SetWindowText(recipeSelected);
		G_MainWnd->m_InstrumentDlg.readGridLCRCorrectRef();

		//G_MainWnd->m_InspectThread.fLCRLoadCorrect = 1; //when model changed, correction before measure

		//Main Dlg
		G_MainWnd->m_ServerDlg.m_staticModel.SetText(part_Test.part_model);
		G_MainWnd->m_ServerDlg.m_staticType.SetText(part_Test.part_type);
		G_MainWnd->m_ServerDlg.fillGridMain(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);

		//Master test reset
		G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_OK] = 0;
		G_MainWnd->m_DataHandling.m_FlashData.data[MASTER_TEST_NG] = 0;
		G_MainWnd->m_ServerDlg.m_masterTest_OK.SetLedState(GRAY_LIGHT);
		G_MainWnd->m_ServerDlg.m_masterTest_NG.SetLedState(GRAY_LIGHT);
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_OK],L"0"); 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[MASTER_TEST_NG],L"0"); 

		//InspectThread
		G_MainWnd->m_DataHandling.m_FlashData.data[LCR_CORRECTION] = 0;
		G_MainWnd->m_DataHandling.m_FlashData.data[DCR_ZERO_ADJUST] = 0; 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[LCR_CORRECTION],L"0"); 
		G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, ST_TABLE_SYSTEM_F_COL[DCR_ZERO_ADJUST],L"0"); 

		//Datahandling: new image data folder
		CString str;
		str.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSavePath, recipeSelected );
		G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

		str.Format(L"%s\\%s", G_MainWnd->m_DataHandling.m_chImageSaveNGPath, recipeSelected);
		G_MainWnd->m_DataHandling.m_fnMakeDirectory(str);

		//Label Printer
#ifdef _FI
		G_MainWnd->m_LabelPrint.setModel(G_MainWnd->m_DataHandling.m_SystemParam.ProductionModel);
		memset(G_MainWnd->m_LabelPrint.m_partLabel.QRcodeScan, 0x00, 256 * sizeof(char));//1127_KYS_ADD
#endif

		ShowWindow(SW_HIDE);
	}
}

//load Recipe Selected in GridModel
int CRecipeSetDlg::fillGridModelRecipe(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;



	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniRecipe.SetFileName(finder.GetFilePath());
				//load recipe data in GridModeRecipe
				strValueData = m_fnReadIniFile( SECTION_RECIPE_DATA, KEY_MODEL,L"");
				GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(strValueData);
				strValueData = m_fnReadIniFile( SECTION_RECIPE_DATA, KEY_TYPE,L"");
				GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(strValueData);
				strValueData = m_fnReadIniFile( SECTION_RECIPE_DATA, KEY_LOTNO,L"");

				for(j=0; j < MAX_TOTAL_TEST ; j++)
				{
					for(i=0; i < NO_RECIPE_ITEM ; i++)
					{
						if(i ==4)//시험 유무
						{
							strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[i],L"");
							CGridCellCheck * pCell = (CGridCellCheck *) m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i]);
							BOOL bTest = pCell->GetCheck();
							if(strValueData == "1")
								pCell->SetCheck(1);
							else
								pCell->SetCheck(0);
						}
						else
						{
							strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[i],L"");
							m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(strValueData);
						}
					}
				}//for(j=0; j<26 ; j++)
				break;
			}
		}//if(strRecipe.Right(4) == L".ini")
	}
	m_GridModelRecipe.Refresh();
	return 0;
}

int CRecipeSetDlg::applyRecipeInProduction(CString recipe)
{
	int row = 1;

	CString strRecipe, strRecipeName;
	int j;
	BOOL bReturn = FALSE;
	CString strValueData;



	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);
			if(strRecipeName == recipe)
			{
				m_iniRecipe.SetFileName(finder.GetFilePath());
				bReturn =true; //found
				break;
			}
		}
	}

	if(bReturn)
	{
		memset(part_Test.part_model, 0x00, 256);
		memset(part_Test.part_type, 0x00, 256);
		if(strRecipeName.GetLength() < 256)
			memcpy(part_Test.part_model, strRecipeName, strRecipeName.GetLength()*sizeof(wchar_t));
		else
			G_AddLog(3,L"if(strRecipeName.GetLength() < 256) < 256) fail ");
		//Part type
		strValueData = m_fnReadIniFile( SECTION_RECIPE_DATA, KEY_TYPE,L"");
		if(strValueData.GetLength() < 256)
			memcpy(part_Test.part_type, strValueData, strValueData.GetLength()*sizeof(wchar_t));
		else
			G_AddLog(3,L"if(strValueData.GetLength() < 256) fail  ");
	}

	if(bReturn)
	for(j=0; j< MAX_TOTAL_TEST ; j++)
	{
		if(j >=0 && j < MAX_L_TEST) //LCR test
		{
			//
			part_Test.lcrTestInfo->no = j;
			//terminal
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].ch1term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].ch1term2 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].ch2term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].ch2term2 = _wtoi(strValueData);
			//test?
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].bTest = _wtoi(strValueData);
			//
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].kHz = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].Vac = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].Vdc = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].sec = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].Ibias = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[10],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].shortPin = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].lowerLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].upperLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[13],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].correctValue = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[14],L"");
			part_Test.lcrTestInfo[part_Test.lcrTestInfo->no].offset = _wtof(strValueData);
		}

		if(j == (MAX_L_TEST + MAX_OSC_TEST - 1)) //OSC test
		{
			//
			part_Test.oscTestInfo->no = j - MAX_L_TEST;
			//terminal
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].ch1term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].ch1term2 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].ch2term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].ch2term2 = _wtoi(strValueData);
			//test?
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].bTest = _wtoi(strValueData);
			//
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].kHz = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].Vac = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].Vdc = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].sec = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].Ibias = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[10],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].turnRatio = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].lowerLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].upperLimit = _wtof(strValueData);

			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[13],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].correctValue = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[14],L"");
			part_Test.oscTestInfo[part_Test.oscTestInfo->no].offset = _wtof(strValueData); 

		}

		if(j >= (MAX_L_TEST + MAX_OSC_TEST) && j < (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST)) //DCR test
		{
			//
			part_Test.dcrTestInfo->no = j - MAX_L_TEST - MAX_OSC_TEST;
			//terminal
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].ch1term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].ch1term2 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].ch2term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].ch2term2 = _wtoi(strValueData);
			//test?
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].bTest = _wtoi(strValueData);
			//
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].kHz = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].Vac = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].Vdc = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].sec = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].Ibias = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].lowerLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].upperLimit = _wtof(strValueData);

			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[13],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].correctValue = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[14],L"");
			part_Test.dcrTestInfo[part_Test.dcrTestInfo->no].offset = _wtof(strValueData); 

		}

		if(j >= (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST) && j < (MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST)) //HIPOT test
		{
			//
			part_Test.hipTestInfo->no = j - MAX_L_TEST - MAX_OSC_TEST - MAX_DCR_TEST;
			//terminal
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].ch1term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].ch1term2 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].ch2term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].ch2term2 = _wtoi(strValueData);
			//test?
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].bTest = _wtoi(strValueData);
			//
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].kHz = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].Vac = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].Vdc = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].sec = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[9],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].Ibias = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].lowerLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].upperLimit = _wtof(strValueData);

			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[13],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].correctValue = _wtof(strValueData);
			strValueData = m_fnReadIniFile( ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[14],L"");
			part_Test.hipTestInfo[part_Test.hipTestInfo->no].offset = _wtof(strValueData); 

		}

		if (j >= (MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST) && j < (MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST)) //IR test
		{
			//
			part_Test.irTestInfo->no = j - MAX_L_TEST - MAX_OSC_TEST - MAX_DCR_TEST - MAX_HIPOT_TEST;
			//terminal
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[0], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].ch1term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[1], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].ch1term2 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[2], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].ch2term1 = _wtoi(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[3], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].ch2term2 = _wtoi(strValueData);
			//test?
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].bTest = _wtoi(strValueData);
			//
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[5], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].kHz = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[6], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].Vac = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[7], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].Vdc = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[8], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].sec = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[11], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].lowerLimit = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[12], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].upperLimit = _wtof(strValueData);

			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[13], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].correctValue = _wtof(strValueData);
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[14], L"");
			part_Test.irTestInfo[part_Test.irTestInfo->no].offset = _wtof(strValueData);

		}

		if (j >= (MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST) && j < (MAX_L_TEST + MAX_OSC_TEST + MAX_DCR_TEST + MAX_HIPOT_TEST + MAX_IR_TEST + MAX_VI_TEST)) //VI test
		{
			//
			part_Test.viTestInfo->no = j - MAX_L_TEST - MAX_OSC_TEST - MAX_DCR_TEST - MAX_HIPOT_TEST - MAX_IR_TEST;

			//test?
			strValueData = m_fnReadIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[4], L"");
			part_Test.viTestInfo[part_Test.viTestInfo->no].bTest = _wtoi(strValueData);
			//

		}

	}//for(j=0; j<

	//
	if(bReturn)
	{
		//Axis
		G_MainWnd->m_AxisDlg.applyAxisData(recipe);

	}
	return 0;
}

int CRecipeSetDlg::saveRecipe(CString recipe)
{
	//find recipe
	CString strRecipe, strRecipeName, strRecipeFullPath;
	bool filefound = false;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(RECIPE_FOLDER_PATH);

	while (bWorking) 
	{		
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;
		strRecipe = (LPCTSTR)finder.GetFileName();
		if(strRecipe.Right(4) == L".ini") // R_*.ini (*: recipe name)
		{
			strRecipeName = (LPCTSTR)strRecipe.Left(strRecipe.GetLength()-4);

			if(strRecipeName == recipe)
			{
				strRecipeFullPath = (LPCTSTR)finder.GetFilePath();
				m_iniRecipe.SetFileName(strRecipeFullPath);
				filefound = true;
				break;
			}
		}//
	}

	//if not make new recipe
	if(!filefound)
	{
		strRecipeFullPath = RECIPE_FOLDER  + recipe + L".ini";
		m_iniRecipe.SetFileName(strRecipeFullPath);
	}

	//save recipe data from grid
	int i, j;
	BOOL bReturn = FALSE;
	CString strValueData;



	// ....................
	GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(strValueData);
	bReturn = m_fnWriteIniFile( SECTION_RECIPE_DATA, KEY_MODEL, strValueData);
	GetDlgItem(IDC_EDIT_TYPE)->GetWindowText(strValueData);
	bReturn = m_fnWriteIniFile( SECTION_RECIPE_DATA, KEY_TYPE, strValueData);

	for(j=0; j< MAX_TOTAL_TEST ; j++)
	{
		for(i=0; i < NO_RECIPE_ITEM ; i++)
		{
			if(i ==4)//시험 유무
			{
				CGridCellCheck * pCell = (CGridCellCheck *) m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i]);
				BOOL bTest = pCell->GetCheck();
				if(bTest)
					bReturn = m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[i],L"1");
				else
					bReturn = m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[i],L"0");
			}else
			{
				strValueData = m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->GetText();
				bReturn = m_fnWriteIniFile(ST_RECIPE_TEST_ITEM[j], ST_RECIPE_UNIT_TEST[i], strValueData);
			}

		}
	}
	return 0;
}

BOOL CRecipeSetDlg::m_fnWriteIniFile(LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString) 
{
	BOOL bReturn = FALSE;

	bReturn = m_iniRecipe.WriteProfileString(lpszSection, lpszKeyName, lpString);
	
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";		
		//G_AddLog(3,L"%s 파일 기록 Error : 섹션%s, 키네임%s, 스트링%s", strMessage, lpszSection, lpszKeyName, lpString);
	}	
	return bReturn;
}

CString CRecipeSetDlg::m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default) 
{
	BOOL bReturn = FALSE;

	wchar_t chGetName[100];
	memset(chGetName, 0x00, 100);

	LPCTSTR lpDefault = L"";
	if(default != 0)
	{
		lpDefault = default;
	}

	bReturn = m_iniRecipe.GetProfileString(lpszSection, lpszKeyName, lpDefault, chGetName, 100);
	if(bReturn == FALSE)
	{
		//CString strMessage;
		//strMessage = "RECIPE";
		//G_AddLog(3,L"%s 파일 Read Error : 섹션%s, 키네임%s", strMessage, lpszSection, lpszKeyName);
	}
	return chGetName;
}



void CRecipeSetDlg::OnBnClickedBtnRecipeDelete()
{
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"관리자 모드로 실행할수 있습니다",MB_ICONHAND);
	}else
	{
		CString modelSelected;
		GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(modelSelected);
		//if (modelSelected != "")
		//{
			//saveRecipe(modelSelected);
		//}

		CString strPath = RECIPE_FOLDER; // 여기에 경로를.. 
		strPath.Format(L"%s%s.ini", RECIPE_FOLDER, modelSelected);

		CString strMsg;
		strMsg.Format(L"%s File을 삭제하시겠습니까?", modelSelected);

		if (AfxMessageBox(strMsg, MB_YESNO | MB_SETFOREGROUND | MB_TOPMOST) == IDYES)
		{
			
			DeleteFile(strPath);
			initGridModel();
			fillGridModel();
			fillGridModelRecipe(modelSelected);
			
		}

		//ShowWindow(SW_HIDE);
	
	}
}


void CRecipeSetDlg::OnBnClickedBtnRecipeNew()
{	
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"관리자 모드로 실행할수 있습니다",MB_ICONHAND);
	}else{
		//GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(L"");
		//GetDlgItem(IDC_EDIT_TYPE)->SetWindowText(L"");
		//AfxMessageBox(L"새 모델명과 타입을 입력하세요",MB_ICONINFORMATION);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		CString strNewRecipeName;
		GetDlgItem(IDC_ED_COPY_RECIPE_NAME)->GetWindowText(strNewRecipeName);
		//if(strNewRecipeName.GetLength() == 7)
		if (strNewRecipeName.GetLength() > 0)
		{
			//if(strNewRecipeName.GetAt(2) =='_' && strNewRecipeName.GetAt(4) == '_')
			
				//G_WriteInfo(Log_Normal, "New Recipe Add(%s)", strNewRecipeName.GetBuffer(strNewRecipeName.GetLength()));

				//recipeSelected = m_GridModel.GetCell(pItem->iRow, pItem->iColumn)->GetText();
				//GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(recipeSelected);
				CCellID cell = m_GridModel.GetFocusCell();


				if (cell.row >= 1)
				{
					CString strNewCopyPath;
					strNewCopyPath.Format(L"%s%s.ini", RECIPE_FOLDER, strNewRecipeName);
					GetDlgItem(IDC_EDIT_MODEL)->SetWindowText(strNewRecipeName);
					saveRecipe(strNewRecipeName);
					fillGridModel();
					fillGridModelRecipe(strNewRecipeName);

					GetDlgItem(IDC_ED_COPY_RECIPE_NAME)->SetWindowText(L"");
				}
				else
					AfxMessageBox(L"Copy Target Recipe Select Plz....!!!", MB_ICONHAND);
			
			//else
			//	G_WriteInfo(Log_Check,"Recipe Naming conventions Error!!!(%s)", strNewRecipeName.GetBuffer(strNewRecipeName.GetLength()));
		}
		else
			AfxMessageBox(L"Recipe Name Size Error!!!", MB_ICONHAND);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
	}
	
}


void CRecipeSetDlg::OnBnClickedBtnRecipeSave()
{
	CString modelSelected;
	if(G_MainWnd->m_ServerDlg.m_opMode == MODE_OPERATOR)
	{
		AfxMessageBox(L"관리자 모드로 실행할수 있습니다",MB_ICONHAND);
	}else{
		GetDlgItem(IDC_EDIT_MODEL)->GetWindowText(modelSelected);
		if(modelSelected !="")
		{
			saveRecipe(modelSelected);
		}
		fillGridModel();
		fillGridModelRecipe(modelSelected);
	}
}

int CRecipeSetDlg::clearGridModelRecipe(void)
{
	int i,j;
	for(j=0; j<26 ; j++)
	{
		for(i=0; i<11 ; i++)
		{
			if(i ==4)//시험 유무
			{
				CGridCellCheck * pCell = (CGridCellCheck *) m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i]);
				BOOL bTest = pCell->SetCheck(0);
			}
			else
			{
				m_GridModelRecipe.GetCell(ST_RECIPE_ROW[j],ST_RECIPE_COLUMN[i])->SetText(L"");
			}
		}
	}
	return 0;
}

//reset accumulation count
void CRecipeSetDlg::OnBnClickedButton2()
{
	CString str;
	G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU] = 0;

	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_TEST_COUNT_ACCU,str);

	GetDlgItem(IDC_EDIT3)->SetWindowText(str);
	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);
}

//reset production count
void CRecipeSetDlg::OnBnClickedButton3()
{
	CString str;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_TOTAL] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_OK] = 0;
	G_MainWnd->m_DataHandling.m_FlashData.data[COUNT_NG] = 0;

	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[TEST_COUNT_ACCU]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_TOTAL,str); 
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_OK,str); 
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_COUNT_NG,str); 

	GetDlgItem(IDC_EDIT6)->SetWindowText(str);
	//GetDlgItem(IDC_EDIT3)->SetWindowText(str);

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);
}

//m_btnSetLotTarget
void CRecipeSetDlg::OnBnClickedButton5()
{
	CString str;
	GetDlgItem(IDC_EDIT4)->GetWindowText(str);
	G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET] = _wtoi(str);
	str.Format(L"%d",G_MainWnd->m_DataHandling.m_FlashData.data[LOT_TARGET]);
	G_MainWnd->m_DataHandling.m_fnWriteIniFile(SYSTEM_PARAM, SECTION_FLASH, KEY_LOT_TARGET,str); 

	//display
	::PostMessage(G_MainWnd->m_ServerDlg.m_hWnd, UM_UI_UPDATE, (WPARAM)UM_INIT_UPDATE, NULL);

}
//1224_kys

void CRecipeSetDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
	NIPO *pNIPO = NIPO::GetInstance();
	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImagePath), dImg)) 
	{
		//AfxMessageBox(L"Fail to load Image", MB_ICONHAND);
		////////////////////////////////////////
		if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
			{
				AfxMessageBox(L"Failed to create image view", MB_ICONHAND);
			}
			return;
	}
		else {

				//AfxMessageBox(L"Success to load Image", MB_ICONHAND);
			 }

	m_dImg = dImg;
	ShowImage();
}

void CRecipeSetDlg::ShowImage()
{
	m_wndImg.Show(m_dImg);
	ShowImageInfo();
}

void CRecipeSetDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	m_strImageInfo = szText;
	UpdateData(FALSE);
}

int CRecipeSetDlg::ModelImageLoad(CString recipe)
{
	m_strImagePath = MODEL_IMAGE_FOLDER;
	m_strImagePath += recipeSelected;
	m_strImagePath += ".jpg";
	//GetDlgItem(IDC_IMAGE_PATH)->SetWindowText(m_strImagePath);
	LoadImage();
	return 0;
}