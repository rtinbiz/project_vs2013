#pragma once
#include "afxdialogex.h"
#include "Resource.h"
#include "afxwin.h"
#include "stdafx.h"
#include "1.Common\grid64\gridctrl.h"
#include "1.Common/IniClass/BIniFile.h"
#include "PathDefine.h"
#include "pmiMApi.h"
#include "pmiMApiDefs.h"
#include "1.Common\colorstatic\colorstatic2.h"
#include "1.Common\roundbutton\roundbutton2.h"


#define CARD_NO				0	// LCR
#define CARD_NO2			1	// LCR + SHUTTLE
#define CARD_IO				0

#define AXIS_SHUTTLE		2
//
#define USED_NUM_AXIS	11//TOTAL AXIS No
#define NUM_LCR_AXIS		10	//LCR AXIS No
#define NUM_AXIS_GRID_COL			NUM_LCR_AXIS+1 //AXIS NO + TEST NO.
#define NUM_STEP			100

#define		SECTION_AXIS_DATA		_T("AXIS_DATA")
#define		KEY_IN_STEP					_T("NO_IN_STEP")

///////////////////////////////////////////////////////////////
//UM_AXIS

#define UM_AXIS						WM_USER + 200 

#define UW_PROBE_X				0
#define UW_PROBE_Y				1
#define UW_PROBE_Z				2
#define UW_PROBE_T				3
#define UW_PROBE_L				4
#define UW_SHORT_X				5
#define UW_SHORT_Y				6
#define UW_SHORT_Z				7
#define UW_SHORT_T				8
#define UW_SHORT_L				9
#define UW_SHUTTLE				10

#define UW_CHECK_POS			11
#define UW_STEP_MOVE			12

#define UW_READY_TO_HOME			13
#define UW_LCR_HOME_TO_READY		14
#define UW_LCR_READY_HOME_READY	15
#define UW_SHUTTLE_SYS_INIT			16

#define UM_AXIS_CLEAR			0
#define UM_AXIS_ONGOING		1
#define UM_AXIS_OK				2
#define UM_AXIS_NG				4

typedef struct _LCR_AXIS_MSG
{

	int BUSY; 
	int RSP[UW_LCR_HOME_TO_READY + 1];


	_LCR_AXIS_MSG(){	memset(this,0x00, sizeof(LCR_AXIS_MSG));}
}LCR_AXIS_MSG;
//LCR_AXIS_MSG axisMsgRsp;


//#ifdef _FI
////PROBE
//#define LIMIT_PROBE_X_MIN		 -1000
//#define LIMIT_PROBE_X_MAX      52000 
//
//#define LIMIT_PROBE_Y_MIN      -1000
//#define LIMIT_PROBE_Y_MAX      150000   
//
//#define LIMIT_PROBE_Z_MIN      -1000
//#define LIMIT_PROBE_Z_MAX      150000  
//
//#define LIMIT_PROBE_T_MIN      -10000
//#define LIMIT_PROBE_T_MAX      30000   
//
//#define LIMIT_PROBE_L_MIN      -1000
//#define LIMIT_PROBE_L_MAX      200000   
////SHORT
//#define LIMIT_SHORT_X_MIN       -20000
//#define LIMIT_SHORT_X_MAX      54600 
//
//#define LIMIT_SHORT_Y_MIN      -1000
//#define LIMIT_SHORT_Y_MAX      200000   
//
//#define LIMIT_SHORT_Z_MIN      -1000
//#define LIMIT_SHORT_Z_MAX      150000   
//
//#define LIMIT_SHORT_T_MIN      -10000
//#define LIMIT_SHORT_T_MAX      30000   
//
//#define LIMIT_SHORT_L_MIN      -1000
//#define LIMIT_SHORT_L_MAX      150000   
//
//#define LIMIT_SHUTTLE_MIN      -1000
//#define LIMIT_SHUTTLE_MAX      250000
//
//#else
////PROBE
//#define LIMIT_PROBE_X_MIN		 -1000
//#define LIMIT_PROBE_X_MAX      52000 
//
//#define LIMIT_PROBE_Y_MIN      -1000
//#define LIMIT_PROBE_Y_MAX      150000   
//
//#define LIMIT_PROBE_Z_MIN      -1000
//#define LIMIT_PROBE_Z_MAX      150000  
//
//#define LIMIT_PROBE_T_MIN      -10000
//#define LIMIT_PROBE_T_MAX      30000   
//
//#define LIMIT_PROBE_L_MIN      -1000
//#define LIMIT_PROBE_L_MAX      200000   
////SHORT
//#define LIMIT_SHORT_X_MIN       -1000
//#define LIMIT_SHORT_X_MAX      54600 
//
//#define LIMIT_SHORT_Y_MIN      -1000
//#define LIMIT_SHORT_Y_MAX      200000   
//
//#define LIMIT_SHORT_Z_MIN      -1000
//#define LIMIT_SHORT_Z_MAX      150000   
//
//#define LIMIT_SHORT_T_MIN      -10000
//#define LIMIT_SHORT_T_MAX      50000   
//
//#define LIMIT_SHORT_L_MIN      -1000
//#define LIMIT_SHORT_L_MAX      150000   
//
//#define LIMIT_SHUTTLE_MIN      -1000
//#define LIMIT_SHUTTLE_MAX      250000
//
//
//#endif

#ifdef _FI
//PROBE
#define LIMIT_PROBE_X_MIN		 -1000000
#define LIMIT_PROBE_X_MAX      52000 

#define LIMIT_PROBE_Y_MIN      -1000000
#define LIMIT_PROBE_Y_MAX      150000   

#define LIMIT_PROBE_Z_MIN      -1000000
#define LIMIT_PROBE_Z_MAX      150000  

#define LIMIT_PROBE_T_MIN      -1000000
#define LIMIT_PROBE_T_MAX      30000   

#define LIMIT_PROBE_L_MIN      -1000000
#define LIMIT_PROBE_L_MAX      200000   
//SHORT
#define LIMIT_SHORT_X_MIN       -2000000
#define LIMIT_SHORT_X_MAX      54600 

#define LIMIT_SHORT_Y_MIN      -1000000
#define LIMIT_SHORT_Y_MAX      200000   

#define LIMIT_SHORT_Z_MIN      -1000000
#define LIMIT_SHORT_Z_MAX      150000   

#define LIMIT_SHORT_T_MIN      -1000000
#define LIMIT_SHORT_T_MAX      30000   

#define LIMIT_SHORT_L_MIN      -1000000
#define LIMIT_SHORT_L_MAX      150000   

#define LIMIT_SHUTTLE_MIN      -1000000
#define LIMIT_SHUTTLE_MAX      250000

#else
//PROBE
#define LIMIT_PROBE_X_MIN		 -1000000
#define LIMIT_PROBE_X_MAX      52000 

#define LIMIT_PROBE_Y_MIN      -1000000
#define LIMIT_PROBE_Y_MAX      150000   

#define LIMIT_PROBE_Z_MIN      -1000000
#define LIMIT_PROBE_Z_MAX      150000  

#define LIMIT_PROBE_T_MIN      -1000000
#define LIMIT_PROBE_T_MAX      30000   

#define LIMIT_PROBE_L_MIN      -1000000
#define LIMIT_PROBE_L_MAX      200000   
//SHORT
#define LIMIT_SHORT_X_MIN       -1000000
#define LIMIT_SHORT_X_MAX      54600 

#define LIMIT_SHORT_Y_MIN      -1000000
#define LIMIT_SHORT_Y_MAX      200000   

#define LIMIT_SHORT_Z_MIN      -1000000
#define LIMIT_SHORT_Z_MAX      150000   

#define LIMIT_SHORT_T_MIN      -1000000
#define LIMIT_SHORT_T_MAX      50000   

#define LIMIT_SHORT_L_MIN      -1000000
#define LIMIT_SHORT_L_MAX      150000   

#define LIMIT_SHUTTLE_MIN      -1000000
#define LIMIT_SHUTTLE_MAX      250000


#endif

// m_cbActionMode.AddString(_T("0 : Not Stop"));
// m_cbActionMode.AddString(_T("1 : Immediately Stop"));

#define SOFT_LIMIT_N_STOP				0x00 //인터럽트 신호 만 발생
#define SOFT_LIMIT_E_STOP				0x01 //즉시 정지
#define SOFT_LIMIT_S_STOP				0x02 //감속 후 정지


#define  	COL_AXIS0						_T(" PROBE X ")
#define		COL_AXIS1						_T(" PROBE Y ")
#define		COL_AXIS2						_T(" PROBE Z ")
#define		COL_AXIS3						_T(" PROBE T ")
#define		COL_AXIS4						_T(" PROBE L ")
#define		COL_AXIS5						_T(" SHORT X ")
#define		COL_AXIS6						_T(" SHORT Y ")
#define		COL_AXIS7						_T(" SHORT Z ")
#define		COL_AXIS8						_T(" SHORT T ")
#define		COL_AXIS9						_T(" SHORT L ")
#define		COL_TEST_NO					_T("TEST NO.")
#define  	COL_MOVE						_T("  MOVE  ")

const CString ST_LCR_GRID_COL[] = 
{
	COL_AXIS0,
	COL_AXIS1,
	COL_AXIS2,
	COL_AXIS3,
	COL_AXIS4,
	COL_AXIS5,
	COL_AXIS6,
	COL_AXIS7,
	COL_AXIS8,
	COL_AXIS9,
	COL_TEST_NO,
	COL_MOVE,

};

const CString ST_LCR_GRID_IN_ROW[] = 
{
_T("  IN_STEP0  "),
_T("  IN_STEP1  "),
_T("  IN_STEP2  "),
_T("  IN_STEP3  "),
_T("  IN_STEP4  "),
_T("  IN_STEP5  "),
_T("  IN_STEP6  "),
_T("  IN_STEP7  "),
_T("  IN_STEP8  "),
_T("  IN_STEP9  "),

_T("  IN_STEP10  "),
_T("  IN_STEP11  "),
_T("  IN_STEP12  "),
_T("  IN_STEP13  "),
_T("  IN_STEP14  "),
_T("  IN_STEP15  "),
_T("  IN_STEP16  "),
_T("  IN_STEP17  "),
_T("  IN_STEP18  "),
_T("  IN_STEP19  "),

_T("  IN_STEP20  "),
_T("  IN_STEP21  "),
_T("  IN_STEP22  "),
_T("  IN_STEP23  "),
_T("  IN_STEP24  "),
_T("  IN_STEP25  "),
_T("  IN_STEP26  "),
_T("  IN_STEP27  "),
_T("  IN_STEP28  "),
_T("  IN_STEP29  "),

_T("  IN_STEP30  "),
_T("  IN_STEP31  "),
_T("  IN_STEP32  "),
_T("  IN_STEP33  "),
_T("  IN_STEP34  "),
_T("  IN_STEP35  "),
_T("  IN_STEP36  "),
_T("  IN_STEP37  "),
_T("  IN_STEP38  "),
_T("  IN_STEP39  "),

_T("  IN_STEP40  "),
_T("  IN_STEP41  "),
_T("  IN_STEP42  "),
_T("  IN_STEP43  "),
_T("  IN_STEP44  "),
_T("  IN_STEP45  "),
_T("  IN_STEP46  "),
_T("  IN_STEP47  "),
_T("  IN_STEP48  "),
_T("  IN_STEP49  "),

_T("  IN_STEP50  "),
_T("  IN_STEP51  "),
_T("  IN_STEP52  "),
_T("  IN_STEP53  "),
_T("  IN_STEP54  "),
_T("  IN_STEP55  "),
_T("  IN_STEP56  "),
_T("  IN_STEP57  "),
_T("  IN_STEP58  "),
_T("  IN_STEP59  "),

_T("  IN_STEP60  "),
_T("  IN_STEP61  "),
_T("  IN_STEP62  "),
_T("  IN_STEP63  "),
_T("  IN_STEP64  "),
_T("  IN_STEP65  "),
_T("  IN_STEP66  "),
_T("  IN_STEP67  "),
_T("  IN_STEP68  "),
_T("  IN_STEP69  "),

_T("  IN_STEP70  "),
_T("  IN_STEP71  "),
_T("  IN_STEP72  "),
_T("  IN_STEP73  "),
_T("  IN_STEP74  "),
_T("  IN_STEP75  "),
_T("  IN_STEP76  "),
_T("  IN_STEP77  "),
_T("  IN_STEP78  "),
_T("  IN_STEP79  "),

_T("  IN_STEP80  "),
_T("  IN_STEP81  "),
_T("  IN_STEP82  "),
_T("  IN_STEP83  "),
_T("  IN_STEP84  "),
_T("  IN_STEP85  "),
_T("  IN_STEP86  "),
_T("  IN_STEP87  "),
_T("  IN_STEP88  "),
_T("  IN_STEP89  "),

_T("  IN_STEP90  "),
_T("  IN_STEP91  "),
_T("  IN_STEP92  "),
_T("  IN_STEP93  "),
_T("  IN_STEP94  "),
_T("  IN_STEP95  "),
_T("  IN_STEP96  "),
_T("  IN_STEP97  "),
_T("  IN_STEP98  "),
_T("  IN_STEP99  "),

};

enum
{
	Axis_Probe_X=0,
	Axis_Probe_Y,
	Axis_Probe_Z,
	Axis_Probe_T,
	Axis_Probe_L,
	Axis_Short_X,
	Axis_Short_Y,
	Axis_Short_Z,
	Axis_Short_T,
	Axis_Short_L,
	Axis_Cnt
};

typedef struct _LCR_AXIS_POSITION
{
	wchar_t part_model[256];
	wchar_t part_type[256]; //
	int steps; //total actual step number

	double absPos[NUM_STEP][NUM_AXIS_GRID_COL];

	_LCR_AXIS_POSITION(){	memset(this,0x00, sizeof(LCR_AXIS_POSITION));}
}LCR_AXIS_POSITION;

class AxisMotion : 	public CDialogEx
{
	DECLARE_DYNAMIC(AxisMotion)

public:
	
	BOOL motorBusy;
	int axisCard; //
	double m_dAxisCmdPos[12];
	double m_dAxisActPos[12];
	double m_dAxisCmdVel[12];
	CCriticalSection m_csAxisPos;

	LCR_AXIS_POSITION axisData;
	LCR_AXIS_MSG axisMsgRsp;

	int m_nMotEvtCnt;
	void RefreshParameter();
	void SetHomeMoveSpeed(int iAxisNo);
	void SetPosMoveSpeed(int iAxisNo);
	void SetJogMoveSpeed(int iAxisNo);
	void SetJogMoveSpeed2(int iAxisNo);
	int m_nBoardType;
	void InitDlgItem();
	int m_iDoNum, m_iDoNum2;
	int m_iDiNum, m_iDiNum2;
	int m_iAxisNum;
	int m_iAxisNum2;
	void PosAndVel_UIUpDate();
	void PosAndVel();
	void CardStatusDisplay();
	BOOL ReturnMessage(int iRet);
	CBitmap m_BitMapLedG;
	CBitmap m_BitMapLedR;
	CBitmap m_BitMapLedW;

	CEdit	m_EdtVelocity[8];
	CEdit	m_EdtActPos[8];
	CEdit	m_EdtCmdPos[8];
	CStatic	m_BtmRstStatus;
	CStatic	m_BtmSonStatus;
	CStatic	m_BtmCclrStatus;
	CStatic	m_BtmNLmtStatus;
	CStatic	m_BtmPLmtStatus;
	CStatic	m_BtmAlmStatus;
	CStatic	m_BtmInpStatus;
	CStatic	m_BtmEmgStatus;
	CStatic	m_BtmEzStatus;
	CStatic	m_BtmOrgStatus;
	CComboBox	m_CbAxisSelect;
	CComboBox	m_CbHomeMode;
	CComboBox	m_CbSelectEncoderMode;
	CComboBox	m_CbSelectPulseMode;
	//CComboBox	m_CbSelectRange;
	CComboBox m_CbSpeedMode;

	AxisMotion(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~AxisMotion();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_AXIS2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	void m_fnDeInit(void);

	HICON m_hIcon;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	// 생성된 메시지 맵 함수
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnChkOrgLogic();
	afx_msg void OnChkEzLogic();
	afx_msg void OnChkEmgLogic();
	afx_msg void OnChkInpLogic();
	afx_msg void OnChkAlmLogic();
	afx_msg void OnChkLmtLogic();
	afx_msg void OnChkInpEnable();
	afx_msg void OnClose();
	afx_msg void OnSelchangeCbRange();
	afx_msg void OnChkEncoderDir();
	afx_msg void OnChkHomeDir();
	afx_msg void OnBtnEstop();
	afx_msg void OnBtnSstop();
	//afx_msg void OnBtnPjog();
	//afx_msg void OnBtnNjog();
	afx_msg void OnBtnIncmove();
	afx_msg void OnBtnAbsmove();
	afx_msg void OnBtnHomemove();
	afx_msg void OnSelchangeCbPulseMode();
	afx_msg void OnSelchangeCbEncoderMode();
	afx_msg void OnSelchangeCbHomeMode();
	afx_msg void OnSelchangeCbAxisSelect();
	afx_msg void OnBtnMultiIncmove();
	afx_msg void OnBtnMultiAbsmove();
	afx_msg void OnBtnClearPos();
	afx_msg void OnBtnMultiEstop();
	afx_msg void OnBtnMultiSstop();
	afx_msg void OnBtnOverrideSpeed();
	afx_msg void OnBtnOverrideInc();
	afx_msg void OnBtnOverrideAbs();
	afx_msg void OnBtnCclr();
	afx_msg void OnBtnSvonOnoff();
	afx_msg void OnBtnRstOnoff();
	afx_msg void OnCbnSelchangeCbSpeedMode();
	//afx_msg void OnBtnDioDlg();	
	//afx_msg void OnBnClickedBtnCmpDlg();
	//afx_msg void OnBnClickedBtnInterpolationDlg();
	//afx_msg void OnBnClickedBtnInterruptDlg();
	//afx_msg void OnBnClickedBtnMasterSlaveDlg();
	//afx_msg LRESULT OnReleaseDlg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);

	void SetUMMsgResponse(int msg, int rsp);
	int GetUMMsgResponse(int msg);

	CBIniFile		m_iniLCRAxisData;
	int InStep, InAxis;
	CGridCtrl m_GridLCRAxis;
	int initGridLCRIN(void);
	afx_msg void OnBnClickedBtnInSet();
	int saveLCRAxisData(CString recipe);
	afx_msg void OnBnClickedBtnLcrAxisSave();
	int fillGridLCRAxis(CString recipe);
	CString m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default);
	int applyAxisData(CString recipe);
	int homeLCRAxis(void);
	int homeShuttle(void);
	CEdit m_EdtCmdPos8;
	CEdit m_EdtActPos8;
	CEdit m_EdtVelocity8;
	CColorStatic2 m_staticShuttleForward;
	CColorStatic2 m_staticShuttleReturn;
	afx_msg void OnBnClickedBtnShuttleForwardPos();
	afx_msg void OnBnClickedBtnShuttleReturnPos();

	int stepMoveLCR(int step);
	int stepMoveCheckLCR();
	CEdit m_EdtCmdPos2[8];
	CEdit m_EdtActPos2[8];
	CEdit m_EdtVelocity2[8];
	afx_msg void OnBnClickedBtnAxisReadySet();
	int checkLCRAxisReadyPosition(void);
	int checkLCRAxisPosition(int step);
	int checkLCRAxisHomePosition(void);
	CString m_fnGetAxisName(int iAxis);
	BOOL m_fnSetPosMoveSpeed(int iAxisNo);
	BOOL m_fnAbsmove(int iAxisNo, double nPos);
	BOOL m_fnSetHomeMoveSpeed(int iAxisNo);
	BOOL m_fnHomemove(int iAxisNo);
	BOOL m_fnSoftLimitEnable(BOOL bUse, int iAxisNo, BOOL bStopMode = 0);
	int m_fnAxCheckDone(int iAxisNo, int * nDone, int nWait);
	int m_fnAxHomeCheckDone(int iAxisNo, int * nDone, int nWait);
	BOOL m_fnClearPos(int iAxisNo);
	int m_fnAxisHome(int iAxis);
	int m_fnAxisHome2(int iAxis);
	int initAxis(int iAxisNo);
	
	afx_msg void OnBnClickedBtnShuttleFwd();
	afx_msg void OnBnClickedBtnShuttleRet();
	afx_msg void OnBnClickedBtnShuttleHome();

	CRoundButton2 m_btnShuttleFwd;
	CRoundButton2 m_btnShuttleRev;

	//Auto Home, Ready Position
	int m_fnLCRAxisReadyToHome(void);
	int m_fnLCRAxisHomeToReady(void);
	int m_fnLCRAxisReadyToHome2(void);
	int m_fnLCRAxisHomeToReady2(void);
	int m_fnLCRAxisReadyToHomePI(void);
	int m_fnLCRAxisHomeToReadyPI(void);

	afx_msg void OnBnClickedBtnLcrAxisGoToReadyPosition(UINT nID);
	afx_msg void OnBnClickedBtnLcrAxisGoToHome(UINT nID);
	afx_msg void OnBnClickedBtnLcrAxisAlarmReset(UINT nID);

	afx_msg void OnBnClickedBtnShuttleAlarmReset();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();

	CRoundButton2 m_btnLCRAxisAlarmReset[10];
	CRoundButton2 m_btnLCRAxisGoToReady[10];
	CRoundButton2 m_btnLCRAxisHome[10];
	CRoundButton2 m_btnShuttleAlarmReset;
	CRoundButton2 m_btnShuttleHome;
	CRoundButton2 m_btnShuttleSetFWD;
	CRoundButton2 m_btnShuttleSetRET;

	int m_fnDisplayShuttleStatus(void);
	int m_fnDisplayLCRAxisStatus(void);
	CRoundButton2 m_btnLCRAxisEStop;
	CRoundButton2 m_btnShuttleEStop;
	CRoundButton2 m_btnLCRAxisReadySet;

	int sysInitLCRAxisPosCheck(void);
	CRoundButton2 m_btnLCRAxisJOGP[10];
	CRoundButton2 m_btnLCRAxisJOGN[10];
	CRoundButton2 m_btnMotorBusy;
	afx_msg void OnBnClickedBtnAxisBusyClear();

	afx_msg void OnBnClickedBtnAxisMsgBusyClear();
	CRoundButton2 m_btnLCRPosSet;
	CRoundButton2 m_btnLCRPosSave;
	CRoundButton2 m_btn_AutoAxisBusy;
	afx_msg void OnBnClickedBtnLcrReady();
	afx_msg void OnBnClickedBtnLcrHome();
	CRoundButton2 m_btnLCRHomeAll;
	CRoundButton2 m_btnLCRReadyAll;
	int sysInitShuttle(void);
	afx_msg void OnBnClickedBtnShuttleInit2();
	CRoundButton2 m_btnShuttleInit;
	//AXIS HOME ENABLE LAMP ADD _160111_KYS
	CRoundButton2 m_btnShortHomeEnable;
	CRoundButton2 m_btnProbeHomeEnable;
	int m_fnLCRAxisStatusBitCheck(void);
};
/*
///////////////////////////////////////////////////
************ READY TO HOME *******************
Condition:
((PROBE && SHORT) HOME ENABLE SENSOR DETECT CHECK) ||
(READY POSITION CHECK) && m_fnLCRAxisStatusBitCheck

m_fnLCRAxisStatusBitCheck:
if (PROBE && SHORT) HOME ENABLE SENSOR DETECT CHECK,
H_OK Signal SKIP.

<FI>
PROBE/SHORT Z HOME
PROBE/SHORT T,L HOME
PROBE/SHORT X,Y HOME

<PI>
PROBE Z HOME
PROBE X+, Y- JOG MOVE TO LIMIT
PROBE T,L HOME
PROBE X,Y HOME

SHORT Z HOME
SHORT X-, Y- JOG MOVE TO LIMIT
SHORT T,L HOME
SHORT X,Y HOME

************ HOME TO READY *******************
Condition:
HOME POSITION CHECK && m_fnLCRAxisStatusBit CHECK

<FI>
ALL AXIS TOGETHER READY POS. MOVE

<PI>
SHORT TOGETHER READY POS. MOVE

PROBE X ABS POS: 40000 MOVE
PROBE Y ABS POS: 40000 MOVE
PROBE Z,T,L READY POS. MOVE
PROBE X,Y   READY POS. MOVE
///////////////////////////////////////////////////
*/
