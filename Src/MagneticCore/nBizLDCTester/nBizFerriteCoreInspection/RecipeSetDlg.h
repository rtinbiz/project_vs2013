#pragma once
#include "Resource.h"
#include "1.Common\Grid64\GridCtrl.h"
#include "1.Common\Grid64\NewCellTypes\gridcellcheck.h"
#include "afxwin.h"
#include "1.Common/IniClass/BIniFile.h"
#include "PathDefine.h"
#include "1.Common/LogMng/LogMng.h"
#include "1.Common\roundbutton\roundbutton2.h"

#define MODEL_IMAGE_FOLDER L"D:\\nBizSystem\\Recipe\\MODEL_IMAGE\\"
#define MAX_L_TEST			14
#define MAX_OSC_TEST		1
#define MAX_DCR_TEST		6
#define MAX_HIPOT_TEST		5
#define MAX_IR_TEST			5
#define MAX_VI_TEST			1
#define MAX_TOTAL_TEST		MAX_L_TEST+MAX_OSC_TEST+MAX_DCR_TEST+MAX_HIPOT_TEST+MAX_IR_TEST+MAX_VI_TEST //31
/////////////////////////////////////////////////////////////////////////////////////////////////////
//Recipe data
#define  SECTION_RECIPE_DATA						_T("RECIPE_DATA")
//SECTION
#define  SECTION_L1						_T("L1")
#define  SECTION_L2						_T("L2")
#define  SECTION_L3						_T("L3")
#define  SECTION_L4						_T("L4")
#define  SECTION_L5						_T("L5")
#define  SECTION_L6						_T("L6")
#define  SECTION_L7						_T("L7")
#define  SECTION_L8						_T("L8")
#define  SECTION_L9						_T("L9")
#define  SECTION_L10						_T("L10")
#define  SECTION_L11						_T("L11")
#define  SECTION_L12						_T("L12")
#define  SECTION_L13						_T("L13")
#define  SECTION_L14						_T("L14")

#define  SECTION_OSC						_T("OSC")

#define  SECTION_R1						_T("R1")
#define  SECTION_R2						_T("R2")
#define  SECTION_R3						_T("R3")
#define  SECTION_R4						_T("R4")
#define  SECTION_R5						_T("R5")
#define  SECTION_R6						_T("R6")

#define  SECTION_P1						_T("P1")
#define  SECTION_P2						_T("P2")
#define  SECTION_P3						_T("P3")
#define  SECTION_P4						_T("P4")
#define  SECTION_P5						_T("P5")

#define  SECTION_I1						_T("I1")
#define  SECTION_I2						_T("I2")
#define  SECTION_I3						_T("I3")
#define  SECTION_I4						_T("I4")
#define  SECTION_I5						_T("I5")

#define  SECTION_VI						_T("VI")

#define  SECTION_LCR						_T("LCR CORRECTION")

//28
const CString ST_RECIPE_TEST_ITEM[] = 
{
  SECTION_L1,
  SECTION_L2,
  SECTION_L3,
  SECTION_L4,
  SECTION_L5,
  SECTION_L6,
  SECTION_L7,
  SECTION_L8,
  SECTION_L9,
  SECTION_L10,
  SECTION_L11,
  SECTION_L12,
  SECTION_L13,
  SECTION_L14,

  SECTION_OSC,

  SECTION_R1,
  SECTION_R2,
  SECTION_R3,
  SECTION_R4,
  SECTION_R5,
  SECTION_R6,

  SECTION_P1,
  SECTION_P2,
  SECTION_P3,
  SECTION_P4,
  SECTION_P5,

  SECTION_I1,
  SECTION_I2,
  SECTION_I3,
  SECTION_I4,
  SECTION_I5,

  SECTION_VI,

  SECTION_LCR
};


//KEY
#define		KEY_MODEL						_T("MODEL")
#define		KEY_TYPE						_T("TYPE")
#define		KEY_LOTNO						_T("LOTNO")

#define		KEY_TERM1						_T("TEST TERMINAL 1")
#define		KEY_TERM2						_T("TEST TERMINAL 2")
#define		KEY_TERMS1					_T("SHORT TERMINAL 1")
#define		KEY_TERMS2					_T("SHORT TERMINAL 2")
#define		KEY_TEST						_T("TEST")
#define  	KEY_KHZ							_T("KHZ")
#define  	KEY_VAC						_T("VAC")
#define  	KEY_VDC						_T("VDC")
#define  	KEY_SEC							_T("SEC")
#define  	KEY_BIASAMP					_T("BIASAMP")
#define  	KEY_TURNRATIO				_T("TURNRATIO")
#define  	KEY_ULIMIT						_T("ULIMIT")
#define  	KEY_LLIMIT						_T("LLIMIT")
#define  	KEY_CORRECT					_T("CORRECT VALUE")
#define  	KEY_OFFSET					_T("OFFSET")
#define  	KEY_TEST_ITEM				_T("TEST ITEM")

#define  	NO_RECIPE_ITEM					16

const CString ST_RECIPE_UNIT_TEST[] = 
{
	KEY_TERM1,
	KEY_TERM2,
	KEY_TERMS1,
	KEY_TERMS2,
	KEY_TEST,
	KEY_KHZ,
	KEY_VAC,
	KEY_VDC,		//BIAS
	KEY_SEC,
	KEY_BIASAMP,
	KEY_TURNRATIO, //10
	KEY_LLIMIT,
	KEY_ULIMIT,
	KEY_CORRECT,
	KEY_OFFSET,
	KEY_TEST_ITEM
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
//GRID ROW 
#define  ROW_L1							1
#define  ROW_L2							2
#define  ROW_L3							3
#define  ROW_L4							4
#define  ROW_L5							5
#define  ROW_L6							6
#define  ROW_L7							7
#define  ROW_L8							8
#define  ROW_L9							9
#define  ROW_L10						10
#define  ROW_L11						11
#define  ROW_L12						12
#define  ROW_L13						13
#define  ROW_L14						14

#define  ROW_OSC						15

#define  ROW_R1							16
#define  ROW_R2							17
#define  ROW_R3							18
#define  ROW_R4							19
#define  ROW_R5							20
#define  ROW_R6							21

#define  ROW_P1							22
#define  ROW_P2							23
#define  ROW_P3							24
#define  ROW_P4							25
#define  ROW_P5							26

#define  ROW_I1							27
#define  ROW_I2							28
#define  ROW_I3							29
#define  ROW_I4							30
#define  ROW_I5							31

#define  ROW_VI							32

const int ST_RECIPE_ROW[] = 
{
  ROW_L1,
  ROW_L2,
  ROW_L3,
  ROW_L4,
  ROW_L5,
  ROW_L6,
  ROW_L7,
  ROW_L8,
  ROW_L9,
  ROW_L10,
  ROW_L11,
  ROW_L12,
  ROW_L13,
  ROW_L14,

  ROW_OSC,

  ROW_R1,
  ROW_R2,
  ROW_R3,
  ROW_R4,
  ROW_R5,
  ROW_R6,

  ROW_P1,
  ROW_P2,
  ROW_P3,
  ROW_P4,
  ROW_P5,

  ROW_I1,
  ROW_I2,
  ROW_I3,
  ROW_I4,
  ROW_I5,

  ROW_VI

};

//GRID COLUMN
#define		COL_TERM1							1
#define		COL_TERM2							2
#define		COL_TERMS1						3
#define		COL_TERMS2						4
#define		COL_TEST							5
#define  	COL_KHZ							6
#define  	COL_VAC							7
#define  	COL_VDC							8 	//BIAS
#define  	COL_SEC							9
#define  	COL_DCAMP						10
#define  	COL_TURNRATIO					11
#define  	COL_ULIMIT						12
#define  	COL_LLIMIT							13
#define  	COL_CORRECT						14
#define  	COL_OFFSET						15
#define  	COL_TEST_ITEM					16

const int ST_RECIPE_COLUMN[] = 
{
		COL_TERM1,		
		COL_TERM2,		
		COL_TERMS1,	
		COL_TERMS2,	
		COL_TEST,		
  		COL_KHZ,		
  		COL_VAC,	
  		COL_VDC, 
  		COL_SEC,
		COL_DCAMP,
		COL_TURNRATIO,
  		COL_ULIMIT,	
  		COL_LLIMIT,
		COL_CORRECT,
		COL_OFFSET,
		COL_TEST_ITEM
};

//////////////////////////////////////////////////////////////////////////////////////////////////
//LCR TEST Condition
typedef struct _LCR_TEST_INFO
{
	int no; // test no.

	//시험 단자
	int ch1term1;
	int ch1term2;
	int ch2term1; //short terminal
	int ch2term2; //short terminal

	int bTest; //test or not (1/0)

	double kHz;
	double Vac;
	double Vdc;
	double sec;
	double Ibias;
	int shortPin;

	//range limit
	double upperLimit;
	double lowerLimit;

	double correctValue;
	double offset;

	double refPrimary;
	double refSecondary;

	_LCR_TEST_INFO(){	memset(this,0x00, sizeof(LCR_TEST_INFO));}
} LCR_TEST_INFO;

//OSC TEST Condition
typedef struct _OSC_TEST_INFO
{
	int no; // test no.

	//시험 단자
	int ch1term1;
	int ch1term2;
	int ch2term1;
	int ch2term2;

	int bTest; //test or not (1/0)

	double kHz;
	double Vac;
	double Vdc;
	double sec;
	double Ibias;
	double turnRatio;

	//range limit
	double upperLimit;
	double lowerLimit;

	double correctValue;
	double offset;

	_OSC_TEST_INFO(){	memset(this,0x00, sizeof(OSC_TEST_INFO));}
}OSC_TEST_INFO;

//DCR TEST Condition
typedef struct _DCR_TEST_INFO
{
	int no; // test no.

	//시험 단자
	int ch1term1;
	int ch1term2;
	int ch2term1;
	int ch2term2;

	int bTest; //test or not (1/0)

	double kHz;
	double Vac;
	double Vdc;
	double sec;
	double Ibias;

	//range limit
	double upperLimit;
	double lowerLimit;
	
	double correctValue;
	double offset;

	_DCR_TEST_INFO(){	memset(this,0x00, sizeof(DCR_TEST_INFO));}
}DCR_TEST_INFO;

//HIPOT TEST Condition
typedef struct _HIPOT_TEST_INFO
{
	int no; // test no.

	//시험 단자
	int ch1term1;
	int ch1term2;
	int ch2term1;
	int ch2term2;

	int bTest; //test or not (1/0)

	double kHz;
	double Vac;
	double Vdc;
	double sec;
	double Ibias;

	//range limit
	double upperLimit;
	double lowerLimit;

	double correctValue;
	double offset;

	_HIPOT_TEST_INFO(){	memset(this,0x00, sizeof(HIPOT_TEST_INFO));}
}HIPOT_TEST_INFO;

//IR TEST Condition
typedef struct _IR_TEST_INFO
{
	int no; // test no.

	//시험 단자
	int ch1term1;
	int ch1term2;
	int ch2term1;
	int ch2term2;

	int bTest; //test or not (1/0)

	double kHz;
	double Vac;
	double Vdc;
	double sec;
	double Ibias;

	//range limit
	double upperLimit;
	double lowerLimit;

	double correctValue;
	double offset;

	_IR_TEST_INFO(){	memset(this,0x00, sizeof(IR_TEST_INFO));}
}IR_TEST_INFO;


//IR TEST Condition
typedef struct _VI_TEST_INFO
{
	int no; // test no.

	int bTest; //test or not (1/0)

	_VI_TEST_INFO(){	memset(this,0x00, sizeof(VI_TEST_INFO));}
}VI_TEST_INFO;

typedef struct _PART_TEST_INFO
{
	wchar_t part_model[256];
	wchar_t part_type[256];
	wchar_t lot[256];

	wchar_t QRcode[256]; //part info

	LCR_TEST_INFO lcrTestInfo[MAX_L_TEST];
	OSC_TEST_INFO oscTestInfo[MAX_OSC_TEST];
	DCR_TEST_INFO dcrTestInfo[MAX_DCR_TEST];
	HIPOT_TEST_INFO hipTestInfo[MAX_HIPOT_TEST];
	IR_TEST_INFO irTestInfo[MAX_IR_TEST];
	VI_TEST_INFO viTestInfo[MAX_VI_TEST];

	_PART_TEST_INFO(){	memset(this,0x00, sizeof(PART_TEST_INFO));}

}PART_TEST_INFO;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

class CRecipeSetDlg : public CDialogEx
{

	DECLARE_DYNAMIC(CRecipeSetDlg)

public:
	CRecipeSetDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CRecipeSetDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_RECIPE_SETTING };



	PART_TEST_INFO part_Test;

//////////////////////////////////////////////////////////////////////////////////////////////////

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg void OnGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridDblClick2(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnGridClick2(NMHDR *pNotifyStruct, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
	Mat m_dImg;
	NIPLImageCtl m_wndImg;

public:
	CString recipeSelected;
	virtual BOOL OnInitDialog();
	void m_fnInit(void);
	int initGridModel(void);
	int initGridModelRecipe(void);
	CGridCtrl m_GridModel;
	CGridCtrl m_GridModelRecipe;
	int fillGridModel(void);
	afx_msg void OnBnClickedBtnApply();
	int fillGridModelRecipe(CString recipe);
	int applyRecipeInProduction(CString recipe);

	CBIniFile		m_iniRecipe;

	int saveRecipe(CString recipe);
	BOOL m_fnWriteIniFile(LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR lpString);
	CString m_fnReadIniFile( LPCTSTR lpszSection, LPCTSTR lpszKeyName, LPCTSTR default); 
	afx_msg void OnBnClickedBtnRecipeDelete();
	afx_msg void OnBnClickedBtnRecipeNew();
	afx_msg void OnBnClickedBtnRecipeSave();
	int clearGridModelRecipe(void);
	afx_msg void OnBnClickedButton2();
	CRoundButton2 m_btnRecipeDelete;
	CRoundButton2 m_btnRecipeNew;
	CRoundButton2 m_btnRecipeSave;
	CRoundButton2 m_btnRecipeApply;
	CRoundButton2 m_btnResetAccu;
	CRoundButton2 m_btnResetProductionCount;
	afx_msg void OnBnClickedButton3();
	CRoundButton2 m_btnSetLotTarget;
	afx_msg void OnBnClickedButton5();

	CStatic m_ctlImageArea;
	void LoadImage();
	void ShowImage();
	void ShowImageInfo();
	CString m_strImagePath;
	CString m_strImageInfo;
	//afx_msg void OnBnClickedBtnImgView();
	int ModelImageLoad(CString recipe);
};
//160104_kys
const CString PrefixedModelName[] =
{
	_T("AE_EV_OBC_TR"),
	_T("AE_EV_OBC_PFC_IN"),
	_T("AE_EV_OBC_IN"),
	_T("JF_AE_DE_PHEV_OBC_TR"),
	_T("JF_AE_DE_PHEV_OBC_TR_1"),
	_T("PFC_INDUCTOR"),
	_T("JF_AE_DE_PHEV_OBC_PFC_IN"),
	_T("6.6kW_OBC_LLC"),
	_T("6.6kW_OBC_PFC_IN"),
	_T("COMP_FILTER"),
	_T("PS_EV_OBC"),
};

