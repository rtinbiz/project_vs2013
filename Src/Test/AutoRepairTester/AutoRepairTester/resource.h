//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// AutoRepairTester.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_AUTOREPAIRTESTER_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDC_IMAGE_AREA                  1000
#define IDC_MASK_AREA                   1001
#define IDC_IMAGE_FILE_PATH             1003
#define IDC_LOAD_IMAGE                  1004
#define IDC_LOG_LIST                    1005
#define IDC_IMAGE_POS                   1006
#define IDC_IMAGE_INFO                  1007
#define IDC_MODEL_LIST                  1008
#define IDC_MASK_FOLDER_PATH            1008
#define IDC_LOAD_MASK                   1009
#define IDC_MASK_TEMPLATE               1010
#define IDC_TYPE_ACTIVE                 1010
#define IDC_MASK_LAYER_1                1011
#define IDC_TYPE_GATE                   1011
#define IDC_MASK_LAYER_2                1012
#define IDC_TYPE_SD                     1012
#define IDC_MASK_LAYER_3                1013
#define IDC_DEFECT_START_POS_X          1023
#define IDC_DEFECT_START_POS_Y          1024
#define IDC_TEST_BINARIZE_THRESHOLD     1025
#define IDC_TEST_DEFECT_MIN_SIZE        1026
#define IDC_TEST_DIFF_THRESHOLD         1027
#define IDC_TEST_HOLE_MIN_SIZE          1027
#define IDC_DEFECT_END_POS_X            1028
#define IDC_TEST_SEED_MIN_SIZE          1028
#define IDC_DEFECT_END_POS_Y            1029
#define IDC_TEST                        1043
#define IDC_CLASSIFY                    1043
#define IDC_TEST_RESULT                 1044
#define IDC_DRAW                        1045
#define IDC_APPLY_ROI                   1047
#define IDC_FIND_DEFECT                 1071
#define IDC_IMAGE_LIST                  1073
#define IDC_MASK_COMBO                  1074
#define IDC_IMAGE_RELOAD                1075
#define IDC_FOCUS_START_POS             1076
#define IDC_EDIT_COLOR_TH_R             1077
#define IDC_FOCUS_END_POS               1078
#define IDC_EDIT_BEST_FIT_IMAGE_SCALE   1078
#define IDC_EDIT_COLOR_TH_G             1079
#define IDC_EDIT_COLOR_TH_B             1080
#define IDC_EDIT_COLOR_TH_R2            1081
#define IDC_EDIT_HV_RATE                1081
#define IDC_DATA_FOLDER_COMBO           1083
#define IDC_FIND_BEST_FIT_IMAGE         1085
#define IDC_SAVE_BEST_FIT_IMAGE         1086
#define IDC_SCALE_BEST_FIT_IMAGE        1087
#define IDC_RADIO_IMAGE_ROTATION_0      1089
#define IDC_RADIO_IMAGE_ROTATION_90     1090
#define IDC_RADIO_IMAGE_ROTATION_180    1091
#define IDC_RADIO_IMAGE_ROTATION_270    1092
#define IDC_CHECK_IMAGE_FLIP_HORIZ      1093
#define IDC_CHECK_IMAGE_FLIP_VERT       1094
#define IDC_IMAGE_CTRL                  2001
#define IDC_MASK_CTRL                   2002

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1095
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
