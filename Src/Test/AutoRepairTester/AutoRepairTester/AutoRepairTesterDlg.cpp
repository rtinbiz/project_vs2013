
// AutoRepairTesterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AutoRepairTester.h"
#include "AutoRepairTesterDlg.h"
#include "afxdialogex.h"

#include <array>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define CALC_DISTANCE(pt1, pt2) sqrt(pow(pt1.x - pt2.x, 2) + pow(pt1.y - pt2.y, 2))

#define SET_DRAW_MASK_INDEX(r, a, b, m) if(r.a && r.b) r.m_nDrawMaskIndex |= m;
#define SET_DRAW_MASK_INDEX_2(r, r2, a, b, m) if(r.a && r2.b) r.m_nDrawMaskIndex |= m;

#define SET_TARGET_DRAW_MASK_INDEX(r) \
	SET_DRAW_MASK_INDEX(r, m_bSDLineMask, m_bELVDDLineMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_ELVDDLINE); \
	SET_DRAW_MASK_INDEX(r, m_bELVDDLineMask, m_bInitialLineMask, SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_INITIALLINE); \
	SET_DRAW_MASK_INDEX(r, m_bELVDDLineMask, m_bCirclePatternMask, SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_CIRCLEPATTERN); \
	SET_DRAW_MASK_INDEX(r, m_bELVDDLineMask, m_bBridgePatternMask, SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_BRIDGEPATTERN); \
	SET_DRAW_MASK_INDEX(r, m_bInitialLineMask, m_bCirclePatternMask, SD_DRAW_MASK_SHORT_CUT_INITIALLINE_CIRCLEPATTERN); \
	SET_DRAW_MASK_INDEX(r, m_bInitialLineMask, m_bBridgePatternMask, SD_DRAW_MASK_SHORT_CUT_INITIALLINE_BRIDGEPATTERN); \
	SET_DRAW_MASK_INDEX(r, m_bCirclePatternMask, m_bBridgePatternMask, SD_DRAW_MASK_SHORT_CUT_CIRCLEPATTERN_BRIDGEPATTERN);

#define SET_TARGET_DRAW_MASK_INDEX_2(r, r2, v) \
	if(v) {	\
		SET_DRAW_MASK_INDEX_2(r, r2, m_bInitialLineMask, m_bCirclePatternMask, SD_DRAW_MASK_SHORT_CUT_INITIALLINE_CIRCLEPATTERN); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bCirclePatternMask, m_bInitialLineMask, SD_DRAW_MASK_SHORT_CUT_INITIALLINE_CIRCLEPATTERN); \
	} \
	else { \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bInitialLineMask, m_bSDLineMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_INITIALLINE); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bSDLineMask, m_bInitialLineMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_INITIALLINE); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bCirclePatternMask, m_bSDLineMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_CIRCLEPATTERN); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bSDLineMask, m_bCirclePatternMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_CIRCLEPATTERN); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bBridgePatternMask, m_bSDLineMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_BRIDGEPATTERN); \
		SET_DRAW_MASK_INDEX_2(r, r2, m_bSDLineMask, m_bBridgePatternMask, SD_DRAW_MASK_SHORT_CUT_SDLINE_BRIDGEPATTERN); \
				}


#define IF_CHECK_DRAW_MASK_INDEX(m) if (nDrawMaskIndex & m) { nDrawMaskIndex &= ~m;
#define ELSE_IF_CHECK_DRAW_MASK_INDEX(m) } else IF_CHECK_DRAW_MASK_INDEX(m)
#define ELSE_CHECK_DRAW_MASK_INDEX } else 

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();


// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()

};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAutoRepairTesterDlg dialog

CAutoRepairTesterDlg::CAutoRepairTesterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAutoRepairTesterDlg::IDD, pParent)
	, m_nTypeIndex(0)
	, m_strTestResult(_T(""))
	, m_bScaleBestFitImage(FALSE)
	, m_nBestFitImageScale(0.005f)
	, m_nHoleMinSize(0)
	, m_nImageRotation(0)
	, m_bFlipImageHoriz(FALSE)
	, m_bFlipImageVert(FALSE)
	, m_nSeedMinSize(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	wchar_t szPath[MAX_PATH];
	GetModuleFileName(NULL, szPath, MAX_PATH - 1);
	PathRemoveFileSpec(szPath);

	m_strRootDataFolder = szPath;
	m_strRootDataFolder += L"\\AutoRepairData\\";

	// set default value
	m_strDefaultDataFolder = "2016-02-17";
	m_nTypeIndex = 2;

	m_nProcessStep = STEP_NONE;

	m_nImageRotation = IMAGE_ROTATION_0;

	m_bShowDetectWithMask = false;
}

void CAutoRepairTesterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGE_AREA, m_ctlImageArea);
	DDX_Control(pDX, IDC_MASK_AREA, m_ctlMaskArea);
	DDX_Control(pDX, IDC_LOG_LIST, m_ctlLogList);
	DDX_Radio(pDX, IDC_TYPE_ACTIVE, m_nTypeIndex);
	DDX_Text(pDX, IDC_TEST_BINARIZE_THRESHOLD, m_nTestBinarizeThreshold);
	DDX_Text(pDX, IDC_TEST_DEFECT_MIN_SIZE, m_nDefectMinSize);
	DDX_Text(pDX, IDC_TEST_RESULT, m_strTestResult);
	DDX_Control(pDX, IDC_IMAGE_LIST, m_ctlImageList);
	DDX_Control(pDX, IDC_MASK_COMBO, m_ctlMaskList);
	DDX_Control(pDX, IDC_DATA_FOLDER_COMBO, m_ctlDataFolderList);
	DDX_Check(pDX, IDC_SCALE_BEST_FIT_IMAGE, m_bScaleBestFitImage);
	DDX_Text(pDX, IDC_EDIT_BEST_FIT_IMAGE_SCALE, m_nBestFitImageScale);
	DDV_MinMaxFloat(pDX, m_nBestFitImageScale, 0.001f, 5.f);
	DDX_Text(pDX, IDC_TEST_HOLE_MIN_SIZE, m_nHoleMinSize);
	DDX_Radio(pDX, IDC_RADIO_IMAGE_ROTATION_0, m_nImageRotation);
	DDX_Check(pDX, IDC_CHECK_IMAGE_FLIP_HORIZ, m_bFlipImageHoriz);
	DDX_Check(pDX, IDC_CHECK_IMAGE_FLIP_VERT, m_bFlipImageVert);
	DDX_Text(pDX, IDC_TEST_SEED_MIN_SIZE, m_nSeedMinSize);
}

BEGIN_MESSAGE_MAP(CAutoRepairTesterDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_NIPL_IMAGE_POS_VALUE, &CAutoRepairTesterDlg::OnImagePosValue)

	ON_COMMAND_RANGE(IDC_TYPE_ACTIVE, IDC_TYPE_SD, OnClickedTypeRadio)
	ON_WM_DROPFILES()
	ON_BN_CLICKED(IDC_LOAD_MASK, &CAutoRepairTesterDlg::OnBnClickedLoadMask)
	ON_BN_CLICKED(IDC_FIND_DEFECT, &CAutoRepairTesterDlg::OnBnClickedFindDefect)
	ON_LBN_SELCHANGE(IDC_IMAGE_LIST, &CAutoRepairTesterDlg::OnSelchangeImageList)
	ON_CBN_SELCHANGE(IDC_MASK_COMBO, &CAutoRepairTesterDlg::OnSelchangeMaskList)
	ON_BN_CLICKED(IDC_IMAGE_RELOAD, &CAutoRepairTesterDlg::OnBnClickedImageReload)
	ON_BN_CLICKED(IDC_FOCUS_START_POS, &CAutoRepairTesterDlg::OnBnClickedFocusStartPos)
	ON_BN_CLICKED(IDC_FOCUS_END_POS, &CAutoRepairTesterDlg::OnBnClickedFocusEndPos)
	ON_BN_CLICKED(IDC_CLASSIFY, &CAutoRepairTesterDlg::OnBnClickedClassify)
	ON_BN_CLICKED(IDC_DRAW, &CAutoRepairTesterDlg::OnBnClickedDraw)
	ON_CBN_SELCHANGE(IDC_DATA_FOLDER_COMBO, &CAutoRepairTesterDlg::OnSelchangeDataFolderCombo)
	ON_BN_CLICKED(IDC_FIND_BEST_FIT_IMAGE, &CAutoRepairTesterDlg::OnBnClickedFindBestFitImage)
	ON_BN_CLICKED(IDC_SAVE_BEST_FIT_IMAGE, &CAutoRepairTesterDlg::OnBnClickedSaveBestFitImage)
	ON_BN_CLICKED(IDC_SCALE_BEST_FIT_IMAGE, &CAutoRepairTesterDlg::OnBnClickedScaleBestFitImage)
	ON_COMMAND_RANGE(IDC_RADIO_IMAGE_ROTATION_0, IDC_CHECK_IMAGE_FLIP_VERT, &CAutoRepairTesterDlg::OnClickedRadioImageRotation)
END_MESSAGE_MAP()


// CAutoRepairTesterDlg message handlers

BOOL CAutoRepairTesterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	DragAcceptFiles(TRUE);

	// TODO: Add extra initialization here
	// Set Image Control
	CRect rcImageArea;
	m_ctlImageArea.GetWindowRect(&rcImageArea);
	ScreenToClient(&rcImageArea);
	m_ctlImageArea.ShowWindow(SW_HIDE);

	if (!m_wndImg.CreateCtl(this, IDC_IMAGE_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}
	m_wndImg.SetWindowPos(NULL, rcImageArea.left, rcImageArea.top, rcImageArea.Width(), rcImageArea.Height(), SWP_NOZORDER);

	// Set Mask Image Control
	CRect rcMaskArea;
	m_ctlMaskArea.GetWindowRect(&rcMaskArea);
	ScreenToClient(&rcMaskArea);
	m_ctlMaskArea.ShowWindow(SW_HIDE);

	if (!m_wndMask.CreateCtl(this, IDC_MASK_CTRL))
	{
		AddLogText(L"Failed to create image view");
		return FALSE;
	}
	m_wndMask.SetSendEventMessage(false);
	m_wndMask.SetWindowPos(NULL, rcMaskArea.left, rcMaskArea.top, rcMaskArea.Width(), rcMaskArea.Height(), SWP_NOZORDER);

	LoadDataFolderList();
	LoadData();

	//2016.03.24 정의천
	SetDlgItemText(IDC_EDIT_COLOR_TH_R, _T("10"));
	SetDlgItemText(IDC_EDIT_COLOR_TH_G, _T("10"));
	SetDlgItemText(IDC_EDIT_COLOR_TH_B, _T("10"));
	SetDlgItemText(IDC_EDIT_HV_RATE, _T("10"));
	///////////////////////////////////////////////////

	OnBnClickedScaleBestFitImage();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAutoRepairTesterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAutoRepairTesterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAutoRepairTesterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CAutoRepairTesterDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialogEx::OnSetFocus(pOldWnd);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	m_wndImg.SetFocus();
}

void CAutoRepairTesterDlg::ShowImage(Mat dImg)
{
	m_wndImg.Show(dImg);
	ShowImageInfo();
}

void CAutoRepairTesterDlg::ShowImageWithMask(Mat dImg)
{
	m_dDrawImg = Mat::zeros(m_dImg.size(), CV_8UC3);

	TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
	TargetResult &dTargetResult = dTargetInfo.m_dTargetResult;

	if (dTargetResult.m_bSDLineMask) DrawDefect_Mask(MASK_SD_SD_LINE, Mat(), CV_RGB(255, 255, 255), 1.f);
	if (dTargetResult.m_bELVDDLineMask) DrawDefect_Mask(MASK_SD_ELVDD_LINE, Mat(), CV_RGB(255, 255, 255), 1.f);
	if (dTargetResult.m_bInitialLineMask) DrawDefect_Mask(MASK_SD_INITIAL_LINE, Mat(), CV_RGB(255, 255, 255), 1.f);
	if (dTargetResult.m_bCirclePatternMask) DrawDefect_Mask(MASK_SD_CIRCLE_PATTERN, Mat(), CV_RGB(255, 255, 255), 1.f);
	if (dTargetResult.m_bBridgePatternMask) DrawDefect_Mask(MASK_SD_BRIDGE_PATTERN, Mat(), CV_RGB(255, 255, 255), 1.f);
	if (m_dDefectTargetInfo.m_nDefectIndex == DEFECT_1_HOLE) {
		DrawDefect_Mask(MASK_SD_HOLE, Mat(), CV_RGB(255, 255, 255), 1.f);
	}

	DrawDefect_Defect(CV_RGB(0, 255, 0), 0.5f);

	ShowImage(m_dDrawImg);
}

void CAutoRepairTesterDlg::ShowMask()
{
	m_wndMask.Show(m_dMask);
}

bool CAutoRepairTesterDlg::LoadImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg;
	if (!pNIPO->LoadImage(LPCTSTR(m_strImageFile), dImg)) {
		AddLogText(L"Fail to load Image : %s", LPCTSTR(m_strImageFile));
		return false;
	}
	else {
		AddLogText(L"Success to load Image : %s", LPCTSTR(m_strImageFile));
	}

	m_dImg = dImg;

	return true;
}

void CAutoRepairTesterDlg::AddLogText(const wchar_t* szText, ...)
{
	wchar_t szBufferText[2048] = { 0, };

	va_list vargs;
	va_start(vargs, szText);
	vswprintf_s(szBufferText, szText, (va_list)vargs);
	va_end(vargs);

	SYSTEMTIME dTime;
	GetLocalTime(&dTime);

	wchar_t szTime[256];
	swprintf_s(szTime, L"[%02d:%02d:%02d.%03d] ", dTime.wHour, dTime.wMinute, dTime.wSecond, dTime.wMilliseconds);

	wstring strFullText(szTime);
	strFullText += szBufferText;

	int nIndex = m_ctlLogList.GetCount();
	nIndex = m_ctlLogList.InsertString(nIndex, strFullText.c_str());
	m_ctlLogList.SetCurSel(nIndex);
}

LRESULT CAutoRepairTesterDlg::OnImagePosValue(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your message handler code here and/or call default
	int nPosX = (int)wParam;
	int nPosY = (int)lParam;
	ShowImagePosValue(CPoint(nPosX, nPosY));

	return 0;
}

void CAutoRepairTesterDlg::ShowImagePosValue(CPoint ptImagePos)
{
	if (CHECK_EMPTY_IMAGE(m_dImg)) {
		return;
	}

	wchar_t szValue[256] = L"Unsupported Type";
	int nDepth = m_dImg.depth();
	int nChannels = m_dImg.channels();
	if (nChannels == 1) {
		switch (nDepth) {
		case CV_8U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_8S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT8>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16U:
			swprintf_s(szValue, L"%u", m_dImg.at<UINT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_16S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT16>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32S:
			swprintf_s(szValue, L"%d", m_dImg.at<INT32>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_32F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<FLOAT>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		case CV_64F:
			swprintf_s(szValue, L"%.3f", m_dImg.at<DOUBLE>(Point(ptImagePos.x, ptImagePos.y)));
			break;
		}
	}
	else if (nChannels == 3) {
		if (nDepth == CV_8U) {
			Vec3b dPixel = m_dImg.at<Vec3b>(Point(ptImagePos.x, ptImagePos.y));
			swprintf_s(szValue, L"%u, %u, %u", dPixel[2], dPixel[1], dPixel[0]);
		}
	}

	wchar_t szText[256];
	swprintf_s(szText, L"Pos(%d, %d) Value(%s)", ptImagePos.x, ptImagePos.y, szValue);
	GetDlgItem(IDC_IMAGE_POS)->SetWindowText(szText);
}

void CAutoRepairTesterDlg::ShowImageInfo()
{
	int nSizeX = m_dImg.cols;
	int nSizeY = m_dImg.rows;

	wstring strDepth = L"?";
	int nDepth = m_dImg.depth();
	switch (nDepth) {
	case CV_8U: strDepth = L"8U"; break;
	case CV_8S: strDepth = L"8S"; break;
	case CV_16U: strDepth = L"16U"; break;
	case CV_16S: strDepth = L"16S"; break;
	case CV_32S: strDepth = L"32S"; break;
	case CV_32F: strDepth = L"32F"; break;
	case CV_64F: strDepth = L"64F"; break;
	}

	int nChannels = m_dImg.channels();

	wchar_t szText[256];
	swprintf_s(szText, L"Size(%d, %d), Type(%s), Channel(%d)", nSizeX, nSizeY, strDepth.c_str(), nChannels);
	GetDlgItem(IDC_IMAGE_INFO)->SetWindowText(szText);
}

void CAutoRepairTesterDlg::OnClickedTypeRadio(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeType();
}

void CAutoRepairTesterDlg::ShowPosBox()
{
	UpdateData();

	m_wndImg.ClearImageMark();

	if (!CHECK_EMPTY_IMAGE(m_dImg) && !CHECK_EMPTY_IMAGE(m_dMask)) {
		if (m_nProcessStep == STEP_DETECT) {
			for (auto dTargetInfo : m_listTargetInfo) {
				ImageMark dMark;
				dMark.m_rcBoundingBox = dTargetInfo.m_rcArea;
				m_wndImg.AddImageMark(dMark);
			}
		}
	}

	m_wndImg.ShowImageMark();
}

void CAutoRepairTesterDlg::ClearPosBox()
{
	m_wndImg.ClearImageMark();
}

void CAutoRepairTesterDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int nCount = DragQueryFile(hDropInfo, 0xffffffff, nullptr, 0);
	if (nCount > 1) {
		AddLogText(L"Cannot load multiple files.");
		return;
	}

	wchar_t szPath[1024];
	DragQueryFile(hDropInfo, 0, szPath, 1023);

	m_strImageFile = szPath;

	m_nProcessStep = STEP_LOAD_IMAGE;

	LoadImage();
	ShowImage(m_dImg);
	
	CDialogEx::OnDropFiles(hDropInfo);
}

void CAutoRepairTesterDlg::OnBnClickedLoadMask()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	LoadMask();
}

bool CAutoRepairTesterDlg::ClassifyDefect()
{
	UpdateData();

	m_nProcessStep = STEP_NONE;
	ShowPosBox();

	if (!FindTarget()) {
		AddLogText(L"Cannot Do Test. Fail to find Target.");

		m_strTestResult = L"NO TARGET";
		UpdateData(FALSE);

		return false;
	}

	if (!FindDefect()) {
		AddLogText(L"Cannot Do Test. Fail to find Defect");

		m_strTestResult = L"NO DEFECT";
		UpdateData(FALSE);

		return false;
	}

	CheckDefect();
	UpdateData(FALSE);

	return true;
}

void CAutoRepairTesterDlg::CheckDefect()
{
	m_nDefectIndex = DEFECT_UNKNOWN;
	switch (m_nTypeIndex) {
	case TYPE_ACTIVE: m_nDefectIndex = CheckDefect_Active(); break;
	case TYPE_GATE: m_nDefectIndex = CheckDefect_Gate(); break;
	case TYPE_SD: m_nDefectIndex = CheckDefect_SD(); break;
	}

	switch (m_nDefectIndex) {
	case DEFECT_UNKNOWN: m_strTestResult = "UNKNOWN"; break;
	case DEFECT_PARTICLE: m_strTestResult = "PARTICLE"; break;
	case DEFECT_2_PTN_PARTICLE: m_strTestResult = "DARK 2 PTN"; break;
	case DEFECT_1_PTN_SHORT: m_strTestResult = "1 PTN SHORT"; break;
	case DEFECT_2_PTN_SHORT: m_strTestResult = "2 PTN SHORT"; break;
	case DEFECT_1_SEED_SHORT: m_strTestResult = "1 SEED SHORT"; break;
	case DEFECT_2_SEED_SHORT: m_strTestResult = "2 SEED SHORT"; break;
	case DEFECT_1_HOLE: m_strTestResult = "1 HOLE"; break;
	case DEFECT_2_HOLE: m_strTestResult = "2 HOLE"; break;
	case DEFECT_1_HABU_SHORT: m_strTestResult = "1 HABU SHORT"; break;
	case DEFECT_2_HABU_SHORT: m_strTestResult = "2 HABU SHORT"; break;
	case DEFECT_LINE_SHORT: m_strTestResult = "LINE SHORT"; break;
	case DEFECT_LINE_SEED: m_strTestResult = "LINE SEED"; break;
	case DEFECT_DEPO_OPEN: m_strTestResult = "DEPO OPEN"; break;
	case DEFECT_DARK_DEPO: m_strTestResult = "DARK DEPO"; break;
	case DEFECT_DARK_LINE: m_strTestResult = "DARK LINE"; break;
	case DEFECT_1_DARK_PTN: m_strTestResult = "1 DARK PTN"; break;
	case DEFECT_2_DARK_PTN: m_strTestResult = "2 DARK PTN"; break;

	// Gate
	case DEFECT_GATE_DEPO_OPEN: m_strTestResult = "DEPO OPEN"; break;
	case DEFECT_GATE_2_PTN_SHORT:m_strTestResult = "2 PTN SHORT"; break;
	case DEFECT_GATE_1_PTN_ERR: m_strTestResult = "1 PTN ERR"; break;
	case DEFECT_GATE_2_PTN_ERR: m_strTestResult = "2 PTN ERR"; break;
	case DEFECT_GATE_LINE_SHORT: m_strTestResult = "LINE SHORT"; break;
	case DEFECT_GATE_SEED_LINE_SHORT: m_strTestResult = "SEED LINE SHORT"; break;
	case DEFECT_GATE_2_PTN_SEED_SHORT: m_strTestResult = "PTN SEED SHORT"; break;
	case DEFECT_GATE_DARK_DEPO:  m_strTestResult = "DARK DEPO"; break;
	case DEFECT_GATE_2_DARK_PTN:  m_strTestResult = "DARK 2 PTN"; break;
	case DEFECT_GATE_DARK_LINE_SHORT:m_strTestResult = "DARK Line Short"; break;
	case DEFECT_GATE_1_HABU_PTN_SHORT: m_strTestResult = "HABU PTN SHORT"; break;
	case DEFECT_GATE_2_HABU_PTN_SHORT: m_strTestResult = "HABU PTN SHORT"; break;
	case DEFECT_GATE_1_HOLE: m_strTestResult = "1 HOLE"; break;
	case DEFECT_GATE_THIN: m_strTestResult = "THINNING"; break;
	}
}


DEFECT_INDEX CAutoRepairTesterDlg::CheckDefect_Active()
{
	DEFECT_INDEX nDefectIndex = DEFECT_UNKNOWN;
	bool bHasDarkPoint = false;
	for (auto dTargetInfo : m_listTargetInfo) {
		Rect rcTarget = dTargetInfo.m_rcArea;
		bHasDarkPoint = bHasDarkPoint || dTargetInfo.m_bHasDarkPoint;
		m_dTargetResult = Mat(m_dResultForMaskingImg, rcTarget);

		AddLogText(L"Check Target Area : Pos(%d, %d) Size(%d, %d)", rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);

		m_ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		bool bExistLayer1 = CheckMask(MASK_LAYER_1);
		bool bExistLayer2 = CheckMask(MASK_LAYER_2);

		AddLogText(L"Check Mask Layer 1 : %s", bExistLayer1 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 2 : %s", bExistLayer2 ? L"true" : L"false");

		int nExistCount = 0;
		if (bExistLayer1) nExistCount++;
		if (bExistLayer2) nExistCount++;
	
		if (nExistCount > 1) {
			if (nDefectIndex == DEFECT_UNKNOWN) {
				nDefectIndex = DEFECT_PARTICLE;
			}
			else {
				nDefectIndex = DEFECT_2_PTN_PARTICLE;
			}
		}
		else if (bExistLayer1) {
			if (bHasDarkPoint) {
				if (nDefectIndex == DEFECT_UNKNOWN) {
					nDefectIndex = DEFECT_1_SEED_SHORT;
				}
				else {
					if (nDefectIndex == DEFECT_PARTICLE || nDefectIndex == DEFECT_2_PTN_PARTICLE) {
						nDefectIndex = DEFECT_2_PTN_PARTICLE;
					}
					else {
						nDefectIndex = DEFECT_2_SEED_SHORT;
					}
				}
			}
			else {
				if (nDefectIndex == DEFECT_UNKNOWN) {
					nDefectIndex = DEFECT_1_PTN_SHORT;
				}
				else {
					if (nDefectIndex == DEFECT_PARTICLE || nDefectIndex == DEFECT_2_PTN_PARTICLE) {
						nDefectIndex = DEFECT_2_PTN_PARTICLE;
					}
					else {
						nDefectIndex = DEFECT_2_PTN_SHORT;
					}
				}
			}
		}
	}

	return nDefectIndex;
}

DEFECT_INDEX CAutoRepairTesterDlg::CheckDefect_Gate()
{
	DEFECT_INDEX nDefectIndex = DEFECT_UNKNOWN;
	bool bHasDarkPoint = false;

	bool bSteveTemp4 = false;
	bool bSteveTemp5 = false;
	bool bSteveTemp6 = false;
	bool bSteveTemp7 = false;

	bool bSteveAndTemp6 = true;

	//Steve
	int nPTNCount = 0;
	for (auto dTargetInfo : m_listTargetInfo) {

		
		Rect rcTarget = dTargetInfo.m_rcArea;

		bHasDarkPoint = bHasDarkPoint || dTargetInfo.m_bHasDarkPoint;
		m_dTargetResult = Mat(m_dResultForMaskingImg, rcTarget);

		AddLogText(L"Check Target Area : Pos(%d, %d) Size(%d, %d)", rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);

		m_ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		bool bExistLayer1 = CheckMask(MASK_LAYER_1);
		bool bExistLayer2 = CheckMask(MASK_LAYER_2);
		bool bExistLayer3 = CheckMask(MASK_LAYER_3);
		//Steve
		bool bExistLayer4 = CheckMask(MASK_LAYER_4); //뚱뚱 Gate 최상위
		bSteveTemp4 = bSteveTemp4 | bExistLayer4;

		bool bExistLayer5 = CheckMask(MASK_LAYER_5); //뚱뚱 Gate 2 Pattern
		bSteveTemp5 = bSteveTemp5 | bExistLayer5;

		bool bExistLayer6 = CheckMask(MASK_LAYER_6); //뚱뚱 Gate 3 Pattern
		bSteveTemp6 = bSteveTemp6 | bExistLayer6;
		bSteveAndTemp6 = bSteveAndTemp6 & bExistLayer6;
		bool bExistLayer7 = CheckMask(MASK_LAYER_7); //뚱뚱 gate 최하위
		bSteveTemp7 = bSteveTemp7 | bExistLayer7;
		

		bool bExistLayer8 = CheckMask(MASK_LAYER_8); //hole 뚱뚱 Active
//		bSteveTemp8 = bSteveTemp8 | bExistLayer8;

		nPTNCount++;  //Steve

		AddLogText(L"Check Mask Layer 1 : %s", bExistLayer1 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 2 : %s", bExistLayer2 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 3 : %s", bExistLayer3 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 4 : %s", bExistLayer3 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 5 : %s", bExistLayer3 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 6 : %s", bExistLayer3 ? L"true" : L"false");
		AddLogText(L"Check Mask Layer 7 : %s", bExistLayer3 ? L"true" : L"false");

		int nExistCount = 0;
		if (bExistLayer1) nExistCount++;
		if (bExistLayer2) nExistCount++;
		if (bExistLayer3) nExistCount++;

		//if (nExistCount > 1) {
		//	nDefectIndex = DEFECT_PARTICLE;
		//}
		//else if (bExistLayer3) {
		//	nDefectIndex = DEFECT_OPEN;
		//}
		//else if (bExistLayer1) {
		//	nDefectIndex = DEFECT_SHORT;
		//}
		/* Steve Lee 
		    Layer8 == hole 뚱뚱 마스크사용(Layer 2의 hole 부분만 뚱뚱함) 
			Layer 4,5,6,7 뚱뚱 mask 사용

		
		       
		*/
		nDefectIndex = DEFECT_UNKNOWN;
		if (nExistCount > 1)
		{
			if (bHasDarkPoint && (bSteveTemp4 || bSteveTemp5 || bSteveTemp7))
			{
				nDefectIndex = DEFECT_GATE_DARK_DEPO;
		
				if ((bSteveTemp4 && bSteveTemp5) || (bSteveTemp5 && bSteveTemp7) || (bSteveTemp4 && bSteveTemp7))
					nDefectIndex = DEFECT_GATE_DARK_LINE_SHORT;
			}

			if (bHasDarkPoint && nPTNCount > 1)
			{
				if ((nDefectIndex == DEFECT_GATE_DARK_DEPO) && (bExistLayer4 || bExistLayer5 || bExistLayer7))
					nDefectIndex = DEFECT_GATE_DARK_DEPO;
				else if (nDefectIndex == DEFECT_GATE_LINE_SHORT)
					nDefectIndex = DEFECT_GATE_LINE_SHORT;
				else
					nDefectIndex = DEFECT_GATE_2_DARK_PTN;
			}
		}
		else if (bExistLayer3)
		{
			if (bHasDarkPoint)
				nDefectIndex = DEFECT_GATE_DARK_DEPO;
			else if (bExistLayer4 || bExistLayer5 || bExistLayer7)
				nDefectIndex = DEFECT_GATE_DEPO_OPEN;
		}
/*
		else if (bSteveAndTemp6 && bHasDarkPoint)
			nDefectIndex = DEFECT_GATE_2_PTN_SEED_SHORT;
		else if (bSteveAndTemp6)
			nDefectIndex = DEFECT_GATE_2_PTN_SHORT;
*/

//		else if (nPTNCount == 2 && bExistLayer6)
//			nDefectIndex = DEFECT_2_PTN_SHORT;

/////
		
///////////
		else if (!(bExistLayer1 && bExistLayer2 && bExistLayer3) && (((bSteveTemp4 && bSteveTemp5) || (bSteveTemp5 && bSteveTemp7) || (bSteveTemp4 && bSteveTemp7)) && bHasDarkPoint))
			nDefectIndex = DEFECT_GATE_SEED_LINE_SHORT;

		else if (bSteveTemp6 && bHasDarkPoint)
			nDefectIndex = DEFECT_GATE_2_PTN_SEED_SHORT;

		else if (bSteveAndTemp6 && bExistLayer8 && nPTNCount == 2)
			nDefectIndex = DEFECT_GATE_2_PTN_SHORT;


		


//		else if ((bExistLayer1 && bExistLayer2 && bExistLayer3) && bHasDarkPoint)
//			nDefectIndex = DEFECT_GATE_LINE_SHORT;
		else if ((bSteveTemp4 && bSteveTemp5) || (bSteveTemp5 && bSteveTemp7) || (bSteveTemp4 && bSteveTemp7))
			nDefectIndex = DEFECT_GATE_LINE_SHORT;
//		else if (0)
//			nDefectIndex = DEFECT_DARK_DEPO;

		else if ((bExistLayer1 && bSteveTemp6 /*bExistLayer6*/ && !bHasDarkPoint) && nPTNCount == 1)
			nDefectIndex = DEFECT_GATE_1_PTN_ERR;
		else if ((bExistLayer1 && bSteveTemp6 /*bExistLayer6*/ && !bHasDarkPoint) && nPTNCount == 2)
			nDefectIndex = DEFECT_GATE_2_PTN_ERR;


		else if (0)
			nDefectIndex = DEFECT_GATE_2_DARK_PTN;

		else if (bExistLayer1 && (bExistLayer7 && bExistLayer8))
			nDefectIndex = DEFECT_GATE_1_HOLE;

		else if (bExistLayer1 && !bHasDarkPoint) // && !bExistLayer7)
		{
//			if (!bHasDarkPoint)
			nDefectIndex = DEFECT_GATE_2_HABU_PTN_SHORT;
		}

		else if (0)
			nDefectIndex = DEFECT_GATE_THIN;

	}

	return nDefectIndex;
}

DEFECT_INDEX CAutoRepairTesterDlg::CheckDefect_SD()
{
	DEFECT_INDEX nDefectIndex = DEFECT_UNKNOWN;

	TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
	Rect rcTarget = dTargetInfo.m_rcArea;
	m_dTargetResult = Mat(m_dResultForMaskingImg, rcTarget);
	m_ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

	AddLogText(L"Check Target Area : Pos(%d, %d) Size(%d, %d)", rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);

	TargetResult &dTargetResult = dTargetInfo.m_dTargetResult;
	dTargetResult.m_bSDLineMask = CheckMask(MASK_SD_SD_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bELVDDLineMask = CheckMask(MASK_SD_ELVDD_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bInitialLineMask = CheckMask(MASK_SD_INITIAL_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bCirclePatternMask = CheckMask(MASK_SD_CIRCLE_PATTERN, m_nDefectMinSize, true);
	dTargetResult.m_bBridgePatternMask = CheckMask(MASK_SD_BRIDGE_PATTERN, m_nDefectMinSize, true);
	dTargetResult.m_bHoleMask = CheckMask(MASK_SD_HOLE, m_nHoleMinSize);
	dTargetResult.m_bCommonHoleMask = false; // CheckMask(MASK_SD_COMMON_HOLE, m_nHoleMinSize);
	dTargetResult.m_bEmissionLineMask = CheckMask(MASK_GATE_EMISSION_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bScanLineMask = CheckMask(MASK_GATE_SCAN_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bOpenLineMask = CheckMask(MASK_GATE_OPEN_LINE, m_nDefectMinSize, true);
	dTargetResult.m_bCapMask = CheckMask(MASK_GATE_CAP, m_nDefectMinSize, true);

	bool bDark = dTargetResult.m_bDark;
	bool bHasSeed = dTargetResult.m_bHasSeed;

	AddLogText(L"Check Mask SD Line Mask : %s", dTargetResult.m_bSDLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask ELVDD Line Mask : %s", dTargetResult.m_bELVDDLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask Initial Line Mask : %s", dTargetResult.m_bInitialLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask Circle Pattern Mask : %s", dTargetResult.m_bCirclePatternMask ? L"true" : L"false");
	AddLogText(L"Check Mask Bridge Pattern Mask : %s", dTargetResult.m_bBridgePatternMask ? L"true" : L"false");
	AddLogText(L"Check Mask Hole Mask : %s", dTargetResult.m_bHoleMask ? L"true" : L"false");
	AddLogText(L"Check Mask Common Hole Mask : %s", dTargetResult.m_bCommonHoleMask ? L"true" : L"false");
	AddLogText(L"Check Mask Emission Line Mask : %s", dTargetResult.m_bEmissionLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask Scan Line Mask : %s", dTargetResult.m_bScanLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask Open Line Mask : %s", dTargetResult.m_bOpenLineMask ? L"true" : L"false");
	AddLogText(L"Check Mask Cap Mask : %s", dTargetResult.m_bCapMask ? L"true" : L"false");

	if (dTargetResult.m_bSDLineMask) dTargetResult.m_nConnectedLineCount++;
	if (dTargetResult.m_bELVDDLineMask) dTargetResult.m_nConnectedLineCount++;
	if (dTargetResult.m_bInitialLineMask) dTargetResult.m_nConnectedLineCount++;

	if (dTargetResult.m_bEmissionLineMask) dTargetResult.m_nHabuConnectedLineCount++;
	if (dTargetResult.m_bScanLineMask) dTargetResult.m_nHabuConnectedLineCount++;
	if (dTargetResult.m_bCapMask) dTargetResult.m_nHabuConnectedLineCount++;

	AddLogText(L"Connected Count : %d, Habu : %d", dTargetResult.m_nConnectedLineCount, dTargetResult.m_nHabuConnectedLineCount);

	if (dTargetResult.m_nConnectedLineCount == 0) {
		if (dTargetResult.m_bCirclePatternMask && dTargetResult.m_bBridgePatternMask) {
			nDefectIndex = DEFECT_1_PTN_SHORT;
		}
	}
	if (dTargetResult.m_nConnectedLineCount == 1) {
		if (dTargetResult.m_bCirclePatternMask || dTargetResult.m_bBridgePatternMask) {
			nDefectIndex = DEFECT_1_PTN_SHORT;
		}
		else if (!dTargetResult.m_bHoleMask && (dTargetResult.m_bSDLineMask || dTargetResult.m_bInitialLineMask)) {
			nDefectIndex = DEFECT_DEPO_OPEN;
		}
	}
	else if (dTargetResult.m_nConnectedLineCount >= 2) {
		if (dTargetResult.m_bSDLineMask) {
			nDefectIndex = DEFECT_LINE_SHORT;
		}
		else if (dTargetResult.m_bCirclePatternMask || dTargetResult.m_bBridgePatternMask) {
			nDefectIndex = DEFECT_1_PTN_SHORT;
		}
		else if (dTargetResult.m_bELVDDLineMask && dTargetResult.m_bInitialLineMask) {
			nDefectIndex = DEFECT_LINE_SHORT;
		}
	}

	if (nDefectIndex == DEFECT_UNKNOWN && dTargetResult.m_nHabuConnectedLineCount >= 2) {
		nDefectIndex = DEFECT_1_HABU_SHORT;
	}

	if (nDefectIndex == DEFECT_UNKNOWN && dTargetResult.m_bHoleMask) {
		nDefectIndex = DEFECT_1_HOLE;
	}

	if (m_dDefectTargetInfo.m_bConnectedTarget) {
		// Check Two Targets
		TargetInfo &dTargetInfo2 = m_dDefectTargetInfo.m_dTargetInfo2;
		rcTarget = dTargetInfo2.m_rcArea;
		m_dTargetResult = Mat(m_dResultForMaskingImg, rcTarget);
		m_ptStartInTemplate = dTargetInfo2.m_ptStartPosInTemplate;

		AddLogText(L"Check Target Area 2 : Pos(%d, %d) Size(%d, %d)", rcTarget.x, rcTarget.y, rcTarget.width, rcTarget.height);

		TargetResult &dTargetResult2 = dTargetInfo2.m_dTargetResult;

		dTargetResult2.m_bSDLineMask = CheckMask(MASK_SD_SD_LINE, m_nDefectMinSize, true);
		dTargetResult2.m_bELVDDLineMask = CheckMask(MASK_SD_ELVDD_LINE, m_nDefectMinSize, true);
		dTargetResult2.m_bInitialLineMask = CheckMask(MASK_SD_INITIAL_LINE, m_nDefectMinSize, true);
		dTargetResult2.m_bCirclePatternMask = CheckMask(MASK_SD_CIRCLE_PATTERN, m_nDefectMinSize, true);
		dTargetResult2.m_bBridgePatternMask = CheckMask(MASK_SD_BRIDGE_PATTERN, m_nDefectMinSize, true);
		dTargetResult2.m_bHoleMask = CheckMask(MASK_SD_HOLE, m_nHoleMinSize);
		dTargetResult2.m_bCommonHoleMask = false; // CheckMask(MASK_SD_COMMON_HOLE, m_nHoleMinSize);

		bDark = bDark || dTargetResult2.m_bDark;
		bHasSeed = bHasSeed || dTargetResult2.m_bHasSeed;

		AddLogText(L"Check Mask 2 SD Line Mask : %s", dTargetResult2.m_bSDLineMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 ELVDD Line Mask : %s", dTargetResult2.m_bELVDDLineMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 Initial Line Mask : %s", dTargetResult2.m_bInitialLineMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 Circle Pattern Mask : %s", dTargetResult2.m_bCirclePatternMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 Bridge Pattern Mask : %s", dTargetResult2.m_bBridgePatternMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 Hole Mask : %s", dTargetResult2.m_bHoleMask ? L"true" : L"false");
		AddLogText(L"Check Mask 2 Common Hole Mask : %s", dTargetResult2.m_bCommonHoleMask ? L"true" : L"false");

		if (dTargetResult2.m_bSDLineMask) dTargetResult2.m_nConnectedLineCount++;
		if (dTargetResult2.m_bELVDDLineMask) dTargetResult2.m_nConnectedLineCount++;
		if (dTargetResult2.m_bInitialLineMask) dTargetResult2.m_nConnectedLineCount++;

		if (dTargetResult2.m_bEmissionLineMask) dTargetResult2.m_nHabuConnectedLineCount++;
		if (dTargetResult2.m_bScanLineMask) dTargetResult2.m_nHabuConnectedLineCount++;
		if (dTargetResult2.m_bCapMask) dTargetResult2.m_nHabuConnectedLineCount++;


		AddLogText(L"Connected Count 2 : %d, Habu : %d", dTargetResult2.m_nConnectedLineCount, dTargetResult2.m_nHabuConnectedLineCount);

		if (m_dDefectTargetInfo.m_bConnectedVertically) {
			if (dTargetResult2.m_nConnectedLineCount >= 1) {
				if (nDefectIndex == DEFECT_DEPO_OPEN && (!dTargetResult.m_bHoleMask && !dTargetResult2.m_bHoleMask && (dTargetResult2.m_bSDLineMask || dTargetResult2.m_bInitialLineMask))) {
					nDefectIndex = DEFECT_DEPO_OPEN;	// keep DEPO_OPEN
				}
				else if (dTargetResult2.m_bSDLineMask || dTargetResult2.m_bELVDDLineMask) {
					nDefectIndex = DEFECT_LINE_SHORT;
				}
				else if (dTargetResult.m_bCirclePatternMask || dTargetResult.m_bBridgePatternMask || dTargetResult2.m_bCirclePatternMask || dTargetResult2.m_bBridgePatternMask) {
					nDefectIndex = DEFECT_2_PTN_SHORT;
				}
				else {
					nDefectIndex = DEFECT_LINE_SHORT;
				}
			}
		}
		else {
			if (dTargetResult2.m_nConnectedLineCount >= 1) {
				if (dTargetResult2.m_bSDLineMask) {
					if (dTargetResult.m_bCirclePatternMask || dTargetResult.m_bBridgePatternMask) {
						nDefectIndex = DEFECT_2_PTN_SHORT;
					}
					else {
						nDefectIndex = DEFECT_LINE_SHORT;
					}
				}
				else if (dTargetResult.m_bSDLineMask || dTargetResult.m_bELVDDLineMask || dTargetResult.m_bInitialLineMask
					|| dTargetResult2.m_bELVDDLineMask || dTargetResult2.m_bInitialLineMask || dTargetResult2.m_bCirclePatternMask || dTargetResult2.m_bBridgePatternMask) {
					nDefectIndex = DEFECT_LINE_SHORT;
				}
			}
		}

		if (nDefectIndex == DEFECT_UNKNOWN && (dTargetResult.m_nHabuConnectedLineCount + dTargetResult2.m_nHabuConnectedLineCount) >= 2) {
			nDefectIndex = DEFECT_2_HABU_SHORT;
		}

		if (nDefectIndex == DEFECT_1_HOLE && dTargetResult2.m_bHoleMask) {
			nDefectIndex = DEFECT_2_HOLE;
		}
	}

	AddLogText(L"Check Has Dark Point : %s, Seed : %s", bDark ? L"true" : L"false", bHasSeed ? L"true" : L"false");

	// Post Checking
	if (bDark) {
		if (nDefectIndex == DEFECT_DEPO_OPEN) {
			nDefectIndex = DEFECT_DARK_DEPO;
		}
		else if (nDefectIndex == DEFECT_LINE_SHORT) {
			nDefectIndex = DEFECT_DARK_LINE;
		}
		else if (nDefectIndex == DEFECT_1_PTN_SHORT) {
			nDefectIndex = DEFECT_1_DARK_PTN;
		}
		else if (nDefectIndex == DEFECT_2_PTN_SHORT) {
			nDefectIndex = DEFECT_2_DARK_PTN;
		}
	}
	else if (bHasSeed) {
		if (nDefectIndex == DEFECT_LINE_SHORT) {
			nDefectIndex = DEFECT_LINE_SEED;
		}
		else if (nDefectIndex == DEFECT_1_PTN_SHORT) {
			nDefectIndex = DEFECT_1_SEED_SHORT;
		}
		else if (nDefectIndex == DEFECT_2_PTN_SHORT) {
			nDefectIndex = DEFECT_2_SEED_SHORT;
		}
	}

	m_dDefectTargetInfo.m_nDefectIndex = nDefectIndex;

	return nDefectIndex;
}

bool CAutoRepairTesterDlg::FindTarget()
{
	if (!LoadImage()) {
		AddLogText(L"Fail to find Target. Cannot load Image.");
		return false;
	}
	ShowImage(m_dImg);

	CString strTemplateFile = GetMaskFilePath(MASK_TEMPLATE);
	if (!LoadMask(strTemplateFile, m_dTemplate)) {
		AddLogText(L"Fail to find Target. Cannot load Template Image.");
		return false;
	}

	FitTemplateScale();

	int nImgSizeX = m_dImg.cols;
	int nImgSizeY = m_dImg.rows;
	int nTemplateImgSizeX = m_dTemplate.cols;
	int nTemplateImgSizeY = m_dTemplate.rows;

	Mat dTemplate = m_dTemplate;
	if (m_dImg.channels() != dTemplate.channels()) {
		if (m_dImg.channels() == 3) {
			cvtColor(dTemplate, dTemplate, CV_GRAY2BGR);
		}
		else {
			cvtColor(dTemplate, dTemplate, CV_BGR2GRAY);
		}
	}
	if (m_dImg.type() != dTemplate.type()) {
		dTemplate.convertTo(dTemplate, m_dImg.type());
	}

	Mat dMatchImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	matchTemplate(m_dImg, dTemplate, dMatchImg, CV_TM_CCOEFF_NORMED);

	Point ptMax;
	minMaxLoc(dMatchImg, 0, 0, 0, &ptMax);

	int nTemplatePosX = ptMax.x;
	int nTemplatePosY = ptMax.y;
	while (nTemplatePosX >= 0)  nTemplatePosX -= nTemplateImgSizeX;	// find first pos x
	while (nTemplatePosY >= 0)  nTemplatePosY -= nTemplateImgSizeY; // find first pos y

	m_listTargetInfo.clear();

	NIPLCV *pNIPLCV = NIPLCV::GetInstance();

	int nIndexX = 0;
	int nIndexY = 0;

	for (int nY = nTemplatePosY; nY < nImgSizeY; nY += nTemplateImgSizeY) {
		nIndexX = 0;
		for (int nX = nTemplatePosX; nX < nImgSizeX; nX += nTemplateImgSizeX) {
			Rect rcTarget(nX, nY, nTemplateImgSizeX, nTemplateImgSizeY);
			Point ptStartInTemplate(0, 0);

			if (rcTarget.x < 0) {
				ptStartInTemplate.x = -rcTarget.x;
				rcTarget.width += rcTarget.x;
				rcTarget.x = 0;
			}
			if (rcTarget.y < 0) {
				ptStartInTemplate.y = -rcTarget.y;
				rcTarget.height += rcTarget.y;
				rcTarget.y = 0;
			}
			if (rcTarget.x + rcTarget.width > nImgSizeX) rcTarget.width = nImgSizeX - rcTarget.x;
			if (rcTarget.y + rcTarget.height > nImgSizeY) rcTarget.height = nImgSizeY - rcTarget.y;

			// filtering too small target
			if (rcTarget.width < nTemplateImgSizeX * 0.25f) {
				continue;
			}
			if (rcTarget.height < nTemplateImgSizeY * 0.25f) {
				continue;
			}

			TargetInfo dTargetInfo;
			dTargetInfo.m_rcArea = rcTarget;
			dTargetInfo.m_ptStartPosInTemplate = ptStartInTemplate;
			dTargetInfo.m_bHasDarkPoint = false;
			dTargetInfo.m_nIndexX = nIndexX;
			dTargetInfo.m_nIndexY = nIndexY;

			m_listTargetInfo.push_back(dTargetInfo);

			nIndexX++;
		}
		nIndexY++;
	}

	if (m_listTargetInfo.size() > 0) {
		return true;
	}

	return false;
}

bool CAutoRepairTesterDlg::FindDefect()
{
	UpdateData();

	int nImgSizeX = m_dImg.cols;
	int nImgSizeY = m_dImg.rows;

	/////////////정의천////////////
	int icnt = 0;
	CString strPath;
	//2016.03.24 정의천
	int nColorTH_R = GetDlgItemInt(IDC_EDIT_COLOR_TH_R);
	int nColorTH_G = GetDlgItemInt(IDC_EDIT_COLOR_TH_G);
	int nColorTH_B = GetDlgItemInt(IDC_EDIT_COLOR_TH_B);
	int nSizeHVRate = GetDlgItemInt(IDC_EDIT_HV_RATE);
	////////////////////////////
	m_dResultImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);
	m_dResultForMaskingImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	vector<TargetInfo> listTargetInfo2;

	NIPLCV *pNIPLCV = NIPLCV::GetInstance();

	bool bFoundDefect = false;
	for (auto dTargetInfo : m_listTargetInfo) {
		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTarget(m_dImg, rcTarget);

		int nTargetImgSizeX = dTarget.cols;
		int nTargetImgSizeY = dTarget.rows;

		Mat dTemplate(m_dTemplate, Rect(ptStartInTemplate.x, ptStartInTemplate.y, nTargetImgSizeX, nTargetImgSizeY));

		NIPLInput dInput;
		NIPLOutput dOutput;

		NIPLParam_Diff dParam_Diff;
		dParam_Diff.m_dTargetImg = dTarget;
		dParam_Diff.m_nMaskSize = 1;
	//	dParam_Diff.m_bUseGpu = true;

		dInput.m_dImg = dTemplate;
		dInput.m_pParam = &dParam_Diff;
		NIPL_ERR nErr = pNIPLCV->Diff(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return false;
		}
		Mat dDiff = dOutput.m_dImg;
		/////////////정의천////////////////////		
	/*	strPath.Format(L"D:\\saveImage\\Template%d.bmp", icnt);
		nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dTemplate);
		strPath.Format(L"D:\\saveImage\\Diff%d.bmp", icnt);
		nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dDiff);
		strPath.Format(L"D:\\saveImage\\Target%d.bmp", icnt);
		nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dTarget);
		icnt++;*/

		///////////////////////////////////////
		Mat dTargetResult;
		switch (m_nTypeIndex) {
		case TYPE_ACTIVE: dTargetResult = FindDefect_Active(dTemplate, dDiff, ptStartInTemplate); break;
		case TYPE_GATE: dTargetResult = FindDefect_Gate(dTemplate, dDiff, ptStartInTemplate); break;
		case TYPE_SD: dTargetResult = FindDefect_SD(dTemplate, dDiff, ptStartInTemplate); break;
		}
		// Find Dark Point (Seed)
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_ColorThresholding dParam_ColorThresholding;

		//2016.03.22 정의천 Threshold 값 변경
		dParam_ColorThresholding.m_nMethod_R = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_R = (float)nColorTH_R;// 10;// 50;
		dParam_ColorThresholding.m_nMethod_G = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_G = (float)nColorTH_G;// 10;// 50;
		dParam_ColorThresholding.m_nMethod_B = NIPLParam_ColorThresholding::METHOD_LOWER;
		dParam_ColorThresholding.m_nLowerValue_B = (float)nColorTH_B;// 10;// 50;
		//2016.03.22 정의천

		dInput.m_dImg = dTarget;
		dInput.m_pParam = &dParam_ColorThresholding;
		nErr = pNIPLCV->ColorThresholding(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		Mat dDarkPointResult = dOutput.m_dImg;
////////////////////////
		//strPath.Format(L"D:\\saveImage\\dTarget%d.bmp", icnt);
		//nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dTarget);

		//strPath.Format(L"D:\\saveImage\\dTargetResult%d.bmp", icnt);
		//nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dTargetResult);

		//strPath.Format(L"D:\\saveImage\\dDarkPointResult%d.bmp", icnt);
		//nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dDarkPointResult);

		//icnt++;
		/////////////////////////////////////////////////
		//2016.03.22 정의천 사이즈 필터링	
		dInput.Clear();
		dOutput.Clear();
		NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;

		dInput.m_dImg = dTargetResult;
		dInput.m_pParam = &dParam_FindBlob;
		nErr = pNIPLCV->FindBlob(&dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
			if (dOutput.m_pResult != NULL)
			{
				for (auto dBlob : pResult_FindBlob->m_listBlob) {
					Rect rcBox = dBlob.m_rcBoundingBox;
					if (rcBox.width < 6 || rcBox.height < 6 || dBlob.m_nSize < m_nDefectMinSize || ((rcBox.width / rcBox.height) >nSizeHVRate) || ((rcBox.height / rcBox.width) >nSizeHVRate))
					{
						Mat dTemp(dTargetResult, rcBox);
						dTemp = 0;
					}
				}
				delete pResult_FindBlob;
			}
		}

		dInput.Clear();
		dOutput.Clear();
		//NIPLParam_FindBlob dParam_FindBlob;
		dParam_FindBlob.m_bFindInRange = true;

		dInput.m_dImg = dDarkPointResult;
		dInput.m_pParam = &dParam_FindBlob;
		nErr = pNIPLCV->FindBlob(&dInput, &dOutput);
		if (NIPL_SUCCESS(nErr)) {
			NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
			if (dOutput.m_pResult != NULL)
			{
				for (auto dBlob2 : pResult_FindBlob->m_listBlob) {
					Rect rcBox = dBlob2.m_rcBoundingBox;
					if (rcBox.width < 6 || rcBox.height < 6 || dBlob2.m_nSize < m_nSeedMinSize || ((rcBox.width / rcBox.height) > 10) || ((rcBox.height / rcBox.width) > 10))
					{
						Mat dTemp(dDarkPointResult, rcBox);
						dTemp = 0;
					}
				}
				delete pResult_FindBlob;
			}
			////2016.03.22 정의천 필터링...
			///////////////////////////////////////////////////
		}

		int nResultSize = countNonZero(dTargetResult);
		int nDarkPointSize = countNonZero(dDarkPointResult);

		if (nResultSize > 0 || nDarkPointSize > 0) {
			if (nDarkPointSize >= nResultSize) {	
				dTargetResult = dDarkPointResult;

				Mat dTargetResultImg(m_dResultImg, rcTarget);
				dTargetResult.copyTo(dTargetResultImg);


				dTargetResultImg = Mat(m_dResultForMaskingImg, rcTarget);
				dTargetResult.copyTo(dTargetResultImg);
			}
			else {			// if it's a seed
				Mat dTargetResultImg(m_dResultImg, rcTarget);
				dTargetResult.copyTo(dTargetResultImg);

				dTargetResult = dTargetResult - dDarkPointResult;

				dTargetResultImg = Mat(m_dResultForMaskingImg, rcTarget);
				dTargetResult.copyTo(dTargetResultImg);
			}

			//strPath.Format(L"D:\\saveImage\\m_dResultImg%d.bmp", icnt);
			//nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), m_dResultImg);
			//icnt++;

			if (nDarkPointSize > 0) {
				float nDarkPointRatio = (float)nDarkPointSize / nResultSize;
				if (nDarkPointRatio < 0.8f) {
					dTargetInfo.m_bHasSeed = true;
					dTargetInfo.m_dTargetResult.m_bHasSeed = true;

				}
				else {
					dTargetInfo.m_bHasDarkPoint = true;
					dTargetInfo.m_dTargetResult.m_bDark = true;
				}
			}

			bFoundDefect = true;

			listTargetInfo2.push_back(dTargetInfo);

			pNIPLCV->DebugPrint("Add Defect Target (%d, %d), Pos : (%d, %d) ~ (%d, %d), Size : %d, DarkSize : %d", 
				dTargetInfo.m_nIndexX, dTargetInfo.m_nIndexY, dTargetInfo.m_rcArea.x, dTargetInfo.m_rcArea.y, 
				dTargetInfo.m_rcArea.x + dTargetInfo.m_rcArea.width, dTargetInfo.m_rcArea.y + dTargetInfo.m_rcArea.height,
				nResultSize, nDarkPointSize);
		}
	}

	//NIPLCV *pNIPLCV = NIPLCV::GetInstance();
	//strPath.Format(L"D:\\saveImage\\dTargetResult%d.bmp", icnt);
	//NIPL_ERR nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), m_dResultImg);
	//icnt++;

	m_dDefectTargetInfo.Clear();
	int nCount = listTargetInfo2.size();
	for (int i = 0; i < nCount; i++) {
		TargetInfo &dTargetInfo = listTargetInfo2[i];
		pNIPLCV->DebugPrint("Check Defect Target (%d) (%d, %d), Set : %s", i,
			dTargetInfo.m_nIndexX, dTargetInfo.m_nIndexY, dTargetInfo.m_bSetToDefectTarget ? "true" : "false");

		if (dTargetInfo.m_bSetToDefectTarget) {
			continue;
		}
		dTargetInfo.m_bSetToDefectTarget = true;

		DefectTargetInfo dDefectTargetInfo;
		dDefectTargetInfo.m_dTargetInfo = dTargetInfo;
		
		for (int j = i + 1; j < nCount; j++) {
			TargetInfo &dTargetInfo2 = listTargetInfo2[j];
			pNIPLCV->DebugPrint("  Check Defect Target 2 (%d) (%d, %d), Set : %s", j,
				dTargetInfo2.m_nIndexX, dTargetInfo2.m_nIndexY, dTargetInfo2.m_bSetToDefectTarget ? "true" : "false");

			if (dTargetInfo2.m_bSetToDefectTarget) {
				continue;
			}

			if ((dTargetInfo2.m_nIndexY == dTargetInfo.m_nIndexY && dTargetInfo2.m_nIndexX - 1 == dTargetInfo.m_nIndexX)) {
				dDefectTargetInfo.m_bConnectedTarget = true;
				if (m_nImageRotation == IMAGE_ROTATION_90 || m_nImageRotation == IMAGE_ROTATION_270) {
					dDefectTargetInfo.m_bConnectedVertically = true;
				}
			}
			else if (dTargetInfo2.m_nIndexX == dTargetInfo.m_nIndexX && dTargetInfo2.m_nIndexY - 1 == dTargetInfo.m_nIndexY) {
				dDefectTargetInfo.m_bConnectedTarget = true;
				if (m_nImageRotation == IMAGE_ROTATION_0 || m_nImageRotation == IMAGE_ROTATION_180) {
					dDefectTargetInfo.m_bConnectedVertically = true;
				}
			}

			if (dDefectTargetInfo.m_bConnectedTarget) {
				dTargetInfo2.m_bSetToDefectTarget = true;
				dDefectTargetInfo.m_dTargetInfo2 = dTargetInfo2;

				pNIPLCV->DebugPrint("  => Selected Target 2");
				break;
			}
		}

		Point ptDefectTargetCenterPos = m_dDefectTargetInfo.GetCenterPos();
		Point ptTargetCenterPos = dDefectTargetInfo.GetCenterPos();

		float nDistDefectTargetCenterPos = CALC_DISTANCE(ptDefectTargetCenterPos, CPoint(nImgSizeX * 0.5f, nImgSizeY * 0.5f));
		float nDistTargetCenterPos = CALC_DISTANCE(ptTargetCenterPos, CPoint(nImgSizeX * 0.5f, nImgSizeY * 0.5f));

		pNIPLCV->DebugPrint("Check Defect Target (%d, %d), Connected : %s, Dist : %.3f < %.3f, CurPos : (%d, %d), DefectPos : (%d, %d) CenterPos : (%d, %d)",
			dTargetInfo.m_nIndexX, dTargetInfo.m_nIndexY, dDefectTargetInfo.m_bConnectedTarget ? "true" : "false",
			nDistTargetCenterPos, nDistDefectTargetCenterPos, ptTargetCenterPos.x, ptTargetCenterPos.y, 
			ptDefectTargetCenterPos.x, ptDefectTargetCenterPos.y, (int)(nImgSizeX * 0.5f), (int)(nImgSizeY * 0.5f));


		if (nDistTargetCenterPos < nDistDefectTargetCenterPos) {
			pNIPLCV->DebugPrint("  => Selected");

			m_dDefectTargetInfo = dDefectTargetInfo;
		}
	}

	m_listTargetInfo.clear();
	m_listTargetInfo.push_back(m_dDefectTargetInfo.m_dTargetInfo);
	if (m_dDefectTargetInfo.m_bConnectedTarget) {
		m_listTargetInfo.push_back(m_dDefectTargetInfo.m_dTargetInfo2);
	}

	return bFoundDefect;
}

Mat CAutoRepairTesterDlg::FindDefect_Active(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate)
{
	Mat dResultImg;
	for (int i = MASK_LAYER_1; i <= MASK_LAYER_2; i++) {
		Mat dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, i);
		if (CHECK_EMPTY_IMAGE(dResultImg)) {
			dResultImg = dResult_Layer;
		}
		else {
			dResultImg |= dResult_Layer;
		}
	}

	return dResultImg;
}

Mat CAutoRepairTesterDlg::FindDefect_Gate(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate)
{
	Mat dResultImg;
	for (int i = MASK_LAYER_1; i <= MASK_LAYER_3; i++) {
		Mat dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, i);
		if (CHECK_EMPTY_IMAGE(dResultImg)) {
			dResultImg = dResult_Layer;
		}
		else {
			dResultImg |= dResult_Layer;
		}
	}

	return dResultImg;
}

Mat CAutoRepairTesterDlg::FindDefect_SD(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate)
{
	Mat dResultImg;
	dResultImg = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate);

/*
	Mat dSD_All = GetMergeMask(MASK_SD_SD_LINE, MASK_SD_BRIDGE_PATTERN);
	Mat dSD_BG = ~dSD_All;
	Mat dGate_All = GetMergeMask(MASK_GATE_EMISSION_LINE, MASK_GATE_CAP);
	Mat dSDGate_BG = ~(dSD_All | dGate_All);
	Mat dActiveMask = GetMask(MASK_ACTIVE_PATTERN);
	Mat dBG = ~(dSD_All | dGate_All | dActiveMask);

	Mat dResultImg;
	Mat dResult_Layer;
	for (int i = MASK_SD_SD_LINE; i <= MASK_SD_BRIDGE_PATTERN; i++) {
		dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, i);
		if (CHECK_EMPTY_IMAGE(dResultImg)) {
			dResultImg = dResult_Layer;
		}
		else {
			dResultImg |= dResult_Layer;
		}
	}

	for (int i = MASK_GATE_EMISSION_LINE; i <= MASK_GATE_CAP; i++) {
		dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, i, dSD_BG);
		if (CHECK_EMPTY_IMAGE(dResultImg)) {
			dResultImg = dResult_Layer;
		}
		else {
			dResultImg |= dResult_Layer;
		}
	}

	dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, MASK_ACTIVE_PATTERN, dSDGate_BG);
	if (CHECK_EMPTY_IMAGE(dResultImg)) {
		dResultImg = dResult_Layer;
	}
	else {
		dResultImg |= dResult_Layer;
	}

	dResult_Layer = ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, dBG);
	if (CHECK_EMPTY_IMAGE(dResultImg)) {
		dResultImg = dResult_Layer;
	}
	else {
		dResultImg |= dResult_Layer;
	}
*/

	return dResultImg;
}

bool CAutoRepairTesterDlg::CheckMask(int nMaskIndex, int nDefectMinSize, bool bCheckedConnected)
{
	UpdateData();

	CString strMaskFile = GetMaskFilePath(nMaskIndex);

	Mat dMask;
	if (!LoadMask(strMaskFile, dMask)) {
		AddLogText(L"Fail to check Mask. Cannot load Mask Layer %d.", nMaskIndex);
		return false;
	}

	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;
	NIPLCV *pNIPLCV = NIPLCV::GetInstance();

	int nTargetImgSizeX = m_dTargetResult.cols;
	int nTargetImgSizeY = m_dTargetResult.rows;

	if (nDefectMinSize == 0) {
		nDefectMinSize = m_nDefectMinSize;
	}

	Mat dTargetResult = m_dTargetResult;
	if (bCheckedConnected) {
		// Extend Defect Size
		dInput.Clear();
		dOutput.Clear();

		NIPLParam_MorphologyOperate dParam_MorphologyOperate;
		dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
		dParam_MorphologyOperate.m_bCircleFilter = true;
		dParam_MorphologyOperate.m_nFilterSizeX = 15;
		dParam_MorphologyOperate.m_nFilterSizeY = 15;

		dInput.m_dImg = dTargetResult;
		dInput.m_pParam = &dParam_MorphologyOperate;
		nErr = pNIPLCV->MorphologyOperate(&dInput, &dOutput);
		if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
			return nErr;
		}
		dTargetResult = dOutput.m_dImg;

		// reset minimum size
		nDefectMinSize = 1;
	}

	Mat dTargetMask(dMask, Rect(m_ptStartInTemplate.x, m_ptStartInTemplate.y, nTargetImgSizeX, nTargetImgSizeY));
	Mat dBinImg = dTargetMask & dTargetResult;

	// FindBlob
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_nMinSize = (float)nDefectMinSize;
	dParam_FindBlob.m_bFindInRange = true;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_FindBlob;

	nErr = pNIPLCV->FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return false;
	}

	if (dOutput.m_pResult == nullptr) {
		return false;
	}
	else {
		delete dOutput.m_pResult;
	}

	return true;
}

Mat CAutoRepairTesterDlg::ApplyMask(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate, int nMaskIndex, Mat dFilterMask)
{
	CString strMaskFile = GetMaskFilePath(nMaskIndex);
	Mat dMask;
	if (!LoadMask(strMaskFile, dMask)) {
		AddLogText(L"Fail to check Mask. Cannot load Mask Layer %d.", nMaskIndex);

		int nTargetImgSizeX = dDiff.cols;
		int nTargetImgSizeY = dDiff.rows;
		Mat dResultImg = Mat::zeros(nTargetImgSizeY, nTargetImgSizeX, CV_8UC1);
		return dResultImg;
	}

	return ApplyMask(dTemplate, dDiff, ptStartPosInTemplate, dMask, dFilterMask);
}

Mat CAutoRepairTesterDlg::ApplyMask(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate, Mat dMask, Mat dFilterMask)
{
	UpdateData();

	int nTargetImgSizeX = dDiff.cols;
	int nTargetImgSizeY = dDiff.rows;
	Mat dResultImg = Mat::zeros(nTargetImgSizeY, nTargetImgSizeX, CV_8UC1);

	if (CHECK_EMPTY_IMAGE(dMask)) {
		dMask = Mat::ones(m_dTemplate.size(), CV_8UC1);
	}

	if (!CHECK_EMPTY_IMAGE(dFilterMask)) {
		dMask &= dFilterMask;
	}

	Mat dTargetMask(dMask, Rect(ptStartPosInTemplate.x, ptStartPosInTemplate.y, nTargetImgSizeX, nTargetImgSizeY));

	Scalar dMean;
	Scalar dStd;

	meanStdDev(dTemplate, dMean, dStd, dTargetMask);
//	meanStdDev(dTemplate, dMean, dStd);
	float nStd = (float)dStd[0];

	NIPLParam_Thresholding dParam_Thresholding;
	dParam_Thresholding.m_nMethod = NIPLParam_Thresholding::METHOD_UPPER;
	dParam_Thresholding.m_nThreshold = pow(m_nTestBinarizeThreshold, 2.f) * nStd;

	NIPLInput dInput;
	NIPLOutput dOutput;
	dInput.m_dImg = dDiff;
	dInput.m_dMask = dTargetMask;
	dInput.m_pParam = &dParam_Thresholding;

	NIPLCV *pNIPLCV = NIPLCV::GetInstance();
	NIPL_ERR nErr = pNIPLCV->Thresholding(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return dResultImg;
	}
	Mat dBinImg = dOutput.m_dImg;

///////////////
	/**
	CString strPath;
	static int icnt = 0;
	strPath.Format(L"D:\\saveImage\\2DTemplate%d.bmp", icnt);
	nErr = pNIPLCV->SaveImage(strPath.GetBuffer(), dBinImg);
	icnt++;
	*/
//////////////////

/*
	dInput.Clear();
	dOutput.Clear();

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_bFindInRange = true;

	dInput.m_dImg = dBinImg;
	dInput.m_pParam = &dParam_FindBlob;
	nErr = pNIPLCV->FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return dResultImg;
	}

	dResultImg = dOutput.m_dImg;
*/
	dResultImg = dBinImg;

	return dResultImg;
}

bool CAutoRepairTesterDlg::LoadMask(CString strMaskFile, Mat &dMask)
{
	NIPO *pNIPO = NIPO::GetInstance();

	CString strTemplateFileName = GetMaskFilePath(MASK_TEMPLATE);
	bool bTemplate = (strMaskFile.CompareNoCase(strTemplateFileName) == 0);

	Mat dImg;
	auto it = m_mapMask.find(strMaskFile);
	if (it != m_mapMask.end()) {
		dImg = it->second;
	}
	else {
		if (!pNIPO->LoadImage(LPCTSTR(strMaskFile), dImg)) {
			AddLogText(L"Fail to load Mask : %s", LPCTSTR(strMaskFile));
			dMask.release();

			return false;//2016.02.24 jec add
		}
		else {
			AddLogText(L"Success to load Mask : %s", LPCTSTR(strMaskFile));

			if (!bTemplate && dImg.channels() == 3) {
				cvtColor(dImg, dImg, CV_BGR2GRAY);
			}

			m_mapMask[strMaskFile] = dImg;
		}
	}

	dMask = RotateImage(dImg);
	if (!bTemplate && !CHECK_EMPTY_IMAGE(m_dTemplate) && m_dTemplate.size() != dMask.size()) {
		resize(dMask, dMask, m_dTemplate.size());
	}

	return true;
}

bool CAutoRepairTesterDlg::LoadMask()
{
	return LoadMask(m_strMaskFile, m_dMask);
}

void CAutoRepairTesterDlg::OnBnClickedFindDefect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!FindTarget()) {
		AddLogText(L"Cannot Do Test. Fail to find Target.");
	}
}

void CAutoRepairTesterDlg::LoadData()
{
	ChangeType();
}

void CAutoRepairTesterDlg::ChangeType()
{
	UpdateData();

	CString strData;
	switch (m_nTypeIndex) {
	case TYPE_ACTIVE:
		m_nTestBinarizeThreshold = 4.5;
		m_nDefectMinSize = 100;
		m_nHoleMinSize = 0;
		m_nSeedMinSize = 10;
		m_nImageRotation = IMAGE_ROTATION_0;
		strData = L"Active";
		break;
	case TYPE_GATE: 
		m_nTestBinarizeThreshold = 1.9;
		m_nDefectMinSize = 200;
		m_nHoleMinSize = 0;
		m_nSeedMinSize = 10;
		m_nImageRotation = IMAGE_ROTATION_0;
		strData = L"Gate";
		break;
	case TYPE_SD: 
		m_nTestBinarizeThreshold = 1.4;
		m_nDefectMinSize = 100;
		m_nHoleMinSize = 10;
		m_nSeedMinSize = 10;
		m_nImageRotation = IMAGE_ROTATION_180;
		strData = L"SD";
		break;
	}
	UpdateData(FALSE);

	m_strImageFolder = m_strDataFolder + strData + L"\\Image\\";
	m_strMaskFolder = m_strDataFolder + strData + L"\\Mask\\";

	LoadImageList();
	LoadMaskList();

	if (!CHECK_EMPTY_IMAGE(m_dImg) && !CHECK_EMPTY_IMAGE(m_dMask)) {
		UpdateData(FALSE);

		ShowPosBox();
	}
}

void CAutoRepairTesterDlg::LoadImageList()
{
	m_ctlImageList.ResetContent();

	WIN32_FIND_DATA dFindData;
	wstring strFormat = m_strImageFolder;
	strFormat += L"*.*";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	int nFileCount = 0;
	if (hFile != INVALID_HANDLE_VALUE) {
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			if (dFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				continue;
			}

			int nIndex = m_ctlImageList.GetCount();
			nIndex = m_ctlImageList.InsertString(nIndex, dFindData.cFileName);
		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);
	}

	int nCount = m_ctlImageList.GetCount();
	if (nCount > 0) {
		AddLogText(L"Success to load Image List , Count : %d", nCount);
		m_ctlImageList.SetCurSel(0);
		ChangeImage();
	}
	else {
		AddLogText(L"Fail to load Image List, Count : %d", nCount);
	}
}

void CAutoRepairTesterDlg::LoadMaskList()
{
	m_mapMask.clear();
	m_mapMaskFileName.clear();
	m_ctlMaskList.ResetContent();

	WIN32_FIND_DATA dFindData;
	wstring strFormat = m_strMaskFolder;
	strFormat += L"*.*";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	int nFileCount = 0;
	if (hFile != INVALID_HANDLE_VALUE) {
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			if (dFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				continue;
			}

			CString strFullFileName = dFindData.cFileName;
			CString strFileName;
			wchar_t *pCut = wcsrchr(dFindData.cFileName, '.');
			if (pCut) {
				wchar_t *pExt = pCut + 1;
				if (!CHECK_STRING(pExt, L"bmp") && !CHECK_STRING(pExt, L"jpg")) {
					continue;
				}

				*pCut = 0x00;
				strFileName = dFindData.cFileName;
			}
			else {
				continue;
			}

			int nIndex = m_ctlMaskList.GetCount();
			nIndex = m_ctlMaskList.InsertString(nIndex, strFileName);
			
			m_mapMaskFileName[strFileName.MakeLower()] = strFullFileName;

		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);
	}

	int nCount = m_ctlMaskList.GetCount();
	if (nCount > 0) {
		AddLogText(L"Success to load Image List , Count : %d", nCount);

		m_ctlMaskList.SetCurSel(nCount - 1);
		ChangeMask();
	}
	else {
		AddLogText(L"Fail to load Image List, Count : %d", nCount);
	}
}


void CAutoRepairTesterDlg::ChangeImage()
{
	int nIndex = m_ctlImageList.GetCurSel();
	if (nIndex < 0) {
		AddLogText(L"Fail to change Image. No Image selected.");
		return;
	}

	CString strFileName;
	m_ctlImageList.GetText(nIndex, strFileName);

	m_strImageFile = m_strImageFolder + strFileName;
	m_nProcessStep = STEP_LOAD_IMAGE;

	LoadImage();
	ShowImage(m_dImg);
	ShowPosBox();
}

void CAutoRepairTesterDlg::ChangeMask()
{
	int nIndex = m_ctlMaskList.GetCurSel();
	if (nIndex < 0) {
		AddLogText(L"Fail to change Mask. No Mask selected.");
		return;
	}

	CString strFileName;
	m_ctlMaskList.GetLBText(nIndex, strFileName);

	m_strMaskFile = GetMaskFilePath(strFileName);

	LoadMask();
	ShowMask();
}

void CAutoRepairTesterDlg::OnSelchangeImageList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeImage();
}

void CAutoRepairTesterDlg::OnSelchangeMaskList()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeMask();
}

CString CAutoRepairTesterDlg::GetMaskFilePath(int nMaskIndex)
{
	CString strFileName;
	switch (nMaskIndex) {
	case MASK_TEMPLATE: strFileName = STR_MASK_TEMPLATE; break;
	default:
		strFileName = STR_MASK_LAYER;
		strFileName += to_wstring(nMaskIndex).c_str();
		break;
	}

	return GetMaskFilePath(strFileName);
}

CString CAutoRepairTesterDlg::GetMaskFilePath(CString strFileName)
{
	auto it = m_mapMaskFileName.find(strFileName.MakeLower());
	if (it == m_mapMaskFileName.end()) {
		return L"";
	}
	else {
		strFileName = it->second;
	}

	CString strMaskFile = m_strMaskFolder + strFileName;

	return strMaskFile;
}


void CAutoRepairTesterDlg::OnBnClickedImageReload()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nProcessStep = STEP_LOAD_IMAGE;

	if (!LoadImage()) {
		AddLogText(L"Fail to find Target. Cannot load Image.");
		return;
	}
	ShowImage(m_dImg);
	ShowPosBox();
}

bool CAutoRepairTesterDlg::IsRectOverlap(Rect rc1, Rect rc2)
{
	if (rc1.x + rc1.width < rc2.x) return false;
	if (rc1.x > rc2.x + rc2.width) return false;
	if (rc1.y + rc1.height < rc2.y) return false;
	if (rc1.y > rc2.y + rc2.height) return false;

	return true;
}


void CAutoRepairTesterDlg::OnBnClickedFocusStartPos()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnBnClickedImageReload();
}


void CAutoRepairTesterDlg::OnBnClickedFocusEndPos()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	OnBnClickedImageReload();
}


void CAutoRepairTesterDlg::OnBnClickedClassify()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!ClassifyDefect()) {
		AddLogText(L"Fail to classify Defect.");
	}
	else {
		AddLogText(L"Success to classify Defect.");

		if (m_bShowDetectWithMask) {
			ShowImageWithMask(m_dResultImg);
		}
		else {
			ShowImage(m_dResultImg);
		}

		m_nProcessStep = STEP_DETECT;
		ShowPosBox(); // 불량이 이 있는 섹터
	}
}

void CAutoRepairTesterDlg::OnBnClickedDraw()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!ClassifyDefect()) {
		AddLogText(L"Fail to classify Defect.");
		return;
	}
	else {
		AddLogText(L"Success to classify Defect.");
	}

	if (!DrawDefect()) {
		AddLogText(L"Fail to draw Defect.");
		return;
	}
	else {
		AddLogText(L"Success to draw Defect.");
	}
}

bool CAutoRepairTesterDlg::DrawDefect()
{
	UpdateData();

	m_nProcessStep = STEP_DRAW;
	ShowPosBox();

	m_dImg.copyTo(m_dDrawImg);

	bool bSuccess = false;
	DEFECT_INDEX nDefectIndex = DEFECT_UNKNOWN;
	switch (m_nTypeIndex) {
	case TYPE_ACTIVE: bSuccess = DrawDefect_Active(); break;
	case TYPE_GATE: bSuccess = DrawDefect_Gate(); break;
	case TYPE_SD: bSuccess = DrawDefect_SD(); break;
	}

	if (bSuccess) {
		ShowImage(m_dDrawImg);
	}

	return bSuccess;
}

bool CAutoRepairTesterDlg::DrawDefect_Active()
{
	Mat dBoundingBox = GetDefectBoundingBoxMask(50);

	switch (m_nDefectIndex) {
	case DEFECT_1_PTN_SHORT: 
	case DEFECT_2_PTN_SHORT: 
	case DEFECT_1_SEED_SHORT: 
	case DEFECT_2_SEED_SHORT:
		{
			DrawDefect_Blob(m_dDrawImg, dBoundingBox, CV_RGB(0, 255, 0), 0.4f);
			DrawDefect_Defect(CV_RGB(255, 0, 0), 0.4f);
	}
		break;
	case DEFECT_PARTICLE:
	case DEFECT_2_PTN_PARTICLE:
		{
			DrawDefect_LayerCut(ACTIVE_DRAW_MASK_LAYER_CUT, CV_RGB(255, 0, 0), 0.8f);
			DrawDefect_ParcleCut(ACTIVE_DRAW_MASK_PARTICLE_CUT, dBoundingBox, CV_RGB(255, 0, 0), 0.8f);
		}
		break;
	}

	return true;
}

bool CAutoRepairTesterDlg::DrawDefect_Gate()
{
	Mat dBoundingBox = GetDefectBoundingBoxMask(50);

	switch (m_nDefectIndex) {
		case DEFECT_GATE_DEPO_OPEN:
			//DrawDefect_LayerCut(GATE_DRAW_MASK_LAYER_CUT, CV_RGB(255, 0, 0), 0.8f);
			DrawDefect_ParcleCut(GATE_DRAW_MASK_LINE_OPEN, dBoundingBox, CV_RGB(0, 0, 255), 0.8f);
			break;
		case DEFECT_GATE_2_DARK_PTN:
			DrawDefect_ParcleCut(GATE_DRAW_MASK_DARK_2PTN, dBoundingBox, CV_RGB(255, 0, 0), 0.8f);
			break;
/*
		case DEFECT_GATE_2_PTN_SHORT:
		{
			DrawDefect_LayerCut(GATE_DRAW_MASK_LAYER_CUT, CV_RGB(255, 0, 0), 0.8f);
			DrawDefect_ParcleCut(GATE_DRAW_MASK_PARTICLE_CUT, dBoundingBox, CV_RGB(255, 0, 0), 0.8f);
		}	break;
*/
		case DEFECT_GATE_1_HABU_PTN_SHORT:
		case DEFECT_GATE_2_HABU_PTN_SHORT:
		{
			DrawDefect_Blob(m_dDrawImg, dBoundingBox, CV_RGB(0, 255, 0), 0.4f);
			DrawDefect_Defect(CV_RGB(255, 0, 0), 0.4f);
		} break;
		case DEFECT_GATE_DARK_DEPO:
			DrawDefect_ParcleCut(GATE_DRAW_MASK_DARK_DEPO, dBoundingBox, CV_RGB(255, 0, 0), 0.8f);
			break;
		case DEFECT_2_SEED_SHORT:
		case DEFECT_GATE_2_PTN_ERR:
//			break;
		case DEFECT_GATE_LINE_SHORT:
//			break;
		case DEFECT_GATE_1_HOLE:

		case DEFECT_GATE_THIN:
		default:
		{
			DrawDefect_Blob(m_dDrawImg, dBoundingBox, CV_RGB(0, 255, 0), 0.4f);
			DrawDefect_Mask(MASK_LAYER_3, dBoundingBox, CV_RGB(0, 0, 255), 0.4f);
		}
		break;
	}

	return true;
}

bool CAutoRepairTesterDlg::DrawDefect_SD()
{
	Mat dBoundingBox = GetDefectBoundingBoxMask(10);
	int nDrawMask;

	switch (m_nDefectIndex) {
	case DEFECT_1_PTN_SHORT:
	case DEFECT_2_PTN_SHORT:
	case DEFECT_1_SEED_SHORT:
	case DEFECT_2_SEED_SHORT:
	case DEFECT_LINE_SHORT:
	case DEFECT_LINE_SEED:
		//DrawDefect_LayerCut(GATE_DRAW_MASK_LAYER_CUT, CV_RGB(255, 0, 0), 0.8f);
		CheckDrawMaskIndex();
		DrawDefect_Cut(dBoundingBox, CV_RGB(255, 0, 0), 0.8f);
		break;
	case DEFECT_DEPO_OPEN:
	{
		TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
		TargetResult &dTargetResult = dTargetInfo.m_dTargetResult;
		int nMaskIndex = dTargetResult.m_bSDLineMask ? MASK_SD_SD_LINE : MASK_SD_INITIAL_LINE;
		DrawDefect_Defect(CV_RGB(128, 128, 128), 0.9f, nMaskIndex);
	}
	break;
	case DEFECT_1_HOLE:
	case DEFECT_2_HOLE:
		DrawDefect_Defect(CV_RGB(255, 0, 0), 0.9f, MASK_SD_HOLE);
		break;

	default:
		return false;
	}

	return true;
}

void CAutoRepairTesterDlg::CheckDrawMaskIndex()
{
	int nDrawMaskIndex = 0;
	switch (m_nDefectIndex) {
	case DEFECT_1_PTN_SHORT:
	case DEFECT_2_PTN_SHORT:
	case DEFECT_1_SEED_SHORT:
	case DEFECT_2_SEED_SHORT:
	case DEFECT_LINE_SHORT:
	case DEFECT_LINE_SEED:
	{
			TargetInfo &dTargetInfo = m_dDefectTargetInfo.m_dTargetInfo;
			TargetResult &dTargetResult = dTargetInfo.m_dTargetResult;
			SET_TARGET_DRAW_MASK_INDEX(dTargetResult);

			if (m_dDefectTargetInfo.m_bConnectedTarget) {
				TargetInfo &dTargetInfo2 = m_dDefectTargetInfo.m_dTargetInfo2;
				TargetResult &dTargetResult2 = dTargetInfo2.m_dTargetResult;

				SET_TARGET_DRAW_MASK_INDEX(dTargetResult2);
				SET_TARGET_DRAW_MASK_INDEX_2(dTargetResult, dTargetResult2, m_dDefectTargetInfo.m_bConnectedVertically);
			}
		}

		break;
	}
}


Mat CAutoRepairTesterDlg::GetDefectBoundingBoxMask(int nExtendSize)
{
	int nImgSizeX = m_dDrawImg.cols;
	int nImgSizeY = m_dDrawImg.rows;

	Mat dBox = Mat::zeros(nImgSizeY, nImgSizeX, CV_8UC1);

	NIPLInput dInput;
	NIPLOutput dOutput;

	NIPLParam_FindBlob dParam_FindBlob;
	dParam_FindBlob.m_bFindInRange = true;

	dInput.m_dImg = m_dResultImg;
	dInput.m_pParam = &dParam_FindBlob;

	NIPLCV *pNIPLCV = NIPLCV::GetInstance();
	NIPL_ERR nErr = pNIPLCV->FindBlob(&dInput, &dOutput);
	if (NIPL_FAIL(nErr) || NIPL_PASS(nErr)) {
		return dBox;
	}

	if (dOutput.m_pResult) {
		int nImgSizeX = m_dImg.cols;
		int nImgSizeY = m_dImg.rows;

		int nStartX = nImgSizeX;
		int nStartY = nImgSizeY;
		int nEndX = -1;
		int nEndY = -1;

		NIPLResult_FindBlob *pResult_FindBlob = (NIPLResult_FindBlob *)dOutput.m_pResult;
		for (auto dBlob : pResult_FindBlob->m_listBlob) {
			Rect rcBox = dBlob.m_rcBoundingBox;

			//2016.03.22 정의천 사이즈 필터링
			if (rcBox.width < 6 || rcBox.height < 6)
				continue;
			if (rcBox.x < 50 || rcBox.y <100)
				continue;
			//2016.03.22 정의천

			if (rcBox.x < nStartX) {
				nStartX = rcBox.x;
			}
			if (rcBox.y < nStartY) {
				nStartY = rcBox.y;
			}
			if (rcBox.x + rcBox.width > nEndX) {
				nEndX = rcBox.x + rcBox.width;
			}
			if (rcBox.y + rcBox.height > nEndY) {
				nEndY = rcBox.y + rcBox.height;
			}
		}

		delete pResult_FindBlob;
		Rect rcTemp(nStartX, nStartY, nEndX - nStartX, nEndY - nStartY);
		m_rcBox = rcTemp;

		nStartX -= nExtendSize;
		nStartY -= nExtendSize;
		nEndX += nExtendSize;
		nEndY += nExtendSize;

		if (nStartX < 0) nStartX = 0;
		if (nStartY < 0) nStartY = 0;
		if (nEndX > nImgSizeX - 1) nEndX = nImgSizeX - 1;
		if (nEndY > nImgSizeY - 1) nEndY = nImgSizeY - 1;
		
		m_rcExtBox = Rect(nStartX, nStartY, nEndX - nStartX, nEndY - nStartY);
		Mat dBoxArea(dBox, m_rcExtBox);
		dBoxArea = 1;
	}

	return dBox; 
}

void CAutoRepairTesterDlg::DrawDefect_Mask(int nMaskIndex, Mat dBoundingBox, Scalar dColor, float nAlpha)
{
	Mat dMask = GetMask(nMaskIndex);
	if (CHECK_EMPTY_IMAGE(dMask)) {
		return;
	}

	if (CHECK_EMPTY_IMAGE(dBoundingBox)) {
		dBoundingBox = Mat::ones(m_dDrawImg.size(), CV_8UC1);
	}

	int nCount = m_listTargetInfo.size();
	for (int i = 0; i < nCount; i++) {
		TargetInfo dTargetInfo = m_listTargetInfo[i];

		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTarget(m_dDrawImg, rcTarget);
		Mat dTargetBoundingBox(dBoundingBox, rcTarget);
		Mat dTargetMask(dMask, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));

		Mat dDrawMask = dTargetMask & dTargetBoundingBox;

		DrawDefect_TargetBlob(dTarget, dDrawMask, dColor, nAlpha);
	}
}

void CAutoRepairTesterDlg::DrawDefect_Defect(Scalar dColor, float nAlpha, int nOverlappedMaskIndex)
{
	Mat &dImg = m_dDrawImg;

	Mat dMask;
	if (nOverlappedMaskIndex >= 0) {
		dMask = GetMask(nOverlappedMaskIndex);
	}

	for (auto dTargetInfo : m_listTargetInfo) {
		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTarget(dImg, rcTarget);
		Mat dTargetBlob(m_dResultImg, rcTarget);

		if (!CHECK_EMPTY_IMAGE(dMask)) {
			NIPLCV *pNIPLCV = NIPLCV::GetInstance();
			NIPLInput dInput;
			NIPLOutput dOutput;

			NIPLParam_MorphologyOperate dParam_MorphologyOperate;
			dParam_MorphologyOperate.m_nMethod = NIPLParam_MorphologyOperate::METHOD_DILATE;
			dParam_MorphologyOperate.m_nFilterSizeX = 15;
			dParam_MorphologyOperate.m_nFilterSizeY = 15;

			dInput.m_dImg = dTargetBlob;
			dInput.m_pParam = &dParam_MorphologyOperate;
			NIPL_ERR nErr = pNIPLCV->MorphologyOperate(&dInput, &dOutput);
			if (NIPL_SUCCESS(nErr)) {
				Mat dTargetMask = dMask;
				if (dOutput.m_dImg.size() != dMask.size()) {
					dTargetMask = Mat(dMask, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));
				}

				dTargetBlob = dOutput.m_dImg & dTargetMask;
			}
		}

		DrawDefect_TargetBlob(dTarget, dTargetBlob, dColor, nAlpha);
	}
}

void CAutoRepairTesterDlg::DrawDefect_Blob(Mat &dImg, Mat dBlob, Scalar dColor, float nAlpha)
{
	bool bFullImg = false;
	if (dImg.size == dBlob.size) {
		bFullImg = true;
	}

	for (auto dTargetInfo : m_listTargetInfo) {
		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTarget(dImg, rcTarget);
		Mat dTargetBlob;
		if (bFullImg) {
			dTargetBlob = Mat(dBlob, rcTarget);
		}
		else {
			dTargetBlob = Mat(dBlob, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));
		}

		DrawDefect_TargetBlob(dTarget, dTargetBlob, dColor, nAlpha);
	}
}

void CAutoRepairTesterDlg::DrawDefect_TargetBlob(Mat &dImg, Mat dBlob, Scalar dColor, float nAlpha)
{
	Mat dColorData(1, 1, CV_8UC3, dColor);

	auto itImg = dImg.begin<Vec3b>();
	auto itImg_End = dImg.end<Vec3b>();
	auto itBlob = dBlob.begin<UINT8>();
	auto itColor = dColorData.begin<Vec3b>();

	while (itImg != itImg_End) {
		if (*itBlob != 0) {
			*itImg = (*itImg) * (1.f - nAlpha) + (*itColor) * (nAlpha);
		}

		itImg++;
		itBlob++;
	}
}

void CAutoRepairTesterDlg::DrawDefect_LayerCut(int nDrawMaskIndex, Scalar dColor, float nAlpha)
{
	Mat dDrawMask = GetDrawMask(nDrawMaskIndex);

	int nCount = m_listTargetInfo.size();
	for (int i = 0; i < nCount - 1; i++) {
		TargetInfo dTargetInfo = m_listTargetInfo[i];
		TargetInfo dNextTargetInfo = m_listTargetInfo[i + 1];

		// Check if they are adjecent targets
		if (dTargetInfo.m_nIndexY != dNextTargetInfo.m_nIndexY) {
			continue;
		}
		if ((dTargetInfo.m_nIndexX + 1) != dNextTargetInfo.m_nIndexX) {
			continue;
		}

		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTarget(m_dDrawImg, rcTarget);
		Mat dMask(dDrawMask, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));

		DrawDefect_TargetBlob(dTarget, dMask, dColor, nAlpha);
	}
}

void CAutoRepairTesterDlg::DrawDefect_ParcleCut(int nDrawMaskIndex, Mat dBoundingBox, Scalar dColor, float nAlpha)
{
	Mat dDrawMask = GetDrawMask(nDrawMaskIndex);

	for (auto dTargetInfo : m_listTargetInfo) {
		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dDefect(m_dResultImg, rcTarget);
		Mat dMask(dDrawMask, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));

		Mat dResult = dDefect & dMask;
		if (countNonZero(dResult) > 0) {
			Mat dTarget(m_dDrawImg, rcTarget);
			Mat dBox(dBoundingBox, rcTarget);

			dResult = dMask & dBox;
			DrawDefect_TargetBlob(dTarget, dResult, dColor, nAlpha);
		}
	}
}

void CAutoRepairTesterDlg::DrawDefect_Cut(Mat dBoundingBox, Scalar dColor, float nAlpha)
{
	vector<TargetInfo> listTargetInfo;
	listTargetInfo.push_back(m_dDefectTargetInfo.m_dTargetInfo);
	if (m_dDefectTargetInfo.m_bConnectedTarget) {
		listTargetInfo.push_back(m_dDefectTargetInfo.m_dTargetInfo2);
	}

	for (auto dTargetInfo : listTargetInfo) {
		TargetResult &TargetResult = dTargetInfo.m_dTargetResult;
		Mat dDrawMask = GetDrawMask(TargetResult.m_nDrawMaskIndex);
		if (CHECK_EMPTY_IMAGE(dDrawMask)) {
			continue;
		}

		Rect rcTarget = dTargetInfo.m_rcArea;
		Point ptStartInTemplate = dTargetInfo.m_ptStartPosInTemplate;

		Mat dTargetDefect(m_dResultImg, rcTarget);
		Mat dTargetMask = dDrawMask;
		if (dTargetMask.size() != rcTarget.size()) {
			dTargetMask = Mat(dDrawMask, Rect(ptStartInTemplate.x, ptStartInTemplate.y, rcTarget.width, rcTarget.height));
		}

		Mat dResult = dTargetDefect & dTargetMask;
		if (countNonZero(dResult) > 0) {
			Mat dTarget(m_dDrawImg, rcTarget);
			Mat dBox(dBoundingBox, rcTarget);

			dResult = dTargetMask & dBox;
			DrawDefect_TargetBlob(dTarget, dResult, dColor, nAlpha);
		}
	}
}


Mat CAutoRepairTesterDlg::GetMask(int nMaskIndex)
{
	CString strMaskFile = GetMaskFilePath(nMaskIndex);

	Mat dMask;
	if (!LoadMask(strMaskFile, dMask)) {
		AddLogText(L"Fail to check Mask. Cannot load Mask Layer %d.", nMaskIndex);
		return Mat();
	}

	return dMask;
}

Mat CAutoRepairTesterDlg::GetDrawMask(int nDrawMaskIndex)
{
//	Mat dDrawMask = Mat::zeros(GetOrigScaledTemplateSize(), CV_8UC1);
	// temp code
	Mat dDrawMask = Mat::zeros(402, 201, CV_8UC1);

	int nMaskSizeX = dDrawMask.cols;
	int nMaskSizeY = dDrawMask.rows;

	bool bDrawCut = false;
	bool bDrawCutVertically = false;
	float nDrawCut_MidRatio = 0.5f;
	Scalar dDrawCut_Color = CV_RGB(255, 0, 0);

	while (nDrawMaskIndex != 0x00) {
		bDrawCut = false;
		bDrawCutVertically = false;
		nDrawCut_MidRatio = 0.5f;

		// Active 
		IF_CHECK_DRAW_MASK_INDEX(ACTIVE_DRAW_MASK_LAYER_CUT)
		{
			int nX = 320;
			Point ptStart(nX, nMaskSizeY - 30);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 3);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(ACTIVE_DRAW_MASK_PARTICLE_CUT)
		{
			Point ptStart(0, nMaskSizeY - 1);
			Point ptEnd(nMaskSizeX - 1, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 5);
		}
		// Gate
		ELSE_IF_CHECK_DRAW_MASK_INDEX(GATE_DRAW_MASK_LAYER_CUT)
		{
			int nY = 320;
			Point ptStart(nMaskSizeX - 30, nY);
			Point ptEnd(nMaskSizeX - 1, nY);
			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 3);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(GATE_DRAW_MASK_PARTICLE_CUT)
		{
			Point ptStart(nMaskSizeX - 1, 0);
			Point ptEnd(nMaskSizeX - 1, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 5);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(GATE_DRAW_MASK_LINE_OPEN)
		{
			Point ptStart(m_rcBox.x - 20, m_rcBox.y + m_rcBox.height / 2);
			Point ptEnd(m_rcBox.x + m_rcBox.width + 20, m_rcBox.y + m_rcBox.height / 2);

			line(m_dDrawImg, ptStart, ptEnd, CV_RGB(0, 0, 255), 10);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(GATE_DRAW_MASK_DARK_DEPO)
		{
			Point ptStart(m_rcBox.x - 20, m_rcBox.y + 5);
			Point ptEnd(m_rcBox.x - 20, m_rcBox.y + m_rcBox.height - 5);
			line(m_dDrawImg, ptStart, ptEnd, CV_RGB(255, 0, 0), 2);
			line(m_dDrawImg, CvPoint(m_rcBox.x + m_rcBox.width + 20, m_rcBox.y + 5), CvPoint(m_rcBox.x + m_rcBox.width + 20, m_rcBox.y + m_rcBox.height - 5), CV_RGB(255, 0, 0), 2);

			line(m_dDrawImg, CvPoint(m_rcBox.x - 50, m_rcBox.y + m_rcBox.height / 2 + 50), CvPoint(m_rcBox.x - 50, m_rcBox.y + m_rcBox.height / 2), CV_RGB(0, 0, 255), 6);
			line(m_dDrawImg, CvPoint(m_rcBox.x + m_rcBox.width + 50, m_rcBox.y + m_rcBox.height / 2 + 50), CvPoint(m_rcBox.x + m_rcBox.width + 50, m_rcBox.y + m_rcBox.height / 2), CV_RGB(0, 0, 255), 6);
			line(m_dDrawImg, CvPoint(m_rcBox.x - 50, m_rcBox.y + m_rcBox.height / 2 + 50), CvPoint(m_rcBox.x + m_rcBox.width + 50, m_rcBox.y + m_rcBox.height / 2 + 50), CV_RGB(0, 0, 255), 6);

			circle(m_dDrawImg, CvPoint(m_rcBox.x - 50, m_rcBox.y + m_rcBox.height / 2 - 5), 3, CV_RGB(255, 0, 0), 6);
			circle(m_dDrawImg, CvPoint(m_rcBox.x + m_rcBox.width + 50, m_rcBox.y + m_rcBox.height / 2 - 5), 3, CV_RGB(255, 0, 0), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(GATE_DRAW_MASK_DARK_2PTN)
		{
			Point ptStart(m_rcBox.x + m_rcBox.width / 2 - 70, m_rcBox.y + m_rcBox.height / 2 - 110);
			Point ptEnd(m_rcBox.x + m_rcBox.width / 2 - 70, m_rcBox.y + m_rcBox.height / 2 - (80 - 30));

			line(m_dDrawImg, ptStart, ptEnd, CV_RGB(255, 0, 0), 6);
		}
		// SD
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_SDLINE_ELVDDLINE)
		{
			int nX = 25;
			Point ptStart(nX, 0);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_SDLINE_INITIALLINE)
		{
			int nX = 198;
			Point ptStart(nX, 0);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_SDLINE_CIRCLEPATTERN)
		{
			int nX = 198;
			Point ptStart(nX, 0);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_SDLINE_BRIDGEPATTERN)
		{
			int nX = 198;
			Point ptStart(nX, 0);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_INITIALLINE)
		{
			int nX = 100;
			Point ptStart(nX, 270);
			Point ptEnd(nX, nMaskSizeY - 1);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_CIRCLEPATTERN)
		{
			int nX = 100;
			Point ptStart(nX, 0);
			Point ptEnd(nX, 110);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_BRIDGEPATTERN)
		{
			int nX = 85;
			Point ptStart(nX, 60);
			Point ptEnd(nX, 290);

			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_INITIALLINE_CIRCLEPATTERN)
		{
			int nY = 5;
			Point ptStart(95, nY);
			Point ptEnd(nMaskSizeX - 1, nY);
			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);

			nY = nMaskSizeY - 5;
			ptStart = Point(95, nY);
			ptEnd = Point(nMaskSizeX - 1, nY);
			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_INITIALLINE_BRIDGEPATTERN)
		{
			int nY = 280;
			Point ptStart(80, nY);
			Point ptEnd(nMaskSizeX - 1, nY);
			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_IF_CHECK_DRAW_MASK_INDEX(SD_DRAW_MASK_SHORT_CUT_CIRCLEPATTERN_BRIDGEPATTERN)
		{
			int nY = 90;
			Point ptStart(85, nY);
			Point ptEnd(nMaskSizeX - 1, nY);
			line(dDrawMask, ptStart, ptEnd, CV_RGB(255, 255, 255), 6);
		}
		ELSE_CHECK_DRAW_MASK_INDEX
		{
			AddLogText(L"Fail to get Draw Mask. Cannot get Mask Layer 0x%016x.", nDrawMaskIndex);
			return Mat();
		}
	}

	// temp code
	resize(dDrawMask, dDrawMask, GetOrigScaledTemplateSize());

	dDrawMask = RotateImage(dDrawMask);

	return dDrawMask;
}


BOOL CAutoRepairTesterDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		return true;
	}
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		return true;
	}
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_SPACE)
	{
		return true;
	}


	return CDialogEx::PreTranslateMessage(pMsg);
}

void CAutoRepairTesterDlg::OnSelchangeDataFolderCombo()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ChangeDataFolder();
}

void CAutoRepairTesterDlg::ChangeDataFolder()
{
	int nIndex = m_ctlDataFolderList.GetCurSel();
	if (nIndex < 0) {
		AddLogText(L"Fail to change Data Folder. No Data Folder selected.");
		return;
	}

	CString strFolderName;
	m_ctlDataFolderList.GetLBText(nIndex, strFolderName);

	m_strDataFolder = m_strRootDataFolder + strFolderName + L"\\";

	LoadData();
}

void CAutoRepairTesterDlg::LoadDataFolderList()
{
	m_ctlDataFolderList.ResetContent();

	WIN32_FIND_DATA dFindData;
	wstring strFormat = m_strRootDataFolder;
	strFormat += L"*.*";
	HANDLE hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
	int nFileCount = 0;
	if (hFile != INVALID_HANDLE_VALUE) {
		int nIndex = 0;
		hFile = ::FindFirstFile(strFormat.c_str(), &dFindData);
		do {
			if (!(dFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				continue;
			}

			if (wcscmp(dFindData.cFileName, L".") == 0 || wcscmp(dFindData.cFileName, L"..") == 0) {
				continue;
			}

			int nIndex = m_ctlDataFolderList.GetCount();
			nIndex = m_ctlDataFolderList.InsertString(nIndex, dFindData.cFileName);
		} while (::FindNextFile(hFile, &dFindData));

		::FindClose(hFile);
	}

	int nCount = m_ctlDataFolderList.GetCount();
	if (nCount > 0) {
		AddLogText(L"Success to load Data Folder List , Count : %d", nCount);
		m_ctlDataFolderList.SelectString(-1, m_strDefaultDataFolder);
		ChangeDataFolder();
	}
	else {
		AddLogText(L"Fail to load Data Folder List, Count : %d", nCount);
	}
}

void CAutoRepairTesterDlg::FitTemplateScale()
{
	if (!m_bScaleBestFitImage) {
		return;
	}

	if (CHECK_EMPTY_IMAGE(m_dTemplate)) {
		return;
	}

	Rect rcTarget = FindBestFitTargetPos();
	resize(m_dTemplate, m_dTemplate, rcTarget.size());
}

Rect CAutoRepairTesterDlg::FindBestFitTargetPos()
{
	int nImgSizeX = m_dImg.cols;
	int nImgSizeY = m_dImg.rows;
	int nTemplateImgSizeX = m_dTemplate.cols;
	int nTemplateImgSizeY = m_dTemplate.rows;

	Mat dTemplate = m_dTemplate;
	if (m_dImg.channels() != dTemplate.channels()) {
		if (m_dImg.channels() == 3) {
			cvtColor(dTemplate, dTemplate, CV_GRAY2BGR);
		}
		else {
			cvtColor(dTemplate, dTemplate, CV_BGR2GRAY);
		}
	}
	if (m_dImg.type() != dTemplate.type()) {
		dTemplate.convertTo(dTemplate, m_dImg.type());
	}

	Mat dMatchImg = Mat::zeros(nImgSizeY, nImgSizeX, CV_32FC1);
	Rect rcBestFitTarget(0, 0, 0, 0);


	float nBestFitImageScale = m_bScaleBestFitImage ? m_nBestFitImageScale : 0.f;

	NIPLCV *pNIPLCV = NIPLCV::GetInstance();
	float nMinScale = max(1.f - nBestFitImageScale, 0.01f);
	float nMaxScale = nBestFitImageScale < 1.f ? 1.f + nBestFitImageScale : nBestFitImageScale;
	nMaxScale = min(nMaxScale, min((float)nImgSizeX / nTemplateImgSizeX, (float)nImgSizeY / nTemplateImgSizeY));
	float nScaleGap = 1.f / max(nTemplateImgSizeX, nTemplateImgSizeY);

	Size dPrevSize(0, 0);
	double nBestFitValue = 0;
	for (float nScale = nMinScale; nScale <= nMaxScale; nScale += nScaleGap) {
		Size dSize(cvRound(nTemplateImgSizeX * nScale), cvRound(nTemplateImgSizeY * nScale));
		if (dPrevSize == dSize) {
			continue;
		}
		dPrevSize = dSize;

		AddLogText(L"Finding Best Fit Target, Scale : %.4f (%.4f ~ %.4f), Size (%d, %d)", nScale, nMinScale, nMaxScale, dSize.width, dSize.height);

		Mat dResizedTemplate;
		resize(dTemplate, dResizedTemplate, dSize);

		matchTemplate(m_dImg, dResizedTemplate, dMatchImg, CV_TM_CCOEFF_NORMED);

		Point ptMax;
		double nMax;
		minMaxLoc(dMatchImg, 0, &nMax, 0, &ptMax);

		if (nMax <= nBestFitValue) {
			continue;
		}

		nBestFitValue = nMax;
		rcBestFitTarget = Rect(ptMax.x, ptMax.y, dSize.width, dSize.height);
	}

	AddLogText(L"Found Best Fit Target, Pos (%d, %d), Size (%d, %d)",
		rcBestFitTarget.x, rcBestFitTarget.y, rcBestFitTarget.width, rcBestFitTarget.height);

	return rcBestFitTarget;
}

bool CAutoRepairTesterDlg::FindBestFitTarget()
{
	m_dBestFitTarget.release();

	if (!LoadImage()) {
		AddLogText(L"Fail to find Target. Cannot load Image.");
		return false;
	}
	ShowImage(m_dImg);

	CString strTemplateFile = GetMaskFilePath(MASK_TEMPLATE);
	if (!LoadMask(strTemplateFile, m_dTemplate)) {
		AddLogText(L"Fail to find Target. Cannot load Template Image.");
		return false;
	}

	Rect rcBestFitTarget = FindBestFitTargetPos();

	m_listTargetInfo.clear();

	TargetInfo dTargetInfo;
	dTargetInfo.m_rcArea = rcBestFitTarget;

	m_listTargetInfo.push_back(dTargetInfo);

	m_dBestFitTarget = Mat(m_dImg, rcBestFitTarget);

	return true;
}

void CAutoRepairTesterDlg::OnBnClickedFindBestFitImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (!UpdateData()) {
		return;
	}

	if (!FindBestFitTarget()) {
		AddLogText(L"Cannot Do Test. Fail to find Target.");
		return;
	}

	m_nProcessStep = STEP_DETECT;
	ShowPosBox();
}

void CAutoRepairTesterDlg::OnBnClickedSaveBestFitImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (CHECK_EMPTY_IMAGE(m_dBestFitTarget)) {
		AddLogText(L"No Best Fit Image selected. Find Best Fit Image first..");
		return;
	}

	wchar_t szFilter[] = L"Image Files (*.bmp;*.png;*.jpg)|*.bmp;*.png;*.jpg|All Files (*.*)|*.*||";
	CFileDialog iFileDlg(FALSE, L"bmp", m_strMaskFolder + "new TEMPLATE.bmp", OFN_OVERWRITEPROMPT, szFilter);
	if (iFileDlg.DoModal() != IDOK) {
		return;
	}

	wstring strPath(LPCTSTR(iFileDlg.GetPathName()));

	NIPO *pNIPO = NIPO::GetInstance();

	Mat dImg = ReverseRotateImage(m_dBestFitTarget);
	pNIPO->SaveImage(strPath, dImg);
}


void CAutoRepairTesterDlg::OnBnClickedScaleBestFitImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
	GetDlgItem(IDC_EDIT_BEST_FIT_IMAGE_SCALE)->EnableWindow(m_bScaleBestFitImage);
}

Mat CAutoRepairTesterDlg::RotateImage(Mat dImg)
{
	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	Mat dRotatedImg = dImg.clone();

	switch (m_nImageRotation) {
	case IMAGE_ROTATION_90: 
		transpose(dRotatedImg, dRotatedImg);
		flip(dRotatedImg, dRotatedImg, 0);
		break;
	case IMAGE_ROTATION_180:
		flip(dRotatedImg, dRotatedImg, 0);
		flip(dRotatedImg, dRotatedImg, 1);
		break;
	case IMAGE_ROTATION_270: 
		transpose(dRotatedImg, dRotatedImg);
		flip(dRotatedImg, dRotatedImg, 1);
		break;
	}

	if (m_bFlipImageHoriz) {
		flip(dRotatedImg, dRotatedImg, 1);
	}
	if (m_bFlipImageVert) {
		flip(dRotatedImg, dRotatedImg, 0);
	}

	return dRotatedImg;
}

Mat CAutoRepairTesterDlg::ReverseRotateImage(Mat dImg)
{
	int nImgSizeX = dImg.cols;
	int nImgSizeY = dImg.rows;

	Mat dRotatedImg = dImg.clone();
	if (m_bFlipImageVert) {
		flip(dRotatedImg, dRotatedImg, 0);
	}
	if (m_bFlipImageHoriz) {
		flip(dRotatedImg, dRotatedImg, 1);
	}

	switch (m_nImageRotation) {
	case IMAGE_ROTATION_90:
		transpose(dRotatedImg, dRotatedImg);
		flip(dRotatedImg, dRotatedImg, 1);
		break;
	case IMAGE_ROTATION_180:
		flip(dRotatedImg, dRotatedImg, 0);
		flip(dRotatedImg, dRotatedImg, 1);
		break;
	case IMAGE_ROTATION_270:
		transpose(dRotatedImg, dRotatedImg);
		flip(dRotatedImg, dRotatedImg, 0);
		break;
	}

	return dRotatedImg;
}

Size CAutoRepairTesterDlg::GetOrigScaledTemplateSize()
{
	Mat dImg = ReverseRotateImage(m_dTemplate);
	return dImg.size();
}

void CAutoRepairTesterDlg::OnClickedRadioImageRotation(UINT nID)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData();
	ChangeMask();
}

Mat CAutoRepairTesterDlg::GetMergeMask(int nMaskIndexFrom, int nMaskIndexTo, bool bInverse)
{
	Mat dMergeMask;
	Mat dMask;
	for (int i = nMaskIndexFrom; i <= nMaskIndexTo; i++) {
		dMask = GetMask(i);
		if (CHECK_EMPTY_IMAGE(dMask)) {
			continue;
		}

		if (CHECK_EMPTY_IMAGE(dMergeMask)) {
			dMergeMask = dMask;
		}
		else {
			dMergeMask |= dMask;
		}
	}

	if (CHECK_EMPTY_IMAGE(dMergeMask) && bInverse) {
		dMergeMask = ~dMergeMask;
	}

	return dMergeMask;
}
