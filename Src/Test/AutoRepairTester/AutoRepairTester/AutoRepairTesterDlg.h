
// AutoRepairTesterDlg.h : header file
//

#pragma once
#include "afxwin.h"

enum IMAGE_ROTATION {
	IMAGE_ROTATION_0,
	IMAGE_ROTATION_90,
	IMAGE_ROTATION_180,
	IMAGE_ROTATION_270
};

enum TYPE_INDEX {
	TYPE_ACTIVE = 0,
	TYPE_GATE,
	TYPE_SD
};

enum MASK_INDEX {
	MASK_TEMPLATE = 0,
	MASK_LAYER_1,
	MASK_LAYER_2,
	MASK_LAYER_3,
	MASK_LAYER_4,
	MASK_LAYER_5,
	MASK_LAYER_6,
	MASK_LAYER_7,
	MASK_LAYER_8,
	MASK_LAYER_9,
	MASK_LAYER_10,
	MASK_LAYER_11,
	MASK_LAYER_12,
	MASK_LAYER_13,
	MASK_LAYER_14,
	MASK_LAYER_15
};

enum MASK_SD_INDEX {
	MASK_SD_TEMPLATE = 0,
	MASK_SD_SD_LINE = 1,
	MASK_SD_ELVDD_LINE = 2,
	MASK_SD_INITIAL_LINE = 3,
	MASK_SD_CIRCLE_PATTERN = 4,
	MASK_SD_BRIDGE_PATTERN = 5,
	MASK_SD_HOLE = 6,
	MASK_SD_COMMON_HOLE = 7,
	MASK_GATE_EMISSION_LINE = 8,
	MASK_GATE_SCAN_LINE = 9,
	MASK_GATE_OPEN_LINE = 10,
	MASK_GATE_CAP = 11,
	MASK_ACTIVE_PATTERN = 12
};


enum DRAW_MASK_INDEX {
	// Active
	ACTIVE_DRAW_MASK_LAYER_CUT		= 0x00000001,
	ACTIVE_DRAW_MASK_PARTICLE_CUT	= 0x00000002,

	// Gate
	GATE_DRAW_MASK_LAYER_CUT						= 0x00000010,
	GATE_DRAW_MASK_PARTICLE_CUT						= 0x00000020,
	GATE_DRAW_MASK_DARK_DEPO						= 0x00000040,
	GATE_DRAW_MASK_DARK_2PTN						= 0x00000080,
	GATE_DRAW_MASK_LINE_OPEN						= 0x00000100,
	GATE_DRAW_MASK_SHORT_CUT_CAP_EMISSIONLINE		= 0x00000200,
	GATE_DRAW_MASK_SHORT_CUT_CAP_SCANLINE			= 0x00000400,
	
	// SD
	SD_DRAW_MASK_SHORT_CUT_SDLINE_ELVDDLINE				= 0x00010000,
	SD_DRAW_MASK_SHORT_CUT_SDLINE_INITIALLINE			= 0x00020000,
	SD_DRAW_MASK_SHORT_CUT_SDLINE_CIRCLEPATTERN			= 0x00040000,
	SD_DRAW_MASK_SHORT_CUT_SDLINE_BRIDGEPATTERN			= 0x00080000,
	SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_INITIALLINE		= 0x00100000,
	SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_CIRCLEPATTERN		= 0x00200000,
	SD_DRAW_MASK_SHORT_CUT_ELVDDLINE_BRIDGEPATTERN		= 0x00400000,
	SD_DRAW_MASK_SHORT_CUT_INITIALLINE_CIRCLEPATTERN	= 0x00800000,
	SD_DRAW_MASK_SHORT_CUT_INITIALLINE_BRIDGEPATTERN	= 0x01000000,
	SD_DRAW_MASK_SHORT_CUT_CIRCLEPATTERN_BRIDGEPATTERN	= 0x02000000
};

enum DEFECT_INDEX {
	DEFECT_UNKNOWN = 0,
	DEFECT_PARTICLE,
	DEFECT_2_PTN_PARTICLE,
	DEFECT_1_PTN_SHORT,
	DEFECT_2_PTN_SHORT,
	DEFECT_1_SEED_SHORT,
	DEFECT_2_SEED_SHORT,
	DEFECT_1_HOLE,
	DEFECT_2_HOLE,
	DEFECT_1_HABU_SHORT,
	DEFECT_2_HABU_SHORT,
	DEFECT_LINE_SHORT,
	DEFECT_LINE_SEED,
	DEFECT_DEPO_OPEN,
	DEFECT_DARK_DEPO,
	DEFECT_DARK_LINE,
	DEFECT_1_DARK_PTN,
	DEFECT_2_DARK_PTN,

// Gate
	DEFECT_GATE_1_PTN_ERR,
	DEFECT_GATE_1_HABU_PTN_SHORT,
	DEFECT_GATE_1_HOLE,
	DEFECT_GATE_1_SEED_SHORT,
	DEFECT_GATE_2_PTN_ERR,
	DEFECT_GATE_2_HABU_PTN_SHORT,
	DEFECT_GATE_2_PTN_SHORT,
	DEFECT_GATE_2_PTN_SEED_SHORT,
	DEFECT_GATE_2_DARK_PTN,
	DEFECT_GATE_DARK_DEPO,
	DEFECT_GATE_DARK_LINE_SHORT,
	DEFECT_GATE_DEPO_OPEN,
	DEFECT_GATE_SEED_LINE_SHORT,
	DEFECT_GATE_LINE_SHORT,
	DEFECT_GATE_THIN,
};

#define STR_MASK_TEMPLATE L"TEMPLATE"
#define STR_MASK_LAYER L"MASK_LAYER_"
#define STR_DRAW L"_DRAW_"

enum PROCESS_STEP {
	STEP_NONE = 0,
	STEP_LOAD_IMAGE = 0,
	STEP_DETECT,
	STEP_DRAW
};

struct TargetResult {
// Active Layer
	bool m_bActiveMask;

// Gate Mask
	bool m_bEmissionLineMask;
	bool m_bScanLineMask;
	bool m_bOpenLineMask;
	bool m_bCapMask;

// SD Mask
	bool m_bSDLineMask;
	bool m_bELVDDLineMask;
	bool m_bInitialLineMask;
	bool m_bCirclePatternMask;
	bool m_bBridgePatternMask;
	bool m_bHoleMask;
	bool m_bCommonHoleMask;

// Dark or Seed
	bool m_bDark;
	bool m_bHasSeed;

// Line Connected
	int m_nConnectedLineCount;
	int m_nHabuConnectedLineCount;

// Draw Mask
	int m_nDrawMaskIndex;

	TargetResult() {
		Clear();
	}

	void Clear() {
		m_bActiveMask= false;

		m_bEmissionLineMask= false;
		m_bScanLineMask= false;
		m_bOpenLineMask= false;
		m_bCapMask= false;

		m_bSDLineMask= false;
		m_bELVDDLineMask= false;
		m_bInitialLineMask= false;
		m_bCirclePatternMask= false;
		m_bBridgePatternMask= false;
		m_bHoleMask= false;
		m_bCommonHoleMask= false;

		m_bDark = false;
		m_bHasSeed= false;

		m_nConnectedLineCount = 0;
		m_nHabuConnectedLineCount = 0;

		m_nDrawMaskIndex = 0;
	}
};

struct TargetInfo {
	int m_nIndexX;
	int m_nIndexY;

	Rect m_rcArea;
	Point m_ptStartPosInTemplate;
	bool m_bHasDarkPoint;
	bool m_bHasSeed;

	TargetResult m_dTargetResult;

	bool m_bSetToDefectTarget;

	TargetInfo() {
		Clear();
	}

	void Clear() {
		m_nIndexX = -1;
		m_nIndexY = -1;

		m_rcArea = Rect(0, 0, 0, 0);
		m_ptStartPosInTemplate = Point(0, 0);

		m_dTargetResult.Clear();

		m_bHasDarkPoint = false;
		m_bHasSeed = false;

		m_bSetToDefectTarget = false;
	}

	Point GetCenterPos() {
		Point ptCenterPos;
		ptCenterPos.x = cvRound(m_rcArea.x + m_rcArea.width * 0.5f);
		ptCenterPos.y = cvRound(m_rcArea.y + m_rcArea.height * 0.5f);

		return ptCenterPos;
	}
};

struct DefectTargetInfo {
	int m_nDefectIndex;

	bool m_bConnectedTarget;
	bool m_bConnectedVertically;
	TargetInfo m_dTargetInfo;
	TargetInfo m_dTargetInfo2;

	DefectTargetInfo() {
		Clear();
	}

	void Clear() {
		m_nDefectIndex = DEFECT_UNKNOWN;
		m_bConnectedTarget = false;
		m_bConnectedVertically = false;
		m_dTargetInfo.Clear();
		m_dTargetInfo2.Clear();
	}

	Point GetCenterPos() {
		if (!m_bConnectedTarget) {
			return m_dTargetInfo.GetCenterPos();
		}

		Point ptFirstTargetCenterPos = m_dTargetInfo.GetCenterPos();
		Point ptSecondTargetCenterPos = m_dTargetInfo2.GetCenterPos();

		Point ptCenterPos;
		ptCenterPos.x = cvRound((ptFirstTargetCenterPos.x + ptSecondTargetCenterPos.x) * 0.5f);
		ptCenterPos.y = cvRound((ptFirstTargetCenterPos.y + ptSecondTargetCenterPos.y) * 0.5f);

		return ptCenterPos;
	}
};

// CAutoRepairTesterDlg dialog
class CAutoRepairTesterDlg : public CDialogEx
{
// Construction
public:
	CAutoRepairTesterDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_AUTOREPAIRTESTER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	NIPLImageCtl m_wndImg;
	Mat m_dImg;
	Mat m_dResultImg;
	Mat m_dResultForMaskingImg;
	Mat m_dDrawImg;

	NIPLImageCtl m_wndMask;
	Mat m_dMask;

	Mat m_dDiff;

	Rect m_rcBox;
	Rect m_rcExtBox;

	vector<TargetInfo> m_listTargetInfo;
	DefectTargetInfo m_dDefectTargetInfo;
	Point m_ptStartInTemplate;

	Mat m_dTemplate;
	Mat m_dTargetResult;
	Mat m_dBestFitTarget;

	CString m_strRootDataFolder;
	CString m_strDataFolder;
	CString m_strDefaultDataFolder;
	CString m_strImageFolder;
	CString m_strImageFile;
	CString m_strMaskFolder;
	CString m_strMaskFile;

	int m_nProcessStep;

	map<CString, Mat> m_mapMask;
	map<CString, CString> m_mapMaskFileName;

	bool m_bShowDetectWithMask;

public:
	void ShowImage(Mat dImg);
	void ShowImageWithMask(Mat dImg);
	void ShowMask();
	bool LoadImage();
	void AddLogText(const wchar_t* szText, ...);
	LRESULT OnImagePosValue(WPARAM wParam, LPARAM lParam);
	void ShowImagePosValue(CPoint ptImagePos);
	void ShowImageInfo();
	void ShowPosBox();
	void ClearPosBox();

	void ChangeType();
	void ChangeImage();
	void ChangeMask();

	bool ClassifyDefect();

	bool LoadMask();
	bool LoadMask(CString strMaskFile, Mat &dMask);

	bool FindTarget();

	bool FindDefect();
	Mat FindDefect_Active(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate);
	Mat FindDefect_Gate(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate);
	Mat FindDefect_SD(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate);

	void LoadData();

	void LoadImageList();
	void LoadMaskList();

	CString GetMaskFilePath(int nMaskIndex);
	CString GetMaskFilePath(CString strFileName);

	void CheckDefect();
	DEFECT_INDEX CheckDefect_Active();
	DEFECT_INDEX CheckDefect_Gate();
	DEFECT_INDEX CheckDefect_SD();

	Mat ApplyMask(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate, int nMaskIndex, Mat dFilterMask = Mat());
	Mat ApplyMask(Mat dTemplate, Mat dDiff, Point ptStartPosInTemplate, Mat dMask = Mat(), Mat dFilterMask = Mat());
	bool CheckMask(int nMaskIndex, int nDefectMinSize = 0, bool bCheckConnected = false);

	bool IsRectOverlap(Rect rc1, Rect rc2);

	bool DrawDefect();
	bool DrawDefect_Active();
	bool DrawDefect_Gate();
	bool DrawDefect_SD();

	Mat GetDefectBoundingBoxMask(int nExtendSize);
	void DrawDefect_Mask(int nMaskIndex, Mat dBoundingBox, Scalar dColor, float nAlpha = 0.5f);
	void DrawDefect_Defect(Scalar dColor, float nAlpha = 0.5f, int nOverlappedMaskIndex = -1);

	void DrawDefect_Blob(Mat &dImg, Mat dBlob, Scalar dColor, float nAlpha = 0.5f);
	void DrawDefect_TargetBlob(Mat &dImg, Mat dBlob, Scalar dColor, float nAlpha = 0.5f);

	void DrawDefect_LayerCut(int nDrawMaskIndex, Scalar dColor, float nAlpha);
	void DrawDefect_ParcleCut(int nDrawMaskIndex, Mat dBoundingBox, Scalar dColor, float nAlpha);
	void DrawDefect_Cut(Mat dBoundingBox, Scalar dColor, float nAlpha);
	Mat GetDrawMask(int nDrawMaskIndex);
	Mat GetMask(int nMaskIndex);

	void LoadDataFolderList();
	void ChangeDataFolder();
	void FitTemplateScale();
	Rect FindBestFitTargetPos();
	bool FindBestFitTarget();
	Mat RotateImage(Mat dImg);
	Mat ReverseRotateImage(Mat dImg);
	Mat GetMergeMask(int nMaskIndexFrom, int nMaskIndexTo, bool bInverse = false);
	void CheckDrawMaskIndex();

	Size GetOrigScaledTemplateSize();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_ctlImageArea;
	CStatic m_ctlMaskArea;
	CListBox m_ctlLogList;
	int m_nTypeIndex;
	int m_nDefectIndex;

	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnClickedTypeRadio(UINT nID);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnBnClickedLoadMask();
	afx_msg void OnBnClickedFindDefect();
	float m_nTestBinarizeThreshold;
	afx_msg void OnBnClickedOpenImage();
	int m_nDefectMinSize;
	CString m_strTestResult;
	CListBox m_ctlImageList;
	CComboBox m_ctlMaskList;
	afx_msg void OnSelchangeImageList();
	afx_msg void OnSelchangeMaskList();
	afx_msg void OnBnClickedImageReload();
	afx_msg void OnBnClickedFocusStartPos();
	afx_msg void OnBnClickedFocusEndPos();
	afx_msg void OnBnClickedClassify();
	afx_msg void OnBnClickedDraw();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CComboBox m_ctlDataFolderList;
	afx_msg void OnSelchangeDataFolderCombo();
	afx_msg void OnBnClickedFindBestFitImage();
	afx_msg void OnBnClickedSaveBestFitImage();
	BOOL m_bScaleBestFitImage;
	afx_msg void OnBnClickedTempSave();
	float m_nBestFitImageScale;
	afx_msg void OnBnClickedScaleBestFitImage();
	int m_nHoleMinSize;
	int m_nImageRotation;
	afx_msg void OnClickedRadioImageRotation(UINT nID);
	afx_msg void OnBnClickedCheckImageFlip();
	BOOL m_bFlipImageHoriz;
	BOOL m_bFlipImageVert;
	afx_msg void OnBnClickedCheckImageFlipHoriz();
	int m_nSeedMinSize;
};
