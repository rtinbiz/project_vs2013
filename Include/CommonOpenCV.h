#pragma once

/*
MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)
MSVC++ 12.0 _MSC_VER == 1800 (Visual Studio 2013)
MSVC++ 11.0 _MSC_VER == 1700 (Visual Studio 2012)
MSVC++ 10.0 _MSC_VER == 1600 (Visual Studio 2010)
MSVC++ 9.0  _MSC_VER == 1500 (Visual Studio 2008)
MSVC++ 8.0  _MSC_VER == 1400 (Visual Studio 2005)
MSVC++ 7.1  _MSC_VER == 1310 (Visual Studio 2003)
*/

#if (_MSC_VER == 1900) // VS2015, VC14
#ifdef _DEBUG
#pragma comment (lib, "../Lib/OpenCV310/vc14/lib/opencv_world310d.lib")
#else
#pragma comment (lib, "../Lib/OpenCV310/vc14/lib/opencv_world310.lib")
#endif
#else					// default, VS2013, VC12
#ifdef _DEBUG
#pragma comment (lib, "../Lib/OpenCV310/vc12/lib/opencv_world310d.lib")
#else
#pragma comment (lib, "../Lib/OpenCV310/vc12/lib/opencv_world310.lib")
#endif
/*
#ifdef _DEBUG
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_ts300d.lib")
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_world300d.lib")
#else
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_ts300.lib")
#pragma comment (lib, "../Lib/OpenCV300/vc12/lib/opencv_world300.lib")
#endif
*/
#endif


#include "opencv2/opencv.hpp"
using namespace cv;
