#pragma once

#include "NIPL.h"
#include "NIPLImageCtl.h"

#ifndef _BUILD_NIPL_GUI
#ifdef _DEBUG
#pragma comment (lib, "NIPLGUI_d.lib")
#else
#pragma comment (lib, "NIPLGUI.lib")
#endif
#endif

CV_INLINE RECT NormalizeRect(RECT r);
CV_INLINE RECT NormalizeRect(RECT r)
{
	int t;
	if (r.left > r.right)
	{
		t = r.left;
		r.left = r.right;
		r.right = t;
	}
	if (r.top > r.bottom)
	{
		t = r.top;
		r.top = r.bottom;
		r.bottom = t;
	}

	return r;
}
CV_INLINE CvRect RectToCvRect(RECT sr);
CV_INLINE CvRect RectToCvRect(RECT sr)
{
	sr = NormalizeRect(sr);
	return cvRect(sr.left, sr.top, sr.right - sr.left, sr.bottom - sr.top);
}
CV_INLINE RECT CvRectToRect(CvRect sr);
CV_INLINE RECT CvRectToRect(CvRect sr)
{
	RECT dr;
	dr.left = sr.x;
	dr.top = sr.y;
	dr.right = sr.x + sr.width;
	dr.bottom = sr.y + sr.height;

	return dr;
}
CV_INLINE CvPoint PointToCvPoint(POINT pt);
CV_INLINE CvPoint PointToCvPoint(POINT pt)
{
	return cvPoint(pt.x, pt.y);
}
CV_INLINE POINT CvPointToPoint(CvPoint pt);
CV_INLINE POINT CvPointToPoint(CvPoint pt)
{
	return CPoint(pt.x, pt.y);
}
