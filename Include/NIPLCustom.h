#pragma once

#include "NIPLCV.h"
#include "NIPLCustomParam.h"
#include "NIPLCustomResult.h"

#ifndef _BUILD_NIPL_CUSTOM
#ifdef _DEBUG
#pragma comment (lib, "NIPLCustom_d.lib")
#else
#pragma comment (lib, "NIPLCustom.lib")
#endif
#endif

#define TIME_PROFILE_MAG_CORE

#define NIPL_CUSTOM_METHOD_IMPL(FuncName) NIPL_ERR NIPLCustom::FuncName(NIPLInput *pInput, NIPLOutput *pOutput)

class AFX_EXT_CLASS NIPLCustom : public NIPLCV {
public :
	static NIPLCustom *pThis;
	static NIPLCustom *GetInstance(BOOL bNew = FALSE);
	static void ReleaseInstance(NIPLCustom *pThat = 0x00);

	NIPLCustom();
	~NIPLCustom();

	NIPL_METHOD_DECL(LDC_Calibration);
	NIPL_METHOD_DECL(LDC_Terminal);
	NIPL_METHOD_DECL(LDC_Tube);
	NIPL_METHOD_DECL(LDC_Guide);
	NIPL_METHOD_DECL(LDC_Bolt);
	NIPL_METHOD_DECL(LDC_Paper);
	NIPL_METHOD_DECL(LDC_Clip);

private :
	NIPL_ERR LDC_Terminal_CheckExist(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect);
	NIPL_ERR LDC_Tube_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, Point ptRef, float nThreshold, float nRefThreshold, int nColorType, bool &bDefect);
	NIPL_ERR LDC_Guide_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect);
	NIPL_ERR LDC_Bolt_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect);
	NIPL_ERR LDC_Paper_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, Point ptRef, float nThreshold, float nRefThreshold, bool &bDefect);
	NIPL_ERR LDC_Clip_CheckColor(Mat dImg, Mat &dOutputImg, Rect rcBoundingBox, float nThreshold, bool &bDefect);
	void LDC_Terminal_ExcludeOutOfRadiusRange(Mat dImg, Mat &dOutputImage, Point ptCenter, int nMaxRadius);
	void LDC_Tube_NormalizeBrightness(Mat dImg, Mat &dOutputImg);

	bool LDC_LoadData_Calibration(NIPLParam_LDC_Calibration *pParam);
	bool LDC_LoadData_Terminal(NIPLParam_LDC *pParam);
	bool LDC_LoadData_Tube(NIPLParam_LDC *pParam);
	bool LDC_LoadData(NIPLParam_LDC *pParam, wstring strComp);

	//
	// MagCore
	// 
public:
	struct TimeProfile_MagCore {
		DWORD m_nTotalStartTime;
		DWORD m_nStartTime;
		DWORD m_nTotalTime;

		DWORD m_nCalibration_Start;
		DWORD m_nCalibration_LoadData;
		DWORD m_nCalibration_Color2Gray;
		DWORD m_nCalibration_BrightnessEnhancement;
		DWORD m_nCalibration_FindCircle;
		DWORD m_nCalibration_AdjustCircle;
		DWORD m_nCalibration_Rest;
		DWORD m_nCalibration_End;

		DWORD m_nInspection_Start;
		DWORD m_nInspection_LoadData;
		DWORD m_nInspection_Circle2Rect;
		DWORD m_nInspection_EdgeCheck_MinRadius;
		DWORD m_nInspection_EdgeCheck_MaxRadius;
		DWORD m_nInspection_CharCheck_Binarize;
//		DWORD m_nInspection_FitBackground;
		DWORD m_nInspection_CharCheck_Inspection;
//		DWORD m_nInspection_Binarize;
//		DWORD m_nInspection_EliminateEdgeNoise;
		DWORD m_nInspection_Surface;
		DWORD m_nInspection_Hole;
		//		DWORD m_nInspection_Rect2Circle;
//		DWORD m_nInspection_EliminateNoise;
//		DWORD m_nInspection_FindBlob;
		DWORD m_nInspection_Rest;
		DWORD m_nInspection_End;

		void Clear_Calibration() {
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nCalibration_Start = 0;
			m_nCalibration_LoadData = 0;
			m_nCalibration_Color2Gray = 0;
			m_nCalibration_BrightnessEnhancement = 0;
			m_nCalibration_FindCircle = 0;
			m_nCalibration_AdjustCircle = 0;
			m_nCalibration_Rest = 0;
			m_nCalibration_End = 0;
		}

		void Clear_Inspection() {
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nInspection_Start = 0;
			m_nInspection_LoadData = 0;
			m_nInspection_Circle2Rect = 0;
			m_nInspection_EdgeCheck_MinRadius = 0;
			m_nInspection_EdgeCheck_MaxRadius = 0;
			m_nInspection_CharCheck_Binarize = 0;
//			m_nInspection_FitBackground = 0;
			m_nInspection_CharCheck_Inspection = 0;
//			m_nInspection_Binarize = 0;
//			m_nInspection_EliminateEdgeNoise = 0;
			m_nInspection_Surface = 0;
			m_nInspection_Hole = 0;
//			m_nInspection_Rect2Circle = 0;
//			m_nInspection_EliminateNoise = 0;
//			m_nInspection_FindBlob = 0;
			m_nInspection_Rest = 0;
			m_nInspection_End = 0;
		}

		void SetStartTime() 
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nStartTime = ::GetTickCount();
		}
		void SetTime(DWORD &nTime)
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			nTime = ::GetTickCount() - m_nStartTime;
		}
		void SetStartTime_Calibration()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nCalibration_Start = ::GetTickCount();
		}
		void SetEndTime_Calibration()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nCalibration_End = ::GetTickCount() - m_nCalibration_Start;
		}
		void SetRestTime_Calibration()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nCalibration_Rest = m_nCalibration_End -
				(m_nCalibration_LoadData + m_nCalibration_Color2Gray + m_nCalibration_BrightnessEnhancement + m_nCalibration_FindCircle + m_nCalibration_AdjustCircle);
		}

		void SetStartTime_Inspection()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nInspection_Start = ::GetTickCount();
		}
		void SetEndTime_Inspection()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nInspection_End = ::GetTickCount() - m_nInspection_Start;
			m_nTotalTime = m_nCalibration_End + m_nInspection_End;
		}
		void SetRestTime_Inspection()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nInspection_Rest = m_nInspection_End -
				(m_nInspection_LoadData + m_nInspection_Circle2Rect + m_nInspection_EdgeCheck_MinRadius + m_nInspection_EdgeCheck_MaxRadius
				+ m_nInspection_CharCheck_Binarize + m_nInspection_CharCheck_Inspection + m_nInspection_Surface + m_nInspection_Hole);
		}

		void SetEndTime_Total()
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			m_nTotalTime = m_nCalibration_End + m_nInspection_End;
		}

		wstring GetPrintString(DWORD nTime, DWORD nEndTime)
		{
#ifndef TIME_PROFILE_MAG_CORE
			return L"";
#endif
			wchar_t szText[256];
			if (nEndTime == 0) {
				swprintf_s(szText, L"%d ms", nTime);
			}
			else {
				swprintf_s(szText, L"%d ms (%.2f%%)", nTime, ((float)nTime / nEndTime) * 100.f);
			}
			
			return szText;
		}
		wstring GetPrintString_End(DWORD nEndTime)
		{
#ifndef TIME_PROFILE_MAG_CORE
			return L"";
#endif
			wchar_t szText[256];
			swprintf_s(szText, L"%d ms", nEndTime);

			return szText;
		}

		void Print_Calibration(NIPL *pNIPL)
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			SetEndTime_Calibration();
			SetRestTime_Calibration();

			pNIPL->DebugPrint(L"==== MagCore Calibration Time Profile ====");
			pNIPL->DebugPrint(L" > Load Data : %s", GetPrintString(m_nCalibration_LoadData, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > Color to Gray : %s", GetPrintString(m_nCalibration_Color2Gray, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > Brightness Enhancement : %s", GetPrintString(m_nCalibration_BrightnessEnhancement, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > Find Circle : %s", GetPrintString(m_nCalibration_FindCircle, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > Adjust Circle : %s", GetPrintString(m_nCalibration_AdjustCircle, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > Rest Part : %s", GetPrintString(m_nCalibration_Rest, m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L" > End : %s", GetPrintString_End(m_nCalibration_End).c_str());
			pNIPL->DebugPrint(L"==== MagCore Calibration Time Profile, End ====");
		}

		void Print_Inspection(NIPL *pNIPL)
		{
#ifndef TIME_PROFILE_MAG_CORE
			return;
#endif
			SetEndTime_Inspection();
			SetRestTime_Inspection();
			SetEndTime_Total();

			pNIPL->DebugPrint(L"==== MagCore Inspection Time Profile ====");
			pNIPL->DebugPrint(L" > Load Data : %s", GetPrintString(m_nInspection_LoadData, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Circle to Rect : %s", GetPrintString(m_nInspection_Circle2Rect, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Edge Inspection : %s", GetPrintString(m_nInspection_EdgeCheck_MinRadius + m_nInspection_EdgeCheck_MaxRadius, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Char Binarize : %s", GetPrintString(m_nInspection_CharCheck_Binarize, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Fit Background : %s", GetPrintString(m_nInspection_FitBackground, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Char Inspection : %s", GetPrintString(m_nInspection_CharCheck_Inspection, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Binarize : %s", GetPrintString(m_nInspection_Binarize, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Eliminate Edge Noise : %s", GetPrintString(m_nInspection_EliminateEdgeNoise, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Surface Inspection : %s", GetPrintString(m_nInspection_Surface, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Hole Inspection : %s", GetPrintString(m_nInspection_Hole, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Rect to Circle : %s", GetPrintString(m_nInspection_Rect2Circle, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Eliminate Noise : %s", GetPrintString(m_nInspection_EliminateNoise, m_nInspection_End).c_str());
//			pNIPL->DebugPrint(L" > Find Blob : %s", GetPrintString(m_nInspection_FindBlob, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > Rest Part : %s", GetPrintString(m_nInspection_Rest, m_nInspection_End).c_str());
			pNIPL->DebugPrint(L" > End : %s", GetPrintString_End(m_nInspection_End).c_str());
			pNIPL->DebugPrint(L"==== MagCore Calibration Time Profile, End ====");

			pNIPL->DebugPrint(L"==== MagCore Total Time Profile ====");
			pNIPL->DebugPrint(L" > Calibration : %s", GetPrintString(m_nCalibration_End, m_nTotalTime).c_str());
			pNIPL->DebugPrint(L" > Inspection : %s", GetPrintString(m_nInspection_End, m_nTotalTime).c_str());
			pNIPL->DebugPrint(L" > Total : %s", GetPrintString_End(m_nTotalTime).c_str());
			pNIPL->DebugPrint(L"==== MagCore Total Time Profile, End ====");
		}
	};

	TimeProfile_MagCore dTimeProfile_MagCore;

	NIPL_METHOD_DECL(MagCore_Calibration);
	NIPL_METHOD_DECL(MagCore_Inspection);

	bool MagCore_LoadData_Calibration(NIPLParam_MagCore_Calibration *pParam);
	bool MagCore_LoadData_Inspection(NIPLParam_MagCore *pParam);
	NIPL_ERR MagCore_Calibration_FindCircle(Mat dImg, Mat &dOutputImg, NIPLParam_MagCore_Calibration *pParam, NIPLEllipse &dFoundCircle, bool bMaxCircle);
	void MagCore_Calibration_EdgeDetecting(Mat dImg, Mat &dOutputImg, bool bReverse);
	NIPL_ERR MagCore_Calibration_AdjustCircle(Mat dImg, Mat dCircleImg, Mat &dOutputImg, NIPLParam_MagCore_Calibration *pParam, const NIPLCircle &dMinCircle, const NIPLCircle &dMaxCircle);
	void MagCore_Calibration_GetCircleValues(Mat dImg, Mat dCircleImg, int nCenterPosX, int nCenterPosY, float nMidX, float nMidY, float nDeltaX, float nDeltaY, int nSamplingCount, vector<UINT8> &listValue);
	NIPL_ERR MagCore_Inspection_RadiusEdge(Mat dImg, Mat &dRadiusEdgeImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect, bool bMaxRadius);
	NIPL_ERR MagCore_Inspection_RadiusEdge_Inside(Mat dImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect);
	NIPL_ERR MagCore_Inspection_RadiusEdge_Binarize(Mat dImg, Mat &dRadiusEdgeBinImg, Mat &dRadiusEdgeSpotImg, Mat &dRadiusEdgeSpotMask, NIPLParam_MagCore *pParam, bool bMaxRadius, int &nRadiusEdgeEndY, bool bSecondLine = false);
	NIPL_ERR MagCore_Inspection_RadiusEdge_CheckHole(Mat dImg, Mat dBinImg, Mat &dRadiusEdgeHoleImg, Mat &dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, bool bMaxRadius, bool bSecondLine = false);
	bool MagCore_Inspection_RadiusEdge_CheckHoleInsideLine(Mat dLineImg, Mat dHoleImg, Rect rcHole, bool &bPerfectHole);
	NIPL_ERR MagCore_Inspection_RadiusEdge_Line(Mat dImg, Mat &dRadiusEdgeImg, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, int nFullImgSizeY, int nMarginY, bool &bFoundDefect, bool bMaxRadius, bool bSecondLine = false);
	void MagCore_Inspection_RadiusEdge_GetLineProfile(Mat dRadiusEdgeBinImg, Mat &dRadiusEdgeProfileLine, Mat &dRadiusEdgeHoleMask, Mat &dEmptyLineImg, int nRadiusEdgeMaxSize);
	void MagCore_Inspection_RadiusEdge_GetLineProfileCurvePoint(Mat dProfileLine, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, vector<pair<int, bool>> &listCurvePoint);
	void MagCore_Inspection_RadiusEdge_CheckLineProfile(Mat dProfileLine, Mat &dProfileLineOutput, const vector<pair<int, bool>> &listCurvePoint, Mat dRadiusEdgeSpotMask, Mat dRadiusEdgeHoleMask, NIPLParam_MagCore *pParam, bool &bFoundDefect, bool bMaxRadius, bool bSecondLine = false);
	NIPL_ERR MagCore_Inspection_Character(Mat dImg, Mat &dCharImg, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bBrokenChar);
	void MagCore_Inspection_Character_AdjustBoundingBox(Mat dImg, Rect &dBoundRect, NIPLParam_MagCore *pParam);
	bool MagCore_Inspection_Character_Compare(Mat dImg, Mat &dCharTemplateImg, NIPLParam_MagCore *pParam, Point &ptPos, bool &bBrokenChar, float &nMatchRatio, float nRotateAngle);
	void MagCore_Inspection_Convert_Rect(Rect &rc, NIPLParam_Circle2Rect *Param);
	void MagCore_Inspection_Convert_Pos(Point &pt, NIPLParam_Circle2Rect *Param);
	void MagCore_Inspection_ExtendImage(Mat dImg, Mat &dOutputImg, int nExtSize);
	NIPL_ERR MagCore_Inspection_EliminateRectNoise(Mat &dImg, NIPLParam_MagCore *pParam);
	NIPL_ERR MagCore_Inspection_Surface(Mat dImg, Mat &dSurfaceImg, Mat dMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect);
	NIPL_ERR MagCore_Inspection_Hole(Mat dImg, Mat &dHoleImg, Mat dMask, NIPLParam_MagCore *pParam, NIPLResult_Defect_MagCore *pResult, bool &bFoundDefect);
};