#pragma once

#include "CvvImage.h"
#include <vector>
using namespace std;

#define WM_NIPL_IMAGE_POS_VALUE (WM_USER + 812)
#define WM_NIPL_IMAGE_ZOOM (WM_USER + 813)
#define WM_NIPL_IMAGE_POS_DBLCLICK (WM_USER + 814)
#define WM_NIPL_IMAGE_MARK_UPDATE (WM_USER + 815)
#define WM_NIPL_MASK_UPDATE (WM_USER + 816)

#define COLOR_POS RGB(0, 0, 255)
#define COLOR_BOX RGB(255, 0, 0)
#define COLOR_DRAG_BOX RGB(150, 150, 150)

struct AFX_EXT_CLASS ImageMark {
	bool m_bCircle;
	bool m_bFill;
	Rect m_rcBoundingBox;

	ImageMark() {
		Clear();
	}

	void Clear() {
		m_bCircle = false;
		m_bFill = false;
		m_rcBoundingBox = Rect(0, 0, 0, 0);
	}
};

struct SYNC_IMAGE_DATA {
	bool m_bValid;		// flag for validity of this data
	float m_nZoom;
	int m_nPosX;
	int m_nPosY;
	int m_nImagePosX;
	int m_nImagePosY;
	bool m_bUpdateMark;
	vector<ImageMark> m_listImageMark;
	bool m_bUpdateMask;
	Mat m_dMask;
	bool m_bShowMask;

	SYNC_IMAGE_DATA() {
		Clear();
	}

	void Clear() {
		m_bValid = false;
		m_nZoom = 0.f;
		m_nPosX = 0;
		m_nPosY = 0;
		m_nImagePosX = 0;
		m_nImagePosY = 0;
		m_bUpdateMark = false;
		m_listImageMark.clear();
		m_bShowMask = false;
		m_dMask.release();
		m_bUpdateMask = false;
	}
};

class AFX_EXT_CLASS NIPLImageCtl : public CWnd
{
	DECLARE_DYNAMIC(NIPLImageCtl)

public:
	NIPLImageCtl();
	virtual ~NIPLImageCtl();
	BOOL CreateCtl(CWnd* pParentWnd, UINT nID);
	void Clear();

	void Show(Mat dMat, bool bKeepPosAndZoom = false);
	void ShowBackground();
	void ShowImageMark(ImageMark dMark, bool bGotoImagePos = true);
	void ShowImageMark();
	void ClearImageMark();
	void AddImageMark(ImageMark dMark);
	bool HasImageMark();
	void Close();
	void DrawImage();
	void DrawImagePos(bool bUpdateImagePos = true);
	void DrawImageMark();
	void DrawDragBox(CPoint ptStart, CPoint ptEnd, bool bBegin = false);
	void ResetScrollSize(BOOL bZoom = FALSE, BOOL bResetPosition = FALSE);
	void ZoomImage(float nDelta);
	void MoveImage(int nDeltaX, int nDeltaY);

	void SetMask(const Mat &dMask);
	void UpdateMask(CPoint ptStart, CPoint ptEnd, bool bSet);

	bool ChangeCursor();

public:
	static vector<NIPLImageCtl *> listSyncImageTarget;
	static SYNC_IMAGE_DATA dSyncImage;

	void SetSyncImageTarget(bool bSet = true);
	bool IsSyncImageTarget() { return m_bSyncImageTarget; }
	void SetKeepPosAndZoom(bool bSet = true) { m_bKeepPosAndZoom = bSet; }
	bool IsKeepPosAndZoom() { return m_bKeepPosAndZoom; }
	void SetShowImagePos(bool bSet = true);
	bool IsShowImagePos() { return m_bShowImagePos; }
	void SetShowMask(bool bSet = true);

	void SendSyncImage(bool bUpdateMask = false, bool bUpdateMark = false);
	void SyncImage(bool bUpdateMask = false);

	void ChangeZoom(float nZoom, bool bResetZoomBefore = false);
	void GetZoom(float &nZoom, float &nMinZoom, float &nMaxZoom);
	void UpdateShowROI();
	void UpdateImagePos();
	void GetImagePos(CPoint pt, CPoint &ptImagePos);
	bool GetScreenPos(CPoint ptImagePos, CPoint &pt);
	void GotoImagePos(CPoint ptImagePos);
	void Refresh();
	Mat GetImage() { return m_dImg; }
	Mat GetMask() { return m_dMask; }
	vector<ImageMark> &GetImageMarkList() { return m_listImageMark; }
	CWnd *GetParentWnd();
	void SetSendEventMessage(bool bSet);


protected:
	DECLARE_MESSAGE_MAP()

	virtual void PostNcDestroy();
public:
	afx_msg void OnClose();
	afx_msg void OnPaint();

private :
	CvvImage m_dViewImage;
	Mat m_dImg;
	Mat m_dMask;

	int m_nScrollSizeX; 
	int m_nScrollPageSizeX; 
	int m_nScrollMaxPosX; 
	int m_nScrollSizeY; 
	int m_nScrollPageSizeY; 
	int m_nScrollMaxPosY; 
	float m_nMinZoomX;
	float m_nMinZoomY;
	float m_nMinZoom;
	float m_nMaxZoom;
	float m_nZoom;
	float m_nZoomBefore;
	int m_nImgSizeX;
	int m_nImgSizeY;
	bool m_bGrab;
	CPoint m_ptGrabStart;
	bool m_bDrag;
	CPoint m_ptDragStart;
	CRect m_rcPrevDragBox;
	bool m_bSyncImageTarget;
	bool m_bSyncImageHost;
	bool m_bKeepPosAndZoom;
	bool m_bDoingSyncImage;
	Rect m_rcShowROI;
	int m_nImgPosX;
	int m_nImgPosY;
	bool m_bShowImagePos;
	bool m_bShowMask;

	vector<ImageMark> m_listImageMark;

	bool m_bSendEventMessage;

public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};